<?php
Yii::setPathOfAlias('bd', dirname(__FILE__) . '/../../bd');
Yii::setPathOfAlias('common', dirname(__FILE__) . '/../../common');
Yii::setPathOfAlias('main', dirname(__FILE__) . '/../../main');
Yii::setPathOfAlias('mis', dirname(__FILE__) . '/../../mis');
Yii::setPathOfAlias('credit', dirname(__FILE__) . '/../../credit');
Yii::setPathOfAlias('batch', dirname(__FILE__) . '/..');
Yii::setPathOfAlias('gendan', dirname(__FILE__) . '/../../../../gendan');

return array(
    'basePath' => dirname(__FILE__) . '/..',
    'runtimePath' => dirname(__FILE__) . '/../../../../log/batch_api',
    'controllerPath' => dirname(__FILE__) . '/../controller',
    'viewPath' => dirname(__FILE__) . '/../view',
    'extensionPath' => dirname(__FILE__) . '/../../shared',

	'name' => 'Rong360.COM',

	// preloading 'log' component
	'preload' => array('log'),

	// autoloading model and component classes
	'import' => array(
		'main.model.*',
        'main.model.service.*',
        'main.service.rank.*',
        'main.model.dal.*',
        'main.components.*',
		'mis.model.*',
		'common.model.*',
		'common.service.*',
		'common.service.ip.*',
		'common.components.*',
		'bd.model.*',
		'batch.components.*',
		'batch.model.*',
	    'credit.model.*',
	    'credit.model.service.*',
        'gendan.model.*',
        'ext.phpconnectpool.*',
        'ext.encrypt.*',
        'ext.db.*',
        'ext.http.*',
        'ext.passport.*',
        'ext.phpmailer.*',
        'ext.rongconfig.*',
        'ext.tools.*',
        'ext.v_code.*',
        'ext.msg.*',
		'ext.exception.*',
		'ext.400.*',
		'ext.api.*',
	),

    'language' => 'zh_cn',
    'timeZone' => 'Asia/Shanghai',
    'charset' => 'utf-8',
	// application components
	'components' => array(
		'user' => array(
			// enable cookie-based authentication
			'allowAutoLogin' => true,
		),
        'request' => array(
            'enableCookieValidation' => true,
            'enableCsrfValidation' => false,
            'csrfTokenName' => 'RONG360_CSRF_TOKEN',
        ),
        'session' => array(
            'autoStart' => true,
            'timeout' => 3600,
        ),
        'urlManager' => array(
            'showScriptName' => false,
            'urlFormat' => 'path',
            'urlSuffix' => '.html',
            'caseSensitive' => false,
            'rules' => array(
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ),
        ),
		'errorHandler' => array(
			// use 'site/error' action to display errors
            //'errorAction' => 'site/error',
        ),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => ' info',
                    'filter' => 'CRongLogFilter',
				),
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
                    'filter' => 'CRongLogFilter',
					'logFile' => 'application.log.wf',
				),
				// uncomment the following to show log messages on web pages
                /*
                array(
					'class'=>'CWebLogRoute',
				),
                */
			),
		),

		'mailer' => array(
		    'class'=>'ext.phpmailer.CPhpMailer',
			'host' =>'mail.rong360.com',
			'port' =>25,
			'from' =>'service@rong360.com',
			'fromName'=>'rong360',
		),

        'viewRenderer' => array(
            'class' => 'ext.smarty.CSmartyViewRenderer',
            'fileExtension' => '.tpl',
        ),

        'rongConfig' => array(
            'class' => 'ext.rongconfig.CRongConfig',
        ),
        'r400' => array(
            'class' => 'ext.400.C400',
        ), 
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
);
