<?php
Yii::setPathOfAlias('common', dirname(__FILE__) . '/../../../common');
Yii::setPathOfAlias('PhpAmqpLib', dirname(__FILE__) . '/../../../common/shared/PhpAmqpLib');
Yii::setPathOfAlias('batch', dirname(__FILE__) . '/..');
Yii::setPathOfAlias('oa', dirname(__FILE__) . '/../..');

return array(
    'basePath' => dirname(__FILE__) . '/..',
    'runtimePath' => dirname(__FILE__) . '/../../../log/oa',
    'extensionPath' => dirname(__FILE__) . '/../../../common/shared',

    'commandPath' => dirname(__FILE__).'/../command',

	'name' => 'Rong360.COM',
	// preloading 'log' component
	'preload' => array('log'),

	// autoloading model and component classes
	'import' => array(
            'common.model.*',
            'common.service.*',
            'common.service.ip.*',
            'common.components.*',
            'batch.components.*',
            'batch.model.*',
            'oa.components.*',
            'oa.service.*',
            'oa.model.*',
            'ext.phpconnectpool.*',
            'ext.encrypt.*',
            'ext.db.*',
            'ext.http.*',
            'ext.passport.*',
            'ext.phpmailer.*',
            'ext.rongconfig.*',
            'ext.tools.*',
            'ext.v_code.*',
            'ext.msg.*',
            'ext.exception.*',
            'ext.400.*',
            'ext.api.*',
            'ext.phpQuery.*',
            'ext.computervision.*',
	),

    'language' => 'zh_cn',
    'timeZone' => 'Asia/Shanghai',
    'charset' => 'utf-8',

	// application components
	'components' => array(
		'user' => array(
			// enable cookie-based authentication
			'allowAutoLogin' => true,
		),
        'session' => array(
            'autoStart' => true,
            'timeout' => 3600,
        ),
		'errorHandler' => array(
			// use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'info',
                    'filter' => 'CRongLogFilter',
					'logFile' => 'application.log',
				),
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
                    'filter' => 'CRongLogFilter',
					'logFile' => 'application.log.wf',
				),
			),
		),
       'mailer' => array(
            'class' => 'ext.phpmailer.CPhpMailerSMTP',
            //'class' => 'ext.phpmailer.CPhpMailer',
            'host' => 'mail.rong360.com',
            'port' => 25,
            'from' => 'service@rong360.com',
            'fromName' => 'rong360',
            'user' => 'rong360system',
            'pass' => '1c5shw|9a_',
        ),

        'rongConfig' => array(
            'class' => 'ext.rongconfig.CRongConfig',
        ),    
	),

	// application-level parameters that can be accessed
);
