<?php
class CRongConsoleCommand extends CConsoleCommand
{
    protected function beforeAction($action, $params)
    {
		$str = get_class($this) . '.' . $action;
        Yii::log($str . ' process started...', 'info');
		return true;
    }

    protected function afterAction($action, $params)
    {
		$str = get_class($this) . '.' . $action;
        Yii::log($str . ' process finished.', 'info');
    }
}
