<?php
require_once 'soapclientcore.php';

class KRService extends Baidu_Api_Client_Core {
	public function __construct($provinceId=0) {
		parent::__construct('KRService',$provinceId);
	}

    public function getSeedWord($keyword){
        $output_headers = array();
        $arguments = array(
            'getKRbySeedWordRequest' => array(
                'seedWord' => $keyword,
                'seedFilter' =>array (
                    'matchType' => 3,
                    'maxNum' => 500)
            )
        );

        $output_response = $this->soapCall('getKRbySeedWord', $arguments,$output_headers);
        return $output_response;
    }


    public function getKrFileId($keywords){

        $output_headers = array();
        $arguments = array(
            'getKRFileIdbySeedWordRequest' => array(
                'seedWords' => $keywords,
                'seedFilter' =>array (
                    'maxNum' => 500
                )
            )
        );

        $output_response = $this->soapCall('getKRFileIdbySeedWord', $arguments,$output_headers);

        return $output_response->krFileId;
    }

    public function getKrFileState($id){
        $output_headers = array();
        $arguments = array(
            'getKRFileStateRequest' => array(
                'krFileId' => $id
            )
        );
        $output_response = $this->soapCall('getKRFileState', $arguments,$output_headers);

        return $output_response->isGenerated;
    }

    public function getKRFilePath($id){
        $output_headers = array();
        $arguments = array(
            'getKRFilePathRequest' => array(
                'krFileId' => $id
            )
        );
        $output_response = $this->soapCall('getKRFilePath', $arguments,$output_headers);
        return $output_response->filePath;
    }
}
