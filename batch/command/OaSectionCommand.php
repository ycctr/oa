<?php
/**
	extract section info frm section.xls,and insert into table section.
*/

class OaSectionCommand extends CRongConsoleCommand{

    //加入offset、limit，防止中途出现错误，重新运行脚本。
    //offset表示excel中开始要加入的行号。
    //limit表示，从offset开始要加入的行数。
	public function actionInsert($offset = 2,$limit = 99999){
       
        $dataList = self::actionGetDataList();
       // var_dump($dataList[5]);
        //return;
        //获取表头title对应列号（A、B..）和数据库表格列的映射关系，保存到$pair数组中
        $pairs = array();
        $title = $dataList[1];
        //var_dump(excelPairConfig::$userExcelPairs);

        $insertedArray = array();
        $intNum = 1;
        $lastParentId = 0;
        $lastParentName = "";
        //将用户数据，循环插入数据库表oa.section中
        for($intCurrentRow = $offset;$intCurrentRow<($offset+$limit) && $intCurrentRow<=count($dataList);$intCurrentRow++)
        {
            $data = $dataList[$intCurrentRow];
            $arrData = array();
            //var_dump($data);
            //获取section对应数据
            /*foreach($pairs as $key=>$value)
            {
                $arrData["$value"] = $data["$key"];
            }*/
            $name = $data['C'];
            $parent_name = $data['B'];
            $top_section_name = $data['A'];
            if(is_null($name)&& is_null($parent_name)&& is_null($data['A'])){
              var_dump($data);
              break;
            }
            /*if(is_null($parent_name) && is_null($name))
            {
                print (($intNum++)+2)."\n";
            }*/
            if(!is_null($top_section_name))
            {
                $arrData['create_time'] = time();
                $arrData['name'] = $top_section_name;
                $arrData['modify_time'] = time();
                $arrData['status'] = 1;
                $model = new Section();$id=1;
                $id = $model->save($arrData);
                if($id)
                {
                    // $insertedArray["$parent_name"] = $id;
                    $lastTopId = $id;$lastTopName = $top_section_name;
                    echo (($intNum++)+2)."：($id,$top_section_name)\n";
                }
                else
                {
                    return;
                }
            }
            if(/*!array_key_exists($parent_name,$insertedArray) && */!is_null($parent_name))
            {
                $arrData['name'] = $parent_name;
                /*if(strstr($arrData['name'],"本部"))
                {
                    $pos = stripos($arrData['name'],"本部");
                    $arrData['name'] = substr($arrData['name'],0,$pos);
                    if(!strstr($arrData['name'],"部"))
                    {
                         $arrData['name'] = $arrData['name']."部";
                    }
                }*/ 
                $arrData['parent_id'] = $lastTopId;
                $arrData['create_time'] = time();
                $arrData['modify_time'] = time();
                $arrData['status'] = 1;
                $model = new Section();$id=1;
                $id = $model->save($arrData);
                if($id)
                {
                    // $insertedArray["$parent_name"] = $id;
                    $lastParentId = $id;$lastParentName = $parent_name;
                    echo (($intNum++)+2)."：($id,$parent_name)\n";
                }
                else
                {
                    return;
                }
            }
            if(/*!array_key_exists($name,$insertedArray) && */!is_null($name))
            {
                /*if(strstr($name,"本部"))
                {
                    continue;
                }*/
                $arrData['name'] = $name;
                //$arrData['parent_id'] = $insertedArray["$parent_name"];
                $arrData['parent_id'] = $lastParentId;
                $arrData['create_time'] = time();
                $arrData['modify_time'] = time();
                $arrData['status'] = 1;
                $model = new Section();$id=2;
                $id = $model->save($arrData);
                if($id)
                {
                   // $insertedArray["$name"] = $id;
                    echo (($intNum++)+2)."：==============>($id,$name).\n";
                }
                else
                {
                    return;
                }
            }
            //echo "\n";
            //return;
        }
        //echo "完成，插入条数：".($limit)."\n";
        //self::actionGetManager($dataList,$insertedArray);
	}
    public function actionGetDataList()
    {
         //加入环境配置，导入phpexcel类
        $path = Config::$basePath."/trunk/rong360/shared/excel/";
        set_include_path(get_include_path() . PATH_SEPARATOR . $path);
        include 'PHPExcel/IOFactory.php';

        //加载excel文件
        $dataFile =  Config::$basePath."/oa/config/Section.xls";
        if(!file_exists($dataFile))
        {
            print "file not exist --------------".$dataFile."\n";
            exit;
        }
        $objPHPExcel = PHPExcel_IOFactory::load($dataFile); 

        //读取excel文件
        $dataList = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        //var_dump($dataList);
        if(empty($dataList))
        {
            print "file data empty.\n";
            exit;
        }
        return $dataList;
    }
    public function actionGetSectionList()
    {
       //include_once "/home/rong/www/oa/model/OaBaseModel.php";
       //include_once "/home/rong/www/oa/model/Section.php";
        $model = new Section();
        $db = $model->db();
        $strSql = "select id,name from section";
        $ret = $db->query($strSql);
        $section_list = array();
        if(is_array($ret))
        {
            foreach($ret as $current)
            {
                $section_list[''.$current['name']] = $current['id'];
            }
        }
        return $section_list;
    }
    public function actionGetManager()
    {
        //echo "now start to link";
        $dataList = self::actionGetDataList();
        $sectionList = self::actionGetSectionList();
        //var_dump($dataList[2]['A']);
      //  var_dump($sectionList);return;
        //var_dump($dataList[8]);return;
        //var_dump($sectionList);
        if(is_null($dataList) || is_null($sectionList))
            return;
        echo "start for:\n"; //print count($dataList);
        for($intCurrentRow = 2;$intCurrentRow<=count($dataList);$intCurrentRow++)
        {
            //echo "($intCurrentRow)";
            $data = $dataList[$intCurrentRow];
            if(is_null($data['B']) && is_null($data['C']))
            {
                if (is_null($data['A']))
                {
                    break;
                }
                //continue;
            }
            //$section_name = is_null($data['B'])? $data['C'] : $data['B'];
            $section_name = $data['A'] . "".$data['B']."" . "".$data['C'];
            /*$manager_email = $data['K'];
            if(strstr($manager_email,"@"))
            {
                $pos = stripos($manager_email,"@");
                $manager_email = substr($manager_email,0,$pos);
            }*/
            $manager_name = $data['D'];
            //$vp_name = $data['E'];
            //对CEO进行特殊处理
            if($manager_name == "叶大清")
            {
                $manager_name = "YEDAQING";
            }
            if($vp_name == "叶大清"){
                $vp_name = "YEDAQING";
            }
            //$manager_email = "ccm";
            //var_dump($manager_email);
            /*if(strstr($section_name,"本部"))
            {
                $pos = stripos($section_name,"本部");
                $section_name = substr($section_name,0,$pos);
            }*/
            $section_id = $sectionList["$section_name"];
            /*echo  $section_id;
            echo $section_name;
            echo $sectionList["$section_name"];
            */
            if(is_null($section_id))
            {
                //print "is null:$section_name=====$section_id---\n";
                //continue;
                print "can not find section:$section_name";
                break;
            }
            //unset($sectionList["$section_name"]);
            if(empty($sectionList))
                return;
            $model = new User();
            //$manager = $model->GetItemByEmail($manager_email);
            $strSql = "select * from user where name='".$manager_name."'";
            /*if($manager_name=="吕晓琳")
                var_dump($strSql);*/
            $Manager = $model->db()->query($strSql);
            $manager = $Manager[0];
            $manager_id = $manager['id'];
            /*$strSql = "select * from user where name='".$vp_name."'";
            $Vp = $model->db()->query($strSql);
            $vp = $Vp[0];
            $vp_id = $vp['id'];*/
            $arrData = array();
            $arrData['id'] = $section_id;
            $arrData['manager_id'] = $manager_id;
            //$arrData['vp_id'] = $vp_id;
            echo "($intCurrentRow)".$section_id."-".$section_name."-".$manager_id."-".$manager['name']."\n";
            $model = new Section();
            $model->save($arrData);
        }
    }


    //定时脚本，按月给部门增加团建经费
    public function actionAddTeamBuildingCost()
    {
        $sectionModel = new Section(); 

        $rows = $sectionModel->getAllBuildSectionUserNumber();

        if(!empty($rows)){
            $model = new TeamBuildingRecord();
            foreach($rows as $row){
                $arrData = array();
                $arrData['section_id'] = $row['id']; 
                $arrData['date'] = date('Y-m-01');
                $arrData['create_time'] = time();
                $arrData['amount'] = 100*count($row['users']);
                $arrData['remark'] = implode(',',$row['users']);
                $arrData['status'] = STATUS_VALID;
                $item = $model->getAutoItemByDate($arrData['date'],$arrData['section_id']);
                if($item==false){                   
                    if(!$model->save($arrData)){
                        echo $row['name'].'-'.date('Y-m-d').'-'.print_r($arrData,true)."\r\n";
                    } 
                }else{
                    $item['create_time'] = time();
                    $item['amount'] = $arrData['amount'];
                    $item['remark'] = $arrData['remark'];
                    $item['status'] = STATUS_VALID;
                    $isAdd = $model->save($item);
                }   
                           

            }        
        }

        echo "success!\r\n";
    }

    //获取可团建部门和其成员
    public function actionGetAllSectionsAndMembers(){
        $sectionModel = new Section();
        $rows = $sectionModel->getAllBuildSectionUserNumber();
        foreach ($rows as $section){
            $users = implode(',', $section['users']);
            echo $section['id'] . "\t" . $section['name'] . "\t" . $users . "\t" . count($section['users']) ."\n";
        }
    }

    //团建负责人分配团建经费脚本
    public function actionAddTeamBuildingSeniorCost() { 

        $teamSeniosModel = new TeamBuildingSenior();
        $tService = new TeamBuildingService();
        $teamData = $teamSeniosModel->getAllSectionData();

        $dataFile =  Config::$basePath."/oa/config/teambuildingsenior.txt";

        foreach($teamData as $team) {

            $addData = array();
            $addData['from_section'] = $team['from_section'];
            $addData['to_section']   = $team['to_section'];
            $addData['quota']        = $team['quota'];
            $addData['id']           = $team['id'];
            $addData['name']         = $team['name'];
            $isAdd = $tService->batchSeniorBuilding($addData);

            if(!$isAdd) {

              file_put_contents($dataFile, 'seniorid:'.$team['id']."|".var_export($addData, true)."\n", FILE_APPEND);

            }

        }

        echo "success!\r\n";

    }

    //团队激励经费
    public function actionAddTeamEncourageCost() { 

        $sectionModel = new Section(); 
        $rows = $sectionModel->getAllEncourageSectionUserNumber();
        $dataFile =  Config::$basePath."/oa/config/teamEncourageRecord.txt";

        if(!empty($rows)){

            $model = new TeamEncourageRecord();

            foreach($rows as $row){
                $arrData = array();
                $arrData['section_id'] = $row['id']; 
                $arrData['date'] = date('Y-m-01');
                $arrData['create_time'] = time();
                if(count($row['users'])<=1){
                    $arrData['amount'] = 0;
                }elseif($arrData['section_id'] == 111){
                    $arrData['amount'] = 6500;
                }else{
                    $arrData['amount'] = 50*count($row['users']);
                }
                $arrData['remark'] = implode(',',$row['users']);
                $arrData['status'] = STATUS_VALID;
                $item = $model->getAutoItemByDate($arrData['date'],$arrData['section_id']);
                if($item==false){                   
                    $isAdd = $model->save($arrData);
                    if(!$isAdd) {
                        file_put_contents($dataFile, 'encourage:'.$row['id']."|".var_export($addData, true)."\n", FILE_APPEND);
                    }
                }else{
                    $item['create_time'] = time();
                    $item['amount'] = $arrData['amount'];
                    $item['remark'] = $arrData['remark'];
                    $item['status'] = STATUS_VALID;
                    $isAdd = $model->save($item);
                }   
            }
            echo "success!\r\n";              
        }
    }


}

