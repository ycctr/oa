<?php
/**
        extract user info frm user.xls,and insert into table user.
 */
//excelColumnTitle => userTableColumnName
//这里一个例外：分部列存储部门名称，section_id存储部门id，代码中会进行相应的转换
class excelPairConfig{
    public static  $userExcelPairs = array(
        '工号'=> 'id_card', '公司邮箱'=> 'email', '手机'=> 'mobile',
        '姓名'=> 'name', '性别'=> 'sex', '分部'=> 'section_id'
    );

}


class OaUserCommand extends CRongConsoleCommand{

    public static $SECTIONID = 91;//指明要停止发邮件的的部门
    public static $closeEmailSectionIds = array();
    //客服部、审贷部、催收部 因为部门排班的原因，对于10:05后打卡暂不认定为迟到
    public static $exceptionSection = [107,42,245];
    public Static $sectionParentIdMap = array();//部门所属上级部门

    public function actionInsert($offset = 2,$limit = 99999){
        //加入环境配置，导入phpexcel类
        $path = Config::$basePath."/trunk/rong360/shared/excel/";
        set_include_path(get_include_path() . PATH_SEPARATOR . $path);
        include 'PHPExcel/IOFactory.php';

        //加载excel文件
        $dataFile =  Config::$basePath."/oa/config/user-old.xls";
        if(!file_exists($dataFile))
        {
            print "file not exist ".$dataFile."\n";
            exit;
        }
        $objPHPExcel = PHPExcel_IOFactory::load($dataFile); 

        //读取excel文件
        $dataList = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        //var_dump($dataList);
        if(empty($dataList))
        {
            print "file data empty.\n";
            exit;
        }
        //获取表头title对应列号（A、B..）和数据库表格列的映射关系，保存到$pair数组中
        $pairs = array();
        $title = $dataList[1];
        //var_dump(excelPairConfig::$userExcelPairs);
        foreach($title as $key=>$value)
        {
            if ( excelPairConfig::$userExcelPairs["$value"] != null)
            {
                $pairs["$key"] = excelPairConfig::$userExcelPairs["$value"];
            }
        }

        //将用户数据，循环插入数据库表oa.user中

        for($intCurrentRow = $offset;$intCurrentRow<($offset+$limit) && $intCurrentRow<=count($dataList);$intCurrentRow++)
        {
            $data = $dataList[$intCurrentRow];
            $arrData = array();
            //var_dump($pairs);
            //获取user对应数据
            foreach($pairs as $key=>$value)
            {
                $arrData["$value"] = $data["$key"];
            }
            //var_dump($arrData);
            if(is_null($arrData['id_card']))
            {
                echo "完成，插入条数：".($intCurrentRow-$offset)."\n";
                return;
            }
            //对包含名称本部的部门名称，去掉本部
            /*if(strstr($arrData['section_id'],"本部"))
            {
                $pos = stripos($arrData['section_id'],"本部");
                $arrData['section_id'] = substr($arrData['section_id'],0,$pos);
                if(!strstr($arrData['section_id'],"部"))
                {
                    $arrData['section_id'] = $arrData['section_id']."部";
                }
            }*/
            //通过部门名称，获取部门id
            $model = new Section();
            $strSql = sprintf("select id from section where name='%s'",$arrData['section_id']);
            $section_name = $arrData['section_id'];
            $result = $model->db()->query($strSql);
            if(empty($result))
            {
                print "无当前部门，脚本停止。工号：".$arrData['id_card']."\t 部门：$section_name\n";
                exit;
            }
            //对姓名、部门、性别、手机号进行处理,手机号若有多个(以顿号隔开)，默认只取第一个。
            if(strstr($arrData['name'],"(")||strstr($arrData['name'],"（"))
            {
                //var_dump($arrData['name']);
                $Leftpos_e = stripos($arrData['name'],"(");
                $Leftpos_c = stripos($arrData['name'],"（");
                $Leftpos = $Leftpos_e ? $Leftpos_e : $Leftpos_c;
                $span = $Leftpos_e ? 1:3;
                $Rightpos_e = stripos($arrData['name'],")");
                $Rightpos_c = stripos($arrData['name'],"）");
                $Rightpos = $Rightpos_e ? $Rightpos_e : $Rightpos_c;
                $arrData['name'] = mb_substr($arrData['name'],$Leftpos+$span,$Rightpos-$Leftpos-$span);
                /*echo $arrData['name'];
                var_dump($arrData['name']);
                exit;*/
            }
            $arrData['section_id'] = $result[0]['id'];
            if($arrData['sex'] == "男")
            {
                $arrData['sex'] = 1;
            }
            else
            {
                $arrData['sex'] = 0;
            }
            //$arrData['mobile'] = explode('、',$arrData['mobile'])[0];
            //echo "input:".$arrData['name'];
            echo ($intCurrentRow-$offset+1).":".$arrData['name']."--".$arrData['id_card']."--".$arrData['sex']."--".$arrData['email']."--".$arrData['mobile']."--".$section_name."(".$arrData['section_id'].")";
            $arrData['create_time'] = time();
            $arrData['modify_time'] = time();
            $arrData['status'] = 1;
            //处理email,去掉@以及后面的字符
            $pos = stripos($arrData['email'],"@");
            $arrData['email'] = substr($arrData['email'],0,$pos);

            $model = new User();
            $bool = $model->save($arrData);
            if($bool)
            {
                echo "--success.\n";
            }
            else
            {
                echo "--failed.请检查邮箱或者工号是否有重复。\n";
                return;
            }
            //echo "\n";
        }
        echo "完成，插入条数：".($limit<(count($dataList)+1)?$limit:count($dataList)-$offset+1)."\n";
    }

    //导入考勤数据,全量
    public function actionImportkaoqin()
    {
        $dbConfig = array(
            array(
                'service' => 'kaoqinServer',
                'user'    => 'kaoqin',
                'password'=> 'kaoqin123',    
                'databases' => array('CN','JRZX','SH','SZ'),
            )
        );

        foreach($dbConfig as $service){
            $conn = mssql_connect($service['service'],$service['user'],$service['password']);
            if(!$conn){
                echo '连接考勤数据库失败';
            }
            mssql_select_db($service['databases']);

            $ret = mssql_query('SELECT max(userid) as userid,ssn FROM userinfo group by ssn');

            $logPath = '/home/rong/www/log/oa/user.log';
            $model = new CheckInOut();
            $userModel = new User();
            $today = date('Y-m-d');
            while($item = mssql_fetch_assoc($ret)){
                if($item['ssn'] === null || $item['ssn'] == ''){
                    continue;
                }
                $user = $userModel->getItemByIdCard($item['ssn'], 1);
                if(empty($user)){
                    file_put_contents($logPath,'缺失用户:'.print_r($item,true),FILE_APPEND);
                    continue;
                }
                $userid = $item['userid'];
                //获取考勤记录
                $sql = sprintf("select checktime,sensorid from checkinout where userid=%d order by checktime asc ",$userid);
                $rows = mssql_query($sql);
                $data = array();
                while($row = mssql_fetch_assoc($rows)){
                    $time = strtotime($row['checktime']);
                    $key = date('Y-m-d',$time);
                    if($key == $today){
                        continue;
                    }
                    if(isset($data[$key])){
                        if($time < $data[$key]['checkin']){
                            $data[$key]['checkin'] = $time;
                        }elseif($time > $data[$key]['checkout']){
                            $data[$key]['checkout'] = $time;
                        } 
                    }else{
                        $data[$key]['checkin'] = $data[$key]['checkout'] = $time;
                    }
                }
                if(!empty($data)){
                    foreach($data as $day){
                        $day['user_id'] = $user['id'];
                        $day['date'] = date('Y-m-d',$day['checkin']);
                        if($day['checkin'] == $day['checkout']){
                            if(date('H',$day['checkin']) > 12){
                                $day['checkout_time'] = date('Y-m-d H:i:s',$day['checkout']);
                            }else{
                                $day['checkin_time'] = date('Y-m-d H:i:s',$day['checkin']);
                            }
                        }else{
                            $day['checkin_time'] = date('Y-m-d H:i:s',$day['checkin']);
                            $day['checkout_time'] = date('Y-m-d H:i:s',$day['checkout']);
                        }
                        unset($day['checkin']);
                        unset($day['checkout']);
                        $result = $model->save($day);
                        if(!$result){
                            Yii::log('checkinout insert fail:'.print_r($day,true),'info');
                        }
                    }
                }
            }
        }
    }

    public static $dbConfig = array(
        array(
            'service' => 'kaoqinServer',
            'user'    => 'kaoqin',
            'password'=> 'kaoqin123',    
            'databases' => array('JRZX','CN','SH','SZ'),
        )
    );

    //判断用户是不是在三个例外部门中
    public function userInExceptionSection($user)
    {
        if(!isset(self::$sectionParentIdMap[$user['section_id']])){
            $model = new Section();         
            $userSectionIds = $model->getAllSectionIdsByUserId($user['id']);
            self::$sectionParentIdMap[$user['section_id']] = $userSectionIds;
        }else{
            $userSectionIds = self::$sectionParentIdMap[$user['section_id']];
        }
        $exceptionSection = self::$exceptionSection;
        $ret =  array_intersect($userSectionIds,$exceptionSection);
        if(!empty($ret)){
            return true;
        }
        return false;
    }

    public static $sensorIdIpMap = array(
        100 => '10.0.2.246',
        102 => '10.0.2.242',
        103 => '10.0.2.243',
        200 => '10.2.2.101', 
        201 => '10.2.2.102', 
        202 => '10.2.2.103', 
        197 => '10.1.2.229',
        198 => '10.1.2.230',
        199 => '10.1.2.231',  
        150 => '192.168.20.250',
        140 => '192.168.6.250',
    );

    function pingAddress($address) {  
        $status = -1;  
        if (strcasecmp(PHP_OS, 'WINNT') === 0) {  
            // Windows 服务器下  
            $pingresult = exec("ping -n 1 {$address}", $outcome, $status);  
        } elseif (strcasecmp(PHP_OS, 'Linux') === 0) {  
            // Linux 服务器下  
            $pingresult = exec("ping -c 1 {$address}", $outcome, $status);  
        }  
        if (0 == $status) {  
            $status = true;  
        } else {  
            $status = false;  
        }  
        return $status; 
    }  

    public function checkIpLink()
    {
        $failOffice = [];
        foreach(self::$sensorIdIpMap as $sensorid => $ip){
            $pingresult = $this->pingAddress($ip); 
            if($pingresult === false){
                $failOffice[] = self::$sensorIds[$sensorid];
            }
        }
        if(!empty($failOffice)){
            Yii::log('打卡机没有联网的:'.implode(',',$failOffice),'error');
        }
        return array_unique($failOffice);
    }

    public static $sensorIds = array(
        100 => '互联网金融中心', 
        102 => '互联网金融中心',
        103 => '互联网金融中心',
        201 => '时代',
        200 => '时代',
        202  => '时代',
        197 => '昌宁',
        198 => '昌宁',
        199 => '昌宁',
        150 => '深圳',
        140 => '上海',
    );

    //判断是否所有打卡机都有记录
    public function checkSensor($machineList)
    {
        $machines = array_keys($machineList); 
        $sensorIds = array_keys(self::$sensorIds);
        $diff = array_diff($sensorIds,$machines);
        if(!empty($diff)){
            Yii::log('打卡机没有记录的设备号:'.implode(',',$diff),'error');
            return $diff;
        }
        return false;
    }

    //每天跑一次，4点开始导入前一天的考勤数据
    //三重保险看考勤是否异常
    //1、ping考勤机
    //2、看考勤记录中是否有主要考勤设备号
    //3、看总体的异常率。
    //如果出现异常，会在考勤页面提示大家先忽略异常
    public function actionImportthedaykaoqin($begin=null)
    {
        $dbConfig = self::$dbConfig;

        //防止重复发送邮件,初始化joyceyu不发邮件（注意没初始化会报错）
        $repeat_arr = array('joyceyu');
        //需要发送邮件的列表,统一发送
        $mailList = array();
        //考勤人数
        $kaoqinCnt = 0;
        //当前工作日的考勤异常人数
        $current_kaoqin_yichang = 0;
        //考勤机器列表 记录晚上6点至8点有考勤记录的打卡机
        $machineList = array();

        if(is_array($dbConfig)){
            foreach($dbConfig as $service){
                $conn = mssql_connect($service['service'],$service['user'],$service['password']);
                if(!$conn){
                    Yii::log("连接考勤数据库失败:".print_r($service,true),'error');
                    break;
                }
                $dbs = $service['databases'];

                foreach($dbs as $db){
                    mssql_select_db($db);

                    $ret = mssql_query('SELECT max(userid) as userid,ssn FROM userinfo group by ssn');

                    $logPath = '/home/rong/www/log/oa/user.log';
                    $model = new CheckInOut();
                    $userModel = new User();
                    $absenceModel = new Absence();
                    while($item = mssql_fetch_assoc($ret)){
                        if($item['ssn'] === null){
                            continue;
                        }
                        $user = $userModel->getItemByIdCard(trim($item['ssn']), 1);
                        if(empty($user)){
                            file_put_contents($logPath,'缺失用户:'.print_r($item,true),FILE_APPEND);
                            continue;
                        }
                        $kaoqinCnt++;
                        $userid = $item['userid'];

                        //$isExceptionSection = $this->userInExceptionSection($user);
                        if(empty($begin)){
                            $startDay =  date('Y-m-d',strtotime('-1 week'));
                        }else{
                            $startDay = $begin;
                        }
                        $endDay = date('Y-m-d',strtotime('-1 day')); 
                        while($startDay <= $endDay){
                            $nextDay = date("Y-m-d",strtotime('+1 day',strtotime($startDay)));
                            $preDay = date("Y-m-d",strtotime('-1 day',strtotime($startDay)));
                            $holiday = VacationService::isHoliday($startDay);
                            //获取前一天的考勤记录的下班时间
                            $preCheckinout = $model->getItemByUserIdDate($userid,$preDay);
                            if($preCheckinout){
                                $preCheckoutTime = $preCheckinout['checkout_time'];
                            }
                            //获取今天考勤记录
                            $sql = sprintf("select checktime,sensorid from checkinout where userid=%d and (convert(varchar,checktime,23)='%s' or convert(varchar,checktime,23)='%s') order by CHECKTIME asc ",$userid,$startDay,$nextDay);
                            $rows = mssql_query($sql);
                            $data = array();
                            while($row = mssql_fetch_assoc($rows)){
                                $time = strtotime($row['checktime']);
                                if($time > strtotime($endDay.' 18:00:00') && $time < strtotime($endDay.' 21:30:00'))
                                {
                                    if(!isset($machineList[$row['sensorid']])){
                                        $machineList[$row['sensorid']] = 1;
                                    } 
                                    $machineList[$row['sensorid']]++;
                                }
                                if(isset($data[$startDay])){
                                    if($time < $data[$startDay]['checkin'] && $time >= strtotime($startDay." 04:00:00")){
                                        $data[$startDay]['checkin'] = $time;
                                    }elseif($time > $data[$startDay]['checkout'] && $time < strtotime($nextDay." 04:00:00")){
                                        $data[$startDay]['checkout'] = $time;
                                    } 
                                }else{
                                    if($time >= strtotime($startDay." 04:00:00") && $time < strtotime($nextDay." 04:00:00")){
                                        $data[$startDay]['checkin'] = $data[$startDay]['checkout'] = $time;
                                    }                                    
                                }
                            }

                            if(!empty($data)){
                                foreach($data as $day){
                                    $day['user_id'] = $user['id'];
                                    $day['date'] = $startDay;
                                    $record = $model->getItemByUserIdDate($day['user_id'],$day['date']);
                                    if($day['checkin'] == $day['checkout']){
                                        if(date('H',$day['checkin']) > 12){
                                            $day['checkout_time'] = date('Y-m-d H:i:s',$day['checkout']);
                                        }else{
                                            $day['checkin_time'] = date('Y-m-d H:i:s',$day['checkin']);
                                        }
                                    }else{
                                        $day['checkin_time'] = date('Y-m-d H:i:s',$day['checkin']);
                                        $day['checkout_time'] = date('Y-m-d H:i:s',$day['checkout']);
                                    }                                    
                                    if(strtotime($preCheckoutTime) > strtotime($preDay." 22:00:00")){
                                        $lateTime = " 10:30:00";
                                    }else{
                                        $lateTime = " 10:06:00";
                                    }

                                    //工作日上班打卡记录晚于10:06、下班打卡记录早于5:30、上班或者下班没有打卡记录
                                    //都算作异常，需要去看今天是否有请假
                                    //前一日下班打卡时间在22:00之后的，第二日考勤维护时间改为10:30进行维护
                                    if(!$holiday && ($day['checkin'] > strtotime(date('Y-m-d',$day['checkin']).$lateTime) || $day['checkout'] < strtotime(date('Y-m-d',$day['checkout'])." 18:00:00"))){
                                        $vService = new VacationService();
                                        $ret1 = $vService->getAttendanceStatus($user['id'],$day['date']);
                                        if($ret1['status'] != 0){
                                            $day['absence_type'] = $ret1['absence_type']; 
                                            if($ret1['absence_type'] == VACATION_CHUCHAI) {
                                                $day['absence_type'] = SYSTEM_CHUCHA;
                                            }

                                            $day['absenteeism_type'] = $ret1['status'];

                                        } elseif( !in_array($user['email'], $repeat_arr) && (empty($record) || $record['absenteeism_type'] == 0)){//有空勤，并且没请假，发邮件提醒
                                            $closeEmail = $this->closeEmailAlert($user['section_id']);
                                            if( !$closeEmail ){
                                                $mailList[$user['id']] = $user;
                                            }
                                            if($startDay == $endDay){
                                                $current_kaoqin_yichang++; 
                                            }
                                        }
                                    }

                                    //加班至次日凌晨时自动维护
                                    if(date("Y-m-d",$day['checkin'])!=date("Y-m-d",$day['checkout'])){
                                        $day['absenteeism_type'] = 13;
                                    }

                                    if(!empty($record)){ //如果存在的是空记录。更新
                                        $day['id'] = $record['id'];
                                    }
                                    unset($day['checkin']);
                                    unset($day['checkout']);
                                    $result = $model->save($day);
                                    if(!$result){
                                        Yii::log('checkinout insert fail:'.print_r($day,true),'error');
                                    }
                                }
                            }else{
                                //对于没有考勤记录的需要补充缺勤记录，如果是节假日的话 就不需要补充了
                                //员工必须已经入职，才需要补充空白记录
                                $ruzhi = $user['create_time'] <= strtotime($startDay) ? 1 : 0;
                                $record = $model->getItemByUserIdDate($user['id'],$startDay);
                                if(!$holiday && empty($record) && $ruzhi){ //工作日,当天已经入职的员工补录空考勤
                                    $arrData = array();
                                    $arrData['date'] = $startDay; 
                                    $arrData['user_id'] = $user['id'];

                                    //判断当日员工是否请假,是否销假
                                    $vService = new VacationService();
                                    $ret1 = $vService->getAttendanceStatus($user['id'],$startDay);

                                    if($ret1['status'] != 0){
                                        $arrData['absence_type'] = $ret1['absence_type']; 
                                        if($ret1['absence_type'] == VACATION_CHUCHAI) {
                                            $day['absence_type'] = SYSTEM_CHUCHA;
                                        }
                                        $arrData['absenteeism_type'] = $ret1['status'];
                                    }elseif( !in_array($user['email'], $repeat_arr) ) {//有空勤，并且没请假，发邮件提醒
                                        $closeEmail = $this->closeEmailAlert($user['section_id']);
                                        if(!$closeEmail){
                                            $mailList[$user['id']] = $user;
                                        }
                                        if($startDay == $endDay){
                                            $current_kaoqin_yichang++; 
                                        }
                                    }
                                    if(!empty($record)){
                                        $arrData['id'] = $record['id'];
                                    }
                                    $result = $model->save($arrData);
                                    if(!$result){
                                        Yii::log('checkinout insert fail:'.print_r($day,true),'error');
                                    }
                                }

                            }

                            $startDay = date('Y-m-d',strtotime('+1 day',strtotime($startDay)));
                        }
                    }
                }
            }
            $rongCache = new RongCache();
            $key = KAOQINERROR.date('Y-m-d');
            $failOffice = $this->checkIpLink();
            $failSensor = $this->checkSensor($machineList);
            $holiday = VacationService::isHoliday(date('Y-m-d',strtotime('-1 day')));
            if(!$conn || (!$holiday&&($current_kaoqin_yichang/$kaoqinCnt) > 0.5) || !empty($failOffice) || $failSensor){
                //发送考勤脚本异常邮件
                $rongCache->set($key,true,86400);
                $mailer = Yii::app()->mailer->_mailer;
                $mailer->AddAddress('yangwenqing@rong360.com');
                $mailer->AddAddress('zhouhongwei@rong360.com');
                $mailer->AddAddress('wanglu01@rong360.com');
                $mailer->AddAddress('wuhuiqun@rong360.com');
                $mailer->AddAddress('xujingjing01@rong360.com');
                $mailer->Subject = '考勤数据有异常';
                $body = '考勤异常，';
                if(!$conn){
                    $body .= '数据库链接不上，请检查服务;'; 
                }elseif(!empty($failOffice)){
                    $body .= '打卡机未联机，可能造成数据没有完全上传数据库，请检查办公区:'.implode(',',$failOffice);
                }
                if(!$holiday&&($current_kaoqin_yichang/$kaoqinCnt) > 0.5){
                    $body .= '员工大比例出现考勤异常，超过50%，请检查数据是否正确上传。'; 
                }
                if(!empty($failSensor)){
                    $body .= '打卡机没有18-21点记录的设备号：'.implode(',',$failSensor);
                }
                $mailer->Body = $body;
                $mailer->send();
            }else{
                $rongCache->set($key,false,86400);
                foreach($mailList as $mailer){
                    $this->sendKaoqinEmail($mailer);
                }
            }
        }
    }

    public function actionXiuZhengKaoqin(){
        //进行考勤修正，从2016-11-16开始重新跑数据
        $this->actionImportthedaykaoqin("2016-11-16");
    }

    //补全系统和人力计算的11月的年病假差额
    public function actionAddNianAndBingBalance(){
        $userModel = new User();
        $userAll = $userModel->getSectionUser('');
        $vacationModel = new VacationRecord();
        $vacationService = new VacationService();
        foreach ($userAll as $value) {
            $nianHr = $vacationModel->getHrNianOrBingByUser($value['id'],VACATION_NIANJIA);
            $bingHr = $vacationModel->getHrNianOrBingByUser($value['id'],VACATION_BINGJIA);

            $nianSys = $vacationModel->getSysNianOrBingByUser($value['id'],VACATION_NIANJIA);
            $bingSys = $vacationModel->getSysNianOrBingByUser($value['id'],VACATION_BINGJIA);

            $nianAbs = $nianHr -$nianSys;
            if($nianAbs){
                $arrData['user_id'] = $value['id'];
                $arrData['absence_type'] = VACATION_NIANJIA;
                $arrData['date'] = date("Y-m-d",time());
                $arrData['create_time'] = time();
                $arrData['day_num'] = $nianAbs;
                $arrData['status'] = STATUS_VALID;
                $arrData['remark'] = "xiuzheng";
                $arrData['type'] = 1;
                $vacationModel->save($arrData);
            }
            $bingAbs = $bingHr -$bingSys;
            if($bingAbs){
                $arrData['user_id'] = $value['id'];
                $arrData['absence_type'] = VACATION_BINGJIA;
                $arrData['date'] = date("Y-m-d",time());
                $arrData['create_time'] = time();
                $arrData['day_num'] = $bingAbs;
                $arrData['status'] = STATUS_VALID;
                $arrData['remark'] = "xiuzheng";
                $arrData['type'] = 1;
                $vacationModel->save($arrData);
            }
            //将员工11月25日之后的假期重新生成
            $vacationModel->delRowByUserIdAndDate2($value['id'],'1480089600');
            $vacationService->additionalVacationRecord2($value['id'],'1480089600');
        }
        echo "修正完毕";
    }

    public function sendKaoqinEmail($user)
    {
        $arrMail['to'] = $user['email']."@rong360.com";
        $arrMail['subject'] = 'oa '.$user['name'].'你有考勤异常';
        $arrMail['body'] = $user['name'].':'.'考勤异常，请登录oa修改(<a href="http://oa.rong360.com/site/login.html">oa.rong360.com</a>)';
        $mailer = Yii::app()->mailer->_mailer;
        $mailer->Subject = $arrMail['subject'];
        $mailer->AddAddress($arrMail['to']);
        $mailer->Body = $arrMail['body'];
        $is_ok = $mailer->send();

        if($is_ok) {
            Yii::log('day kaoqin send mail to:'.$arrMail['to']);
        } else {
            $log = "";
            $log = var_export($is_ok, true);
            Yii::log('day kaoqin send failed:'.$arrMail['to'].$log);
        }
    }


    //临时补救几天没有导入的考勤记录
    public function actionbulu()
    {
        $startDay = '2016-07-01';
        $this->actionImportthedaykaoqin($startDay);            
    }


    //删除重复的考勤记录
    public function actionDelRepeat()
    {
        $model = new CheckInOut();

        $sql = "select user_id,date,count(*) as cnt from checkinout group by user_id,date having cnt > 1";

        $result = $model->db()->query($sql);

        foreach($result as $row){
            $user_id = $row['user_id'];
            $date = $row['date'];
            if(empty($user_id) || empty($date)){
                continue;
            }

            $sql1 = "select id from checkinout where user_id=".$user_id." and date='".$date."' order by id asc ";
            $rows = $model->db()->query($sql1);
            $ids = array();
            if(count($rows) > 1){
                array_shift($rows);
                foreach($rows as $row){
                    $ids[] = $row['id']; 
                }    
            } 
            if(count($ids)){
                $delSql = "delete from checkinout where id in(".implode(',',$ids).")";
                $model->db()->query($delSql);
            }
        }
    }


    //定时脚本、增加员工的年假、病假
    public function actionAddVacation()
    {
        //取在职员工，不包含实习生
        $sql = "select * from user where status=1 and id_card > 0 ";  
        $userModel = new User();

        $rows = $userModel->db()->query($sql);

        if(empty($rows)){
            return;
        }

        $model = new VacationRecord();

        $preMonth = date('Y-m-26',strtotime('-1 month')); //上次脚本运行的时间
        foreach($rows as $row){
            $work_date = $row['work_date']; 
            $nianjia = $this->getNianjiaNumber($work_date);
            $arrData = array();
            $arrData['user_id'] = $row['id'];
            $arrData['date'] = date('Y-m-01',strtotime('+1 month'));
            $arrData['day_num'] = round($nianjia/12,2);
            $arrData['create_time'] = time();
            $arrData['remark'] = date('m',strtotime('+1 month'))."月新增";
            $arrData['status'] = 1;
            $arrData['absence_type'] = VACATION_NIANJIA;
            $id = $model->save($arrData);
            if(!$id){
                Yii::log('vacation_record insert fail:'.print_r($arrData,true),'info');
                echo $row['name'].'-'.$arrData['date'].'-年假-'.print_r($arrData,true)."\r\n";
                continue;
            }

            $arrData['day_num'] = round(5/12,2);
            $arrData['absence_type'] = VACATION_BINGJIA;
            $id = $model->save($arrData);
            if(!$id){
                Yii::log('vacation_record insert fail:'.print_r($arrData,true),'info');
                echo $row['name'].'-'.$arrData['date'].'-病假-'.print_r($arrData,true)."\r\n";
                continue;
            }


            if($preMonth < '2015-11-01'){ //为了两次调整脚本运行时间的兼容(由每月1号改为26号运行)
                $preMonth = '2015-11-01';
            }

            if( $row['hiredate'] >= $preMonth ){//上个月入职的新员工,补上当月的年假和病假

                $hiredate_time = strtotime($row['hiredate']);

                $next_monthday_date = date("Y-m-01", strtotime('+1 month') ); //下个月1号日期
                $next_monthday_time = strtotime($next_monthday_date);
                $diffDay = intval(($next_monthday_time-$hiredate_time) / 86400) ; 

                $arrData['day_num'] = round( ($nianjia/12/30) * $diffDay,2); 
                $arrData['absence_type'] = VACATION_NIANJIA;
                $arrData['remark'] = date('m')."月新员工根据入职时间, 补假{$diffDay}天";

                if($arrData['day_num'] > 0){
                    $model->save($arrData);
                }

                $arrData['day_num'] = round( (5/12/30) * $diffDay,2);
                $arrData['absence_type'] = VACATION_BINGJIA;
                if($arrData['day_num'] > 0){
                    $model->save($arrData);
                }
            }
        }
    }

    //根据工龄给出员工年假天数
    public function getNianjiaNumber($work_date)
    {
        $num = 0;
        $year = date('Y',time()) - date('Y',strtotime($work_date));

        $month = date('m',time()) - date('m',strtotime($work_date));

        $monthTotal = $year*12 + $month;

        if($monthTotal > 20*12){
            $num = 15;
        }else{
            $num = 10;
        }
        return $num;
    }

    public function actionLuruNianjia()
    {
        $vacationModel = new VacationRecord();
        $model = new User();

        $dataFile =  Config::$basePath."/oa/config/error_ruzhiriqi.txt";
        $dataList = file($dataFile);
        foreach($dataList as $key => $line){
            $row = explode('----',$line);
            $workdate = $row[1];
            $userId = explode(':',$row[0])[1];
            $user = $model->getItemByIdCard($userId);
            if(empty($userId) || empty($user)){
                continue; 
            }
            if($user['work_date'] != date('Y-m-d',strtotime($workdate))){

                $oldNum = $this->getNianjiaNumber($user['work_date']);
                $newNum = $this->getNianjiaNumber($workdate);
                $day_num = round($oldNum/12,2);

                $item = $vacationModel->getItemByUserIdAndDayNum($user['id'],$day_num);
                if(empty($item)){
                    $item = array();
                    $item['user_id'] = $user['id'];
                    $item['date'] = date('Y-m-d',time());
                    $item['day_num'] = round($newNum/12,2);
                    $item['create_time'] = time();
                    $item['status'] = 1;
                    $item['absence_type'] = VACATION_NIANJIA;
                }else{
                    $item['day_num'] = round($newNum/12,2);
                }
                $vacationModel->save($item);

                if(!empty($user)){
                    $user['work_date'] = date('Y-m-d',strtotime($workdate));
                    $model->save($user);
                }
            }
        }
        /*
        $dataFile =  Config::$basePath."/oa/config/nianjia.csv";
        $dataList = file($dataFile);
        foreach($dataList as $key => $line){
            $row = explode(',',$line);
            $user = $model->getItemByIdCard($row[0]);
            if(empty($user)){
                echo 'id_card:'.$row[0]."\r\n";
                continue;
            }else{
                $user['work_date'] = date('Y-m-d',strtotime($row[2]));
                $model->save($user);
            }
            $data = array();
            $data['user_id'] = $user['id'];
            $data['absence_type'] = 2;
            $data['day_num'] = $row[1];
            $data['date'] = date('Y-m-d');
            $data['create_time'] = time();
            $data['status'] = 1;
            $vacationModel->save($data);
        }

        $dataFile =  Config::$basePath."/oa/config/bingjia.csv";
        $bingjiaList = file($dataFile);
        foreach($bingjiaList as $key => $line){
            $row = explode(',',$line);

            $user = $model->getItemByIdCard($row[0]);
            $data = array();
            $data['user_id'] = $user['id'];
            $data['absence_type'] = 1;
            $data['day_num'] = round(($row[1]+5/12),2);
            $data['date'] = date('Y-m-d');
            $data['create_time'] = time();
            $data['status'] = 1;
            $vacationModel->save($data);
        }
         */

    }

    public function actionLuruTuanjian()
    {
        $dataFile =  Config::$basePath."/oa/config/tuanjian.csv";
        $dataList = file($dataFile);
        $model = new Section();
        $tbRecordModel = new TeamBuildingRecord();
        foreach($dataList as $key => $line){
            $row = explode(',',$line);
            $section = $model->getItemByName(trim($row[0]));
            if(empty($section)){
                echo 'section_name:'.$row[0]."\r\n";
                continue;
            }
            $data = array();
            $data['section_id'] = $section['id'];
            $data['date'] = date('Y-m-d');
            $data['create_time'] = time();
            $data['amount'] = round($row[1],2);
            $data['status'] = 1;
            $tbRecordModel->save($data);
        }

    }

    public function actionRuzhiriqi()
    {
        $userModel = new User();
        $vacationModel = new VacationRecord();
        $dataFile =  Config::$basePath."/oa/config/ruzhiriqi.csv";
        $dataList = file($dataFile);
        foreach($dataList as $line){
            $row = explode(',',$line); 
            $user = $userModel->getItemByIdCard($row[0]);
            $hiredate = date('Y-m-d',strtotime($row[1]));
            $workdate = date('Y-m-d',strtotime($row[2]));
            if(empty($user)){
                continue;
            }
            if($hiredate > '1970-01-01'){
                $user['hiredate'] = $hiredate;
            }

            if($user['work_date'] != $workdate){

                $oldNum = $this->getNianjiaNumber($user['work_date']);
                $newNum = $this->getNianjiaNumber($workdate);
                $day_num = round($oldNum/12,2);

                $item = $vacationModel->getItemByUserIdAndDayNum($user['id'],$day_num);
                if(empty($item)){
                    $item = array();
                    $item['user_id'] = $user['id'];
                    $item['date'] = date('Y-m-d',time());
                    $item['day_num'] = round($newNum/12,2);
                    $item['create_time'] = time();
                    $item['status'] = 1;
                    $item['absence_type'] = VACATION_NIANJIA;
                }else{
                    $item['day_num'] = round($newNum/12,2);
                }
                $vacationModel->save($item);

                $user['work_date'] = $workdate;
            }
            $userModel->save($user);
        } 
    }

    public function actionJiaqi8yuefix()
    {
        $njFilePath = Config::$basePath."/oa/config/8yuekounianjia.csv";
        $bjFilePath = Config::$basePath."/oa/config/8yuekoubingjia.csv";

        $userModel = new User();
        $vacationModel = new VacationRecord();
        $list = file($njFilePath);
        foreach($list as $line){
            $row = explode(',',$line);
            $id_card = $row[0];
            $day_num = $row[1];
            $user = $userModel->getItemByIdCard($id_card);

            $data = array();
            $data['user_id'] = $user['id'];
            $data['absence_type'] = 2;
            $data['day_num'] = $day_num;
            $data['date'] = date('Y-m-d');
            $data['create_time'] = time();
            $data['status'] = 1;
            $vacationModel->save($data);
        } 


        $list = file($bjFilePath);
        foreach($list as $line){
            $row = explode(',',$line);
            $id_card = $row[0];
            $day_num = $row[1];
            $user = $userModel->getItemByIdCard($id_card);

            $data = array();
            $data['user_id'] = $user['id'];
            $data['absence_type'] = 1;
            $data['day_num'] = $day_num;
            $data['date'] = date('Y-m-d');
            $data['create_time'] = time();
            $data['status'] = 1;
            $vacationModel->save($data);
        } 
    }

    /**
     * 考勤异常 
     *
     **/
    public function actionCheckingKaoQin() {

        $curDay = date('Ymd',strtotime("+1 day")); 
        $dayPre26 = date("Ym26", strtotime("-1 month") );  //获取上月26日
        $checkModel = new CheckInOut();

        $day = $dayPre26;
        $userTotal = array();
        $check_section_id = array();//存放部门ID
        $userModel = new User();

        while($day < $curDay) {

            $dayTime = strtotime($day);
            $dateTmp    = date("Y-m-d", $dayTime);

            $condition['date'] = $dateTmp;
            if($dateTmp > '2016-11-01'){
                $condition['gt_checkin_time']  = "{$dateTmp} ".KAOQIN_CHECKIN;
            }else{
                $condition['gt_checkin_time']  = "{$dateTmp} 10:30:00";
            }
            
            if($dateTmp > '2016-11-29'){
                $condition['lt_checkout_time']  = "{$dateTmp} ".KAOQIN_CHECKOUT;
            }else{
                $condition['lt_checkout_time']  = "{$dateTmp} 17:30:00";
            }

            $tmpData = array();
            $isHoliday = VacationService::isHoliday($dateTmp);
            if($isHoliday){
                $tmpData = $checkModel->getExcptionListHoliday($dateTmp);
            }else{
                $tmpData = $checkModel->getCheckInOutList($condition);
            }


            if(!empty($tmpData)) {

                foreach($tmpData as $val) {

                    $level = $val['level'];
                    if($level < AUDIT_VP) {
                        //getting section id
                        $check_user_id = $val['user_id'];
                        $user = $userModel->getItemByPk($check_user_id);
                        $check_section_id[$val['email']] = $user['section_id'];//相同的保存一份
                        $email = $val['email'];
                        $userTotal[$email] += 1;
                    }

                }

            }

            $day = date("Ymd", strtotime("$day +1 day") );

        }


        $emailMsg = "";
        $send_date = date("Y-m-26", strtotime("-1 month") );  //获取上月26日
        $month = date("m", strtotime("-1 month") );


        foreach($userTotal as $email=>$toal)
        {
            $closeEmail = false;
            if(!empty($check_section_id[$email]))
            {
                $closeEmail = $this->closeEmailAlert($check_section_id[$email]);
            }
            if( !$closeEmail )
            {
                $i++;
                $email .= "@rong360.com";
                $this->sendEmail($email,$total,$month);
            }
        }
        $msg = "W1447932252:考勤统计总共发送邮件{$i}条记录";
        echo $msg."\n";
        Yii::log($msg);
    } 

    public function sendEmail($email, $total, $month) {

        $mailer = Yii::app()->mailer->_mailer;
        $mailer->Subject = "【重要】您有异常考勤需要维护";
        $mailer->AddAddress($email);
        $mailer->Body = "亲爱的小融：<br/><p style='text-indent: 2em'>{$month}月26日至今，您有<strong style='color:red;'>{$total}</strong>天考勤异常，请登录OA系统维护异常情况，操作步骤：OA--打卡记录--异常描述。</p>
            <p style='text-indent: 2em'><strong>点击进入：</strong><a href='http://oa.rong360.com/userworkflow/list.html'>我的OA系统</a><p>
            <p style='text-indent: 2em'>系统中考勤信息将作为薪资核算依据，为不影响工资发放，请您及时维护。</p>";
        $is_ok = $mailer->send();

        if($is_ok) {

            Yii::log('month kaoqin send mail to:'.$email);

        } else {

            $log = "";
            $log = var_export($is_ok, true);
            Yii::log('month kaoqin send failed:'.$email.$log);

        }

    }

    //关闭指定部门邮件提醒功能
    public function closeEmailAlert($section_id)
    {
        if(empty(self::$closeEmailSectionIds)){
            $sectionModel = new Section();
            self::$closeEmailSectionIds = $sectionModel->getSectionIdsById(self::$SECTIONID);
        }
        if(in_array($section_id,self::$closeEmailSectionIds)){
            return true;
        }
        return false;
    }

    //测试测试接口
    public function actionDemo() {

        $arrMail['to'] = "qiaoyuan@rong360.com";
        $arrMail['subject'] = 'oa 你有考勤异常';
        $arrMail['body'] = '考勤异常，请登录oa修改(<a href="http://oa.rong360.com/site/login.html">oa.rong360.com</a>)';
        $mailer = Yii::app()->mailer->_mailer;
        $mailer->Subject = $arrMail['subject'];
        $mailer->AddAddress($arrMail['to']);
        $mailer->Body = $arrMail['body'];
        $is_ok = $mailer->send();

        if($is_ok) {

            Yii::log('day kaoqin send mail to:'.$arrMail['to']);

        } else {

            $log = "";
            $log = var_export($is_ok, true);
            Yii::log('day kaoqin send failed:'.$arrMail['to'].$log);

        }

    }

    //对未进行借款报销的每半个月发送一封邮件
    public function actionSendFinanceOffsetEmail(){
        $model = new FinanceLoan();
        $financeOffsetList = $model->getFullLoanList();

        foreach ($financeOffsetList as $item){
            if($item['email']){
                //获取财务审核通过的日期
                $userWorkflowModel = new UserWorkFlow();
                $userWorkflowId = $userWorkflowModel->getUserWorkflowId($item['id'],'finance_loan');
                $modify_time = $userWorkflowModel->getItemByPk($userWorkflowId)['modify_time'];
                $email = $item['email']."@rong360.com";
                $arrMail['to'] = $email;
                $arrMail['subject'] = '您有借款未报销';
                $arrMail['body'] = '您有一笔借款尚未归还：<br><br>借款时间： '.date("Y年m月d日",$item['modify_time']).'<br>借款金额： '.$item['amount'].'元<br>借款事由： '.$item['summary'].'<br>烦请及时登录OA-发起新的流程-借款冲抵，及时核销该笔借款。在审核通过后OA上打印单据，连同发票一起送到财务部办理核销。';
                $mailer = Yii::app()->mailer->_mailer;
                $mailer->Subject = $arrMail['subject'];
                $mailer->AddAddress($arrMail['to']);
                $mailer->Body = $arrMail['body'];
                $is_ok = $mailer->send();
                if($is_ok) {
                    Yii::log('finance offset send mail to:'.$arrMail['to']);
                } else {
                    $log = "";
                    $log = var_export($is_ok, true);
                    Yii::log('finance offset send failed:'.$arrMail['to'].$log);
                }
            }
        }
        echo "发送完毕";
    }

    //对于休假完成后5天仍未审批通过的假期，如果一级负责人已审核，系统自动审批通过
    public function actionAutoPassVacation(){
        $absenceModel = new Absence();
        $userTaskModel = new UserTask();
        $userWorkflowModel = new UserWorkFlow();
        $list = $absenceModel->getUnpassedList(); 
        // var_dump($list);
        // exit();
        foreach ($list as $value) {
            while (1) {
                if($value['audit_status'] == OA_WORKFLOW_AUDIT_PASS){
                   break;
                }
                $lastTask = $userTaskModel->getLastTaskByUwfId($value['uwf_id']);
                $lastTask['audit_user_id'] = $lastTask['user_id'];
                $lastTask['audit_status'] = OA_TASK_TONGGUO;
                $userTaskModel->save($lastTask);
                $workflowService = new WorkflowService($value['uwf_id']);
                $workflowService->workflowProcess();
                $value['audit_status'] = $userWorkflowModel->getItemByPk($value['uwf_id'])['audit_status'];
            }
        }
    }

    //五周年邮件贺卡
    public function actionFiveYearEmail()
    {
        $filePath = '/home/rong/www/oa/config/wuzhounian.csv';
        $users = file($filePath);
        $userModel = new User();

        $sectionMap = ['Estaff' => 1,'运营' => 2,'产品研发' => 3,'风控' => 4,'市场&广告' => 5,'职能' => 6];
        $day = date('Y-m-d');
        if(is_array($users)){
            foreach($users as $str){
                $user = explode(',',$str);
                $data['year'] = date('Y-m-d',strtotime($user[7]));    
                $data['rank'] = $user[6];
                $data['daynum']  = $user[8];
                $data['section'] = $user[5];
                $mobile = trim($user[4]);
                $mobile = rongcrypt_encrypt($mobile);
                if($user[2] == '在职' && $day=='2016-10-14'){
                    $email = trim($user[3]);
                    $email = trim($email,"?");
                    $email = trim($email,'"');
                    if(!in_array($email,$this->emailConfig)){
                        continue;
                    }
                    $mailer = Yii::app()->mailer->_mailer;
                    $mailer->Subject = "公司五周年庆典";
                    $mailer->AddAddress($email);
                    $mailer->Body = $this->getEmailBody($data);
                    $is_ok = $mailer->send();

                    if($is_ok) {
                        Yii::log('month kaoqin send mail to:'.$email);
                    } else {
                        $log = var_export($is_ok, true);
                        Yii::log('month kaoqin send failed:'.$email.$log);
                    }
                }else{
                    if($day == '2016-10-14'){
                        continue;
                    }
                    $url = 'http://m.rong360.com/welcome/'.trim($data['year']).'_'.trim($data['rank']).'_'.trim($data['daynum']).'_'.$sectionMap[$data['section']];
                    $sortUrl = SortUrl::getSortUrl($url);
                    $params = ['mobile'=>$mobile,'template_id'=>10527,'short_url'=>$sortUrl];
                    MessageService::sendMessageApi($params);
                }
            }
        }
    }

    public function getEmailBody($data)
    {
        $year = date('Y年m月d日',strtotime($data['year']));
        if($data['section'] == 'Estaff' || $data['section'] == '职能'){
            $str = '<meta charset="utf-8"><table cellspacing="0" cellpadding="0" style="text-align:center;font-family: Microsoft YaHei;"><tr><td><img src="http://s0.rong360.com/upload/png/a0/97/a0979a0fa90308d5ffe3e5f9a2ff038d.png" alt="" width="720"></td></tr><tr><td><table cellspacing="0" cellpadding="0" ><tr><td valign="top"><img src="http://s0.rong360.com/upload/png/55/88/55887770e165fc1457b3b00a3543fd25.png" width="196"></td><td valign="top" style="width:334px;"><p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;'.$year.'，</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;你加入了融360，</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;成为了第<font color="#ED4F43">'.$data['rank'].'</font>个小融</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;我们共同度过了<font color="#3284FF">'.$data['daynum'].'</font>天！</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;padding-top: 15px;">&nbsp;你已不可替代，</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;你的陪伴，我们必将全力以赴！</p>    </td>    <td valign="top"><img src="http://s0.rong360.com/upload/png/ad/da/adda15c56b147fa4ac429da8afa24e0a.png"  width="190"></td>    </tr>    </table>    </td>    </tr>    <tr>    <td valign="top"><img src="http://s0.rong360.com/upload/png/4e/22/4e22496c9f94611949e3c79a981d5b7d.png" alt="" width="720" style="margin-top:-3px;"></td>    </tr>    </table>';
        }elseif($data['section'] == '运营'){
            $str = '<meta charset="utf-8"><table cellspacing="0" cellpadding="0" style="text-align:center;font-family: Microsoft YaHei;"><tr><td><img src="http://s0.rong360.com/upload/png/ad/65/ad657a9ef1236ba24e50b012cd72484c.png" alt="" width="720"></td></tr><tr><td><table cellspacing="0" cellpadding="0" ><tr><td valign="top"><img src="http://s0.rong360.com/upload/png/91/80/918072d9e6a01a51d854d3ed16162a7e.png" width="196"></td><td valign="top" style="width:334px;"><p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;'.$year.'，</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;你加入了融360，</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;成为了第<font color="#ED4F43">'.$data['rank'].'</font>个小融</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;我们共同度过了<font color="#3284FF">'.$data['daynum'].'</font>天！</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;padding-top: 15px;">&nbsp;你已不可替代，</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;你的陪伴，我们必将全力以赴！</p>    </td>    <td valign="top"><img src="http://s0.rong360.com/upload/png/a4/f5/a4f5bb2879729400ae6df278dd963d72.png"  width="190"></td>    </tr>    </table>    </td>    </tr>    <tr>    <td valign="top"><img src="http://s0.rong360.com/upload/png/98/4f/984f8bfecf8bd0f3cabe8192366f8275.png" alt="" width="720" style="margin-top:-3px;"></td>    </tr>    </table>';
        }elseif($data['section'] == '产品研发'){
            $str ='<meta charset="utf-8"><table cellspacing="0" cellpadding="0" style="text-align:center;font-family: Microsoft YaHei;"><tr><td><img src="http://s0.rong360.com/upload/png/f5/d0/f5d0301708eee86e0350ad447ecf150e.png" alt="" width="720"></td></tr><tr><td><table cellspacing="0" cellpadding="0" ><tr><td valign="top"><img src="http://s0.rong360.com/upload/png/86/31/8631570c5d6c9f871f06b30d1124f9f5.png" width="196"></td><td valign="top" style="width:334px;"><p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;'.$year.'，</p><p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;你加入了融360，</p><p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;成为了第<font color="#ED4F43">'.$data['rank'].'</font>个小融</p><p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;我们共同度过了<font color="#3284FF">'.$data['daynum'].'</font>天！</p><p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;padding-top: 15px;">&nbsp;你已不可替代，</p><p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;你的陪伴，我们必将全力以赴！</p></td><td valign="top"><img src="http://s0.rong360.com/upload/png/4c/c2/4cc2ccf48a20bf508cb60fa3dd666fbe.png"  width="190"></td></tr></table></td></tr><tr><td valign="top"><img src="http://s0.rong360.com/upload/png/a6/c0/a6c0ce5af0933d4304c887145920d99d.png" alt="" width="720" style="margin-top:-3px;"></td></tr></table>';
        }elseif($data['section'] == '风控'){
            $str = '<meta charset="utf-8"><table cellspacing="0" cellpadding="0" style="text-align:center;font-family: Microsoft YaHei;"><tr><td><img src="http://s0.rong360.com/upload/png/f8/60/f860007a079b4d97e9793fccdf2c4c8e.png" alt="" width="720"></td></tr><tr><td><table cellspacing="0" cellpadding="0" ><tr><td valign="top"><img src="http://s0.rong360.com/upload/png/09/f6/09f6750eb939b47df2e60620820107be.png" width="196"></td><td valign="top" style="width:334px;"><p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;'.$year.'，</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;你加入了融360，</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;成为了第<font color="#ED4F43">'.$data['rank'].'</font>个小融</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;我们共同度过了<font color="#3284FF">'.$data['daynum'].'</font>天！</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;padding-top: 15px;">&nbsp;你已不可替代，</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;你的陪伴，我们必将全力以赴！</p>    </td>    <td valign="top"><img src="http://s0.rong360.com/upload/png/2a/ec/2aec3cf7076648f29e5a9951dae38293.png"  width="190"></td>    </tr>    </table>    </td>    </tr>    <tr>    <td valign="top"><img src="http://s0.rong360.com/upload/png/c1/30/c130285688b7ddeb9a466b2d38bedd5d.png" alt="" width="720" style="margin-top:-3px;"></td>    </tr>    </table>';
        }elseif($data['section'] == '市场&广告'){
            $str = '<meta charset="utf-8"><table cellspacing="0" cellpadding="0" style="text-align:center;font-family: Microsoft YaHei;"><tr><td><img src="http://s0.rong360.com/upload/png/19/f6/19f62f54bc6c99624f5b9e69e90b12ad.png" alt="" width="720"></td></tr><tr><td><table cellspacing="0" cellpadding="0" ><tr><td valign="top"><img src="http://s0.rong360.com/upload/png/0b/e8/0be8eca19f4c0312fe2c492a7469a7fd.png" width="196"></td><td valign="top" style="width:334px;"><p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;'.$year.'，</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;你加入了融360，</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;成为了第<font color="#ED4F43">'.$data['rank'].'</font>个小融</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;我们共同度过了<font color="#3284FF">'.$data['daynum'].'</font>天！</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;padding-top: 15px;">&nbsp;你已不可替代，</p>    <p style="font-family: Microsoft YaHei;text-align:center;width:334px;font-size: 23px;color:#512C20;margin:0;padding:0px;line-height:38px;">&nbsp;你的陪伴，我们必将全力以赴！</p>    </td>    <td valign="top"><img src="http://s0.rong360.com/upload/png/da/7a/da7a5cf7dc5969a1f5d67e947943ead1.png"  width="190"></td>    </tr>    </table>    </td>    </tr>    <tr>    <td valign="top"><img src="http://s0.rong360.com/upload/png/9d/ae/9dae42f94f6c18b8b2dc47d851170ef3.png" alt="" width="720" style="margin-top:-3px;"></td>    </tr>    </table>';
        }
        return $str;
    }


    public $emailConfig = array(
        "litongtong@rong360.com"
        ,"tianpeng@rong360.com"
        ,"quxiuxiu@rong360.com"
        ,"xujun01@rong360.com"
        ,"lixiaoci@rong360.com"
        ,"douhaibo@rong360.com"
        ,"lixiajun@rong360.com"
        ,"haomeng@rong360.com"
        ,"wuhao@rong360.com"
        ,"xuyugui@rong360.com"
        ,"jichenbo@rong360.com"
        ,"zhangpenghui@rong360.com"
        ,"liguangzhe@rong360.com"
        ,"yangfeng@rong360.com"
        ,"wujunqi@rong360.com"
        ,"zhangwenjing@rong360.com"
        ,"yangjinlong@rong360.com"
        ,"wangwen@rong360.com"
        ,"zhuxinglin@rong360.com"
        ,"fenghan@rong360.com"
        ,"wangmengsheng@rong360.com"
        ,"dengxinyue@rong360.com"
        ,"songxuejiao01@rong360.com"
        ,"liuyafeng@rong360.com"
        ,"wangxing@rong360.com"
        ,"tala@rong360.com"
        ,"dingwei@rong360.com"
        ,"lizhiwei@rong360.com"
        ,"liyanjiao@rong360.com"
        ,"zhaozhenyue@rong360.com"
        ,"yangchenglu@rong360.com"
        ,"zhangyangyang@rong360.com"
        ,"xuchaojie@rong360.com"
        ,"wangyadan@rong360.com"
        ,"tangshijie@rong360.com"
        ,"hexiaodong@rong360.com"
        ,"zhaotingting@rong360.com"
        ,"wanglu01@rong360.com"
        ,"xuchen@rong360.com"
        ,"dingxue@rong360.com"
        ,"linyu@rong360.com"
        ,"zhaojing01@rong360.com"
        ,"lijia01@rong360.com"
        ,"zhangjiaojiao@rong360.com"
        ,"zhaottiantian@rong360.com"
        ,"zhanlili@rong360.com"
        ,"guoyanjiao@rong360.com"
        ,"fanyaoyao@rong360.com"
        ,"huapei@rong360.com"
        ,"liyong@rong360.com"
        ,"lihanyang@rong360.com"
        ,"liman@rong360.com"
        ,"wangdandan02@rong360.com"
        ,"litian@rong360.com"
        ,"donghang@rong360.com"
        ,"liujie@rong360.com"
        ,"liujuan@rong360.com"
        ,"yuanmengyao@rong360.com"
        ,"suhongli@rong360.com"
        ,"wangmi@rong360.com"
        ,"liuweiyu@rong360.com"
        ,"renpanjun@rong360.com"
        ,"wangjinping@rong360.com"
        ,"wangzixuan@rong360.com"
        ,"daiyu@rong360.com"
        ,"zhaoyiyuan@rong360.com"
    );


}

