<?php
class VacationCommand extends CRongConsoleCommand{

    //年假 病假记录 重新生成步骤
    //1、导入到7月底的剩余年假
    //2、扣除7.26-8.1号之间审批通过的假期
    //3、7.26-8.1之间销假，重新扣除假期
    //4、8月假期系统生成
    //5、扣除8.1-9.1之间审批通过的假期
    //6、8.1-9.1之间销假的重新扣除假期
    //7、9月假期系统生成
    //8、扣除9.1-现在之间审批过的机器
    //9、9.1-现在销假的重新扣除假期
    
    public function actionVacationRecord()
    {
        $this->actionLuruNianjia(); 
        $this->absenceVacationRecord('2015-07-26','2015-08-01');
        $this->cancelAbsenceVacationRecord('2015-07-26','2015-08-01');

        //增加8月假期
        $this->addVacation('2015-08-01');
        $this->absenceVacationRecord('2015-08-01','2015-09-01');
        $this->cancelAbsenceVacationRecord('2015-08-01','2015-09-01');

        //增加9月假期
        $this->addVacation('2015-09-01');
        $this->absenceVacationRecord('2015-09-01','2015-10-01');
        $this->cancelAbsenceVacationRecord('2015-09-01','2015-10-01');

        //增加10月假期
        $this->addVacation('2015-10-01');
        $this->absenceVacationRecord('2015-10-01','2015-11-01');
        $this->cancelAbsenceVacationRecord('2015-10-01','2015-11-01');

        //增加11月假期
        $this->addVacation('2015-11-01');
        $this->absenceVacationRecord('2015-11-01','2015-11-26');
        $this->cancelAbsenceVacationRecord('2015-11-01','2015-11-26');

        //增加12月假期
        $this->addVacation('2015-12-01');
        $this->absenceVacationRecord('2015-11-26','2015-12-26');
        $this->cancelAbsenceVacationRecord('2015-11-26','2015-12-26');

        //增加2016年1月假期
        $this->addVacationNianjiaOrBingjia('2016-01-01',2);
        $this->absenceVacationRecord('2015-12-26','2016-01-01');
        $this->cancelAbsenceVacationRecord('2015-12-26','2016-01-01');

        //病假清零脚本
        $this->actionrestBingJia();
        $this->addVacationNianjiaOrBingjia('2016-01-01',1);
        $this->absenceVacationRecord('2016-01-01','2016-01-07');
        $this->cancelAbsenceVacationRecord('2016-01-01','2016-01-07');
        echo "操作成功\n";

    }


    public function actionLuruNianjia()
    {
        $vacationModel = new VacationRecord();
        $model = new User();

        $dataFile =  Config::$basePath."/oa/config/nianjia.csv";
        $dataList = file($dataFile);
        foreach($dataList as $key => $line){
            $row = explode(',',$line);
            $user = $model->getItemByIdCard($row[0]);
            if(empty($user)){
                echo 'id_card:'.$row[0]."\r\n";
                continue;
            }

            $data = array();
            $data['user_id'] = $user['id'];
            $data['absence_type'] = 2;
            $data['day_num'] = $row[1];
            $data['date'] = '2015-07-25';
            $data['create_time'] = time();
            $data['status'] = 1;
            $data['remark'] = '2015年7月底剩余年假';
            $vacationModel->save($data);
        }

        $dataFile =  Config::$basePath."/oa/config/bingjia.csv";
        $bingjiaList = file($dataFile);
        foreach($bingjiaList as $key => $line){
            $row = explode(',',$line);

            $user = $model->getItemByIdCard($row[0]);
            $data = array();
            $data['user_id'] = $user['id'];
            $data['absence_type'] = 1;
            $data['day_num'] = round($row[1],2);
            $data['date'] = '2015-07-25';
            $data['create_time'] = time();
            $data['status'] = 1;
            $data['remark'] = '2015年7月底剩余病假';
            $vacationModel->save($data);
        }
    }

    public function absenceVacationRecord($startDay,$endDay)
    {
        $vService = new VacationService();
        $absenceModel = new Absence();
        $absences =  $absenceModel->getItemsByDay($startDay,$endDay);

        if(!empty($absences)){
            foreach($absences as $absence){
                $vService->createHolidayRecordByAbsence($absence); 
            }
        }
    }

    public function cancelAbsenceVacationRecord($startDay,$endDay)
    {
        $cancelAbsenceModel = new CancelAbsence(); 
        $cancelAbsences = $cancelAbsenceModel->getItemsByDay($startDay,$endDay);

        $vService = new VacationService();
        $model = new VacationRecord(); 
        $absenceModel = new Absence();
        if(!empty($cancelAbsences)){
            foreach($cancelAbsences as $cancelAbsence){
                $end_day = date('Y-m-d',$cancelAbsence['end_time']);
                $day = date('Ymd',$cancelAbsence['start_time']);
                $model->delRowByUserIdAndDate($cancelAbsence['user_id'],date('Y-m-d',$cancelAbsence['start_time']));


                $rows = $absenceModel->getItemByUserIdAfterDateQujian($cancelAbsence['user_id'],$cancelAbsence['end_time'],$startDay,$endDay);

                if(!empty($rows)){
                    foreach($rows as $absence){
                        if($absence['start_time'] < $cancelAbsence['end_time']){
                            $absence['start_time'] = $cancelAbsence['end_time'];
                        }
                        $vService->createHolidayRecordByAbsence($absence);
                    } 
                }
            }
        }
    }


    //定时脚本、增加员工的年假、病假
    public function addVacation($day)
    {
        $time = strtotime($day);
        //取当前时间已入职的在职员工，不包含实习生 
        $sql = "select * from user where status=1 and id_card > 0 and unix_timestamp(hiredate) < $time";  
        $userModel = new User();

        $rows = $userModel->db()->query($sql);

        if(empty($rows)){
            return;
        }

        $model = new VacationRecord();
        foreach($rows as $row){
            $work_date = $row['work_date']; 
            $nianjia = $this->getNianjiaNumber($work_date);
            $arrData = array();
            $arrData['user_id'] = $row['id'];
            $arrData['date'] = date('Y-m-d',$time);
            $arrData['day_num'] = round($nianjia/12,2);
            if(date('m',$time) == 12 && $nianjia == 10){
                $arrData['day_num'] +=0.04;
            }
            $arrData['create_time'] = $time;
            $arrData['remark'] = date('m',$time)."月新增";
            $arrData['status'] = 1;
            $arrData['absence_type'] = VACATION_NIANJIA;
            $id = $model->save($arrData);
            if(!$id){
                Yii::log('vacation_record insert fail:'.print_r($arrData,true),'info');
                echo $row['name'].'-'.date('Y-m-d').'-年假-'.print_r($arrData,true)."\r\n";
                continue;
            }

            $arrData['day_num'] = round(5/12,2);
            if(date('m',$time) == 12){
                $arrData['day_num'] -=0.04;
            }
            $arrData['absence_type'] = VACATION_BINGJIA;
            $id = $model->save($arrData);
            if(!$id){
                Yii::log('vacation_record insert fail:'.print_r($arrData,true),'info');
                echo $row['name'].'-'.date('Y-m-d').'-病假-'.print_r($arrData,true)."\r\n";
                continue;
            }

            $preMonth = date('Y-m-d',strtotime('-1 month',$time)); //上次脚本运行的时间
            if($preMonth >= '2015-08-01'){
                if($row['hiredate'] >= $preMonth){//上个月入职的新员工,补上当月的年假和病假
                    $total_day = (strtotime(date('Y-m-d',$time)) - strtotime($preMonth))/3600*24;
                    $ruzhi_day = (strtotime(date('Y-m-d',$time)) - strtotime($row['hiredate']))/3600*24;
                    $arrData['day_num'] = round($nianjia/12*($ruzhi_day/$total_day),2); 
                    $arrData['absence_type'] = VACATION_NIANJIA;
                    $arrData['remark'] = date('m',strtotime($preMonth))."月新增假期";
                    if($arrData['day_num'] > 0){
                        $model->save($arrData);
                    }

                    $arrData['day_num'] = round(5/12*($ruzhi_day/$total_day),2);
                    $arrData['absence_type'] = VACATION_BINGJIA;
                    if($arrData['day_num'] > 0){
                        $model->save($arrData);
                    }
                }
            }
        }
    }

    //定时脚本、增加员工的年假
    public function addVacationNianjiaOrBingjia($day,$type)
    {
        $time = strtotime($day);
        //取当前时间已入职的在职员工，不包含实习生 
        $sql = "select * from user where status=1 and id_card > 0 and unix_timestamp(hiredate) < $time";  
        $userModel = new User();

        $rows = $userModel->db()->query($sql);

        if(empty($rows)){
            return;
        }

        $model = new VacationRecord();
        foreach($rows as $row){
            $work_date = $row['work_date']; 
            $nianjia = $this->getNianjiaNumber($work_date);
            $arrData = array();
            $arrData['user_id'] = $row['id'];
            $arrData['date'] = date('Y-m-d',$time);
            $arrData['day_num'] = round($nianjia/12,2);
            if(date('m',$time) == 12 && $nianjia == 10){
                $arrData['day_num'] +=0.04;
            }
            $arrData['create_time'] = $time;
            $arrData['remark'] = date('m',$time)."月新增";
            $arrData['status'] = 1;
            $arrData['absence_type'] = VACATION_NIANJIA;
            if($type==2){
                $id = $model->save($arrData);
                if(!$id){
                    Yii::log('vacation_record insert fail:'.print_r($arrData,true),'info');
                    echo $row['name'].'-'.date('Y-m-d').'-年假-'.print_r($arrData,true)."\r\n";
                    continue;
                }
            }

            if($type==1){
                $arrData['day_num'] = round(5/12,2);
                if(date('m',$time) == 12){
                    $arrData['day_num'] -=0.04;
                }
                $arrData['absence_type'] = VACATION_BINGJIA;
                $id = $model->save($arrData);
                if(!$id){
                    Yii::log('vacation_record insert fail:'.print_r($arrData,true),'info');
                    echo $row['name'].'-'.date('Y-m-d').'-病假-'.print_r($arrData,true)."\r\n";
                    continue;
                }
            }

            $preMonth = date('Y-m-d',strtotime('-1 month',$time)); //上次脚本运行的时间
            if($preMonth >= '2015-08-01'){
                if($row['hiredate'] >= $preMonth){//上个月入职的新员工,补上当月的年假和病假
                    $total_day = (strtotime(date('Y-m-d',$time)) - strtotime($preMonth))/3600*24;
                    $ruzhi_day = (strtotime(date('Y-m-d',$time)) - strtotime($row['hiredate']))/3600*24;
                    $arrData['day_num'] = round($nianjia/12*($ruzhi_day/$total_day),2); 
                    $arrData['absence_type'] = VACATION_NIANJIA;
                    $arrData['remark'] = date('m',strtotime($preMonth))."月新增假期";
                    if($type==2){
                        if($arrData['day_num'] > 0){
                            $model->save($arrData);
                        }
                    }

                }
            }
        }
    }

    //根据工龄给出员工年假天数
    public function getNianjiaNumber($work_date)
    {
        $num = 0;
        $year = date('Y',time()) - date('Y',strtotime($work_date));

        $month = date('m',time()) - date('m',strtotime($work_date));

        $monthTotal = $year*12 + $month;

        if($monthTotal > 20*12){
            $num = 15;
        }else{
            $num = 10;
        }
        return $num;
    }

    /**
     * 重置病假
     **/
    public function actionrestBingJia() {

        //init
        $absence_type = 1;          //病假
        $date         = date("Y-m-d");
        $status       = 1;         //有效
        $absence_id   = 0;         //无请假记录
        $preYear      = date("Y") - 1;
        $remark       = $preYear.'年终病假重置';
        $create_time  = time();


        //病假全部读出来；
        $vacationModel = new VacationRecord(); 
        $absenceModel = new Absence();
        $condition = array(

            "absence_type" => $absence_type,
            "ledate"       => $date,

            );
        $cur_time = strtotime($date);
        $data = $vacationModel -> getOverplusVacation($condition);
        foreach($data as $val) {
            $addData = array();

            if($val ['day_num'] <= 0) {

                continue;

            }

            $user_id = $val['user_id'];
            $addData['user_id'] = $user_id;
            $addData['day_num'] = - $val ['day_num'];
            $addData['absence_type'] = $absence_type;
            $addData['date']         = $date;
            $addData['status']       = $status;
            $addData['absence_id']   = $absence_id;
            $addData['create_time']  = $create_time;
            $addData['remark']       = $remark;

            $is_add = $vacationModel->save($addData);

            if(!$is_add) {


                Yii::log('restbingjia_vacation insert fail:'.print_r($arrData,true),'info');

            }

            $wheresql = " absence.user_id={$user_id} AND absence.type = 1 AND absence.status=1  AND  user_workflow.audit_status = 2   AND absence.end_time < $cur_time ";
            $is_check = $absenceModel -> getListWithCondition($wheresql);

            if(!empty($is_check)) {

                $uname = $is_check[0]['name'];
                $str_data .= "{$uname}: userid:$user_id:".date("Y-m-d H:i:s")." \n";
            
            }

        }

        $fileLog =  Config::$basePath."/oa/config/rest_bingjia.txt";
        file_put_contents($fileLog, $str_data);

        echo "重置病假操作成功\n";

    }

    public function actionDemo() {

        
        $this->addVacationNianjiaOrBingjia('2016-01-01',1);


    }

}

?>
