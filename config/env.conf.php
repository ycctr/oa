<?php
//测试环境下，可以定义YII_DEBUG, YII_TRACE_LEVEL，用于调试。
define('YII_DEBUG', false);
define('YII_TRACE_LEVEL', 1); 
class REnv
{
	//开发模式
	public static $devMode = true; 
	public static $testMail = "limanman@rong360.com";
	public static $qc_mail ="limanman@rong360.com";
	public static $default_order_day_limit = 5;
    public static $fastlend_single_line_log=false;
	public static $push_order_to_anjiashihang=true;

//	public static $test_mobile = 13910584390;
//	public static $test_mobile = 15901174672;
	public static $crmHostConfig = array('host' => '127.0.0.1', 'port' => '80');

	public static $jinqiaoIp = array(
			'127.0.0.1' => 1,
			'192.168' => 1,
			);

	public static $pimgUrl = 'http://static.rong360.com/';
	//js文件路径
	public static $jsPath   = '/static/js/';

	//移动版静态文件路径配置
	public static $mobileStatPath   = '/static/mobile/';    
	
	public static $mis_dev_mode= true;
#	public static $mis_dev_mode= false;
	//public static $mis_operation_400 = false;
	public static $mis_operation_400 = true;

	//支付相关
	 public static $onlinepay_channel_rand_config = array('pnr'=>100,'99bill'=>0);
	 public static $chinapnrWebUrl = 'http://test.chinapnr.com/gar/RecvMerchant.do';//前台提交地址
	 public static $chinapnrBackendUrl = 'http://test.chinapnr.com/gar/RecvMerchant.do';//后台提交地址
	 public static $chinapnrMerId = '510010';//商户号
	 public static $npcServerHost = '127.0.0.1'; //服务IP
     public static $npcServerPort = 8733; //服务端口号
	 public static $chinapnrBgRetUrl = 'http://119.57.151.158/bd/recharge/receivePnrBg.html'; //后台返回的前缀
	 public static $chinapnrRetUrl = 'http://119.57.151.158/recharge/receivePnr.html'; //web浏览器返回的url前缀
		     

    public static $kefuMysqlConfig = array(
        'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,                                                                                                                               
            'strategy'              => array('name' => 'Simple')
        ),  
            
        'machine' => array(
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
        ),  

        'auth' => array(
            'dbuser' => 'root',
            'dbpass' => 'rong360.com',
            'dbname' => 'kefu'
        ),  
    );
    public static $passportMysqlConfig = array(
        'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,
            'strategy'              => array('name' => 'Simple')
        ),  
        'machine' => array(
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
            #array('host' => 'dbmaster.rong360.com', 'port' => 3550),
           # array('host' => '10.0.2.102', 'port' => 3550),
        ),  
        'auth' => array(
            'dbuser' => 'root',
            'dbpass' => 'rong360.com',
            'dbname' => 'r360_passport'
        ),
    );
	public static $moneyMysqlConfig = array(

			'talk' => array(
				'connect_timeout_ms'    => 1000,
				'read_timeout_ms'       => 4000,
				'write_timeout_ms'      => 1000,
				'retry'                 => 2,
				'strategy'              => array('name' => 'Simple')
				),

			'machine' => array(
				array('host' => 'dbmaster.rong360.com', 'port' => 3550),
				),

			'auth' => array(
				'dbuser' => 'root',
				'dbpass' => 'rong360.com',
				'dbname' => 'money'
				),
			);
  
    public static $safeMysqlConfig = array(
        'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,                                                                                                                               
            'strategy'              => array('name' => 'Simple')
        ),  
            
        'machine' => array(
            #array('host' => '10.0.2.40', 'port' => 3550),
            #array('host' => '10.0.2.40', 'port' => 3550),
            #array('host' => '10.0.2.40', 'port' => 3550),
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
        ),  

        'auth' => array(
            'dbuser' => 'root',
            'dbpass' => 'rong360.com',
            'dbname' => 'safe'
        ),  
    ); 
   
    public static $icreditMysqlConfig = array(
	'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,
            'strategy'              => array('name' => 'Simple')
        ),

        'machine' => array(
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
        ),

        'auth' => array(
            'dbuser' => 'root',
            'dbpass' => 'rong360.com',
            'dbname' => 'icredit'
        ),
    );
    public static $icreditMysqlConfigRo = array(
	'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,
            'strategy'              => array('name' => 'Simple')
        ),

        'machine' => array(
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
        ),

        'auth' => array(
            'dbuser' => 'root',
            'dbpass' => 'rong360.com',
            'dbname' => 'icredit'
        ),
    );
    
    public static $wdMysqlConfig = array(
        'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,
            'strategy'              => array('name' => 'Simple')
        ),

        'machine' => array(
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
        ),

        'auth' => array(
            'dbuser' => 'root',
            'dbpass' => 'rong360.com',
            'dbname' => 'wd'
        ),
    );

    public static $wdMysqlConfigRo = array(
        'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,
            'strategy'              => array('name' => 'Simple')
        ),

        'machine' => array(
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
        ),

        'auth' => array(
            'dbuser' => 'root',
            'dbpass' => 'rong360.com',
            'dbname' => 'wd'
        ),
    );
    
    public static $jinqiaoMysqlConfig = array(
            'talk' => array(
                'connect_timeout_ms'    => 1000,
                'read_timeout_ms'       => 4000,
                'write_timeout_ms'      => 1000,
                'retry'                 => 2,
                'strategy'              => array('name' => 'Simple')
                ),
            'machine' => array(
                array('host' => 'dbmaster.rong360.com', 'port' => 33550),
                ),
            'auth' => array(
                'dbuser' => 'rong360',
                'dbpass' => 'rong360.com',
                'dbname' => 'jinqiao'
                ),
            );

    public static $rong360OfficeIp = array(
            '10.10.10.40',
        /*
			array('119.57.11.10','119.57.11.13'), //北京
			'210.13.116.70','210.13.107.94', //上海
			array('10.10.0.0','10.10.255.255'),
			'116.6.18.2', '116.6.18.58', //深圳
			'10.10.20.149',
			'10.10.10.91',
        */
	    	array('10.10.10.0','10.10.10.255'),
		array('10.0.8.0','10.0.8.255'),
		array('10.0.0.0','10.0.255.255'),
     );  

    public static $crmMysqlConfig = array(
	    'talk' => array(
		    'connect_timeout_ms'    => 1000,
		    'read_timeout_ms'       => 4000,
		    'write_timeout_ms'      => 1000,
		    'retry'                 => 2,
		    'strategy'              => array('name' => 'Simple')
	    ),
	    'machine' => array(
			array('host' => 'dbmaster.rong360.com', 'port' => 3550),
	    ),
	    'auth' => array(
		    'dbuser' => 'crmwrite',
		    'dbpass' => 'crm@write',
		    'dbname' => 'crm'
	    ),
    );

   public static $mysqlConfigMis = array(
        'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,
            'strategy'              => array('name' => 'Simple')
        ),
        'machine' => array(
           # array('host' => '10.0.2.105', 'port' => 3550),
           # array('host' => '10.0.2.105', 'port' => 3550),
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
        ),
        'auth' => array(
            'dbuser' => 'root',
            'dbpass' => 'rong360.com',
            'dbname' => 'r360'
        ),
    );

    public static $crmMysqlConfigRo = array(
	    'talk' => array(
		    'connect_timeout_ms'    => 1000,
		    'read_timeout_ms'       => 4000,
		    'write_timeout_ms'      => 1000,
		    'retry'                 => 2,
		    'strategy'              => array('name' => 'Simple')
	    ),
	    'machine' => array(
			array('host' => 'dbmaster.rong360.com', 'port' => 3550),
	    ),
	    'auth' => array(
		    'dbuser' => 'crmread',
		    'dbpass' => 'crm@read',
		    'dbname' => 'crm'
	    ),
	);

    public static $dpfApiMysqlConfig = array(                                                                                                               
        'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,
            'strategy'              => array('name' => 'Simple')
        ),  
        'machine' => array(
            array('host' => 'dpf_api.rong360.com', 'port' => 3550),
            array('host' => 'dpf_api.rong360.com', 'port' => 3550),
        ),  
        'auth' => array(
            'dbuser' => 'dpf_api_read',
            'dbpass' => 'r1t9WyUjnu',
            'dbname' => 'dpf_api',
        ),  
    ); 

    public static $creditMysqlConfig = array(
        'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,
            'strategy'              => array('name' => 'Simple')
        ),

        'machine' => array(
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
        ),

        'auth' => array(
            'dbuser' => 'creditwrite',
            'dbpass' => 'credit@write',
            'dbname' => 'credit'
        ),
        );

    public static $creditMysqlConfigRo = array(
            'talk' => array(
                'connect_timeout_ms'    => 1000,
                'read_timeout_ms'       => 4000,
                'write_timeout_ms'      => 1000,
                'retry'                 => 2,
                'strategy'              => array('name' => 'Simple')
                ),

            'machine' => array(
                array('host' => 'dbmaster.rong360.com', 'port' => 3550),
                ),

            'auth' => array(
                'dbuser' => 'creditread',
                'dbpass' => 'credit@read',
                'dbname' => 'credit'
                ),
            );

    public static $callRecordConfig = array(
        'talk' => array(
                'connect_timeout_ms'    => 1000,
                'read_timeout_ms'       => 4000,
                'write_timeout_ms'      => 1000,
                'retry'                 => 2,
                'strategy'              => array('name' => 'Simple')
            ),
            'machine' => array(
                array('host' => 'dbcall.rong360.com', 'port' => 3306),
            ),
            'auth' => array(
                'dbuser' => 'mysql2010',
                'dbpass' => 'mysql2010',
                'dbname' => 'asteriskcdrdb'
        )
    );


    public static $cmsMysqlConfig = array(
        'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,
            'strategy'              => array('name' => 'Simple')
        ),

        'machine' => array(
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
        ),

        'auth' => array(
            'dbuser' => 'root',
            'dbpass' => 'rong360.com',
            'dbname' => 'cms'
        ),
        );

    public static $cmsMysqlConfigRo = array(
        'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,
            'strategy'              => array('name' => 'Simple')
        ),

        'machine' => array(
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
        ),

        'auth' => array(
            'dbuser' => 'root',
            'dbpass' => 'rong360.com',
            'dbname' => 'cms'
        ),
        );
    public static $mysqlConfig = array(
	    'talk' => array(
		    'connect_timeout_ms'    => 1000,
		    'read_timeout_ms'       => 4000,
		    'write_timeout_ms'      => 1000,
		    'retry'                 => 2,
		    'strategy'              => array('name' => 'Simple')
	    ),
	    'machine' => array(
			#array('host' => 'dbmaster.rong360.com', 'port' => 3550),
			array('host' => 'dbmaster.rong360.com', 'port' => 3550),
	    ),
	    'auth' => array(
		    'dbuser' => 'root',
		    'dbpass' => 'rong360.com',
		    'dbname' => 'r360'
	    ),
    );


    public static $mysqlConfigRo = array(
	    'talk' => array(
		    'connect_timeout_ms'    => 1000,
		    'read_timeout_ms'       => 4000,
		    'write_timeout_ms'      => 1000,
		    'retry'                 => 2,
		    'strategy'              => array('name' => 'Simple')
	    ),
	    'machine' => array(
			#array('host' => '10.0.2.105', 'port' => 3550),
			array('host' => 'dbmaster.rong360.com', 'port' => 3550),
	    ),
	    'auth' => array(
		    'dbuser' => 'root',    //这个账号要能同时读取r360和safe库
		    'dbpass' => 'rong360.com',

		    //'dbuser' => 'r360read',    
		    //'dbpass' => 'r360@read',
		    'dbname' => 'r360'
	    ),
    );

	public static $dpfMysqlConfig = array(                                                                                                                  
        'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,
            'strategy'              => array('name' => 'Simple')
        ),  

        'machine' => array(
            array('host' => 'dbmaster.rong360.com', 'port' => 3550), 
        ),  

        'auth' => array(
            'dbuser' => 'root',
            'dbpass' => 'rong360.com',
            'dbname' => 'dpf'
        ),  
    );                   

	public static $licaiMysqlConfigRo = array(
		'talk' => array(
	    	'connect_timeout_ms'    => 1000,
			'read_timeout_ms'       => 4000,
			'write_timeout_ms'      => 1000,
			'retry'                 => 2,
			'strategy'              => array('name' => 'Simple')
		),
		
		'machine' => array(
			array('host' => 'dbmaster.rong360.com', 'port' => 3550),
		),

		'auth' => array(
			'dbuser' => 'licairead',
			'dbpass' => 'licai@read',
			'dbname' => 'licai'
		),
	);
	public static $licaiMysqlConfigWo = array(
		'talk' => array(
	    	'connect_timeout_ms'    => 1000,
			'read_timeout_ms'       => 4000,
			'write_timeout_ms'      => 1000,
			'retry'                 => 2,
			'strategy'              => array('name' => 'Simple')
		),
		
		'machine' => array(
			array('host' => 'dbmaster.rong360.com', 'port' => 3550),
		),

		'auth' => array(
			'dbuser' => 'licaiwrite',
			'dbpass' => 'licai@write',
			'dbname' => 'licai'
		),
	);
    public static $bbsMysqlConfig = array(                                                                                           
                'talk' => array(
                 'connect_timeout_ms'    => 5000,
               'read_timeout_ms'       => 10000,
                 'write_timeout_ms'      => 5000,
                'retry'                 => 2,
                'strategy'              => array('name' => 'Simple')
               ),  

             'machine' => array(
                 array('host' => 'dbmaster.rong360.com', 'port' => 3550),
                     ),  

              'auth' => array(
                      'dbuser' => 'bbswrite',
                     'dbpass' => 'bbs@read',
                      'dbname' => 'rbbs'
                      ),  
            );  

    public static $bbsMysqlConfigRO = array(                                                                                           
                'talk' => array(
                 'connect_timeout_ms'    => 5000,
               'read_timeout_ms'       => 10000,
                 'write_timeout_ms'      => 5000,
                'retry'                 => 2,
                'strategy'              => array('name' => 'Simple')
               ),  

             'machine' => array(
                 array('host' => 'dbmaster.rong360.com', 'port' => 3550),
                     ),  

              'auth' => array(
                      'dbuser' => 'bbsread',
                     'dbpass' => 'bbs@read',
                      'dbname' => 'rbbs'
                      ),  
            );  

	public static $licaiMysqlConfig = array(
		'talk' => array(
	    	'connect_timeout_ms'    => 1000,
			'read_timeout_ms'       => 4000,
			'write_timeout_ms'      => 1000,
			'retry'                 => 2,
			'strategy'              => array('name' => 'Simple')
		),
		
		'machine' => array(
			array('host' => 'dbmaster.rong360.com', 'port' => 3550),
		),

		'auth' => array(
			'dbuser' => 'licaiwrite',
			'dbpass' => 'licai@write',
			'dbname' => 'licai'
		),
	);
    public static $gendanMysqlConfig = array(
        'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,
            'strategy'              => array('name' => 'Simple')
        ),  
        'machine' => array(
            array('host' => 'dbgendan.rong360.com', 'port' => 3550),
            array('host' => 'dbgendan.rong360.com', 'port' => 3550),
            array('host' => 'dbgendan.rong360.com', 'port' => 3550),
        ),  
        'auth' => array(
            'dbuser' => 'root',
            'dbpass' => 'rong360.com',
            'dbname' => 'gendan'
        ),  
    ); 


	 public static $pay2MysqlConfig = array(
	 	'talk' => array(
				'connect_timeout_ms'    => 1000,
				'read_timeout_ms'       => 4000,
				'write_timeout_ms'      => 1000,
				'retry'                 => 2,
				'strategy'              => array('name' => 'Simple')
		),
		'machine' => array(
				array('host' => 'dbmaster.rong360.com', 'port' => 3550),
				array('host' => 'dbmaster.rong360.com', 'port' => 3550),
		),
		'auth' => array(
				'dbuser' => 'root',
				'dbpass' => 'rong360.com',
				'dbname' => 'pay2'
		),
	); 

    public static $riskMysqlConfig = array(
        'talk' => array(
            'connect_timeout_ms'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
            'retry'                 => 2,
            'strategy'              => array('name' => 'Simple')
        ),  
            
        'machine' => array(
            #array('host' => '10.0.2.40', 'port' => 3550),
            array('host' => 'dbmaster.rong360.com', 'port' => 3550),
        ),  

        'auth' => array(
            'dbuser' => 'root',
            'dbpass' => 'rong360.com',
            'dbname' => 'risk'                                                                                                                               
        ),  
    );  
	public static $httpConfig = array(
		'talk' => array(
			'connect_timeout_ms'    => 2000,
			'read_timeout_ms'       => 10000,
			'write_timeout_ms'      => 1000,
		),  
		'machine' => array(
			array('host' => 'dbproxy.rong360.com', 'port' => 9011),
			array('host' => 'dbproxy.rong360.com', 'port' => 9011),
		),  
	);




	public static $memcacheConfig = array(
		//array('host' => 'memcached1.rong360.com','port' => '12360'),
		array('host' => '10.10.10.34','port' => '2222'),
		//array('host' => 'memcached2.rong360.com','port' => '12360'),
	);
	 public static $redisConfig = array(
		 array('host' => '127.0.0.1', 'port' => '6379'),
		 //array('host' => 'nutcracker1.rong360.com', 'port' => '6379'),
	 );
    public static $qaSearch = array(
        array('backend_url' => 'http://solra.rong360.com:8080/solr/collection1/select', 
              'timeout_ms' => 6000),
        array('backend_url' => 'http://solrb.rong360.com:8080/solr/collection1/select', 
              'timeout_ms' => 6000),
    );  

    public static $glSearch = array(
        array('backend_url' => 'http://solra.rong360.com:8080/solr/collection_gl/select', 
              'timeout_ms' => 3000),
        array('backend_url' => 'http://solrb.rong360.com:8080/solr/collection_gl/select', 
              'timeout_ms' => 3000),
    );  

    public static $hwSearch = array(
        array('backend_url' => 'http://solra.rong360.com:8080/solr/collection_hw/select', 
              'timeout_ms' => 3000),
        array('backend_url' => 'http://solrb.rong360.com:8080/solr/collection_hw/select', 
              'timeout_ms' => 3000),
    );  

    /** 通用 API 配置 */
    public static $unifyApiConfig = array( 
        /** 产品线统一 API 配置: 主站*/ 
        '50' => array( 
            /** 通讯超时时间设置 */ 
            'talk' => array( 
                'connect_timeout_ms'    => 2000, 
                'read_timeout_ms'       => 10000, 
                'write_timeout_ms'      => 1000, 
            ),  
            /** 服务站机器列表 */ 
            'machine' => array(
                array( 
                    'host' => 'dpf.rong360.com', 
                    'port' => 80,  
                ),   
            ),   
            /** 产品线 token 密钥 */ 
            'secret' => '@rong363#dpf', 
            /**  
             * 接口访问控制策略                                                                                                                              
             * intranet: 仅容许从内网访问 
             * public: 公网也可以访问接口服务 
             * */ 
            'access' => 'intranet', 
        ),  
        PRODUCT_LINE_RONG360 => array( 
            /** 通讯超时时间设置 */ 
            'talk' => array( 
                'connect_timeout_ms'    => 2000, 
                'read_timeout_ms'       => 10000, 
                'write_timeout_ms'      => 1000, 
            ),  
            /** 服务站机器列表 */ 
            'machine' => array(
                array( 
                    'host' => '127.0.0.1', 'port' => 80,  
                ),   
            ), 
            'domain' =>'www.rong360.com',
            'secret' => '@rong363#main', 
            'verify_token' => false,
            'access' => 'intranet', 
        ),  




        PRODUCT_LINE_GENDAN => array( 
            /** 通讯超时时间设置 */ 
            'talk' => array( 
                'connect_timeout_ms'    => 2000, 
                'read_timeout_ms'       => 10000, 
                'write_timeout_ms'      => 1000, 
            ),  
            /** 服务站机器列表 */ 
            'machine' => array(
                array( 
                    'host' => '127.0.0.1', 'port' => 80,
                ),   
            ), 
            'domain' =>'gendan.rong360.com',
            'secret' => '@rong363#gendan', 
            'verify_token' => false,
            'access' => 'intranet', 
    	),



		PRODUCT_LINE_PAY2 => array(
			//通讯超时时间设置
			'talk' => array(
				'connect_timeout_ms' => 2000, 
				'read_timeout_ms' => 10000, 
				'write_timeout_ms' => 1000, 
			),
			//服务站机器列表
			'machine' => array(
				array(
					//'host' => '127.0.0.1', 'port' => 80,
					'host' => 'pay2.rong360.com', 'port'=>80,
				)
			),
			'domain' =>'pay2.rong360.com',
			'secret' => '@rong360AK47#pay2#1216', 
			'verify_token' => false,
			'access' => 'intranet', 
		),


        //支付签名系统pay2sign
        PRODUCT_LINE_PAY2SIGN => array( 
            /** 通讯超时时间设置 */ 
            'talk' => array( 
                'connect_timeout_ms'    => 2000, 
                'read_timeout_ms'       => 10000, 
                'write_timeout_ms'      => 1000, 
            ),   
            /** 服务站机器列表 */ 
            'machine' => array(
                array('host' => 'pay2sign.rong360.com', 'port' => 80),  
            ),   
            'domain' =>'pay2sign.rong360.com',
            'secret' => '@rong360AK47#pay2#1216', 
            'verify_token' => false,
            'access' => 'intranet', 
        ), 


 
        PRODUCT_LINE_RYJ_APP => array( 
            /** 通讯超时时间设置 */ 
            'talk' => array( 
                'connect_timeout_ms'    => 2000, 
                'read_timeout_ms'       => 20000,
                'write_timeout_ms'      => 1000, 
            ),  
            /** 服务站机器列表 */ 
            'machine' => array(
                array( 
                    'host' => 'solra.rong360.com', 'port' => 80,  
                    //'host' => 'batch.rong360.com', 'port' => 80,  
                ),   
            ),  
            'domain' =>'batch.rong360.com',
            'secret' => '@rong363#batch', 
            'verify_token' => false,
            'access' => 'intranet', 
        ),  
        PRODUCT_LINE_BATCH => array(                                              
            /** 通讯超时时间设置 */ 
            'talk' => array( 
                'connect_timeout_ms'    => 2000, 
                'read_timeout_ms'       => 20000,
                'write_timeout_ms'      => 1000, 
            ),  
            /** 服务站机器列表 */ 
            'machine' => array(
                array( 
                    'host' => '127.0.0.1', 'port' => 80,  
                    //'host' => 'batch.rong360.com', 'port' => 80,  
                ),   
            ), 
            'domain' =>'batch.rong360.com',
            'secret' => '@rong363#batch', 
            'verify_token' => false,
            'access' => 'intranet', 
        ),  
        //风控系统
        PRODUCT_LINE_RISK => array( 
            /** 通讯超时时间设置 */ 
            'talk' => array( 
                'connect_timeout_ms'    => 2000, 
                'read_timeout_ms'       => 10000, 
                'write_timeout_ms'      => 1000, 
            ),  
            /** 服务站机器列表 */ 
            'machine' => array(
                array('host' => 'risk.rong360.com', 'port' => 80),  //2013.12.30 modify by caoxiaolin 前端机器变更
            ), 
            'domain' =>'risk.rong360.com',
            'secret' => '@rong363#risk', 
            'verify_token' => false,
            'access' => 'intranet', 
	),
    PRODUCT_LINE_MBD_APP => array( 
            /** 通讯超时时间设置 */ 
            'talk' => array( 
                'connect_timeout_ms'    => 2000, 
                'read_timeout_ms'       => 10000, 
                'write_timeout_ms'      => 1000, 
            ),  
            /** 服务站机器列表 */ 
            'machine' => array(
                array('host' => 'm.bd.rong360.com', 'port' => 80),
            ),  
            'domain' =>'m.bd.rong360.com',
            'secret' => '@rong360#mbd-app', 
            'verify_token' => true,
            'access' => 'internet', 
        ),  
	prODUCT_LINE_KEFU => array( 
        /** 通讯超时时间设置 */ 
        'talk' => array( 
        'connect_timeout_ms'    => 2000, 
        'read_timeout_ms'       => 10000, 
        'write_timeout_ms'      => 1000, 
        ),  
        /** 服务站机器列表 */ 
        'machine' => array(
        array('host' => 'kefu.rong360.com', 'port' => 80),  
        ), 
        'domain' =>'kefu.rong360.com',
        'secret' => '@rong363#kefu', 
        'verify_token' => false,
    	),  

  
   PRODUCT_LINE_MONEY => array(
         /** 通讯超时时间设置 */
        'talk' => array(
        'connect_timeout_ms'    => 2000,
        'read_timeout_ms'       => 10000,
        'write_timeout_ms'      => 1000,
        ),
        /** 服务站机器列表 */
        'machine' => array(
         array('host' => '127.0.0.1', 'port' => 80),
         array('host' => '127.0.0.1', 'port' => 80),
         ),
        'domain' =>'www.rong360.com',
        'secret' => '@rong363#lc',
        'verify_token' => true,
        'access' => 'intranet',
    ),


 
       
        //工具类
        PRODUCT_LINE_TOOLS => array( 
            /** 通讯超时时间设置 */ 
            'talk' => array( 
                'connect_timeout_ms'    => 2000, 
                'read_timeout_ms'       => 10000, 
                'write_timeout_ms'      => 1000, 
            ),  
            /** 服务站机器列表 */ 
            'machine' => array(
                array('host' => 'tools.rong360.com', 'port' => 80),  
            ), 
            'domain' =>'tools.rong360.com',
            'secret' => '@rong363#tools#1215', 
            'verify_token' => true,
            'access' => 'intranet', 
        ),
PRODUCT_LINE_ICREDIT_WEB => array(
	        /** 通讯超时时间设置 */
	        'talk' => array(
	        'connect_timeout_ms'    => 5000,
	        'read_timeout_ms'       => 20000,
	        'write_timeout_ms'      => 5000,
	        ),
	        /** 服务站机器列表 */
	        'machine' => array(
	        array(
	        'host' => 'icredit.rong360.com', 'port' => 80,
	        ),
	        ),
	        'domain' =>'icredit.rong360.com',
	        /*
	         * 产品线密钥
	         */
	        'secret' => '@rong360#icredit-web',
	        'verify_token' => true,
	        /**
	         * 接口访问控制策略
	         * intranet: 仅容许从内网访问
	         * public: 公网也可以访问接口服务
	         * */
	        'access' => 'intranet',
        ),
   PRODUCT_LINE_BD => array(
         /** 通讯超时时间设置 */
        'talk' => array(
        'connect_timeout_ms'    => 2000,
        'read_timeout_ms'       => 10000,
        'write_timeout_ms'      => 1000,
        ),
                                                                                                    /** 服务站机器列表 */
        'machine' => array(
         array('host' => '127.0.0.1', 'port' => 80),
         array('host' => '127.0.0.1', 'port' => 80),
         ),
        'domain' =>'bd.rong360.com',
        'secret' => '@rong363#bd',
        'verify_token' => true,
        'access' => 'intranet',
        //工具类
        ),
    );  

    /** API 内网 ip 限制 */
    public static $apiIntranetIp = array( 
      array('10.10.50.0','10.10.50.255'),
        array('10.10.60.0','10.10.60.255'),
        array('10.10.10.0','10.10.10.255'),
        array('10.10.30.0','10.10.30.255'),
        array('192.168.0.40','192.168.0.255'),
	array('10.0.2.1','10.0.2.255'),
	'10.0.2.105',
	'10.0.9.227',
    '10.0.8.204',
	'127.0.0.1',
	"10.0.8.221",
	"10.0.2.98",
	"10.0.129.30",
	"10.0.2.107",
	"10.0.8.23",
	"10.0.2.216",
	"10.0.2.101",
	"10.0.2.33", 
	"10.0.9.133",
	"10.0.129.57",
	"10.0.129.59",
	"10.0.129.147",
        "10.0.2.117",
	"10.0.2.39",
	"10.0.2.87",
	"10.0.129.26",
	"10.0.129.101",
	"10.0.9.121",
	"10.0.2.102",
	"10.0.2.139",
	"10.0.129.97",
	"10.0.129.228",
	"10.0.2.52",
	array('10.0.10.0','10.0.10.255'),
	array('10.0.9.0','10.0.9.255'),
	array('192.168.3.0','192.168.3.255'),
        array('119.57.151.1','119.57.151.255'),
	array('10.0.128.0','10.0.128.255'),
	array('10.0.2.106','10.0.2.255'), 
	array('10.0.8.84','10.0.8.255', '10.0.129.228'),
    );  

    public static $rabbitConfig = array(
        'talk' => array(
            'connect_timeout_sec'    => 1000,
            'read_timeout_ms'       => 4000,
            'write_timeout_ms'      => 1000,
        ),
        'machine' => array(
            array('host' => '10.0.2.200', 'port' => 5672),
           # array('host' => '10.0.2.103', 'port' => 5672),
        ),
        'auth' => array(
            'user'      => 'admin',
            'password'  => 'pass123',
            'vhost'     => 'core'
        ),
    );

}

