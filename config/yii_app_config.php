<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

//Yii::setPathOfAlias('mis', dirname(__FILE__) . '/../../../trunk/rong360/mis');
//Yii::setPathOfAlias('bd', dirname(__FILE__) . '/../../../trunk/rong360/bd');
//Yii::setPathOfAlias('batch', dirname(__FILE__) . '/../../../trunk/rong360/batch');
//Yii::setPathOfAlias('gendan', dirname(__FILE__) . '/../../../gendan');
Yii::setPathOfAlias('common', dirname(__FILE__) . '/../common');

return array(
    'basePath' => dirname(__FILE__) . '/..',
    'runtimePath' => dirname(__FILE__) . '/../../../logs/application/oa',
    'controllerPath' => dirname(__FILE__) . '/../controller',
    'viewPath' => dirname(__FILE__) . '/../view',
    'extensionPath' => dirname(__FILE__) . '/../common/shared',

	'name' => 'Rong360.COM',

	// preloading 'log' component
	'preload' => array('log'),

	// autoloading model and component classes
	'import' => array(
        'common.model.*',
        'common.service.*',
        'ext.phpconnectpool.*',
        'ext.db.*',
        // 'ext.encrypt.*',
        // 'ext.api.*',
        // 'ext.http.*',
        // 'ext.passport.*',
        'ext.phpmailer.*',
        // 'ext.tools.*',
        // 'ext.v_code.*',
        // 'ext.msg.*',
        // 'ext.400.*',
        'ext.exception.*',
        // 'ext.rongconfig.*',
        'ext.excel.*',
        'application.components.*',
        'application.model.*',
        'application.service.*',
	),

    'language' => 'zh_cn',
    'timeZone' => 'Asia/Shanghai',
    'charset' => 'utf-8',

	// application components
	'components' => array(
		'user' => array(
			// enable cookie-based authentication
			'allowAutoLogin' => true,
		),
        'request' => array(
            'enableCookieValidation' => true,
            'enableCsrfValidation' => false,
            'csrfTokenName' => 'RONG360_CSRF_TOKEN',
        ),
        'session' => array(
            'autoStart' => true,
            'timeout' => 3600,
        ),
        'urlManager' => array(
            'showScriptName' => false,
            'urlFormat' => 'path',
            'urlSuffix' => '.html',
            'caseSensitive' => false,
            'rules' => array(
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
		'errorHandler' => array(
			// use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
		                'class' => 'CFileLogRoute',
		                'levels' => 'info',
		        ),
		        array(
		                'class' => 'CFileLogRoute',
		                'levels' => 'error, warning',
		                'logFile' => 'application.log.wf',
		        ),
			),
		),
       'mailer' => array(
            'class' => 'ext.phpmailer.CPhpMailerSMTP',
            'host' => 'smtp.exmail.qq.com',
            'port' => 465,
            'from' => 'oa@i-yc.com',
            'fromName' => 'OA系统',
            'user' => 'oa@i-yc.com',
            'pass' => 'OADemo123$',
        ),
        'viewRenderer' => array(
            'class' => 'ext.smarty.CSmartyViewRenderer',
            'fileExtension' => '.tpl',
        ),
        'rongConfig' => array(
            'class' => 'ext.rongconfig.CRongConfig',
        ),    
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=> include(dirname(__FILE__) . '/oa.php'),
);
