<?php

DEFINE('STATIC_VERSION',    '201504081131');   //静态资源版本号

DEFINE('MALE',1);
DEFINE('FEMALE',0);
DEFINE('KAOQINERROR','oa_kaoqin_batch_error_');

DEFINE('WORKFLOW_XINGZHENG',10); //行政
DEFINE('WORKFLOW_RENLI',20);     //人力
DEFINE('WORKFLOW_CAIWU',30);     //财务
DEFINE('WORKFLOW_RD', 40);       //工程师
DEFINE('WORKFLOW_IT', 50);       //IT支持

DEFINE('OA_WORKFLOW_AUDIT_CHUANGJIAN', 1); //新创建工作流
DEFINE('OA_WORKFLOW_AUDIT_SHENPIZHONG', 2); //工作流审批中
DEFINE('OA_WORKFLOW_AUDIT_PASS', 3); //工作流审批通过
DEFINE('OA_WORKFLOW_AUDIT_REJECT', 4); //工作流审批拒绝
DEFINE('OA_WORKFLOW_AUDIT_CANCEL', 5); //取消审核
DEFINE('OA_WORKFLOW_AUDIT_ROLLBACK',6); //上线回滚
DEFINE('OA_WORKFLOW_AUDIT_CAOGAO', 99); //草稿

DEFINE('OA_TASK_CHUANGJIAN',0); //新创建任务
DEFINE('OA_TASK_TONGGUO',1);    //审批通过
DEFINE('OA_TASK_JUJUE',2); //审批拒绝
DEFINE('OA_TASK_YISHENPI',3); //已审批 包含 审批通过、审批拒绝


//工单问题类型
DEFINE('WORKORDER_TYPE_EMIAL',0);    //'邮件问题',
DEFINE('WORKORDER_TYPE_PWD',1);      //密码问题
DEFINE('WORKORDER_TYPE_RISK',2);     //'Risk问题',
DEFINE('WORKORDER_TYPE_KEFU',3);     //客服问题
DEFINE('WORKORDER_TYPE_TELL',4);     //电话问题
DEFINE('WORKORDER_TYPE_HARDWARE',5); //硬件问题'
DEFINE('WORKORDER_TYPE_SOFTWARE',6); //软件问题'
DEFINE('WORKORDER_TYPE_INTERNET',7); //网络问题'
DEFINE('WORKORDER_TYPE_OA',8);       //OA问题'
DEFINE('WORKORDER_TYPE_PRINT',9);    //打印机问题',
DEFINE('WORKORDER_TYPE_MD5',10);     //加密问题',
DEFINE('WORKORDER_TYPE_KAOQIN',11); //考勤问题',
DEFINE('WORKORDER_TYPE_MONITOR',12); //监控问题',
DEFINE('WORKORDER_TYPE_OTHER',13);   //其它问题',

//工单问题级别
DEFINE('WORKORDER_LEVEL_COM',0);//'普通'
DEFINE('WORKORDER_LEVEL_IMP',1);//重要',
DEFINE('WORKORDER_LEVEL_URG',2);//'紧急',

//工单状态
DEFINE('WORKORDER_NOT_ISSUE',100);  //NOT ISSUE
DEFINE('WORKORDER_ISSUEED',101);    //ISSUEED
DEFINE('WORKORDER_PAUSE',102);      //PAUSE
DEFINE('WORKORDER_CLOSE',103);      //CLOSE
DEFINE('WORKORDER_START',104);      //START
DEFINE('WORKORDER_NOT_START',105);  //NOT START

//VPN类型
DEFINE('CM_VPN',0);//普通VPN
DEFINE('RD_VPN',1);//研发VPN

DEFINE('STATUS_VALID',1); //有效
DEFINE('STATUS_INVALID',0);//无效

//票务相关, 1为订票, 2为退票, 3为改签
DEFINE('TICKET_BOOK',1);
DEFINE('TICKET_REFUND',2);
DEFINE('TICKET_ENDORSE',3);

DEFINE('ROOT_SECTION_ID',0);//定义根部门的id，
DEFINE('ROOT_SECTION_PARENT_ID',999999);//跟部门父部门id


//领导请假审批行政级别
DEFINE('AUDIT_CEO',100);
DEFINE('AUDIT_VP',90);
DEFINE('AUDIT_MANAGER',80); //部门负责人
DEFINE('AUDIT_VICE_MANAGER',70);//分部门负责人
DEFINE('AUDIT_GROUP',60); //组负责人
DEFINE('AUDIT_LITTLE_GROUP',50);//小组负责人



DEFINE('DEFAULT_PAGE_NUMBER',20);

DEFINE('HOLIDAY_SHANGWU',1); //上午
DEFINE('HOLIDAY_ZHONGWU',2); //中午
DEFINE('HOLIDAY_WANSHANG',3);//下午

DEFINE('BUSINESS_ADVANCE_TYPE_RENT', 1); //房租借款
DEFINE('BUSINESS_ADVANCE_TYPE_OTHERTTAXI', 2); //其他出租车费
DEFINE('BUSINESS_ADVANCE_TYPE_ENTERTAINMENT', 3); //招待费借款
DEFINE('BUSINESS_ADVANCE_TYPE_OTHERTRAVEL', 4); //其他交通费
DEFINE('BUSINESS_ADVANCE_TYPE_OTHEROTHER', 5); //其他其他
DEFINE('BUSINESS_ADVANCE_TYPE_OTHER', 6); //其他

DEFINE('BUSINESS_REIMBURSEMENT_TYPE_TRAFFIC', 1); //交通报销
DEFINE('BUSINESS_REIMBURSEMENT_TYPE_TAXI', 2); //出租车报销
DEFINE('BUSINESS_REIMBURSEMENT_TYPE_RENT', 3); //房租报销
DEFINE('BUSINESS_REIMBURSEMENT_TYPE_ENTERTAINMENT', 4); //招待费报销

DEFINE('REIMBURSEMENT_TYPE_TAXI', 1); //打车费
DEFINE('REIMBURSEMENT_TYPE_OTHERTRAFFIC', 2); //其他交通费
DEFINE('REIMBURSEMENT_TYPE_ENTERTAINMENT', 3); //招待费
DEFINE('REIMBURSEMENT_TYPE_OVERTIME', 4); //加班费
DEFINE('REIMBURSEMENT_TYPE_OTHER', 5); //其他费用

//角色权限操作日志类型
DEFINE('ROLE_ADD',1);
DEFINE('ROLE_EDIT',2);
DEFINE('ROLE_DEL',3);
DEFINE('ADMIN_ADD',1);
DEFINE('ADMIN_EDIT',2);
DEFINE('ADMIN_DEL',3);

//缺勤类型定义 
DEFINE('SYSTEM_QINGJIA',101); //缺勤类型 请假
DEFINE('SYSTEM_XIAOJIA',102); //缺勤类型 消假
DEFINE('SYSTEM_CHUCHA',103);  //出差
DEFINE('SYSTEM_XIAOCHUCHA',104);  //销出差
DEFINE('SYSTEM_YINGONGWAICHU',105);  //缺勤类型 因公外出
DEFINE('SYSTEM_QITA',999);    //员工没有维护，但是打卡异常的情况

//请假类型
DEFINE('VACATION_SHIJIA',3);
DEFINE('VACATION_BINGJIA',1);
DEFINE('VACATION_NIANJIA',2);
DEFINE('VACATION_BINGXIUJIA',12);
DEFINE('VACATION_CHUCHAI',13);
DEFINE('VACATION_YINGONGWAICHU',25);

//财务借款付款性质
DEFINE('FINANCE_LOAN_DAILY',0);
DEFINE('FINANCE_LOAN_ADMIN',1);
DEFINE('FINANCE_LOAN_HR',2);
DEFINE('FINANCE_LOAN_TICKET',3);

//考试异常时间
DEFINE('KAOQIN_CHECKIN',  "10:06:00");
DEFINE('KAOQIN_CHECKOUT', "18:00:00");


//是否离职 
DEFINE('USER_UN_LEAVE',  "1"); //在职
DEFINE('USER_LEAVE', "2");     //离职

//团建每人经费100
DEFINE('TEAMBUILDING_MONEY', 100); 

//操作日志任务类型
DEFINE('OP_TYPE_SECTION_EDIT',1);
DEFINE('OP_TYPE_SECTION_ADD',2);
DEFINE('OP_TYPE_SECTION_DEL',3);

class OAConfig
{
    public static $OaDataPath = '/home/rong/yunpan/oa/';

    public static $absenceType = array(
        VACATION_SHIJIA => '事假',
        VACATION_BINGJIA => '病假',   
        VACATION_NIANJIA => '年假',
        '4' => '婚假',   
        '5' => '丧假',
        '6' => '调休',
        '7' => '产假',
        '8' => '陪产假',
        '9' => '产检假',
        '10' => '哺乳假',
        '11' => '其他',
        // '12' => '病休假' //病休假站位
        '13' => '出差'   //出差站位
    );

    //日常报销类型
    public static $daily_reimbursementType = array(
        REIMBURSEMENT_TYPE_TAXI  => '出租车费',    
        REIMBURSEMENT_TYPE_OTHERTRAFFIC  => '加班交通费',    
        REIMBURSEMENT_TYPE_ENTERTAINMENT  => '招待费',    
        REIMBURSEMENT_TYPE_OVERTIME  => '加班餐费',    
        REIMBURSEMENT_TYPE_OTHER  => '其他费用',    
    );

    //出差预借款类型
    public static $business_advanceType = array(
        BUSINESS_ADVANCE_TYPE_RENT  => '房租',    
        BUSINESS_ADVANCE_TYPE_ENTERTAINMENT  => '宴请费',    
        BUSINESS_ADVANCE_TYPE_OTHERTTAXI  => '出租车费',    
        BUSINESS_ADVANCE_TYPE_OTHERTRAVEL  => '交通费',    
        BUSINESS_ADVANCE_TYPE_OTHEROTHER  => '其他费用',    
        BUSINESS_ADVANCE_TYPE_OTHER  => '其他费用',    
    );

    //出差报销类型
    public static $business_reimbursementType = array(
        BUSINESS_REIMBURSEMENT_TYPE_RENT  => '房租',    
        BUSINESS_REIMBURSEMENT_TYPE_ENTERTAINMENT  => '宴请费',    
        BUSINESS_REIMBURSEMENT_TYPE_TAXI  => '出租车费',    
        BUSINESS_REIMBURSEMENT_TYPE_TRAFFIC  => '交通费',      
    );

    //财务借款性质
    public static $pay_types = array(
        FINANCE_LOAN_DAILY => '部门日常',
        FINANCE_LOAN_ADMIN => '行政类',
        FINANCE_LOAN_HR => '人事类',
        FINANCE_LOAN_TICKET => '票务'
    );

    //财务付款性质
    public static $payment_natures = array(
        1 => '一次性付款',
        2 => '首付',
        3 => '进度款',
        4 => '尾款'
    );

	//工单问题类型
	public static $workorderType = array(
        WORKORDER_TYPE_EMIAL     => '邮件问题',
        WORKORDER_TYPE_PWD       => '密码问题',
        WORKORDER_TYPE_RISK      => 'Risk问题',
        WORKORDER_TYPE_KEFU      => '客服问题',
        WORKORDER_TYPE_TELL      => '电话问题',
        WORKORDER_TYPE_HARDWARE  => '硬件问题',
        WORKORDER_TYPE_SOFTWARE  => '软件问题',
        WORKORDER_TYPE_INTERNET  => '网络问题',
        WORKORDER_TYPE_OA        => 'OA问题',
        WORKORDER_TYPE_PRINT     => '打印机问题',
        WORKORDER_TYPE_MD5       => '加密问题',
        WORKORDER_TYPE_KAOQIN   => '考勤问题',
        WORKORDER_TYPE_MONITOR   => '监控问题',
        WORKORDER_TYPE_OTHER     => '其它问题',

	);

    //工单问题级别
    public static $workorderLevel = array(
	   WORKORDER_LEVEL_COM =>'普通',
	   WORKORDER_LEVEL_IMP =>'重要',
	   WORKORDER_LEVEL_URG =>'紧急'
	);
    //工单处理人
	public static $workorderSolve = array(
	   ZHUANGLINAN  => '张李楠',
	   ZHOUHONGWEI  => '周宏伟',
           YANGXUHUI    => '杨旭辉',
           MEIMINGJING  => '梅明静',
	);

	public static $solver = array(
	   'ZHUANGLINAN' => 140,
	   'ZHOUHONGWEI' => 265,
	   'YANGXUHUI'   => 857,
           'MEIMINGJING' => 1512,
	);

	public static $vpnType = array(
       CM_VPN   => '普通VPN',
	   RD_VPN   => '研发VPN',
	);

    //user_workflow目前审核状态
    public static $audit_status = array(
        OA_WORKFLOW_AUDIT_CAOGAO => '草稿',
        OA_WORKFLOW_AUDIT_SHENPIZHONG => '审批中',
        OA_WORKFLOW_AUDIT_PASS  => '审批通过',
        OA_WORKFLOW_AUDIT_REJECT => '审批拒绝',
        OA_WORKFLOW_AUDIT_CANCEL => '取消审核',
        OA_WORKFLOW_AUDIT_ROLLBACK => '回滚',
        WORKORDER_NOT_ISSUE      => '未分配',
        WORKORDER_ISSUEED        => '进行中',
	WORKORDER_CLOSE          => '已处理'
    );

    //user_task订单的前端过滤状态
    public static $user_task_audit_status = array(
        OA_TASK_CHUANGJIAN => "待审批",
        OA_TASK_YISHENPI   => "已审批"
    );

    //workorder状态
	public static $workorder_status = array(
	   WORKORDER_NOT_ISSUE => '未分配',  
	   WORKORDER_ISSUEED   => '已分配',  
	   WORKORDER_START     => '开始处理',  
	   WORKORDER_NOT_START => '未处理',  
	   WORKORDER_PAUSE     => '暂停',  
	   WORKORDER_CLOSE     => '关闭'
	);

    //工作流类别
    public static $workflow_category = array(
        WORKFLOW_XINGZHENG => '行政',
        WORKFLOW_RENLI     => '人力',
        WORKFLOW_CAIWU     => '财务',
        WORKFLOW_RD        => '研发',
        WORKFLOW_IT        =>'IT支持及权限申请'
    );

    //流程类型
    public static $workflow_type = array(
        'absence'             => '请假',   
        'daily_reimbursement' => '日常报销',
        'cancel_absence'      => '销假',
        'cancel_business'      => '销出差',
        'business_card'       => '申请名片',
        'team_building'       => '团建报销',
        'team_encourage'      => '部门激励报销',
        'online_order'        => '上线单',
        'workorder'           => '工单系统',
        'papp_vpn'            => 'VPN权限申请',
        'papp_db'             => '数据库权限申请',
        'entry'             => '员工入职',
        'dimission'             => '员工离职',
        'regular'             => '员工转正',
        'transfer'             => '员工异动',
        'administrative_expense' => '行政费用付款申请',
        'office_supply'       => '办公用品领用',   
        'ticket_book'         => '票务',
        'administrative_apply'=> '物品申请',
        'business_advance'=>'出差预借款',
        'business_reimbursement'=>'出差报销',
        'finance_loan'=>'借款申请',
        'finance_offset'=>'借款冲抵',
        'finance_payment'=>'付款审批'
    );

    //将时间转换为时间段(上午，中午，下午)
    public static $time_node = array(
        '09:00:00' => '上午',
        '12:00:00' => '中午',                
        '18:00:00' => '下午'
    );

    //将上午、中午、下午转换成时间
    public static $nodeTotime = array(
        HOLIDAY_SHANGWU => '09:00:00',
        HOLIDAY_ZHONGWU => '12:00:00',
        HOLIDAY_WANSHANG => '18:00:00'        
    );

    //params day 日期，node "HOLIDAY_SHANGWU HOLIDAY_ZHONGWU HOLIDAY_WANSHANG"
    public static function nodeToTimeStamp($day,$node)
    {
        if(empty($day) || empty($node)){
            return 0;
        } 
        $time = self::$nodeTotime[$node];

        return strtotime($day.$time);
    }

    //缺勤类型 员工自己描述
    public static $absenteeismType = array(
        3 => '未带卡',
        4 => '忘打卡',
        5 => '外出培训',
        //6 => '卡丢失',
        7 => '其他分部办公',
        8 => '在家办公',
        10 => '迟到',
        11 => '早退',
        13 => '加班至次日凌晨',
        14 => '部门排班',
        15 => '哺乳假',
        16 => '入职当天无工卡',
        17 => '体检',
    );

    //缺勤类型 系统描述 系统根据打卡记录和请假记录 自己打标记
    public static $systemAbsenteeismType = array(
        SYSTEM_QINGJIA => '请假', 
        SYSTEM_XIAOJIA => '销假',
        SYSTEM_CHUCHA  => '出差',
        SYSTEM_XIAOCHUCHA  => '销出差',
        SYSTEM_YINGONGWAICHU  => '因公外出',

    );

    public static $op_types = array(
        OP_TYPE_SECTION_EDIT => '编辑部门',
        OP_TYPE_SECTION_ADD => '添加部门',
        OP_TYPE_SECTION_DEL => '删除部门',
    );

    public static function absenteeismTypeAll()
    {
        return self::$absenteeismType + self::$systemAbsenteeismType;
    }

    public static function officeTelephone()
    {
        return array(
            'bj1' => '(北京)010-82625755',   
            'cn1' => '(昌宁)010-57490812',
            'hn1' => '(华南)0755-83231515',
            'hd1' => '(华东)021-61703163', 
            'hd2' => '(华东)021-61703178', 
        );
    }

    public static $function_type = array(
        1 => '功能升级',
        2 => '新功能',
        3 => '修订BUG',
        4 => '改版', 
        5 => '优化',
    );

    public static $qa_type = array(
        1 => '核心功能回归',
        2 => '全部功能回归'  
    );

    public static $business_role = array(
        'rd'                  => '开发工程师',
        'pm'                  => '产品',
        'yunying'             => '运营',
        'qa'                  => '测试工程师',
        'ue'                  => 'UE负责人',
        'rd_manger'           => '技术部总监',
        'op'                  => '操作上线op',
        'interviewer'         => '面试官',
        'hr'                  => '面试hr',
        'directManager'       => '直接主管',
        'subSectionManager'   => '分部主管',
        'sectionManager'      => '部门主管',
        'businessLineManager' => '业务线主管',
        'HRD'                 => '人力主管',
        'VP'                  => 'VP',
        'CEO'                 => 'CEO',
        'self'                 => '员工个人',
    );

    //团建类型
    public static $activity_type = array(
        1 => '部门内活动',
        2 => '部门间联谊',
        3 => '培训和学习',
        10 => '其他' 
    );

    public static $hireTypes = array(
        1=>'正式员工',
        2=>'校招实习',
        3=>'实习生',
        4=>'劳务',
        5=>'顾问'
    );

    public static $section_team = array(

            "产品研发",
            "风控",
            "市场&广告",
            "运营",
            "职能",
            "Estaff",

        );

    public static $levelNumToName = array(
        0 => '无',
        50 => '小组负责人',
        60 => '组负责人',
        70 => '分部门负责人',
        80 => '部门负责人',
        90 => 'VP',
        100 => 'CEO',
    );

    public static $lunchLocation = array(
        '互联网金融中心',
        '时代网络大厦',
        '昌宁大厦',
    );

    public static $lunchChangeType = array(
        '增加订餐',
        '取消订餐',
    );

    public static $officeLocation = array(
        '互联网金融中心',
        '时代网络大厦',
        '昌宁大厦',
//        '上海',
//        '深圳',
//        '其他',
    );

    public static $officeSupply = array(
        '笔类' => array(
            '按动签字笔',
            '按动签字笔芯',
            '自动铅笔',
            '自动铅笔芯',
            '桌笔',
            '白板笔',
        ),
        '本类' => array(
            'B5笔记本',
        ),
        '粘胶类' => array(
            '胶水',
            '小胶带',
            '宽胶带',
            '双面胶',
            '胶条座',
        ),
        '电池' => array(
            '5号电池',
            '7号电池',
        ),
        '接线板' => array(
            '6位接线板',
            '8位接线板',
        ),
        '文件夹类' => array(
            '3寸档案盒',
            '3格书架',
            'A4单页夹',
            'A4按扣带',
            '11孔白条带',
            '80页文件夹',
            '40页文件夹',
            'A4单夹',
            'A4抽杆夹',
            'A4牛皮纸档案袋',
            'A4快劳夹',
            '牛皮纸信封',
        ),
        '贴纸类' => array(
            '便签纸',
            '书写标签',
            '5色塑料贴',
            '不干胶标签纸',
        ),
        '长尾夹' => array(
            '50mm',
            '41mm',
            '32mm',
            '25mm',
            '19mm',
            '15mm',
        ),
        '其他' => array(
            '网线',
            '计算器',
            '剪刀',
            '订书针',
            '订书机',
            '回形针',
            '回形针筒',
            '橡皮',
            '修正带',
            '笔筒',
        ),
    );

    public static function checkUserAccount($name) {

        $name = trim($name);
        $name = str_replace(array('@rong360.com'), "", $name);

        return $name;
    }
}






