<?php
class ExcelReader
{
    private $_file;

    public function __construct($file)
    {
        //加入环境配置，导入phpexcel类
        $path = Config::$basePath."/trunk/rong360/shared/excel/";
        set_include_path(get_include_path() . PATH_SEPARATOR . $path);
        include 'PHPExcel/IOFactory.php';
        $this->_file = $file;
    }

    public function GetDataList()
    {
        //加载excel文件
        if(!file_exists($this->_file))
        {
            print "导入出错。";
            exit;
        }
        $objPHPExcel = PHPExcel_IOFactory::load($this->_file);
        //读取excel文件
        $dataList = array();
        $dataList = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        return $dataList;
    }
}
?>
