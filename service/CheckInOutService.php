<?php
class CheckInOutService
{
    public static function actionImportkaoqin($uid,$id_card)
    {
        if(empty($id_card) || empty($uid)){
            return false;
        }
        $conn = mssql_connect('kaoqinServer','kaoqin','kaoqin123');
        if(!$conn){
            echo '连接考勤数据库失败';
        }
        $dbs = array('CN','JRZX','SH','SZ');
        $today = date('Y-m-d');
        $model = new CheckInOut();
        foreach($dbs as $db){
            mssql_select_db($db);

            $ret = mssql_query("SELECT max(userid) as userid,ssn FROM userinfo WHERE ssn='".$id_card."' group by ssn");

            while($item = mssql_fetch_assoc($ret)){
                $userid = $item['userid'];//考勤记录中的id

                //获取考勤记录
                $sql = sprintf("select checktime,sensorid from checkinout where userid=%d order by checktime asc ",$userid);
                $rows = mssql_query($sql);
                $data = array();
                while($row = mssql_fetch_assoc($rows)){
                    $time = strtotime($row['checktime']);
                    $key = date('Y-m-d',$time);
                    if($key == $today){
                        continue;
                    }
                    if(isset($data[$key])){
                        if($time < $data[$key]['checkin']){
                            $data[$key]['checkin'] = $time;
                        }elseif($time > $data[$key]['checkout']){
                            $data[$key]['checkout'] = $time;
                        } 
                    }else{
                        $data[$key]['checkin'] = $data[$key]['checkout'] = $time;
                    }
                }
                if(!empty($data)){
                    foreach($data as $day){
                        $day['user_id'] = $uid;
                        $day['date'] = date('Y-m-d',$day['checkin']);
                        if($day['checkin'] == $day['checkout']){
                            if(date('H',$day['checkin']) > 12){
                                $day['checkout_time'] = date('Y-m-d H:i:s',$day['checkout']);
                            }else{
                                $day['checkin_time'] = date('Y-m-d H:i:s',$day['checkin']);
                            }
                        }else{
                            $day['checkin_time'] = date('Y-m-d H:i:s',$day['checkin']);
                            $day['checkout_time'] = date('Y-m-d H:i:s',$day['checkout']);
                        }
                        unset($day['checkin']);
                        unset($day['checkout']);
                        $result = $model->save($day);
                        if(!$result){
                            Yii::log('checkinout insert fail:'.print_r($day,true),'info');
                        }
                    }
                }
            }
        }
    }

    //整理考勤,将数据整理成以user_id、date为key的二维数组
    public function finishingCheckinoutToArray($list)
    {
        $ret = array();
        if(empty($list)){
            return $ret;
        } 

        foreach($list as $item){
            //删除系统维护的“销假”"请假"状态
            if($item['absenteeism_type'] == SYSTEM_QINGJIA || $item['absenteeism_type'] == SYSTEM_XIAOJIA){
                $item['absenteeism_type'] = 0; 
            }
            //将没有打卡记录、并且没有维护状态的记录删除掉(系统维护的‘请假’、‘销假’不算维护状态)
            if($item['checkin_time'] == '0000-00-00 00:00:00' && $item['checkout_time'] == '0000-00-00 00:00:00' && $item['absenteeism_type'] == 0){
                continue;
            }
            
            //检查员工的考勤是否存在异常
            if($item['absenteeism_type'] == 0){
                if($item['checkin_time'] == '0000-00-00 00:00:00' 
                    || $item['checkout_time'] == '0000-00-00 00:00:00' 
                    || strtotime($item['checkin_time'])>strtotime(date('Y-m-d',strtotime($item['checkin_time']))." 10:30:00") 
                    || strtotime($item['checkout_time'])<strtotime(date('Y-m-d',strtotime($item['checkout_time']))." 17:30:00")){
                        $item['absenteeism_type'] = SYSTEM_QITA;   
                    }
            }
            //上班时间再早6点至早8点之间
            if(strtotime($item['checkin_time'])>=strtotime(date('Y-m-d',strtotime($item['checkin_time']))." 06:00:00") && strtotime($item['checkin_time'])<=strtotime(date('Y-m-d',strtotime($item['checkin_time']))." 08:00:00")){
                $item['zaocan'] = 1;
            }
            $item['day_num'] = 1;
            $date = date('Y-m-d',strtotime($item['date']));
            $ret[$item['user_id']][$date] = $item; 
        }
        return $ret;
    }

    public function statisticsCheckInOut($checkInOuts,$workDay)
    {
        $ret = array();
        if(!is_array($checkInOuts)){
            return $ret;
        }
        foreach($checkInOuts as $userId => $items){
            foreach($items as $date => $item){
                if($workDay[$date]){//工作日
                    $ret[$userId]['zaocan'] += 0 + $item['zaocan']; //早餐
                    if($item['absenteeism_type'] > 0){
                        $ret[$userId][$item['absenteeism_type']] += $item['day_num'];
                    }else{
                        $ret[$userId]['gongzuori'] += $item['day_num'];
                    } 
                }else{//非工作日
                    if($item['absenteeism_type'] == 14){ //部门排班在节假日也要计算
                        $ret[$userId][$item['absenteeism_type']] += $item['day_num'];
                    }else{
                        $ret[$userId]['jiejiari'] += $item['day_num'];
                    }
                }
            } 
        }
        return $ret;
    }
    
}

?>
