<?php 
class UserTaskService
{
    //根据状态获取待审批、已审批的流程数据
    //userId 用户
    //audit_status 流程状态
    //keyword 申请人姓名或者工号
    //obj_type 流程类型 可为空
    //start 开始位置
    //rn 获取数量
    public function search($userId,$audit_status,$keyword,$obj_type='',$start,$rn)
    {
        $roleModel = new Role();
        $model = new UserTask();

        if(!empty($keyword)){
            $userModel = new User();
            $applicantIds = $userModel->getUserIdsByCardOrName($keyword);
        }

        if($audit_status == OA_TASK_YISHENPI){ //对于已审批的，允许用户看到自己下属及所属角色的审批记录
            $userIds = array();
            $sectionModel = new Section();
            $users = $sectionModel->getReportPersonForUser($userId);
            foreach($users as $user){
                $userIds[] = $user['id'];
            }
            $roleIds = $roleModel->getRoleIdByUserId($userId);
            $arr= array(3);
            $roleIds = array_intersect($roleIds,$arr);
            // $roleIds = array();
            $list = $model->getCompleteItems($userId,$userIds,$roleIds,$applicantIds,$obj_type,$start,$rn);
            $userTaskCnt = $model->getCompleteItemCnt($userId,$userIds,$roleIds,$applicantIds,$obj_type);
        }else{ //用户需要看到需要自己和自己所属角色的所有审批
            $roleIds = $roleModel->getRoleIdByUserId($userId);
            $list = $model->getTodoItemsByUserId($userId,$roleIds,$applicantIds,$obj_type,$start,$rn);
            $userTaskCnt = $model->getTodoItemCntByUserId($userId,$roleIds,$applicantIds,$obj_type);
        }
        $table_id = array();
        if(is_array($list)){
            foreach($list as $item){
                $table_id[$item['obj_type']][] = $item['obj_id'];
            }
        }

        $userworkflowModel = new UserWorkFlow();
        $extList = $userworkflowModel->getExtListByUserWorkflow($table_id);

        if(is_array($list)){
            foreach($list as &$item){
                $item['obj_id'] = $extList[$item['obj_type']][$item['obj_id']];
                $objRole = $roleModel->getItemByPk($item['role_id']);
                $item['role_name'] = $objRole['name'];
            }
        }

        $this->createShowTitle($list);
        $ret['list'] = $list;
        $ret['userTaskCnt'] = $userTaskCnt;
        return $ret;
    } 


    public function createShowTitle(&$list)
    {
        if(empty($list)){
            return;
        } 
        $absenceType = OAConfig::$absenceType;
        $workflow_type = OAConfig::$workflow_type;
        $workorder_type = OAConfig::$workorderType;

        foreach($list as &$item){
            if($item['obj_type'] == 'absence' || $item['obj_type'] == 'cancel_absence'){

                $strType = $workflow_type[$item['obj_type']];

                if($item['obj_id']['type'] == VACATION_CHUCHAI) {

                    $strType = "出差";

                }
                if($item['obj_id']['type'] == VACATION_YINGONGWAICHU) {

                    $strType = "因公外出";

                }
                $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'.date('Y-m-d',$item['obj_id']['start_time'])."至".date('Y-m-d',$item['obj_id']['end_time']).$strType.$absenceType[$item['obj_id']['type']]."共".round($item['obj_id']['day_number'],1)."天";

            }elseif($item['obj_type'] == 'business_advance'){
                $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'.$workflow_type[$item['obj_type']].$item['obj_id']['total_amount']."元";
            }elseif($item['obj_type'] == 'daily_reimbursement'){
                $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'.$workflow_type[$item['obj_type']].$item['obj_id']['total_amount']."元"; 
            }elseif($item['obj_type'] == 'business_reimbursement'){
                 $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'.$workflow_type[$item['obj_type']].$item['obj_id']['total_amount']."元";
            }elseif($item['obj_type'] == 'daily_reimbursement'){
            }elseif($item['obj_type'] == 'online_order'){
                $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'.$item['obj_id']['title'].$workflow_type[$item['obj_type']];
            }elseif($item['obj_type'] == 'team_building'){
                $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'.$workflow_type[$item['obj_type']].$item['obj_id']['amount']."元";
            }elseif($item['obj_type'] == 'team_encourage'){
                $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'.$workflow_type[$item['obj_type']].$item['obj_id']['amount']."元";
            }elseif($item['obj_type'] == 'ticket_book'){
                if($item['obj_id']['status'] == TICKET_BOOK){
                    $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'. "订票";
                    if($item['obj_id']['hurry_reason']){
                        $item['show_title'] = $item['show_title']."（". $item['obj_id']['hurry_reason'] . "）";
                    }else{
                        $item['show_title'] = $item['show_title']."（非紧急出行）";
                    }
                }elseif($item['obj_id']['status'] == TICKET_REFUND){
                    $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'. "退票（" . $item['obj_id']['trip_reason'] . "）";
                }elseif($item['obj_id']['status'] == TICKET_ENDORSE){
                    $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'. "改签（" . $item['obj_id']['trip_reason'] . "）";
                }
            }elseif($item['obj_type'] == 'office_supply'){
                $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'. "办公用品领用";
            }elseif($item['obj_type'] == 'lunch_change'){
                $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'. "午餐变更";
            }elseif($item['obj_type'] == 'workorder'){
				if($item['uwf_audit_status'] == WORKORDER_START){
                    $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'.$workflow_type[$item['obj_type']]."~".$workorder_type[$item['obj_id']['type']]."(开始处理中)";
				}elseif($item['uwf_audit_status'] == WORKORDER_PAUSE){
                    $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'.$workflow_type[$item['obj_type']]."~".$workorder_type[$item['obj_id']['type']]."(暂停中)";
				}else{
                    $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'.$workflow_type[$item['obj_type']]."~".$workorder_type[$item['obj_id']['type']];
				}
            }elseif ($item['obj_type'] == 'administrative_expense') {
                $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'.$item['obj_id']['summary'].$workflow_type[$item['obj_type']];
            }elseif ($item['obj_type'] == 'finance_loan' || $item['obj_type'] == 'finance_offset' || $item['obj_type'] == 'finance_payment') {
                $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'.$workflow_type[$item['obj_type']].$item['obj_id']['amount']."元";
            }
            else{
                $item['show_title'] = $item['obj_id']['user_name'].'（'.$item['obj_id']['id_card'].'）'.$workflow_type[$item['obj_type']];
            }
        }
    }
}

?>
