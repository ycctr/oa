<?php
/**
 * 团建服务
 * 
 **/

class MyTeamService {

    public $sectionModel;
    public $teamModel;
    public $teamSeniosModel;
    public $checkRepeat = array();
    public $checkDirect = array();  //直属校验


    public function __construct() {

        $this->sectionModel    = new Section();

    }


    public function getSectionList($userId) {
        
        $teamArr = $this->sectionModel->getAllSections($userId); 
        foreach($teamArr as $val) {

            $this->checkDirect[] = $val['id'];

        }

        if(empty($teamArr)) {

            return false;

        }

        $html = "";

        foreach($teamArr as $team) {

            $id           = $team['id'];

            if(in_array($id, $this->checkRepeat) ) {

                continue;

            }

            $this->checkRepeat[] = $id;


            $tmpName = "&nbsp;&nbsp;";
            if(in_array($id, $this->checkDirect) ) {

               $tmpName = "* ";

            }

            $name         = $tmpName.$team['name'];
            $html .= "<li><a onclick='superfish_tree(this)' section_id='{$id}'>{$name}</a>"; 

            $tmp_html = true;
            $tmp_html = $this->getParentId($id);
            if($tmp_html){

                $html .= "<ul>{$tmp_html}</ul>"; 

            }
            
            $html .= "</li>"; 

        }

        return $html;

    }

    //根据部门secionID,获取所有子部门
    public function getParentId($parentId) {

        $teamArr = $this->sectionModel->getParentId($parentId);

        if(empty($teamArr) ) {

            return false;

        }

        $html = "";
        $teamData = array();
        foreach($teamArr as $team) {

            $id               = $team['id'];

            if(in_array($id, $this->checkRepeat) ) {

                continue;

            }
            $this->checkRepeat[] = $id;

            $tmpName = "&nbsp;&nbsp;";
            if(in_array($id, $this->checkDirect) ) {

               $tmpName = "* ";

            }

            $name         = $tmpName.$team['name'];
            $html .= "<li><a onclick='superfish_tree(this)' section_id='{$id}'>{$name}</a>"; 
            $tmp_html = true;
            $tmp_html = $this->getParentId($id);
            if($tmp_html){

                $html .= "<ul>{$tmp_html}</ul>"; 

            }

            $html .= "</li>"; 

        }

        return $html;


    }

    public function makeHtml($sectionList, &$html) {

        // var_dump($sectionList);
        // die();
        //
        //
        // foreach($sectionList as $sectionId=>$section)  {
        //
        //     if(isset($section['name']) ){
        //         $name = $section['name'];
        //         $html .= "<li><a href='#'>{$name}</a>"; 
        //     }
        //
        //    if(!empty($section['sub'])) {
        //
        //        $html .= "<ul>";
        //        $html .= $this->makeHtml($section); 
        //        $html .= "</ul>";
        //
        //    }
        //
        //    $html .= "</li>";
        //
        // }



    }

    public function getSectionObj($sectionId) {

        $section = $this->sectionModel->getItemByPk($sectionId);
        $sectionId = $section['id'];
        $sectionName = $section['name'];
        $sectionInfo[$sectionId] = $sectionName;

        return $sectionInfo;

    }

    public function getSubSectionId($section_id, $userId) {

        $teamEncouageModel = new TeamEncourageService();
        $data = null;

        if($section_id == 0) {

            $data = $teamEncouageModel->getSectionEncourage($userId);

        } else {

            //获取子部门ID
            $teamEncouageModel->getParentId($section_id, $data);
        
        }


        return $data;

    }

    /**
     *  搜索主要逻辑
     **/
    public function searchMyTeam($condition, $limitStr) {

        $whereSql = "";

        if(!empty($condition['section_id']) ) {
            $section_id = $condition['section_id'];
            $whereSql .= " AND u.section_id in ({$section_id}) ";

        }

        if(!empty($condition['tagname']) ) {
            $tagname = $condition['tagname'];
            $whereSql .= " AND (u.name = '{$tagname}' OR u.email = '{$tagname}' ) ";
            $limitStr = "";

        }

        $userModel = new User();

        $res = array();
        $res['total'] = $userModel->getBySqlUserTotal($whereSql);
        $res['data'] = $userModel->getBySqlUser($whereSql, $limitStr);

        return $res;

    }

}

