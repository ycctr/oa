<?php
class WorkflowConfig
{
    /*
     *工作流节点定义，键值为workflow的obj_type，内容为审批节点步骤
     *
     */
    private static $config = array(
        'absence' => array( 
            'A' => array(//依据请假天数确定使用哪个流程(一个节点有多个规则) 
                array(
                    'maxday' => 1,
                    'type' => 'manage',
                    'level'  => AUDIT_VICE_MANAGER, 
                    'minlevel' => 0, //如果发起人达到这个级别，就不需要上级审批了,直接进入下一个节点
                    'maxlevel' => AUDIT_CEO //审批人最大不超过这个级别
                ),
                array( //左开右闭
                    'minday' => 1,
                    'maxday' => 5,
                    'type' => 'manage',
                    'level'  => AUDIT_MANAGER,
                    'minlevel' => 0,
                    'maxlevel' => AUDIT_CEO
                ),
                array(
                    'minday' => 5,
                    'type' => 'manage',
                    'level'  => AUDIT_VP,
                    'minlevel' => 0,
                    'maxlevel' => AUDIT_CEO 
                ),
            ),
        ),   
        'cancel_absence' => array( 
            'A' => array(//依据请假天数确定使用哪个流程(一个节点有多个规则)
                array(
                    'maxday' => 1,
                    'type' => 'manage',
                    'level'  => AUDIT_VICE_MANAGER, 
                    'minlevel' => 0, //至少审批要到这个级别,如果没到审批步骤可以延长超过num定义的数字 
                    'maxlevel' => AUDIT_CEO //审批人最大不超过这个级别
                ),
                array(
                    'minday' => 1,
                    'maxday' => 5,
                    'type' => 'manage',
                    'level'  => AUDIT_MANAGER,
                    'minlevel' => 0,
                    'maxlevel' => AUDIT_CEO
                ),
                array(
                    'minday' => 5,
                    'type' => 'manage',
                    'level'  => AUDIT_VP,
                    'minlevel' => 0,
                    'maxlevel' => AUDIT_CEO 
                ),
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '人力',     
            )
        ),   

        'cancel_business' => array( 
            'A' => array(//依据请假天数确定使用哪个流程(一个节点有多个规则)
                array(
                    'maxday' => 1,
                    'type' => 'manage',
                    'level'  => AUDIT_VICE_MANAGER, 
                    'minlevel' => 0, //至少审批要到这个级别,如果没到审批步骤可以延长超过num定义的数字 
                    'maxlevel' => AUDIT_CEO //审批人最大不超过这个级别
                ),
                array(
                    'minday' => 1,
                    'maxday' => 5,
                    'type' => 'manage',
                    'level'  => AUDIT_MANAGER,
                    'minlevel' => 0,
                    'maxlevel' => AUDIT_CEO
                ),
                array(
                    'minday' => 5,
                    'type' => 'manage',
                    'level'  => AUDIT_VP,
                    'minlevel' => 0,
                    'maxlevel' => AUDIT_CEO 
                ),
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '人力',     
            )
        ),   

        'daily_reimbursement' => array(
            'A' =>  array(
                'type' => 'manage',
                'level'  => AUDIT_VP,
                'minlevel' => 0,
                'maxlevel' => AUDIT_CEO
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '财务',     
            )
        ),

        'business_advance' => array(
            'A' =>  array(
                'type' => 'manage',
                'level'  => AUDIT_VP,
                'minlevel' => 0,
                'maxlevel' => AUDIT_CEO
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '财务',     
            )
        ),

        'business_reimbursement' => array(
            'A' =>  array(
                'type' => 'manage',
                'level'  => AUDIT_VP,
                'minlevel' => 0,
                'maxlevel' => AUDIT_CEO
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '财务',     
            )
        ),

        'business_card' => array(
            'A' =>  array(
                'type' => 'manage',
                'level'  => AUDIT_MANAGER,
                'minlevel' => AUDIT_CEO,
                'maxlevel' => AUDIT_CEO
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '人力名片审核专员',     
            ),
            'C' => array(
                'type' => 'role',
                'rolename' => '人力主管'    
            ),
            'D' => array(
                'type' => 'role',
                'rolename' => '行政',     
            ),
        ),
        'workorder' => array(
            'A' => array(
                'type' => 'role',
                'rolename' => 'IT支持',
            ),
            'B' => array(
                'type' => 'custom'
            ),
        ),
        'papp_vpn' => array(
            'A' =>  array(
                'type' => 'manage',
                'level'  => AUDIT_MANAGER,
                'minlevel' => AUDIT_CEO,
                'maxlevel' => AUDIT_CEO
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => 'IT支持',
            ),
        ),

        'papp_db' => array(
            'A' =>  array(
                'type' => 'manage',
                'level'  => AUDIT_VP,
                'minlevel' => 0,
                'maxlevel' => AUDIT_CEO
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '数据库权限审核专员',
            ),
        ),


        'online_order' => array(
            'A' => array(
                'type' => 'custom',
            ),   
            'B' => array(
                'type' => 'custom',
            ),   
            'C' => array(
                'type' => 'custom',
            ),   
        ),
        'team_building' => array(//只有有团建权限的部门领导能发起
            'A' =>  array(
                'type' => 'manage',
                'level'  => AUDIT_VP,
                'minlevel' => AUDIT_VP,
                'maxlevel' => AUDIT_CEO 
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '团建审核专员',     
            ),
            'C' => array(
                'type' => 'role',
                'rolename' => '人力主管'    
            ),
            'D' => array(
                'type' => 'role',
                'rolename' => '财务', 
            )
        ),
        'team_encourage' => array(  //激励经费
            'A' =>  array(
                'type' => 'manage',
                'level'  => AUDIT_VP,
                'minlevel' => 0,
                'maxlevel' => AUDIT_CEO 
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '团建审核专员', 
            ),
            'C' => array(
                'type' => 'role',
                'rolename' => '人力主管', 
            ),
            'D' => array(
                'type' => 'role',
                'rolename' => '财务', 
            ),           
        ),
        'entry' => array(
            'A' => array(
                'type' => 'custom',
            ),
        ),
        'dimission' => array(
            'A' =>  array(
                'type' => 'manage',
                'level'  => AUDIT_VP,
                'minlevel' => 0,
                'maxlevel' => AUDIT_CEO
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '人力',
            ),
            'C' =>  array(
                'type' => 'manage',
                'level'  => AUDIT_VICE_MANAGER,
                'minlevel' => 0,
                'maxlevel' => AUDIT_CEO
            ),
            'D' => array(
                'type' => 'role',
                'rolename' => '离职系统账号和特殊网络专员',
            ),
            'E' => array(
                'type' => 'role',
                'rolename' => '离职RISK系统账号专员',
            ),
            'F' => array(
                'type' => 'role',
                'rolename' => '离职邮箱和VPN账号专员',
            ),
            'G' => array(
                'type' => 'role',
                'rolename' => '离职财务专员',
            ),
            'H' => array(
                'type' => 'role',
                'rolename' => '离职行政专员',
            ),
            'I' => array(
                'type' => 'role',
                'rolename' => '人力',
            ),
            'J' => array(
                'type' => 'role',
                'rolename' => '人力主管',
            ),
            'K' => array(
                'type' => 'custom',
            ),
        ),
        'ticket_book' => array(
            'A' =>  array(
                'type' => 'manage',
                'level'  => AUDIT_VICE_MANAGER,
                'minlevel' => AUDIT_VP,
                'maxlevel' => AUDIT_CEO
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '票务专员',
            )
        ),
        'ticket_book_hurry' => array(
            'A' =>  array(
                'type' => 'manage',
                'level'  => AUDIT_VICE_MANAGER,
                'minlevel' => AUDIT_VP,
                'maxlevel' => AUDIT_CEO
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '人力主管',
            ),
            'C' => array(
                'type' => 'role',
                'rolename' => '票务专员',
            ),
        ),
        'office_supply' => array(
            'A' =>  array(
                'type' => 'role',
                'rolename'  => '办公用品专员',
            ),
        ),
        'lunch_change' => array(
            'A' =>  array(
                'type' => 'role',
                'rolename'  => '午餐预定专员',
            ),
        ),
        'administrative_expense' => array(
            'A' =>  array(
                'type' => 'custom',
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '行政费用审批专员',  
            ),
            'C' => array(
                'type' => 'role',
                'rolename' => '财务',  
            ),
        ),
        'administrative_apply' => array(
            'A' =>  array(
                'type' => 'role',
                'rolename' => '物品申请审批专员',
            ),
            'B' =>  array(
                'type' => 'manage',
                'level'  => AUDIT_MANAGER,
                'minlevel' => AUDIT_CEO,
                'maxlevel' => AUDIT_CEO
            ),
            'C' => array(
                'type' => 'custom',
            ),
            'D' => array(
                'type' => 'role',
                'rolename' => '物品申请审批专员',
            ),
        ),
        'administrative_apply_5000' => array(
            'A' =>  array(
                'type' => 'role',
                'rolename' => '物品申请审批专员',
            ),
            'B' =>  array(
                'type' => 'manage',
                'level'  => AUDIT_VP,
                'minlevel' => AUDIT_VP,
                'maxlevel' => AUDIT_CEO
            ),
            'C' => array(
                'type' => 'custom',
            ),
            'D' => array(
                'type' => 'role',
                'rolename' => '物品申请审批专员',
            ),
        ),
        'finance_loan' => array(
            'A' => array(
                'type' => 'custom',
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '借款单审批专员',
            ),
        ),
        'finance_offset' => array(
            'A' => array(
                'type' => 'custom',
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '借款单审批专员',
            ),
        ),
        'finance_payment' => array(
            'A' => array(
                'type' => 'custom',
            ),
            'B' => array(
                'type' => 'role',
                'rolename' => '借款单审批专员',
            ),
        ),
    );

    // 指定审批人的配置文件
    public static $entrust_audit_conf = array(

        //郭慧文38:申请流程人的user_id    
        '38' => array( 

            'absence' => 1,  //大清1:委托人的user_id 

        ) 

    );

    public static function getWorkFlowConfigByObjType($userWorkflow)
    {
        if(!empty($userWorkflow['userworkflow_config']) && isset(self::$config[$userWorkflow['userworkflow_config']])){
            //有些工作流 可能需要多个审批流程
            return self::$config[$userWorkflow['userworkflow_config']];
        }
        if(isset(self::$config[$userWorkflow['obj_type']])){
            return self::$config[$userWorkflow['obj_type']];
        }
        return array();
    }
}


