<?php
/**
 * 团建服务
 * 
 **/

class TeamBuildingService {

    public $sectionModel;
    public $teamModel;
    public $teamSeniosModel;


    public function __construct() {

        $this->sectionModel    = new Section();
        $this->teamModel       = new TeamBuildingRecord();
        $this->teamSeniosModel = new TeamBuildingSenior();

    }

    public function formAmountData(&$data) {



    }

    public function getTemaBuilding($userId) {

        $teamData = $this->getSectionBuilding($userId);
        if(empty($teamData)) {

            return false;

        }

        $data = array();
        foreach($teamData  as $id=>$team ){

            if($team['can_building'] == 1) {

                $data[$id]['name'] = $team['name'];
                $data[$id]['id'] = $id;

            }

        }

        return $data;
        

    }

    public function isSectionBuilding($userId) {

        $teamData = $this->getSectionBuilding($userId);

        if(empty($teamData)) {

            return false;

        }

        foreach($teamData  as $team){

            if($team['can_building'] == 1) {

                return true;

            }

        }

        return false;

    }

    public function getSectionBuilding($userId) {
        
        $buildingTeamArr = $this->sectionModel->getAllSections($userId); 
        if(empty($buildingTeamArr)) {

            return false;

        }

        $teamData = array();

        foreach($buildingTeamArr as $buildTeam) {

            $id           = $buildTeam['id'];
            $name         = $buildTeam['name'];
            $can_building = $buildTeam['can_building']; 

            $teamTmp['id'] = $id;
            $teamTmp['name'] = $name;
            $teamTmp['can_building'] = $can_building;
            $teamData[$id] = $teamTmp;
            $this->getParentId($id, $teamData);

        }

        return $teamData;

    }

    //根据部门secionID,获取所有子部门
    public function getParentId($parentId, &$data) {

        $buildTeamArr = $this->sectionModel->getParentId($parentId);

        if(empty($buildTeamArr) ) {

            return false;

        }

        foreach($buildTeamArr as $team) {

            $id               = $team['id'];
            $name             = $team['name'];
            $can_building     = $team['can_building'];
            $senior_building  = $team['senior_building'];


            $data[$id] = array(

                'id'           => $id,
                'name'         => $name,
                'can_building' => $can_building,
                'senior_building'=> $senior_building

            );

            $this->getParentId($id, $data);
            

        }


    }

    public function isParentsTeambuildingSenior($sectionId) {

        $sectionArr = $this->sectionModel->getItemByPk($sectionId);
        $parentId = $sectionArr['parent_id']; 
        $parentData = array();
        $this->getParentsSections($parentId, $parentData);

        foreach($parentData as $data) {

            if($data['senior_building'] == 1) {

                return true; 

            }

        }

        return false;

    }

    public function isSubTeambuildingSenior($sectionId) {

        $subData = $this->getBySectionData($sectionId);
        foreach($subData as $data) {

            if($data['senior_building'] == 1) {

                return true; 

            }

        }

        return false;

    }

    //获取所有上级用户
    public function getParentsSections($sectionId, &$data) {

        $sectionArr = $this->sectionModel->getItemByPk($sectionId);
        $parentId = $sectionArr['parent_id']; 

        $id = $sectionArr['id'];
        $data[$id] = $sectionArr;

        if($parentId == 0) {

            return false; 

        }

        $this->getParentsSections($parentId, $data);


    }

    //判断是否存在高级团建权限
    public function isSeniorTeambuilding($userId) {

        $condition = array( "senior_building" => "1" );
        $sectionArr = $this->sectionModel->getConditionSection($userId, $condition);

        if(!empty($sectionArr) )  {

            return true;

        }

        return false;

    }

    /**
     * 获取某一个sectionID下所有子部门
     **/
    public function getBySectionData($sectionId) {

       // $sectionArr = $this->sectionModel->getParentId($sectionId);


       $data = array();

       $this->getParentId($sectionId, $data); 


       $canBuildingData = array();

       foreach($data as $id=>$team) {

           if( $team['can_building'] == 1 ) {

                $canBuildingData[$id] = $team;   

           }

       }

       return $canBuildingData;

    }

    /**
     * @parms array & $seniorData  引用传递所有子部门信息 
     * @return void
     **/
    public function formTeamBuding( &$sectionData)  {


        foreach($sectionData as $id=>&$data ) {

            $data['sum']   = 0;
            $data['quota'] = 0;
            $users = $this->sectionModel->getAllUserOfBuildingSection($id);
            $quota = $this->teamSeniosModel->getBySectionIdData($id);

            if(!empty($quota)) {

                $data['quota'] = intval($quota[0]['quota']);

            }

            $data['total'] = count($users) * TEAMBUILDING_MONEY;
            $data['money'] = intval($data['quota']) * 0.01 * $data['total'];

        }


    }

    public function addTeamBuildingSenior($data) {

            $sectionId = $data['from_section'];

            if(empty($sectionId) ) {

                return false;

            }


            $res = $this->teamSeniosModel->getBySectionIdData($sectionId);  

            //利率没有发生改变
            if($res['0']['quota'] == $data['quota'] ) {

                return true;

            }

            if(!empty($res)) {

                $arrData['id']      = $res[0]['id'];
                $arrData['status']  = STATUS_INVALID;
                $is_add = $this->teamSeniosModel->save($arrData);

            }

            $arrData['id'] = null;
            $arrData['create_time']  =   time();
            $arrData['quota']        =   $data['quota'];
            $arrData['date']         =   date("Y-m-d");
            $arrData['operator_id']  =   intval(Yii::app()->session['oa_user_id'] ); 
            $arrData['from_section'] =   $sectionId;
            $arrData['to_section']   =   $data['to_section'];
            $arrData['status']       =   STATUS_VALID;

            $is_add = $this->teamSeniosModel->save($arrData);

            return $is_add;

    }

    /**
     * 每月batch脚本执行方法
     **/
    function batchSeniorBuilding($data) {

        $users = $this->sectionModel->getAllUserOfBuildingSection($data['from_section']);
        $amount = count($users) * TEAMBUILDING_MONEY;
        $data['amount'] = $amount * $data['quota'] * 0.01;
        $isAdd = $this->teamModel->seniorTeamBuildingRecord($data);

        return $isAdd;

    }


}

