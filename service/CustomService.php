<?php
class CustomService{
    private $_userWorkflowId;
    private $_objType;
    private $_userWorkflow;

    public function __construct($userWorkflowId){
        $userWorkflowMode = new UserWorkFlow();
        $userWorkflow = $userWorkflowMode->getAllOfUserWorkflowById($userWorkflowId);
        $this->_userWorkflow = $userWorkflow;
        $this->_userWorkflowId = $userWorkflowId;
        $this->_objType = $userWorkflow['obj_type'];
    }

    public function customProcess(){
        switch ($this->_objType) {
            case 'administrative_apply':
                $this->createAdministrativeApplyAuditList();
                break;
            case 'administrative_expense':
                $this->createAdministrativeExpenseAuditList();
                break;
            case 'finance_loan':
                $this->createFinanceLoanOrPaymentAuditList('finance_loan');
                break;
            case 'finance_offset':
                $this->createFinanceOffsetAuditList();
                break;
            case 'finance_payment':
                $this->createFinanceLoanOrPaymentAuditList('finance_payment');
                break;
            case 'online_order':
            case 'entry':
            case 'dimission':
                $this->createAuditList2();
                break;
        }

    }

    public function createAdministrativeApplyAuditList() {
        $applyModel = new AdministrativeApply();
        $applyItem = $applyModel->getItemByPk($this->_userWorkflow['obj_id']);
        $amount = $applyItem['staff_total'];
        $config = array(
            array(
                'min' => 0,
                'max' => 999,
                'audit' => array(
                    '吕晓琳'   
                )
            ),
            array(
                'min' => 1000,
                'max' => 9999,
                'audit' => array(
                    '吕晓琳',
                    '郭慧文'   
                )
            ),
            array(
                'min' => 10000,
                'max' => null,
                'audit' => array(
                    '吕晓琳',
                    '郭慧文',
                    'YEDAQING'  
                )
            )
        );
        $nextNodeTag = 'C';
        $this->createAuditList($amount,$config,$nextNodeTag);
    }

    public function createAdministrativeExpenseAuditList() {
        $expenseModel = new AdministrativeExpense();
        $expenseItem = $expenseModel->getItemByPk($this->_userWorkflow['obj_id']);
        $amount = $expenseItem['amount'];
        $sectionIds = [48,86,93,100,143,92,99,132];
        $user = Yii::app()->params['user'];
        $sectionModel = new Section();
        $section = $sectionModel->getItemByPk($user['section_id']);
        $sections = $sectionModel->getParentSectionBySectionId($section['id']);
        $sections = array_merge(array($section),$sections);
        $sectionIdsSelf = array(); 
        if(is_array($sections)){
            foreach($sections as $item)
                $sectionIdsSelf[] = $item['id']; 
        }
        $keys = array_intersect($sectionIds,$sectionIdsSelf);
        $configKey = reset($keys);
        $configAll = array(
            48 => array( //网点运营部
                array(
                    'min' => 0,
                    'max' => 1200,
                    'audit' => array(
                        '贾晓宁'   
                    )
                ),
                array(
                    'min' => 1200,
                    'max' => 10000,
                    'audit' => array(
                        '贾晓宁',
                        '董科'   
                    )
                ),
                array(
                    'min' => 10000,
                    'max' => null,
                    'audit' => array(
                        '贾晓宁',
                        '董科',
                        'YEDAQING'   
                    )
                ),
            ),   
            86 => array( //华北蜂鸟
                array(
                    'min' => 0,
                    'max' => 1000,
                    'audit' => array(
                        '季毛杰',
                        '徐志东',
                        '费杰'   
                    )
                ),
                array(
                    'min' => 1000,
                    'max' => null,
                    'audit' => array(
                        '季毛杰',
                        '徐志东',
                        '费杰',
                        '陆佳彦'   
                    )
                ),
            ),
            93 => array( //华东蜂鸟
                array(
                    'min' => 0,
                    'max' => 1000,
                    'audit' => array(
                        '张毅',
                        '郜华',
                        '费杰'   
                    )
                ),
                array(
                    'min' => 1000,
                    'max' => null,
                    'audit' => array(
                        '张毅',
                        '郜华',
                        '费杰',
                        '陆佳彦'   
                    )
                ),
            ),
            100 => array( //华南蜂鸟
                array(
                    'min' => 0,
                    'max' => 1000,
                    'audit' => array(
                        '邓文平',
                        '林小鸿',
                        '费杰'   
                    )
                ),
                array(
                    'min' => 1000,
                    'max' => null,
                    'audit' => array(
                        '邓文平',
                        '林小鸿',
                        '费杰',
                        '陆佳彦'   
                    )
                ),
            ),
            143 => array( //华东抵押贷款部
                array(
                    'min' => 0,
                    'max' => 1000,
                    'audit' => array(
                        '徐志东',
                        '吕晓琳'
                    )
                ),
                array(
                    'min' => 1000,
                    'max' => 10000,
                    'audit' => array(
                        '徐志东',
                        '吕晓琳',
                        '郭慧文'   
                    )
                ),
                array(
                    'min' => 10000,
                    'max' => null,
                    'audit' => array(
                        '徐志东',
                        '吕晓琳',
                        '郭慧文',
                        '陆佳彦'   
                    )
                ),
            ),
            92 => array( //华东商务运营部
                array(
                    'min' => 0,
                    'max' => 1000,
                    'audit' => array(
                        '郜华',
                        '吕晓琳'
                    )
                ),
                array(
                    'min' => 1000,
                    'max' => 10000,
                    'audit' => array(
                        '郜华',
                        '吕晓琳',
                        '郭慧文'   
                    )
                ),
                array(
                    'min' => 10000,
                    'max' => null,
                    'audit' => array(
                        '郜华',
                        '吕晓琳',
                        '郭慧文',
                        'YEDAQING'   
                    )
                ),
            ),
            99 => array( //华南商务运营部
                array(
                    'min' => 0,
                    'max' => 1000,
                    'audit' => array(
                        '林小鸿',
                        '吕晓琳'
                    )
                ),
                array(
                    'min' => 1000,
                    'max' => 10000,
                    'audit' => array(
                        '林小鸿',
                        '吕晓琳',
                        '郭慧文'   
                    )
                ),
                array(
                    'min' => 10000,
                    'max' => null,
                    'audit' => array(
                        '林小鸿',
                        '吕晓琳',
                        '郭慧文',
                        'YEDAQING'   
                    )
                ),
            ),
            132 => array( //行政部
                array(
                    'min' => 0,
                    'max' => 1000,
                    'audit' => array(
                        '吕晓琳'
                    )
                ),
                array(
                    'min' => 1000,
                    'max' => 10000,
                    'audit' => array(
                        '吕晓琳',
                        '郭慧文'   
                    )
                ),
                array(
                    'min' => 10000,
                    'max' => null,
                    'audit' => array(
                        '吕晓琳',
                        '郭慧文',
                        'YEDAQING'   
                    )
                ),
            ),
        );
        if($user['id'] == 1636){
            $config = array(
                array(
                    'min' => 0,
                    'max' => 1000,
                    'audit' => array(
                        '吕晓琳'
                    )
                ),
                array(
                    'min' => 1000,
                    'max' => 10000,
                    'audit' => array(
                        '吕晓琳',
                        '郭慧文'   
                    )
                ),
                array(
                    'min' => 10000,
                    'max' => null,
                    'audit' => array(
                        '吕晓琳',
                        '郭慧文',
                        'YEDAQING'   
                    )
                ),
            );
        }else{
            $config = $configAll[$configKey];
        }
        $nextNodeTag = 'A';
        $this->createAuditList($amount,$config,$nextNodeTag);
    }

    public function createFinanceOffsetAuditList(){
        $financeLoanId = $this->_userWorkflow['ext']['loan_id'];
        $userWorkflowModel = new UserWorkFlow();
        //获取冲抵借款单对应的借款单的uwf的id
        $financeLoanUwfId = $userWorkflowModel->getUserWorkflowId($financeLoanId,'finance_loan');
        $auditListModel = new AuditList();
        //获取相应借款单的audit_list;
        $auditList = $auditListModel->getAuditListByUserWorkflowId($financeLoanUwfId);
        foreach ($auditList as $value) {
            $ret = array();
            foreach ($value as $k => $v) {
                if($k == 'id'){
                    continue;
                }elseif($k == 'status'){
                    $ret['status'] = STATUS_VALID;
                }elseif($k == 'user_workflow_id'){
                    $ret['user_workflow_id'] = $this->_userWorkflowId;
                }elseif($k == 'create_time'){
                    $ret['create_time'] = time();
                }else{
                    $ret[$k] = $v;
                }
            }
            $auditListModel->save($ret);
        }
    }

    public function createFinanceLoanOrPaymentAuditList($obj_type){
        $sectionMoneyRightModel = new SectionMoneyRight();
        if($obj_type == 'finance_loan'){
            $financeLoanModel = new FinanceLoan();
            $financeItem = $financeLoanModel->getItemByPk($this->_userWorkflow['obj_id']);
        }elseif ($obj_type == 'finance_payment'){
            $financePaymentModel = new FinancePayment();
            $financeItem = $financePaymentModel->getItemByPk($this->_userWorkflow['obj_id']);
        }
        $amount = $financeItem['amount'];
        $user = Yii::app()->params['user'];
        $sectionModel = new Section();
        $auditListModel = new AuditList();
        $section = $sectionModel->getItemByPk($user['section_id']);
        $userModel = new User();
        $pos = 1;
        $lastManager = 0;//记录上一级部门的manager_id
        while ($section) {
            $right = $sectionMoneyRightModel->getItemBySectionRight($section['id'],1);
            //联盟特殊处理，2000以下廖舟审核
            if($section['id'] == 141){
                $section['manager_id'] = 21;
            }
            //审批人不重复
            if($lastManager != $section['manager_id'] && $section['manager_id'] != $user['id']){
                $arrData['user_id'] = $section['manager_id'];
                $arrData['user_workflow_id'] = $this->_userWorkflowId;
                $arrData['workflow_step'] = 'A';
                $arrData['pos'] = $pos;
                $arrData['create_time'] = time();
                $arrData['status'] = STATUS_VALID;
                $pos += 1;
                $auditListModel->save($arrData);
            }
            $lastManager = $section['manager_id'];

            if($section['manager_id'] == 38 && $this->_userWorkflow['ext']['pay_type'] == FINANCE_LOAN_TICKET){
                break;
            }
            if($right && $right['money'] > $amount){
                break;
            }else{
                $parentSection = $sectionModel->getItemByPk($section['parent_id']);
                //如果审批到最上一级部门leader且金额权限仍不满足，若leader不是ceo，需要添加ceo审批
                if(empty($parentSection)){
                    if($section['manager_id'] != 1){
                        $arrData['user_id'] = 1;
                        $arrData['user_workflow_id'] = $this->_userWorkflowId;
                        $arrData['workflow_step'] = 'A';
                        $arrData['pos'] = $pos;
                        $arrData['create_time'] = time();
                        $arrData['status'] = STATUS_VALID;
                        $pos += 1;
                        $auditListModel->save($arrData);
                    }
                }
                $section = $parentSection;
                continue;
            }          
        }
        if($this->_userWorkflow['ext']['pay_type'] == FINANCE_LOAN_ADMIN){
            $arrData['user_id'] = 38;
            $arrData['user_workflow_id'] = $this->_userWorkflowId;
            $arrData['workflow_step'] = 'A';
            $arrData['pos'] = $pos;
            $arrData['create_time'] = time();
            $arrData['business_role'] = "HR";
            $arrData['status'] = STATUS_VALID;
            $pos += 1;
            $auditListModel->save($arrData);
        }elseif($this->_userWorkflow['ext']['pay_type'] == FINANCE_LOAN_HR){
            $arrData['user_id'] = 71;
            $arrData['user_workflow_id'] = $this->_userWorkflowId;
            $arrData['workflow_step'] = 'A';
            $arrData['pos'] = $pos;
            $arrData['create_time'] = time();
            $arrData['business_role'] = "HR";
            $arrData['status'] = STATUS_VALID;
            $pos += 1;
            $auditListModel->save($arrData);
            $arrData['user_id'] = 38;
            $arrData['pos'] = $pos;
            $pos += 1;
            $auditListModel->save($arrData);
        }
    }

    //administrative_apply，administrative_expense插入audit_list
    public function createAuditList($amount,$config,$nodeTag)
    {
        $userworkflowId = $this->_userWorkflowId;
        $userWorkflowInfo = $this->_userWorkflow;
        $userId = $userWorkflowInfo['user_id'];
        $auditList = array();
        if(!empty($config) && is_array($config)){
            foreach($config as $item){
                if(isset($item['min']) && $amount >= $item['min']){
                    if(is_null($item['max'])){
                        $auditList = $item['audit']; 
                    }elseif($amount < $item['max']){
                        $auditList = $item['audit']; 
                    }    
                }
            }
        }


        if(empty($auditList)){
            Yii::log('config fail userId:'.$userId,'error');
        }
        $userModel = new User();
        $auditListModel = new AuditList();
        $arrData = array();
        $arrData['user_workflow_id'] = $userworkflowId;
        $arrData['workflow_step'] = $nodeTag;
        $arrData['create_time'] = time();
        $arrData['status'] = STATUS_VALID;
        foreach($auditList as $key => $name){
            $user = $userModel->getItemByName($name);
            if(empty($user)){
                Yii::log('配置用户名不存在:'.$name,'error'); 
            }
            $arrData['user_id'] = $user['id'];
            $arrData['pos'] = $key+1;
            $id = $auditListModel->save($arrData);
        }
    }

    //online_order，entry，dimission插入audit_list
    public function createAuditList2(){
        $userWorkflowId = $this->_userWorkflowId;
        $userWorkflow = $this->_userWorkflow;
        $userWorkflowObjType = $this->_objType;
        $auditList = Yii::app()->request->getParam('auditlist');
        $oa_user_id = Yii::app()->session['oa_user_id'];

        $userModel = new User();
        $taskModel = new UserTask();
        $model = new AuditList();
        $index = 0;

        foreach($auditList as $step => $row){
            if(empty($row)){
                continue;
            }
            foreach($row as $business_role => $val){
                if(empty($val)){
                    continue;
                }   
                ++ $index;
                if($business_role == "op"){
                    $index = 1;
                }
                $name = $val;
                //支持分割符号：中文逗号、|、换行符、制表符、下划线
                $name = str_replace(array("，", "|", "\n", "\t", "_"," "), ',', $name);
                //判断是否多个用户邮箱
                if( strpos($name, ',') !== false ) {                   
                    $rd_arr = array();
                    $rd_arr = explode(",", $name);
                    foreach($rd_arr as $rd_val) {
                        if(empty($rd_val)){
                            continue;
                        }
                        $is_add = $this->addRdQueueList($step, $business_role, $rd_val, $userWorkflowId, $index);
                        if(!$is_add){
                            continue;
                        }
                    }
                }else {
                    $is_add = $this->addRdQueueList($step, $business_role, $val, $userWorkflowId, $index); 
                    if(!$is_add){
                        continue;
                    }
                }
            }
        }

        if($userWorkflowObjType == 'online_order') {
            //复制C节点的值,有些细节处理，可能代码有相同
            $index = 0;
            foreach ($auditList as $step => $row) {
                if (empty($row)) {
                    continue;
                }
                foreach ($row as $business_role => $val) {
                    if (empty($val) || $business_role == "ue" || $business_role == "rd_manger" || $business_role == "op") {
                        continue;
                    }
                    ++$index;
                    $name = $val;
                    //支持分割符号：中文逗号、|、换行符、制表符、下划线
                    $name = str_replace(array("，", "|", "\n", "\t", "_", " "), ',', $name);
                    //判断是否多个用户邮箱
                    if (strpos($name, ',') !== false) {
                        $rd_arr = array();
                        $rd_arr = explode(",", $name);
                        foreach ($rd_arr as $rd_val) {
                            if (empty($rd_val)) {
                                continue;
                            }
                            $users = $userModel->getItemByEmailOrName(trim($rd_val));
                            $user = reset($users);
                            if (empty($user)) {
                                continue;
                            }
                            $this->addCStepQueueList($user['id'], $business_role, $index, $userWorkflowId);

                        }

                    }else {
                        $users = $userModel->getItemByEmailOrName(trim($val));
                        $user = reset($users);
                        if (empty($user)) {
                            continue;
                        }
                        $this->addCStepQueueList($user['id'], $business_role, $index, $userWorkflowId);
                    }
                }
            }
        }
    }

    public function addCStepQueueList($user_id,$business_role,$index,$userWorkflowId){ 
        $model = new AuditList();
        $tmp = array();
        $tmp['pos'] = $index;
        $tmp['user_id'] = $user_id;
        $tmp['business_role'] = $business_role;
        $tmp['user_workflow_id'] = $userWorkflowId;
        $tmp['workflow_step'] = 'C';
        $tmp['create_time'] = time();
        $tmp['status'] = STATUS_VALID; 

        $model->save($tmp);
    }

    public function addRdQueueList($step, $business_role, $name, $userWorkflowId, $index) {

        $model = new AuditList();
        $userModel = new User();
        $taskModel = new UserTask();
        $oa_user_id = Yii::app()->session['oa_user_id'];
        $email = OAConfig::checkUserAccount($name);
        $users = $userModel->getItemByEmailOrName(trim($name));
        $user = reset($users);

        if( empty($user) ) {
            return false;
        }

        $tmp = array();
        $tmp['pos'] = $index;
        $tmp['user_id'] = $user['id'];
        $tmp['business_role'] = $business_role;
        $tmp['user_workflow_id'] = $userWorkflowId;
        $tmp['workflow_step'] = $step;
        $tmp['create_time'] = time();

        if($business_role == 'rd' && $user['id'] == $oa_user_id){
            //生成已经审批的记录 
            $tmp['status'] = STATUS_INVALID;
            $row = array();
            $row['user_id'] = $user['id'];
            $row['user_workflow_id'] = $userWorkflowId;
            $row['audit_user_id'] = $user['id'];
            $row['workflow_step'] = $step;
            $row['audit_status']  = OA_TASK_TONGGUO;
            $row['audit_desc']    = 'rd';
            $row['create_time']   = $row['modify_time'] = time();
            $taskModel->save($row);
        }else{
            $tmp['status'] = STATUS_VALID; 
        }

        $model->save($tmp);

    }
}
?>
