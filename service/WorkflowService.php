<?php
class WorkflowService
{
    private $_userWorkflowId;
    public  $_userWorkflow; //工作流所有信息，包含用户填写的申请表信息
    private $_workflowConfig;
    private $_lastTask;
    private $_processAgain = false; //对于自动审批通过的流程 需要重走process，生成下个审批节点

    public function __construct($userworkflowId)
    {
        if(empty($userworkflowId)){
            throw new CHttpException(404,'初始化工作流错误,缺少工作流id');
        }
        $userWorkflowMode = new UserWorkFlow();
        $userWorkflow = $userWorkflowMode->getAllOfUserWorkflowById($userworkflowId);
        if(empty($userWorkflow)){
            throw new CHttpException(404,'初始化工作流错误,工作流不存在');
        }

        $this->_userWorkflowId = $userworkflowId;
        $this->_userWorkflow = $userWorkflow; 
        $this->_workflowConfig = WorkflowConfig::getWorkFlowConfigByObjType($userWorkflow);//set node number
        $this->_lastTask = $this->getCurrentUserTask($userworkflowId);
    } 

    //拿到审批流程的下一个节点
    public function getNextAuditNode($currentNodeTag)
    {
        $workflowConfigKeyMap = array_keys($this->_workflowConfig);
        if($currentNodeTag){
            $currentNodeStatus = $this->getStatusOfCurrentNode($currentNodeTag);
            if($currentNodeStatus){
                $currentKey = array_search($currentNodeTag,$workflowConfigKeyMap);
                $nextNodeTag = $workflowConfigKeyMap[$currentKey+1];//下一个节点
            }else{
                $nextNodeTag = $currentNodeTag;
            }
        }else{
            $nextNodeTag = reset($workflowConfigKeyMap);//第一个节点
        }

        //判断审批节点是否需要跳过
        $judge = true;
        while($judge && $nextNodeTag){
            $nodeConfig = $this->getNodeConfigByNodeTag($nextNodeTag); 
            if($nodeConfig['type'] == 'manage'){
                $userModel = new User();
                $user = $userModel->getItemByPk($this->_userWorkflow['user_id']); 
                if(!empty($nodeConfig['minlevel']) && $user['level'] >= $nodeConfig['minlevel'] ){
                    $currentKey = array_search($nextNodeTag,$workflowConfigKeyMap);
                    $nextNodeTag = $workflowConfigKeyMap[$currentKey+1];//下一个节点
                    $judge = true; //当前节点跳过，需要重新判断下一个节点是否需要跳过
                }else{
                    $judge = false;
                }
            }else{
                $judge = false;
            }
        }
        return $nextNodeTag; 
    }

    //根据工作流状态阶段,审批被拒绝的不可以走这个流程
    public function workflowProcess()
    {
        $currentNodeTag = false;
        $nextNodeTag = false;
        if($this->_lastTask){
            $currentNodeTag = $this->_lastTask['workflow_step'];//compare the step from worf flow config
        }
        $nextNodeTag = $this->getNextAuditNode($currentNodeTag);
        if($nextNodeTag){
            $ret = $this->createTaskByNode($nextNodeTag);//create user task and update user work flow  
            if($ret && $this->_processAgain){ //自动审批通过的,去生成下一个流程节点
                //初始化参数
                $this->_lastTask = $this->getCurrentUserTask($this->_userWorkflowId);
                $this->_processAgain = false;

                $this->workflowProcess();
            }
        }else{
            //整个流程结束
            $userWorkflow['id'] = $this->_userWorkflowId;
            $userWorkflow['audit_status'] = OA_WORKFLOW_AUDIT_PASS;
            $userWorkflow['modify_time'] = time();
            $this->updateUserWorkflow($userWorkflow);

            //如果是销假，审批通过后需要去更改考勤记录
            if($this->_userWorkflow['obj_type'] == 'cancel_absence'){
                $vService = new VacationService(); 
                $vService->updateCheckInOutByCancelAbsence($this->_userWorkflow['obj_id']);

                //将审批通过的销假 删除(vacation_record)记录
                $vService->delHolidayRecordByCancelAbsence($this->_userWorkflow['ext']);
            }

            //如果是销出差，审批通过后需要去更改考勤记录
            if($this->_userWorkflow['obj_type'] == 'cancel_business'){
                $vService = new VacationService(); 
                $vService->updateCheckInOutByCancelBusiness($this->_userWorkflow['obj_id']);

                //将审批通过的销出差 删除(vacation_record)记录
                $vService->delHolidayRecordByCancelBusiness($this->_userWorkflow['ext']);
            }

            //补假，也需要去更改考勤记录
            if($this->_userWorkflow['obj_type'] == 'absence'){
                $vService = new VacationService(); 
                $vService->updateCheckInOutByAbsence($this->_userWorkflow['obj_id']);

                //将审批通过的请假，插入休假(vacation_record)记录
                $vService->createHolidayRecordByAbsence($this->_userWorkflow['ext']);
            }

            //团建报销，向团建记录中插入数据
            if($this->_userWorkflow['obj_type'] == 'team_building'){
                $tbRecordModel = new TeamBuildingRecord();
                $arrData = array();
                $arrData['section_id'] = $this->_userWorkflow['ext']['section_id']; 
                $arrData['user_workflow_id'] = $this->_userWorkflow['id'];
                $arrData['date'] = date('Y-m-d');
                $arrData['create_time'] = time();
                $arrData['amount'] = 0 - $this->_userWorkflow['ext']['amount'];
                $arrData['remark'] = '';
                $arrData['status'] = STATUS_VALID;
                $tbRecordModel->save($arrData);
            }

            //团建报销，向团建记录中插入数据
            if($this->_userWorkflow['obj_type'] == 'team_encourage'){
                $tbRecordModel = new TeamEncourageRecord();
                $arrData = array();
                $arrData['section_id'] = $this->_userWorkflow['ext']['section']; 
                $arrData['user_workflow_id'] = $this->_userWorkflow['id'];
                $arrData['date'] = date('Y-m-d');
                $arrData['create_time'] = time();
                $arrData['amount'] = 0 - $this->_userWorkflow['ext']['amount'];
                $arrData['remark'] = '';
                $arrData['status'] = STATUS_VALID;
                $tbRecordModel->save($arrData);
            }
        }
    }

    //用户当前工作流节点
    public function getCurrentUserTask($userworkflowId)
    {
        $userTask = new UserTask(); 

        $lastTask = $userTask->getLastTaskByUwfId($userworkflowId);

        return $lastTask;
    }

    //判断用户当前节点是否完成
    public function getStatusOfCurrentNode($currentNodeTag)
    {
        $currentNodeConfig = $this->getNodeConfigByNodeTag($currentNodeTag);

        if($currentNodeConfig['type'] == 'manage'){
            $status = $this->isPassOfTypeManage($currentNodeConfig);
        }elseif($currentNodeConfig['type'] == 'role'){
            $status = $this->isPassOfTypeRole();
        }elseif($currentNodeConfig['type'] == 'custom'){
            $status = $this->isPassOfTypeCustom($currentNodeTag);
        }
        return $status;
    }

    //判断manage类型的节点是否完成
    public function isPassOfTypeManage($currentNodeConfig)
    {
        if($this->_lastTask['audit_status'] == OA_TASK_CHUANGJIAN){
            return false;
        }
        $maxlevel = $currentNodeConfig['maxlevel'];
        $level = $currentNodeConfig['level'];
         
        $userModel = new User();
        $auditUserId = $this->_lastTask['entrust_user_id'] ? $this->_lastTask['entrust_user_id'] : $this->_lastTask['user_id'];//如果是委托审批,用委托人去验证权限
        $auditUser = $userModel->getItemByPk($auditUserId);
        if(empty($auditUser)){
            throw new CHttpException(404,'工作流错误，审核人员不存在');
        }
        if($auditUser['level'] >= $level){
            return true;
        }
        if(!empty($maxlevel) && $auditUser['level'] >= $maxlevel){ //到最高审批人
            //配置项中level比maxlevel大。算是错误的
            return true;
        }
        
        $nextApprover = $this->getNextApprover();
        //下一级审批人超过max时
        if(!empty($maxlevel) && $nextApprover['level'] > $maxlevel){ 
            throw new CHttpException(404,'当前审批人没有最终审批权限，下一级审批人已经超出最大审批人');
        }

        return false;
    }

    //判断role类型的节点是否完成
    public function isPassOfTypeRole()
    {
        if($this->_lastTask['audit_status'] == OA_TASK_CHUANGJIAN){
            return false;
        }
        return true;
    }

    //判断custom类型的节点是否完成
    public function isPassOfTypeCustom($nodeTag)
    {
		$model = new UserTask();
		$isBatch = $model->isBatchFlow($this->_lastTask['user_workflow_id']);//multi audit

        if($this->_lastTask['audit_status'] == OA_TASK_CHUANGJIAN || $isBatch == true){
            return false;
        } 

        $model = new AuditList(); 
        $todo = $model->getItemByUserWorkflowIdAndStep($this->_lastTask['user_workflow_id'],$nodeTag);
        if(!empty($todo)){
            return false;
        }
        return true;
    }


    //创建节点流程
    public function createTaskByNode($nodeTag)
    {
        if(!$this->_lastTask){
            if($this->_userWorkflow['obj_type']=='dimission'||$this->_userWorkflow['obj_type']=='entry'||$this->_userWorkflow['obj_type']=='online_order'){
                $customService = new CustomService($this->_userWorkflowId);
                $customService->customProcess();
            }
        }
        $nodeConfig = $this->getNodeConfigByNodeTag($nodeTag); 

        if($nodeConfig['type'] == 'manage'){
            return $this->createManageTask($nodeTag);    
        }elseif($nodeConfig['type'] == 'role'){
            return $this->createRoleTask($nodeTag);
        }elseif($nodeConfig['type'] == 'custom'){
            return $this->createCustom($nodeTag); 
        }
    }

    //创建manage类型user_task
    public function createManageTask($nodeTag)
    {
        $data = array();
        $nodeConfig = $this->getNodeConfigByNodeTag($nodeTag); 
        $userModel = new User();
        $maxlevel = $nodeConfig['maxlevel'];
        $level = $nodeConfig['level'];
        if($this->_lastTask && $this->_lastTask['workflow_step'] == $nodeTag){ //当前节点未完成，继续当前节点
            //选择相信上面的判断，就不再重复判断该节点的上一步是否完成 
            if($this->_lastTask['audit_status'] == OA_TASK_CHUANGJIAN){
                return false;
            }
            $userId = $this->_lastTask['user_id'];
            $nextApprover = $this->getNextApprover();

            if(!empty($maxlevel) && $nextApprover['level'] > $maxlevel){
                throw new CHttpException(404,'当前审批人未到级别，但是下一级审批人已经超过最大级别');
            }elseif(!empty($nextApprover)){
                $data['user_id'] = $nextApprover['id'];
                $data['user_workflow_id'] = $this->_userWorkflowId;
                $data['workflow_step'] = $nodeTag;
                $data['create_time'] = time();
            }
        }else{ //创建工作流新的节点step
            $nextApprover = $this->getNextApprover();
            if(!empty($nextApprover) && !(!empty($maxlevel) && $nextApprover['level'] > $maxlevel)){
                $data['user_id'] = $nextApprover['id'];
                $data['user_workflow_id'] = $this->_userWorkflowId;
                $data['workflow_step'] = $nodeTag;
                $data['create_time'] = time();
            }
        }

        if(!empty($data)){
            $taskModel = new UserTask();
            $trusteeUser = $this->getTrusteeUser($nextApprover['id']);//代理人
            if(!empty($trusteeUser)){//将任务分配给代理人
                $data['user_id'] = $trusteeUser['id'];
                $data['entrust_user_id'] = $nextApprover['id'];
                $auditUserIds = $taskModel->getAuditUserIdByUwfId($this->_userWorkflowId,$this->_lastTask['workflow_step']);
                if(in_array($trusteeUser['id'],$auditUserIds)){ //代理人已经审核过一次的,直接通过
                    $this->_processAgain = true;
                    $data['audit_status'] = OA_TASK_TONGGUO; 
                    $data['audit_user_id'] = $trusteeUser['id'];
                    $email = false;
                }else{
                    $email = $trusteeUser['email'];
                }
            }else{
                $email = $nextApprover['email'];
            }


            if(!$taskModel->save($data)){
                Yii::log('error: save user_task fail, user_workflow_id'.$this->_userWorkflowId);
                return false;
            }else{
                //发送邮件通知
                $this->sendEmailtoManage($email);
                //更新工作流状态
                $userWorkflow['id'] = $this->_userWorkflowId;
                $userWorkflow['audit_status'] = OA_WORKFLOW_AUDIT_SHENPIZHONG;
                $userWorkflow['modify_time'] = time();
                $this->updateUserWorkflow($userWorkflow);
                return true;
            }
        }
    }

    //创建role类型user_task
    public function createRoleTask($nodeTag)
    {
        $nodeConfig = $this->getNodeConfigByNodeTag($nodeTag); 
        if($this->_lastTask && $this->_lastTask['workflow_step'] == $nodeTag){
            //当前节点未完成，继续当前节点
            if($this->_lastTask['audit_status'] == OA_TASK_CHUANGJIAN){
                return false;
            }
        }
        $roleModel = new Role();
        $role = $roleModel->getItemByName($nodeConfig['rolename']);
        if(empty($role)){
            Yii::log('工作流节点错误，角色配置错误。workflow_id:'.$this->_userWorkflow['workflow_id']);
            throw new CHttpException(404,'配置角色不存在');
        }
        $data = array();
        $data['role_id'] = $role['id'];
        $data['user_workflow_id'] = $this->_userWorkflowId;
        $data['workflow_step'] = $nodeTag;
        $data['create_time'] = time();
        $taskModel = new UserTask();
        if(!$taskModel->save($data)){
            Yii::log('error: save user_task fail, user_workflow_id'.$this->_userWorkflowId);
            return false;
        }else{
            //发送邮件
            $roleUsers = $roleModel->getUserByRoleId($role['id']);
            if(!empty($roleUsers)){
                foreach($roleUsers as $item){
                    $this->sendEmailtoManage($item['email'],$this->_userWorkflow['obj_type']);
                }
            }

            //更新工作流状态
            $userWorkflow['id'] = $this->_userWorkflowId;
			if($this->_userWorkflow['obj_type'] == "workorder"){
                $userWorkflow['audit_status'] = WORKORDER_NOT_ISSUE;
			}else{
                $userWorkflow['audit_status'] = OA_WORKFLOW_AUDIT_SHENPIZHONG;
			}
            $userWorkflow['modify_time'] = time();
            $this->updateUserWorkflow($userWorkflow);
            return true;
        }
    }

    //创建custom类型user_task
    public function createCustom($nodeTag)
    {
        $nodeConfig = $this->getNodeConfigByNodeTag($nodeTag); 
        if($this->_lastTask && $this->_lastTask['workflow_step'] == $nodeTag){
            //当前节点未完成，继续当前节点	
            $model = new UserTask();
            $isBatch = $model->isBatchFlow($this->_lastTask['user_workflow_id']);//multi audit

            if($this->_lastTask['audit_status'] == OA_TASK_CHUANGJIAN || $isBatch == true){
                return false;
            }
        }
        if($this->_lastTask['workflow_step'] != $nodeTag){
            if(!($this->_userWorkflow['obj_type']=='dimission'||$this->_userWorkflow['obj_type']=='entry'||$this->_userWorkflow['obj_type']=='online_order')){
                $customService = new CustomService($this->_userWorkflowId);
                $customService->customProcess();
            }
            
        }

        $auditListModel = new AuditList();
        //判断流程
        $items = $this->onlineOrderUserTask($this->_userWorkflowId, $nodeTag);

        if(empty($items) ){
            Yii::log('工作流状态判断错误。workflow_id:'.$this->_userWorkflow['workflow_id']);
            throw new CHttpException(404,'工作流节点状态判断错误');
        }

        foreach($items as $item){

            $data = array();
            $data['user_id'] = $item['user_id'];
            $data['user_workflow_id'] = $this->_userWorkflowId;
            $data['workflow_step'] = $nodeTag;
            $data['create_time'] = time();
            $data['audit_desc'] = $item['business_role'];
            $taskModel = new UserTask();
            $trusteeUser = $this->getTrusteeUser($item['user_id']);//代理人
            if(!empty($trusteeUser)){//将任务分配给代理人
                $data['user_id'] = $trusteeUser['id'];
                $data['entrust_user_id'] = $item['user_id'];
                $auditUserIds = $taskModel->getAuditUserIdByUwfId($this->_userWorkflowId,$this->_lastTask['workflow_step']);
                if(in_array($trusteeUser['id'],$auditUserIds)){ //代理人已经审核过一次的,直接通过
                    $this->_processAgain = true;
                    $data['audit_status'] = OA_TASK_TONGGUO; 
                    $data['audit_user_id'] = $trusteeUser['id'];
                    $email = false;
                }else{
                    $email = $trusteeUser['email'];
                }
            }

            //$isBAudit = false;
            if($this->_userWorkflow['obj_type'] == "online_order" && $this->_userWorkflow['deploy_id'] > 0 && $nodeTag == "B"){
                $this->_processAgain = true;
                $email = false;
                $data['audit_status']  = OA_TASK_TONGGUO;
                $data['modify_time']   = time();
                $data['audit_user_id'] = 8;
            } 

            if(!$taskModel->save($data)){
                Yii::log('error: save user_task fail, user_workflow_id'.$this->_userWorkflowId);
            }else{
                //发送邮件
                $userModel = new User();
                $user = $userModel->getItemByPk($item['user_id']);
                if(empty($email) && $email !== false){
                    $email = $user['email'];
                }

                if($this->_userWorkflow['obj_type'] == 'online_order'){
                    if($nodeTag != "B" || $this->_userWorkflow['deploy_id'] == 0){
                        $deploy_id = $this->_userWorkflow['deploy_id'];
                        $this->onlineOrderEmail($item,$user,$deploy_id,$email);
                    } 
                }else{
                    $this->sendEmailtoManage($email);
                }

                //更改audit_list表状态
                $item['status'] = STATUS_INVALID;
                $auditListModel->save($item);

                //更新工作流状态
                $userWorkflow['id'] = $this->_userWorkflowId;
                if($this->_userWorkflow['obj_type'] == "workorder"){
                    $userWorkflow['audit_status'] = WORKORDER_ISSUEED;
                }else{
                    $userWorkflow['audit_status'] = OA_WORKFLOW_AUDIT_SHENPIZHONG;
                }
                $userWorkflow['modify_time'] = time();
                $this->updateUserWorkflow($userWorkflow);
            }

        }

        return true;
    }

    function onlineOrderUserTask($userWorkflowId, $nodeTag) {

        $auditListModel = new AuditList();
        $items = $auditListModel->getItemsByUserWorkflow($userWorkflowId, $nodeTag);

        if(!empty($items)) {

            return $items;

        }

        return null;

    }

    //更新工作流状态
    public function updateUserWorkflow($userWorkflow)
    {
        $userWorkflowMode = new UserWorkFlow(DBModel::MODE_RW);
        if(!$userWorkflowMode->save($userWorkflow)){
            Yii::log('error: update user_workflow fail, user_workflow_id'.$this->_userWorkflowId);
        }
    }

    //提醒审批邮件
    public function sendEmailtoManage($toEmail,$isWorkorder="other")
    {
        $userModel = new User();
        $user = $userModel->getItemByPk($this->_userWorkflow['user_id']);
        $arrMail['to'] = $toEmail."@rong360.com";
        if($isWorkorder == "workorder")
        {
            $arrMail['subject'] = 'oa '.$user['name'].$this->_userWorkflow['name'].'需要处理';
            $arrMail['body'] = $user['name'].$this->_userWorkflow['name'].':'.$this->_userWorkflow['ext']['title'].'请登录oa处理任务(<a href="http://oa.rong360.com/site/login.html">oa.rong360.com</a>)';
        }else{
            $arrMail['subject'] = 'oa '.$user['name'].$this->_userWorkflow['name'].'需要你审批';
            $arrMail['body'] = $user['name'].$this->_userWorkflow['name'].':'.$this->_userWorkflow['ext']['title'].'请登录oa审批(<a href="http://oa.rong360.com/site/login.html">oa.rong360.com</a>)';

        }
        //这里发邮件
        // $mq = new MessageQueue();
        // $ret = $mq->publishEmail($arrMail['to'],$arrMail['subject'],$arrMail['body']);
    }

    //获取当前节点适用的规则
    public function getNodeConfigByNodeTag($currentNodeTag)
    {
        $nodeConfigs = $this->_workflowConfig[$currentNodeTag];
        if(empty($nodeConfigs)){
            Yii::log('工作流节点错误，配置中不存在。user_workflow_id:'.$this->_userWorkflowId);
            throw new CHttpException(404,'节点配置错误');
        }

        if(is_array(reset($nodeConfigs))){
            if($this->_userWorkflow['obj_type'] == 'absence' || $this->_userWorkflow['obj_type'] == 'cancel_absence' || $this->_userWorkflow['obj_type'] == 'cancel_business'){
                $nodeConfig = $this->getAbsenceConfig($nodeConfigs);
            } 
        }else{
            $nodeConfig = $nodeConfigs;
        }

        if(empty($nodeConfig)){
            Yii::log('工作流节点错误，配置中不存在。user_workflow_id:'.$this->_userWorkflowId);
            throw new CHttpException(404,'节点配置错误');
        }
	if($nodeConfig['type'] == 'manage' && !empty($nodeConfig['maxlevel']) && $nodeConfig['level'] > $nodeConfig['maxlevel']){
            Yii::log('工作流节点错误，配置错误。workflow_id:'.$this->_userWorkflow['workflow_id']);
            throw new CHttpException(404,'节点配置错误');
        }
        if($nodeConfig['type'] == 'role' && empty($nodeConfig['rolename'])){
            Yii::log('工作流节点错误，配置错误。workflow_id:'.$this->_userWorkflow['workflow_id']);
            throw new CHttpException(404,'节点配置错误');
        }
        return $nodeConfig;
    }

    //获取请假的节点配置
    public function getAbsenceConfig($nodeConfigs)
    {
        $day_number = $this->_userWorkflow['ext']['day_number'];
        foreach($nodeConfigs as $nodeConfig){
            if(!empty($nodeConfig['minday']) && $day_number <= $nodeConfig['minday']){
                continue;
            }    
            if(!empty($nodeConfig['maxday']) && $day_number > $nodeConfig['maxday']){
                continue;
            }
            return $nodeConfig;
        } 
    }

    //获取审批流程的下一个审批人(通过部门的层级关系)
    public function getNextApprover()
    {
        $userId = $this->_userWorkflow['user_id'];

        $userModel = new User();
        //获取该流程的已审核人员
        $userTaskModel = new UserTask();
        $auditUserIds = $userTaskModel->getAuditUserIdByUwfId($this->_userWorkflowId,$this->_lastTask['workflow_step']);

        $sectionModel = new Section();
        //获得员工所在部门
        $section = $sectionModel->getSectionByUserId($userId);
        //如果部门leader是自己或者已经审批过的人,再找上级部门
        while($section['manager_id'] == $userId || in_array($section['manager_id'],$auditUserIds)){ 
            $section = $sectionModel->getItemByPk($section['parent_id']);
        }

        if(empty($section)){//说明没有下一级审批人了。
            throw new CHttpException(404,'当前审批人未到级别，但是没有下一级审批人');
        }
        $nextApprover = $userModel->getItemByPk($section['manager_id']);

        return $nextApprover;
    }

    //获取工作流的代理审批人
    public function getTrusteeUser($entrustUserId)
    {
        $sectionIds = array();

        //工作流发起人
        $authorId = $this->_userWorkflow['user_id'];
        $sectionModel = new Section();
        $sectionIds = $sectionModel->getAllSectionIdsByUserId($authorId);

        $trusteeUser = false;
        $entrust_audit_conf = WorkflowConfig::$entrust_audit_conf;
        $userWorkflow_type = $this->_userWorkflow['obj_type'];

        $userModel = new User();

        if(!empty($entrust_audit_conf[$authorId][$userWorkflow_type]) ) {

            $trustee_user_id = $entrust_audit_conf[$authorId][$userWorkflow_type];
            $trusteeUser = $userModel->getItemByPk($trustee_user_id);

        } else {


            //查看审批人是否有委托审批行为
            $model = new EntrustAudit();
            $entrustAudit = $model->getItemByEntrustUserIdAndObjType($entrustUserId,$this->_userWorkflow['obj_type'],$sectionIds);
            if(!empty($entrustAudit)){
                $trusteeUser = $userModel->getItemByPk($entrustAudit['trustee_user_id']);
            }

        }

        return $trusteeUser;

    }

    //上线单流程的提醒邮件
    public function onlineOrderEmail($auditList,$user,$deploy_id=0,$email)
    {
        $model = new AuditList();
        $ccMail = $model->getAllUserEmailByUserWorkflowId($this->_userWorkflowId);

        $arrMail['to'] = $email."@rong360.com";
        if( $deploy_id > 0){
            if($auditList['workflow_step'] == 'A' && $this->_userWorkflow['obj_type'] == 'online_order'){
                //上线前邮件提醒签字 
                $subject = $this->_userWorkflow['ext']['title']."项目上线需要你签字";
                $body = $user['name'].',oa系统(<a href="http://oa.rong360.com/site/login.html">oa.rong360.com</a>)有一个来自上线平台的上线单需要审批，'.$this->_userWorkflow['ext']['title'].'项目申请上线，上线后请确认。';

            }elseif($auditList['workflow_step'] == 'C' && $this->_userWorkflow['obj_type'] == 'online_order'){
                //上线后确认效果签字 
                $subject = $this->_userWorkflow['ext']['title']."项目已经上线，请确认效果并签字";
                $body = $user['name'].',oa系统(<a href="http://oa.rong360.com/site/login.html">oa.rong360.com</a>)有一个来自上线平台的上线单需要审批，'.$this->_userWorkflow['ext']['title'].'项目已经上线，请确认效果。';

  
           } 
        }else{
            if($auditList['workflow_step'] == 'A' && $this->_userWorkflow['obj_type'] == 'online_order'){
                //上线前邮件提醒签字 
                $subject = $this->_userWorkflow['ext']['title']."项目上线需要你签字";
                $body = $user['name'].',oa系统(<a href="http://oa.rong360.com/site/login.html">oa.rong360.com</a>)有一个上线单需要审批，'.$this->_userWorkflow['ext']['title'].'项目申请上线，上线后请确认。';

            }elseif($auditList['workflow_step'] == 'B' && $this->_userWorkflow['obj_type'] == 'online_order'){
                //让op上线操作的邮件 
                $ccMail[] = 'op@rong360.com';
                $ccMail[] = 'cto-all@rong360.com';
                $subject = $this->_userWorkflow['ext']['title']."项目申请上线,请操作";

                $body = $user['name'].',oa系统(<a href="http://oa.rong360.com/site/login.html">oa.rong360.com</a>)有一个上线单需要审批，'.$this->_userWorkflow['ext']['title'].'项目申请上线，上线后请确认。';

            }elseif($auditList['workflow_step'] == 'C' && $this->_userWorkflow['obj_type'] == 'online_order'){
                //上线后确认效果签字 
                $subject = $this->_userWorkflow['ext']['title']."项目已经上线，请确认效果并签字";
                $body = $user['name'].',oa系统(<a href="http://oa.rong360.com/site/login.html">oa.rong360.com</a>)有一个上线单需要审批，'.$this->_userWorkflow['ext']['title'].'项目已经上线，请确认效果。';
           } 

        }
        $arrMail['subject'] = $subject;
        $arrMail['body'] = $body;

        //这里发邮件
        // $mq = new MessageQueue();
        // $ret = $mq->publishEmail($arrMail['to'],$arrMail['subject'],$arrMail['body'],'',$ccMail);
    }

}
?>
