<?php
class AuditListService{
	
	public function createAuditList($userWorkflowId)
    {
        $auditList = Yii::app()->request->getParam('auditlist');
        $oa_user_id = Yii::app()->session['oa_user_id'];

        $userModel = new User();
        $taskModel = new UserTask();
        $model = new AuditList();
        $index = 0;


        
        foreach($auditList as $step => $row){

            if(empty($row)){
                continue;
            }

            foreach($row as $business_role => $val){

                if(empty($val)){
                    continue;
                }
    
                ++ $index;
		if($business_role == "op")
		{
		    $index = 1;
		}

                $name = $val;
                $name = str_replace(array("��", "|", "\n", "\t", "_"," "), ',', $name);

                if( strpos($name, ',') !== false ) {
                    
                    $rd_arr = array();
                    $rd_arr = explode(",", $name);
                    foreach($rd_arr as $rd_val) {

                        if(empty($rd_val)){
                            continue;
                        }

                        $is_add = $this->addRdQueueList($step, $business_role, $rd_val, $userWorkflowId, $index);
                        if(!$is_add){

                            continue;

                        }

                    }


                }else {

                    $is_add = $this->addRdQueueList($step, $business_role, $val, $userWorkflowId, $index); 

                    if(!$is_add){

                        continue;

                    }

                }

            }

        }
        $index = 0;
        foreach($auditList as $step => $row){

            if(empty($row)){
                continue;
            }

            foreach($row as $business_role => $val){

                if(empty($val) || $business_role == "ue" || $business_role == "rd_manger" || $business_role == "op"){
                    continue;
                }

                ++ $index;
                $name = $val;
                $name = str_replace(array("��", "|", "\n", "\t", "_"," "), ',', $name);

                if( strpos($name, ',') !== false ) {
                    
                    $rd_arr = array();
                    $rd_arr = explode(",", $name);
                    foreach($rd_arr as $rd_val) {

                        if(empty($rd_val)){
                            continue;
                        }
                        $users = $userModel->getItemByEmailOrName(trim($rd_val));
                        $user = reset($users);
                        if(empty($user)){
                           continue;
                        }

                        $this->addCStepQueueList($user['id'],$business_role,$index,$userWorkflowId);

                    }


                }else {
                        $users = $userModel->getItemByEmailOrName(trim($val));
                        $user = reset($users);
                        if(empty($user)){
                           continue;
                        }
                        $this->addCStepQueueList($user['id'],$business_role,$index,$userWorkflowId);

                }

            }

        }



    }

    public function addCStepQueueList($user_id,$business_role,$index,$userWorkflowId)
	{ 
        $model = new AuditList();
        $tmp = array();
        $tmp['pos'] = $index;
        $tmp['user_id'] = $user_id;
        $tmp['business_role'] = $business_role;
        $tmp['user_workflow_id'] = $userWorkflowId;
        $tmp['workflow_step'] = 'C';
        $tmp['create_time'] = time();
        $tmp['status'] = STATUS_VALID; 
        $model->save($tmp);
	}

    public function addRdQueueList($step, $business_role, $name, $userWorkflowId, $index) {

        $model = new AuditList();
        $userModel = new User();
        $taskModel = new UserTask();
        $oa_user_id = Yii::app()->session['oa_user_id'];
        $email = OAConfig::checkUserAccount($name);
        $users = $userModel->getItemByEmailOrName(trim($name));
        $user = reset($users);

        if( empty($user) ) {

            return false;

        }

        $tmp = array();
        $tmp['pos'] = $index;
        $tmp['user_id'] = $user['id'];
        $tmp['business_role'] = $business_role;
        $tmp['user_workflow_id'] = $userWorkflowId;
        $tmp['workflow_step'] = $step;
        $tmp['create_time'] = time();

        if($business_role == 'rd' && $user['id'] == $oa_user_id){

            $tmp['status'] = STATUS_INVALID;
            $row = array();
            $row['user_id'] = $user['id'];
            $row['user_workflow_id'] = $userWorkflowId;
            $row['audit_user_id'] = $user['id'];
            $row['workflow_step'] = $step;
            $row['audit_status']  = OA_TASK_TONGGUO;
            $row['audit_desc']    = 'rd';
            $row['create_time']   = $row['modify_time'] = time();
            $taskModel->save($row);

        }else{

            $tmp['status'] = STATUS_VALID; 

        }

        $model->save($tmp);

    }

    public function exceUrl($messagePost,$url)
	{
		$ch = curl_init();                                                                                                           
	    curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $messagePost);
		$data = curl_exec( $ch);
		curl_close($ch);
		return $data;
	}

    public function onLinePlatformAPI($obj_type,$deploy_id,$deploy_status)
	{
     	if($obj_type == "online_order"  && $deploy_id > 0)
		{
			$tokenstr = $deploy_id."Rong360";
			$md5value = md5($tokenstr);
			$message = array(
			    'deploy_id'=>urlencode($deploy_id),
			    'token' =>urlencode($md5value),
			    'deploy_status'=>$deploy_status
			 );
			$messagePost = json_encode($message);
		        //$url = 'http://10.0.2.189:8000/deploy/api/status/';
			$url = 'http://thor.rong360.com/deploy/api/status/';
			$datap = $this->exceUrl($messagePost,$url);
			return $datap;
		 }else{
			 return false;
		 }
	}
	
}
