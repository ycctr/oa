<?php
/**
 * 团建服务
 * 
 **/
class HomeService {

    const PAGENO = "40";
    public $dinnerModel;
    public $expressModel;

    //以下两个配置要key要一一对应
    public $officeConf = array(//订餐办公地
        1=>'互联网金融中心', 
        2=>'时代网络',
    );

    //快递ems的办公地点
    public $officeEmsConf = array(
        1=>'互联网金融中心', 
        2=>'时代网络',
        3=>'昌宁大厦',
    );

    public $IPConfig = array(
        '1' => array(
            array(
                'min' => "10.0.3.2",
                'max' => "10.0.3.254",
            ),
            array(
                'min' => "10.0.128.2",
                'max' => "10.0.131.254",
            ),
            array(
                'min' => "10.0.8.2",
                'max' => "10.0.11.254",
            )
        ),

        '2' => array(
            array(
                'min' => "10.0.12.1",
                'max' => "10.0.12.254",
            ),
        ),
        
        '3' => array(
            array(
                'min' => "10.1.3.2",
                'max' => "10.1.3.254",
            ),
            array(
                'min' => "10.1.128.2",
                'max' => "10.1.131.254",
            ),
            array(
                'min' => "10.1.8.2",
                'max' => "10.1.11.254",
            )
        ),
    
    );
    

    public $goodsConf = array(
        '1'=>'文件合同',
        '2'=>'物料印刷品',
        '3'=>'礼品',
        '4'=>'其他',
    );

    public $expressConf = array(
        1=>'顺丰',
        //2=>'申通',
        3=>'中通'
    );
    public $expressAllConf = array(
        1=>'顺丰',
        2=>'申通',
        3=>'中通'
    );


    public function __construct() {

        $this -> dinnerModel  = new Dinner();
        $this -> expressModel = new Express();

    }

    //根据IP获取办公区,默认金融中心
    public function getByIPAddress($targetIp) {
        $targetNo  = ip2long($targetIp);

        $IPConfig   = $this->IPConfig;

        foreach($IPConfig as $index => $config) {

            foreach($config as $ip) {

                $min = ip2long($ip['min']);
                $max = ip2long($ip['max']);

                if( $min <= $targetNo && $max >= $targetNo ) {

                    return $index;

                }

            }


        }

        return 0;

    }

    public function addDinner($office) {
        
        $office = Yii::app()->request->getParam('office', 1);
        $oa_user_id = intval(Yii::app()->session['oa_user_id']);
        $time   = time();
        $date   = date("Y-m-d", $time);

        $data = $this->dinnerModel->getUserByUserId($oa_user_id);

        //判断用户当天是否订过餐
        if(!empty($data) ) {

          $office = $data[0]['office'];
          $data[0]['office'] = $this->officeConf[$office];
          $data[0]['create_time'] = date("H:i:s");
          return $data[0];        

        }


        $addData = array();
        // $addData['id']          = null; 
        $addData['user_id']     = $oa_user_id; 
        $addData['office']      = $office; 
        $addData['date']        = $date; 
        $addData['create_time'] = $time; 

        $isAdd = $this->dinnerModel->save($addData);

        if(empty($isAdd)) {

            return false;

        }

        //ajax返回值
        $userModel = new User();
        $userData  = $userModel->getItemByPk($oa_user_id);
        $addData['id_card'] = $userData['id_card'];
        $addData['name']    = $userData['name'];
        $addData['email']   = $userData['email'];
        $addData['office']  = $this->officeConf[$office];
        $addData['create_time'] = date("H:i",$time); 

        return $addData;

    } 

    public function exportDinner() {

        $data = $this->dinnerModel-> getUserByDate(0);

        $rtnData = array();
        foreach($data as $key=>$value) {
            $tmpArr = array();

            $tmpArr['id_card'] = $value['id_card']; 
            $tmpArr['name']    = $value['name'];
            $tmpArr['email']   = $value['email'];
            $office = $value['office'];
            $tmpArr['office']  = $this->officeConf[$office];
            $tmpArr['date']    = date("H:i", $value['create_time']);
            $rtnData[] = $tmpArr;

        }

        return $rtnData;

    }

    /**
     * 获取自己快递单
     **/
    public function getSelfEMS($startPage=1) {

        $user_id = intval(Yii::app()->session['oa_user_id']);
        $homeService = new HomeService();

        $start = 0;
        if($startPage >= 1 ) {

            $start = ($startPage-1) * self::PAGENO;

        }

        $selfEMS = $this->expressModel->getSelfExpress($user_id, $start, self::PAGENO);

        if(empty($selfEMS) ) {

            return false;

        }

        return $selfEMS;

    }   

}

