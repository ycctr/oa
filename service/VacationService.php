<?php
class VacationService
{
    //查看员工该时间段的工作日是否都是请假状态
    //是 返回true,否则false
    public function isVacation($user_id,$start_day,$end_day,$type="")
    {
        if(empty($user_id) || empty($start_day) || empty($end_day)){
            return false;
        }
        $model = new Absence();
        $day = date('Ymd',strtotime($start_day));
        while($day < date('Ymd',strtotime("$end_day +1 day")))
        {
            if($type != VACATION_CHUCHAI) {

                if($this->isHoliday($day)){
                    $day = date('Ymd',strtotime("$day +1 day"));
                    continue;
                } 

            }

            if(!$model->isVacation($user_id,$day,$type)){//该日没有请假记录
                return false; 
            }

            $day = date('Ymd',strtotime("$day +1 day"));
        }
        return true;
    }

    //计算是否节假日
    public static function isHoliday($day)
    {
        $holiday = array();
        $day = date('Ymd',strtotime($day));

        if(isset($holiday[$day])){
            if($holiday[$day] == 1){
                return true;
            }else{
                return false;
            } 
        } 
        $week = date('w',strtotime($day));
        if($week == 0 || $week == 6){
            return true;
        }
        return false;
    }

    //销假后更改考勤记录
    //只对已经休过的假进行销假处理，
    public function updateCheckInOutByCancelAbsence($obj_id)
    {
        $model = new CheckInOut(); 
        $absenceModel = new Absence();
        $absence = $absenceModel->getItemByPk($obj_id);
        if($absence['is_cancel'] == 1){
            $cancelAbsence = $absence;
        }else{
            $cancelAbsenceModel = new CancelAbsence();
            $cancelAbsence = $cancelAbsenceModel->getItemByPk($obj_id);
        }
        // $cancelAbsence = $absenceModel->getItemByPk($obj_id);
        $start_day = date('Y-m-d',$cancelAbsence['start_time']);
        $end_day = date('Y-m-d',$cancelAbsence['end_time']);

        $userId = $cancelAbsence['user_id'];  
        $day = date('Ymd',strtotime($start_day));
        while($day < date('Ymd',strtotime("$end_day +1 day")))
        {
            if(strtotime($day) >= strtotime(date('Ymd'))){//时间大于今天(包含今天)的销假不处理
                return;
            }
            if($this->isHoliday($day)){
                $day = date('Ymd',strtotime("$day +1 day"));
                continue;
            } 
            if(!$absenceModel->isVacation($userId,$day)){//该日没有请假记录
                $day = date('Ymd',strtotime("$day +1 day"));
                continue; 
            }else{
                $checkInOut = $model->getItemByUserIdDate($userId,$day);
                if(!empty($checkInOut)){
                    $checkInOut['absenteeism_type'] = SYSTEM_XIAOJIA;
                    if(!$model->save($checkInOut)){
                        throw new CHttpException(404,'打卡记录设置销假失败'); 
                    }
                }
            }
            $day = date('Ymd',strtotime("$day +1 day"));
        }
        return;
    }

    //销出差后更改考勤记录
    //只对已经休过的假进行销假处理，
    public function updateCheckInOutByCancelBusiness($obj_id)
    {
        $model = new CheckInOut(); 
        $absenceModel = new Absence();
        $cancelBusinessModel = new CancelBusiness();
        $cancelBusiness = $cancelBusinessModel->getItemByPk($obj_id);
        $start_day = date('Y-m-d',$cancelBusiness['start_time']);
        $end_day = date('Y-m-d',$cancelBusiness['end_time']);

        $userId = $cancelBusiness['user_id'];  
        $day = date('Ymd',strtotime($start_day));
        while($day < date('Ymd',strtotime("$end_day +1 day")))
        {
            if(strtotime($day) >= strtotime(date('Ymd'))){//时间大于今天(包含今天)的销假不处理
                return;
            }

            if(!$absenceModel->isVacation($userId,$day,VACATION_CHUCHAI)){//该日没有出差记录

                $day = date('Ymd',strtotime("$day +1 day"));
                continue; 

            }else{

                $checkInOut = $model->getItemByUserIdDate($userId,$day);

                if($checkInOut['absenteeism_type'] == SYSTEM_CHUCHA ){

                    $checkInOut['absenteeism_type'] = SYSTEM_XIAOCHUCHA;

                    if(!$model->save($checkInOut)){
                        throw new CHttpException(404,'打卡记录设置销假失败'); 
                    }

                }
            }
            $day = date('Ymd',strtotime("$day +1 day"));
        }
        return;
    }
    //补假后更改考勤记录
    public function updateCheckInOutByAbsence($obj_id)
    {
        $model = new CheckInOut(); 
        $absenceModel = new Absence();
        $absence = $absenceModel->getItemByPk($obj_id);
        $start_day = date('Y-m-d',$absence['start_time']);
        $end_day = date('Y-m-d',$absence['end_time']);

        $userId = $absence['user_id'];  
        $day = date('Ymd',strtotime($start_day));
        while($day < date('Ymd',strtotime("$end_day +1 day")))
        {
            if(strtotime($day) >= strtotime(date('Ymd'))){//时间大于今天(包含今天)的销假不处理
                return;
            }
            if($this->isHoliday($day)){
                $day = date('Ymd',strtotime("$day +1 day"));
                continue;
            } 
                
            //更改考勤记录状态为请假
            $checkInOut = $model->getItemByUserIdDate($userId,$day);
            if(!empty($checkInOut)){


                if($absence['type'] == VACATION_CHUCHAI && $checkInOut['absenteeism_type'] != SYSTEM_QINGJIA ) { //是出差没标记请假

                    $checkInOut['absenteeism_type'] = SYSTEM_CHUCHA;

                } elseif($absence['type'] == VACATION_YINGONGWAICHU && $checkInOut['absenteeism_type'] != SYSTEM_QINGJIA ) { //是因公外出没标记请假

                    $checkInOut['absenteeism_type'] = SYSTEM_YINGONGWAICHU;

                } else { //请假 

                    $checkInOut['absenteeism_type'] = SYSTEM_QINGJIA;

                }

                if(!$model->save($checkInOut)){
                    throw new CHttpException(404,'打卡记录设置请假失败'); 
                }
            }
            
            $day = date('Ymd',strtotime("$day +1 day"));
        }
        return;
    }


    //判断员工某日的打卡状态（请假、销假、缺勤）
    public function getAttendanceStatus($userId,$date)
    {
        if(empty($userId) || empty($date)){
            return false;
        } 
        $ret = array('status' => 0,'absence_type' => 0);

        $absenceModel = new Absence();
        $absence = $absenceModel->getItemByUserIdAndDate($userId,$date);

        $cancelAbsenceModel = new CancelAbsence();
        $cancelAbsence = $cancelAbsenceModel->getItemByUserIdAndDate($userId,$date);

        if($absence['is_cancel'] == 1){
            $cancelAbsence = $absence;
        }

        //出差
        $chuchai = $absenceModel->getItemByUserIdAndDate($userId,$date,VACATION_CHUCHAI);

        //因公外出
        $yingongwaichu = $absenceModel->getItemByUserIdAndDate($userId,$date,VACATION_YINGONGWAICHU);


        //销出差
        $cancelBusinessModel = new CancelBusiness();
        $cancelBusiness = $cancelBusinessModel->getItemByUserIdAndDate($userId,$date);

        if(!empty($chuchai)){ //出差
                $ret['status'] = SYSTEM_CHUCHA;
                $ret['absence_type'] = $absence['type'];
            if(!empty($cancelBusiness)) {

                $ret['status'] = SYSTEM_XIAOCHUCHA;
                $ret['absence_type'] = "";
            }
        }

        if(!empty($yingongwaichu)){
            $ret['status'] = SYSTEM_YINGONGWAICHU;
            $ret['absence_type'] = $absence['type'];
        }elseif(!empty($cancelAbsence) && !empty($absence)){ //存在请假和销假情况下
            if(empty($chuchai)){ //出差
                $ret['status'] = SYSTEM_XIAOJIA;
                $ret['absence_type'] = "";
            }
        }elseif(!empty($absence)){ //请假
            $ret['status'] = SYSTEM_QINGJIA;
            $ret['absence_type'] = $absence['type'];
        }

        return $ret;
    }

    //请假中当做年假、病假扣除的部分插入vacation_record
    public function createHolidayRecordByAbsence($absence)
    {
        if(empty($absence)){
            return false;
        } 
        $model = new VacationRecord();
        $sickLeaveNum = $model->getHolidayQuantity($absence['user_id'],VACATION_BINGJIA);
        $annualLeaveNum = $model->getHolidayQuantity($absence['user_id'],VACATION_NIANJIA);
        $nowMonth = date("m",time());
        $absMonth = date("m",$absence['end_time']);
        if($nowMonth!=$absMonth){
            //判断员工年假天数
            $userModel = new User();
            $work_date = $userModel->getItemByPk($absence['user_id'])['work_date'];
            $nianPerYear = $this->getNianjiaNumber($work_date);
            $annualLeaveNumAdv =  $annualLeaveNum + round($nianPerYear/12,2);
            $sickLeaveNumAdv = $sickLeaveNum + round(5/12,2);
            if($absMonth == 12){
                if($nianPerYear == 10){
                    $sickLeaveNumAdv -= 0.04;
                }
                $annualLeaveNumAdv += 0.04;
            }
        }else{
            $sickLeaveNumAdv = $sickLeaveNum;
            $annualLeaveNumAdv = $annualLeaveNum;
        }

        if($absence['type'] == VACATION_BINGJIA){//病假
    
            $this->createHolidayRecordBySick($absence,$sickLeaveNumAdv,$annualLeaveNumAdv);

        }elseif($absence['type'] == VACATION_SHIJIA || $absence['type'] == VACATION_NIANJIA){
            
            //请年假、事假的直接扣除年假
            $this->createHolidayRecordByAnnual($absence,$annualLeaveNumAdv);

        }else {

            $this->createElsejia($absence);

        }
    } 

     //请假中当做年假、病假扣除的部分插入vacation_record，一次性修正脚本使用
    public function createHolidayRecordByAbsence2($absence)
    {
        if(empty($absence)){
            return false;
        } 
        $model = new VacationRecord();
        $sickLeaveNum = $model->getHolidayQuantity($absence['user_id'],VACATION_BINGJIA);
        $annualLeaveNum = $model->getHolidayQuantity($absence['user_id'],VACATION_NIANJIA);
        // $nowMonth = date("m",$absence['create_time']);
        $absMonth = date("m",$absence['end_time']);
        if($absMonth == 1 && $model->getDecNianOrBingByUser($absence['user_id'],VACATION_NIANJIA) && $model->getDecNianOrBingByUser($absence['user_id'],VACATION_BINGJIA)){
            //判断员工年假天数
            $userModel = new User();
            $work_date = $userModel->getItemByPk($absence['user_id'])['work_date'];
            $nianPerYear = $this->getNianjiaNumber($work_date);
            $annualLeaveNumAdv =  $annualLeaveNum + round($nianPerYear/12,2);
            $sickLeaveNumAdv = $sickLeaveNum + round(5/12,2);
            if($absMonth == 12){
                if($nianPerYear == 10){
                    $sickLeaveNumAdv -= 0.04;
                }
                $annualLeaveNumAdv += 0.04;
            }
        }else{
            $sickLeaveNumAdv = $sickLeaveNum;
            $annualLeaveNumAdv = $annualLeaveNum;
        }

        if($absence['type'] == VACATION_BINGJIA){//病假
    
            $this->createHolidayRecordBySick($absence,$sickLeaveNumAdv,$annualLeaveNumAdv);

        }elseif($absence['type'] == VACATION_SHIJIA || $absence['type'] == VACATION_NIANJIA){
            
            //请年假、事假的直接扣除年假
            $this->createHolidayRecordByAnnual($absence,$annualLeaveNumAdv);

        }else {

            $this->createElsejia($absence);

        }
    } 

     //根据工龄给出员工年假天数
    public function getNianjiaNumber($work_date)
    {
        $num = 0;
        $year = date('Y',time()) - date('Y',strtotime($work_date));

        $month = date('m',time()) - date('m',strtotime($work_date));

        $monthTotal = $year*12 + $month;

        if($monthTotal > 20*12){
            $num = 15;
        }else{
            $num = 10;
        }
        return $num;
    }

    //删除假期记录 todo
    //如果当天请整天假，消半天假 怎么处理？
    //如果当天请整天假期，但是有两条记录，半天病假，半天年假。我只销假半天 怎么处理？
    //策略 销假时 把销假日期后的vacation_record记录全部删除，然后销假日期后的请假重新生成vacation_record记录
    public function delHolidayRecordByCancelAbsence($cancelAbsence)
    {
        if(empty($cancelAbsence)){
            return false;
        }     

        $userWorkflowModel = new UserWorkFlow();

        //获取销假对应的请假单工作流，以获得请假单通过审批时间
        $userWorkflowId= $userWorkflowModel->getUserWorkflowId($cancelAbsence['id'], 'absence');
        $userWorkflow = $userWorkflowModel->getItemByPk($userWorkflowId);
        

        $model = new VacationRecord();
        
        //删除审批通过日期在该销假单对应请假单之后的所有休假记录，以便重新生成休假记录，避免扣假类型不对
        $model->delRowByUserIdAndDate($cancelAbsence['user_id'],$userWorkflow['modify_time']);

        //需要将之后请的假期，没有使用年假的补上使用年假记录
        $this->additionalVacationRecord($cancelAbsence['user_id'],$userWorkflow['modify_time']);
    }

    //删除出差记录 todo
    //如果当天出差，消半天出差 怎么处理？
    //如果当天出整天出差
    public function delHolidayRecordByCancelBusiness($cancelBusiness)
    {
        if(empty($cancelBusiness)){
            return false;
        }     

        $end_day = date('Y-m-d',$cancelBusiness['end_time']);
        $day = date('Ymd',$cancelBusiness['start_time']);
        $model = new VacationRecord();
        
        $start_time = $cancelBusiness['start_time'];
        $end_time   = $cancelBusiness['end_time'];

        //删除销假日期后的所有休假记录，以便重新生成休假记录，避免扣假类型不对
        $model->delChuChaiRowByUserIdAndDate($cancelBusiness['user_id'], $start_time, $end_time);

    }

    //销假后，将销假后的请假因为没有年假而没有使用年假的补充上年假记录
    public function additionalVacationRecord($userId,$audit_time)
    {
        if(empty($userId) || empty($audit_time)){
            return false;
        } 

        $model = new Absence();

        $rows = $model->getItemByUserIdAfterDate($userId,$audit_time);
        
        if(!empty($rows)){
            foreach($rows as $absence){
                $this->createHolidayRecordByAbsence($absence);
            } 
        }
    }

    //对11月25日之后的假期重新生成vr记录，一次性修正脚本使用
    public function additionalVacationRecord2($userId,$audit_time)
    {
        if(empty($userId) || empty($audit_time)){
            return false;
        } 

        $model = new Absence();

        $rows = $model->getItemByUserIdAfterDate2($userId,$audit_time);
        
        if(!empty($rows)){
            foreach($rows as $absence){
                $this->createHolidayRecordByAbsence2($absence);
            } 
        }
    }

    //对请事假、年假的生成扣除年假记录
    public function createHolidayRecordByAnnual($absence,$annualLeaveNum)
    {

        $end_day = date('Y-m-d',$absence['end_time']);

        $userId = $absence['user_id']; 
        $day = date('Ymd',$absence['start_time']);
        $model = new VacationRecord();
        while($day < date('Ymd',strtotime("$end_day +1 day")))
        {
            $num = $this->getDayNumOfDate($absence,$day);
            if($num == 0){
                $day = date('Ymd',strtotime("$day +1 day"));
                continue;
            }
            //为了应对历史错误数据，有人重复请假
            $vacation = $model->getVacationItemByUserIdAndDate($userId,$day);
            if(!empty($vacation)){
                $day = date('Ymd',strtotime("$day +1 day"));
                continue; 
            }

            $arrData = array();
            $arrData['user_id'] = $userId;
            $arrData['absence_type'] = VACATION_NIANJIA;
            $arrData['date'] = date('Y-m-d',strtotime($day));
            $arrData['absence_id'] = $absence['id'];
            $arrData['create_time'] = time();
            $arrData['status'] = STATUS_VALID;

            if($annualLeaveNum >= 0.5){

                if($num <= $annualLeaveNum){

                    //直接插入数据
                    $annualLeaveNum -= $num;
                    $arrData['day_num'] = 0-$num;
                    $model->duplicateUpdate($arrData);
                    $num -= $num;

                }else{

                    //插入半天数据 
                    $annualLeaveNum -= 0.5;
                    $num -= 0.5;
                    $arrData['day_num'] = -0.5;
                    $model->duplicateUpdate($arrData);


                }

                if($num > 0) {

                    $arrData['absence_type'] = VACATION_SHIJIA;
                    $arrData['day_num'] = -$num;
                    $model->duplicateUpdate($arrData);

                }

            } else {//请事假

                $arrData['absence_type'] = VACATION_SHIJIA;
                $arrData['day_num'] = 0-$num;
                $model->duplicateUpdate($arrData);
                $num -= $num;
            
            }


        
            $day = date('Ymd',strtotime("$day +1 day"));
        }
    }

    //对请病假的生成扣除假期记录(先扣病假、病假不够再扣年假)
    public function createHolidayRecordBySick($absence,$sickLeaveNum,$annualLeaveNum)
    {
        //先抵扣病假，病假不够再抵扣年假 
        $end_day = date('Y-m-d',$absence['end_time']);

        $userId = $absence['user_id'];  
        $day = date('Ymd',$absence['start_time']);
        $model = new VacationRecord();
        while($day < date('Ymd',strtotime("$end_day +1 day")))
        {

            $num = $this->getDayNumOfDate($absence,$day);
            if($num == 0){
                $day = date('Ymd',strtotime("$day +1 day"));
                continue;
            }

            //为了应对历史错误数据，有人重复请假
            $vacation = $model->getVacationItemByUserIdAndDate($userId,$day);
            if(!empty($vacation)){
                $day = date('Ymd',strtotime("$day +1 day"));
                continue; 
            }

            $arrData = array();
            $arrData['user_id'] = $userId;
            $arrData['date'] = date('Y-m-d',strtotime($day));
            $arrData['create_time'] = time();
            $arrData['absence_id'] = $absence['id'];
            $arrData['status'] = STATUS_VALID;
            
            //只有半天病假
            if($sickLeaveNum >= 0.5 && $sickLeaveNum < 1){

                $arrData['absence_type'] = VACATION_BINGJIA;

                //插入半天病假 
                $sickLeaveNum -= 0.5;
                $arrData['day_num'] = -0.5;
                $model->duplicateUpdate($arrData);
                $num -= 0.5;

                //已经扣完病假需要年假
                if($num > 0 && $annualLeaveNum >= 0.5 )  {

                    $arrData['absence_type'] = VACATION_NIANJIA;

                    //扣半天年假
                    $annualLeaveNum -= 0.5;
                    $arrData['day_num'] = -0.5;
                    $model->duplicateUpdate($arrData);
                    $num -= 0.5;


                }else if($num > 0)  {//扣病休假
                    
                    $arrData['absence_type'] = VACATION_BINGXIUJIA; 
                    $arrData['day_num'] = -0.5;
                    $model->duplicateUpdate($arrData);
                    $num -= 0.5;

                }

                $day = date('Ymd',strtotime("$day +1 day"));
                continue;

            }

            //足够一天病假
            if($sickLeaveNum >= 1){//有病假，先扣除病假

                $arrData['absence_type'] = VACATION_BINGJIA;
                //生成扣除1病假的记录
                $sickLeaveNum -= $num;
                $arrData['day_num'] = 0-$num;
                $model->duplicateUpdate($arrData);
                $num -= $num;
                //可直接退出、进入下个循环
                $day = date('Ymd',strtotime("$day +1 day"));
                continue;


            } 

            //病假不够，扣年假
            if($num > 0 && $annualLeaveNum >= 0.5){//如果病假不够，有年假扣除年假

                //生成扣除年假的记录
                $arrData['absence_type'] = VACATION_NIANJIA;
                if($num <= $annualLeaveNum){

                    //直接插入数据
                    $annualLeaveNum -= $num;
                    $arrData['day_num'] = 0-$num;
                    $num -= $num;
                    $model->duplicateUpdate($arrData);

                }else{

                    //插入半天数据 
                    $annualLeaveNum -= 0.5;
                    $arrData['day_num'] = -0.5;
                    $num -= 0.5;
                    $model->duplicateUpdate($arrData);


                }

                //年假不够一天
                if($num >0) {

                    $arrData['absence_type'] = VACATION_BINGXIUJIA; 
                    $arrData['day_num'] = -0.5;
                    $model->duplicateUpdate($arrData);

                }

                $day = date('Ymd',strtotime("$day +1 day"));
                continue;

            }


            //没有年假和病假
            $arrData['absence_type'] = VACATION_BINGXIUJIA; 
            $arrData['day_num'] = -$num;
            $model->duplicateUpdate($arrData);
            $day = date('Ymd',strtotime("$day +1 day"));

        }
    }


    //非病假和年假的其他假期
    public function createElsejia($absence)
    {

        $end_day = date('Y-m-d',$absence['end_time']);

        $userId = $absence['user_id']; 
        $day = date('Ymd',$absence['start_time']);
        $model = new VacationRecord();

        while($day < date('Ymd',strtotime("$end_day +1 day"))) {

            $num = $this->getDayNumOfDate($absence,$day);

            if($num == 0){
                $day = date('Ymd',strtotime("$day +1 day"));
                continue;
            }
            
            //为了应对历史错误数据，有人重复请假
            $vacation = $model->getVacationItemByUserIdAndDate($userId,$day);
            if(!empty($vacation)){
                $day = date('Ymd',strtotime("$day +1 day"));
                continue; 
            }

            $arrData = array();
            $arrData['user_id'] = $userId;
            $arrData['absence_type'] = $absence['type'];
            $arrData['date'] = date('Y-m-d',strtotime($day));
            $arrData['absence_id'] = $absence['id'];
            $arrData['create_time'] = time();
            $arrData['status'] = STATUS_VALID;


            if($num == 1){

                //直接插入数据
                $arrData['day_num'] = 0-$num;
                $model->duplicateUpdate($arrData);
                $num -= $num;

            }else{

                //插入半天数据 
                $num -= 0.5;
                $arrData['day_num'] = -0.5;
                $model->duplicateUpdate($arrData);


            }

            $day = date('Ymd',strtotime("$day +1 day"));
        }
    }


    //计算某日请假是整天还是半天
    public function getAbsenceNumOfDay($absence,$day)
    {
        $num = 0;
        if($this->isHoliday($day)){
            return $num;
        } 
        $day = date('Ymd',strtotime($day));
        //计算当天请假是半天还是1天
        if($day == date('Ymd',$absence['start_time']) && $day == date('Ymd',$absence['end_time'])){
            $sHour = date('G',$absence['start_time']);
            $eHour = date('G',$absence['end_time']);
            if($sHour == $eHour){
                $num = 0; 
            }elseif(($eHour - $sHour) == 9){
                $num = 1;
            }else{
                $num = 0.5;
            }
        }elseif($day == date('Ymd',$absence['start_time'])){
            $hour = date('G',$absence['start_time']);
            if($hour == 9){
                $num = 1;
            }elseif($hour == 12){
                $num = 0.5;
            } 
        }elseif($day == date('Ymd',$absence['end_time'])){
            $hour = date('G',$absence['end_time']);
            if($hour == 12){
                $num = 0.5;
            }elseif($hour == 18){
                $num = 1;
            } 
        }else{
            $num = 1;
        }
        return $num;
    }

    //根据请假销假记录判断某日请假是整天、半天
    public function getDayNumOfDate($absence,$day)
    {
        $absenceDayNum = $this->getAbsenceNumOfDay($absence,$day);    

        //获取销假记录，看当天有无销假记录
        $model = new CancelAbsence();
        $cancelAbsence = $model->getItemByUserIdAndDate($absence['user_id'],$day);

        $cancelAbsenceDayNum = 0;
        if(!empty($cancelAbsence)){
            $cancelAbsenceDayNum = $this->getAbsenceNumOfDay($cancelAbsence,$day);        
        }

        $num = $absenceDayNum - $cancelAbsenceDayNum;
        
        return $num > 0 ? $num : 0;
    }

    //整理考勤,将数据整理成以user_id、date为key的数组
    public function finishingVacationToArray($list)
    {
        if(empty($list)){
            return array();
        }    
        $ret = array();
        foreach($list as $item){
            $date = date('Y-m-d',strtotime($item['date']));
            if(isset($ret[$item['user_id']][$date])){
                if(is_array($ret[$item['user_id']][$date])){
                    $ret[$item['user_id']][$date][] = $item;
                }else{
                    $ret[$item['user_id']][$date] = array(
                        $ret[$item['user_id']][$date],    
                        $item
                    );
                }
            }else{
                $ret[$item['user_id']][$date] = $item;
            }
        }
        return $ret;
    }

    //根据请假记录，统计出各种请假供多少天
    public function statisticsVacationRecord($vacationRecords,&$checkInOuts)
    {
        $ret = array();
        if(!is_array($vacationRecords)){
            return $ret; 
        }
        foreach($vacationRecords as $user_id => $vacations){
            if(empty($vacations)){
                continue;
            }
            foreach($vacations as $date => $item){
                //item是单条记录的情况
                if(isset($item['day_num'])){
                    $ret[$user_id][$item['absence_type']] += $item['day_num']; 
                    if($item['day_num'] == -1){
                        unset($checkInOuts[$user_id][$date]);
                    }else{
                        $checkInOuts[$user_id][$date]['day_num'] += $item['day_num'];
                    }
                }else{//item是数组的情况
                    foreach($item as $one){
                        $ret[$user_id][$one['absence_type']] += $one['day_num']; 
                        if($one['day_num'] == -1){
                            unset($checkInOuts[$user_id][$date]);
                        }else{
                            $checkInOuts[$user_id][$date]['day_num'] += $one['day_num'];
                        }
                    }
                }
            }
        }
        return $ret;
    }

    //将日期整理成工作日和节假日
    public function workingDay($startDay,$endDay)
    {
        $ret = array();
        if(empty($startDay)||empty($endDay)){
            return $ret; 
        }
        for($i=$startDay;$i <= $endDay;$i=date('Y-m-d',strtotime("+1 day",strtotime($i)))){
            $ret[$i] = !$this->isHoliday($i);
        }
        return $ret;
    }

}

