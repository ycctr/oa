<?php
set_time_limit(0);
ini_set('memory_limit', -1);

$strDir = dirname(__FILE__);

require_once('/home/rong/www/config/global.define.php');
//环境相关配置
require_once($strDir . '/../../common/shared/rongconfig/rong360.conf.php');
require_once($strDir . '/../config/define.php');

//应用相关配置
require_once($strDir . '/../../main/config/define.php');


//框架相关
require_once($strDir . '/../../framework/yii.php');
$config = $strDir . '/../batch/config/yii_app_config.php';
$app = Yii::createConsoleApplication($config);

// automatically send every new message to available log routes
Yii::getLogger()->autoFlush = 1;
// when sending a message to log routes, also notify them to dump the message
// into the corresponding persistent storage (e.g. DB, email)
Yii::getLogger()->autoDump = true;

$app->run();
