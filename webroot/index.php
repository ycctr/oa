<?php

$strDir = dirname(__FILE__);

// require_once('/home/rong/www/config/global.define.php');
//加载通用配置
define('WWW_PATH', '/home/aixin/sites/oa');
define('CONFIG_PATH', WWW_PATH . '/config');

//环境相关配置
// require_once($strDir . '/../common/aixin.conf.php');
require_once($strDir . '/../common/shared/rongconfig/aixin.conf.php');

//应用相关配置
require_once($strDir . '/../config/define.php');

//框架相关
require_once($strDir . '/../framework/yii.php');

$config = $strDir . '/../config/yii_app_config.php';


Yii::createWebApplication($config)->run();

