var check_loading = {
    init: function(path) {
        document.write("<div id='check_div_over' style='display: none;position: absolute;top: 0;left: 0;width: 100%;height: 100%;background-color: #f5f5f5;opacity:0.5;filter:alpha(opacity=40); z-index: 1000;'><\/div><div id='check_div_layout' style='display: none; position: absolute; top: 60%; left: 40%; width: 20%; height: 20%; z-index: 1001; text-align:center;'><img src='"+path+"' \/><\/div>");

        window.onscroll = function(){
            var lazyheight = document.documentElement.scrollTop || document.body.scrollTop; 
            var second_top = lazyheight + 250 + "px";
            document.getElementById("check_div_over").style.top = lazyheight+"px";
            document.getElementById("check_div_layout").style.top = second_top;
        }

    },

    hide: function() {

        document.getElementById("check_div_over").style.display="none"; 
        document.getElementById("check_div_layout").style.display="none";
        document.getElementById("check_div_layout").style.top = "50%";

    },

    show: function() {
        
        document.getElementById("check_div_over").style.display="block"; 
        document.getElementById("check_div_layout").style.display="block";

    }


};


check_loading.init("/static/images/loading_demo.gif");

