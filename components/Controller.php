<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */

class Controller extends CController
{

    protected $right_list;
    /*
     * 保留空方法
     */
    public function actionIndex()
    {

    }

    public function afterAction($action)
    {
        // echo "afterAction";
    }

    public function beforeAction($action)
    {
        // RongSessionHandler::apply(); //使用基于redis缓存的session存储。

        //禁止浏览器缓存
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        //获取controller和action
        $actionName = strtolower($action->id);
        $controllerName = strtolower($this->id);

        Yii::app()->params['control'] = $this->getId();
        Yii::app()->params['action']  = $this->action->id;

        //api接口不用登陆
        if ( in_array( $controllerName, array('api') ) ) {
            return true;
        }
        //Yii::app()->session['oa_user_id'] = 7;
        $oa_user_id = intval(Yii::app()->session['oa_user_id']);

        $userModel = new User();
        $user = $userModel->getItemByPk($oa_user_id);
        if(!empty($user)){
            Yii::app()->params['user'] = $user;
            Yii::app()->params['username'] = $user['name'];
        }
        if($oa_user_id !== 0)
        {
            $id_card = strtolower($user['id_card']);
            if(strpos($id_card,'sx') !== false){ //实习生没有查看年假权限
                Yii::app()->params['nianjia'] = false;
            }else{
                Yii::app()->params['nianjia'] = true;
            }

            //团建
            $teamService = new TeamBuildingService();
            $is_manger = $teamService -> isSectionBuilding($oa_user_id);
            Yii::app()->params['section_flag'] = $is_manger;

            //高级团建
            $is_senior = $teamService -> isSeniorTeambuilding($oa_user_id);

            if($is_senior) {

                Yii::app()->params['is_senior'] = true;

            }

            if($user['level'] > 0){
                Yii::app()->params['has_team'] = true;
            }

            //激励经费
            $encourageService = new TeamEncourageService();
            $is_encourage = $encourageService -> isSectionEncourage($oa_user_id);
            Yii::app()->params['encourage_flag'] = $is_encourage;


            $right = new Right(DBModel::MODE_RA);
            $right_list = $right->getPrivilegeByUserID($oa_user_id);
            $this->right_list = $right_list;
            $header_list = array();
            foreach($right_list as $key=>$value)
            {
                if(intval($value['default_allow']) == 0)
                {
                    $header_list[] = $value['controller'];
                    $header_list[] = $value['controller']."/".$value['action'];
                }
            }
            //有权限操作的controller
            $header_list = array_unique($header_list);
            Yii::app()->params['header_list'] = $header_list;
        }
        if (in_array($actionName, array('login', 'logout', 'calculatespan','error','index','rolevalidate','adminvalidate','batchimport')))
        {
            return TRUE;
        } elseif ($oa_user_id === 0) { //如果用户未登录
            Yii::app()->request->redirect(Yii::app()->createURL('site/login'));
        }

        $roleModel = new Role();
        $roleIds = $roleModel->getRoleIdByUserId($oa_user_id);
        $userTaskModel = new UserTask(); 
        Yii::app()->params['userWorkflowCnt'] = $userTaskModel->getTodoItemCntByUserId($oa_user_id,$roleIds);

        $checkinoutModel = new CheckInOut(); 
        $checkDate = $checkinoutModel->getCheckinoutCycle();

        $checkResult = $checkinoutModel->getListByUserId($oa_user_id,$checkDate['start_date'],$checkDate['end_date']);

        $checkCnt = 0;
        foreach($checkResult as $item)
        {
            if($item['abnormal'] == 1 && $item['absenteeism_type'] == 0)
            {
                $checkCnt += 1;
            }
        }
        Yii::app()->params['checkCnt'] = $checkCnt;
        $checkCnt = 0;

        if ( isset($right_list) && ! empty($right_list)) {
            foreach ($right_list as $key => $value) {
                // var_dump($controllerName,  $actionName);
                if ($controllerName == strtolower($value['controller']) && $actionName == strtolower($value['action'])) {
                    return TRUE;
                }
            }
            echo "Access Forbidden!";
            return FALSE;

        } else {

            Yii::app()->request->redirect(Yii::app()->createURL('site/login'));

        }
        return true;
    }

    public function actionError()
    {
        $error=Yii::app()->errorHandler->error;
        if(!$error){ 
            return;
        }

        //AJAX请求，直接返回文字。
        if(Yii::app()->request->isAjaxRequest){
            if($error['type'] == 'SystemException'){
                echo '系统异常，请稍后重试。';
            }else{
                echo '很抱歉，您要访问的网页不存在。';
            }
        }else{
            //系统异常。
            if($error['type'] == 'SystemException') {
                header("HTTP/1.1 503 Service Unavailable");
                $this->renderFile("503.tpl");
            }else{
                $code = $error['code'];
                $tplData = array(
                    "code"=>$code,
                    "message"=>$error['message']
                );
                $tplData['arrRAPageName'] = array('page_name' => 'error_404');

                switch($code){
                    //做为iframe session失效错误处理码:601
                case 601:
                    $this->renderFile('404.tpl',$tplData);
                    break;
                case 404:
                default:
                    header("HTTP/1.1 404 Not Found");
                    $this->renderFile("404.tpl",$tplData);
                    break;
                }
            }
        }
    }

    public function renderFile($viewFile,$arrTplData=null,$result=false)
    {
        /*
        $detect = new Mobile_Detect(); 
        $isMobile = $detect->isMobile();
        if($isMobile){
            $viewFile = 'mobile/'.$viewFile;
        }
         */
        parent::renderFile($viewFile, $arrTplData, $return);
    }
}
