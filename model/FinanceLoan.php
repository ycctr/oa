<?php 
class FinanceLoan extends OaBaseModel
{
    public $table = 'finance_loan';

    //取出某员工所有的借款记录,不包括人事类及审批中或审批通过的借款单
    public function getLoanListByUser($user_id){
        $sql = sprintf('select fl.* from finance_loan fl inner join user_workflow uwf on uwf.obj_id=fl.id where uwf.obj_type="finance_loan" and fl.user_id=%d and fl.pay_type!=%d and uwf.audit_status=%d and fl.status=%d and uwf.status=%d
        	  and fl.id not in (select loan_id from finance_payment fp inner join user_workflow uwf on fp.id=uwf.obj_id where uwf.obj_type="finance_offset" and uwf.audit_status in (%d,%d,%d))
        	',$user_id,FINANCE_LOAN_HR,OA_WORKFLOW_AUDIT_PASS,STATUS_VALID,STATUS_VALID,OA_WORKFLOW_AUDIT_CHUANGJIAN,OA_WORKFLOW_AUDIT_SHENPIZHONG,OA_WORKFLOW_AUDIT_PASS);
        $ret = $this->db()->query($sql);
        return $ret;
    }

    //取出所有未冲抵借款记录，不包括人事类及审批中或审批通过的借款单
    public function getFullLoanList(){
        $sql = sprintf('select fl.*,u.email as email,uwf.modify_time from finance_loan fl inner join user_workflow uwf on uwf.obj_id=fl.id inner join user u on fl.user_id=u.id where uwf.obj_type="finance_loan" and fl.pay_type!=%d and uwf.audit_status=%d and fl.status=%d and uwf.status=%d
              and fl.id not in (select loan_id from finance_payment fp inner join user_workflow uwf on fp.id=uwf.obj_id where uwf.obj_type="finance_offset" and uwf.audit_status in (%d,%d,%d))
            ',FINANCE_LOAN_HR,OA_WORKFLOW_AUDIT_PASS,STATUS_VALID,STATUS_VALID,OA_WORKFLOW_AUDIT_CHUANGJIAN,OA_WORKFLOW_AUDIT_SHENPIZHONG,OA_WORKFLOW_AUDIT_PASS);
        $ret = $this->db()->query($sql);
        return $ret;
    }

    //取出所有未完成冲抵借款记录，不包括人事类
    public function getUnpayedLoanList(){
        $sql = sprintf('select fl.*,uwf.modify_time,uwf.id as loan_uwf_id,u.name from finance_loan fl inner join user_workflow uwf on uwf.obj_id=fl.id inner join user u on fl.user_id=u.id where uwf.obj_type="finance_loan" and fl.pay_type!=%d and uwf.audit_status=%d and fl.status=%d and uwf.status=%d
             and fl.id not in (select loan_id from finance_payment fp inner join user_workflow uwf on fp.id=uwf.obj_id where uwf.obj_type="finance_offset" and uwf.audit_status=%d)
            ',FINANCE_LOAN_HR,OA_WORKFLOW_AUDIT_PASS,STATUS_VALID,STATUS_VALID,OA_WORKFLOW_AUDIT_PASS);
        $ret = $this->db()->query($sql);
        return $ret;
    }

    //取出所有已冲抵借款记录，不包括人事类
    public function getPayedLoanList(){
        $sql = sprintf('select fl.*,uwf.modify_time,uwf.id as loan_uwf_id,u.name from finance_loan fl inner join user_workflow uwf on uwf.obj_id=fl.id inner join user u on fl.user_id=u.id where uwf.obj_type="finance_loan" and fl.pay_type!=%d and uwf.audit_status=%d and fl.status=%d and uwf.status=%d
              and fl.id in (select loan_id from finance_payment fp inner join user_workflow uwf on fp.id=uwf.obj_id where uwf.obj_type="finance_offset" and uwf.audit_status=%d)
            ',FINANCE_LOAN_HR,OA_WORKFLOW_AUDIT_PASS,STATUS_VALID,STATUS_VALID,OA_WORKFLOW_AUDIT_PASS);
        $ret = $this->db()->query($sql);
        foreach ($ret as &$v) {
            $sql = sprintf('select uwf.modify_time as finish_time,uwf.id as pay_uwf_id from user_workflow uwf inner join finance_payment fp on uwf.obj_id=fp.id where uwf.obj_type="finance_offset" and fp.loan_id=%d and uwf.audit_status=%d and uwf.status=%d and fp.status=%d',$v['id'],OA_WORKFLOW_AUDIT_PASS,STATUS_VALID,STATUS_VALID);
            $ret2 =  $this->db()->query($sql);
            $v['finish_time'] = $ret2[0]['finish_time'];
            $v['pay_uwf_id'] = $ret2[0]['pay_uwf_id'];
        }
        return $ret;
    }

    //取出人事类借款
    public function getHrLoanList(){
        $sql = sprintf('select fl.*,uwf.modify_time,uwf.id as loan_uwf_id,u.name from finance_loan fl inner join user_workflow uwf on uwf.obj_id=fl.id inner join user u on fl.user_id=u.id where uwf.obj_type="finance_loan" and fl.pay_type=%d and uwf.audit_status=%d and fl.status=%d and uwf.status=%d
             ',FINANCE_LOAN_HR,OA_WORKFLOW_AUDIT_PASS,STATUS_VALID,STATUS_VALID);
        $ret = $this->db()->query($sql);
        return $ret;
    }
}

