<?php
class TeamBuilding extends OaBaseModel
{
    public $table = 'team_building';

    public function getListByDate($startDay,$endDay,$sectionName='') {

        if(empty($startDay) || empty($endDay)) {
            $startDay = date("Y-m-d"); 
            $endDay = date("Y-m-d"); 
        }

        $whereSql = "";
        $startTime = strtotime($startDay);
        $endTime   = strtotime($endDay)+86400;
        $whereSql = " AND t.activity_time >= {$startTime} AND t.activity_time <= {$endTime}";
        if(!empty($sectionName)){
            $whereSql .= sprintf(" and s.name like '%%%s%%' ",$sectionName); 
        }

        $sql = "SELECT s.team_name team, s.name s_name, u.name uname, t.activity_time time, t.activity_type type, t.title title, t.desc, t.section_id s_id, t.amount amount, uwf.audit_status  FROM team_building t, section s, user u , user_workflow uwf where uwf.obj_type='team_building' AND  uwf.obj_id = t.id AND uwf.audit_status in (".OA_WORKFLOW_AUDIT_SHENPIZHONG.",".OA_WORKFLOW_AUDIT_PASS.") AND t.section_id = s.id AND t.user_id=u.id  {$whereSql} AND t.status = 1 ORDER BY team, uname ";

        $result = $this->db()->query($sql);

        return $result;

    } 
}
?>
