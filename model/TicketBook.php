<?php
/**
 * Created by PhpStorm.
 * User: songyuzhuo
 * Date: 2016/6/17
 * Time: 14:07
 * 订票表
 */

class TicketBook extends OaBaseModel{

    public $table = 'ticket_book';

    //获取给定条件下已审批的票务记录
    public function getListWithCondition($arrQuery){
        $strBuildSql = '';
        if(isset($arrQuery['start_time']) && $arrQuery['start_time']){
            $strBuildSql .= ' and uwf.modify_time > ' . strtotime($arrQuery['start_time']);
        }
        if(isset($arrQuery['end_time']) && $arrQuery['end_time']){
            $strBuildSql .= ' and uwf.modify_time < ' . (strtotime($arrQuery['end_time']) + 86400);
        }
        if(isset($arrQuery['name']) && $arrQuery['name']){
            $strBuildSql .= " and u.name='" . $arrQuery['name'] . "'";
        }
        if(isset($arrQuery['section_name']) && $arrQuery['section_name']){
            $sectionModel = new Section();
            $section_id = $sectionModel->getIdByName($arrQuery['section_name']);
            if($section_id){
                $strBuildSql .= " and u.section_id=" . $section_id[0]['id'];
            }else{
                $strBuildSql .= " and u.section_id=" . 0;
            }
        }
        $strSql = sprintf("select t.*, u.name as user_name, u.id_card as user_id_card, u.section_id as user_section_id, uwf.modify_time as uwf_modify_time 
                  from %s t, user_workflow uwf, user u
                  where u.id=uwf.user_id and t.user_id=u.id and uwf.audit_status='%s' and uwf.obj_id=t.id and uwf.obj_type like 'ticket%%' 
                  " . $strBuildSql . " order by uwf.modify_time limit %s, %s",
                  $this->table, OA_WORKFLOW_AUDIT_PASS, ($arrQuery['page'] - 1) * $arrQuery['pageSize'], $arrQuery['pageSize']);
        if(isset($arrQuery['csv']) && $arrQuery['csv']){
            $strSql = sprintf("select t.*, u.name as user_name, u.id_card as user_id_card, u.section_id as user_section_id, uwf.modify_time as uwf_modify_time 
                  from %s t, user_workflow uwf, user u
                  where u.id=uwf.user_id and t.user_id=u.id and uwf.audit_status='%s' and uwf.obj_id=t.id and uwf.obj_type like 'ticket%%' 
                  " . $strBuildSql . " order by uwf.modify_time ",
                $this->table, OA_WORKFLOW_AUDIT_PASS);
        }
        $arrData = $this->db()->query($strSql);
        foreach ($arrData as &$record){
            $sectionModel = new Section();
            $section = $sectionModel->getItemByPk($record['user_section_id']);
            $sections = $sectionModel->getParentSectionBySectionId($record['user_section_id']);
            $sections = array_merge(array($section),$sections);
            if(count($sections) == 1){
                $record['yewuxian'] = $record['bumen'] = $record['jutibumen'] = $sections[0]['name'];
            }elseif(count($sections) == 2){
                $record['bumen'] = $record['jutibumen'] = $sections[0]['name'];
                $record['yewuxian'] = $sections[1]['name'];
            }else{
                $record['jutibumen'] = $sections[0]['name'];
                $record['bumen'] = $sections[count($sections) - 2]['name'];
                $record['yewuxian'] = $sections[count($sections) - 1]['name'];
            }
        }
        return $arrData;
    }


    //获取筛选条件下记录总数
    public function getCntWithCondition($arrQuery){
        $strBuildSql = '';
        if(isset($arrQuery['start_time']) && $arrQuery['start_time']){
            $strBuildSql .= ' and uwf.modify_time > ' . strtotime($arrQuery['start_time']);
        }
        if(isset($arrQuery['end_time']) && $arrQuery['end_time']){
            $strBuildSql .= ' and uwf.modify_time < ' . (strtotime($arrQuery['end_time']) + 86400);
        }
        if(isset($arrQuery['name']) && $arrQuery['name']){
            $strBuildSql .= " and u.name='" . $arrQuery['name'] . "'";
        }
        if(isset($arrQuery['section_name']) && $arrQuery['section_name']){
            $sectionModel = new Section();
            $section_id = $sectionModel->getIdByName($arrQuery['section_name']);
            if($section_id){
                $strBuildSql .= " and u.section_id=" . $section_id[0]['id'];
            }else{
                $strBuildSql .= " and u.section_id=" . 0;
            }
        }
        $strSql = sprintf("select count(*) as cnt
                  from %s t, user_workflow uwf, user u
                  where u.id=uwf.user_id and t.user_id=u.id and uwf.audit_status='%s' and uwf.obj_id=t.id and uwf.obj_type like 'ticket%%' 
                  " . $strBuildSql,
            $this->table, OA_WORKFLOW_AUDIT_PASS);
        $arrData = $this->db()->query($strSql);
        return $arrData[0]['cnt'];
    }
}

