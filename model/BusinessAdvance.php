<?php
class BusinessAdvance extends OABaseModel
{
    public $table = "business_advance"; 

    public function getItemsByParentId($parentId)
    {
        if($parentId - 0 < 1){
            return false;
        } 

        $ret = array();
        $sql = sprintf("select * from business_advance where (id=%d or parent_id=%d) ",$parentId,$parentId);
        $result = $this->db()->query($sql);
        $sql2 = sprintf("select card_no,start_day,start_node,end_day,end_node,business_place,business_purpose from business_advance where (id=%d or parent_id=%d) limit 1 ",$parentId,$parentId);
        $result2 = $this->db()->query($sql2);
        $ret['card_no'] = $result2[0]['card_no'];
        $ret['start_day'] = $result2[0]['start_day'];
        $ret['start_node'] = $result2[0]['start_node'];
        $ret['end_day'] = $result2[0]['end_day'];
        $ret['end_node'] = $result2[0]['end_node'];
        $ret['business_purpose'] = $result2[0]['business_purpose'];
        $ret['business_place'] = $result2[0]['business_place'];
        if(!empty($result)){
            foreach($result as $item){
                if($item['type'] == BUSINESS_ADVANCE_TYPE_RENT){
                    $ret[BUSINESS_ADVANCE_TYPE_RENT][] = $item;
                }elseif($item['type'] == BUSINESS_ADVANCE_TYPE_ENTERTAINMENT){
                    $ret[BUSINESS_ADVANCE_TYPE_ENTERTAINMENT][] = $item;
                }else{
                    $ret[BUSINESS_ADVANCE_TYPE_OTHER][] = $item;
                }          
            }
        }

        return $ret;
    }

    public function getSumAmountByParentId($parentId)
    {
        if($parentId - 0 < 1){
            return false;
        } 

        $totalCnt = 0;
        $sql = sprintf("select sum(amount) as totalCnt from business_advance where (id=%d or parent_id=%d) ",$parentId,$parentId);
        $result = $this->db()->query($sql);
        if(!empty($result)){
            $totalCnt = $result[0]['totalCnt']; 
        }
        return $totalCnt;
    }

     public function getListByIds($ids)
    {
        if(empty($ids)){
            return false;
        } 
        
        if(!is_array($ids)){ //将id 整理为数组
            $id = (int)$ids;
            $ids = array();
            $ids[] = $id;
        }

        $sql = "select a.*,u.name as user_name,u.id_card,sum(b.amount) as other_amount from business_advance as a left join business_advance b on a.id=b.parent_id inner join user as u on u.id=a.user_id where a.id in (".implode(',',$ids).") and a.status=1 group by a.id ";
        $result = $this->db()->query($sql);
        if(!empty($result)){
            foreach($result as &$item){
                $item['total_amount'] = $item['amount'] + $item['other_amount'];
            } 
        }
        return $result;
    }
}

?>
