<?php
class SectionMoneyRight extends OaBaseModel
{
	public $table = "section_money_right";

	public function getItemBySectionRight($section_id,$section_right_id){
		if(empty($section_id)||empty($section_right_id)){
			return false;
		}
		$sql = sprintf("select * from section_money_right where section_id=%d and section_right_id=%d",$section_id,$section_right_id);
		$ret = $this->db()->query($sql);
		if(!empty($ret)){
			return $ret[0];
		}
		return false;
	}

	public function deleteByPk($id){
		if(empty($id)){
			return false;
		}
		$sql = sprintf("delete from section_money_right where id=%d",$id);
		$ret = $this->db()->query($sql);
	}
}