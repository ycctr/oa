<?php
class RoleRight extends OaBaseModel
{
    private $table = "role_right";

    public function getRoleRights($role_id)
    {
        $strSql = sprintf("select right_id from role_right where role_id=%d", $role_id);
        $ret = $this->db()->query($strSql);
        return $ret;
    }

    public function DeleteRoleRight($role_id)
    {
        $strSql = sprintf("delete from role_right where role_id=%d", $role_id);
        $this->db()->query($strSql);
    }
}
?>
