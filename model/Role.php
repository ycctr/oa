<?php
class Role extends OaBaseModel
{
    public $table = 'role';

    public function getItemByName($name)
    {
        if(empty($name)){
            return false;
        } 

        $sql = sprintf("select * from role where name='%s'",$name);

        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return $ret[0];
        }
        return false;
    }
    public function getRightListByRoleId($roleId)
    {
        if(empty($roleId)){
            return false;
        }
        $sql = sprintf("select right_id as id from role_right where role_id=%d",$roleId);
        //var_dump($sql);
        $ret = $this->db()->query($sql);
        $rightList = array();
        if(!empty($ret)&&is_array($ret))
        {
            foreach($ret as $current)
            {
                array_push($rightList,$current['id']);
            }
        }
        return $rightList;
    }
    public function getRoleNameByRoleId($roleId)
    {
        if(empty($roleId)){
            return false;
        }
        $sql = sprintf("select name from role where id=%d",$roleId);
        //var_dump($sql);
        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return $ret[0]['name'];
        }
        return false;
    }
    public function getRoleByUserId($userId)
    {
        if(empty($userId)){
            return false;
        }

        $sql = sprintf("select r.* from role r inner join user_role ur on r.id=ur.role_id where ur.user_id=%d and r.status=%d ",$userId,STATUS_VALID);
        $ret = $this->db()->query($sql);
        return $ret;
    }

    public function DeleteItemByRoleId($role_id)
    {
        $strSql = sprintf("update user_role set status=0 where role_id=%d", $role_id);
        $this->db()->query($strSql);
    } 

    public function getRoleIdByUserId($userId)
    {
        $ret = array();
        $result = $this->getRoleByUserId($userId);
        if(!empty($result)){
            foreach($result as $item){
                $ret[] = $item['id'];
            } 
        }
        return $ret;
    }

    public function getRoleIdByUserIds($userIds)
    {
        if(empty($userIds)){
            return false;
        }

        $sql = sprintf("select r.* from role r inner join user_role ur on r.id=ur.role_id where ur.user_id in (".implode(',',$userIds).") and r.status=%d group by r.id ",STATUS_VALID);
        $result = $this->db()->query($sql);
        if(!empty($result)){
            foreach($result as $item){
                $ret[] = $item['id'];
            } 
        }
        return $ret; 
    }
    public function getRoleListWithUserId($user_id)
    {
        $strSql = "select * from role where status=1 and name !='系统管理员'";
        $role_list = $this->db()->query($strSql);

        $result = array();
        if(!isset($role_list))
            return false;
        foreach($role_list as $key=>$value)
        {
            $result[$value['id']]['id'] = $value['id']; 
            $result[$value['id']]['name'] = $value['name'];
            $result[$value['id']]['is_checked'] = 0;
        }
        if(intval($user_id) > 0)
        {
            $strSql = sprintf("select role_id from user_role where user_id=%d and status=1",$user_id);
            $user_role = $this->db()->query($strSql);
            if(isset($user_role))
            {
                foreach($user_role as $key=>$value)
                {
                    if(isset($result[$value['role_id']])){
                        $result[$value['role_id']]['is_checked'] = 1;
                    }
                }
            }
        }
        return $result;
    }
    public function getRight($role_id)
    {
        $strSql = "select * from `right` where default_allow = 0";
        $rights = $this->db()->query($strSql);

        $result = array();
        foreach($rights as $key=>$value)
        {
            $result[$value['id']]['id'] = $value['id'];
            $result[$value['id']]['title'] = $value['title'];
            $result[$value['id']]['controller'] = $value['controller'];
            $result[$value['id']]['action'] = $value['action'];
            $result[$value['id']]['is_checked'] = 0;
        }
        if(intval($role_id) > 0)
        {
            $roleRight = array();
            $strSql = sprintf("select * from `role_right` where role_id=%d", $role_id);
            $roleRight = $this->db()->query($strSql);
            foreach($roleRight as $key=>$value)
            {
                if(isset($result[$value['right_id']])){
                    $result[$value['right_id']]['is_checked'] = 1;
                }
            }
        }
        return $result;
    }
    public function saveRoleRight($arrData, $role_id)
    {
        if(intval($role_id) > 0)
        {
            $strSql = sprintf("delete from `role_right` where role_id=%d", $role_id);
            $this->db()->query($strSql);
        }
        if (!empty($arrData) && isset($arrData))
        {
            foreach($arrData as $right_id)
            {
                $strSql = sprintf("insert into `role_right`(right_id, role_id) values(%d, %d)", intval($right_id), intval($role_id));
                $this->db()->query($strSql);
            }
        }
        return true;
    }

    //根据角色获取所属用户的Id
    public function getUserIdByRoleIds($roleIds)
    {
        if(empty($roleIds)){
            return false;
        } 
        if(is_array($roleIds)){
            $roleSql = "select user_id from user_role where role_id in (".implode(',',$roleIds).") and status=1 "; 
        }else{
            $roleSql = sprintf("select user_id from user_role where role_id=%d and status=%d",$roleIds,STATUS_VALID);
        }
        $ret = array();
        $roleRet = $this->db()->query($roleSql);
        if(!empty($roleRet)){
            foreach($roleRet as $row)
                $ret[] = $row['user_id'];
        } 
        return $ret;
    }

    //根据角色获取所属用户
    public function getUserByRoleId($roleId)
    {
        if(empty($roleId)){
            return false;
        } 
        $roleSql = sprintf("select u.* from user_role as ur inner join user as u on ur.user_id=u.id where ur.role_id=%d and ur.status=%d",$roleId,STATUS_VALID);
        $ret = $this->db()->query($roleSql);
        return $ret;
    }
}
?>
