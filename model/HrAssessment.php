<?php
/**
 * Created by PhpStorm.
 * User: songyuzhuo
 * Date: 2016/5/25
 * Time: 14:07
 * 员工入职表
 */

class HrAssessment extends OaBaseModel{

    public $table = 'hr_assessment';

//    public function
    public function getHrAssessmentsByEntryId($entryId){
        $strSql = sprintf("select * from %s where `entry_id`=%s",$this->table, $entryId);
        $arrData = $this->db()->query($strSql);
        return $arrData;
    }
}