<?php

class timeSpanCalculate
{

    public function isHoliday($day)
    {
        $holiday = CRongConfig::$holiday;
        $time = strtotime($day);
        $week=date("w",$time);
        if(isset($holiday[$day]))
        {
            if($holiday[$day] == 1)
                return  1;
            else
                return 0;
        }
        else
        {
            if($week == 0 || $week ==6 )
                return  1;
            else
                return 0;
        }
    }

    public function Calculate($btime,$etime,$from,$end)
    {
        $cnt = 0;
       // $bdate = "20150404"; $from = "2";
       // $edate = "20150408"; $end  = "2";

        //$btime = strtotime($bdate);
       // $etime = strtotime($edate);


        while($this->isHoliday(date("Ymd",$btime)))
        {
            $btime += 86400;
        }

        if($bdate != date("Ymd",$btime))
        {
            $bdate = date("Ymd",$btime);
            $from = "1";
        }
        while($bdate < $edate)
        {
            if(!$this->isHoliday($bdate))
                $cnt++;
            $bdate = date("Ymd",strtotime($bdate)+86400);
        }

        if($cnt>0 && $from == 2)//从中午开始请假的情况
            $cnt = $cnt - 0.5;

        if(!$this->isHoliday($edate))
        {
                $cnt = $cnt + ($end - 1)*0.5;
        }
        return $cnt;
    }

}
