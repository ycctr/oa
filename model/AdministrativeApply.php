<?php 
class AdministrativeApply extends OaBaseModel
{
    public $table = 'administrative_apply';

    public function getListByQuery($startDay,$endDay,$idCard='',$sectionName='',$userName=''){
        $strSql = 'select aa.create_time,u.name as user,s.name sname,s2.name as spname,aa.staff_type,aa.staff,aa.staff_ver,aa.staff_price,aa.number,aa.staff_total,aa.usage,aa.commit_time';
        $strSql .= sprintf(" from administrative_apply aa inner join user_workflow uwf on aa.id=uwf.obj_id and uwf.obj_type='administrative_apply' inner join user u on aa.user_id=u.id inner join section s on u.section_id=s.id left join section s2 on s.parent_id=s2.id where uwf.audit_status !=1 and uwf.audit_status !=99 and aa.commit_time!=0 ");
        if(!empty($startDay)){
        	$strSql .= sprintf("and aa.create_time>= %d ",strtotime($startDay));
        }
        if(!empty($endDay)){
        	$strSql .= sprintf("and aa.create_time<= %d ",strtotime($endDay));
        }
        if(!empty($idCard)){
            $strSql .= sprintf("and u.id_card=%d ",$idCard);
        }
        if(!empty($sectionName)){
            $strSql .= " and s.name like '%".$sectionName."%'";
        }
        if(!empty($userName)){
            $strSql .= " and u.name like '%".$userName."%'";
        }
        $ret = $this->db()->query($strSql);
        return $ret;
    }
}

