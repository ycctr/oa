<?php

/**
 * 订餐脚本
 *
 **/

class Dinner extends OaBaseModel {

    public $table = "dinner";
    
    /**
     * 根据用户id，获取用户当天信息
     **/
    public function getUserByUserId($user_id) {

        $date = date("Y-m-d");
        $strSql = "SELECT u.id_card,u.name, d.office, u.email, d.create_time FROM dinner d, user u WHERE d.user_id=u.id AND d.user_id={$user_id} AND date='{$date}' ";

        $ret = $this->db()->query($strSql);

        if( empty($ret) ) {

            return 0;

        }

        return $ret[0];

    }

    //获取当日订餐列表
    public function getUserByDate($isSelf=1) {

        $user_id = intval(Yii::app()->session['oa_user_id']);

        $where = "";
        if($isSelf == 1) {

            $where = " AND d.user_id <> {$user_id} ";

        }

        $date = date("Y-m-d");
        $strSql = "SELECT u.id_card,u.name,u.email, d.office, d.create_time FROM dinner d, user u WHERE d.user_id=u.id  AND date='{$date}' {$where} ORDER BY d.office ASC";
        

        $ret = $this->db()->query($strSql);

        if( empty($ret) ) {

            return 0;

        }

        return $ret;

    }

}
