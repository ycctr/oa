<?php
class UserRole extends OaBaseModel
{
    public $table = 'user_role';

    /*
    public function getItemByName($name)
    {
        if(empty($name)){
            return false;
        } 

        $sql = sprintf("select * from user_role where name='%s'",$name);

        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return $ret[0];
        }
        return false;
    }
     */
   //删除管理员 
   public function DeleteAdmin($user_id)
   {
       $strSql = sprintf("update user_role set status=0  where user_id=%d",$user_id);
       $this->db()->query($strSql);
   }
   public function getUserNameByUserId($id)
   {
        $strSql = sprintf("select name from user where id=%d",$id);
        $ret = $this->db()->query($strSql);
        if(empty($ret))
            return false;
        return $ret[0]['name'];
   }
   //获取总的记录条数
   public function getCountWithCondition($arrCondition,$user_id)
   {
        $strSql = sprintf("select count(distinct user_id) as num from user_role where status=1 ");
        if(isset($arrCondition['role_id']) and !empty($arrCondition['role_id']))
            $strSql = sprintf("%s and role_id=%d ",$strSql,intval($arrCondition['role_id']));
        if(!empty($user_id))
            $strSql = sprintf("%s and user_id in %s",$strSql,$user_id);
        Yii::log($strSql);
        $ret = $this->db()->query($strSql);
        //var_dump($strSql);exit;
        if(empty($ret))
            return 0;
        return $ret[0]['num'];
   }
   public function getUserRoleByUserId($id = -1)
   {
        if($id == -1)
            return false;
        $strSql = sprintf("select role_id from user_role where user_id=%d and status=1",$id);
        $ret = $this->db()->query($strSql);
        if(empty($ret))
            return false;
        $data = array();
        foreach($ret as $current)
        {
            array_push($data,$current['role_id']);
        }
        return $data;
   }
   public function getUserInfoById($id = -1)
   {
        if($id == -1)
            return false;
        $strSql = sprintf("select u.name name,ur.user_id user_id from user_role ur,user u where ur.id=%d and ur.id=u.id",$id);
        $ret = $this->db()->query($strSql);
        if(empty($ret))
            return false;
        return $ret;
   }
   public function getUserIdsByRoleId($role_id = -1)
   {
       if($role_id == -1)
            $strSql = sprintf("select distinct user_id from user_role where status=1");
       else
            $strSql = sprintf("select distinct user_id from user_role where role_id=%d and status=1",$role_id);
       Yii::log($strSql);
       $ret = $this->db()->query($strSql);
       if(empty($ret) || !is_array($ret))
           return null;
        $ids = array();
        foreach($ret as $current)
        {
            array_push($ids,$current['user_id']);
        }
        return $ids;
   }
   public function getInfoListWithCondition($arrCondition,$user_id,$offset,$size)
   {
 //       $strSql = sprintf("select ur.id,ur.last_modify_time u.name as user_name,u.email,r.name as role_name from user u,role r,user_role ur where ");
        if(!empty($user_id))
            $userIds = $user_id;
        else{

            $userIds = self::getUserIdsByRoleId(-1);
        }

        if(isset($arrCondition['role_id']) && !empty($arrCondition['role_id']))
        {
           $ids = self::getUserIdsByRoleId(intval($arrCondition['role_id']));
           //$userIds = array_merge($userIds,$ids);
           $userIds = $ids;
        }
       // $userIds = array_unique($userIds);
       // sort($userIds);
        $strSql = sprintf("select distinct u.id as user_id,u.email,u.name as user_name,ur.last_modify_time,ur.status from user u inner join user_role ur on u.id=ur.user_id where ur.status=1 and u.id in (%s) ",implode(",",$userIds));
       // $strSql = sprintf("select distinct u.id_card,u.name as user_name,u.email,ur.last_modify_time,ur.status from user u,user_role ur where ur.status=1 and u.id_card in (%s) order by u.id_card",implode(",",$userIdCards));
      //  $strSql = sprintf("select ur.id,ur.last_modify_time,ur.status,u.name as user_name,u.email,role.name as role_name from user_role ur left join role on ur.role_id=role.id left join user u on ur.user_id=u.id where ur.status=1 and  ");
        /*$role_id = "";
        $fix = " 1=1 ";
        if(!empty($user_id))
            $user_id = " ur.user_id in ".$user_id." ";
        if(!empty($arrCondition['role_id']))
            $role_id = " ur.role_id=".$arrCondition['role_id']." ";
        if(empty($user_id) && empty($role_id))
            $strSql = $strSql.$fix;
        else if(!empty($user_id) && !empty($role_id))
            $strSql = $strSql.$role_id."and ".$user_id;
        else
            $strSql = $strSql.$role_id.$user_id;*/
        $limit = sprintf(" limit %d,%d",$offset,$size);
        $strSql = $strSql.$limit;
        Yii::log($strSql);
//        var_dump($strSql);
        $ret = $this->db()->query($strSql);
        return $ret;
   }
   public function getRoleByUserId($user_id)
   {
        if(empty($user_id)){
            return false;
        }
        $sql = sprintf("select role_id from user_role where user_id=%d and status=1",$user_id);
        $ret = $this->db()->query($sql);
        $roleList = array();
        if(!empty($ret)&&is_array($ret))
        {
            foreach($ret as $current)
            {
                array_push($roleList,$current['role_id']);
            }
        }
        return $roleList;
   }
   public function saveUserRole($Role_id,$user_id)
   {
       Yii::log("saveuserrole:".$Role_id);
       Yii::log("saveuserrole:".$user_id);
       if(intval($user_id) > 0)
       {
           $strSql = sprintf("update user_role set status=0 and last_modify_time='%s' where user_id=%d and role_id!=2 ",time(),$user_id);
           Yii::log($strSql);
           $this->db()->query($strSql);
       }
       else
           return;
       if(!empty($Role_id) && is_array($Role_id))
       {
            foreach($Role_id as $role_id)
            {
                $arrData = array();
                $arrData['user_id'] = $user_id;
                $arrData['role_id'] = $role_id;
                $arrData['status'] = 1;
                $arrData['last_modify_time'] = time();
                $this->save($arrData); 
            }
       }
   }

   //获取用户的角色名称
   public function getRoleNameByUserId($userId)
   {
       if(empty($userId)){
           return false;
       }

       $sql = sprintf("select r.name from user_role ur inner join role r on ur.role_id=r.id where ur.user_id=%d and ur.status=%d and r.status=%d ",$userId,STATUS_VALID,STATUS_VALID);

       $ret = array();
       $rows = $this->db()->query($sql);
       if(!empty($rows)){
           foreach($rows as $row){
               $ret[] = $row['name'];
           }
       }
       return $ret;
   }
}
?>
