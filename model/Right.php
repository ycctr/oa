<?php
class Right extends OaBaseModel
{
    public $table = 'right'; 

	public function getPrivilegeByUserID($user_id)
	{
		if(isset($user_id))
		{
                    $strSql = sprintf("SELECT * FROM `right` r " .
                        "			LEFT JOIN (select distinct(right_id) as right_id from role_right rrr, user_role urr " .
                        "			WHERE urr.user_id=%d and urr.role_id=rrr.role_id and urr.status=%d) t ON r.id=t.right_id " .
                        "		WHERE r.default_allow=1 OR t.right_id IS NOT NULL  " .
                        "		ORDER BY id ASC ", $user_id,STATUS_VALID);
		}
		$result=$this->db()->query($strSql);
		return $result;
	}

    /**
     * 是否有这个权限
     **/
    public function isHasRight($ctrl, $action) {

        $oa_user_id = intval(Yii::app()->session['oa_user_id']);
        $user_id = $oa_user_id;
        $sql = "SELECT r.id FROM role_right rr , `right` r WHERE rr.right_id=r.id AND r.controller = '{$ctrl}' AND r.action = '{$action}' ";
        $res = $this->db()->query($sql);

        if(empty($res) ) {

            return false;


        }
        return true;

    }
}
?>
