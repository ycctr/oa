<?php
class AuditList extends OaBaseModel
{
    public $table = 'audit_list';

    public function getItemByUserWorkflowIdAndStep($userWorkflowId, $nodeTag='')
    {
        if(empty($userWorkflowId)){
            return false;
        }

        $sql = sprintf("select * from audit_list where user_workflow_id=%d and status=%d ",$userWorkflowId,STATUS_VALID);

        if(!empty($nodeTag)){
            $sql .= sprintf(" and workflow_step='%s'",$nodeTag); 
        }

        $sql .= " order by pos asc limit 1";
        $result = $this->db()->query($sql);
        if(!empty($result)){
            return reset($result);
        }

        return false;
    }

    public function getItemsByUserWorkflow($userWorkflowId, $nodeTag='')
    {
        if(empty($userWorkflowId)){
            return false;
        }

        $sql = sprintf("SELECT id, pos, user_id, business_role FROM `oa`.`audit_list` WHERE user_workflow_id = %d AND `status` = %d  ", $userWorkflowId, STATUS_VALID);

        if(!empty($nodeTag)){

            $sql .= sprintf(" AND workflow_step='%s' ",$nodeTag); 

        }

        $sql .= " GROUP BY pos, user_id  ORDER BY pos ASC ";

        $result = $this->db()->query($sql);

        if(empty($result) ) {

            return false;

        }
        $data = array();
        foreach($result as $val) {

            $pos        = $val['pos'];
            $data[$pos][] = $val;

        }
        return reset($data);


    }


    public function getAuditListStatusByUserWorkflowId($userWorkflowId)
    {
        if(empty($userWorkflowId)){
            return false;
        }

        $sql = sprintf("select al.*, ut.audit_status, ut.deploy_desc,u.name as username from audit_list al left join user_task ut on al.user_workflow_id=ut.user_workflow_id and al.workflow_step=ut.workflow_step and al.user_id=ut.user_id inner join user u on al.user_id=u.id where al.user_workflow_id=%d order by al.workflow_step,al.pos asc ",$userWorkflowId);
        // echo $sql;

        $result = $this->db()->query($sql);
        $ret = array();
        if(!empty($result)){
            foreach($result as $row){
                $ret[$row['workflow_step']][] = $row;
            }
        }

        return $ret;
    }

    public function getAuditListByUserWorkflowId($userWorkflowId){
        if(empty($userWorkflowId)){
            return false;
        }
        $sql = sprintf("select * from audit_list where user_workflow_id=%d",$userWorkflowId);
        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return $ret;
        }
        return false;
    }


    //获取流程相关所有人员email
    public function getAllUserEmailByUserWorkflowId($userWorkflowId)
    {
        if(empty($userWorkflowId)){
            return false;
        }

        $ret = array();
        $sql = sprintf("select u.email from audit_list a,user u where a.user_workflow_id=%d and a.user_workflow_id=u.id group by u.id ",$userWorkflowId);
        $result = $this->db()->query($sql);
        if(!empty($result)){
            foreach($result as $row){
                $ret[] = $row['email'];
            }
        }
        
        return $ret;
    }

    /**
     *
     * 判断是否op签字
     * @param int user_id 用户ID
     * @param int user_workflow_id 流程ID
     * @return bool
     **/

    public function checkingOpDone($user_id, $user_workflow_id) {

        $user_id          = intval($user_id);
        $user_workflow_id = intval($user_workflow_id);

        $sql = "SELECT id FROM audit_list WHERE user_workflow_id = {$user_workflow_id} AND user_id = {$user_id} AND business_role='op'";        

        $result = $this->db()->query($sql);


        if(empty($result) ){

            return false;

        }
        
        return true;

    }

    public function countAstepNumByUserWorkflowId($user_workflow_id)
    {
        $user_workflow_id = intval($user_workflow_id);

        $sql = "SELECT count(id) total FROM audit_list WHERE user_workflow_id={$user_workflow_id} AND workflow_step='A'";
        $result = $this->db()->query($sql);


        if( $result[0]['total'] >= 1 ){

            return $result[0]['total'];

        }
        
        return false;
  

    }
    
    public function getPos($user_workflow_id,$user_id)
    {   
         $user_workflow_id = intval($user_workflow_id);
         
         $sql = "select pos  from audit_list WHERE user_workflow_id ={$user_workflow_id} AND workflow_step='A' AND user_id = {$user_id}
                 AND status = 0 order by id desc limit 1";
         $result = $this->db()->query($sql);


         if( $result ){

             return $result[0]['pos']; 
         }   
      
         return false;   
         
    }

    public function getAuditListInfo($user_workflow_id,$pos)
    {
         $user_workflow_id = intval($user_workflow_id);
 
         $sql = "select *  from audit_list WHERE user_workflow_id ={$user_workflow_id} AND workflow_step='A' AND status = 1 AND pos >= {$pos} order by id asc limit 1";
         $result = $this->db()->query($sql);


         if( $result ){

             return $result[0]; 
         }   
      
         return false;   

    }

    public function updateAuditListInfo($user_workflow_id,$user_id)
    {
         $user_workflow_id = intval($user_workflow_id);
         $user_id          = intval($user_id);
 
         $sql = "update audit_list set status = 0 where  user_workflow_id ={$user_workflow_id} AND workflow_step='A' AND user_id ={$user_id} and status = 1 limit 1";
         $result = $this->db()->query($sql);


         if( $result ){

             return true; 
         }   
      
         return false;   



    }
    
    public function getNextAuditorInfo($user_workflow_id)
    {
         $user_workflow_id = intval($user_workflow_id);
 
         $sql = "select * from audit_list where user_workflow_id ={$user_workflow_id} AND status = 1 
                 AND workflow_step='A' order by id asc limit 1";
         $result = $this->db()->query($sql);


         if( $result ){

             return $result[0]; 
         }   
      
         return false;   

    }

}
?>
