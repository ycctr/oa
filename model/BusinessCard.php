<?php
class BusinessCard extends OaBaseModel
{
    public $table = 'business_card';

    public function getListByQuery($startDay,$endDay,$idCard='',$sectionName='',$userName=''){
        $strSql = 'select DATE_FORMAT(FROM_UNIXTIME(uwf.create_time),"%Y-%m-%d"),u.id_card,bc.user_name,s.name,bc.position,concat_ws(" ",bc.telephone,bc.extension_number),bc.mobile,u.email,count,FROM_UNIXTIME(uwf.modify_time)';
        $strSql .= sprintf(" from business_card bc inner join user_workflow uwf on bc.id=uwf.obj_id and uwf.obj_type='business_card' inner join user u on bc.user_id=u.id inner join section s on u.section_id=s.id where uwf.audit_status =3 ");
        if(!empty($startDay)){
        	$strSql .= sprintf("and uwf.create_time>= %d ",strtotime($startDay));
        }
        if(!empty($endDay)){
        	$strSql .= sprintf("and uwf.create_time<= %d ",strtotime($endDay));
        }
        if(!empty($idCard)){
            $strSql .= sprintf("and u.id_card=%d ",$idCard);
        }
        if(!empty($sectionName)){
            $strSql .= " and s.name like '%".$sectionName."%'";
        }
        if(!empty($userName)){
            $strSql .= " and u.name like '%".$userName."%'";
        }
        $ret = $this->db()->query($strSql);
        return $ret;
    }
}
?>
