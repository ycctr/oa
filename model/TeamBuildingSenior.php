<?php
/**
 * 高级团建分配model类
 * created by qiaoyuan@rong360.com, 2015-12-08
 *
 **/
class TeamBuildingSenior extends OaBaseModel{

    public $table = "team_building_senior";


    public function getBySectionIdData($sectionId) {

        $sql = " SELECT * FROM team_building_senior WHERE from_section = {$sectionId} AND status =".STATUS_VALID; 
        $data = $this->db()->query($sql);

        if(empty($data) ) {

            return false;

        }

        return $data;

    }

    public function getAllSectionData() {

        $sql = " SELECT * FROM team_building_senior t, section s WHERE s.id=t.from_section AND  t.status =".STATUS_VALID; 
        $data = $this->db()->query($sql);

        if(empty($data) ) {

            return false;

        }

        return $data;
        

    }

    

}
