<?php
class TeamBuildingRecord extends OaBaseModel
{
    public $table = 'team_building_record'; 

    public function getItemsBySectionId($sectionId,$start,$limit)
    {
        if(empty($sectionId)){
            return false;
        }

        $sql = sprintf("select tbr.*,u.name as user_name from team_building_record tbr left join user u on tbr.operator_id=u.id where tbr.section_id=%d and tbr.status=%d order by tbr.id desc ",$sectionId,STATUS_VALID);
        if(!empty($limit)){
            $sql .= " limit $start,$limit ";
        }

        $result = $this->db()->query($sql);
        return $result;
    }

    public function getItemTotalBySectionId($sectionId)
    {
        if(empty($sectionId)){
            return false;
        }

        $sql = sprintf("select count(*) as total from team_building_record where section_id=%d and status=%d ",$sectionId,STATUS_VALID);
        
        $result = $this->db()->query($sql);
        if(!empty($result)){
            return $result[0]['total'];
        }
        return false;
    }

    //获取所有部门的团建经费
    public function getTeamBudingBySection($sectionIdArr = array(),$sectionName='',$userName='')
    {
        $whereSql = "";
        if(!empty($sectionIdArr)) {

            $sectionIdStr = implode(',', $sectionIdArr);
            $whereSql = " s.id in ({$sectionIdStr})  AND ";

        }
        if(!empty($sectionName)){
            $whereSql .= sprintf(" s.name like '%%%s%%' and ",$sectionName);
        }
        if(!empty($userName)){
            $whereSql .= sprintf(" u.name like '%%%s%%' and ",$userName);
        }

        $sql = "select s.id as section_id, s.name as section_name,s.team_name as team_name, u.name as manager_name,sb.name as parent_name,sum(tbr.amount) as amount from section s left join team_building_record tbr on tbr.section_id=s.id and tbr.status=1 inner join user u on s.manager_id=u.id left join section sb on s.parent_id=sb.id  where {$whereSql} s.can_building=1 and s.status=1 group by s.id  order by team_name,section_name  ";

        $result = $this->db()->query($sql);

        return $result;
    }

    //获取部门团建经费总额
    public function getTotalAmountBySectionId($sectionId)
    {
        if(empty($sectionId)){
            return false;
        }

        $sql = sprintf("select sum(amount) as total from team_building_record where section_id=%d and status=%d ",$sectionId,STATUS_VALID);
        
        $result = $this->db()->query($sql);
        if(!empty($result)){
            return $result[0]['total'];
        }
        return false;
    }

    /**
     * 高级团建产生流水操作
     **/
    public function seniorTeamBuildingRecord($data) {

        $from_section   = $data['from_section'];
        $to_section     = $data['to_section'];
        $amount         = $data['amount'];
        $senior_id      = $data['id'];

        if(empty($from_section) || empty($to_section) || empty($amount) ) {

           return false;

        }
        $condition['senior_id'] = $senior_id;


        $teamSeniosModel = new TeamBuildingSenior();
        $teamSeniosModel->getItemByPk($senior_id);


        //公共变量
        $addData['create_time'] = time();
        $addData['date'] = date("Y-m-d");
        $addData['senior_id'] = $senior_id;
        $addData['status'] = STATUS_VALID;

        //产生扣流水
        $minusAmount = -$amount;
        $addData['amount'] = $minusAmount;
        $addData['section_id'] = $from_section;
        $addData['remark'] = "额外扣除".$amount;
        $isAdd = $this->save($addData);
        
        if(!$isAdd) {

           return false; 

        }

        //产生加流水
        $addAmount = $amount;
        $addData['amount'] = $addAmount;
        $addData['section_id'] = $to_section;
        $addData['remark'] = "从 [{$data['name']}] 部门额外增加".$amount;
        $this->save($addData);

        return true;



    }

    //获取某部门某月的团建经费发放记录
    public function getAutoItemByDate($date,$section_id){
        $sql = sprintf("select * from team_building_record where operator_id=0 and date='%s' and section_id=%d ",$date,$section_id);
        $ret = $this->db()->query($sql);
        if($ret){
            return $ret[0];
        }
        return false;
    }


}
?>
