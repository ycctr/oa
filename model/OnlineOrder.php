<?php
class OnlineOrder extends OaBaseModel
{
    public $table = 'online_order';


    public static function getFileName($attachment_address)
    {   
        $ret = array();
        $attachment_address = trim($attachment_address,"\;"); 
        $files = explode(';',$attachment_address);
        if(is_array($files)){
            foreach($files as $file){
                list(,$filename) = explode('--',$file); 
                $ret[urlencode($file)] = $filename;
            }   
        }   
        return $ret;
    }  
    
    //来源ID
    public function getRepeatSourceId($sourceId, $sourceType) {

        $sql = "SELECT * FROM {$this->table} WHERE source_id = {$sourceId} AND source = {$sourceType} LIMIT 1"; 
        $result=$this->db()->query($sql);

        if(!empty($result) ) {

            return true;

        } else {

            return false;

        }

    }

}
?>
