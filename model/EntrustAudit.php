<?php
class EntrustAudit extends OaBaseModel
{
    public $table = 'entrust_audit';

    public function getList()
    {
        $sql = "select a.id,a.obj_type,b.name as section_name,c.name as entrust_name,d.name as trustee_name from entrust_audit a inner join section b on a.section_id=b.id inner join user c on a.entrust_user_id=c.id inner join user d on a.trustee_user_id=d.id "; 
        $ret = $this->db()->query($sql);

        return $ret;
    }

    public function del($id)
    {
        if($id < 1){
            return false;
        }

        $sql = sprintf("delete from entrust_audit where id=%d",$id);

        $ret = $this->db()->query($sql);

        return $ret;
    }

    //根据委托人id 工作流类型 员工所属部门 获取委托记录
    public function getItemByEntrustUserIdAndObjType($entrustUserId,$obj_type,$sectionIds)
    {
        if(empty($entrustUserId) || empty($obj_type) || empty($sectionIds)){
            return false;
        } 

        $sql = sprintf("select * from entrust_audit where entrust_user_id=%d and obj_type='%s' ",$entrustUserId,$obj_type);
        $rows = $this->db()->query($sql);

        if(!empty($rows)){
            foreach($rows as $row){
                if(in_array($row['section_id'],$sectionIds)){
                    return $row;
                }
            } 
        }
        return false;
    }
}

?>
