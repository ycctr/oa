<?php
class Absence extends OaBaseModel
{
    public $table = "absence";

    public function getCountWithCondition($where = "1=1")
    {
        $strSql = "select count(*) as num from absence inner join user_workflow on absence.id=user_workflow.obj_id and user_workflow.obj_type='absence' where ".$where;
        $ret = $this->db()->query($strSql);
        if(empty($ret))
            return 0;
        return $ret[0]['num'];
    }

    public function getListWithCondition($where = "1=1")
    {
        $strSql = "select user.name,user.id_card,section.name as section_name,t2.name as parent_name,absence.title,absence.day_number,absence.leave_reason,absence.start_time,absence.end_time,absence.type,user_workflow.audit_status, absence.city from absence,user,section left join section t2 on section.parent_id=t2.id,user_workflow where absence.id=user_workflow.obj_id and user_workflow.obj_type='absence' and user.id=absence.user_id and user.section_id=section.id and ".$where;
        Yii::log($strSql); 
        $ret = $this->db()->query($strSql);
        return $ret;
    }

    //获取员工某日的休假记录
    public function getItemByUserIdAndDate($userId, $date, $type="")
    {
        if(empty($userId) || empty($date)){
            return false;
        } 
        $time = strtotime($date);
        
        if($type == VACATION_CHUCHAI || $type == VACATION_YINGONGWAICHU) {
            $typeSql = " AND a.type = {$type} ";
        } else {
            $typeSql = " AND a.type <> ".VACATION_CHUCHAI." AND a.type <> ".VACATION_YINGONGWAICHU;
        }

        $sql = sprintf("select a.* from user_workflow uwf inner join absence a on uwf.obj_id=a.id and uwf.obj_type='absence'  where uwf.user_id=%d and uwf.audit_status=%d and uwf.status=%d {$typeSql}  ",$userId,OA_WORKFLOW_AUDIT_PASS,STATUS_VALID);

        $sql .= " and unix_timestamp(from_unixtime(a.start_time,'%Y%m%d')) <=".$time." and unix_timestamp(from_unixtime(end_time,'%Y%m%d')) >=".$time." and a.status=".STATUS_VALID;

        $ret = $this->db()->query($sql);

        if(!empty($ret)){
            return $ret[0];
        }
        return false;
    }

    //获取员工在某天是否休假状态
    public function isVacation($user_id,$day,$type="")
    {
        if(empty($user_id) || empty($day)){
            return false;
        } 


        $ret = $this->getItemByUserIdAndDate($user_id,$day,$type);

        if(!empty($ret)){
            return true;   
        }
        return false;
    }

    //获取员工在某审批单通过后的请假记录
    public function getItemByUserIdAfterDate($userId,$dateTime)
    {
        if(empty($userId) || empty($dateTime)){
            return false;
        } 
        
        $sql = sprintf("select a.* from user_workflow uwf inner join absence a on uwf.obj_id=a.id and uwf.obj_type='absence' where uwf.user_id=%d and uwf.audit_status=%d and uwf.status=%d ",$userId,OA_WORKFLOW_AUDIT_PASS,STATUS_VALID);
        $sql .= " and uwf.modify_time >=". $dateTime ." and a.status=".STATUS_VALID." and a.is_cancel=0";
        $ret = $this->db()->query($sql);
    
        return $ret; 
    }

    //获取员工在某请假日期过后的请假记录
    public function getItemByUserIdAfterDate2($userId,$dateTime)
    {
        if(empty($userId) || empty($dateTime)){
            return false;
        } 
        
        $sql = sprintf("select a.* from user_workflow uwf inner join absence a on uwf.obj_id=a.id and uwf.obj_type='absence' where uwf.user_id=%d and uwf.audit_status=%d and uwf.status=%d ",$userId,OA_WORKFLOW_AUDIT_PASS,STATUS_VALID);
        $sql .= " and a.end_time >=". $dateTime ." and a.status=".STATUS_VALID." and a.is_cancel=0";
        $ret = $this->db()->query($sql);
    
        return $ret; 
    }

    //获取某时间段审批通过的请假记录
    public function getItemsByDay($startDay,$endDay)
    {
        $startTime = strtotime($startDay);
        $endTime = strtotime($endDay);
        $zero = strtotime('2015-07-26');
        $sql = sprintf("select a.* from user_workflow uwf inner join absence a on uwf.obj_id=a.id and uwf.obj_type='absence' where uwf.audit_status=%d and uwf.status=%d ",OA_WORKFLOW_AUDIT_PASS,STATUS_VALID);
        $sql .= " and uwf.modify_time >=". $startTime . " and uwf.modify_time < ". $endTime ." and a.status=".STATUS_VALID;
        $sql .= " and a.end_time >= $zero ";
        $ret = $this->db()->query($sql);
        return $ret; 
    }


    public function getItemByUserIdAfterDateQujian($userId,$dateTime,$startDay,$endDay)
    {
        if(empty($userId) || empty($dateTime)){
            return false;
        } 
        $startTime = strtotime($startDay);
        $endTime = strtotime($endDay);
        
        $sql = sprintf("select a.* from user_workflow uwf inner join absence a on uwf.obj_id=a.id and uwf.obj_type='absence' where uwf.user_id=%d and uwf.audit_status=%d and uwf.status=%d ",$userId,OA_WORKFLOW_AUDIT_PASS,STATUS_VALID);
        $sql .= " and uwf.modify_time >=". $startTime . " and uwf.modify_time < ". $endTime ;
        $sql .= " and a.end_time >=". $dateTime ." and a.status=".STATUS_VALID;
        $ret = $this->db()->query($sql);
    
        return $ret; 
    }

    //获取员工在某时间段的请假记录(不包含草稿、创建、拒绝的以及已经进行销假的)
    public function getValidItemsByDay($userId,$start_time,$end_time,$type="")
    {
        if(empty($userId)){
            return false;
        }

        if($type) {

            $strType = " AND a.type = {$type} ";

        } else {

            $strType = sprintf(" AND a.type <> %s AND a.type <> %s ", VACATION_CHUCHAI, VACATION_YINGONGWAICHU);

        }

        $sql = sprintf("select a.* from user_workflow uwf inner join absence a on uwf.obj_id=a.id and uwf.obj_type='absence' where a.is_cancel=0 and uwf.user_id=%d and uwf.audit_status!=%d and uwf.audit_status!=%d and uwf.audit_status!=%d and uwf.audit_status!=%d and uwf.status=%d ",$userId,OA_WORKFLOW_AUDIT_CAOGAO,OA_WORKFLOW_AUDIT_CHUANGJIAN,OA_WORKFLOW_AUDIT_REJECT, OA_WORKFLOW_AUDIT_CANCEL, STATUS_VALID);
        $sql .= sprintf(" and ((a.start_time <= %d and a.end_time > %d) or (a.start_time < %d and a.end_time >= %d) or (a.start_time >= %d and a.end_time <= %d)) {$strType}  ",$start_time,$start_time,$end_time,$end_time,$start_time,$end_time);

        $ret = $this->db()->query($sql);
        return $ret; 
        
    }

    //根据部门 姓名 工号 日期搜索请假记录
    public function getListByDateCondition($startDay,$endDay,$idCard='',$sectionName='',$userName='',$offset=0,$limit=0,$type=0)
    {
        if(empty($startDay) || empty($endDay)){
            $startDay = date('Y-m-26',strtotime('-1 month')); 
            $endDay   = date('Y-m-25');
        } 
        $strSql = 'select u.name,u.id_card,s.name as section_name,s2.name as parent_name,a.title,a.day_number,a.leave_reason,a.start_time,a.end_time,a.type,uwf.audit_status,a.city';
        $strSql .= sprintf(" from absence a inner join user_workflow uwf on a.id=uwf.obj_id and uwf.obj_type='absence' inner join user u on a.user_id=u.id inner join section s on u.section_id=s.id left join section s2 on s.parent_id=s2.id where a.is_cancel=0 and a.start_time >= %d and a.start_time <= %d and uwf.audit_status in (1,2,3,6) ",strtotime($startDay),strtotime($endDay));
        if(!empty($idCard)){
            $strSql .= sprintf("and u.id_card=%d ",$idCard);
        }
        if(!empty($type)){
            $strSql .= " and a.type = {$type} "; 
        }else{
            $strSql .= " and a.type <> 13 and a.type <> 25 ";
        }
        if(!empty($sectionName)){
            $strSql .= " and s.name like '%".$sectionName."%'";
        }
        if(!empty($userName)){
            $strSql .= " and u.name like '%".$userName."%'";
        }
        if(!empty($limit)){
            $strSql .= " order by a.user_id desc limit $offset,$limit";
        }
        $ret = $this->db()->query($strSql);
        return $ret;
    }

    //根据部门 姓名 工号 日期搜索请假记录，取出user_workflow_id
    public function getListByDateConditionWithUwf($startDay,$endDay,$idCard='',$sectionName='',$userName='',$offset=0,$limit=0,$type=0)
    {
        if(empty($startDay) || empty($endDay)){
            $startDay = date('Y-m-26',strtotime('-1 month')); 
            $endDay   = date('Y-m-25');
        } 
        $strSql = 'select u.name,u.id_card,s.name as section_name,s2.name as parent_name,a.title,a.day_number,a.leave_reason,a.start_time,a.end_time,a.type,uwf.audit_status,a.city,uwf.id as user_workflow_id';
        $strSql .= sprintf(" from absence a inner join user_workflow uwf on a.id=uwf.obj_id and uwf.obj_type='absence' inner join user u on a.user_id=u.id inner join section s on u.section_id=s.id left join section s2 on s.parent_id=s2.id where a.is_cancel=0 and a.start_time >= %d and a.start_time <= %d and uwf.audit_status in (1,2,3,6) ",strtotime($startDay),strtotime($endDay));
        if(!empty($idCard)){
            $strSql .= sprintf("and u.id_card=%d ",$idCard);
        }
        if(!empty($type)){
            $strSql .= " and a.type = {$type} "; 
        }else{
            $strSql .= " and a.type <> 13 and a.type <> 25 ";
        }
        if(!empty($sectionName)){
            $strSql .= " and s.name like '%".$sectionName."%'";
        }
        if(!empty($userName)){
            $strSql .= " and u.name like '%".$userName."%'";
        }
        if(!empty($limit)){
            $strSql .= " order by a.user_id desc limit $offset,$limit";
        }
        $ret = $this->db()->query($strSql);
        return $ret;
    }

    public function getCountByDateCondition($startDay,$endDay,$idCard='',$sectionName='',$userName='',$type=0)
    {
        if(empty($startDay) || empty($endDay)){
            $startDay = date('Y-m-26',strtotime('-1 month')); 
            $endDay   = date('Y-m-25');
        } 
        $strSql = 'select count(*) as cnt ';
        $strSql .= sprintf(" from absence a inner join user_workflow uwf on a.id=uwf.obj_id and uwf.obj_type='absence' inner join user u on a.user_id=u.id inner join section s on u.section_id=s.id left join section s2 on s.parent_id=s2.id where a.is_cancel=0 and a.start_time >= %d and a.start_time <= %d and uwf.audit_status in (1,2,3,6) ",strtotime($startDay),strtotime($endDay));
        if(!empty($idCard)){
            $strSql .= sprintf("and u.id_card=%d ",$idCard);
        }
        if($type>0){
            $strSql .= " and a.type = {$type} "; 
        }else{
            $strSql .= " and a.type <> 13 and a.type <> 25 ";
        }
        if(!empty($sectionName)){
            $strSql .= " and s.name like '%".$sectionName."%'";
        }
        if(!empty($userName)){
            $strSql .= " and u.name like '%".$userName."%'";
        }

        $ret = $this->db()->query($strSql);
        if(empty($ret)){
            return 0;
        }
        return $ret[0]['cnt'];
    }

    //取出某员工所有本考勤周期内审批通过的请假记录
    public function getAbsenceListByUser($user_id){
        $start = strtotime("-1 month",strtotime(date("Y-m-25",time())));
        $sql = sprintf('select abs.* from absence abs left join user_workflow uwf on uwf.obj_id=abs.id and uwf.obj_type="absence" where abs.user_id=%d and uwf.audit_status=%d and abs.status=%d and uwf.status=%d and abs.is_cancel=0 and abs.type not in (13,25) and abs.start_time>=%d',$user_id,OA_WORKFLOW_AUDIT_PASS,STATUS_VALID,STATUS_VALID,$start);
        $ret = $this->db()->query($sql);
        return $ret;
    }

    //获取休假结束后5个工作日仍未通过二级或以上审批的假期
    public function getUnpassedList(){
        $startDay = strtotime(date("Y-m-d",time()));
        $count = 0;
        while($count<5){
            $startDay = strtotime("-1 day",$startDay);
            if(!(VacationService::isHoliday(date("Y-m-d",$startDay)))){
                $count++;
            }
        }
        $sql = sprintf("select uwf.id as uwf_id,uwf.audit_status,a.* from user_workflow uwf,absence a,user_task ut where uwf.id=ut.user_workflow_id and uwf.obj_id=a.id and uwf.obj_type='absence' and uwf.audit_status=%d and uwf.status=%d and uwf.workflow_id=1 and ut.audit_status=%d and a.end_time<%d and a.end_time>1480089600 and a.status=%d and a.is_cancel=0",OA_WORKFLOW_AUDIT_SHENPIZHONG,STATUS_VALID,OA_TASK_TONGGUO,$startDay,STATUS_VALID);
        $ret = $this->db()->query($sql);
        return $ret;
    }

}
