<?php
/**
 * Created by PhpStorm.
 * User: songyuzhuo
 * Date: 2016/5/25
 * Time: 14:07
 * 员工入职表
 */

class Entry extends OaBaseModel{

    public $table = 'entry';

    //根据状态获取入职记录
    public function getEntriesByStatus($start, $rn, $status){
        $strSql = "select * from `user_workflow` uwf, {$this->table} e 
                    where uwf.obj_type='{$this->table}' and e.id=uwf.obj_id  and ";
        $status = $status ? 'uwf.audit_status=' : 'uwf.audit_status!=';
        $status .= OA_WORKFLOW_AUDIT_PASS;
        $strSql .= $status;
        $strSql .= " limit $start, $rn ";
        $arrData = $this->db()->query($strSql);
        $userModel = new User();
        $sectionModel = new Section();
        foreach ($arrData as &$item){
            $businessLine = $sectionModel->getItemByPk($item['business_line_id']);
            $item['business_line'] = $businessLine['name'];
            $section = $sectionModel->getItemByPk($item['section_id']);
            $item['section'] = $section['name'];
            $subsection = $sectionModel->getItemByPk($item['subsection_id']);
            $item['subsection'] = $subsection['name'];
            $user = $userModel->getItemByPk($item['user_id']);
            $item['applyUserName'] = $user['name'];
            $entryId = $item['id'];
            $departmentAssessmentModel = new DepartmentAssessment();
            $departmentAssessments = $departmentAssessmentModel->getDepartmentAssessmentsByEntryId($entryId);
            $item['departmentAssessments'] = $departmentAssessments;
            $hrAssessmentModel = new HrAssessment();
            $hrAssessments = $hrAssessmentModel->getHrAssessmentsByEntryId($entryId);
            $item['hrAssessments'] = $hrAssessments;
        }
        return $arrData;
    }

    //根据状态获取记录条数
    public function getEntryCntByStatus($status=0){
        $strSql = "select count(*) as cnt from `user_workflow` where obj_type='{$this->table}' and ";
        $status = $status ? 'audit_status=' : 'audit_status!=';
        $status .= OA_WORKFLOW_AUDIT_PASS;
        $strSql .= $status;
        $arrData = $this->db()->query($strSql);
        return $arrData[0]['cnt'];
    }
}

