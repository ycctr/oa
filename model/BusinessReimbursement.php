<?php
class BusinessReimbursement extends OABaseModel
{
    public $table = "business_reimbursement"; 

    public function getItemsByParentId($parentId)
    {
        if($parentId - 0 < 1){
            return false;
        } 

        $ret = array();
        $sql = sprintf("select * from business_reimbursement where (id=%d or parent_id=%d) ",$parentId,$parentId);
        $result = $this->db()->query($sql);
        $sql2 = sprintf("select card_no,bstart_day,bend_day,bstart_node,bend_node,business_place,business_purpose from business_reimbursement where (id=%d or parent_id=%d) limit 1 ",$parentId,$parentId);
        $result2 = $this->db()->query($sql2);
        $ret['card_no'] = $result2[0]['card_no'];
        $ret['bstart_day'] = $result2[0]['bstart_day'];
        $ret['bstart_node'] = $result2[0]['bstart_node'];
        $ret['bend_day'] = $result2[0]['bend_day'];
        $ret['bend_node'] = $result2[0]['bend_node'];
        $ret['business_purpose'] = $result2[0]['business_purpose'];
        $ret['business_place'] = $result2[0]['business_place'];
        if(!empty($result)){
            foreach($result as $item){
                if($item['type'] == BUSINESS_REIMBURSEMENT_TYPE_TRAFFIC){
                    $ret[BUSINESS_REIMBURSEMENT_TYPE_TRAFFIC][] = $item;
                }elseif($item['type'] == BUSINESS_REIMBURSEMENT_TYPE_ENTERTAINMENT){
                    $ret[BUSINESS_REIMBURSEMENT_TYPE_ENTERTAINMENT][] = $item;
                }elseif($item['type'] == BUSINESS_REIMBURSEMENT_TYPE_RENT){
                    $ret[BUSINESS_REIMBURSEMENT_TYPE_RENT][] = $item;
                }else{
                    $ret[BUSINESS_REIMBURSEMENT_TYPE_TAXI][] = $item;
                }          
            }
        }

        return $ret;
    }

    public function getParentItemById($parentId)
    {
        if($parentId - 0 < 1){
            return false;
        } 
        $sql = sprintf("select * from business_reimbursement where id=%d",$parentId);
        $result = $this->db()->query($sql);
        if(!empty($result)){
            return $result[0];
        }

        return false;
    }

    public function getSumAmountByParentId($parentId)
    {
        if($parentId - 0 < 1){
            return false;
        } 

        $totalCnt = 0;
        $sql = sprintf("select sum(amount) as totalCnt from business_reimbursement where (id=%d or parent_id=%d) ",$parentId,$parentId);
        $result = $this->db()->query($sql);
        if(!empty($result)){
            $totalCnt = $result[0]['totalCnt']; 
        }
        return $totalCnt;
    }

     public function getListByIds($ids)
    {
        if(empty($ids)){
            return false;
        } 
        
        if(!is_array($ids)){ //将id 整理为数组
            $id = (int)$ids;
            $ids = array();
            $ids[] = $id;
        }

        $sql = "select a.*,u.name as user_name,u.id_card,sum(b.amount) as other_amount from business_reimbursement as a left join business_reimbursement b on a.id=b.parent_id inner join user as u on u.id=a.user_id where a.id in (".implode(',',$ids).") and a.status=1 group by a.id ";
        $result = $this->db()->query($sql);
        if(!empty($result)){
            foreach($result as &$item){
                $item['total_amount'] = $item['amount'] + $item['other_amount'];
            } 
        }
        return $result;
    }

    //所有已审批报销
    public function getCompleteList($startTime,$endTime)
    {
        if(empty($startTime) || empty($endTime)){
            return false;
        }

        $sql = sprintf("select a.id,u.id_card,u.name,u.section_id,s.name as sname,a.card_no from business_reimbursement a 
            inner join user_workflow uwf on a.id=uwf.obj_id 
            inner join user u on a.user_id=u.id
            inner join section s on u.section_id=s.id
            where a.parent_id=0 and uwf.obj_type='business_reimbursement' and uwf.audit_status=%d and uwf.modify_time>=%d and uwf.modify_time<%d ",OA_WORKFLOW_AUDIT_PASS,$startTime,$endTime);

        $result = $this->db()->query($sql);
        foreach ($result as $key => $value) {
            $id = $value['id'];
            $sql = sprintf("select type,sum(amount) as amount from business_reimbursement a where a.id=%d or a.parent_id=%d group by a.type",$id,$id);
            $ret = $this->db()->query($sql);
            $ret2 = array();
            foreach ($ret as $k => $v) {
                $ret2[$v['type']] = $v;
            }
            $result[$key]['sum'] = $ret2;  
        }
     
        return $result;
    }
}

?>
