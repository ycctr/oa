<?php
class logModel extends OaBaseModel
{
   public $table = "right_op_log";

   public function clean2Darray($arrData)
   {
       $ret = array();
       if(is_array($arrData))
       {
           foreach($arrData as $current)
           {
               if(is_array($current))
               {
                   //var_dump($current);exit;
                   foreach($current as $tmp)
                        array_push($ret,$tmp);
               }else{
                array_push($ret,$current);
               }
           }
       }
       return $ret;
   }
   public function packageString($id,$name,$arrData)
   {
        if(empty($name))
           return '';
        $arrData = self::clean2Darray($arrData);
        $str = "".$id.",".$name."(";
        $str = $str.implode(",",$arrData);
        $str = $str.")";
        return $str;
   }
   
   public function RoleRightSave($user_id,$role_id,$role_old_name,$arr_old_Right,$role_name,$arrRight,$op_type)
   {
       $arrData = array();
       $arrData['user_id'] = $user_id;
       $arrData['operation_time'] = time();
       $arrData['op_type'] = $op_type;
       $old_value = self::packageString($role_id,$role_old_name,$arr_old_Right);
       $new_value = self::packageString($role_id,$role_name,$arrRight);
       //针对firefox 迅雷插件 一次get，发送两次请求这个bug做特殊处理。
       $arrParams['condition'] = $arrData;
       switch(intval($op_type))
       {
           case ROLE_ADD:
                    $arrParams['condition']['new'] = $new_value;
                    $arrParams['condition']['old'] = $old_value;
                    break;
           case ROLE_DEL:
                    $arrParams['where'] = " old like '$role_id,$role_old_name(%'";
                   // $arrParams['condition']['new'] = $new_value;
                    $new_value = "";
                    var_dump($arrParams);
                    break;
           case ROLE_EDIT:
                    $arrParams['condition']['new'] = $new_value;
                    //$arrParams['condition']['old'] = $old_value;

       }
      // $arrParams['condition'] = self::resetCondition($arrData,$role_id,$role_name,$arrRight);
       $this->setParams($arrParams);
      // var_dump($arrParams);return;
       $intNum = $this->getCount();
       $arrData['old'] = $old_value;
       $arrData['new'] = $new_value;
       if($intNum != 0 )
           return;
       $this->save($arrData);
   }

   public function UserRoleSave($user_id,$admin_id,$admin_name,$arr_old_Role,$arrRole,$op_type)
   {
       $arrData = array();
       $arrData['user_id'] = $user_id;
       $arrData['operation_time'] = time();
       $arrData['op_type'] = $op_type;
       $old_value = self::packageString($admin_id,$admin_name,$arr_old_Role);
       $new_value = self::packageString($admin_id,$admin_name,$arrRole);
       Yii::log("user_id:".$user_id);
       Yii::log("name:".$admin_name);
       Yii::log("old-value:".$old_value);
       Yii::log("new-value".$new_value);
       Yii::log("type:".intval($op_type));
        //针对firefox 迅雷插件 一次get，发送两次请求这个bug做特殊处理。
       $arrParams['condition'] = $arrData;
       switch(intval($op_type))
       {
           case ADMIN_ADD:
                    $arrParams['condition']['new'] = $new_value;
                    $old_value = "";
                    //$arrParams['condition']['old'] = $old_value;
                    break;
           case ADMIN_DEL:
                    $arrParams['where'] = " old like '$admin_id,$admin_name(%'";
                    $arrParams['condition']['new'] = "";
                    $new_value = "";
                    var_dump($arrParams);
                    break;
           case ADMIN_EDIT:
                    $arrParams['condition']['new'] = $new_value;
                    //$arrParams['condition']['old'] = $old_value;

       }
      // $arrParams['condition'] = self::resetCondition($arrData,$role_id,$role_name,$arrRight);
       $this->setParams($arrParams);
     //  var_dump($arrParams);return;
       $intNum = $this->getCount();
       $arrData['old'] = $old_value;
       $arrData['new'] = $new_value;
       if($intNum != 0 )
           return;
       $this->save($arrData);
   }
}
?>
