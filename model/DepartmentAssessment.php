<?php
/**
 * Created by PhpStorm.
 * User: songyuzhuo
 * Date: 2016/5/25
 * Time: 14:07
 * 员工入职表
 */

class DepartmentAssessment extends OaBaseModel{

    public $table = 'department_assessment';

//    public function
    public function getDepartmentAssessmentsByEntryId($entryId){
        $strSql = sprintf("select * from %s where `entry_id`=%s",$this->table, $entryId);
        $arrData = $this->db()->query($strSql);
        return $arrData;
    }
}