<?php 
class AdministrativeExpense extends OaBaseModel
{
    public $table = 'administrative_expense';

    public function getListByTime($startTime,$endTime)
    {
        if(empty($startTime) || empty($endTime)){
            return array();
        }
        $sql = sprintf("select e.*,u.name as user_name,s.name as section_name,uwf.audit_status from user_workflow uwf inner join ".$this->table." e on uwf.obj_type='".$this->table."' and uwf.obj_id=e.id inner join user u on uwf.user_id=u.id inner join section s on u.section_id=s.id where uwf.audit_status!=%d and uwf.audit_status!=%d and e.create_time>=%d and e.create_time < %d ",OA_WORKFLOW_AUDIT_CANCEL,OA_WORKFLOW_AUDIT_CHUANGJIAN,$startTime,$endTime);

        $list = $this->db()->query($sql);
        return $list;
    }
}

