<?php

/**
 * 订餐脚本
 *
 **/
class Express  extends OaBaseModel {

    public $table = "express";
    
    /**
     * 获取某一个用户的快递单
     *
     **/
    public function getSelfExpress($user_id, $start=0, $limit=10) {

        $limitSql = " LIMIT {$start}, {$limit}";

        $strSql = "SELECT e.create_time, e.date, u.name, s.team_name, s.name,e.office, e.goods_type, e.to_where, e.to_city, e.express_type, e.express_num  FROM express e, user u, section s WHERE  u.id = {$user_id} AND s.id=u.section_id AND u.id = e.user_id AND e.status = 1 ORDER BY e.create_time DESC  {$limitSql} ";

        $data = $this->db()->query($strSql);

        if( empty($data) ) {

            return false;

        }
        $ret = array();

        $ret['data'] = $data;
        $strTotal = "SELECT count(*) total FROM express e, user u, section s WHERE  u.id = {$user_id} AND s.id=u.section_id AND u.id = e.user_id AND e.status = 1 ";
        $total = $this->db()->query($strTotal);
        $ret['total'] = intval($total[0]['total']);

        return $ret;

    }

    /**
     * 获取某一天的
     *
     **/
    public function getAllExpress($whereSql, $start=0, $per=10) {


        if($start == 0 && $per == 0) {

            $limitSql = ""; 
        }else {

            $limitSql = " LIMIT {$start}, {$per} ";

        }

        $strSql = "SELECT e.create_time, e.id, e.date, u.name, s.team_name,  s.name section_name, e.office, e.goods_type, e.express_type, e.express_num, e.to_where, e.to_city FROM express e, user u, section s WHERE u.id = e.user_id AND s.id=u.section_id AND  {$whereSql} AND e.status = 1 ORDER BY e.create_time {$limitSql} ";

        $data = $this->db()->query($strSql);

        $ret = array();

        $ret['data'] = $data;
        $strTotal = "SELECT count(*) total FROM express e, user u, section s WHERE u.id = e.user_id AND s.id=u.section_id AND  {$whereSql} AND e.status = 1";
        $total = $this->db()->query($strTotal);
        $ret['total'] = intval($total[0]['total']);

        if( empty($ret) ) {

            return false;

        }

        return $ret;

    }

    //根据快递编号判断快递是否存在
    public function isByNumExperss($express_num) {

        $sql = "SELECT count(*) total FROM express WHERE express_num = '{$express_num}' AND status = 1  limit 1";

        $ret = $this->db()->query($sql);

        if($ret[0]['total'] < 1 ) {

            return false;

        }

        return true;

    }

    //删除用户：将用户状态设置为无效
    public function deleteUserById($id)
    {
        $strSql = "update express set status=".STATUS_INVALID." where id=".$id;
        $this->db()->query($strSql);
    }

}
