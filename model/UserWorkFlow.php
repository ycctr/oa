<?php
class UserWorkFlow extends OaBaseModel
{
    public $table = "user_workflow";

    public function getItemsByUserId($userId,$audit_status,$workflow_type,$start=0,$rn=DEFAULT_INCLUDE_PATH)
    {
        if(empty($userId)){
            return false;
        } 
        $sql = sprintf("select a.*,b.name from user_workflow a inner join workflow b on a.workflow_id=b.id where a.user_id=%d and a.status=%d ",$userId,STATUS_VALID);
        if(!empty($audit_status)){
            $sql .= " and a.audit_status=".$audit_status;
        }
        if(!empty($workflow_type)){
            $sql .= sprintf(" and a.obj_type='%s' ",$workflow_type);
        }
        $sql .= sprintf(" order by a.id desc limit %d,%d ",$start,$rn);
        $ret = $this->db()->query($sql);

        return $ret; 
    }

    public function getItemCntByUSerId($userId,$audit_status,$workflow_type)
    {
        if(empty($userId)){
            return false;
        } 
        $sql = sprintf("select count(*) as cnt from user_workflow a inner join workflow b on a.workflow_id=b.id where a.user_id=%d and a.status=%d ",$userId,STATUS_VALID);
        if(!empty($audit_status)){
            $sql .= " and a.audit_status=".$audit_status;
        }
        if(!empty($workflow_type)){
            $sql .= sprintf(" and a.obj_type='%s' ",$workflow_type);
        }
        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return $ret[0]['cnt'];
        }

        return false; 
    }

    //获取扩展信息
    public function getExtByUserWorkflow($table,$id)
    {
        if(empty($table) || empty($id)){
            return false;
        }
        if($table == 'cancel_absence'){
            $table = 'absence';
        }
        if($table == 'finance_offset'){
            $table = 'finance_payment';
        }
        $sql = sprintf("select * from ".$table." where id=%d",$id);
        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            $row = $ret[0];
            if($table == 'administrative_expense'){
                $img_address = trim($row['img_address'],';');
                if(!empty($img_address)){
                    $row['img_address'] = explode(';',$row['img_address']);
                }
            }
            return $row;
        }

        return $ret;
    }

    //获取工作流扩展信息 针对userworkflow和信息表1v1的
    //table 表名
    //ids id数组
    public function getListByTableAndIds($table,$ids)
    {
        if($table == 'cancel_absence'){
            $table = 'absence';
        }
        if($table == 'finance_offset'){
            $table = 'finance_payment';
        }
        if(empty($ids) || empty($table)){
            return false;
        }
        if(strpos($table, 'ticket_book') === 0 ||
            $table == 'ticket_refund' ||
            $table == 'ticket_endorse'){
            $table = 'ticket_book';
        }
        if(is_array($ids)){
            $sql = "select tmp.*,u.name as user_name,u.id_card from $table as tmp inner join user as u on    
                tmp.user_id=u.id where tmp.id in (".implode(',',$ids).") and tmp.status=1 ";
            if($table == 'ticket_book'){
                $sql = "select tmp.*,u.name as user_name,u.id_card from $table as tmp inner join user as u on    
                tmp.user_id=u.id where tmp.id in (".implode(',',$ids).") and tmp.status>0 ";
            }
        }else{
            $sql = sprintf("select tmp.*,u.name as user_name from $table as tmp inner join user as 
                u on tmp.user_id=u.id where tmp.id=%d and tmp.status=1 ",$ids);
            if($table == 'ticket_book'){
                $sql = sprintf("select tmp.*,u.name as user_name from $table as tmp inner join user as 
                u on tmp.user_id=u.id where tmp.id=%d and tmp.status>0 ",$ids);
            }
        }

        $result = $this->db()->query($sql);
        return $result;
         
    }

    //获取扩展信息列表
    //@table_ids 
    //array(
    //  'tablename' => array(
    //          id
    //  )
    //)
    public function getExtListByUserWorkflow($table_ids)
    {
        if(empty($table_ids) || !is_array($table_ids)){
            return false;
        } 

        $ret = array();
        foreach($table_ids as $tablename => $ids){
            switch($tablename){
                case 'daily_reimbursement':
                    $model = new DailyReimbursement();
                    $result = $model->getListByIds($ids);
                    break;
                case 'business_advance':
                    $model = new BusinessAdvance();
                    $result = $model->getListByIds($ids);
                    break;
                case 'business_reimbursement':
                    $model = new BusinessReimbursement();
                    $result = $model->getListByIds($ids);
                    break;
                default:
                    $result = $this->getListByTableAndIds($tablename,$ids);
                    break;
            }

            if(is_array($result)){
                foreach($result as $item)
                    $ret[$tablename][$item['id']] = $item;
            }
        }
        return $ret;
    }

    //获取工作流的所有信息
    public function getAllOfUserWorkflowById($userWorkflowId)
    {
        if(empty($userWorkflowId)){
            return false;
        } 

        $obj = array();
        $sql = sprintf("select uwf.*,wf.name from user_workflow as uwf,workflow as wf where uwf.workflow_id=wf.id and uwf.id=%d ",$userWorkflowId);
        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            $obj = $ret[0]; 
            if($obj['obj_type'] == 'cancel_absence'){
                $obj['ext'] = $this->getExtByUserWorkflow('absence',$obj['obj_id']);
            }else{
                $obj['ext'] = $this->getExtByUserWorkflow($obj['obj_type'],$obj['obj_id']);
            }
        }
        return $obj;
    }

    //搜索工作流程
    public function getUserWorkFlow($condition, $currentPage=1, $per=50 ) {

        $modelWorkFlow = new WorkFlow();
        $modelUserRole = new UserRole();
        $modelUser     = new User();
        $modelTask     = new UserTask(); 
        
        $whereSql = "";
        $whereTaskSql = "";

        //申请人
        if( !empty($condition['user_id']) ) {

           $user_id = $condition['user_id'];
           $whereSql .= " AND uf.user_id={$user_id} ";

        }
        

        //审核状态
        if( $condition['check_flag'] == 1 ) { //审核中

            $whereSql .= " AND uf.audit_status = ".OA_WORKFLOW_AUDIT_SHENPIZHONG." AND ut.audit_status = 0 ";

        }

        //流程类型
        if( !empty($condition['obj_type'])) {

           $obj_type = $condition['obj_type'];
           $whereSql .= " AND uf.obj_type='{$obj_type}' ";

        }

        $checkNameSql = "";//获取所有审批人信息

        //审批人
        if( !empty($condition['check_user_id']) ) {

           $check_user_id = $condition['check_user_id'];
           $checkNameSql = " AND ( ut.user_id='{$check_user_id}' ";


            if( !empty($condition['role_id']) ) {

                $role_str = $condition['role_id'];
                $checkNameSql .= " or ut.role_id in ({$role_str}) )";

            } else {
                
                $checkNameSql .= " )";

            }

        }

        $whereSql .= "{$checkNameSql}  AND uf.status = 1 ";

        if($currentPage > 1) {

            $currentPage = ($currentPage - 1) * $per;

        }else {

            $currentPage = 0;

        }

        $limit = " LIMIT {$currentPage}, {$per} ";

        $sql = "SELECT uf.id, u.name uname, uf.audit_status, uf.obj_type, uf.obj_id, ut.user_id audit_user_id, ut.role_id, uf.create_time  FROM user_workflow uf, user u, user_task ut  WHERE uf.user_id = u.id AND ut.user_workflow_id = uf.id  {$whereSql} ORDER BY create_time DESC {$limit}";
        // echo $sql;

        $sqlTotal = "SELECT count(*) total FROM user_workflow uf, user u, user_task ut  WHERE uf.user_id = u.id AND ut.user_workflow_id = uf.id  {$whereSql}";

        $ret = $this->db()->query($sql);
        $countRes = $this->db()->query($sqlTotal);

        if(!empty($countRes[0]['total'])) {

            $data['total'] = $countRes[0]['total'];

        }

        $data = array();

        if(!empty($ret)) {

            foreach($ret as $value) {

                $tmp_arr        = array();
                $obj_id         = $value['obj_id'];
                $user_name      = $value['uname'];
                $id             = $value['id'];
                $audit_user_id  = $value['audit_user_id'];
                $role_id        = $value['role_id'];
                $obj_type       = $value['obj_type'];

                $title = $modelWorkFlow->getObjType($obj_id, $obj_type);

                if(empty($title)) {

                    $title = "标题不存在";

                }
                $len = 0;
                $len = strlen($title);
                
                //UTF8编码长度不超过42字节
                if($len>60) {

                    $title = substr($title, 0, 60)."...";

                }

                $check_name = "";
                $check_name_arr = array();
                if(!empty($audit_user_id) ) {

                    $userData = $modelUser->getItemByPK($audit_user_id); 
                    $check_name_arr[] = $userData['name'];

                } else if (!empty($role_id) ) {

                    $roleArr = $modelUserRole -> getUserIdsByRoleId($role_id); 

                    foreach($roleArr as $user_id ) {

                        $role_user = $modelUser->getItemByPK($user_id); 
                        $check_name_arr[] = $role_user['name']; 
                    }

                    
                }

                $check_name = implode(",", $check_name_arr);


                if(empty($check_name)) {

                    continue;

                }

                //判断出差
                if($obj_type == "absence") {

                    $modelAbsence = new Absence();
                    $objData = $modelAbsence->getItemByPk($obj_id);

                    if($objData['type'] == 13) {

                        $obj_type = "business";

                    }

                }

                $tmp_arr['title']      = $title;
                $tmp_arr['obj_type']   = $obj_type;
                $tmp_arr['user_name']  = $user_name;
                $tmp_arr['check_name'] = $check_name;
                $tmp_arr['create_time'] = $value['create_time'];
                $data[$id] = $tmp_arr; 
            }

        }


       return $data;

    }

    /**
     * obj_id   某申请业务记录的ID，
     * oby_type 某一个业务分类
     **/
    public function getUserWorkflowId($obj_id, $obj_type) {

        
        $sql = "SELECT id FROM user_workflow WHERE obj_id = {$obj_id} AND obj_type='{$obj_type}' LIMIT 1 ";
        $res = $this->db()->query($sql);

        if(empty($res) ){

            return false;

        }

        return $res[0]['id'];


    }

    /**
     * 删除流程
     *
     **/
    public function delByFlowId($flowId) {

        $flowId = intval($flowId);
        if($flowId == 0 ) {

            return false; 

        }

        $sql = "UPDATE user_workflow SET audit_status = ".OA_WORKFLOW_AUDIT_CANCEL." WHERE id = {$flowId}";

        $is_update = $this->db()->query($sql);

        if($is_update) {

            return true;

        } else {

            return false;
        
        }

    }

    public function updateWorkflowStatusById($uwfId,$status) {

        $uwfId = intval($uwfId);
        if($uwfId == 0 ) {

            return false; 

        }

        $sql = "UPDATE user_workflow SET audit_status = {$status} WHERE id = {$uwfId}";

        $is_update = $this->db()->query($sql);

        if($is_update) {

            return true;

        } else {

            return false;
        
        }

    }

	public function getItemByDeployId($deploy_id)
	{
        $deployId = intval($deploy_id);
        if($deployId == 0 ) {

            return false; 

        }

        $sql = "select * from  user_workflow where deploy_id = {$deployId}";

        $res = $this->db()->query($sql);
        if(empty($res)){
			return false;
		}

		return $res;

	}

    public function getUwfByEntryId($entryId){
        $strSql = "select * from {$this->table} where obj_type='entry' and obj_id={$entryId}";
        $arrData = $this->db()->query($strSql);
        return $arrData[0];
    }

    public function getUwfByDimissionId($dimissionId){
        $strSql = "select * from {$this->table} where obj_type='dimission' and obj_id={$dimissionId}";
        $arrData = $this->db()->query($strSql);
        return $arrData[0];
    }


    public function saveUserWorkflow($workflow,$obj_id)
    {
        $arrData = array();
        $arrData['user_id'] = Yii::app()->session['oa_user_id'];
        $arrData['workflow_id'] = $workflow['id'];
        $arrData['category'] = $workflow['category'];
        $arrData['obj_type'] = $workflow['obj_type'];
        $arrData['obj_id'] = $obj_id;
        $arrData['userworkflow_config'] = $workflow['userworkflow_config'];
        $arrData['create_time'] = time();
        $arrData['audit_status'] = OA_WORKFLOW_AUDIT_CHUANGJIAN;
        $arrData['status'] = STATUS_VALID;
        $userWorkflowId = $this->save($arrData);
        if($userWorkflowId < 1){
            Yii::log('userworkflow save fail:'.print_r($arrData,true));
            throw new CHttpException(404,"保存资料失败");
        }
        return $userWorkflowId; 
    }
}
