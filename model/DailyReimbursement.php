<?php
class DailyReimbursement extends OABaseModel
{
    public $table = "daily_reimbursement"; 

    public function getItemsByParentId($parentId)
    {
        if($parentId - 0 < 1){
            return false;
        } 

        $ret = array();
        $sql = sprintf("select * from daily_reimbursement where (id=%d or parent_id=%d) ",$parentId,$parentId);
        $result = $this->db()->query($sql);
        if(!empty($result)){
            foreach($result as $item){
                if($item['type'] == REIMBURSEMENT_TYPE_TAXI){
                    $ret[REIMBURSEMENT_TYPE_TAXI][] = $item;
                }elseif($item['type'] == REIMBURSEMENT_TYPE_ENTERTAINMENT){
                    $ret[REIMBURSEMENT_TYPE_ENTERTAINMENT][] = $item;
                }else{
                    $ret[REIMBURSEMENT_TYPE_OTHER][] = $item;
                }
            }
        }

        return $ret;
    }

    public function getParentItemById($parentId)
    {
        if($parentId - 0 < 1){
            return false;
        } 
        $sql = sprintf("select * from daily_reimbursement where id=%d",$parentId);
        $result = $this->db()->query($sql);
        if(!empty($result)){
            return $result[0];
        }

        return false;
    }

    public function getSumAmountByParentId($parentId)
    {
        if($parentId - 0 < 1){
            return false;
        } 

        $totalCnt = 0;
        $sql = sprintf("select sum(amount) as totalCnt from daily_reimbursement where (id=%d or parent_id=%d) ",$parentId,$parentId);
        $result = $this->db()->query($sql);
        if(!empty($result)){
            $totalCnt = $result[0]['totalCnt']; 
        }
        return $totalCnt;
    }


    //所有已审批报销
    public function getCompleteList($startTime,$endTime)
    {
        if(empty($startTime) || empty($endTime)){
            return false;
        }
        $sql = sprintf("select a.id,user.id_card,user.name,section.name as section_name from daily_reimbursement a 
            inner join user on a.user_id=user.id 
            inner join section on user.section_id=section.id 
            inner join user_workflow uwf on a.id=uwf.obj_id and uwf.obj_type='daily_reimbursement' 
            where a.parent_id=0 and uwf.obj_type='daily_reimbursement' and uwf.audit_status=%d and uwf.modify_time>=%d and uwf.modify_time < %d group by a.id",OA_WORKFLOW_AUDIT_PASS,$startTime,$endTime); 
        $result = $this->db()->query($sql);

        foreach ($result as $key => $value) {
            $id = $value['id'];
            $sql = sprintf("select type,sum(amount) as amount from daily_reimbursement a where a.id=%d or a.parent_id=%d group by a.type",$id,$id);
            $ret = $this->db()->query($sql);
            $ret2 = array();
            foreach ($ret as $k => $v) {
                $ret2[$v['type']] = $v;
            }
            $result[$key]['sum'] = $ret2;  
        }
        return $result;
    }

    public function getListByIds($ids)
    {
        if(empty($ids)){
            return false;
        } 
        
        if(!is_array($ids)){ //将id 整理为数组
            $id = (int)$ids;
            $ids = array();
            $ids[] = $id;
        }

        $sql = "select a.*,u.name as user_name,u.id_card,sum(b.amount) as other_amount from daily_reimbursement as a left join daily_reimbursement b on a.id=b.parent_id inner join user as u on u.id=a.user_id where a.id in (".implode(',',$ids).") and a.status=1 group by a.id ";
        $result = $this->db()->query($sql);
        if(!empty($result)){
            foreach($result as &$item){
                $item['total_amount'] = $item['amount'] + $item['other_amount'];
            } 
        }
        return $result;
    }
}

?>
