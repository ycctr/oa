<?php
class UserTask extends OaBaseModel
{
    public $table = 'user_task';

    public function getLastTaskByUwfId($userworkflowId)
    {
        if(empty($userworkflowId))
            return false;
        $sql = sprintf('select * from user_task where user_workflow_id=%d order by id desc limit 1',$userworkflowId);
        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return $ret[0];
        }
        return false;
    }

    public function getTaskCntByStep($step,$userworkflowId)
    {
        if(empty($step) || empty($userworkflowId)){
            return false;
        }

        $sql = sprintf("select count(*) as cnt from user_task where user_workflow_id=%d and workflow_step='%s'",$userworkflowId,$step,OA_TASK_CHUANGJIAN);
        $ret = $this->db()->query($sql);

        if(!empty($ret)){
            return $ret[0]['cnt'];
        }
        return false;
    }

    public function getTaskByStep($step,$userworkflowId)
    {
        if(empty($step) || empty($userworkflowId)){
            return false;
        }

        $sql = sprintf("select * from user_task where user_workflow_id=%d and workflow_step='%s' ",$userworkflowId,$step);
        $ret = $this->db()->query($sql);

        if(!empty($ret)){
            return $ret[0];
        }
        return false;
    }

    //获取待审核项目
    public function getTodoItemsByUserId($userId,$roleIds,$applicantIds='',$obj_type='',$start=0,$rn=DEFAULT_PAGE_NUMBER)
    {
        if(empty($userId)){
            return false;
        }
        
        if(count($roleIds)){
            $sql = sprintf("select ut.*,uwf.obj_type,uwf.obj_id,uwf.audit_status as uwf_audit_status,uwf.create_time as uwf_create_time from user_task ut,user_workflow uwf,workflow wf where ut.user_workflow_id=uwf.id and uwf.workflow_id=wf.id and (ut.user_id=%d or ut.role_id in (".implode(',',$roleIds).")) and uwf.audit_status!=%d ",$userId,OA_WORKFLOW_AUDIT_CANCEL);
        }else{
            $sql = sprintf("select ut.*,uwf.obj_type,uwf.obj_id,uwf.audit_status as uwf_audit_status,uwf.create_time as uwf_create_time from user_task ut,user_workflow uwf,workflow wf where ut.user_workflow_id=uwf.id and uwf.workflow_id=wf.id and ut.user_id=%d and uwf.audit_status!=%d ",$userId,OA_WORKFLOW_AUDIT_CANCEL);
        }
        if(!empty($obj_type)){
            $sql .= sprintf(" and uwf.obj_type='%s'",$obj_type);
        }
        $sql .= sprintf(" and ut.audit_status=%d ",OA_TASK_CHUANGJIAN);
        if(!empty($applicantIds)){
            $sql .= " and uwf.user_id in (".implode(',',$applicantIds).") ";
        }
        $sql .= "group by ut.id order by uwf.create_time desc limit $start,$rn ";
        $ret = $this->db()->query($sql);

        return $ret;
    }

    //获取待审核项目数量
    public function getTodoItemCntByUserId($userId,$roleIds,$applicantIds='',$obj_type='')
    {
        if(empty($userId)){
            return false;
        }

        if(count($roleIds)){
            $sql = sprintf("select count(distinct uwf.id) as cnt from user_task ut,user_workflow uwf where ut.user_workflow_id=uwf.id and (ut.user_id=%d or ut.role_id in (".implode(',',$roleIds).")) and uwf.audit_status!=%d ",$userId,OA_WORKFLOW_AUDIT_CANCEL);
        }else{
            $sql = sprintf("select count(distinct uwf.id) as cnt from user_task ut,user_workflow uwf where ut.user_workflow_id=uwf.id and ut.user_id=%d and uwf.audit_status!=%d ",$userId,OA_WORKFLOW_AUDIT_CANCEL);
        }
        if(!empty($obj_type)){
            $sql .= sprintf(" and uwf.obj_type='%s'",$obj_type);
        }
        $sql .= sprintf(" and ut.audit_status=%d ",OA_TASK_CHUANGJIAN);
        if(!empty($applicantIds)){
            $sql .= " and uwf.user_id in (".implode(',',$applicantIds).") ";
        }

        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return $ret[0]['cnt'];
        }

        return false;
    }

    //获取已审批项目
    //userIds 下级审批人数组
    //userId 审批人自己
    //applicantIds 申请人id
    public function getCompleteItems($userId,$userIds,$roleIds,$applicantIds='',$obj_type='',$start=0,$rn=DEFAULT_PAGE_NUMBER)
    {
        if(empty($userId)){
            return false;
        }
        if(!empty($roleIds)){
            $sql = sprintf("select ut.*,uwf.obj_type,uwf.obj_id,uwf.audit_status as uwf_audit_status,u.name as audit_user_name,uwf.create_time as uwf_create_time from user_task ut inner join user_workflow uwf on ut.user_workflow_id=uwf.id inner join workflow wf on uwf.workflow_id=wf.id inner join user u on ut.audit_user_id=u.id where (((ut.audit_user_id=%d or ut.entrust_user_id=%d or (ut.role_id in (".implode(',',$roleIds).") and uwf.obj_type='business_advance')) and (ut.audit_status=%d or ut.audit_status=%d))",$userId,$userId,OA_TASK_TONGGUO,OA_TASK_JUJUE);
        }
        else{
            $sql = sprintf("select ut.*,uwf.obj_type,uwf.obj_id,uwf.audit_status as uwf_audit_status,u.name as audit_user_name,uwf.create_time as uwf_create_time from user_task ut inner join user_workflow uwf on ut.user_workflow_id=uwf.id inner join workflow wf on uwf.workflow_id=wf.id inner join user u on ut.audit_user_id=u.id where (((ut.audit_user_id=%d or ut.entrust_user_id=%d) and (ut.audit_status=%d or ut.audit_status=%d))",$userId,$userId,OA_TASK_TONGGUO,OA_TASK_JUJUE);
        }
        // echo $sql;
        // exit();
        if(!empty($userIds)){
            $sql .= sprintf(" or (ut.audit_user_id in (".implode(',',$userIds).") and (uwf.audit_status=%d or uwf.audit_status=%d or uwf.audit_status=%d)))",OA_WORKFLOW_AUDIT_PASS,OA_WORKFLOW_AUDIT_REJECT,OA_WORKFLOW_AUDIT_ROLLBACK);
        }else{
            $sql .= ")";
        }
        if(!empty($obj_type)){
            $sql .= sprintf(" and uwf.obj_type='%s'",$obj_type);
        }
        if(!empty($applicantIds)){
            $sql .= " and uwf.user_id in (".implode(',',$applicantIds).") ";
        }
        $sql .= " order by id desc ";

        $tmpSql = "select * from ($sql) as tt  group by user_workflow_id order by uwf_create_time desc limit $start,$rn ";
        $ret = $this->db()->query($tmpSql);

        return $ret;
    }

    //已审批项目数量
    public function getCompleteItemCnt($userId,$userIds,$roleIds,$applicantIds='',$obj_type='')
    {
        if(empty($userId)){
            return false;
        }

        if($roleIds){
            $sql = sprintf("select count(distinct uwf.id) as cnt from user_task ut inner join user_workflow uwf on ut.user_workflow_id=uwf.id where (((ut.audit_user_id=%d or (ut.role_id in (".implode(',',$roleIds).") and uwf.obj_type='business_advance')) and (ut.audit_status=%d or ut.audit_status=%d)) ",$userId,OA_TASK_TONGGUO,OA_TASK_JUJUE);
        }
        else{
            $sql = sprintf("select count(distinct uwf.id) as cnt from user_task ut inner join user_workflow uwf on ut.user_workflow_id=uwf.id where ((ut.audit_user_id=%d and (ut.audit_status=%d or ut.audit_status=%d )) ",$userId,OA_TASK_TONGGUO,OA_TASK_JUJUE);
        }

        if(!empty($userIds)){
            $sql .= sprintf(" or (ut.audit_user_id in (".implode(',',$userIds).") and (uwf.audit_status=%d or uwf.audit_status=%d or uwf.audit_status=%d)))",OA_WORKFLOW_AUDIT_PASS,OA_WORKFLOW_AUDIT_REJECT,OA_WORKFLOW_AUDIT_ROLLBACK);
        }else{
            $sql .= ")";
        }
        if(!empty($obj_type)){
            $sql .= sprintf(" and uwf.obj_type='%s'",$obj_type);
        }
        if(!empty($applicantIds)){
            $sql .= " and uwf.user_id in (".implode(',',$applicantIds).") ";
        }
// echo $sql;
// exit();
        $ret = $this->db()->query($sql);

        if(!empty($ret)){
            return $ret[0]['cnt'];
        }

        return $ret;
    }


    public function getWorkflowByUserTaskId($user_task_id)
    {
        if(empty($user_task_id)){
            return false;
        }   

        $sql = sprintf("select ut.*,u.name as user_name,s.name as section_name,s.id as section_id,uwf.obj_type,uwf.obj_id from user_task ut,user_workflow uwf,user u,section s where ut.user_workflow_id=uwf.id and uwf.user_id=u.id and u.section_id=s.id and ut.id=%d ",$user_task_id);
        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return $ret[0];
        }

        return false;
    }

    public function getItemsByUserWorkflowId($userWorkflowId)
    {
        if(empty($userWorkflowId)){
            return false;
        }

        $sql = sprintf("select ut.*,user.name as user_name,user.level from user_task as ut left join user on ut.user_id=user.id where user_workflow_id=%d ",$userWorkflowId);
        $ret = $this->db()->query($sql);

        return $ret;
    }

    //获取流程的所有已审核人员
    public function getAuditUserIdByUwfId($userWorkflowId,$step='')
    {
        $ret = array();
        if(empty($userWorkflowId)){
            return $ret;
        } 

        $sql = sprintf("select user_id,entrust_user_id from user_task where user_workflow_id=%d and user_id != 0 ",$userWorkflowId);
        if(!empty($step)){
            $sql .= sprintf(" and workflow_step='%s' ",$step);
        }
        $rows = $this->db()->query($sql);
        if(!empty($rows)){
            foreach($rows as $row){ //获取流程的实际审批人员
                if($row['entrust_user_id']){
                    $ret[] = $row['entrust_user_id'];
                }else{
                    $ret[] = $row['user_id'];
                }
            }
        }
        return $ret;
    }

    //获取工作流的所有审核人员id,包括委托人
    public function getAuditUserIdsByUwfId($userWorkflowId)
    {
        $userWorkflowId = $userWorkflowId - 0;
        if($userWorkflowId < 1){
            return false;
        } 

        $auditUserIds = array();
        $roleIds = array();
        $sql = sprintf("select user_id,role_id,entrust_user_id from user_task where user_workflow_id=%d ",$userWorkflowId);
        $rows = $this->db()->query($sql);
        if(!empty($rows)){
            foreach($rows as $row){
                if($row['user_id'] > 0){
                    $auditUserIds[] = $row['user_id'];
                    if($row['entrust_user_id'] > 0){
                        $auditUserIds[] = $row['entrust_user_id'];
                    }
                }else{
                    $roleIds[] = $row['role_id'];
                }
            }
        }
        if(!empty($roleIds)){
            $roleModel = new Role();
            $userIds = $roleModel->getUserIdByRoleIds($roleIds);
            if(!empty($userIds)){
                $auditUserIds = array_merge($auditUserIds,$userIds);
                $auditUserIds = array_unique($auditUserIds);
            }
        }
        return $auditUserIds;
    }

    //是否是第三方来源的任务
    public function isOnlineOrderSource($userTaskId) {

        $sql = "SELECT  oo.`source`, oo.`source_id` FROM  `user_task` ut, `user_workflow` uw, `online_order` oo WHERE ut.id = {$userTaskId} AND ut.`user_workflow_id`=uw.`id` AND uw.`obj_id` = oo.`id` AND uw.`obj_type`='online_order'  ";

        $rows = $this->db()->query($sql);

        if(empty($rows)) {

            return false;

        }

        return $rows[0];


    }


    public function delByFlowId($flowId) {

        $flowId = intval($flowId);
        if($flowId == 0 ) {

            return false; 

        }

        $sql = "UPDATE user_task SET audit_status = ".OA_WORKFLOW_AUDIT_CANCEL." WHERE user_workflow_id = {$flowId}";

        $is_update = $this->db()->query($sql);

        if($is_update) {

            return true;

        } else {

            return false;
        
        }

    }

    public function isBatchFlow($userworkflowId)
    {
        if(empty($userworkflowId) ) { 
            return false;
        }

        $sql = "SELECT count(id) total FROM user_task WHERE user_workflow_id={$userworkflowId} AND (audit_status = 0 or audit_status = 2)";
        $ret = $this->db()->query($sql);

        if($ret[0]['total'] >= 1) {

            return true; 

        }

        return false;

    }
    
     public function countAstepAuditNumByUserWorkflowId($user_workflow_id)
     {
         $user_workflow_id = intval($user_workflow_id);
 
         $sql = "SELECT count(id) total FROM user_task WHERE user_workflow_id={$user_workflow_id} AND workflow_step='A' AND audit_status = 1";
         $result = $this->db()->query($sql);
 
         
         if( $result[0]['total'] >= 1 ){
 
             return $result[0]['total'];
 
         }   
         
         return false;   
         
     }

     public function updateAuditStatusByuwfId($user_workflow_id)
     {
         $user_workflow_id = intval($user_workflow_id);
 
         $sql = "UPDATE user_task SET audit_status = 1 WHERE user_workflow_id ={$user_workflow_id} AND workflow_step='A' AND audit_status = 0";
         $result = $this->db()->query($sql);
 
         
         if( $result ){
 
             return true;
 
         }   
         
         return false;   
         
     }
    
     public function updateAuditStatusAndDescByuwfId($user_workflow_id,$desc,$user_id) 
     {
         $user_workflow_id = intval($user_workflow_id);
 
         $sql = "UPDATE user_task SET audit_status = 1,deploy_desc={$desc},audit_user_id={$user_id} WHERE user_workflow_id ={$user_workflow_id} 
                 AND workflow_step='A' AND user_id={$user_id} AND audit_status = 0 limit 1";
         Yii::log("update-set".$sql);
         $result = $this->db()->query($sql);
 
         
         if( $result ){
 
             return true;
 
         }   
         
         return false;   
         
     }
 
     public function checkIsExistsNotAuditRecord($user_workflow_id) 
     {
         $user_workflow_id = intval($user_workflow_id);
 
         $sql = "select id from user_task WHERE user_workflow_id ={$user_workflow_id} AND workflow_step='A' 
                 AND audit_status = 0 limit 1";
         Yii::log("update-set".$sql);
         $result = $this->db()->query($sql);
 
         
         if( $result ){
 
             return true;
 
         }   
         
         return false;   
         
     }


     public function getLastAuditRecord($user_workflow_id)
     {
         $user_workflow_id = intval($user_workflow_id);
 
         $sql = "select user_id  from user_task WHERE user_workflow_id ={$user_workflow_id} AND workflow_step='A' AND audit_status = 1 order by id desc limit 1";
         $result = $this->db()->query($sql);
 
         
         if( $result ){
 
             return $result[0]['user_id'];
 
         }   
         
         return false;   
         
     }


}

