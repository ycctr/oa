<?php
class Section extends OaBaseModel
{
    public $table = "section";
    public static $section_users = array();

    //根据用户id获取部门
    public function getSectionByUserId($userId)
    {
        if(empty($userId)){
            return false;
        } 

        $sql = sprintf("select s.* from section s inner join user u on s.id=u.section_id where u.id=%d and s.status = %d",$userId,STATUS_VALID);
        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return $ret[0];
        }
        return false;
    }
    //获取总的记录条数
    public function getCount()
    {
        $strSql = "select count(*) num from section";
        $ret = $this->db()->query($strSql);
        if(empty($ret))
        {
            return 0;
        }
        return $ret[0]['num'];
    }
    //获取所有部门的id和名称的list，用于下拉列选项
    public function getSectionIdNameList($includeRoot = true , $onlyValidate = true)
    {
        $strSql = "select id,name from section";
       /* $strIncludeRoot = "";
        $strOnlyValidate = "";
        if(!$includeRoot)
            $strIncludeRoot = " parent_id != ".ROOT_SECTION_PARENT_ID;*/
        if($onlyValidate)
            $strSql  = $strSql." where status = ".STATUS_VALID;

        /*if(!$includeRoot || $onlyValidate)
            $strSql = $strSql." where ";
        $params = "";
        if(!$includeRoot && $onlyValidate)
            $params = $strIncludeRoot." and ".$strOnlyValidate;
        else
            $params = $strIncludeRoot." ".$strOnlyValidate;
        $strSql = $strSql.$params;*/
        //$ret = $this->db()->query($strSql);
        //$ret = array();
        $result = $this->db()->query($strSql);
        if($includeRoot)
        {
            //array_push($ret,array("id"=>"0","name"=>"无"));
            array_push($result,array('id'=>'0','name'=>'无'));
        }
        if(!empty($result))
        {
            return $result;
        }
        return false;
    }
    //通过部门负责人ID，获取部门名称
    public function getNameByManager_Id($manager_id,$status = STATUS_VALID)
    {
        $strSql = "select id,name from section where manager_id=".$manager_id;
        $ret = $this->db()->query($strSql);
        return $ret;
    }
    //通过部门名称，获取部门id，该函数被调用，判断邮箱是否重复
    public function getIdByName($name,$status = STATUS_VALID)
    {
        $strSql = "select id from section where name='".$name."' and status=".$status;
        $ret = $this->db()->query($strSql);
        return $ret;
    }
    public function getCountWithCondition($Condition = "",$status = STATUS_VALID)
    {
        if($Condition == ""){
            $strSql = "select count(*) num from section t1 left join section t2 on t1.parent_id=t2.id left join user on t1.manager_id=user.id where t1.status=1";  
        }else{
            $strSearch = "t1.name like '%$Condition%' or user.name='".$Condition."' or t2.name like '%$Condition%'";
            $strSql = "select count(*) num from section t1 left join section t2 on t1.parent_id=t2.id left join user on t1.manager_id=user.id where t1.status=1 and (".$strSearch.") ";  
        }
        $ret = $this->db()->query($strSql);
        if(empty($ret)){
            return 0;
        }
        return $ret[0]['num'];

    }
    //过页数，和每页展示的个数，获取对应的记录。用于部门管理中的页面展示
    public function getItemByPageAndPerPageNum($currentPage = 1,$perPageNum = 8,$Condition="",$status = STATUS_VALID)
    {
        if($Condition == ""){
            $strSql = "select t1.*,t2.name as parent_name,user.name as manager_name from section t1 left join section t2 on t1.parent_id=t2.id left join user on t1.manager_id=user.id where t1.status=1 order by t1.id  limit ".(($currentPage-1)*$perPageNum).",".$perPageNum;  
       
        }else{
            $strSearch = "t1.name like '%$Condition%' or user.name='".$Condition."' or t2.name like '%$Condition%'";
            $strSql = "select t1.*,t2.name as parent_name,user.name as manager_name from section t1 left join section t2 on t1.parent_id=t2.id left join user on t1.manager_id=user.id where t1.status=1 and (".$strSearch.") order by t1.id  limit ".(($currentPage-1)*$perPageNum).",".$perPageNum;  
        }
        $ret = $this->db()->query($strSql);

        foreach ($ret as &$v) {
            $section_id = $v['id'];
            $sql = sprintf("select * from user where section_id=%d and status=%d and is_leave=1",$section_id,STATUS_VALID);
            $users = $this->db()->query($sql);
            $v['users'] = $users;
            $sql = sprintf("select count(*) as num from user where section_id=%d and status=%d and is_leave=1",$section_id,STATUS_VALID);
            $count = $this->db()->query($sql);
            if($count){
                $v['user_num'] = $count[0]['num'];
            }else{
                $v['user_num'] = 0;
            }
        }
        return $ret;
    }
    //
    public function getSectionInfoById($id,$status = STATUS_VALID)
    {       
        $strSql = "select t1.id,t1.name as section_name, t1.team_name, t1.create_time,t1.parent_id,t3.name as parent_name,t2.name as manager_name,t2.id_card as manager_id,t1.can_building, t1.senior_building, t1.team_encourage from section as t1 left join user as t2 on t1.manager_id=t2.id left join section t3 on t1.parent_id=t3.id where t1.id=".$id;

        $ret = $this->db()->query($strSql);
        return $ret;
    }
    //
    public function getCertainSectionByName($section_name,$status = STATUS_VALID)
    {
        $strSql = "select id,name,parent_id from section where status=".$status." and name !='".$section_name."'";
        $ret = $this->db()->query($strSql);
        return $ret;
    }

    public function getIdByParentId($tobeDeleteId,$status = STATUS_VALID)
    {
        $strSql = "select id from section where parent_id=".$tobeDeleteId;
        $ret = $this->db()->query($strSql);
        return $ret;
    }
    public function DeleteSectionsByIdList($strDeleteId)
    {
        $ids = explode(',',$strDeleteId);
        if(is_array($ids)){
            foreach($ids as $id){
                if(empty($id)){
                    continue;
                }
                $arrData = array();
                $arrData['id'] = $id; 
                $arrData['status'] = 0;
                $this->save($arrData);
            } 
        }
    }

    //获取该用户的所有下级汇报人
    public function getReportPersonForUser($userId)
    {
        $ret = array();
        if($userId < 1){
            return $ret;
        }
        $sectionIds = array();
        $sql = sprintf("select u.*,s.id as section_id,s.parent_id from section s inner join user u on s.manager_id=u.id where (s.parent_id != 0 or s.manager_id=%d ) and s.status=%d ",$userId,STATUS_VALID);
        $rows = $this->db()->query($sql);
        if(!empty($rows)){
            $rowsMap = array();
            foreach($rows as $row){
                if($row['id'] == $userId){
                    $sectionIds[] = $row['section_id']; //一个人可能是多个部门的领导,数组为部门Id
                }
                $rowsMap[$row['section_id']] = $row;
            } 

            //将所有子部门放入结果集
            foreach($rowsMap as $row){
                $tmp = $row;
                do{
                    if(in_array($tmp['parent_id'],$sectionIds)){
                        $ret[$row['id']] = $row;
                        break;
                    }
                    $tmp = $rowsMap[$tmp['parent_id']];//上级部门
                }while(!empty($tmp));
            }
        }
        return $ret;
    }

    public function isSectionEmpty($section_Id = 0)
    {
        if( $section_Id <= 0)
            return true;
        $arrData = $this->db()->query("select id,parent_id,name from section"); 
        if(empty($arrData))
            return true;
        if(is_array($arrData))
        {   
            $arr = array();
            foreach($arrData as $current)
            {
                    
                $arr["".$current['id']]['id'] = $current['id'];
                $arr["".$current['id']]['parent_id'] = $current['parent_id'];
                $arr["".$current['id']]['name'] = $current['name'];
            }
            $sectionIds = array();
            array_push($sectionIds,$section_Id);
            foreach($arr as $current)
            {
                $currentID = $current['id'];
                if($currentID == $section_Id)
                    continue;
                $flag = true;
                $tmp = $current;
                while($tmp['parent_id'] != 0)
                {        
                    $tmp = $arr["".$tmp['parent_id']];
                    if($tmp['id'] == $section_Id)
                    {
                        array_push($sectionIds,$current['id']);
                        break;
                    }
                }
            }
            $num = 0;
            foreach ( $sectionIds as $section)
            {
                $ret = $this->db()->query("select count(*) num  from user where status=1 and section_id=$section");
                if(intval($ret[0]['num']) != 0)
                {
                    return false;
                }
            }
        }
        return true;
    }

    public function getUserIdsBySectionName($section_name)
    {
        $strSql = sprintf("select user.id from user,section where user.section_id = section.id and section.name='%s'",$section_name);
        $ret  = $this->db()->query($strSql);
        return $ret;
    }

    //获取部门的所有上级部门
    public function getParentSectionBySectionId($section_id)
    {
        if(empty($section_id)){
            return false;
        } 

        $ret = array();
        do{
            $sql = sprintf("select b.* from section a inner join section b on a.parent_id=b.id where a.id=%d and a.status=%d and b.status=%d ",$section_id,STATUS_VALID,STATUS_VALID);
            $rows = $this->db()->query($sql);
            if(!empty($rows)){
                $ret[] = $rows[0]; 
                $section_id = $rows[0]['id'];
            }else{
                $section_id = 0;
            }
        }while($section_id);

        return $ret;
    }

    //获取员工所属部门，包括所有上级部门
    public function getAllSectionIdsByUserId($userId)
    {
        if(empty($userId)){
            return false;
        }

        $sectionIds = array();
        $section = $this->getSectionByUserId($userId);
        if(!empty($section)){
            $sectionIds[] = $section['id'];
            $parentSections = $this->getParentSectionBySectionId($section['id']);
            if(is_array($parentSections)){
                foreach($parentSections as $item)
                    $sectionIds[] = $item['id'];
            }
        }

        return $sectionIds;
    }

    //根据员工Id获取他所领导的有团建权限的部门
    public function getSectionsByManagerId($userId,$sectionId=null)
    {
        if(empty($userId)){
            return false;
        } 

        $sql = sprintf("select * from section where manager_id=%d and status=%d and can_building=1 ",$userId,STATUS_VALID);
        if(!empty($sectionId)){
            $sql .= " and id=$sectionId ";
        }

        $ret = $this->db()->query($sql);
        return $ret;
    }

    //根据员工Id获取他所领导的有部门激励权限的部门
    public function getEncourageByManagerId($userId,$sectionId=null)
    {
        if(empty($userId)){
            return false;
        } 

        $sql = sprintf("select * from section where manager_id=%d and status=%d and team_encourage=1 ",$userId,STATUS_VALID);
        if(!empty($sectionId)){
            $sql .= " and id=$sectionId ";
        }

        $ret = $this->db()->query($sql);
        return $ret;
    }

    //获取所有有团建权限的部门
    public function getAllSectionHasBuilding()
    {
        $sql = sprintf("select * from section where status=%d and can_building=%d ",STATUS_VALID,STATUS_VALID);

        $ret = $this->db()->query($sql);
        return $ret;
    }

    //获取所有有部门激励权限的部门
    public function getAllSectionHasEncourage()
    {
        $sql = sprintf("select * from section where status=%d and team_encourage=%d ",STATUS_VALID,STATUS_VALID);

        $ret = $this->db()->query($sql);
        return $ret;
    }

    //根据员工Id获取他所领导的有团建权限的部门及子部门
    public function getItemsByManagerId($userId,$sectionId=null)
    {
        $ret = array();
        //直属部门
        $zhishu = $this->getSectionsByManagerId($userId);
        if(empty($zhishu)){
            return $ret;
        }
        foreach($zhishu as $item){
            $ret[$item['id']] = $item;
        }
        $allSection = $this->getAllSectionHasBuilding();
        foreach($allSection as $section){
            if(isset($ret[$section['parent_id']])){
                $ret[$section['id']] = $section;
            }
        }
        if($sectionId){
            return isset($ret[$sectionId]);
        }else{
            return $ret; 
        }
    }

    

    //根据员工Id获取他所领导的有激励权限的部门及子部门
    public function getItemsByEncourage($userId,$sectionId=null)
    {
        $ret = array();
        //直属部门
        $zhishu = $this->getEncourageByManagerId($userId);
        if(empty($zhishu)){
            return $ret;
        }
        foreach($zhishu as $item){
            $ret[$item['id']] = $item;
        }
        $allSection = $this->getAllSectionHasEncourage();

        foreach($allSection as $section){
            if(isset($ret[$section['parent_id']])){
                $ret[$section['id']] = $section;
            }
        }
        if($sectionId){
            return isset($ret[$sectionId]);
        }else{
            return $ret; 
        }
    }

    //判断员工是不是某个人的下属员工
    public function isSubordinates($userName,$managerId)
    {
        if(empty($userName) || empty($managerId)){
            return false;
        }

        $sql = sprintf("select user.id from user inner join section on user.section_id=section.id where user.name='%s' and section.manager_id=%d ",$userName,$managerId);
        $result = $this->db()->query($sql);
        if(!empty($result)){
            return $result[0]['id'];
        }
        return false;
    }

    //判断员工是否部门lead
    public function isSectionManager($userId)
    {
        if(empty($userId)){
            return false;
        }
        $ret = $this->getNameByManager_Id($userId);
        if(!empty($ret)){
            return true;
        }
        return false;
    }

    //获取所有团建权限部门的人数
    public function getAllBuildSectionUserNumber()
    {
        $ret = array();
        $type = [4,5];
        $sections = $this->getAllSectionNormalUserName($type);
        foreach($sections as $section){
            if($section['can_building'] == 1){
                $section['users'] = $this->getAllUserOfBuildingSection($section['id']);
                $ret[$section['id']] = $section;
            }
        }
        return $ret;
    }


    //获取所有激励费用
    public function getAllEncourageSectionUserNumber()
    {
        $ret = array();
        $type = [3,4,5];
        $sections = $this->getAllSectionNormalUserName($type);
        foreach($sections as $section){
            if($section['team_encourage'] == 1){
                $section['users'] = $this->getAllUserOfEncourageSection($section['id']);
                $ret[$section['id']] = $section;
            }
        }
        return $ret;
    }

    //获取所有部门附带本部门员工
    // public function getAllSectionUserName()
    // {
    //     if(empty(self::$section_users)){
    //         $data = array();
             
    //         $leave_date = date("Y-m-01");

    //         $sql = "select s.*,u.name as user_name from section s left join user u on s.id=u.section_id  where (u.status=1 or u.status is null) AND (u.leave_date > '{$leave_date}' or u.is_leave =1 or u.is_leave is null) and s.status=1 order by s.id "; //is_leave=1 在职
    //         $ret = $this->db()->query($sql);
    //         foreach($ret as $row){
    //             if(isset($data[$row['id']])){
    //                 if($row['user_name']){
    //                     $data[$row['id']]['users'][] = $row['user_name'];
    //                 }
    //             }else{
    //                 $row['users'] = array();
    //                 if($row['user_name']){
    //                     $row['users'][] = $row['user_name']; 
    //                     unset($row['user_name']);
    //                 }
    //                 $data[$row['id']] = $row; 
    //             } 
    //         }
    //         self::$section_users = $data;
    //     }
    //     return self::$section_users;
    // }

     //获取所有部门附带本部门员工，不包括type
    public function getAllSectionNormalUserName($type)
    {
        if(empty(self::$section_users)){
            $data = array();
             
            $leave_date = date("Y-m-01");

            $sql = "select s.*,u.name as user_name,u.type as user_type from section s left join user u on s.id=u.section_id  where u.status=1 AND (u.leave_date > '{$leave_date}' or u.is_leave =1) and s.status=1 order by s.id "; //is_leave=1 在职
            $ret = $this->db()->query($sql);

            $sql2 = "select s.* from section s where s.status=1 and s.id not in (select s.id from section s left join user u on s.id=u.section_id  where u.status=1 AND (u.leave_date > '{$leave_date}' or u.is_leave =1) and s.status=1)";
            $ret2 = $this->db()->query($sql2);
            foreach($ret as $row){
                if(isset($data[$row['id']])){
                    if($row['user_name']){
                        if(!in_array($row['user_type'],$type)){
                            $data[$row['id']]['users'][] = $row['user_name'];
                        }
                    }
                }else{
                    $row['users'] = array();
                    if($row['user_name']){
                        if(!in_array($row['user_type'],$type)){
                            $row['users'][] = $row['user_name']; 
                        }
                        unset($row['user_name']);
                    }
                    $data[$row['id']] = $row; 
                } 
            }
            foreach ($ret2 as $row) {
                $row['users'] = array();
                $data[$row['id']] = $row;
            }
            self::$section_users = $data;
        }
        return self::$section_users;
    }

    //获取一个团建部门的所有人员
    public function getAllUserOfBuildingSection($sectionId)
    {
        $type = [4,5];
        $sections = $this->getAllSectionNormalUserName($type);
        $section = $sections[$sectionId];
        if($section['can_building'] == 1){
            $child = $this->getAllChildUser($section,$sections);
            return array_merge($section['users'],$child);
        }else{
            return array();
        }
    }

    //获取一个激励部门的所有人员
    public function getAllUserOfEncourageSection($sectionId)
    {
        $type = [3,4,5];
        $sections = $this->getAllSectionNormalUserName($type);
        $section = $sections[$sectionId];
        if($section['team_encourage'] == 1){
            $child = $this->getAllChildUserEncourage($section,$sections);
            return array_merge($section['users'],$child);
        }else{
            return array();
        }
    }

    //递归获取没有团建权限的子部门人员
    public function getAllChildUser($section,$sections)
    {
        $result = array(); 
        foreach($sections as $item){
            if($item['parent_id'] == $section['id'] && $item['can_building'] == 0){
                $child = $this->getAllChildUser($item,$sections);
                $tmp = array_merge($item['users'],$child);
                $result = array_merge($result,$tmp);
            }
        }
        return $result;
    }

    //递归获取没有激励权限的子部门人员
    public function getAllChildUserEncourage($section,$sections)
    {
        $result = array(); 
        foreach($sections as $item){
            if($item['parent_id'] == $section['id'] && $item['team_encourage'] == 0){
                $child = $this->getAllChildUserEncourage($item,$sections);
                $tmp = array_merge($item['users'],$child);
                $result = array_merge($result,$tmp);
            }
        }
        return $result;
    }

    public function getItemByName($name)
    {
        if(empty($name)){
            return false;
        } 
        $strSql = "select * from section where name='".$name."' and status=1 ";
        $ret = $this->db()->query($strSql);
        if(!empty($ret)){
            return $ret[0];
        }
        return false;
    }

    //根据员工Id获取他所领导的有团建权限的部门
    public function getAllSections($userId, $sectionId=null)
    {
        if(empty($userId)){

            return false;

        } 

        if(!empty($sectionId)){

            $whereSql .= " AND id=$sectionId ";
        }

        $status = STATUS_VALID;
        $sql = "SELECT id,name,can_building,team_encourage FROM section WHERE manager_id={$userId} AND status={$status} {$whereSql} ";

        $ret = $this->db()->query($sql);

        if(empty($ret) ) {

            return false;

        }

        return $ret;
    }

    public function getParentId($parentId){

        $sql = "SELECT id, name, can_building, senior_building, team_encourage FROM section WHERE parent_id={$parentId} AND status=1 ";
        $ret = $this->db()->query($sql);

        if(empty($ret) ){

            return false;

        }

        return $ret;

    }

    public function getConditionSection($userId, $condition) {

        if(!empty($condition['senior_building']) ) {
            $senior_building = $condition['senior_building'];
            $whereSql = " AND senior_building = {$senior_building}";
        }
        $sql = "SELECT * FROM section WHERE manager_id={$userId} AND status=1 {$whereSql} ";

        $ret = $this->db()->query($sql);

        if(empty($ret) ){

            return false;

        }

        return $ret;
    }

    //获取某部门及其子部门ID
    public function getSectionIdsById($sectionId)
    {
        $ids = array();                 
        if(empty($sectionId)){
            return $ids;
        }
        $ids[] = $sectionId;
        $sql = "select id,parent_id from section where status=1 and parent_id > 0"; 
        $result = $this->db()->query($sql);
        if(empty($result)){
            return $ids;
        }
        $data = array();
        foreach($result as $item){
            $data[$item['id']] = $item['parent_id'];
        }
        foreach($data as $key => $val){
            if($key == $sectionId || $val == $sectionId){
                $ids[] = $key;
            }else{
                $parentId = $val;
                while(isset($data[$parentId])){
                    if($data[$parentId] == $sectionId){
                        $ids[] = $key;
                        break;
                    }
                    $parentId = $data['parentId'];
                }    
            }
        }
        $ids = array_unique($ids);
        return $ids;
    }

}
