<?php
class CheckInOut extends OaBaseModel
{
    public $table = 'checkinout';

    public function getListByUserId($userId,$start_day='',$end_day='',$start=0,$limit=22)
    {
        if(empty($userId)){
            return false;
        } 

        $sql = sprintf("select * from checkinout where user_id=%d ",$userId);
        if($start_day){
            $sql .= sprintf(" and unix_timestamp(date) >= %d ",strtotime($start_day));
        }
        if($end_day){
            $sql .= sprintf(" and unix_timestamp(date) <= %d ",strtotime($end_day));
        }

        $sql .= sprintf(" order by date desc limit %d,%d ",$start,$limit);

        $result = $this->db()->query($sql);
        if(!empty($result)){
            foreach($result as &$row){

                $holiday = VacationService::isHoliday($row['date']);

                if($holiday){
                    if(strtotime($row['checkout_time']) - strtotime($row['checkin_time']) >=14400){
                        $row['abnormal'] = true;
                    }
                }elseif($row['checkin_time'] == '0000-00-00 00:00:00' 
        || $row['checkout_time'] == '0000-00-00 00:00:00' 
        || (strtotime($row['checkin_time']) > strtotime(date('Y-m-d',strtotime($row['checkin_time']))." 10:06:00") && date('Y-m-d',strtotime($row['checkin_time'])) > '2016-11-01') 
        || (strtotime($row['checkin_time']) > strtotime(date('Y-m-d',strtotime($row['checkin_time']))." 10:30:00") && date('Y-m-d',strtotime($row['checkin_time'])) < '2016-11-02') 
        || (strtotime($row['checkout_time']) < strtotime(date('Y-m-d',strtotime($row['checkout_time']))." 17:30:00") && date('Y-m-d',strtotime($row['date'])) < '2016-11-30')
        || (strtotime($row['checkout_time']) < strtotime(date('Y-m-d',strtotime($row['checkout_time']))." 18:00:00") && date('Y-m-d',strtotime($row['date'])) > '2016-11-29')){
                    $row['abnormal'] = true; 
                }

                $preDay = date("Y-m-d",strtotime('-1 day',strtotime($row['date'])));
                $preCheckinout = $this->getItemByUserIdDate($row['user_id'],$preDay);
                if($preCheckinout){
                    $preCheckoutTime = $preCheckinout['checkout_time'];
                }
                if((date('Y-m-d',strtotime($row['date'])) >= '2016-12-21') && (strtotime($preCheckoutTime) > strtotime($preDay." 22:00:00"))
                && (strtotime($row['checkin_time']) < strtotime(date('Y-m-d',strtotime($row['date']))." 10:30:00")) 
                && (strtotime($row['checkout_time']) > strtotime(date('Y-m-d',strtotime($row['date']))." 18:00:00"))){
                    $row['abnormal'] = false;
                }
            }
        }

        return $result;
    }

    public function getListCntByUserId($userId,$start_day='',$end_day='')
    {
        if(empty($userId)){
            return false;
        } 

        $sql = sprintf("select count(*) as cnt from checkinout where user_id=%d ",$userId);
        if($start_day){
            $sql .= sprintf(" and unix_timestamp(date) >= %d ",strtotime($start_day));
        }
        if($end_day){
            $sql .= sprintf(" and unix_timestamp(date) <= %d ",strtotime($end_day));
        }
        $result = $this->db()->query($sql);
        if(!empty($result)){
            return $result[0]['cnt'];
        }
    }

    //按时期导出考勤记录
    public function getListByDate($start_day,$end_day)
    {
        if(empty($start_day) && empty($end_day)){
            return false;
        }
        $sql = "select c.*,u.name as user_name,u.id_card,s.name as section_name from checkinout c inner join user u on c.user_id=u.id inner join section s on u.section_id=s.id where 1=1 ";
        if($start_day){
            $sql .= sprintf(" and unix_timestamp(date) >= %d ",strtotime($start_day));
        }
        if($end_day){
            $sql .= sprintf(" and unix_timestamp(date) <= %d ",strtotime($end_day));
        }

        $sql .= " order by c.user_id asc,id asc ";

        $result = $this->db()->query($sql);
        return $result;
        
    }

    //按时期导出考勤记录
    public function getListBySearch($startDate, $endDate, $sectionName, $userName, $idCard, $page, $cnt)
    {
        if(empty($startDate) || empty($endDate)){
            return false;
        }
        $endDate = date('Y-m-d',strtotime($endDate) + 86400);
        if($cnt == 1){
            $sql = "select count(*) as total ";
        }else{
            $sql = "select c.*,u.name as user_name,u.id_card,s.name as section_name ";
        }

        $sql .= " from checkinout c, user u, section s where u.section_id=s.id and c.user_id=u.id ";
        if($startDate){
            $sql .= sprintf(" and date >= '%s' ",$startDate);
        }
        if($endDate){
            $sql .= sprintf(" and date <= '%s' ",$endDate);
        }
        if($idCard){
            $sql .= sprintf(" and u.id_card=%d ",$idCard);
        }
        if($sectionName){
            $sql .= sprintf(" and s.name like '%%%s%%' ",$sectionName);
        }
        if($userName){
            $sql .= sprintf(" and u.name like '%%%s%%' ",$userName);
        }

        $sql .= " and (c.date<=u.leave_date or u.leave_date is null) ";
        $sql .= " order by c.user_id asc,date asc ";
        $sql .= $cnt ? '' : ' limit ' . ($page - 1) * 20 . ',20';

        $result = $this->db()->query($sql);
        if($cnt == 1){
            return $result[0]['total'];
        }
        return $result;

    }

    //获取员工某天的考勤
    public function getItemByUserIdDate($userId,$day)
    {
        if(empty($userId) || empty($day)){
            return false;
        } 

        $sql = sprintf("select * from checkinout where user_id=%d and unix_timestamp(date)=%d ",$userId,strtotime($day));
        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return $ret[0];
        }
        return false;
    }

    //按时期导出考勤记录
    public function getListByStartDayAndEndDay($start_day,$end_day)
    {
        if(empty($start_day) && empty($end_day)){
            return false;
        }
        $sql = "select * from checkinout where 1=1 ";
        if($start_day){
            $sql .= sprintf(" and unix_timestamp(date) >= %d ",strtotime($start_day));
        }
        if($end_day){
            $sql .= sprintf(" and unix_timestamp(date) <= %d ",strtotime($end_day));
        }

        $sql .= " order by user_id asc,date asc ";

        $result = $this->db()->query($sql);
        return $result;
    }

    //获取某一天
    public function getCheckInOutList($condition) {

        if($condition['date']) {

            $date = $condition['date'];
            $whereSql = " AND c.date = '$date' "; 

        }

        if($condition['gt_checkin_time'] && $condition['lt_checkout_time']) {
            
            $checkinInTime = $condition['gt_checkin_time'];
            $checkinOutTime = $condition['lt_checkout_time'];
            $whereSql .= " AND ( c.checkin_time > '{$checkinInTime}' OR c.checkout_time < '{$checkinOutTime}' ) "; 

        }

        //0是没处理过的打卡记录
        $sql = "SELECT c.user_id user_id,u.level level, u.email email FROM checkinout c, user u WHERE 1=1 {$whereSql} AND c.absenteeism_type = 0 AND u.id=c.user_id AND u.is_leave = 1 ";

        $result = $this->db()->query($sql);
        
        return $result;

    }

    //节假日获取考勤异常的数据
    public function getExcptionListHoliday($day)
    {
        $sql = "SELECT c.user_id user_id,u.level level, u.email email from checkinout c inner join user u on c.user_id=u.id where c.checkin_time!='0000-00-00 00:00:00' and c.checkout_time!='0000-00-00 00:00:00' and (unix_timestamp(c.checkout_time)-unix_timestamp(c.checkin_time))>=3600*4 and c.absenteeism_type = 0 AND u.is_leave = 1 and c.date='$day' ";
        $result = $this->db()->query($sql);
        return $result;
    }
	
	public function getCheckinoutCycle()
	{
	   $now_day = date("d");
	   settype($now_day,"int");
	   $result_date = array();

	   if($now_day < 26 ){
		   $result_date['start_date'] = date('Y-m-26',strtotime("-1  Months"))." 00:00:00";
		   $result_date['end_date']   = date('Y-m-25',time())." 23:59:59";
	   }else{
		   $result_date['start_date'] = date('Y-m-26',time())." 00:00:00";
		   $result_date['end_date']   = date('Y-m-25',strtotime("+1  Months"))." 23:59:59";
	   }
	   return $result_date;
	}

}
?>
