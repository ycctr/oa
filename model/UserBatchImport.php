<?php
class UserBatchImport{

    public static  $userExcelPairs = array(
        '工号'=> 'id_card', '公司邮箱'=> 'email', '手机'=> 'mobile',
        '姓名'=> 'name', '性别'=> 'sex', '分部'=> 'section_id'
    );

    public function getPairs($title = null)
    {
        if(!is_array($title))
            return array();

        foreach($title as $key=>$value)
        {
            if ( self::$userExcelPairs["$value"] != null)
            {
                $pairs["$key"] = self::$userExcelPairs["$value"];
            }
        }
        return $pairs;
    }
    //加入offset、limit，防止中途出现错误，重新运行脚本。
    //offset表示excel中开始要加入的行号。
    //limit表示，从offset开始要加入的行数。
	public function Import($file,$offset = 2,$limit = 99999){
        $str = "";
        $reader = new ExcelReader($file);
        $dataList = $reader->GetDataList();
        //var_dump($dataList);
        if(empty($dataList))
            return;
        
        $pairs = self::getPairs($dataList[1]);
        var_dump($pairs);;
        for($intCurrentRow = $offset;$intCurrentRow<($offset+$limit) && $intCurrentRow<=count($dataList);$intCurrentRow++)
        {
            $data = $dataList[$intCurrentRow];
            $arrData = array();
            foreach($pairs as $key=>$value)
            {
                $arrData["$value"] = $data["$key"];
            }
            if(is_null($arrData['id_card']))
            {
                $str = $str."finish,all：".($intCurrentRow-$offset)."\n";
                return $str;
            }
           //通过部门名称，获取部门id
            $model = new Section();
            $strSql = sprintf("select id from section where name='%s'",$arrData['section_id']);
            $section_name = $arrData['section_id'];
            $result = $model->db()->query($strSql);
            if(empty($result))
            {
                $str = $str."无当前部门，脚本停止。工号：".$arrData['id_card']."\t 部门：$section_name\n";
                return $str;
            }
            //对姓名、部门、性别进行处理
            if(strstr($arrData['name'],"(")||strstr($arrData['name'],"（"))
            {
                $Leftpos_e = stripos($arrData['name'],"(");
                $Leftpos_c = stripos($arrData['name'],"（");
                $Leftpos = $Leftpos_e ? $Leftpos_e : $Leftpos_c;
                $span = $Leftpos_e ? 1:3;
                $Rightpos_e = stripos($arrData['name'],")");
                $Rightpos_c = stripos($arrData['name'],"）");
                $Rightpos = $Rightpos_e ? $Rightpos_e : $Rightpos_c;
                $arrData['name'] = mb_substr($arrData['name'],$Leftpos+$span,$Rightpos-$Leftpos-$span);
            }
            $arrData['section_id'] = $result[0]['id'];
            if($arrData['sex'] == "男")
            {
                $arrData['sex'] = 1;
            }
            else
            {
                $arrData['sex'] = 0;
            }
            $str = $str. ($intCurrentRow-$offset+1).":".$arrData['name']."--".$arrData['id_card']."--".$arrData['sex']."--".$arrData['email']."--".$arrData['mobile']."--".$section_name."(".$arrData['section_id'].")";
            $arrData['create_time'] = time();
            $arrData['modify_time'] = time();
            $arrData['status'] = 1;
            //处理email,去掉@以及后面的字符
            $pos = stripos($arrData['email'],"@");
            $arrData['email'] = substr($arrData['email'],0,$pos);
            $model = new User();
            //$bool = $model->save($arrData);
            $bool = true;
            if($bool)
            {
                $str = $str."    成功.\n";
            }
            else
            {
                $str = $str."    失败.请检查邮箱或者工号是否有重复。\n";
                return $str;
            }
            //echo "\n";
    //        echo $str;
        }
        $str = $str."finish ,all ：".($limit<(count($dataList)+1)?$limit:count($dataList)-$offset+1)."\n";
        return $str;
	}
}

