<?php
class CancelBusiness extends OaBaseModel
{
    public $table = 'cancel_business';

    public function getItemByUserIdAndDate($userId,$date)
    {
        if(empty($userId) || empty($date)){
            return false;
        } 
        $time = strtotime($date);
        
        $sql = sprintf("select a.* from user_workflow uwf inner join cancel_business a on uwf.obj_id=a.id and uwf.obj_type='cancel_business' where uwf.user_id=%d and uwf.audit_status=%d and uwf.status=%d ",$userId,OA_WORKFLOW_AUDIT_PASS,STATUS_VALID);
        $sql .= " and unix_timestamp(from_unixtime(a.start_time,'%Y%m%d')) <=".$time." and unix_timestamp(from_unixtime(end_time,'%Y%m%d')) >=".$time." and a.status=".STATUS_VALID;

        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return $ret[0];
        }
        return false;
    }

    //判断用户在一个时间段内是否已经有销假记录
    public function isHaveRecord($userId,$start_time,$end_time)
    {
        if(empty($userId) || empty($start_time) || empty($end_time)){
            throw new CHttpException(404,'参数错误，联系技术人员');
        } 
        
        $sql = sprintf("select count(*) as cnt from user_workflow uwf inner join cancel_business a on uwf.obj_id=a.id and uwf.obj_type='cancel_business' where uwf.user_id=%d and uwf.audit_status!=%d and uwf.status=%d ",$userId,OA_WORKFLOW_AUDIT_REJECT,STATUS_VALID);
        $sql .= sprintf(" and ((a.start_time <= %d and a.end_time >= %d) or (a.start_time <= %d and a.end_time >= %d) or (a.start_time >= %d and a.end_time <= %d)) ",$start_time,$start_time,$end_time,$end_time,$start_time,$end_time);

        $ret = $this->db()->query($sql);
        if(!empty($ret) && $ret[0]['cnt'] > 0){
            return true;
        } 
        return false;
    }

    //获取某时间段审批通过的请假记录
    public function getItemsByDay($startDay,$endDay)
    {
        $startTime = strtotime($startDay);
        $endTime = strtotime($endDay);
        $zero = strtotime('2015-07-26');
        $sql = sprintf("select a.* from user_workflow uwf inner join cancel_business a on uwf.obj_id=a.id and uwf.obj_type='cancel_business' where uwf.audit_status=%d and uwf.status=%d ",OA_WORKFLOW_AUDIT_PASS,STATUS_VALID);
        $sql .= " and uwf.modify_time >=". $startTime . " and uwf.modify_time < ". $endTime ." and a.status=".STATUS_VALID;
        $sql .= " and a.end_time >= $zero";
        $ret = $this->db()->query($sql);
        return $ret; 
    }

    //条件查询
    public function getExcelPrint($conditionInfo) {
        $start = $conditionInfo['startTime'];
        $end   = $conditionInfo['endTime'];

        $whereSql = "";

        //时间条件
        if(!empty($start) && !empty($end) ) {
            if($start > $end ) {
                $startTime = $end;
                $endTime   = $start;
            }else if($start < $end) {
                $startTime = $start;
                $endTime   = $end;
            }else {
                $startTime = $start;
                $endTime   = $end + 86400;
            }
            $whereSql = " AND start_time >= {$startTime} AND end_time <= {$endTime}";
        }

        //姓名，部门条件
        if($conditionInfo['uName'] != "" ) {
            $uName = $conditionInfo['uName'];
            $whereSql .= sprintf(" AND u.name like '%%%s%%' ",$uName);
        } 
        if($conditionInfo['sName']) {
            $sName = $conditionInfo['sName'];
            $whereSql .= sprintf(" AND s.name like '%%%s%%' ",$sName);
        }
        if($conditionInfo['idCard']){
            $whereSql .= sprintf(" and u.id_card=%d ",$conditionInfo['idCard']);
        }

        
        $sql = "SELECT u.`id_card`, u.name AS uname,  s.`name` sName, c.`start_time`, c.`end_time`, c.`day_number`, c.`leave_reason`, uwf.audit_status FROM `cancel_business` c, `section` s, `user` u, user_workflow uwf WHERE  c.`user_id` = u.`id` {$whereSql} AND u.`section_id` = s.`id` AND c.status = 1 AND uwf.obj_id = c.id AND uwf.obj_type = 'cancel_business' ORDER BY u.id ";

        $ret = $this->db()->query($sql);

        $data = array();
        foreach($ret as $row) {

            $row['start_time'] = date("Y-m-d", $row['start_time']);
            $row['end_time'] = date("Y-m-d", $row['end_time']);

            switch($row['audit_status']) {

                case OA_WORKFLOW_AUDIT_CHUANGJIAN:
                    $audit_status = '创建工作流';break;
                case OA_WORKFLOW_AUDIT_SHENPIZHONG:
                    $audit_status = '审批中';break;
                case OA_WORKFLOW_AUDIT_PASS:
                    $audit_status = '通过';break;
                case OA_WORKFLOW_AUDIT_REJECT:
                    $audit_status = '拒绝';break;
            }
            $row['audit_status'] = $audit_status;
            $data[] = $row;

        }

        return $data; 

    }


}
?>
