<?php
class WorkFlow extends OaBaseModel
{
    public $table = 'workflow';

    public function getItemsOrderByCategory()
    {
        $ret = array();
        $sql = sprintf("select * from workflow where status=%d order by category asc, display_order asc ",STATUS_VALID);
        $rows = $this->db()->query($sql);
        if(!empty($rows)){
            foreach($rows as $row){
                $ret[$row['category']][] = $row;
            }
        }
        return $ret;
    }

    public function getObjType($obj_id, $obj_type) {

        if(empty($obj_id) || empty($obj_type)) {

            return false;
        
        }
        
        $table = $obj_type;
        $sql = "SELECT * FROM {$table} WHERE id = {$obj_id} limit 1";
        $ret = $this->db()->query($sql);

        if(empty($ret) ) {

            return false;

        }


        if($obj_type  == "daily_reimbursement") {

            $title = $ret[0]['remark'];

        } elseif($obj_type == 'absence') {

            if($ret[0]['type'] == VACATION_CHUCHAI) {

                $typeStr = "出差";

            } elseif($ret[0]['type'] == VACATION_YINGONGWAICHU) {

                $typeStr = "因公外出";

            } else {

                $typeStr = "请假";

            }

            $title = date("Y-m-d", $ret[0]['start_time'])." -- ".date("Y-m-d", $ret[0]['end_time'])."：{$typeStr}{$ret[0]['day_number']}天。"; 
        }elseif($obj_type == 'ticket_book'){
            if($ret[0]['status'] == TICKET_BOOK){
                $title = '订票';
            }elseif($ret[0]['status'] == TICKET_REFUND){
                $title = '退票';
            }elseif($ret[0]['status'] == TICKET_ENDORSE){
                $title = '改签';
            }
        } else {

            $title = $ret[0]['title'];


        }


        return $title;

    }

    public function getIdByName($name = ''){
        $strSql = "select `id` from {$this->table} where `name`='{$name}'";
        $arrData = $this->db()->query($strSql);
        return $arrData[0]['id'];
    }

}

?>
