<?php
/**
 * Created by PhpStorm.
 * User: songyuzhuo
 * Date: 2016/5/25
 * Time: 14:07
 * 员工入职表
 */

class Dimission extends OaBaseModel{

    public $table = 'dimission';

    //根据状态获取离职记录
    public function getDimissionsByStatus($start, $rn, $status){
        $strSql = "select * from `user_workflow` uwf, {$this->table} d 
                    where uwf.obj_type='{$this->table}' and d.id=uwf.obj_id  and ";
        $status = $status ? 'uwf.audit_status=' : 'uwf.audit_status!=';
        $status .= OA_WORKFLOW_AUDIT_PASS;
        $strSql .= $status;
        $strSql .= " limit $start, $rn ";
        $arrData = $this->db()->query($strSql);
        $userModel = new User();
        $sectionModel = new Section();
        foreach ($arrData as &$item){
            $user = $userModel->getItemByPk($item['user_id']);
            $item['name'] = $user['name'];
            $item['id_card'] = $user['id_card'];
            $item['mobile'] = $user['mobile'];
            $item['level'] = $user['level'];
            $section = $sectionModel->getItemByPk($user['section_id']);
            $item['section_name'] = $section['name'];
            $manager_id = $section['manager_id'];
            $manager = $userModel->getItemByPk($manager_id);
            $item['manager_name'] = $manager['name'];
        }
        return $arrData;
    }

    //根据状态获取记录条数
    public function getDimissionCntByStatus($status=0){
        $strSql = "select count(*) as cnt from `user_workflow` where obj_type='{$this->table}' and ";
        $status = $status ? 'audit_status=' : 'audit_status!=';
        $status .= OA_WORKFLOW_AUDIT_PASS;
        $strSql .= $status;
        $arrData = $this->db()->query($strSql);
        return $arrData[0]['cnt'];
    }
}