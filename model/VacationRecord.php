<?php
class VacationRecord extends OaBaseModel
{
    public $table = 'vacation_record';


    //当前请假还能申请的天数(显示人力导入的最后一条记录)
    public function getHolidayQuantity($user_id,$absence_type)
    {
        if(empty($user_id) || empty($absence_type)){
            return false;
        } 

        $sql = sprintf("select sum(day_num) as number from vacation_record where type=1 and absence_type=%d and status=%d and user_id=%d ",$absence_type,STATUS_VALID,$user_id);
        // $sql = sprintf("select day_num as number from vacation_record where absence_type=%d and status=%d and user_id=%d and type=2 order by id desc limit 1 ",$absence_type,STATUS_VALID,$user_id);

        $result = $this->db()->query($sql);
        if($result){
            return $result[0]['number']; 
        }
        return false;
    }

    //获取员工某日是否全天休假
    public function getVacationItemByUserIdAndDate($userId,$date)
    {
        if(empty($userId) || empty($date)){
            return false;
        } 
        $date = date('Y-m-d',strtotime($date));

        $sql = sprintf("select * from vacation_record where user_id=%d and date='%s' and status=%d and day_num = -1 and absence_id > 0 ",$userId,$date,STATUS_VALID);
        $result = $this->db()->query($sql);
        if($result){
            return reset($result);
        }
        return false;
    }

    //有当日无效的记录则更新，无则插入
    public function duplicateUpdate($absence) {

        if(empty($absence)){
            return;
        } 

        $selSql = sprintf("select * from vacation_record where user_id=%d and date='%s' and absence_id > 0 and status=%d  ",$absence['user_id'], $absence['date'], STATUS_INVALID);

        $ret = $this->db()->query($selSql);

        if(!empty($ret)){

            $arrData = reset($ret);
            $absence['id'] = $arrData['id'];

        }

        $id = $this->save($absence);

        if(!$id){

            Yii::log('vacation_record save fail:'.print_r($absence,true),'info');

        }

    }

    //获取员工年假，病假记录统计
    public function getListByUserIdTotal($userId, $selectNum=1)
    {
        if(empty($userId)){
            return false;
        } 

        if($selectNum == 2) {
            $absenceTypSql = " AND vr.absence_type = ".VACATION_NIANJIA;
        } elseif($selectNum == 3) {
            $absenceTypSql = " AND vr.absence_type = ".VACATION_BINGJIA;
        }

        //年假和病假
        $sql = sprintf("select COUNT(vr.id) total from vacation_record vr left join user_workflow uwf on vr.absence_id=uwf.obj_id and uwf.obj_type='absence' where vr.user_id=%d and vr.status=%d and vr.remark!='xiuzheng' and vr.type=1 and vr.absence_type<=12 and (vr.absence_id=0 or (vr.day_num<0 and unix_timestamp(vr.date)>=1480089600)) {$absenceTypSql} order by vr.absence_type desc, vr.date desc ",$userId,STATUS_VALID);

        $ret = $this->db()->query($sql);

        if(!empty($ret[0]['total'])) {

            return $ret[0]['total'];

        }

        return false;
    }


    //获取员工所有的假期记录
    public function getListByUserId($userId, $start=0,$limit=null, $selectNum=1)
    {
        if(empty($userId)){
            return false;
        } 

        if($selectNum == 2) {
            $absenceTypSql = " AND vr.absence_type = ".VACATION_NIANJIA;
        } elseif($selectNum == 3) {
            $absenceTypSql = " AND vr.absence_type = ".VACATION_BINGJIA;
        }

        //年纪和病假
        $sql = sprintf("select vr.*,uwf.id as user_workflow_id from vacation_record vr left join user_workflow uwf on vr.absence_id=uwf.obj_id and uwf.obj_type='absence' where vr.user_id=%d and vr.status=%d and vr.remark!='xiuzheng' and vr.type=1 and vr.absence_type<=12 and (vr.absence_id=0 or (vr.day_num<0 and unix_timestamp(vr.date)>=1480089600)) {$absenceTypSql} order by vr.date desc, vr.absence_type desc ",$userId, STATUS_VALID);

        if(!empty($limit)){
            $sql .= "limit $start,$limit";
        }

        $ret = $this->db()->query($sql);

        return $ret;
    }

    //获取员工的假期记录
    public function getListByAll($condition, $start=0, $limit="") {

        $select_num   = $condition['select_num'];
        $start_date  = $condition['start_date'];
        $end_date    = $condition['end_date'];
        $search      = $condition['search'];


        $whereSql = "";
        if($select_num == 1) {

            $whereSql = " AND vr.absence_type IN (1,2) ";

        } elseif($select_num == 2) {

            $whereSql = " AND vr.absence_type = ".VACATION_NIANJIA;

        } elseif($select_num == 3) {

            $whereSql = " AND vr.absence_type = ".VACATION_BINGJIA;

        }


        if( !empty($start_date) && !empty($end_date) ) {

            if($start_date == $end_date) {

                $whereSql .= " AND  vr.date = '{$start_date}' ";

            }else if($start_date > $end_date) {

                $whereSql .= " AND vr.date <= '$start_date' AND vr.date >= '$end_date' ";

            } else {

                $whereSql .= " AND vr.date >= '$start_date' AND vr.date <= '$end_date' ";

            }

        }

        //名字，id，工号，email前缀
        if(!empty($search) ) {

            $whereSql .= " AND (u.name='{$search}' or u.id=".intval($search)." or u.id_card='{$search}' or u.email='".$search."' or s.name='{$search}' )";

        }


        $limit_str = "";

        if(!empty($limit)){

            $limit_str = "limit {$start},{$limit}";

        }

        $status = STATUS_VALID;


        //年假和病假
        $sql = "SELECT u.id_card, u.name uname, s.name sname, vr.* FROM vacation_record vr , user u,section s WHERE  vr.user_id=u.id  AND u.section_id = s.id AND vr.status={$status} and u.is_leave = 1 and vr.type=1 {$whereSql} ORDER BY vr.absence_type DESC, vr.date DESC {$limit_str}";
        // echo $sql;

        $totalSql = "SELECT count(*) total FROM vacation_record vr , user u,section s WHERE  vr.user_id=u.id  AND u.section_id = s.id AND vr.status={$status} AND u.is_leave=1  {$whereSql}";
        $countRes = $this->db()->query($totalSql); 

        $ret = $this->db()->query($sql);


        $data['ret'] = $ret;
        $data['total'] = $countRes[0]['total'];

        return $data;

    }

    public function getTotalNumByUserId($userId )
    {
        if(empty($userId)){
            return false;
        } 

        $sql = sprintf("select count(*) as total,sum(case when absence_type=1 then day_num else 0 end) as bingjia_total,sum(case when absence_type=2 then day_num else 0 end) as nianjia_total from vacation_record where user_id=%d and status=%d and type=1 ",$userId,STATUS_VALID);
        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return reset($ret);
        }
    }

    //删除某审批通过时间包含之后的所有休假记录
    public function delRowByUserIdAndDate($userId,$date,$type="")
    {
        if(empty($userId) || empty($date)){
            return;
        } 

        if($type == VACATION_CHUCHAI) {

            $strType = " AND  absence_type = {$type} ";


        } else {

            $type = VACATION_CHUCHAI;
            $strType = " AND  absence_type <> {$type} ";

        }
        $sql1 = sprintf('select vr.id from vacation_record vr left join user_workflow uwf on vr.absence_id=uwf.obj_id and obj_type="absence" where uwf.status=%d and vr.status=%d and vr.user_id=%d and uwf.modify_time>=%d',STATUS_VALID,STATUS_VALID,$userId,$date);
        $ret1 = $this->db()->query($sql1);
        $ids = array();
        foreach ($ret1 as $key => $value) {
            array_push($ids,$value['id']);
        }
        $ids = implode(",",$ids);
        $sql2 = sprintf('delete from vacation_record where id in ('.$ids.')');
        // $sql = sprintf("update vacation_record set status=%d where user_id=%d and date >= '%s' and absence_id!=0 {$strType} and type=1 ",STATUS_INVALID,$userId,$date);
        $this->db()->query($sql2);
    }

    //删除某请假单时间包含之后的所有休假记录
    public function delRowByUserIdAndDate2($userId,$date,$type="")
    {
        if(empty($userId) || empty($date)){
            return;
        } 

        if($type == VACATION_CHUCHAI) {

            $strType = " AND  absence_type = {$type} ";


        } else {

            $type = VACATION_CHUCHAI;
            $strType = " AND  absence_type <> {$type} ";

        }
        $sql1 = sprintf('select vr.id from vacation_record vr left join user_workflow uwf on vr.absence_id=uwf.obj_id and obj_type="absence" where uwf.status=%d and vr.status=%d and vr.user_id=%d and unix_timestamp(vr.date)>=%d',STATUS_VALID,STATUS_VALID,$userId,$date);
        $ret1 = $this->db()->query($sql1);
        $ids = array();
        foreach ($ret1 as $key => $value) {
            array_push($ids,$value['id']);
        }
        $ids = implode(",",$ids);
        $sql2 = sprintf('delete from vacation_record where id in ('.$ids.')');
        // $sql = sprintf("update vacation_record set status=%d where user_id=%d and date >= '%s' and absence_id!=0 {$strType} and type=1 ",STATUS_INVALID,$userId,$date);
        $this->db()->query($sql2);
    }

    //删除某日包含之后的所有休假记录
    public function delChuChaiRowByUserIdAndDate($userId, $start_time, $end_time)
    {

        $type = VACATION_CHUCHAI;
        $strType = " AND  absence_type = {$type} ";
        $start_date = date("Y-m-d", $start_time);
        $end_date   = date("Y-m-d", $end_time);

        $sql = "UPDATE vacation_record SET status = 0  WHERE user_id={$userId} {$strType}  AND date >= '{$start_date}' AND date <= '{$end_date}' and absence_id != 0 and type=1 ";

        $this->db()->query($sql);
    }

    //获取个人是多少天的年假记录
    public function getItemByUserIdAndDayNum($userId,$day_num)
    {
        if(empty($userId)){
            return false;
        }

        $sql = sprintf("select * from vacation_record where user_id=%d and day_num=".$day_num." and absence_type=2 and type=1 ",$userId);
        $result = $this->db()->query($sql);
        if(!empty($result)){
            return reset($result);
        }
        return false;
    }   

    public function getAllVaction($date) {

        $date = intval($date);
        $date = date("Y-m-d", $date);

        $sql = "SELECT u.name name, s.name s_name, v.user_id id, u.id_card, absence_type type, sum(v.day_num) total FROM vacation_record v, user u, section s  WHERE v.user_id = u.id AND u.section_id = s.id AND v.date <= '{$date}' AND v.absence_type in (1,2) and v.type=1 GROUP BY v.user_id,v.absence_type "; 
        $result = $this->db()->query($sql);

        
        if(!empty($result)){

            $res = array();
            foreach($result as $data) {

                $id = $data['id'];
                $type = $data['type'];
                $res[$id]['name'] = $data['name'];
                $res[$id]['card'] = "{$data['id_card']}";
                $res[$id]['setion'] = $data['s_name'];


                if($type == 2) {

                    $res[$id]['nianjia'] = $data['total'];

                } else {

                    $res[$id]['bingjia'] = $data['total'];

                }

            }


            return $res;
        }

        return false;

    }

    //按时期获取休假记录
    //absence_id 必须有值 是系统根据请假条生成的记录
    public function getListByStartDayandEndDay($startDay,$endDay)
    {
        if(empty($startDay) || empty($endDay)){
            return false;
        }    
        $sql = "select * from vacation_record where absence_id > 0 and date >= '$startDay' and date <= '$endDay' and type=1 order by user_id asc,absence_type asc ";

        $result = $this->db()->query($sql);
        return $result;
    }

    //读出剩余的假期
    public function getOverplusVacation($condition) {

        $whereSql = "";
        $absence_type = $condition['absence_type'];
        $userId       = $condition['user_id'];
        $ledate       = $condition['ledate'];
        
        if(!empty($userId) ) {

            $whereSql .=  " AND user_id = {$userId} ";

        }

        if(!empty($ledate)) {

            $whereSql .=  " AND date < '{$ledate}' ";
        }

        $sql = "SELECT user_id, SUM(day_num) day_num  FROM  `vacation_record` WHERE absence_type={$absence_type} {$whereSql}  GROUP BY user_id "; 

        $result = $this->db()->query($sql);

        return $result;

    }

    public function getSurplusJiaqi($userId)
    {
        $ret = array();
        if($userId == 0){
            return $ret;
        }
        // $sql1 = sprintf("select * from vacation_record where user_id=%d and status=%d and type=2 and absence_type=1 order by id desc limit 1",$userId,STATUS_VALID);
        $sql1 = sprintf("select sum(day_num) as day_num,date,absence_type from vacation_record where user_id=%d and status=%d and type=1 and absence_type=1",$userId,STATUS_VALID);
        // $sql2 = sprintf("select * from vacation_record where user_id=%d and status=%d and type=2 and absence_type=2 order by id desc limit 1",$userId,STATUS_VALID);
        $sql2 = sprintf("select sum(day_num) as day_num,date,absence_type from vacation_record where user_id=%d and status=%d and type=1 and absence_type=2",$userId,STATUS_VALID);
        $result1 = $this->db()->query($sql1);
        $result2 = $this->db()->query($sql2);
        if(is_array($result1) && is_array($result2)){
            $result = array_merge($result1,$result2);
        }

        if(!empty($result)){
            foreach($result as $item){
                if($item['absence_type'] == VACATION_BINGJIA){
                    $ret['bingjia_total'] = $item['day_num'];
                }elseif($item['absence_type'] == VACATION_NIANJIA){
                    $ret['date'] = $item['date'];
                    $ret['nianjia_total'] = $item['day_num'];
                }
            }
        }

        return $ret;
    }

    //获取员工某条请假单扣假情况
    public function getdeductionNianOrBing($absence_id){
        $sql1 = sprintf('select sum(day_num) as nian_num from vacation_record where absence_id=%d and type=1 and status=%d and absence_type=%d',$absence_id,STATUS_VALID,VACATION_NIANJIA);
        $ret1 = $this->db()->query($sql1);
        $ret['nian'] = !empty($ret1)?-$ret1[0]['nian_num']:0;
        $sql2 = sprintf('select sum(day_num) as bing_num from vacation_record where absence_id=%d and type=1 and status=%d and absence_type=%d',$absence_id,STATUS_VALID,VACATION_BINGJIA);
        $ret2 = $this->db()->query($sql2);
        $ret['bing'] = !empty($ret2)?-$ret2[0]['bing_num']:0;
        return $ret;
    }

    //获取HR上传的用户最新年病假，一次性修正脚本使用
    public function getHrNianOrBingByUser($user_id,$type){
        if(empty($user_id) || empty($type)){
            return false;
        }

        $sql = sprintf("select day_num as number from vacation_record where absence_type=%d and status=%d and user_id=%d and type=2 order by id desc limit 1 ",$type,STATUS_VALID,$user_id);
        $ret = $this->db()->query($sql);
        $sql = sprintf("select day_num as number from vacation_record where absence_type=%d and status=%d and user_id=%d and type=1 and date='2016-12-01' and remark='12月新增' ",$type,STATUS_VALID,$user_id);
        $ret2 = $this->db()->query($sql);
        //校正数据去掉12月新增，防止重复累加
        if($ret){
            return $ret[0]['number']-$ret2[0]['number'];
        }

        return false;
    }

    //获取系统计算的2016年11月26日之前用户年病假，一次性修正脚本使用
    public function getSysNianOrBingByUser($user_id,$type){
        if(empty($user_id) || empty($type)){
            return false;
        }
        $sql = sprintf("select sum(day_num) as number from vacation_record where type=1 and absence_type=%d and status=%d and user_id=%d and (unix_timestamp(date)<1480089600 || remark ='xiuzheng') ",$type,STATUS_VALID,$user_id);
        $ret = $this->db()->query($sql);
        if($ret){
            return $ret[0]['number'];
        }

        return false;
    }

    //获取12月是否上传年病假，一次性修正脚本使用
    public function getDecNianOrBingByUser($user_id,$type){
        if(empty($user_id) || empty($type)){
            return false;
        }
        $sql = sprintf("select day_num as number from vacation_record where absence_type=%d and status=%d and user_id=%d and type=1 and date='2016-12-01' and remark='12月新增' ",$type,STATUS_VALID,$user_id);
        $ret = $this->db()->query($sql);
        if($ret){
            return $ret[0]['number'];
        }

        return false;
    }

}
?>
