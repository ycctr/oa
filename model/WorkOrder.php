<?php
class WorkOrder extends OaBaseModel
{
    public $table = "workorder";

    public function updateIssueValue( $obj_id )
    {
		if(empty($obj_id)){
			return false;
		}
        $strSql = "update workorder set issue = 1 where id={$obj_id}";
        $ret = $this->db()->query($strSql);
        if(empty($ret))
            return false;
        return true;
    }
}
