<?php
class User extends OaBaseModel
{
    public  $table = "user";

    public function getItemByEmail($email,$status = STATUS_VALID)
    {
        if(empty($email)){
            return false; 
        }

        if(strstr($email,"@"))
        {
            $pos = stripos($email,"@");
            $email = substr($email,0,$pos);
        }

        $sql = sprintf("select * from user where email='%s' and status=%d",$email,$status);
        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return $ret[0];
        }
        return false;
    }
    //通过输入的用户名（或者邮箱名）获取用户相关信息
    public function getItemByEmailOrName($name,$status = STATUS_VALID)
    {
        if(strstr($name,"@"))
        {
            $pos = stripos($name,"@");
            $email = substr($name,0,$pos);
        }
        $strSql = "select id,level,email from user where (name like '%".$name."%' or email='".$name."' or id_card='".$name."') and status=".$status;
        $ret = $this->db()->query($strSql);
        return $ret;
    }
    //通过邮件或者工号查询记录，用来判断是否有重复
    public function getItemByEmailOrIdCard($email,$id_card,$status = STATUS_VALID)
    {
        if(strstr($email,"@"))
        {
            $pos = stripos($email,"@");
            $email = substr($email,0,$pos);
        }
        $strSql = "select id from user where (email='".$email."' or id_card='".$id_card."') and status=".$status;
        $ret = $this->db()->query($strSql);

        return $ret;
    }
    public function getItemByIdCard($id_card, $is_leave = "")
    {
        if(empty($id_card)){
            return false;
        }

        if(!empty($is_leave) ){

            $whereSql = " and is_leave = {$is_leave}  ";

        }

        if( (int)$id_card > 0 ){

            $sql = sprintf("select * from user where id_card=%d and status=1 {$whereSql}", $id_card);

        }else{

            $sql = sprintf("select * from user where id_card='%s' and status=1 {$whereSql}", $id_card);
        
        }

        $ret = $this->db()->query($sql);

        if(!empty($ret)){

            return $ret[0];

        }

        return false;
    }
    public function getUserIdsByRoleName($Condition)
    {
        $ids = array();
        if (empty($Condition))
            return $ids;

        $RoleModel = new Role();
        $strSql = sprintf("select id from role where name='%s'",$Condition);
        $ret = $RoleModel->db()->query($strSql);
        if(empty($ret))
            return $ids;
        $role_id =  $ret[0]['id'];

        $UserRoleModel = new UserRole();
        $ids = $UserRoleModel-> getUserIdsByRoleId($role_id);

       // Yii::log(var_dump($ids));
        return $ids;
    }
    public function getCountWithCondition($Condition="",$status = STATUS_VALID, $selectNum="")
    {
        if($Condition == ""){
            $strSql = "select count(*) num from user as t1,section as t2 where t1.section_id=t2.id and t1.status=".$status;
        }else{
            $ids = self::getUserIdsByRoleName($Condition);
            $strSearch = "t1.name like '%$Condition%' or t1.id_card='$Condition' or t1.email='".$Condition."' or t2.name='$Condition' or t1.mobile='$Condition'  ";
            if(count($ids) != 0)
                $strSearch = $strSearch." or t1.id in(".implode(",",$ids).") ";
            $strSql = "select count(*) num from user as t1,section as t2 where t1.section_id=t2.id and t1.status=".$status." and (".$strSearch.")";
        }

        $whereSql = $this->_getLeaveUser($selectNum, $strSql); 
        $strSql .= $whereSql;

        Yii::log($strSql);
        $ret = $this->db()->query($strSql);
        if(empty($ret))
            return 0;
        return $ret[0]['num'];

    }
    //通过页数，和每页展示的个数，获取对应的记录。用于用户管理中的页面展示
    public function getItemByPageAndPerPageNum($currentPage = 1,$perPageNum = 8,$Condition = "",$status = STATUS_VALID, $selectNum="")
    {   

        $whereSql = $this->_getLeaveUser($selectNum); 

        if($Condition == ""){
            $strSql = "select t1.*,t2.name as section_name from user as t1,section as t2 where t1.section_id=t2.id and t1.status=".$status."  {$whereSql} order by t1.id desc limit ".(($currentPage-1)*$perPageNum).",".$perPageNum;
        }else{
            $ids = self::getUserIdsByRoleName($Condition);
            $strSearch = "t1.name like '%$Condition%' or t1.id_card='$Condition' or t1.email='".$Condition."' or t2.name='$Condition' or t1.mobile='$Condition' {$whereSql}";
            if(count($ids) != 0)
                $strSearch = $strSearch." or t1.id in(".implode(",",$ids).") ";
            $strSql = "select t1.*,t2.name as section_name from user as t1,section as t2 where t1.section_id=t2.id and t1.status=".$status." and (".$strSearch.") order by t1.id desc  limit ".(($currentPage-1)*$perPageNum).",".$perPageNum;
        }


        $ret = $this->db()->query($strSql);
        return $ret;
    }
    //通过id获取用户信息，包括用户所在部门的名称
    public function getUserInfoById($id,$status = STATUS_VALID)
    {
        $strSql = "select t1.*,t2.name as section_name, t2.team_name from user t1,section t2 where t1.id=".$id." and t1.section_id=t2.id and t1.status=".$status;
        $ret = $this->db()->query($strSql);
        return $ret;
    }
    //删除用户：将用户状态设置为无效
    public function DeleteUserById($id)
    {
        $arrData['id'] = $id;
        $arrData['status'] = STATUS_INVALID;
        $this->save($arrData);
    }
    public function getItemByName($name = null,$status = STATUS_VALID)
    {
        if(empty($name)){
            return false; 
        }
        $sql = sprintf("select * from user where name='%s' and status=%d",$name,$status);
        Yii::log($sql);
        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return $ret[0];
        }
        return false;

    }

    public function getIdsByName($name = null,$status = STATUS_VALID)
    {
        $sql = sprintf("select id from user where name='%s' and status=%d",$name,$status);
        Yii::log($sql);
        $ret = (array)$this->db()->query($sql);
        return $ret;
    }

    public function getCountWithAdminInput($name = null,$email = null)
    {
        $strSql = sprintf("select count(*) num from user where 1=1 ");
        if(!is_null($name) && !empty($name))
            $strSql = sprintf("%s and name='%s' ",$strSql,$name);
        if(!is_null($email) && !empty($email))
            $strSql = sprintf("%s and email='%s' ",$strSql,$email);
        $ret = $this->db()->query($strSql);
        Yii::log($strSql);
        if(empty($ret))
            return 0;
        return $ret[0]['num'];
    }

    //根据name、id_card获取用户
    public function getUserIdsByCardOrName($keyword)
    {
        if(empty($keyword)){
            return false;
        } 

        $sql = sprintf("select * from user where name='%s' or id_card='%s' ",$keyword,$keyword);
        $result = $this->db()->query($sql);

        $ids = array();
        if(!empty($result)){
            foreach($result as $item)
                $ids[] = $item['id'];
        }
        return $ids;
    }
    //获取部门所有员工名字
    public function getUserNamesBySectionId($sectionId)
    {   
        if(empty($sectionId)){
            return false;
        }   

        $sql = sprintf("select name from user where section_id=%d and status=%d ",$sectionId,STATUS_VALID);
        $rows = $this->db()->query($sql);
        $ret = array();
        if(!empty($rows)){
            foreach($rows as $row)
                $ret[] = $row['name'];
        }
        return $ret;
    }

    //获取所有有效员工
    public function getAllValidUsers()
    {
        $ret = array();
        $sql = "select id,name,id_card from user where status = 1"; 

        $rows = $this->db()->query($sql);
        if(!empty($rows)){
            foreach($rows as $row){
                $ret[$row['id']] = $row;
            } 
        }
        return $ret;
    }

    //只提供给导出功能所有员工的查询
    public function getAllUserInfo()
    {
        $ret = array();
        //注意这里的查询顺序不能随便更改，
        $sql = "SELECT  u.id,u.`id_card`,u.`name` uname, u.`sex`, u.`email`,u.`mobile`, s.`name` sname, s.team_name, u.`is_leave`,u.`status`,u.`work_date`, u.`hiredate`,u.`leave_date`,u.`pay_date` FROM `user` u, section s WHERE u.`section_id`= s.`id` ORDER BY id "; 

        $rows = $this->db()->query($sql);
        if(!empty($rows)){
            foreach($rows as $row){
                $id = $row['id'];
                $sex = $row['sex'];
                $row['sex'] = ($sex == 1)?'男':'女';

                $status = $row['status'];
                $row['status'] = ($status == 1)?'正常':'已删除';

                $is_leave = $row['is_leave'];
                $row['is_leave'] = ($is_leave == 1)?'在职':'离职';

                $row['email'] .= "@rong360.com";
                unset($row['id']);
                $ret[$id] = $row;
            } 
        }
        return $ret;
    }

    

    private function _getLeaveUser($selectNum) {

        if($selectNum != "" && $selectNum != 1) {

            //只显示普通员工
            if($selectNum == 2) { 

                $whereSql = " AND is_leave = 1";

            }else if($selectNum == 3) {  //只显示离职员工

                $whereSql = " AND is_leave = 2";

            }

        }

        return $whereSql;

    }

    public function getBySqlUser($whereSql, $limitStr) {

        $is_leave = USER_UN_LEAVE;
        $sql = "SELECT u.id, u.name, u.section_id, u.sex, u.email, u.mobile, u.id_card, u.level FROM user u WHERE u.status=1 AND u.is_leave = {$is_leave}  {$whereSql} ORDER BY u.section_id {$limitStr} ";

        $res = $this->db()->query($sql);

        if(empty($res) ) {

            return false;

        }

        return $res;

    }

    public function getBySqlUserTotal($whereSql) {

        $is_leave = USER_UN_LEAVE;
        $sql = "SELECT count(*) total FROM user u WHERE u.status=1 AND u.is_leave = {$is_leave}  {$whereSql} ";

        $res = $this->db()->query($sql);

        if(empty($res) ) {

            return false;

        }

        return $res[0]['total'];

    }
    //获得所有manager
    public function getManagers(){
        $strSql = "select * from user where `level` > 0 and `status`=" . STATUS_VALID . " and is_leave=" . USER_UN_LEAVE . " order by name ";
        $arrRes = $this->db()->query($strSql);
        return $arrRes;
    }
    //获得ceo
    public function getCeo(){
        $strSql = "select * from user where `level`=" . AUDIT_CEO . " and `status`=" . STATUS_VALID . " and is_leave=" . USER_UN_LEAVE;
        $arrRes = $this->db()->query($strSql);
        return $arrRes;
    }

    //获得面试官
    public function getInterviewersByWorkflowId($id){
        $ret = array();
        $userModel = new User();
        $strSql = "select user_id from audit_list where user_workflow_id={$id} and business_role='interviewer'";
        $arrUserIds = $this->db()->query($strSql);
        foreach ($arrUserIds as $key => $value){
            $user = $userModel->getItemByPk($value['user_id']);
            $ret[] = $user['name'];
        }
        return $ret;
    }

    
    //获取某部门下所有在职员工
    public function getSectionUser($section_id){
        if(!empty($section_id)){
            $sql = sprintf("select * from user where section_id=%d and status=%d and is_leave=1",$section_id,STATUS_VALID);
        }else{
            $sql = sprintf("select * from user where status=%d and is_leave=1",STATUS_VALID);
        }
        $ret = $this->db()->query($sql);
        return $ret;
    }

}
