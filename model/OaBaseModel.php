<?php
class OaBaseModel extends DBModel
{
    public function __construct($mode='')
    {   
        //读写方式
        if(empty($mode)){
            if(defined('SQL_DEFAULT_MODE')){
                $mode = SQL_DEFAULT_MODE;
            }else{
                $mode = DBModel::MODE_RW;
            }   
        }   

        parent::__construct(Config::$oaMysqlConfig, Config::$oaMysqlConfigRo, $mode);

    } 
    
    public function setParams($arrParams)
    {
         $this->arrParams = $arrParams;
    }

    //需要记录操作日志的表配置，如果用这个记录操作记录 必须走save方法
    private $_opTables = array(
        'section',
        'user',
        'user_role' 
    );

    public function save($arrData,$insertWithId = 0, $tname='')
    {
        if(empty($tname)){
            $tname = $this->table;
        }
        $oplogRow = array();
        //更新操作,记录操作日志
        if(in_array($tname,$this->_opTables)){
            $oplogRow['user_id'] = Yii::app()->session['oa_user_id'];
            $oplogRow['table_name'] = $tname;
            if($arrData['id']){
                $oldRow = $this->getItemByPk($arrData['id']);
            }else{
                $oldRow = array();
            }
            $oplogRow['con_before'] = serialize($oldRow);
            $oplogRow['create_time'] = time();
        }
        $result = parent::save($arrData,$insertWithId, $tname);
        if(!empty($oplogRow) && $result){
            $oplogRow['table_id'] = $result;
            $oplogRow['con_after'] = serialize($arrData);
            $opLogModel = new OpLog(); 
            $opLogModel->save($oplogRow);
        }
        return $result;
    }

}










