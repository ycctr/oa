<?php
class CancelAbsence extends OaBaseModel
{
    public $table = 'cancel_absence';

    public function getItemByUserIdAndDate($userId,$date)
    {
        if(empty($userId) || empty($date)){
            return false;
        } 
        $time = strtotime($date);
        
        $sql = sprintf("select a.* from user_workflow uwf inner join cancel_absence a on uwf.obj_id=a.id and uwf.obj_type='cancel_absence' where uwf.user_id=%d and uwf.audit_status=%d and uwf.status=%d ",$userId,OA_WORKFLOW_AUDIT_PASS,STATUS_VALID);
        $sql .= " and unix_timestamp(from_unixtime(a.start_time,'%Y%m%d')) <=".$time." and unix_timestamp(from_unixtime(end_time,'%Y%m%d')) >=".$time." and a.status=".STATUS_VALID;

        $ret = $this->db()->query($sql);
        if(!empty($ret)){
            return $ret[0];
        }
        return false;
    }

    //判断用户在一个时间段内是否已经有销假记录
    public function isHaveRecord($userId,$start_time,$end_time)
    {
        if(empty($userId) || empty($start_time) || empty($end_time)){
            throw new CHttpException(404,'参数错误，联系技术人员');
        } 
        
        $sql = sprintf("select count(*) as cnt from user_workflow uwf inner join cancel_absence a on uwf.obj_id=a.id and uwf.obj_type='cancel_absence' where uwf.user_id=%d and uwf.audit_status!=%d and uwf.audit_status!=%d and uwf.status=%d ",$userId,OA_WORKFLOW_AUDIT_REJECT,OA_WORKFLOW_AUDIT_CANCEL,STATUS_VALID);
        $sql .= sprintf(" and ((a.start_time <= %d and a.end_time >= %d) or (a.start_time <= %d and a.end_time >= %d) or (a.start_time >= %d and a.end_time <= %d)) ",$start_time,$start_time,$end_time,$end_time,$start_time,$end_time);

        $ret = $this->db()->query($sql);
        if(!empty($ret) && $ret[0]['cnt'] > 0){
            return true;
        } 
        return false;
    }

    //获取某时间段审批通过的请假记录
    public function getItemsByDay($startDay,$endDay)
    {
        $startTime = strtotime($startDay);
        $endTime = strtotime($endDay);
        $zero = strtotime('2015-07-26');
        $sql = sprintf("select a.* from user_workflow uwf inner join cancel_absence a on uwf.obj_id=a.id and uwf.obj_type='cancel_absence' where uwf.audit_status=%d and uwf.status=%d ",OA_WORKFLOW_AUDIT_PASS,STATUS_VALID);
        $sql .= " and uwf.modify_time >=". $startTime . " and uwf.modify_time < ". $endTime ." and a.status=".STATUS_VALID;
        $sql .= " and a.end_time >= $zero";
        $ret = $this->db()->query($sql);
        return $ret; 
    }

    //条件查询
    public function getExcelPrint($conditionInfo) {
        $start = $conditionInfo['startTime'];
        $end   = $conditionInfo['endTime'];
        $whereSql = "";

        //时间条件
        if(!empty($start) && !empty($end) ) {

            if($start > $end ) {
                
                $startTime = $end;
                $endTime   = $start;

            }else if($start < $end) {

                $startTime = $start;
                $endTime   = $end;


            }else {

                $startTime = $start;
                $endTime   = $end + 86400;


            }

            $whereSql = " AND c.start_time >= {$startTime} AND c.end_time <= {$endTime}";
        
        }

        //姓名，部门条件
        if($conditionInfo['uName'] != "" ) {
            $uName = $conditionInfo['uName'];
            $whereSql .= " and u.name like '%{$uName}%' ";
        }
        if($conditionInfo['sName']) {
            $sNanme = $conditionInfo['sName'];
            $whereSql .= " AND s.name like '%{$sNanme}%' ";
        }
        if(!empty($conditionInfo['id_card'])){
            $whereSql .= " and u.id_card={$conditionInfo['id_card']} ";
        }

        
        $sql = "SELECT u.`id_card`, u.name AS uname,  c.`title`, c.`start_time`, c.`end_time`, s.`name` AS sname, s.`team_name` AS teamnam,c.`leave_reason`,c.`day_number`,uwf.audit_status FROM `cancel_absence` c, `section` s, `user` u,user_workflow uwf WHERE  c.`user_id` = u.`id` and uwf.obj_id=c.id and uwf.obj_type='cancel_absence' {$whereSql} AND u.`section_id` = s.`id` AND c.status = 1 AND uwf.audit_status in (1,2,3,6) ORDER BY u.id ";

        $ret = $this->db()->query($sql);

        $data = array();
        foreach($ret as $row) {
            $data[] = $row;
        }

        $sql2 = "SELECT u.`id_card`, u.name AS uname,  c.`title`, c.`start_time`, c.`end_time`, s.`name` AS sname, s.`team_name` AS teamnam,c.`cancel_reason`,c.`day_number`,uwf.audit_status FROM `absence` c, `section` s, `user` u,user_workflow uwf WHERE  c.`user_id` = u.`id` and uwf.obj_id=c.id and uwf.obj_type='cancel_absence' {$whereSql} AND u.`section_id` = s.`id` AND c.status = 1 AND c.is_cancel=1 AND uwf.audit_status in (1,2,3,6) ORDER BY u.id ";
        // echo $sql2;
        // exit();
        $ret2 = $this->db()->query($sql2);
        foreach ($ret2 as $row) {
            $data[] = $row;
        }
        return $data; 
    }


}
?>
