<?php
/**
 * Created by PhpStorm.
 * User: songyuzhuo
 * Date: 2016/5/25
 * Time: 13:50
 * 入职, 离职, 转正, 异动等hr需求
 */

class HrController extends Controller{


    //展示入职列表
    public function actionListEntry(){
        $status = Yii::app()->request->getParam('status');
        $pn = Yii::app()->request->getParam('pn',1);
        $rn = Yii::app()->request->getParam('rn',DEFAULT_PAGE_NUMBER);
        $entryModel = new Entry();
        $start = ($pn-1)*$rn;
        $model = new UserWorkFlow();
        $entries = $entryModel->getEntriesByStatus($start, $rn, $status);
        $cnt = $entryModel->getEntryCntByStatus($status);
        $arrData['pn'] = $pn;
        $arrData['rn'] = $rn;
        $arrData['entries'] = $entries;
        $arrData['status'] = $status;
        $arrData['cnt'] = $cnt;
        $arrData['url'] = "/hr/listEntry?status={$status}&";

        if(Yii::app()->request->isAjaxRequest){
            $this->renderFile("./widget/entry_content.tpl",$arrData);
        }else{
            $this->renderFile("./entry_list.tpl",$arrData);
        }
    }
    public function actionDepartmentAssessmentDetail(){
        $arrTpl = array();
        $departmentAssessmentId = Yii::app()->request->getParam('id');
        $departmentAssessmentModel = new DepartmentAssessment();
        $objDepartmentAssessment = $departmentAssessmentModel->getItemByPk($departmentAssessmentId);
        $arrTpl['departmentAssessment'] = $objDepartmentAssessment;
        $entryId = $objDepartmentAssessment['entry_id'];
        $entryModel = new Entry();
        $objEntry = $entryModel->getItemByPk($entryId);
        $arrTpl['entryName'] = $objEntry['name'];
        $section_id = $objEntry['subsection_id'] == 1 ? $objEntry['section_id'] : $objEntry['subsection_id'];
        $sectionModel = new Section();
        $section = $sectionModel->getItemByPk($section_id);
        $arrTpl['sectionName'] = $section['name'];
        $arrTpl['position'] = $objEntry['position'];
        $userModel = new User();
        $interviewer = $userModel->getItemByPk($objDepartmentAssessment['user_id']);
        $arrTpl['interviewer'] = $interviewer['name'];
        $arrTpl['interviewTime'] = $objEntry['interview_time'];
        $this->renderFile('entry/department_interview_print.tpl', $arrTpl);
    }
    public function actionHrAssessmentDetail(){
        $arrTpl = array();
        $hrAssessmentId = Yii::app()->request->getParam('id');
        $hrAssessmentModel = new HrAssessment();
        $objHrAssessment = $hrAssessmentModel->getItemByPk($hrAssessmentId);
        $arrTpl['hrAssessment'] = $objHrAssessment;
        $entryId = $objHrAssessment['entry_id'];
        $entryModel = new Entry();
        $objEntry = $entryModel->getItemByPk($entryId);
        $arrTpl['entryName'] = $objEntry['name'];
        $arrTpl['resumeSource'] = $objEntry['resume_source'];
        $section_id = $objEntry['subsection_id'] == 1 ? $objEntry['section_id'] : $objEntry['subsection_id'];
        $sectionModel = new Section();
        $section = $sectionModel->getItemByPk($section_id);
        $arrTpl['sectionName'] = $section['name'];
        $arrTpl['position'] = $objEntry['position'];
        $userModel = new User();
        $interviewer = $userModel->getItemByPk($objHrAssessment['user_id']);
        $arrTpl['interviewer'] = $interviewer['name'];
        $arrTpl['interviewTime'] = $objEntry['interview_time'];
        $this->renderFile('entry/hr_interview_print.tpl', $arrTpl);
    }
    public function actionEntryDetail(){
        $entryId = Yii::app()->request->getParam('id');
        $entryModel = new Entry();
        $objEntry = $entryModel->getItemByPk($entryId);
        $arrTpl = array();
        $arrTpl['entry'] = $objEntry;
        $sectionModel = new Section();
        $businessLine = $sectionModel->getItemByPk($objEntry['business_line_id']);
        $arrTpl['businessLine'] = $businessLine['name'];
        $section = $sectionModel->getItemByPk($objEntry['section_id']);
        $arrTpl['section'] = $section['name'];
        $subsection = $sectionModel->getItemByPk($objEntry['subsection_id']);
        $arrTpl['subsection'] = $subsection['name'];
        $arrTpl['position'] = $objEntry['position'];
        $userModel = new User();
        $manager = $userModel->getItemByPk($objEntry['manager_id']);
        $arrTpl['manager'] = $manager['name'];
        $arrTpl['levelNumToName'] = OAConfig::$levelNumToName;
        $arrTpl['interviewTime'] = $objEntry['interview_time'];
        $userWorkflowModel = new UserWorkFlow();
        $userWorkflow = $userWorkflowModel->getUwfByEntryId($entryId);
        $userWorkflowId = $userWorkflow['id'];
        $userTaskModel = new UserTask();
        $userTasks = $userTaskModel->getItemsByUserWorkflowId($userWorkflowId);
        if(is_array($userTasks)){
            foreach ($userTasks as $record){
                if($record['audit_desc'] == 'directManager'){
                    $arrTpl['directManager'] = $record;
                }elseif ($record['audit_desc'] == 'sectionManager'){
                    $arrTpl['sectionManager'] = $record;
                }elseif ($record['audit_desc'] == 'HRD'){
                    $arrTpl['HRD'] = $record;
                }elseif ($record['audit_desc'] == 'VP'){
                    $arrTpl['VP'] = $record;
                }elseif ($record['audit_desc'] == 'CEO'){
                    $arrTpl['CEO'] = $record;
                }
            }
        }
        if(!isset($arrTpl['sectionManager'])){
            $arrTpl['sectionManager'] = $arrTpl['directManager'];
        }


        $this->renderFile('entry/entry_assessment_print.tpl',$arrTpl);
    }
    //展示离职列表
    public function actionListDimission(){
        $status = Yii::app()->request->getParam('status');
        $pn = Yii::app()->request->getParam('pn',1);
        $rn = Yii::app()->request->getParam('rn',DEFAULT_PAGE_NUMBER);
        $dimissionModel = new Dimission();
        $start = ($pn-1)*$rn;
        $model = new UserWorkFlow();
        $dimissions = $dimissionModel->getDimissionsByStatus($start, $rn, $status);
        $arrData['levelNumToName'] = OAConfig::$levelNumToName;
        $cnt = $dimissionModel->getDimissionCntByStatus($status);
        $arrData['pn'] = $pn;
        $arrData['rn'] = $rn;
        $arrData['dimissions'] = $dimissions;
        $arrData['status'] = $status;
        $arrData['cnt'] = $cnt;
        $arrData['url'] = "/hr/listDimission?status={$status}&";

        if(Yii::app()->request->isAjaxRequest){
            $this->renderFile("./widget/dimission_content.tpl",$arrData);
        }else{
            $this->renderFile("./dimission_list.tpl",$arrData);
        }
    }
    //离职记录详情
    public function actionDimissionDetail(){
        $dimissionId = Yii::app()->request->getParam('id');
        $target = Yii::app()->request->getParam('target');
        $dimissionModel = new Dimission();
        $objDimission = $dimissionModel->getItemByPk($dimissionId);
        $arrTpl['dimission'] = $objDimission;
        $sectionModel = new Section();
        $userModel = new User();
        $arrTpl['user'] = $userModel->getItemByPk($objDimission['user_id']);
        $section = $sectionModel->getItemByPk($arrTpl['user']['section_id']);
        $arrTpl['section'] = $section['name'];
        $manager = $userModel->getItemByPk($section['manager_id']);
        if($manager['name'] == $arrTpl['user']['name']){
            $parentSectionId = $section['parent_id'];
            $parentSection = $sectionModel->getItemByPk($parentSectionId);
            $manager_id = $parentSection['manager_id'];
            $manager = $userModel->getItemByPk($manager_id);
        }
        $arrTpl['manager'] = $manager['name'];
        $userWorkflowModel = new UserWorkFlow();
        $userWorkflow = $userWorkflowModel->getUwfByDimissionId($dimissionId);
        $userTaskModel = new UserTask();
        $userTasks = $userTaskModel->getItemsByUserWorkflowId($userWorkflow['id']);

        if($target == 0){
            $arrApproveA = array();
            if(is_array($userTasks)){
                foreach ($userTasks as $record){
                    if($record['workflow_step'] == 'A'){
                        $arrApproveA[] = $record;
                    }
                    if($record['workflow_step'] == 'B'){
                        $HRD = $userModel->getItemByPk($record['audit_user_id']);
                        $record['ext'] = $HRD;
                        $arrTpl['HRD'] = $record;
                    }

                }
                if(count($arrApproveA) == 1){
                    $arrTpl['directManager'] = $arrTpl['sectionManager'] = $arrTpl['VP'] = $arrApproveA[0];
                }elseif(count($arrApproveA) == 2){
                    $arrTpl['directManager'] = $arrTpl['sectionManager'] = $arrApproveA[0];
                    $arrTpl['VP'] = $arrApproveA[1];
                }elseif(count($arrApproveA) == 3){
                    $arrTpl['directManager'] = $arrApproveA[0];
                    $arrTpl['sectionManager'] = $arrApproveA[1];
                    $arrTpl['VP'] = $arrApproveA[2];
                }
            }
            $this->renderFile('dimission/lizhi_bak.tpl', $arrTpl);
        }else{
            if(is_array($userTasks)){
                foreach($userTasks as $record){
                    if($record['workflow_step'] == 'C'){
                        $arrTpl['manager'] = $record;
                    }elseif($record['workflow_step'] == 'D'){
                        $arrTpl['systemAccount'] = $record;
                    }elseif($record['workflow_step'] == 'E'){
                        $arrTpl['riskAccount'] = $record;
                    }elseif($record['workflow_step'] == 'F'){
                        $arrTpl['mailAccount'] = $record;
                    }elseif($record['workflow_step'] == 'G'){
                        $arrTpl['finance'] = $record;
                    }elseif($record['workflow_step'] == 'H'){
                        $arrTpl['administration'] = $record;
                    }elseif($record['workflow_step'] == 'I'){
                        $arrTpl['hr'] = $record;
                    }elseif($record['workflow_step'] == 'J'){
                        $HRD = $userModel->getItemByPk($record['audit_user_id']);
                        $record['ext'] = $HRD;
                        $arrTpl['HRD'] = $record;
                    }elseif($record['workflow_step'] == 'K'){
                        $arrTpl['self'] = $record;
                    }
                }
            }
            $this->renderFile('dimission/lizhi_jiaojie_bak.tpl', $arrTpl);
        }
    }
}