<?php
class EntrustauditController extends Controller
{
    public function actionList()
    {
        $model = new EntrustAudit();
        $arrData['list'] = $model->getList();
        $arrData['workflow_type'] = OAConfig::$workflow_type;
        
        $this->renderFile('./entrustaudit/list.tpl',$arrData);  
    }

    public function actionAdd()
    {
        if(Yii::app()->request->getIsPostRequest()){
            $userModel = new User();
            $entrust_email = Yii::app()->request->getParam('entrust_email');
            $trustee_email = Yii::app()->request->getParam('trustee_email');

            $entrustUser = $userModel->getItemByEmail($entrust_email);
            $trusteeUser = $userModel->getItemByEmail($trustee_email);

            $arrData = array();
            $arrData['section_id'] = Yii::app()->request->getParam('section_id');
            $arrData['obj_type'] = Yii::app()->request->getParam('obj_type');
            if(empty($arrData['section_id']) || empty($arrData['obj_type']) || empty($entrustUser) || empty($trusteeUser)){
                throw new CHttpException(404,'数据不全，请重新填写');
            }
            $arrData['entrust_user_id'] = $entrustUser['id'];
            $arrData['trustee_user_id'] = $trusteeUser['id'];

            $model = new EntrustAudit();
            if(!$model->save($arrData)){
                throw new CHttpException(404,'保存失败，请重新提交');
            }
            $this->redirect('/entrustaudit/list');
        }

        $sectionModel = new Section();

        $sections = $sectionModel->getSectionIdNameList();
        $arrData['sections'] = $sections;
        $arrData['workflow_type'] = OAConfig::$workflow_type;
        $this->renderFile('./entrustaudit/add.tpl',$arrData); 
    }

    public function actionDel()
    {
        $id = Yii::app()->request->getParam('id');

        $model = new EntrustAudit();  

        $ret = array('code'=>0,'msg'=>'');
        if(!$model->del($id)){
            $ret['code'] = 1; 
            $ret['msg'] = '删除失败';
        }

        echo json_encode($ret);
        Yii::app()->end();
    }
}

?>
