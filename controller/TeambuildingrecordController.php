<?php
class TeambuildingrecordController extends Controller
{
    static $pagenumber = 50;

    public function actionList()
    {
        $pn = Yii::app()->request->getParam('pn',1);
        $sectionId = Yii::app()->request->getParam('section_id');
        $userId = Yii::app()->session['oa_user_id'];

        $sectionModel = new Section();
        $is_role   = $this->isTeamBuildingAuditRole($userId);

        //默认关闭
        $isPermission = false;

        $teamService = new TeamBuildingService();
        $model = new Section();

        if(!$is_role) {


            $buildingSection = $teamService->getSectionBuilding($userId);

            if(!empty($buildingSection) && $buildingSection[$sectionId]['can_building'] == 1) {

                $isPermission = true;

            }

            $teamArr = $teamService -> getTemaBuilding($userId); 


        } else {

            $isPermission = true;

            $teamArr = $model -> getAllSectionHasBuilding($userId); 

        }


        $sections = $model->getItemsByManagerId($userId);
        
        if(empty($sectionId)){//没有传递部门id,默认选取第一个

            $section = reset($sections);
            $sectionId = $section['id']; 
            $isPermission = true;

        }else{

            $section = $model->getItemByPk($sectionId); 

        }

        if(!$isPermission){
            throw new CHttpException(404,'您没有权限查看权限');
        }


        $start = ($pn-1)*self::$pagenumber;
        $buildRecordModel = new TeamBuildingRecord();
        $list = $buildRecordModel->getItemsBySectionId($sectionId,$start,self::$pagenumber);
        $total = $buildRecordModel->getItemTotalBySectionId($sectionId);

        $arrData['pn'] = $pn;
        $arrData['rn'] = self::$pagenumber;
        $arrData['url'] = '/teambuildingrecord/list/?section_id='.$sectionId;
        $arrData['total'] = $total;
        $arrData['list'] = $list;

        $arrData['sections'] = $teamArr;
        $arrData['section'] = $section;
        $this->renderFile('./buildingrecord/list.tpl',$arrData);
    } 

    public function actionFinishing()
    {
        if(Yii::app()->request->getIsPostRequest()){
            $amount = Yii::app()->request->getParam('amount');
            $remark = Yii::app()->request->getParam('remark'); 
            $team_type = Yii::app()->request->getParam("team_type", 1);

            if($team_type == 1) {

                $model = new TeamBuildingRecord();
                $sectionId = Yii::app()->request->getParam('section_building_id');

            } else {

                $model = new TeamEncourageRecord();
                $sectionId = Yii::app()->request->getParam('section_encourage_id');

            }

            if(empty($sectionId) || empty($amount)){
                throw new CHttpException(404,'参数不全，请重新填写');
            }

            $row = array();
            $row['section_id'] = $sectionId;
            $row['amount'] = $amount;
            $row['date'] = date('Y-m-d');
            $row['create_time'] = time();
            $row['operator_id'] = Yii::app()->session['oa_user_id'];
            $row['remark'] = $remark;
            $row['status'] = STATUS_VALID;

            
            $id = $model->save($row);

            if(empty($id)){
                throw new CHttpException(404,'保存失败，请重新提交');
            }

        }

        $sectionModel = new Section();
        $sections01 = $sectionModel->getAllSectionHasBuilding();
        $sections02 = $sectionModel->getAllSectionHasEncourage();
        $arrData['sections01'] = $sections01;
        $arrData['sections02'] = $sections02;
        $this->renderFile('./buildingrecord/finishing.tpl',$arrData);
    }

    public function actionOverview()
    {
        $team_type = Yii::app()->request->getParam("team_type", 1);
        $teamBuildingType  = 1;
        $teamEncourageType = 2;
        $control = Yii::app()->params['control'];
        $sectionName = Yii::app()->request->getParam('section_name');
        $userName    = Yii::app()->request->getParam('user_name');

        if($team_type == $teamBuildingType ) {
            $model = new TeamBuildingRecord(); 
            $list = $model->getTeamBudingBySection(array(),$sectionName,$userName);
            $arrData['list'] = $list;
            $sectionModel = new Section();
            $sectionUserNum = $sectionModel->getAllBuildSectionUserNumber();

        } else if( $team_type == $teamEncourageType ) {

            $model = new TeamEncourageRecord(); 
            $list = $model->getTeamEncourageBySection('',$sectionName,$userName);
            $arrData['list'] = $list;
            $sectionModel = new Section();
            $sectionUserNum = $sectionModel->getAllEncourageSectionUserNumber();
            $control = "teamencouragerecord";

        } else {
            throw new CHttpException(404,'页面请求错误, 请重新尝试!'); 
        }
        
        $arrData['section_name'] = $sectionName;
        $arrData['user_name'] = $userName;
        $arrData['team_type'] = $team_type;
        $arrData['control'] = $control;
        $arrData['sectionUserNum'] = $sectionUserNum;
        $this->renderFile('./buildingrecord/overview.tpl',$arrData);
    }

    public function actionAllAmount() {

        $teamService = new TeamBuildingService(); 
        $userId = Yii::app()->session['oa_user_id'];
        $teamArr = $teamService -> getTemaBuilding($userId); 
        $sectionIdArr = array_keys($teamArr);

        $model = new TeamBuildingRecord(); 
        $list = $model->getTeamBudingBySection($sectionIdArr);

        $arrData['list'] = $list;
        $sectionModel = new Section();
        $sectionUserNum = $sectionModel->getAllBuildSectionUserNumber();
        $arrData['sectionUserNum'] = $sectionUserNum;

        $arrData['export_flag'] = 1;
        $arrData['control'] = Yii::app()->params['control'];

        $this->renderFile('./buildingrecord/allamount.tpl',$arrData);

    }


    //获取部门团建金额
    public function actionGetAmount()
    {
        $userId = Yii::app()->session['oa_user_id'];
        $sectionId = Yii::app()->request->getParam('section_id');

        if(!$this->hasPermission($userId,$sectionId)){
            throw new CHttpException(404,'您没有权限查询团建经费，请找部门经理查询');
        }
        $model = new TeamBuildingRecord();

        $amount = $model->getTotalAmountBySectionId($sectionId);

        echo json_encode($amount);
        die;
    }

    //是否人力团建审批人员
    //依赖角色名称，容易造成Bug 目前没有想到特别好的办法，如果用角色赋予权限，维护代价比较大，而且容易忘记更新
    public function isTeamBuildingAuditRole($userId)
    {
        $model = new UserRole(); 
        $roleNames = $model->getRoleNameByUserId($userId);
        if(in_array('团建审核专员',$roleNames)){
            return true;
        }
        return false;
    }

    //权限验证
    public function hasPermission($userId,$sectionId)
    {
        $model = new Section();
        $sections = $model->getItemsByManagerId($userId,$sectionId);
        if(!count($sections) && !$this->isTeamBuildingAuditRole($userId)){
            return false;
        }
        return true;
    }

    //权限验证
    public function actionSenior() {

        $teamService = new TeamBuildingService(); 
        $sectionModel = new Section();

        if(Yii::app()->request->getIsPostRequest()){

            $to_section = Yii::app()->request->getParam('to_section');

            if($to_section < 1){

                throw new CHttpException(404,'报错数据失败,请重新提交!');

            }

            $quota = Yii::app()->request->getParam('quota');
            $quotaList = array();
            foreach($quota as $from_section=>$val) {

                if(!empty($val) ) {

                   $addData["from_section"] = $from_section; 
                   $addData["quota"]        = $val; 
                   $addData["to_section"]   = $to_section; 
                   $is_add = $teamService -> addTeamBuildingSenior($addData);

                   // if(!$is_add) {
                   //
                   //   throw new CHttpException(404,'报错数据失败,请重新提交!');
                   //
                   // }

                }

            }
            
            $sectionId = $to_section;

        } else {

            $sectionId = Yii::app()->request->getParam('section_id');

        }


        //展示所有高级权限部门tab
        $condition = array( "senior_building" => "1" );
        $userId = intval(Yii::app()->session['oa_user_id']);
        $sectionsArr = $sectionModel->getConditionSection($userId, $condition);

        $sectionTab = array();
        $sectionSenior = array();
        $condition['can_building'] = 1;

        $sectionSenior = array();
        foreach($sectionsArr as $section) {

           $id = $section['id']; 
           $name = $section['name'];

           //tab page
           $sectionTab[$id] = $name;  
           $sectionSenior[$id] = $teamService -> getBySectionData($id); 

        }

        if(empty($sectionId)) {

            $sectionId = key($sectionTab);

        }

        $seniorData = $sectionSenior[$sectionId];

        $teamService->formTeamBuding($seniorData);

    

        $arrData['sectionId'] = $sectionId;
        $arrData['section'] = $sectionTab[$sectionId];
        $arrData['sectionTab'] = $sectionTab;
        $arrData['sectionSenior'] = $seniorData;

        $this->renderFile('./buildingrecord/senior.tpl',$arrData);

    }

}
?>
