<?php
class VacationrecordController extends Controller
{
    static $pageNum = 50;
    public function actionList()
    {
        $userId = Yii::app()->session['oa_user_id']; 
        $pn = Yii::app()->request->getParam('pn',1);
        $selectNum = Yii::app()->request->getParam('selectNum',1);
        $start = ($pn - 1)*self::$pageNum;
        $arrData['pn'] = $pn;
        $arrData['rn'] = self::$pageNum;
        $arrData['url'] = '/vacationrecord/list/?';

        $userModel = new User();
        $sectionModel = new Section();
        $arrData['user'] = $user = $userModel->getItemByPk($userId);
        $arrData['section'] = $sectionModel->getItemByPk($user['section_id']);

        $id_card = strtolower($user['id_card']);
        if(strpos($id_card,'sx') !== false){
            throw new CHttpException(404,"您没有权限查看");
        }

        $model = new VacationRecord();

        $result = $model->getListByUserId($userId,$start,self::$pageNum, $selectNum);
        $arrData['list'] = $result;
        $total = $model->getSurplusJiaqi($userId);
        if(empty($total)){
            $total['date'] = date("Y-m-d",time());
        }else{
            $total['date'] = date('Y-m-25',strtotime('-1 month',strtotime($total['date'])));//约定
        }
        $arrData['total'] = $total;
        $arrData['absence_type_total'] = $model->getListByUserIdTotal($userId, $selectNum);

        $week = array('日','一','二','三','四','五','六');

        $arrData['option_list'] = array(
            '1' => "全部流水",
            '2' => "年假流水",
            '3' => "病假流水",
        );
        $arrData['absenceType'] = OAConfig::$absenceType;

        $arrData['week'] = $week;
        $arrData['selectNum'] = $selectNum;
        $arrData['title']  = "年假";


        if($selectNum == 1) {

            $arrData['title']  = "全部假期";

        } else if($selectNum == 3) {

            $arrData['title']  = "病假";

        }

        if(Yii::app()->request->isAjaxRequest) {

            $this->renderFile("./vacationrecord/widget/list.tpl",$arrData);

        } else {

            $this->renderFile('./vacationrecord/list.tpl',$arrData);

        }
    } 

    public function actionsuperList() {

        $pn = Yii::app()->request->getParam('pn',1);
        $select_num = Yii::app()->request->getParam('select_num',2);
        $search = Yii::app()->request->getParam('search');
        $start_date = Yii::app()->request->getParam('start_date');
        $end_date = Yii::app()->request->getParam('end_date');

        $arrData = array();
        $start = ($pn - 1)*self::$pageNum;
        $arrData['pn'] = $pn;
        $arrData['rn'] = self::$pageNum;

        $model = new VacationRecord();
        $condition['select_num'] = $select_num;

        if(empty($start_date) || empty($end_date) ) {

            $end_date = date("Y-m-d");
            $start_time   = strtotime("-30day");
            $start_date = date("Y-m-d", $start_time);

        }

        $arrData['start_date'] = $start_date;
        $arrData['end_date']   = $end_date;
        $arrData['search']   = $search;

        $condition['start_date'] = $start_date; 
        $condition['end_date']   = $end_date; 
        $condition['search'] = $search; 

        $result = $model->getListByAll($condition, $start, self::$pageNum);
        $arrData['list'] = $result['ret'];
        $arrData['total'] = $result['total'];;

        $week = array('日','一','二','三','四','五','六');

        $arrData['option_list'] = array(
            '1' => "全部流水",
            '2' => "年假流水",
            '3' => "病假流水",
        );

        $arrData['week'] = $week;
        $arrData['select_num'] = $select_num;
        $arrData['title']  = "年假";


        if($select_num == 1) {

            $arrData['title']  = "年假、病假";

        } else if($select_num == 3) {

            $arrData['title']  = "病假";

        }

        $query = "?select_num={$select_num}&start_date={$start_date}&end_date={$end_date}&search={$search}";
        $arrData['query'] = $query; //导出规则
        $arrData['url'] = "/vacationrecord/superlist{$query}"; //分页规则


        if(Yii::app()->request->isAjaxRequest) {

            $this->renderFile("./vacationrecord/widget/list_super.tpl",$arrData);

        } else {

            $this->renderFile('./vacationrecord/list_super.tpl',$arrData);

        }

    } 


    public function actionFinishing()
    {
        if(Yii::app()->request->getIsPostRequest()){
            $id_card = Yii::app()->request->getParam('id_card'); 
            $absence_type = Yii::app()->request->getParam('absence_type');
            $day_num = Yii::app()->request->getParam('day_num');
            $remark = Yii::app()->request->getParam('remark');

            $userModel = new User();
            $user = $userModel->getItemByIdCard($id_card);
            if($day_num == 0 || empty($user)){
                throw new CHttpException(404,'信息错误，请重新填写');
            }
            $arrData = array();
            $arrData['user_id'] = $user['id'];
            $arrData['absence_type'] = $absence_type;
            $arrData['day_num'] = $day_num;
            $arrData['date'] = date('Y-m-d');
            $arrData['create_time'] = time();
            $arrData['status'] = STATUS_VALID;
            $arrData['operator_id'] = Yii::app()->session['oa_user_id'];
            $arrData['remark'] = $remark;

            $model = new VacationRecord();
            $id = $model->save($arrData);
            if(empty($id)){
                throw new CHttpException(404,'保存失败，请重新提交');
            }
            $arrData['msg'] = "假期修改成功！";
        } 
        $this->renderFile('./vacationrecord/finishing.tpl',$arrData);
    }

    public function actionUpExcel()
    {
        if(Yii::app()->request->getIsPostRequest()){
            $file = Yii::app()->request->getParam('file');
            $fileName = $_FILES['file']['name'];
            $filePath = $_FILES['file']['tmp_name'].'/'.$fileName;
            $suffix = substr($fileName,strpos($fileName,'.')+1);
            if($suffix != 'xls' && $suffix != 'xlsx'){
                throw new CHttpException(404,'您上传的文件格式不对');
            }
            $toPath = Config::$YunStorageDataPath."/datamatch/jiaqiyue".date('Y-m-dHis').rand(10,99).'.xls';
            if(is_uploaded_file($_FILES['file']['tmp_name']) && move_uploaded_file($_FILES['file']['tmp_name'],$toPath)){
                $data = $this->readerExcel($toPath);
                $userModel = new user();
                $vacationModel = new VacationRecord();
                if(is_array($data)){
                    $isfail = array();
                    foreach($data as $item){
                        $row = array();
                        $id_card = $item[0];
                        $user_name = $item[1];
                        $user = $userModel->getItemByIdCard($id_card);
                        if(empty($user)){
                            $isfail[] = $id_card; 
                            continue;
                        }
                        $row['user_id'] = $user['id'];
                        $row['date'] = date('Y-m-d');
                        $row['create_time'] = time();
                        $row['operator_id'] = Yii::app()->session['oa_user_id'];
                        $row['status'] = STATUS_VALID;
                        $row['type']   = 2;
                        foreach(array(1,2) as $val){
                            $row['absence_type'] = $val;
                            if($val == VACATION_BINGJIA){
                                $row['day_num'] = $item[4];
                            }elseif($val == VACATION_NIANJIA){
                                $row['day_num'] = $item[3];
                            }
                            
                            $id = $vacationModel->save($row);
                            if($id == false){
                                Yii::log('上传假期余额保存失败'.print_r($row,true));
                            }
                        }

                    }
                    Yii::log('上传失败用户'.print_r($isfail,true));
                    $arrData['isfail'] = $isfail;
                    $arrData['msg'] = '上传成功！';
                } 
            }else{
                throw new CHttpException(404,'上传失败，请重新上传!');
            }
        }
        $this->renderFile('./vacationrecord/upexcel.tpl',$arrData); 
    }

    public function readerExcel($file)
    {
        set_include_path(get_include_path() . PATH_SEPARATOR . Config :: $basePath . "/common/shared/excel/");
        include_once 'PHPExcel.php';
        include_once 'PHPExcel/Writer/Excel5.php';

        $objReader = PHPExcel_IOFactory::createReaderForFile($file);
        $objPHPExcel = $objReader->load($file);
        $objPHPExcel->setActiveSheetIndex(0);
        $currentObj = $objPHPExcel->getActiveSheet(0);
        $allColumn = $currentObj->getHighestColumn();
        $allRow = $currentObj->getHighestRow();
        $data = array();
        for($currentRow=2;$currentRow <= $allRow;$currentRow++){
            for($currentColumn = 'A';$currentColumn <= $allColumn;$currentColumn++){
                $value = $currentObj->getCell($currentColumn.$currentRow)->getValue(); 
                $data[$currentRow][] = $value;
            }
        }
        return $data;
    }

}
?>
