<?php
define("ONLINE_ORDER_FLAG", 5);
class ApiController extends Controller
{
    public function actionCheckUser()
    {
        $ret = array('code'=>0,'msg'=>'');
        $name = Yii::app()->request->getParam('name'); 
        $model = new User();
        //支持分割符号：中文逗号、|、换行符、制表符、下划线
        $name = str_replace(array("，", "|", "\n", "\t", "_",' '), ',', $name);
        //判断是否多个用户邮箱
        if( strpos($name, ',') !== false ) {
            
            $rd_arr = array();
            $rd_arr = explode(",", $name);

            foreach($rd_arr as $val) {

                if(empty($val)) {

                    continue;

                }

                $email = OAConfig::checkUserAccount($val);
                $user = $model->getItemByEmailOrName($email);
                if(empty($user)){//没有用户

                    $ret['code'] = 1;
                    $ret['msg'] = "没有[{$email}]相关信息...";
                    echo json_encode($ret);
                    exit();

                }

            }


        }else {

            $email = OAConfig::checkUserAccount($name);
            $user = $model->getItemByEmailOrName($email);

            if(empty($user)){//没有用户

                $ret['code'] = 1;
                $ret['msg'] = "没有[{$email}]相关信息...";
                echo json_encode($ret);
                exit();

            }

        }

        echo json_encode($ret);
        exit();

    } 

    public function actionOnlineOrder() {

        $userName = Yii::app()->request->getParam('useraccount');
        $userName = OAConfig::checkUserAccount($userName);

        $userModel = new User();
        $user = $userModel->getItemByEmailOrName($userName);

        if(empty($user) ) {

            $ret['ackCode'] = 2;
            $ret['msg'] = "上线申请人{$userName}不存在";
            echo json_encode($ret);
            exit();

        }
        $user = $user[0];

        //全局变量
        $_POST['user'] = $user;
        
        Yii::app()->session['oa_user_id'] = $user['id'];
        $demo = Yii::app()->session['oa_user_id'];

        $workflow_id = ONLINE_ORDER_FLAG;
        $sectionModel = new Section();
        $section = $sectionModel->getItemByPk($user['section_id']);

        $workflowModel = new WorkFlow();
        $workflow = $workflowModel->getItemByPk($workflow_id);

        $verificationNameArr = array();

        $arrData = array();

        //来源
        $arrData['source_id']      = intval(Yii::app()->request->getParam('projectId') );
        $arrData['source']         = intval(Yii::app()->request->getParam('source') );

        //重复订单
        $model = new OnlineOrder();
        $isRepeat = $model->getRepeatSourceId($arrData['source_id'], $arrData['source']);

        if($isRepeat) {

            $ret['ackCode'] = 2;
            $ret['msg'] = "projectId:".$arrData['source_id']."重复提交";
            echo json_encode($ret);
            exit();

        }


        //配置
        $arrData['title']          = Yii::app()->request->getParam('projectName');
        $arrData['upgrade_type']   = Yii::app()->request->getParam('updateType', 1);
        $arrData['description']    = Yii::app()->request->getParam('releaseNote');
        $arrData['correction_bug'] = Yii::app()->request->getParam('fixedBug');
        $arrData['qa_type']        = Yii::app()->request->getParam('qa_type', 1);
        $arrData['source_code']    = Yii::app()->request->getParam('svnUrl');
        $arrData['online_steps']   = Yii::app()->request->getParam('updateStep');
        $arrData['user_id']        = $user['id'];
        $arrData['status']         = STATUS_VALID;
        $arrData['create_time']    = time();

        //审核人员
        $verificationNameArr['rd'] = Yii::app()->request->getParam('rdEmail');
        $auditList['A']['rd']       = OAConfig::checkUserAccount($verificationNameArr['rd'] );

        $verificationNameArr['pm'] = Yii::app()->request->getParam('pmEmail');
        $auditList['A']['pm']       = OAConfig::checkUserAccount($verificationNameArr['pm'] );

        $verificationNameArr['qa'] = Yii::app()->request->getParam('qaEmail');
        $auditList['A']['qa']       = OAConfig::checkUserAccount($verificationNameArr['qa'] );

        $verificationNameArr['rd_manger'] = Yii::app()->request->getParam('ctoEmail');
        $auditList['A']['rd_manger']       = OAConfig::checkUserAccount($verificationNameArr['rd_manger'] );

        $verificationNameArr['op'] = Yii::app()->request->getParam('opEmail');
        $auditList['B']['op']       = OAConfig::checkUserAccount($verificationNameArr['op'] );
        $_POST['auditlist'] = $auditList;

        //非空验证
        $emptyCheckArr = array('source_id', 'source', 'title', 'description', 'online_steps');
        foreach($emptyCheckArr as $val) {

            if( empty($arrData[$val]) ) {

                $ret['ackCode'] = 2;
                $ret['msg'] = "{$val}参数缺少";
                echo json_encode($ret);
                exit();


            }

        }

        //判断签字人员是否存在
        foreach($verificationNameArr as $key=>$name) {

            $userName = OAConfig::checkUserAccount($name);
            $userTmp = $userModel->getItemByEmailOrName($userName);
            if(empty($userTmp) ) {
                
                $ret['ackCode'] = 2;
                $ret['msg'] = "{$key}不存在";
                echo json_encode($ret);
                exit();

            }

        }

        $id = $model->save($arrData);

        if($id > 0) {

            $userWorkflowId = $this->saveUserWorkflow($workflow, $id, $arrData['user_id']);
            if($userWorkflowId > 0 && $workflow['obj_type'] == 'online_order'){

                $this->createAuditList($userWorkflowId); 

            }

            $workflowService = new WorkflowService($userWorkflowId);
            $workflowService->workflowProcess();

            $ret['ackCode'] = 1;
            $ret['msg'] = "操作成功";
            echo json_encode($ret);
            exit();

        }

        // Yii::log('online_order save fail:'.print_r($arrData,true));
        $ret['ackCode'] = 2;
        $ret['msg'] = "数据库操作失败,请重新提交";
        echo json_encode($ret);
        exit();

    }

    public function saveUserWorkflow($workflow,$obj_id, $user_id)
    {
        $arrData = array();
        $arrData['user_id'] = $user_id;
        $arrData['workflow_id'] = $workflow['id'];
        $arrData['category'] = $workflow['category'];
        $arrData['obj_type'] = $workflow['obj_type'];
        $arrData['obj_id'] = $obj_id;
        $arrData['create_time'] = time();
        $arrData['audit_status'] = OA_WORKFLOW_AUDIT_CHUANGJIAN;
        $arrData['status'] = STATUS_VALID;
        $model = new UserWorkFlow();
        $userWorkflowId = $model->save($arrData);
        if($userWorkflowId < 1){
            Yii::log('userworkflow save fail:'.print_r($arrData,true));
            throw new CHttpException(404,"保存资料失败");
        }
        return $userWorkflowId; 
    }

    public function createAuditList($userWorkflowId)
    {
        $auditList = Yii::app()->request->getParam('auditlist');

        $userModel = new User();
        $oa_user_id = Yii::app()->session['oa_user_id'];
        $model = new AuditList();

        foreach($auditList as $step => $row){
            if(empty($row)){
                continue;
            }
            $index = 1;
            foreach($row as $business_role => $val){
                if(empty($val)){
                    continue;
                }
                $users = $userModel->getItemByEmailOrName(trim($val));
                $user = reset($users);
                if(empty($user)){
                    continue;
                }
                $tmp = array();
                $tmp['pos'] = $index;
                $tmp['user_id'] = $user['id'];
                $tmp['business_role'] = $business_role;
                $tmp['user_workflow_id'] = $userWorkflowId;
                $tmp['workflow_step'] = $step;
                $tmp['create_time'] = time();
                if($business_role == 'rd' && $user['id'] == $oa_user_id){
                    //生成已经审批的记录 
                    $tmp['status'] = STATUS_INVALID;
                    $taskModel = new UserTask();
                    $row = array();
                    $row['user_id'] = $user['id'];
                    $row['user_workflow_id'] = $userWorkflowId;
                    $row['audit_user_id'] = $user['id'];
                    $row['workflow_step'] = $step;
                    $row['audit_status'] = OA_TASK_TONGGUO;
                    $row['create_time'] = $row['modify_time'] = time();
                    $taskModel->save($row);
                }else{
                    $tmp['status'] = STATUS_VALID; 
                }
                $model->save($tmp);
                $index++;
            }
        }
        
        $rowC = $auditList['A'];
        unset($rowC['ue']);
        unset($rowC['rd_manger']);
        if(is_array($rowC)){//上线后签字和上线前签字完全一样 所以不需要用户填写 自动复制一份
            foreach($rowC as $business_role => $val){
                if(empty($val)){
                    continue;
                }
                $users = $userModel->getItemByEmailOrName(trim($val));
                $user = reset($users);
                if(empty($user)){
                    continue;
                }
                $tmp = array();
                $tmp['pos'] = $index;
                $tmp['user_id'] = $user['id'];
                $tmp['business_role'] = $business_role;
                $tmp['user_workflow_id'] = $userWorkflowId;
                $tmp['workflow_step'] = 'C';
                $tmp['create_time'] = time();
                $tmp['status'] = STATUS_VALID; 
                $model->save($tmp);
                $index++;
            }
        }
    }

    //测试环境
    public function actiontest() {

        //映射禅道系统的参数
        // $projectId = 0;
        // $url = "http://zentao.rong360.com/api/onzentao?type=1&projectId={$projectId}";
        // $res = CurlHttp::get($url);
        // $arr = json_decode($res, true);
        // var_dump($arr);

        

    }



}
?>
