<?php

class AttendenceController extends Controller
{
    
    public function actions()
    {

    }
    public function actionInValidate($error)
    {
        $where = "1=1";
    }
    public function translateTime($time)
    {
        $val = strtotime($time);
        if($val)
            return $val;
        $arr = explode('/',$time);
        $tmp = $arr[2]."-".$arr[1]."-".$arr[0];
        $val = strtotime($tmp);
        return $val;
    }
    public function actionSpellSql()
    {
        //$model = new Absence();
        $arrQuery = Yii::app()->request->getQuery('query');
        if (!empty($arrQuery))
        {  
            if( $arrQuery['start_time'] == "" || $arrQuery['end_time'] == "")
            {
                //self::actionInValidate("时间为必输入项，请检查是否输入完整。");
                return "1=1";
            }
           $where = "absence.start_time >=". self::translateTime($arrQuery['start_time'])." and absence.start_time <".(self::translateTime($arrQuery['end_time']) + 86400)." ";
           $id = "";
           if(!empty($arrQuery['name']))
           {
               $model = new User();
               $ret = $model->getIdsByName($arrQuery['name']);
               // $ret = (array)$model->db()->query("select id from user where name='".$arrQuery['name']."'");
                if(!empty($ret))
                {
                   for($intCurrent = 0;$intCurrent < (count($ret) - 1);$intCurrent++)
                       $id = $id.$ret[$intCurrent]['id'].",";
                    $id = $id.$ret[count($ret) - 1]['id'];
                }
                else
                {
                    return "1=2";
                }
           }
           if(!empty($arrQuery['section_name']))
           {
               $model = new Section();
               $ret =  $model->getUserIdsBySectionName($arrQuery['section_name']);
               //$ret = $model->db()->query("select user.id from user,section where user.section_id = section.id and section.name='".$arrQuery['section_name']."'");
               if(!empty($ret))
               {
                   if($id != "")
                       $id = $id.",";
                   for($intCurrent = 0;$intCurrent < (count($ret) - 1);$intCurrent++)
                       $id = $id.$ret[$intCurrent]['id'].",";
                    $id = $id.$ret[count($ret) - 1]['id'];
               }
               else{
                    return "1=2";
               }
           }
           if($id != "")
               $where = $where." and absence.user_id in (".$id.")";
        }
        else
        {
            $time = self::GetInitTimeSpanValue();
            $where = "absence.start_time >=". strtotime($time['start_time_str'])." and absence.start_time <".(strtotime($time['end_time_str']) + 86400)." ";
        }
        $where .= " and type <> 13 and type <> 25 and user_workflow.audit_status != ".OA_WORKFLOW_AUDIT_CHUANGJIAN." and user_workflow.audit_status != ". OA_WORKFLOW_AUDIT_CAOGAO;
        return $where;
    }
    public function actionList()
    {   
        $query = Yii::app()->request->getQuery('query');
        $startDay = $query['start_time'];
        $endDay = $query['end_time'];
        $sectionName = $query['section_name'];
        $userName = $query['name'];
        $idCard = $query['id_card'];
        $intPageSize = 50;
        $intPage = intval(Yii::app()->request->getQuery('page')) < 1 ? 1 : intval(Yii::app()->request->getQuery('page'));
        $offset = ($intPage-1)*$intPageSize;
        $model = new Absence(); 
        $arrTplData['arrList'] = $model->getListByDateConditionWithUwf($startDay,$endDay,$idCard,$sectionName,$userName,$offset,$intPageSize);

        $arrTplData['arrPager'] = array(
            'count'     => $model->getCountByDateCondition($startDay,$endDay,$idCard,$sectionName,$userName),
            'pagesize'  => $intPageSize,
            'page'      => $intPage,
            'pagelink'  => empty($query) ? 'list?page=%d' : 'list?' . urldecode(Yii::app()->request->getQueryString()) . '&page=%d',
        );
        $arrTplData['savelink'] = "SaveAsCsv?".urldecode(Yii::app()->request->getQueryString());

        if(empty(Yii::app()->request->getQuery('query')))
        {
            $query['start_time'] = date('Y-m-26',strtotime('-1 month'));
            $query['end_time'] = date('Y-m-25');
            $arrTplData['initTime'] = $query;
        }
        $this->renderFile("attendence.tpl",$arrTplData);
    }

    public function GetInitTimeSpanValue()
    {
        $date = 26;

        $month = date("m");
        $month_start = (date("d")>=26) ? $month : ( ($month==12) ? 1:($month-1) );
        $month_end = (date("d")>=26) ? ( ($month==12) ? 1:($month+1)) : $month;

        $year = date("y");
        $year_start = ($month_start != $month)&&($month_start==12) ? ($year-1) : ($year);
        $year_end = ($month_end != $month)&&($month==12) ? ($year+1) : ($year);
        
        if($month_start < 10)
            $month_start = "0".intval($month_start);
        if($month_end < 10)
            $month_end = "0".intval($month_end);
        
        $time['start_time'] = "20".$year_start."-".$month_start."-26";
        $time['end_time'] = "25/".$month_end."/20".$year_end;
        $time['end_time'] = "20".$year_end."-".$month_end."-25";
        $time['start_time_str'] = "20".$year_start."-".$month_start."-26";
        $time['end_time_str'] = "20".$year_end."-".$month_end."-25";
        return $time;
    }
    public function actionSaveAsCsv()
    {
        $query = Yii::app()->request->getQuery('query');
        $startDay = $query['start_time'];
        $endDay = $query['end_time'];
        $sectionName = $query['section_name'];
        $userName = $query['name'];
        $idCard = $query['id_card'];
        $model = new Absence();

        $dataList = $model->getListByDateCondition($startDay,$endDay,$idCard,$sectionName,$userName);

        header('Content-Type: application/csv');

        // $arrQuery = Yii::app()->request->getQuery('query');
        if(empty($query))
        {
            $startDay = date('Y-m-26',strtotime('-1 month'));
            $endDay = date('Y-m-25');
            $filename = "考勤统计(".$startDay."-".$endDay.").csv";
        }
        else
        {
            $filename = "考勤统计(".$startDay."-".$endDay.").csv";
        }
            header("Content-Disposition: attachment; filename=".$filename."");
        
        $csvTitle = array( '姓名','工号','部门','上级部门','标题','天数','请假原因','开始时间','结束时间','请假类别','状态' );
        $output = fopen('php://output','w') or die("Can't open php://output");
    
        for($intNum = 0; $intNum< 11;$intNum++){
            $csvTitle[$intNum] = iconv("UTF-8","gbk",$csvTitle[$intNum]);
        }
        fputcsv($output,$csvTitle);

        foreach ($dataList as $n => $val){
            $index = 0;
            $data = array();
            foreach ($val as $current){
                if($index == 1){
                    $data[$index] = (string)iconv("UTF-8","gbk",$current);
                }elseif($index == 7 || $index == 8){
                    $data[$index] = iconv("UTF-8","gbk",date('Y-m-d', $current).OAConfig::$time_node[date('H:i:s',$current)]);

                }else if($index == 9){
                    $data[$index] = iconv("UTF-8","gbk",OAConfig::$absenceType["$current"]);
                }else if($index == 10)
                {
                    $item = intval($current);
                    switch($item)
                    {
                        case OA_WORKFLOW_AUDIT_CHUANGJIAN:
                            $data[$index] = iconv("UTF-8","gbk","创建工作流");break;
                        case OA_WORKFLOW_AUDIT_SHENPIZHONG:
                            $data[$index] = iconv("UTF-8","gbk","审批中");break;
                        case OA_WORKFLOW_AUDIT_PASS:
                            $data[$index] = iconv("UTF-8","gbk","通过");break;
                        case OA_WORKFLOW_AUDIT_REJECT:
                            $data[$index] = iconv("UTF-8","gbk","拒绝");break;
                        case OA_WORKFLOW_AUDIT_CANCEL:
                            $data[$index] = iconv("UTF-8","gbk","取消审核");break;
                    }
                }else{
                    $data[$index] = iconv("UTF-8","gbk",$current);
                }
                $index++;
            }
		    fputcsv($output,$data);	
		}
        fclose($output) or die("Can't close php://output");            
    }
}
