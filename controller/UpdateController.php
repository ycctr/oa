<?php
class UpdateController extends Controller
{
    public function actionUploadFile()
    {
        if(Yii::app()->request->getIsPostRequest()){
            $filePath = OAConfig::$OaDataPath;

            if($_FILES['dasaifile']['error'] > 0){
                $ret = array(
                    'code' => 1,
                    'msg'  => '上传失败，请重新上传' 
                );
            }else{
                $name = $_FILES['dasaifile']['name'];
                $suffix = substr($name,strpos($name,'.')+1);
                Yii::log('datamatch file type:'.print_r($_FILES,true));
                if((int)$_FILES['dasaifile']['size'] > 1000000){
                    $ret = array(
                        'code' => 2,
                        'msg'  => '上传文件太大，系统暂不支持'    
                    );
                }else{
                    do{
                        $fileAddress = $this->createNewName($filePath,$name,$suffix);
                    }while(file_exists($fileAddress)); 

                    if(!(is_uploaded_file($_FILES['dasaifile']['tmp_name']) && move_uploaded_file($_FILES['dasaifile']['tmp_name'],$fileAddress))){
                        $ret = array(
                            'code' => 4,
                            'msg'  => '上传失败，请重新上传'    
                        );
                    }
                }
            } 
            if($ret['code'] == 0){
                $ret['address'] = substr($fileAddress,strlen($filePath));
                $ret['filename'] = $name;
            }
            echo json_encode($ret);die;
        } 
    }


    public function createNewName($filePath,$fileName,$suffix)
    {
        $time = date('Y-m-dHis');

        return $filePath.$time.rand(10,99).'--'.$fileName;
    }

    //下载
    public function actionDown()
    {
        $file = Yii::app()->request->getParam('file');
        $file = urldecode($file);
        $file = OAConfig::$OaDataPath . $file;
        list(,$filename) = explode('--',$file);    

        $fp = fopen($file,'r');
        $filesize = filesize($file);
        Header("Content-type: application/octet-stream;charset=utf-8"); 
        Header("Accept-Ranges: bytes"); 
        Header("Accept-Length:".$filesize); 
        Header("Content-Disposition: attachment; filename=\"$filename\""); 

        $buffer=1024; 
        $file_count=0; 
        //向浏览器返回数据 
        while(!feof($fp) && $file_count<$filesize){ 
            $file_con=fread($fp,$buffer); 
            $file_count+=$buffer; 
            echo $file_con; 
        } 
        fclose($fp);
    }

    public function actionUploadYunpan()
    {
        $ret = ['code'=>1,'address'=>'','msg'=>'上传失败'];
        if(!empty($_FILES)){

            $tempFile = $_FILES['Filedata']['tmp_name'];
            $fileInfo = pathinfo($_FILES['Filedata']['name']);
            $fileExtension = strtolower($fileInfo['extension']);
            $fileSize = $_FILES['Filedata']['size'];
            $fileContent = file_get_contents($tempFile);
            $contentMd5 = md5($fileContent);

            $param = [];
            //默认路径 
            $path = '/upload/'   . $fileExtension . '/'
                . substr($contentMd5,0,2) . '/' . substr($contentMd5,2,2) . '/'
                . $contentMd5 . '.' . $fileExtension;

            $targetFilePath = Config::$YunStorageDataPath . $path;
            $pathInfo = pathinfo($targetFilePath);
            if (! is_dir($pathInfo['dirname']))
            {
                GeneralUtil::mkdirs($pathInfo['dirname']);
            }
            $is_move = move_uploaded_file($tempFile,$targetFilePath);

            if($is_move) {
                $path = 'http://s0.rong360.com' . $path;
                $ret['code'] = 0;
                $ret['address'] = $path;
            }

            echo json_encode($ret);
            Yii::app()->end();
        }

    }
}

?>
