<?php
class CheckinoutController extends Controller
{
    const PAGENUMBER = 22;
    public function actionList() 
    {
        // $rongCache = new RongCache();
        // $key = KAOQINERROR.date('Y-m-d');
        // $kaoqinException = $rongCache->get($key);

        $user = Yii::app()->params['user'];
        $sectionModel = new Section();
        $arrData['isManager'] = $sectionModel->isSectionManager($user['id']);
        $start_day = Yii::app()->request->getParam('start_day');
        $end_day = Yii::app()->request->getParam('end_day');
        $subordinate_name = Yii::app()->request->getParam('subordinate_name');
        $arrData['subordinate_name'] = $subordinate_name;
        if(!empty($subordinate_name)){
            $checkinoutUserId = $sectionModel->isSubordinates($subordinate_name,$user['id']);
            if(!$checkinoutUserId){
                throw new CHttpException(404,'您没有权限查看该名员工的考勤记录，请找人力提供'); 
            }
            $userModel = new User();
            $checkinoutUser = $userModel->getItemByPk($checkinoutUserId);
        }else{
            $checkinoutUser = $user;
        }
        $pn = Yii::app()->request->getParam('pn',1);
        $start = ($pn-1)*self::PAGENUMBER;

        $url = '/checkinout/list/?';
        $url.='start_day='.$start_day.'&end_day='.$end_day.'&subordinate_name='.$subordinate_name.'&';
        $arrData['start_day'] = $start_day;
        $arrData['end_day'] = $end_day;
        $arrData['pn'] = $pn;
        $arrData['rn'] = self::PAGENUMBER;
        $arrData['url'] = $url;
        $arrData['user'] = $checkinoutUser;
        $arrData['section'] = $sectionModel->getItemByPk($checkinoutUser['section_id']);
        $model = new CheckInOut();

        $arrData['list'] = $model->getListByUserId($checkinoutUser['id'],$start_day,$end_day,$start,self::PAGENUMBER);

        $arrData['listCnt'] = $model->getListCntByUserId($checkinoutUser['id'],$start_day,$end_day);
        $arrData['week'] = array('日','一','二','三','四','五','六');
        $arrData['absenteeismType'] = OAConfig::$absenteeismType;
        $arrData['absenteeismTypeAll'] = OAConfig::absenteeismTypeAll();
        // $arrData['kaoqinException'] = $kaoqinException;

        $this->renderFile('./checkinout/list.tpl',$arrData);

    }

    public function actionEdit()
    {
        $id = Yii::app()->request->getParam('id');
        $absenteeism_type = Yii::app()->request->getParam('absenteeism_type'); 
        $user = Yii::app()->params['user'];

        $ret = array('code'=>0,'msg'=>'');
        $model = new CheckInOut();
        $item = $model->getItemByPk($id);
        if($item['user_id'] != $user['id']){
            $ret['code'] = 1;
            $ret['msg'] = '你没有权限更改';
            echo json_encode($ret);
            Yii::app()->end();
        }

        $item['absenteeism_type'] = $absenteeism_type;
        $item['modify_time'] = time();
        $model->save($item);
        echo json_encode($ret);
        Yii::app()->end();
    }



}

?>
