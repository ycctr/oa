<?php
class  OnlineorderapiController extends CController
{
    public function actions()
    {

    }
    public function actionParams()
    {
        $arrData = array();
        $arrData['deploy_id']      = Yii::app()->request->getParam('deploy_id');
        $arrData['deploy_creator'] = Yii::app()->request->getParam('deploy_creator');
        $arrData['deploy_rd']      = Yii::app()->request->getParam('deploy_rd');
        $arrData['deploy_qa']      = Yii::app()->request->getParam('deploy_qa');
        $arrData['deploy_pm']      = Yii::app()->request->getParam('deploy_pm');
        $arrData['deploy_op']      = "zhanggg";
        $arrData['deploy_name']    = Yii::app()->request->getParam('deploy_name');
        $arrData['deploy_info']    = Yii::app()->request->getParam('deploy_info');
        $arrData['deploy_module']  = Yii::app()->request->getParam('deploy_module');
        $arrData['token']          = Yii::app()->request->getParam('token');

        $tokenstr = $arrData['deploy_id']."Rong360";
        $md5value = md5($tokenstr);
        if($md5value != $arrData['token']){
            $ret['errcode']   = "0";
            $ret['errmsg']    = "非法数据";
            echo json_encode($ret, true);
            Yii::app()->end();
        }

        if(empty($_POST['deploy_id']) || empty($_POST['deploy_rd']) || empty($_POST['deploy_info']) || empty($_POST['deploy_creator']) || empty($_POST['deploy_name']) || empty($_POST['deploy_module']) || empty($_POST['token'])){
            $ret['errcode']   = "0";
            $ret['errmsg']    = "数据不全";
            echo json_encode($ret, true);
            Yii::app()->end();
        }
        //数据保存
        $onlineModel = new OnlineOrder();
        $userModel   = new User();
        $userItem = $userModel->getItemByEmail(trim($arrData['deploy_creator']));
        if(empty($userItem)){
            $ret['errcode']   = "0";
            $ret['errmsg']    = "上线单创建者找不到！";
            echo json_encode($ret, true);
            Yii::app()->end();
        }
        $sectionModel = new Section();
        $sectionItem = $sectionModel->getItemByPk($userItem['section_id']);
        $leaderItem  = $userModel->getItemByPk($sectionItem['manager_id']);
        if(!empty($leaderItem)){
            $arrData['deploy_leader'] = $leaderItem['email'];
        }else{
            $arrData['deploy_leader'] = "liucaofeng";
        }

        $onlineData = array();
        $onlineData['user_id']       = $userItem['id'];
        $onlineData['title']         = $arrData['deploy_name'];
        $onlineData['description']   = $arrData['deploy_info'];
        $online_step = json_decode($arrData['deploy_module']);
        $json_str = "";
        foreach($online_step as $key=>$value){
            $json_str .= $key.". ".$value."<br>";
        }
        $onlineData['online_steps']  = $json_str;//$arrData['deploy_module'];
        $onlineData['create_time']   = time();
        $onlineData['status']        = STATUS_VALID;
        $online_id = $onlineModel->save($onlineData);
        if($online_id < 1){
            $ret['errcode']   = "0";
            $ret['errmsg']    = "上线单数据保存失败！";
            echo json_encode($ret, true);
            Yii::app()->end();
        }

        $uwfModel    = new UserWorkFlow();
        $uwfData = array();
        $uwfData['user_id']      = $userItem['id'];
        $uwfData['workflow_id']  = 5;
        $uwfData['category']     = 40;
        $uwfData['obj_type']     = "online_order";
        $uwfData['obj_id']       = $online_id;
        $uwfData['create_time']  = time();
        $uwfData['audit_status'] = OA_WORKFLOW_AUDIT_CHUANGJIAN;
        $uwfData['status']       = STATUS_VALID;
        $uwfData['deploy_id']    = $arrData['deploy_id'];
        $isExistsDeployId = $uwfModel->getItemByDeployId($arrData['deploy_id']);
        if( $isExistsDeployId == false){
            $userWorkflowId = $uwfModel->save($uwfData);
        }else{
            $ret['errcode']   = "0";
            $ret['errmsg']    = "上线单ID已经存在";
            echo json_encode($ret, true);
            Yii::app()->end();

        }
        if($userWorkflowId < 1){
            $ret['errcode']   = "0";
            $ret['errmsg']    = "工作流程数据保存失败！";
            echo json_encode($ret, true);
            Yii::app()->end();
        }


        $auditList['A']['rd'] = $arrData['deploy_rd'];
        $verificationNameArr['rd'] =  $auditList['A']['rd'];
        $auditList['A']['rd']      = OAConfig::checkUserAccount($verificationNameArr['rd'] );

        if(!empty($arrData['deploy_pm']))
        {
            $auditList['A']['pm'] = $arrData['deploy_pm'];
            $verificationNameArr['pm'] = $auditList['A']['pm'];
            $auditList['A']['pm']       = OAConfig::checkUserAccount($verificationNameArr['pm'] );
        }

        if(!empty($arrData['deploy_qa']))
        {
            $auditList['A']['qa'] = $arrData['deploy_qa'];
            $verificationNameArr['qa'] = $auditList['A']['qa'];
            $auditList['A']['qa']       = OAConfig::checkUserAccount($verificationNameArr['qa'] );
        }

        $auditList['A']['rd_manger'] = $arrData['deploy_leader'];
        $verificationNameArr['rd_manger'] = $auditList['A']['rd_manger'];
        $auditList['A']['rd_manger']       = OAConfig::checkUserAccount($verificationNameArr['rd_manger'] );

        $auditList['B']['op'] = $arrData['deploy_op'];
        $verificationNameArr['op'] = $auditList['B']['op'];
        $auditList['B']['op']       = OAConfig::checkUserAccount($verificationNameArr['op'] );

        $_POST['auditlist'] = $auditList; 
        Yii::app()->session['oa_user_id'] = $userItem['id'];
        $auditlistservice = new AuditListService();
        $auditlistservice->createAuditList($userWorkflowId);
        $workflowService = new WorkflowService($userWorkflowId);
        $workflowService->workflowProcess();//audit operation

        $ret['errcode']   = "1";
        $ret['errmsg']    = "上线单创建成功";
        echo json_encode($ret, true);
        Yii::app()->end();

    }

    public function actionResult()
    {
        $arrData = array();
        $arrData['deploy_id']          = Yii::app()->request->getParam('deploy_id');
        $arrData['token']              = Yii::app()->request->getParam('token');
        //$arrData['deploy_op']          = Yii::app()->request->getParam('deploy_op',"zhanggg");
        $arrData['deploy_status']      = Yii::app()->request->getParam('deploy_status');
        $operation          = Yii::app()->request->getParam('operation');
        $tokenstr = $arrData['deploy_id']."Rong360";
        $md5value = md5($tokenstr);
        if($md5value != $arrData['token']){
            $ret['errcode']   = "0";
            $ret['errmsg']    = "非法数据";
            echo json_encode($ret, true);
            Yii::app()->end();
        }

        if(empty($_POST['deploy_id']) || empty($_POST['token']) || empty($_POST['deploy_status'])){
            $ret['errcode']   = "0";
            $ret['errmsg']    = "数据不全";
            echo json_encode($ret, true);
            Yii::app()->end();
        }

        $uwfModel = new UserWorkFlow(DBModel::MODE_RW);
        $uwfItem = $uwfModel->getItemByDeployId($arrData['deploy_id']);
        if(false == $uwfItem){
            $ret['errcode']   = "0";
            $ret['errmsg']    = "数据找不到";
            echo json_encode($ret, true);
            Yii::app()->end();
        }

        if($arrData['deploy_status'] == "S") 
        {
            $res = $uwfModel->updateWorkflowStatusById($uwfItem[0]['id'],OA_WORKFLOW_AUDIT_SHENPIZHONG);
            if($res == false){
                $ret['errcode']   = "0";
                $ret['errmsg']    = "数据更新成功";
                echo json_encode($ret, true);
                Yii::app()->end();
            }
            $workflowService = new WorkflowService($uwfItem[0]['id']);
            $workflowService->workflowProcess();//audit operation

        }else{
            if($operation == 'rollback'){
                $res = $uwfModel->updateWorkflowStatusById($uwfItem[0]['id'],OA_WORKFLOW_AUDIT_ROLLBACK);
            }else{
                $res = $uwfModel->updateWorkflowStatusById($uwfItem[0]['id'],OA_WORKFLOW_AUDIT_REJECT);
            }
            if($res == false){
                $ret['errcode']   = "0";
                $ret['errmsg']    = "数据更新失败";
                echo json_encode($ret, true);
                Yii::app()->end();
            }
        }

        $ret['errcode']   = "1";
        $ret['errmsg']    = "信息更新成功";
        echo json_encode($ret, true);
        Yii::app()->end();

    }

    public function actionMandatoryaccesssingle()
    {
        $deploy_id  = Yii::app()->request->getParam('deploy_id');
        $audit_desc = Yii::app()->request->getParam('desc');
        $user_id    = Yii::app()->request->getParam('user_id');


        if( empty($deploy_id) || empty($audit_desc) ){
            $ret['errcode']   = "0";
            $ret['errmsg']    = "数据不全";
            echo json_encode($ret, true);
            Yii::app()->end();
        }

        $uwfModel = new UserWorkFlow(DBModel::MODE_RW);
        $uwfItem = $uwfModel->getItemByDeployId($deploy_id);
        $user_workflow_id = intval($uwfItem[0]['id']);
        if(false == $uwfItem){
            $ret['errcode']   = "0";
            $ret['errmsg']    = "数据找不到";
            echo json_encode($ret, true);
            Yii::app()->end();
        }
        //user_task set audit_status = 1 for the same userworkflow
        $userTaskModel =  new UserTask(DBModel::MODE_RW);
        $audit_desc = '"'.$audit_desc.'"';
        $result = $userTaskModel->updateAuditStatusAndDescByuwfId($user_workflow_id,$audit_desc,$user_id);
        $isExists = $userTaskModel->checkIsExistsNotAuditRecord($user_workflow_id);
        if( true == $isExists){
            $ret['errcode']   = "1";
            $ret['errmsg']    = "";
            echo json_encode($ret, true);
            Yii::app()->end();
        }


        $auditlistModel = new AuditList(DBModel::MODE_RW);
        if( $result == true){
            //search if has other audit_list or send finish message
            $cntAl = $auditlistModel->countAstepNumByUserWorkflowId($user_workflow_id);
            $cntUt = $userTaskModel->countAstepAuditNumByUserWorkflowId($user_workflow_id);
            if( $cntAl > $cntUt){//get next data to generate user task record
                $user_id = $userTaskModel->getLastAuditRecord($user_workflow_id);
                $pos     = $auditlistModel->getPos($user_workflow_id,$user_id);
                $item    = $auditlistModel->getAuditListInfo($user_workflow_id,$pos);
                $arrData['user_id'] = $item['user_id'];
                $arrData['role_id'] = 0;
                $arrData['user_workflow_id'] = $user_workflow_id;
                $arrData['audit_user_id']    = 0;
                $arrData['audit_status']     = 0;
                $arrData['audit_desc']       = $item['business_role'];
                $arrData['workflow_step']    = 'A';
                $arrData['create_time']      = time();
                $arrData['entrust_user_id']  = 0;
                $userTaskModel->save($arrData);
                //update audit list data
                $uad = $auditlistModel->updateAuditListInfo($user_workflow_id,$item['user_id']);
                $ret['errcode']   = "1";
                $ret['errmsg']    = "强制审批通过,已经生成下一个审批节点";
                echo json_encode($ret, true);
                Yii::app()->end();
            }else{
                $auditListService = new AuditListService();
                $data = $auditListService->onLinePlatformAPI("online_order",$deploy_id,"T");
                $ret['errcode']   = "1";
                $ret['errmsg']    = "强制审批通过,可以上线了.";
                echo json_encode($ret, true);
                Yii::app()->end();

            }
        }else{
            $ret['errcode']   = "0";
            $ret['errmsg']    = "强制审批失败,可能当前流程已经审批完成";
            echo json_encode($ret, true);
            Yii::app()->end();
        }

    }

    public function actionMandatoryaccess()
    {
        $deploy_id = Yii::app()->request->getParam('deploy_id');
        $token     = Yii::app()->request->getParam('token');

        $tokenstr = $deploy_id."Rong360";
        $md5value = md5($tokenstr);
        if($md5value != $token){
            $ret['errcode']   = "0";
            $ret['errmsg']    = "非法数据";
            echo json_encode($ret, true);
            Yii::app()->end();
        }

        if( empty($deploy_id) || empty($token )){
            $ret['errcode']   = "0";
            $ret['errmsg']    = "数据不全";
            echo json_encode($ret, true);
            Yii::app()->end();
        }

        $uwfModel = new UserWorkFlow(DBModel::MODE_RW);
        $uwfItem = $uwfModel->getItemByDeployId($deploy_id);
        $user_workflow_id = intval($uwfItem[0]['id']);
        if(false == $uwfItem){
            $ret['errcode']   = "0";
            $ret['errmsg']    = "数据找不到";
            echo json_encode($ret, true);
            Yii::app()->end();
        }


        //user_task set audit_status = 1 for the same userworkflow
        $userTaskModel =  new UserTask(DBModel::MODE_RW);
        $auditlistModel = new AuditList(DBModel::MODE_RW);

        $result = $userTaskModel->updateAuditStatusByuwfId($user_workflow_id);
        $cntAl = $auditlistModel->countAstepNumByUserWorkflowId($user_workflow_id);
        $cntUt = $userTaskModel->countAstepAuditNumByUserWorkflowId($user_workflow_id);
        $runNum = $cntAl - $cntUt;
        while( $runNum > 0){
            $item = $auditlistModel->getNextAuditorInfo($user_workflow_id);
            $arrData['user_id'] = $item['user_id'];
            $arrData['role_id'] = 0;
            $arrData['user_workflow_id'] = $user_workflow_id;
            $arrData['audit_user_id']    = 0;
            $arrData['audit_status']     = 1;
            $arrData['audit_desc']       = $item['business_role'];
            $arrData['workflow_step']    = 'A';
            $arrData['create_time']      = time();
            $arrData['entrust_user_id']  = 0;
            $userTaskModel->save($arrData);
            //update audit list data
            $uad = $auditlistModel->updateAuditListInfo($user_workflow_id,$item['user_id']);
            //$ret['errcode']   = "0";
            //$ret['errmsg']    = "已经生成下一个审批节点";
            //echo json_encode($ret, true);
            //Yii::app()->end();
            $runNum = $runNum - 1;
        }

        if( $runNum == 0 ){
            $auditListService = new AuditListService();
            $data = $auditListService->onLinePlatformAPI("online_order",$deploy_id,"T");
            $ret['errcode']   = "1";
            $ret['errmsg']    = "";//"强制审批通过";
            echo json_encode($ret, true);
            Yii::app()->end();
        }else{
            $ret['errcode']   = "0";
            $ret['errmsg']    = "强制审批失败";
            echo json_encode($ret, true);
            Yii::app()->end();
        }
    }

}
