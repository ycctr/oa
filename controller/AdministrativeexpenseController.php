<?php
class AdministrativeexpenseController extends Controller
{
    public static $paymentCompanys = array(
        1 => 'WFOE-北京融联世纪信息技术有限公司',
        2 => 'ICP-北京融世纪信息技术有限公司',
        3 => '北京融汇金融信息服务有限公司',
        4 =>'其他'   
    );

    public static $paymentMethods = array(
        1 => '银行电汇',
        2 => '支票',
        3 => '现金',
        4 => '其他'   
    );

    public static $paymentNatures = array(
        1 => '一次性付款',
        2 => '首付',
        3 => '进度款',
        4 => '尾款'   
    );

    //因为这里的部门有包含关系，所以子部门放在前面，顺序不可错，都是被逼的
    public static $sectionIds = [48,86,93,100,143,92,99,132];

    public function actionAdd()
    {
        $user = Yii::app()->params['user'];//from base Controller
        $workflow_id = Yii::app()->request->getParam('workflow_id');
        $workflow_id = intval($workflow_id);
        $sectionModel = new Section();
        $section = $sectionModel->getItemByPk($user['section_id']);
        $sections = $sectionModel->getParentSectionBySectionId($section['id']);
        $sections = array_merge(array($section),$sections);
        $arrData['sections'] = $sections;
        $workflowModel = new WorkFlow();
        $workflow = $workflowModel->getItemByPk($workflow_id);

        if(empty($workflow_id) || empty($workflow)){
            throw new CHttpException(404,'缺少参数');
        }

        $sectionIds = array(); 
        if(is_array($sections)){
            foreach($sections as $item)
               $sectionIds[] = $item['id']; 
        }
        if(empty(array_intersect(self::$sectionIds,$sectionIds)) && $user['id']!=1636){
            throw new CHttpException(404,'您没有权限申请');
        }

        $arrData['user'] = $user;
        $arrData['section'] = $section;
        $arrData['workflow_id'] = $workflow_id;
        $arrData['workflow'] = $workflow;
        $arrData['paymentCompanys'] = self::$paymentCompanys;
        $arrData['paymentMethods'] = self::$paymentMethods;
        $arrData['paymentNatures'] = self::$paymentNatures;

        $this->renderFile('./administrative_expense/add.tpl',$arrData);
    }

    public function actionAddPost()
    {
        $user = Yii::app()->params['user'];
        $workflow_id = Yii::app()->request->getParam('workflow_id');
       
        if(Yii::app()->request->getIsPostRequest()){
            $arrPost = array();
            $payment_company = Yii::app()->request->getParam('payment_company');
            if($payment_company == 4){
                $arrPost['payment_company'] = Yii::app()->request->getParam('payment_company_name');
            }else{
                $payment_company = Yii::app()->request->getParam('payment_company');
                $arrPost['payment_company'] = self::$paymentCompanys[$payment_company];
            }
            $arrPost['summary'] = Yii::app()->request->getParam('summary');
            $arrPost['supplier'] = Yii::app()->request->getParam('supplier');
            $arrPost['amount'] = Yii::app()->request->getParam('amount');
            $payment_method = Yii::app()->request->getParam('payment_method');
            if($payment_method == 4){
                $arrPost['payment_method'] = Yii::app()->request->getParam('payment_method_name');
            }else{
                $payment_method = Yii::app()->request->getParam('payment_method');
                $arrPost['payment_method'] = self::$paymentMethods[$payment_method];
            }
            $payment_nature = Yii::app()->request->getParam('payment_nature');
            $arrPost['payment_nature'] = self::$paymentNatures[$payment_nature];
            $arrPost['bank_account'] = Yii::app()->request->getParam('bank_account');
            $arrPost['account_name'] = Yii::app()->request->getParam('account_name');
            $arrPost['account_number'] = Yii::app()->request->getParam('account_number');
            $arrPost['img_address'] = trim(Yii::app()->request->getParam('img_address'),';');
            $arrPost['remark'] = Yii::app()->request->getParam('remark');
            $arrPost['create_time'] = time();
            $arrPost['user_id'] = $user['id'];
            
            $model = new AdministrativeExpense();
            $id = $model->save($arrPost);

            $workflowModel = new WorkFlow();
            $workflow =  $workflowModel->getItemByPk($workflow_id);
            
            $userWorkflowModel = new UserWorkFlow();
            $userWorkflowId = $userWorkflowModel->saveUserWorkflow($workflow,$id);

            $workflowService = new WorkflowService($userWorkflowId);
            $workflowService->workflowProcess();//audit operation
            $this->redirect(Yii::app()->createURL('userworkflow/my'));
        }
    }

}

