<?php
class MoneyrightController extends Controller
{
    //各部门付款金额权限编辑
    public function actionManageMoney(){
        $model = new Section();
        $perPageNum = 50;//每页展示的记录条数
        $currentPage = 1;
        $Condition = "";
        if(Yii::app()->request->getIsPostRequest()){
            $Condition = $_POST['Condition'];
        }
        else if(!empty($_GET['pn']))
        {
           $currentPage = $_GET['pn'];
           $Condition = $_GET['Condition'];
        }
        $recordCount = $model->getCountWithCondition($Condition);
        $arrData = $model->getItemByPageAndPerPageNum($currentPage,$perPageNum,$Condition);
        if(!is_array($arrData))
        {
            $arrData = array();
        }
        $tplData = array();
        $tplData['Condition'] = $Condition;
        $tplData['data']['info'] = $arrData;
        $tplData['data']['page']['pn'] = intval($currentPage);
        $tplData['data']['page']['recordCount'] = $recordCount;
        $tplData['data']['page']['pagesize'] = $perPageNum;

        $this->renderFile("sectionManageMoney.tpl",$tplData);
    }

    public function actionEditMoney(){
        if(Yii::app()->request->getIsPostRequest()){
            $money = Yii::app()->request->getParam('loan');
            $sectionId = Yii::app()->request->getParam('section_id');
            $model = new SectionMoneyRight();
            $ret = $model->getItemBySectionRight($sectionId,1);
            if($ret){
                if($money){
                    $ret['money'] = $money;
                    $model->save($ret);
                }else{
                    $model->deleteByPk($ret['id']);
                }
            }else{
                if($money){
                    $ret['section_id'] = $sectionId;
                    $ret['section_right_id'] = 1;
                    $ret['money'] = $money;
                    $model->save($ret);
                }
            }
            Yii::app()->request->redirect(Yii::app()->createURL('section/manageMoney'));
        }

        $sectionId = Yii::app()->request->getParam('id');
        $sectionModel = new Section();
        $sectionInfo = $sectionModel->getItemByPk($sectionId);
        $arrData['section'] = $sectionInfo;
        $model = new SectionMoneyRight();
        $ret = $model->getItemBySectionRight($sectionId,1);
        $arrData['money'] = $ret ? $ret['money'] : 0;

        $this->renderFile("sectionEditMoney.tpl",$arrData);
    }
}
