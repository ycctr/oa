<?php
/**
 * 我的团队
 * 
 **/
class MyTeamController extends Controller {

    const PAGENUMBER = 22;
    public function actions() {

    }

    public function actionList() {

       $oa_user_id = intval(Yii::app()->session['oa_user_id']);
       $myService = new MyTeamService();

       $per = 15;
       $sectionId = Yii::app()->request->getParam('section_id', 0);
       $tagname   = Yii::app()->request->getParam('tagname', '');
       $cur_page  = Yii::app()->request->getParam('cur_page', '1');

       $isAddTagSection = false;
       if($sectionId != 0) {

           $sectionTaget = $myService->getSectionObj($sectionId);
           $isAddTagSection = true;

       }else {
       
           $sectionTaget = array('0'=>"&nbsp;&nbsp;下属部门");

       }

       $sectionTagetId   = key($sectionTaget);
       $sectionTagetName = $sectionTaget[$sectionTagetId];

       //获取通过经理获取所有部门id
       $teamEncouageModel = new TeamEncourageService();
       $allSectionData = $teamEncouageModel->getSectionEncourage($oa_user_id);
       $allSectionId = array_keys($allSectionData); 

       //判断是否权限
       if($sectionTagetId != 0 && !in_array($sectionTagetId, $allSectionId) ) {

            throw new CHttpException(404,'该部门您无权限查看!');

       }


       $sectionArr = $myService->getSubSectionId($sectionTagetId, $oa_user_id);

       if($isAddTagSection){ //仅录入部门id和名字

          $sectionArr[$sectionTagetId]['name'] = $sectionTagetName; 

       }
       
       $keySectionList = array_keys($sectionArr);



       $sectionStrId = implode(",", $keySectionList);

       $condition = array(

           'section_id'   => $sectionStrId, 
           'tagname'      => $tagname, 

       );

       if($cur_page < 1) {

           $cur_page = 1;

       }
       $start = ($cur_page - 1 ) * $per;
       $limitStr = " LIMIT {$start},{$per} "; 

       $res = $myService->searchMyTeam($condition, $limitStr);
       $userList   = $res['data'];
       $page_count = $res['total'];
       $sectionList = $myService->getSectionList($oa_user_id);


       $arrData['sectionList'] = $sectionList;
       $arrData['user_list'] = $userList;
       $arrData['sectionInfo'] = $sectionArr;
       $arrData['section_taget_id']   = $sectionTagetId;
       $arrData['section_taget_name'] = $sectionTagetName;
       $arrData['tagname']    = $tagname;
       $arrData['page_count'] = $page_count;  
       $arrData['per'] = $per;  
       $arrData['cur_page'] = $cur_page;  

       $this->renderFile("myTeam.tpl", $arrData);

    }


    public function actionCheckList() 
    {

        //当前账号可以查看的部门
        $oa_user_id = intval(Yii::app()->session['oa_user_id']);
        $teamEncouageModel = new TeamEncourageService();
        $allSectionData = $teamEncouageModel->getSectionEncourage($oa_user_id);
        $allSectionId = array_keys($allSectionData); 

        $user_id = Yii::app()->request->getParam('id');
        $userModel = new User();
        $user = $userModel->getItemByPk($user_id);
        $sectionId = $user['section_id'];

        if(!in_array($sectionId, $allSectionId) ) {

            throw new CHttpException(404,'该员工您无权限查看考勤记录!');

        }


        $sectionModel = new Section();
        $arrData['isManager'] = $sectionModel->isSectionManager($user['id']);
        $start_day = Yii::app()->request->getParam('start_day');
        $end_day = Yii::app()->request->getParam('end_day');
        $checkinoutUser = $user;

        $pn = Yii::app()->request->getParam('pn',1);
        $start = ($pn-1)*self::PAGENUMBER;

        $url = '/myteam/checklist/?id='.$user_id;
        if(!empty($start_day)){
            $url .= '&start_day='.$start_day;
        }
        if(!empty($end_day)){
            $url .= '&end_day='.$end_day;
        }
        $arrData['start_day'] = $start_day;
        $arrData['end_day'] = $end_day;
        $arrData['pn'] = $pn;
        $arrData['rn'] = self::PAGENUMBER;
        $arrData['url'] = $url;
        $arrData['user'] = $checkinoutUser;
        $arrData['section'] = $sectionModel->getItemByPk($checkinoutUser['section_id']);
        $model = new CheckInOut();
        $arrData['list'] = $model->getListByUserId($checkinoutUser['id'],$start_day,$end_day,$start,self::PAGENUMBER);
        $arrData['listCnt'] = $model->getListCntByUserId($checkinoutUser['id'],$start_day,$end_day);
        $arrData['week'] = array('日','一','二','三','四','五','六');
        $arrData['absenteeismType'] = OAConfig::$absenteeismType;
        $arrData['absenteeismTypeAll'] = OAConfig::absenteeismTypeAll();

        $this->renderFile('./myteam/list.tpl',$arrData);

    }


}
