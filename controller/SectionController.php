<?php
class SectionController extends Controller
{
    public function actions()
    {
    }

    public function actionAdd()
    {
        if(Yii::app()->request->getIsPostRequest())
        {
            if(!empty($_POST['section_name']) /*&& !empty($_POST['parent_id']) */&& !empty($_POST['manager']))
            {
                $time = time();
                $manager_id = 1;
                if(empty($_POST['parent_id']))
                    $_POST['parent_id'] = 0;
                //部门领导必须存在，且level可以合理。
                //这里到底输入姓名还是邮箱或者两者都可以，需要进一步商议。TODO
                $model = new User();
                $arrData = $model->getItemByEmailOrName($_POST['manager']);
                switch( count($arrData))
                {
                    case 0: 
                        {
                            $error = "没有找到该负责人，请检查。";
                            $_POST['manager_name'] = "";
                            self::actionInValidate("sectionAdd.tpl",$error);
                            return;
                        }
                    case 1: $manager_id = $arrData[0]['id'];break;
                    default:
                    {
                        $error = "无法通过负责姓名查找到该负责人，请输入邮箱。";
                        $_POST['manager_name'] = "";
                        self::actionInValidate("sectionAdd.tpl",$error);
                        return;
                    }
                }


                $parent_id = $_POST['parent_id'];
                $model = new Section();
                $arrData = array();
                $arrData['name'] = $_POST['section_name'];
                $arrData['team_name'] = $_POST['team_name'];
                $arrData['manager_id'] = $manager_id;
                $arrData['parent_id'] = $parent_id;
                $arrData['create_time'] = $time;
                $arrData['modify_time'] = $time;
                $arrData['status'] = STATUS_VALID;
                //$arrData['isVirtual'] = $_POST['isVirtual'];
                $model->save($arrData);
                Yii::app()->request->redirect(Yii::app()->createURL('section/manage'));
                return;
            }
            else
            {
                $error = "输入不完整。";
                self::actionInValidate("sectionAdd.tpl",$error);
            }
        }
        else
        {
            $model = new Section();
            $tplData = array();
            $tplData['data']['sections'] = $model->getSectionIdNameList();
            Yii::app()->session['sections'] = $tplData['data']['sections'];
            $tplData['section_team'] = OAConfig::$section_team;

            $this->renderFile("sectionAdd.tpl",$tplData);
        }
    }
    
    public function actionManage()
    {
        $model = new Section();
        //$recordCount = $model->getCount();
        $perPageNum = 50;//每页展示的记录条数
        $currentPage = 1;
        $Condition = "";
        if(Yii::app()->request->getIsPostRequest()){
            $Condition = $_POST['Condition'];
           // Yii::app()->session['Condition'] = $Condition;
        }
        else if(!empty($_GET['pn']))
        {
           //$perPageNum = $_GET['perPageNum'];
           $currentPage = $_GET['pn'];
           //$Condition = Yii::app()->session['Condition'];
           $Condition = $_GET['Condition'];
        }
        else
        {
            //Yii::app()->session['Condition'] = "";
        }
        $recordCount = $model->getCountWithCondition($Condition);
        $arrData = $model->getItemByPageAndPerPageNum($currentPage,$perPageNum,$Condition);
        if(!is_array($arrData))
        {
            $arrData = array();
        }
        $tplData = array();
        $tplData['Condition'] = $Condition;
        $tplData['data']['info'] = $arrData;
        $tplData['data']['page']['pn'] = intval($currentPage);
        $tplData['data']['page']['recordCount'] = $recordCount;
        $tplData['data']['page']['pagesize'] = $perPageNum;
        if(!is_null(Yii::app()->session['error']))
        {
            $tplData['data']['message']['error'] = Yii::app()->session['error'];
            unset(Yii::app()->session['error']);
        }
        // *** 用来控制前端权限显示
		$oa_user_id = intval(Yii::app()->session['oa_user_id']);
		$right = new Right();
		$right_list = $right->getPrivilegeByUserID($oa_user_id);

		$tplData['add_p'] = 0;
		$tplData['edit_p'] = 0;
		$tplData['del_p'] = 0;

		$controllerName = strtolower($this->id);
		foreach($right_list as $key=>$value)
		{
			if($value['controller'] == $controllerName)
			{
				if($value['action'] == 'add')
				{
					$tplData['add_p'] = 1;
				}
				if($value['action'] == 'edit')
				{
					$tplData['edit_p'] = 1;
				}
				if($value['action'] == 'delete')
				{
					$tplData['del_p'] = 1;
				}
			}
		}

        $this->renderFile("sectionManage.tpl",$tplData);
    }

    public function actionSectionUser(){
        $sectionId = $_GET['id'];
        $userModel =new User();
        $sectionModel = new Section();
        $arrData['section'] = $sectionModel->getItemByPk($sectionId);
        $arrData['users'] = $userModel->getSectionUser($sectionId);
        $this->renderFile("sectionUsers.tpl",$arrData);
    }

    public function actionInValidate($view,$error=null)
    {
        $tplData['data']['info'] = $_POST;
        $tplData['data']['sections'] = Yii::app()->session['sections'];
        if(!is_null($error))
        {
            $tplData['data']['message']['error'] = $error;
        }

        $tplData['section_team'] = OAConfig::$section_team;
        $this->renderFile("$view",$tplData);
    }

    public function actionEditPost()
    {
       if(Yii::app()->request->getIsPostRequest())
       {
            $can_building = Yii::app()->request->getParam('can_building');
            $id = $_POST['id'];
            //判断输入项没有空
            if( empty($_POST['section_name']) || empty($_POST['manager']) )
            {
                $error = "输入不完整，请检查。";
                self::actionInValidate("sectionEdit.tpl",$error);
                return;
            }
            if(empty($_POST['parent_id']))
                $_POST['parent_id'] = 0;
            $model = new User();
            $arrData = $model->getItemByEmailOrName($_POST['manager']);
            if(count($arrData)==0)
            {
                $error = "该负责人不存在，请检查。";
                self::actionInValidate("sectionEdit.tpl",$error);
                return;
            }
            //暂时不考虑重名和邮箱问题：TODO
            $manager_id = $arrData[0]['id'];

            $model = new Section();
            $arrData = $model->getIdByName($_POST['parent_name']);
            $parent_id = $addData[0]['id'];

            if($can_building  == 1) {


                //高级团建
                $senior_building = $_POST['senior_building'];

                if($senior_building == 1) {


                    $teamService = new TeamBuildingService();
                    $isSenior = $teamService->isParentsTeambuildingSenior($id);
                    if($isSenior) {

                        $error = "您的上级部门已经开启高级团建权限，你不能开启该功能!";
                        self::actionInValidate("sectionEdit.tpl",$error);
                        return;

                    }

                    $isSenior = $teamService->isSubTeambuildingSenior($id);
                    if($isSenior) {

                        $error = "您的下级部门已经开启高级团建权限，你不能开启该功能!";
                        self::actionInValidate("sectionEdit.tpl",$error);
                        return;

                    }


                }



            }else {

                $senior_building = 0;


            }

            //是否激励
            $team_encourage = $_POST['team_encourage'];


            $tplData = array();
            $tplData['data']['id'] = $id;
            $tplData['data']['name'] = $_POST['section_name'];
            $tplData['data']['team_name'] = $_POST['team_name'];
            $tplData['data']['manager_id']= $manager_id;
            $tplData['data']['parent_id'] = $_POST['parent_id'];
            $tplData['data']['can_building'] = $can_building ? $can_building : 0;
            $tplData['data']['senior_building'] = $senior_building;
            $tplData['data']['team_encourage']  = $team_encourage ? $team_encourage : 0;
            $tplData['data']['vp_id'] = 1;
            $tplData['data']['create_time'] = $_POST['create_time'];
            $tplData['data']['modify_time'] = time();
            $model->save($tplData['data']);

       }
       Yii::app()->request->redirect(Yii::app()->createURL('section/manage'));
    }
    public function actionEdit()
    {
        $model = new Section();
        $sectionData = $model->getSectionInfoById($_GET['id']);
        
        $tplData = array();
        $tplData['data']['info'] = $sectionData[0];
        Yii::app()->session['section_name'] = $sectionData[0]['section_name'];
        //获取所有部门信息
        $arrData = $model->getCertainSectionByName($sectionData[0]['section_name']);
        $tplData['data']['sections'] = array();
        if(is_array($arrData))
        {   
            $arr = array();
            foreach($arrData as $current)
            {
                    
                $arr["".$current['id']]['id'] = $current['id'];
                $arr["".$current['id']]['name'] = $current['name'];
                $arr["".$current['id']]['parent_id'] = $current['parent_id'];
            }
            $num = 0;
            $targetSectionID = $_GET['id'];
            foreach($arr as $current)
            {
                $currentID = $current['id'];
                if($current == $targetSectionID)
                    continue;
                $flag = true;
                $tmp = $current;
                while($tmp['parent_id'] != 0)
                {
                    
                    $tmp = $arr["".$tmp['parent_id']];
                    if(is_null($tmp))
                    {
                        $flag = false;
                        break;
                    }
                    
                }
                if($flag)
                    $tplData['data']['sections'][$num++] = $current;
            }
        }

        $tplData['section_team'] = OAConfig::$section_team;
        $tplData['section_team_target'] = $sectionData[0]['team_name'];

        if($tplData['data']['info']['parent_id'] == 0){
            $tplData['data']['info']['parent_name'] = "无";
        }

        array_push($tplData['data']['sections'],array('id'=>'0','name'=>'无')); 
        Yii::app()->session["sections"] = $tplData['data']['sections'];
        $this->renderFile("sectionEdit.tpl",$tplData);      
    }

    public function actionDelete()
    {        
        if(strpos($_GET['id'],".html"))
        {
            $split = explode(".",$_GET['id']);
            $section_id = $split[0];
        }
        else
            $section_id = $_GET['id'];
        
        //判断该部门下面是否有员工，若有，则不允许删除。
        /*$UserModel = new User();
        $arrParams['condition']['section_id'] = $section_id;
        $UserModel->setParams($arrParams);
        $num = $UserModel->getCount();*/
        $model = new Section();
        $result = $model->isSectionEmpty($section_id);
        if( !$model->isSectionEmpty($section_id) )
        {
            Yii::app()->session['error'] = "该部门下还有员工，不允许删除。";
            Yii::app()->request->redirect(Yii::app()->createURL('section/manage'));
            return;
        }

        //判断该部门下面是否有团建经费, 激励经费，若有，则不允许删除。
        $teamBuildingModel = new TeamBuildingRecord();
        $floatTeamBuilding = $teamBuildingModel->getTotalAmountBySectionId($section_id);
        $floatTeamBuilding = floatval($floatTeamBuilding);

        $teamEncourageModel = new TeamEncourageRecord();
        $floatTeamEncourage = $teamEncourageModel->getTotalAmountBySectionId($section_id);
        $floatTeamEncourage = floatval($floatTeamEncourage);

        if($floatTeamBuilding || $floatTeamEncourage){
            Yii::app()->session['error'] = "该部门下还有团建经费或激励经费，不允许删除。";
            Yii::app()->request->redirect(Yii::app()->createURL('section/manage'));
            return;
        }

       // $model = new Section();
        $tobeDeleteId = $section_id;
        $strDeleteId = "".$section_id;
        $queue = array();
        array_push($queue,$section_id);
        while(count($queue) != 0)
        {
            $tobeDeleteId = array_shift($queue);
            if( $tobeDeleteId != $section_id )
            {
                $strDeleteId = $strDeleteId.",".$tobeDeleteId ;
            }
            $arrData = $model->getIdByParentId($tobeDeleteId);
            if(!is_array($arrData))
                continue;
            foreach($arrData as $current)
            {
                array_push($queue,$current['id']);
            }
        }
        $model->DeleteSectionsByIdList($strDeleteId);
        Yii::app()->request->redirect(Yii::app()->createURL('section/manage'));
    }

    //批量导入
    public function actionBatchImport()
    {
        $extension = end(explode(".", $_FILES["file"]["name"]));
        if ($_FILES["file"]["error"] > 0)
        {
            echo "Error: " . $_FILES["file"]["error"] . "<br />";
        }
        else if ( $extension == "xls")
        {
            echo "Upload: " . $_FILES["file"]["name"] . "<br />";
            echo "Type: " . $_FILES["file"]["type"] . "<br />";
            echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
            echo "Stored in: " . $_FILES["file"]["tmp_name"];
            move_uploaded_file($_FILES["file"]["tmp_name"],
                                "../upload/".$_FILES["file"]["tmp_name"]);
            echo "Stored in: " . "upload/temp";
            $model = new Section();
            $model->BatchImport($_FILES["file"]["tmp_name"]);
        }  
    }
}
