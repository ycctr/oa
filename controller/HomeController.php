<?php
define("UNDINNER_TIME_HOUR", 14);

/**
 * 我的主页
 * 
 **/
class HomeController extends Controller {


    public function actions() {

    }

    public function actionIndex() {

        $host = Yii::app()->request->getUserHostAddress();
        $homeService = new HomeService();
        $officeConf  = $homeService->officeConf;
        $index = $homeService->getByIPAddress($host);
        if(!isset($officeConf[$index]) ) {

            $index = 1;

        }
        $user_id = intval(Yii::app()->session['oa_user_id']);

        if(empty($user_id) ) {

            Yii::app()->request->redirect(Yii::app()->createURL('site/login'));

        }

        $self = $homeService->dinnerModel->getUserByUserId($user_id);

        $display = "";
        $total = 0;
        if(!empty($self) ) {

            $display = "display: none;";
            $total = 1;

        }

        $hour   = date("H");
        $undinner = "0";
        if($hour >= UNDINNER_TIME_HOUR) {

            $undinner = "1";

        }

        $users = $homeService->dinnerModel->getUserByDate();

        if( !empty($users) ) {
            $total = $total + count($users);
        }

        $tplData = array();
        $tplData['office']  = $officeConf;
        $tplData['self']    = $self;
        $tplData['index']   = $index;
        $tplData['users']   = $users;
        $tplData['total']   = $total;
        $tplData['display'] = $display;
        $tplData['undinner'] = $undinner;
        $tplData['dinnerHour'] = UNDINNER_TIME_HOUR - 12;

        $this->renderFile("./home/dinner.tpl",$tplData);
        
    }

    /**
     * 订餐脚本 
     *
     **/
    public function actionAddDinner(){

        $office = Yii::app()->request->getParam('office', 1);
        $oa_user_id = intval(Yii::app()->session['oa_user_id']);

        if(empty($oa_user_id)){

            $ret['code'] = -3;
            echo json_encode($ret);
            die();

        }


        $time   = time();
        $date   = date("Y-m-d", $time);

        $hour   = date("H");
        if($hour >= UNDINNER_TIME_HOUR) {

            $ret['code'] = -2;
            echo json_encode($ret);
            die();

        }

        $homeService = new HomeService();
        $data = $homeService->addDinner($office);
        $ret['code'] = 1;

        if(empty($data) ) {

            $ret['code'] = -1;

        } else {

            $ret['data'] = $data;

        }

        echo json_encode($ret);
        die();

    }

    /**
     * 快递订单
     **/
    public function actionSelfEMS() {

        $dataTpl = array();
        $pageNo = Yii::app()->request->getParam('pageNo', 1);

        $user_id = Yii::app()->session['oa_user_id']; 

        if(empty($user_id) ) {

            Yii::app()->request->redirect(Yii::app()->createURL('site/login'));

        }

        $userModel = new User();
        $userInfo = $userModel->getUserInfoById($user_id);
        $userInfo = $userInfo[0];
        $dataTpl['userInfo'] = $userInfo;
        
        if(!empty($data) ) {

            $dataTpl['self'] = $data;

        }

        $start = 0; 
        $per   = 10; //每页10条

        if($pageNo > 1) {

            $start = ($pageNo-1) * $per; 

        }

        $expressModel = new Express();
        $userRes = $expressModel->getSelfExpress($user_id, $start, $per);
        $userExpress = $userRes['data'];
        $homeService = new HomeService();
        $officeConf  = $homeService->officeEmsConf;
        $goodsConf   = $homeService->goodsConf;
        $expressConf = $homeService->expressConf;

        //根据ip获取办公区
        $host = Yii::app()->request->getUserHostAddress();
        $index = $homeService->getByIPAddress($host);

        $dataTpl['index']       = $index;
        $dataTpl['cur_date']    = date("Y-m-d");
        $dataTpl['userExpress'] = $userExpress;
        $dataTpl['officeConf']  = $officeConf;
        $dataTpl['goodsConf']   = $goodsConf;
        $dataTpl['expressConf'] = $expressConf;
        $dataTpl['expressAllConf'] = $homeService->expressAllConf;
        $dataTpl['total'] = $userRes['total'];
        $dataTpl['per'] = $per;
        $dataTpl['pageNo'] = $pageNo;

        $this->renderFile("./home/selfEMS.tpl",$dataTpl);

    }

    //ajax add
    public function actionAddExpress() {

        $data['code'] = "-1";
        $express  = Yii::app()->request->getParam('express');

        if(empty($express) ) {

            $data['msg'] = "请求参数失败";
            echo json_encode($data, true);
            die();

        }

        $user_id = Yii::app()->session['oa_user_id']; 


        if(empty($user_id) ) {

            $data['msg'] = "请重新登录OA系统";
            echo json_encode($data, true);
            die();

        }

        $msg_conf = array(

            'office'       => "请选择办公区",
            'express_type' => "请选择快递名称不能为空",
            'express_num'  => "请快递单号不能为空",
            'goods_type'   => "请选择物品类型不能为空",
            'to_where'     => "收件人或公司不能为空",
            'to_city'      => "请填写寄往城市信息",

        );

        foreach($express as $key=>$value) {

            if(empty($value) )  {

                $data['msg'] = $msg_conf[$key];
                echo json_encode($data, true);
                die();

            }  

        }
        $expressModel = new Express();
        $is_repeat = $expressModel->isByNumExperss($express['express_num']);

        if($is_repeat) {

            $data['msg'] = "重复添加快递订号:{$express['express_num']}" ;
            echo json_encode($data, true);
            die();

        }
        $addData = $express;
        $addData['user_id'] = $user_id;
        $time = time();
        $addData['create_time'] = $time;
        $addData['date'] = date("Y-m-d", $time);
        $isAdd = $expressModel->save($addData);

        if(!$isAdd) {

            $data['msg'] = "数据录入失败，请刷新页面!";
            echo json_encode($data, true);
            die();

        }

        $data['msg']  = "保存成功";
        $data['code'] = "1";
        echo json_encode($data, true);
        die();


    }

    public function actionAjaxEMS() { 
        $user_id  = Yii::app()->request->getParam('user_id');
        $pageNo = Yii::app()->request->getParam('pageNo', 1);

        $start = 0; 
        $per   = 10; //每页10条 

        if($pageNo > 1) {

            $start = ($pageNo-1)* $per; 

        }

        $data['code'] = -1;

        if(empty($user_id) ) {

           $data['msg'] = "没有获取到用户信息";
           echo json_encode($data, true);
           die();

        }

        $userModel = new User();
        $userInfo = $userModel->getUserInfoById($user_id);
        $userInfo = $userInfo[0];

        $expressModel = new Express();
        $userRes = $expressModel->getSelfExpress($user_id, $start, $per);
        $userExpress = $userRes['data'];
        $total = $userRes['total'];

        $homeService = new HomeService();
        $officeConf  = $homeService->officeEmsConf;
        $goodsConf   = $homeService->goodsConf;
        $expressConf = $homeService->expressConf;

        $dataTpl['officeConf']  = $officeConf;
        $dataTpl['goodsConf']   = $goodsConf;
        $dataTpl['expressConf'] = $expressConf;
        $dataTpl['expressAllConf'] = $homeService->expressAllConf;
        $dataTpl['userExpress'] = $userExpress;
        $dataTpl['userInfo']    = $userInfo;
        $dataTpl['total']    = $total;
        $dataTpl['per']    = $per;
        $dataTpl['pageNo'] = 1;

        $this->renderFile("./home/widget/selfEMS_table.tpl",$dataTpl);

    }

    /**
     * 管理员快递 
     **/
    public function actionEMSmanager() {

        $dataTpl = array();
        $pageNo = Yii::app()->request->getParam('pageNo', 1);
        $msg = Yii::app()->request->getParam('msg');
        $start_date = Yii::app()->request->getParam('start_date');
        $end_date   = Yii::app()->request->getParam('end_date');
        $condition = Yii::app()->request->getParam('condition');
        $office = Yii::app()->request->getParam('office');


        $start = 0; 
        $per   = 10; //每页10条

        if($pageNo > 1) {
            $start = ($pageNo-1) * $per; 
        }
        $expressModel = new Express();
        if(empty($start_date) || empty($end_date)) {
            $start_date = $end_date = date("Y-m-d");
        }

        $whereSql = "";
        if(!empty($condition) ) {
            $condition = trim($condition);
            $whereSql = " (u.name = '{$condition}' OR e.express_num = '{$condition}'  )  AND ";
        }

        if(!empty($office) ) {
            $whereSql .= " e.office = {$office}  AND";
        }
        if(!empty($start_date)){
            $whereSql .= " e.date >= '{$start_date}' AND";
        }
        if(!empty($end_date)){
            $whereSql .= " e.date <= '{$end_date}' ";
        }

        $userRes = $expressModel->getAllExpress($whereSql, 0, 0);
        $total = $userRes['total'];
        $userExpress = $userRes['data'];
        $homeService = new HomeService();
        $officeConf  = $homeService->officeEmsConf;
        $goodsConf   = $homeService->goodsConf;
        $expressConf = $homeService->expressConf;


        $dataTpl['office']  = $office;
        $dataTpl['userExpress'] = $userExpress;
        $dataTpl['officeConf']  = $officeConf;
        $dataTpl['goodsConf']   = $goodsConf;
        $dataTpl['expressConf'] = $expressConf;
        $dataTpl['expressAllConf'] = $homeService->expressAllConf;
        $dataTpl['start_date'] = $start_date;
        $dataTpl['end_date']   = $end_date;
        $dataTpl['total'] = $total;
        $dataTpl['per'] = $per;
        $dataTpl['pageNo'] = $pageNo;
        $dataTpl['condition'] = $condition;

        if(!empty($msg)) {

            $dataTpl['msg'] = $msg;

        }

        $this->renderFile("./home/adminEMS.tpl",$dataTpl);

    }

    public function actionEMSDel() {

        $id = Yii::app()->request->getParam('id');
        if(empty($id)) {
        
            Yii::app()->request->redirect(Yii::app()->createURL('home/emsmanager', array('msg'=>"删除失败,没有获取到唯一id！")));

        }
        $emsModel = new Express();
        $emsModel->deleteUserById($id);
        Yii::app()->request->redirect(Yii::app()->createURL('home/emsmanager'));

    }

    public function actionEMSEdit() {

        $id = Yii::app()->request->getParam('id');
        $is_post = Yii::app()->request->getParam('is_post');

        if(empty($id) ) {

            Yii::app()->request->redirect(Yii::app()->createURL('home/emsmanager', array('msg'=>"操作失败，没有获取到唯一ID编号!")));

        }

        $emsModel = new Express();
        $expressInfo = $emsModel->getItemByPk($id);


        //is_post
        if(!empty($is_post) ) {


            $express  = Yii::app()->request->getParam('express');

            if(empty($express) ) {

                Yii::app()->request->redirect(Yii::app()->createURL('home/EMSEdit', array('msg'=>"保存失败，传递参数失败")));

            }


            $msg_conf = array(

                'user_id' => "操作人不能为空",
                'express_type' => "请选择快递名称不能为空",
                'express_num' => "请快递单号不能为空",
                'goods_type' => "请选择物品类型不能为空",
                'to_where' => "收件人或公司不能为空",
                'to_city' => "请填写寄往城市信息",

            );

            foreach($express as $key=>$value) {

                if(empty($value) )  {

                    $tmp_msg = $msg_conf[$key];
                    Yii::app()->request->redirect(Yii::app()->createURL('home/EMSEdit', array('msg'=>"保存失败，{$tmp_msg}。")));

                }  

            }
            $expressModel = new Express();
            //编辑页面
            $expressInfo = $express;

            $expressInfo['modify_time'] = time();
            $is_update = $emsModel->save($expressInfo);

            if(empty($is_update) ) {

                $tmp_msg = "数据库出错!";
                Yii::app()->request->redirect(Yii::app()->createURL('home/EMSEdit', array('msg'=>"保存失败，{$tmp_msg}。")));

            }else {

                Yii::app()->request->redirect(Yii::app()->createURL('home/emsmanager'));

            }

        }

        $homeService = new HomeService();
        $goodsConf   = $homeService->goodsConf;
        $expressConf = $homeService->expressConf;
        $officeConf  = $homeService->officeEmsConf;

        
        $tplData = array();
        $tplData['expressInfo'] = $expressInfo; 
        $tplData['officeConf']  = $officeConf;
        $tplData['goodsConf']   = $goodsConf;
        $tplData['expressConf'] = $expressConf;
        $this->renderFile("./home/emsEdit.tpl",$tplData);

    }

}
