<?php
class LunchchangeController extends Controller {

    public function actionList() {
        $arrQuery = Yii::app()->request->getQuery('query');
        if(!$arrQuery){
            $arrQuery = self::GetInitTimeSpanValue();
        }
        $intPageSize = 10;
        $intPage = intval(Yii::app()->request->getQuery('page')) < 1 ? 1 : intval(Yii::app()->request->getQuery('page'));
        $lunchChangeModel = new LunchChange();
        $arrQuery['page'] = $intPage;
        $arrQuery['pageSize'] = $intPageSize;
        $arrTplData['lunchLocation'] = OAConfig::$lunchLocation;
        $arrTplData['lunchChangeType'] = OAConfig::$lunchChangeType;
        $arrTplData['arrList'] = $lunchChangeModel->getListWithCondition($arrQuery);
        unset($arrQuery['page']);
        unset($arrQuery['pageSize']);
        $strQuery = '';
        if(is_array($arrQuery)){
            foreach ($arrQuery as $key => $value){
                $strQuery .= 'query[' . $key . ']=' . $value . '&';
            }
            $strQuery = substr($strQuery, 0, strlen($strQuery) - 1);
        }
        $arrTplData['arrPager'] = array(
            'count'     => $lunchChangeModel->getCntWithCondition($arrQuery),
            'pagesize'  => $intPageSize,
            'page'      => $intPage,
            'pagelink'  => empty($arrQuery) ? 'list?page=%d' : 'list?' . $strQuery . '&page=%d',
        );
        $arrTplData['savelink'] = "SaveAsCsv?" . $strQuery;
        if(empty(Yii::app()->request->getQuery('query')))
        {
            $time = self::GetInitTimeSpanValue();
            $query['start_time'] = $time['start_time'];
            $query['end_time'] = $time['end_time'];
            $arrTplData['initTime'] = $query;
        }
        $this->renderFile("lunch_change.tpl",$arrTplData);
    } 


    public function GetInitTimeSpanValue()
    {
        $currentDate = date('Y-m-d');
        $arrTmp = explode('-', $currentDate);
        $arrTmp[2] = '01';
        $firstDayOfMonth = implode('-', $arrTmp);
        return array(
            'start_time' => $firstDayOfMonth,
            'end_time' => $currentDate,
        );
    }

    /**
     * 保存csv
     **/
    public function actionsaveAsCsv() {
        $arrQuery = Yii::app()->request->getQuery('query');
        $arrQuery['csv'] = 1;
        $lunchChangeModel = new LunchChange();
        $arrData = $lunchChangeModel->getListWithCondition($arrQuery);

        header('Content-Type: application/csv');
        if(empty($arrQuery))
        {
            $time = self::GetInitTimeSpanValue();  
            $filename = "午餐变更(".$time['start_time']."至".$time['end_time'].")";
        }
        else
        {
            $filename = "午餐变更(".$arrQuery['start_time']."至".$arrQuery['end_time'].")";
        }
        if(isset($arrQuery['name']) && $arrQuery['name']){
            $filename .= sprintf('(%s)', $arrQuery['name']);
        }
        if(isset($arrQuery['section_name']) && $arrQuery['section_name']){
            $filename .= sprintf('(%s)', $arrQuery['section_name']);
        }
        $filename .= '.csv';

        header("Content-Disposition: business; filename=".$filename."");
        
        $csvTitle = array(
                        '姓名',
                        '工号',
                        '具体部门',
                        '部门',
                        '业务线',
                        '办公区',
                        '变更类型',
                        '申请日期',
                        '审批日期',
        );
        $output = fopen('php://output','w') or die("Can't open php://output");

        foreach ($csvTitle as $key => $value){
            $csvTitle[$key] = iconv("UTF-8","gbk",$value);
        }

        fputcsv($output,$csvTitle);


        foreach ($arrData as  $val){

            $index = 0;
            $data = array();
            $data[0] = (string)iconv("UTF-8","gbk",$val['user_name']);
            $data[1] = (string)iconv("UTF-8","gbk",$val['user_id_card']);
            $data[2] = (string)iconv("UTF-8","gbk",$val['jutibumen']);
            $data[3] = (string)iconv("UTF-8","gbk",$val['bumen']);
            $data[4] = (string)iconv("UTF-8","gbk",$val['yewuxian']);
            $data[5] = (string)iconv("UTF-8","gbk",OAConfig::$lunchLocation[$val['lunch_location']]);
            $data[6] = (string)iconv("UTF-8","gbk",OAConfig::$lunchChangeType[$val['lunch_change_type']]);
            $data[7] = (string)iconv("UTF-8","gbk",date('Y-m-d', $val['create_time']));
            $data[8] = (string)iconv("UTF-8","gbk",date('Y-m-d', $val['uwf_modify_time']));

		    fputcsv($output,$data);	

		}

        fclose($output) or die("Can't close php://output");            

    } 

    public function translateTime($time)
    {
        $val = strtotime($time);
        if($val)
            return $val;
        $arr = explode('/',$time);
        $tmp = $arr[2]."-".$arr[1]."-".$arr[0];
        $val = strtotime($tmp);
        return $val;
    }


    function printExcel($excelInfo) {

        $data = $excelInfo['res'];
        $file = $excelInfo['file'];
        $excelTitle =  $excelInfo['excelTitle'];
        $cols = count($excelTitle);
        $title = $excelInfo['title'];

        $arrLetter = array();

        for ($i = 65; $i < 65+$cols; $i++){

            $arrLetter[] = chr($i); 

        }

        set_include_path(get_include_path() . PATH_SEPARATOR . Config :: $basePath . "/trunk/rong360/shared/excel/");
        include_once 'PHPExcel.php';
        include_once 'PHPExcel/Writer/Excel5.php';

        $objPHPExcel = new PHPExcel(); 

        $objPHPExcel->getProperties()->setCreator("融360");
        $objPHPExcel->getProperties()->setLastModifiedBy("融360");
        $objPHPExcel->getProperties()->setTitle($title);
        $objPHPExcel->getProperties()->setSubject($title);
        $objPHPExcel->getProperties()->setDescription($title);

        $objPHPExcel->setActiveSheetIndex(0);

        foreach($excelTitle as $k => $title){

            $objPHPExcel->getActiveSheet()->SetCellValue($arrLetter[$k].'1',$title); 

        }

        foreach($data as $n => $val){
            $index = 0;
            foreach($val as $item){
                $key = $arrLetter[$index].($n+2);
                if($arrLetter[$index] == 'A'){
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($key,$item,PHPExcel_Cell_DataType::TYPE_STRING); 
                }else{
                    $objPHPExcel->getActiveSheet()->SetCellValue($key,$item); 
                }
                $index++;
            }
        }

        $company = $file;
        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        header("Content-Type: application/force-download"); 
        header("Content-Type: application/octet-stream"); 
        header("Content-Type: application/download"); 
        header('Content-Disposition:inline;filename="'.$file.'"'); 
        header("Content-Transfer-Encoding: binary"); 
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
        header("Pragma: no-cache"); 
        $objWriter->save("php://output");

    }


}
