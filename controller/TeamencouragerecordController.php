<?php
class TeamencouragerecordController extends Controller
{
    static $pagenumber = 50;

    public function actionList()
    {
        $pn = Yii::app()->request->getParam('pn',1);
        $sectionId = Yii::app()->request->getParam('section_id');
        $userId = Yii::app()->session['oa_user_id'];

        $sectionModel = new Section();
        $is_role   = $this->isTeamEncourageAuditRole($userId);

        //默认关闭
        $isPermission = false;

        $teamService = new TeamEncourageService();
        $model = new Section();

        if(!$is_role) {
            $encourageSection = $teamService->getSectionEncourage($userId);
            if(!empty($encourageSection) && $encourageSection[$sectionId]['team_encourage'] == 1) {
                $isPermission = true;
            }
            $teamArr = $teamService -> getTeamEncourage($userId); 
        } else {
            $isPermission = true;
            $teamArr = $model -> getAllSectionHasEncourage(); 
        }
        $sections = $model->getItemsByEncourage($userId);
        
        if(empty($sectionId)){//没有传递部门id,默认选取第一个
            $section = reset($sections);
            $sectionId = $section['id']; 
            $isPermission = true;
        }else{
            $section = $model->getItemByPk($sectionId); 
        }

        if(!$isPermission){
            throw new CHttpException(404,'您没有权限查看权限');
        }


        $start = ($pn-1)*self::$pagenumber;
        $buildRecordModel = new TeamEncourageRecord();
        $list = $buildRecordModel->getItemsBySectionId($sectionId,$start,self::$pagenumber);
        $total = $buildRecordModel->getItemTotalBySectionId($sectionId);

        $arrData['pn'] = $pn;
        $arrData['rn'] = self::$pagenumber;
        $arrData['url'] = '/encouragerecord/list/?section_id='.$sectionId;
        $arrData['total'] = $total;
        $arrData['list'] = $list;

        $arrData['sections'] = $teamArr;
        $arrData['section'] = $section;
        $this->renderFile('./encouragerecord/list.tpl',$arrData);
    } 

    public function actionAllAmount()
    {
        $teamService = new TeamEncourageService(); 

        $userId = Yii::app()->session['oa_user_id'];
        $teamArr = $teamService -> getTeamEncourage($userId); 
        $sectionIdArr = array_keys($teamArr);

        $model = new TeamEncourageRecord(); 
        $list = $model->getTeamEncourageBySection($sectionIdArr);

        $arrData['list'] = $list;
        $sectionModel = new Section();
        $sectionUserNum = $sectionModel->getAllEncourageSectionUserNumber();

        $arrData['sectionUserNum'] = $sectionUserNum;
        $arrData['export_flag'] = 1;

        $this->renderFile('./encouragerecord/overview.tpl',$arrData);
    }


    //获取部门团建金额
    public function actionGetAmount()
    {
        $userId = Yii::app()->session['oa_user_id'];
        $sectionId = Yii::app()->request->getParam('section_id');

        if(!$this->hasPermission($userId,$sectionId)){
            throw new CHttpException(404,'您没有权限查询团建经费，请找部门经理查询');
        }
        $model = new TeamEncourageRecord();

        $amount = $model->getTotalAmountBySectionId($sectionId);

        echo json_encode($amount);
        die;
    }

    //是否审批人员
    //依赖角色名称，容易造成Bug 目前没有想到特别好的办法，如果用角色赋予权限，维护代价比较大，而且容易忘记更新
    public function isTeamEncourageAuditRole($userId)
    {
        $model = new UserRole(); 
        $roleNames = $model->getRoleNameByUserId($userId);
        if(in_array('团建审核专员',$roleNames)){
            return true;
        }
        return false;
    }

    //权限验证
    public function hasPermission($userId,$sectionId)
    {
        $model = new Section();
        $sections = $model->getItemsByEncourage($userId,$sectionId);

        if(!count($sections) && !$this->isTeamEncourageAuditRole($userId)){
            return false;
        }
        return true;
    }

}
?>
