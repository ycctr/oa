<?php
class AdminController extends Controller
{
    private $_model;

    public function __construct()
    {
        parent :: __construct('admin', null);
        $this->_model = new UserRole();
    }
    public function actionIndex()
    {

    }
    public function getRoleIdByQuery($role_name)
    {
        $RoleModel = new Role();
        $strSql = sprintf("select id from role where name='%s'",$role_name);
        Yii::log($strSql);
        $ret = $RoleModel->db()->query($strSql);
        if(empty($ret))
            return false;
        return $ret[0]['id'];
    }
    public function getUserIdByQuery($query)
    {
        $UserModel = new User();
        $strSql = sprintf("select id from user where email='%s'or name='%s'",$query,$query);
        $ret = $UserModel->db()->query($strSql);
        if(empty($ret))
            return false;
        $id = array();
        foreach ($ret as $current)
            array_push($id,$current['id']);
        return $id;
        /*if(count($id)>1)
            $strId = explode(",",$id);
        else
            $strId = $ret[0]['id_card'];
        return "(".$strId.")";*/
    }
    public function actionList()
    {
        $arrCondition = array();
        $findEmpty = false;
        $arrQuery = Yii::app()->request->getQuery('query');
        if (!empty($arrQuery))
        {
            $user_id = self::getUserIdByQuery($arrQuery);
            $role_id = self::getRoleIdByQuery($arrQuery);
            if(!empty($role_id))
                $arrCondition['role_id'] =  $role_id;
            if(empty($role_id) && empty($user_id))
                $findEmpty = true;
        }
        $arrCondition['status'] = 1;

        $intPageSize = 5;
        $intPage = intval(Yii::app()->request->getQuery('page')) < 1 ? 1 : intval(Yii::app()->request->getQuery('page'));
        $offset = ($intPage - 1) * $intPageSize;
        $limit = $intPageSize;
        
        if($findEmpty)
            $totalCount = 0;
        else
            $totalCount = $this->_model->getCountWithCondition($arrCondition,$user_id);
        //var_dump($totalCount);
        $arrTplData['arrPager'] = array(
            'count'     => $totalCount,
            'pagesize'  => $intPageSize,
            'page'      => $intPage,
            'pagelink'  => empty($arrQuery) ? '?page=%d' : '?' . urldecode(Yii::app()->request->getQueryString()) . '&page=%d',
        );
        if(!$findEmpty && $totalCount != 0)
            $arrTplData['arrList'] = $this->_model->getInfoListWithCondition($arrCondition,$user_id,$offset,$limit);

		// *** 用来控制前端权限显示
		$oa_user_id = intval(Yii::app()->session['oa_user_id']);
		$right = new Right();
		$right_list = $right->getPrivilegeByUserID($oa_user_id);

		$arrTplData['add_p'] = 0;
		$arrTplData['view_p'] = 0;
		$arrTplData['edit_p'] = 0;
		$arrTplData['del_p'] = 0;
		$arrTplData['recover_p'] = 0;
		$controllerName = strtolower($this->id);
		foreach($right_list as $key=>$value)
		{
			if($value['controller'] == $controllerName)
			{
				if($value['action'] == 'add')
				{
					$arrTplData['add_p'] = 1;
				}
				if($value['action'] == 'view')
				{
					$arrTplData['view_p'] = 1;
				}
				if($value['action'] == 'edit')
				{
					$arrTplData['edit_p'] = 1;
				}
				if($value['action'] == 'del')
				{
					$arrTplData['del_p'] = 1;
				}
				if($value['action'] == 'recover')
				{
					$arrTplData['recover_p'] = 1;
				}
			}
		}
       // var_dump($arrTplData['del_p']);exit;
		//*****************
        $this->renderFile('admin.tpl', $arrTplData);
    
    }
    
    public function actionAdd()
    {
        if (Yii::app()->request->getIsPostRequest())
        {
            $arrUserPost = (array)Yii::app()->request->getPost('user');

            $user_name = $arrUserPost['name'];
            $user = $this->_model->db()->query("select id from user where name='".$arrUserPost['name']."'or email='".$arrUserPost['email']."'");
            $user_id = $user[0]['id'];

            $arrRolePost = (array)Yii::app()->request->getPost('role');
            $this->_model->saveUserRole($arrRolePost, $user_id);
            $logModel = new logModel();
            $logModel->UserRoleSave(intval(Yii::app()->session['oa_user_id']),$user_id,$user_name,"",$arrRolePost,ADMIN_ADD);
            Yii::app()->request->redirect(Yii::app()->createURL('admin/list'));
            return;
        }

        $role = new Role();
        $arrTplData["role_list"] = $role->getRoleListWithUserId(-1);
        $arrTplData['is_edit'] = 0;
        $arrTplData['is_add'] = 1;
        $this->renderFile('admin_edit.tpl', $arrTplData);
    }
    public function actionDel()
    {
        $id = intval(Yii::app()->request->getParam('user_id'));
        $user_name = $this->_model->getUserNameByUserId($id);
        $arrRole = $this->_model->getUserRoleByUserId($id);
       /* $arrInfo['status'] = 0;
        $arrInfo['user_id_card'] = $id;
        $this->_model->save($arrInfo);
         */
        $this->_model->DeleteAdmin($id);
        $logModel = new logModel();  
        $logModel->UserRoleSave(intval(Yii::app()->session['oa_user_id']),$id,$user_name,$arrRole,'',ADMIN_DEL);
        Yii::app()->request->redirect(Yii::app()->createURL('admin/list'));
    }
    public function actionEdit()
    {
        if (Yii::app()->request->getIsPostRequest())
        {
            $arrUserPost = (array)Yii::app()->request->getPost('user');
            $arrRolePost = (array)Yii::app()->request->getPost('role');

            $user_id = $arrUserPost['id'];
            $user_name = $arrUserPost['name'];
            $arr_old_Role = $this->_model->getRoleByUserId($user_id);
            //array_diff(a,b)这个函数：输出a中和b不同的元素，因此，顺序很重要。
            if( count(array_diff($arr_old_Role,$arrRolePost)) != 0 || count(array_diff($arrRolePost,$arr_old_Role)))
            {

                $this->_model->saveUserRole($arrRolePost, $user_id);

                $logModel = new logModel();
                $logModel->UserRoleSave(intval(Yii::app()->session['oa_user_id']),$user_id,$user_name,$arr_old_Role,$arrRolePost,ADMIN_EDIT);
            
            }

           Yii::app()->request->redirect(Yii::app()->createURL('admin/list'));
           return;
        }
        $arrTplData = self::getTplData(intval(Yii::app()->request->getParam('user_id')));
        $arrTplData['is_edit'] = 1;
        $this->renderFile('admin_edit.tpl', $arrTplData); 
    }

    public function getTplData($user_id = -1)
    {
        /*$item = $this->_model->db()->query("select * from user_role where id_card=".$id);
        $user_id = $item[0]['user_id'];
        $role_id = $item[0]['role_id'];*/

        $user = $this->_model->db()->query("select id,name,email from user where id=".$user_id);
        if(empty($user))
            return false;
        $arrTplData['user'] = $user[0];
        $role = new Role();
        $arrTplData["role_list"] = $role->getRoleListWithUserId($user_id);
        return $arrTplData;
    }
    public function actionView()
    {
        $arrTplData = self::getTplData(intval(Yii::app()->request->getParam('user_id')));
        $arrTplData['is_view'] = 1;
        //var_dump($arrTplData);
        $this->renderFile('admin_edit.tpl', $arrTplData);
    }
    public function actionRecover()
    {
        
    }

    public function actionAdminValidate()
    {
        $name = Yii::app()->request->getParam("name");
        $email = Yii::app()->request->getParam("email");
//var_dump($name);var_dump($email);
        $userModel = new User();
        $userModel->setParams($arrParams);
        $num = $userModel->getCountWithAdminInput($name,$email);
        //试图把字符串消息传回前端，前端暂时代码还没整理，先直接返回true false。TODO
        switch($num)
        {
            case 0:
                $data['msg'] = "找不到该员工，请检查输入。";$data['result'] = false;break;
            case 1:
            {   
                $user = $userModel->getItemByEmail($email); 
                if(!$user)
                    $user = $userModel->getItemByName($name);
                $arrParams = array();
                $arrParams['condition']['user_id_card'] = $user['id_card'];
                $arrParams['condition']['status'] = STATUS_VALID;
                $this->_model->setParams($arrParams);
                $num = $this->_model->getCount();
                if($num == 0){
                    $data['msg'] = "验证成功。";
                    $data['result'] = true;
                }
                else{
                    $data['msg'] = "该管理员以存在，不能重复创建。";
                    $data['result'] = false;
                }
                break;
            }
            default:
                $data['msg'] = "员工名称重复，请输入邮箱。";$data['result'] = false;
        }
        echo json_encode($data);
        Yii::app()->end();
    }
}
