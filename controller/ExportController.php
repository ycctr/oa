<?php
class ExportController extends Controller
{
    
    //财务日常报销导出
    public function actionCompleteDailyReimbursement()
    {
        if(Yii::app()->request->getIsPostRequest()){
            $startDate = Yii::app()->request->getParam('start_day');
            $endDate = Yii::app()->request->getParam('end_day');

            $startTime = strtotime($startDate);
            $endTime = strtotime($endDate) + 86400;
            $model = new DailyReimbursement(); 

            $data = $model->getCompleteList($startTime,$endTime);
            if(empty($data)){
                throw new CHttpException(404,"没有数据");
            }
            $filename = "日常报销(".date('Y/m/d', $startTime)."-".date('Y/m/d', $endTime).").csv";

            header('Content-Type: application/csv');
            header("Content-Disposition: attachment; filename=".$filename."");
            $csvTitle = array('工号','姓名','部门','加班餐费','加班交通费','日常交通费','日常招待费','total');
            $output = fopen('php://output','w') or die("Can't open php://output");

            for($intNum = 0; $intNum< 11;$intNum++){
                $csvTitle[$intNum] = iconv("UTF-8","gbk",$csvTitle[$intNum]);
            }
            fputcsv($output,$csvTitle);
 
            foreach($data as $index => $row){
                $ret = array();
                $ret[0] = $row['id_card']; 
                $ret[1] = iconv("UTF-8","gbk",$row['name']);
                $ret[2] = iconv("UTF-8","gbk",$row['section_name']);
                $ret[3] = $row['sum'][REIMBURSEMENT_TYPE_OVERTIME]['amount']?$row['sum'][REIMBURSEMENT_TYPE_OVERTIME]['amount']:0;
                $ret[4] = $row['sum'][REIMBURSEMENT_TYPE_OTHERTRAFFIC]['amount']?$row['sum'][REIMBURSEMENT_TYPE_OTHERTRAFFIC]['amount']:0;
                $ret[5] = $row['sum'][REIMBURSEMENT_TYPE_TAXI]['amount']?$row['sum'][REIMBURSEMENT_TYPE_TAXI]['amount']:0;
                $ret[6] = $row['sum'][REIMBURSEMENT_TYPE_ENTERTAINMENT]['amount']?$row['sum'][REIMBURSEMENT_TYPE_ENTERTAINMENT]['amount']:0;
                $ret[7] = $ret[3] + $ret[4] + $ret[5] + $ret[6];
                fputcsv($output,$ret);	
            }
            fclose($output) or die("Can't close php://output");            
            Yii::app()->end();
        }

        $this->renderFile('./export/daily_reimbursement.tpl');
    }    

    //出差报销导出
    public function actionBusinessReimbursement()
    {
        if(Yii::app()->request->getIsPostRequest()){
            $startDate = Yii::app()->request->getParam('start_day');
            $endDate = Yii::app()->request->getParam('end_day');

            $startTime = strtotime($startDate);
            $endTime = strtotime($endDate);
            $model = new BusinessReimbursement(); 

            $data = $model->getCompleteList($startTime,$endTime);
            if(empty($data)){
                throw new CHttpException(404,"没有数据");
            }
            $filename = "出差报销(".date('Y/m/d', $startTime)."-".date('Y/m/d', $endTime).").csv";

            header('Content-Type: application/csv');
            header("Content-Disposition: attachment; filename=".$filename."");
            $csvTitle = array('工号','姓名','部门','出差交通费','出差住宿费','出差招待费','total','卡号');
            $output = fopen('php://output','w') or die("Can't open php://output");

            for($intNum = 0; $intNum< 11;$intNum++){
                $csvTitle[$intNum] = iconv("UTF-8","gbk",$csvTitle[$intNum]);
            }
            fputcsv($output,$csvTitle);

            foreach($data as $index => $row){
                $ret = array();
                $ret[0] = $row['id_card']; 
                $ret[1] = iconv("UTF-8","gbk",$row['name']);
                $ret[2] = iconv("UTF-8","gbk",$row['sname']);
                $ret[3] = $row['sum'][BUSINESS_REIMBURSEMENT_TYPE_TRAFFIC]['amount'] + $row['sum'][BUSINESS_REIMBURSEMENT_TYPE_TAXI]['amount'];
                $ret[4] = $row['sum'][BUSINESS_REIMBURSEMENT_TYPE_RENT]['amount']?$row['sum'][BUSINESS_REIMBURSEMENT_TYPE_RENT]['amount']:0;
                $ret[5] = $row['sum'][BUSINESS_REIMBURSEMENT_TYPE_ENTERTAINMENT]['amount']?$row['sum'][BUSINESS_REIMBURSEMENT_TYPE_ENTERTAINMENT]['amount']:0;
                $ret[6] = $ret[3] + $ret[4] +$ret[5];
                $ret[7] = $row['card_no']."\t";
                fputcsv($output,$ret);  
            }
            fclose($output) or die("Can't close php://output");            
            Yii::app()->end();
        }

        $this->renderFile('./export/business_reimbursement.tpl');
    }    


    //打卡记录查询功能
    public function actionCheckinoutList(){
        $startDate = Yii::app()->request->getParam('start_date', '');
        $endDate = Yii::app()->request->getParam('end_date', '');
        $sectionName = Yii::app()->request->getParam('section_name', '');
        $userName = Yii::app()->request->getParam('user_name', '');
        $idCard   = Yii::app()->request->getParam('id_card','');
        $page = intval(Yii::app()->request->getParam('page', 1));
        $strQuery = "";
        if($startDate){
            $strQuery .= "start_date={$startDate}&";
        }
        if($endDate){
            $strQuery .= "end_date={$endDate}&";
        }
        if($sectionName){
            $strQuery .= "section_name={$sectionName}&";
        }
        if($userName){
            $strQuery .= "user_name={$userName}&";
        }
        if($idCard){
            $strQuery .= "id_card={$idCard}&";
        }

        $model = new CheckInOut();

        $ret = array();
        $week = array('日','一','二','三','四','五','六');
        $data = $model->getListBySearch($startDate, $endDate, $sectionName, $userName, $idCard, $page, $cnt=0);
        $absenteeismAllType = OAConfig::absenteeismTypeAll();
        if(is_array($data)){
            foreach($data as $row){
                $tmp = array();
                $tmp['id_card'] = (string)$row['id_card'];
                $tmp['user_name'] = $row['user_name'];
                $tmp['section_name'] = $row['section_name'];
                $tmp['date'] = $row['date'];
                $tmp['week'] = '星期'.$week[date('w',strtotime($row['date']))];
                $tmp['checkin_time'] = $row['checkin_time'];
                $tmp['checkout_time'] = $row['checkout_time'];
                $tmp['absenteeism_type'] = $absenteeismAllType[$row['absenteeism_type']];
                $ret[] = $tmp;
            }
        }
        $arrPager = array(
            'count'     => $model->getListBySearch($startDate, $endDate, $sectionName, $userName,$idCard, $page, $cnt=1),
            'pagesize'  => 20,
            'page'      => $page,
            'pagelink'  => 'checkinoutlist?' . $strQuery . 'page=%d',
        );
        $arrData = array();
        $savelink = 'checkinout?' . $strQuery;
        $arrData['savelink'] = substr($savelink, 0, strlen($savelink) - 1);
        $arrData['data'] = $ret;
        $arrData['start_date'] = $startDate;
        $arrData['end_date'] = $endDate;
        $arrData['section_name'] = $sectionName;
        $arrData['user_name'] = $userName;
        $arrData['id_card']  = $idCard;
        $arrData['arrPager'] = $arrPager;
        $this->renderFile('./export/checkinout_list.tpl', $arrData);
    }

    //打卡记录导出功能
    public function actionCheckinout()
    {
//        if(Yii::app()->request->getIsPostRequest()){
            $start_date = $_GET['start_date'];
            $end_date = $_GET['end_date'];
            $section_name = $_GET['section_name'];
            $user_name = $_GET['user_name'];
            $id_card   = $_GET['id_card'];
            $model = new CheckInOut();

            $ret = array();
            $week = array('日','一','二','三','四','五','六');
            $data = $model->getListBySearch($start_date,$end_date,$section_name, $user_name,$id_card, 1, 2);
            $absenteeismAllType = OAConfig::absenteeismTypeAll();
            if(!empty($data)){
                foreach($data as $row){
                    $tmp = array();
                    $tmp['id_card'] = (string)$row['id_card'];
                    $tmp['user_name'] = $row['user_name'];
                    $tmp['section_name'] = $row['section_name'];
                    $tmp['date'] = $row['date'];
                    $tmp['week'] = '星期'.$week[date('w',strtotime($row['date']))];
                    $tmp['checkin_time'] = $row['checkin_time'];
                    $tmp['checkout_time'] = $row['checkout_time'];
                    $tmp['absenteeism_type'] = $absenteeismAllType[$row['absenteeism_type']];
		    $ret[] = $tmp;
                }
            }

            $excelTitle = array('员工号','姓名','部门','日期','星期','上班时间','下班时间','异常状态');

            $arrLetter = array();
            for ($i = 65; $i < 74; $i++){
                $arrLetter[] = chr($i); 
            }

            set_include_path(get_include_path() . PATH_SEPARATOR . Config :: $basePath . "/trunk/rong360/shared/excel/");
            include_once 'PHPExcel.php';
            include_once 'PHPExcel/Writer/Excel5.php';

            $objPHPExcel = new PHPExcel(); 

            $objPHPExcel->getProperties()->setCreator("融360");
            $objPHPExcel->getProperties()->setLastModifiedBy("融360");
            $objPHPExcel->getProperties()->setTitle("考勤记录".$start_date."_".$end_date);
            $objPHPExcel->getProperties()->setSubject("考勤记录".$start_date."_".$end_date);
            $objPHPExcel->getProperties()->setDescription("考勤记录".$start_date."_".$end_date);

            $objPHPExcel->setActiveSheetIndex(0);

            foreach($excelTitle as $k => $title){
               $objPHPExcel->getActiveSheet()->SetCellValue($arrLetter[$k].'1',$title); 
            }

            foreach($ret as $n => $val){
                $index = 0;
                foreach($val as $item){
                    $key = $arrLetter[$index].($n+2);
                    if($arrLetter[$index] == 'A'){
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit($key,$item,PHPExcel_Cell_DataType::TYPE_STRING); 
                    }else{
                        $objPHPExcel->getActiveSheet()->SetCellValue($key,$item); 
                    }
                    $index++;
                }
            }

            $company = 'kaoqin'.date('Y-m-d',strtotime($start_date))."-".date('Y-m-d',strtotime($end_date)).".xls";
            $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
            header("Content-Type: application/force-download"); 
            header("Content-Type: application/octet-stream"); 
            header("Content-Type: application/download"); 
            header('Content-Disposition:inline;filename="'.$company.'"'); 
            header("Content-Transfer-Encoding: binary"); 
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
            header("Pragma: no-cache"); 
            $objWriter->save("php://output");
//        }
//        $this->renderFile('./export/checkinout.tpl');
    }


    public function actionVacation(){

        if(Yii::app()->request->getIsPostRequest()){

            $tag_day = Yii::app()->request->getParam('start_day');
            $tag_time = strtotime($tag_day);
            $cur_day = date("Y-m-d");
            $cur_time = strtotime($cur_day) - 86400; //昨天日期

            if($cur_time < $tag_time ) {

                Yii::app()->request->redirect(Yii::app()->createURL('export/vacation', array("msgno"=>"-1", "start_day"=>$tag_day) ));
                Yii::app()->end();

            }


            $model = new VacationRecord();
            $dataList = $model->getAllVaction($tag_time);
            if($dataList === false) {

                Yii::app()->request->redirect(Yii::app()->createURL('export/vacation', array("msgno"=>"-3", "start_day"=>$tag_day) ));
                Yii::app()->end();

            }

            $filename = "剩余年假病假导出({$tag_day}).csv";
            header("Content-Disposition: attachment; filename={$filename}");
            
            $csvTitle = array('姓名', '工号', '部门','年假', '病假',);
            $output = fopen('php://output','w') or die("Can't open php://output");
        
            for($intNum = 0; $intNum< 11;$intNum++){
                $csvTitle[$intNum] = iconv("UTF-8","gbk",$csvTitle[$intNum]);
            }
            fputcsv($output,$csvTitle);

            foreach ($dataList as $val){
                $index = 0;
                $data = array();
                $data[0] = iconv("UTF-8","gbk",$val['card']);
                $data[1] = iconv("UTF-8","gbk",$val['name']);
                $data[2] = iconv("UTF-8","gbk",$val['setion']);
                $data[3] = iconv("UTF-8","gbk",$val['nianjia']);
                $data[4] = iconv("UTF-8","gbk",$val['bingjia']);

                fputcsv($output,$data);	
            }

            fclose($output) or die("Can't close php://output");            
            Yii::app()->end();

        } 


        $date = Yii::app()->request->getParam('start_day');
        $msgno = Yii::app()->request->getParam('msgno');

        if(empty($date) ) {

            $date = date("Y-m-d", strtotime("-1 day") );
            
        }

        $data['cur_date']   = date("Y-m-d");

        $data['start_day'] = $date;
        $data['msgno']     = $msgno;

        $this->renderFile('./export/printall.tpl', $data);

    }


    public function actionTeamAll()
    {
        $team_type = Yii::app()->request->getParam("team_type", 1);
        $section_name = Yii::app()->request->getParam('section_name');
        $user_name    = Yii::app()->request->getParam('user_name');

        if($team_type == 2) {

            $model = new TeamEncourageRecord(); 

            $list = $model->getTeamBudingBySection('',$section_name,$user_name);
            $sectionModel = new Section();
            $sectionUserNum = $sectionModel->getAllEncourageSectionUserNumber();
            $team_type_name = '激励经费';


        } else {

            $model = new TeamBuildingRecord(); 

            $list = $model->getTeamBudingBySection('',$section_name,$user_name);
            $sectionModel = new Section();
            $sectionUserNum = $sectionModel->getAllBuildSectionUserNumber();
            $team_type_name = '团建经费';

        }
        $arrData['sectionUserNum'] = $sectionUserNum;


        $date = date("Y-m-d");
        $filename = "{$team_type_name}余额导出({$date}).csv";
        header("Content-Disposition: attachment; filename={$filename}");
        
        $csvTitle = array('业务线', '部门', '负责人','上级部门', '部门人数','经费余额');
        $output = fopen('php://output','w') or die("Can't open php://output");
    
        for($intNum = 0; $intNum< 6;$intNum++){
            $csvTitle[$intNum] = iconv("UTF-8","gbk",$csvTitle[$intNum]);
        }
        fputcsv($output,$csvTitle);

        foreach ($list as $val){
            $index = 0;
            $data = array();

            $data[0] = iconv("UTF-8","gbk", $val['team_name']);
            $data[1] = iconv("UTF-8","gbk", $val['section_name']);
            $data[2] = iconv("UTF-8","gbk", $val['manager_name']);
            $data[3] = iconv("UTF-8","gbk", $val['parent_name']);
            $section = $val['section_id'];
            $preson_num = count($sectionUserNum[$section]['users']);
            $data[4] = iconv("UTF-8","gbk", $preson_num);
            $data[5] = iconv("UTF-8","gbk", $val['amount']);

            fputcsv($output,$data);	
        }

        fclose($output) or die("Can't close php://output");            
        Yii::app()->end();
    }


    public function actionTeamBuilding()
    {
        $start_day = Yii::app()->request->getParam('start_day');
        $end_day = Yii::app()->request->getParam('end_day');
        $team_type = Yii::app()->request->getParam("team_type", 1);
        $section_name = Yii::app()->request->getParam('section_name');
        $saveCsv = Yii::app()->request->getParam('saveCsv');

        if(!empty($start_day) && !empty($end_day)){

            if($team_type == 2) {

                $model = new TeamEncourage(); 
                $data = $model->getListByDate($start_day,$end_day,$section_name);
                $sectionModel = new Section();
                $sectionUserNum = $sectionModel->getAllEncourageSectionUserNumber();
                $team_type_name = '激励经费';
                $csvTitle = array('业务线', '部门', '负责人','激励详情','实际花费','审核状态');

            } else {

                $model = new TeamBuilding();
                $data = $model->getListByDate($start_day,$end_day,$section_name);

                $sectionModel = new Section();
                $sectionUserNum = $sectionModel->getAllBuildSectionUserNumber();
                $team_type_name = '团建经费';
                $csvTitle = array('业务线', '部门', '负责人', '活动时间','活动类型', '活动目的', '活动内容', '参加人数', '实际花费','审核状态');

            }

            $activity_conf = OAConfig::$activity_type;
            if(empty($saveCsv)){
                $arrTplData['start_day'] = $start_day;
                $arrTplData['end_day'] = $end_day;
                $arrTplData['section_name'] = $section_name;
                $arrTplData['arrList'] = $data;
                $arrTplData['team_type'] = $team_type;
                $arrTplData['sectionUserNum'] = $sectionUserNum;
                $arrTplData['activity_conf'] = $activity_conf;
                $arrTplData['audit_status'] = OAConfig::$audit_status;
                $arrTplData['savelink'] = Yii::app()->request->getUrl()."&saveCsv=1";
                $this->renderFile('./export/export_tuanjian.tpl',$arrTplData);
                Yii::app()->end();
            }

            $ret = array();
            $filename = "{$team_type_name}报销导出({$start_day}-{$end_day}).csv";
            header("Content-Disposition: attachment; filename={$filename}");
            
            $output = fopen('php://output','w') or die("Can't open php://output");
        
            $titleNum = count($csvTitle);
            for($intNum = 0; $intNum< $titleNum;$intNum++){
                $csvTitle[$intNum] = iconv("UTF-8","gbk",$csvTitle[$intNum]);
            }
            fputcsv($output,$csvTitle);

            foreach ($data as $val){
                $index = 0;
                $data = array();

                if($team_type == 1) {
                    $data[0] = iconv("UTF-8","gbk", $val['team']);
                    $data[1] = iconv("UTF-8","gbk", $val['s_name']);
                    $data[2] = iconv("UTF-8","gbk", $val['uname']);
                    $data[3] = iconv("UTF-8","gbk", date("Y-m-d", $val['time']) );
                    $data[4] = iconv("UTF-8","gbk", $activity_conf[ $val['type'] ]);
                    $data[5] = iconv("UTF-8","gbk", $val['title']);
                    $data[6] = iconv("UTF-8","gbk", $val['desc']);
                    $section = $val['s_id'];
                    $preson_num = count($sectionUserNum[$section]['users']);
                    $data[7] = iconv("UTF-8","gbk", $preson_num);
                    $data[8] = iconv("UTF-8","gbk", $val['amount']);
                    $check = ($val['audit_status'] == OA_WORKFLOW_AUDIT_SHENPIZHONG) ? '审核中' : '已通过';
                    $data[9] = iconv("UTF-8","gbk", $check);
                } else {

                    $data[0] = iconv("UTF-8","gbk", $val['team']);
                    $data[1] = iconv("UTF-8","gbk", $val['s_name']);
                    $data[2] = iconv("UTF-8","gbk", $val['uname']);
                    $data[3] = iconv("UTF-8","gbk", $val['remark']);
                    $data[4] = iconv("UTF-8","gbk", $val['amount']);
                    $check = ($val['audit_status'] == OA_WORKFLOW_AUDIT_SHENPIZHONG) ? '审核中' : '已通过';
                    $data[5] = iconv("UTF-8","gbk", $check);

                }

                fputcsv($output,$data);	
            }

            fclose($output) or die("Can't close php://output");            
            Yii::app()->end();
        }else{
            $arrTplData['start_day'] = date('Y-m-d',strtotime('-1 month'));
            $arrTplData['end_day'] = date('Y-m-d');
            $this->renderFile('./export/export_tuanjian.tpl',$arrTplData);
        }
    }

    //考勤统计
    //思路 第一 将考勤和请假记录整理成user_id,date为key的二维数组
    //第二 根据vacation_record 统计出各种请假的天数
    //第三 根据checkinout 统计工作日正常出勤及各种原因的出勤天数
    public function actionStatistics()
    {
        if(Yii::app()->request->getIsPostRequest()){
            $startDay = Yii::app()->request->getParam('start_day');
            $endDay = Yii::app()->request->getParam('end_day');

            $modelCheckinout = new CheckInOut();
            $modelVacation = new VacationRecord();
            $serviceCheckInOut = new CheckInOutService();
            $serviceVacation = new VacationService();
            $workDay = $serviceVacation->workingDay($startDay,$endDay);
            $beginDay = $startDay;
            while($startDay < $endDay){ //暂时不考虑分日期取结果，涉及统计集合并
                $nextDay = date('Y-m-d',strtotime('+1 month',strtotime($startDay)));
                if($nextDay > $endDay){
                    $nextDay = $endDay;
                }
                $nextDay = $endDay;
                $checkInOuts = $modelCheckinout->getListByStartDayAndEndDay($startDay,$nextDay);
                $vacationRecords = $modelVacation->getListByStartDayandEndDay($startDay,$nextDay);

                $checkInOuts = $serviceCheckInOut->finishingCheckinoutToArray($checkInOuts);
                $vacationRecords = $serviceVacation->finishingVacationToArray($vacationRecords);

                $vacationStatistics = $serviceVacation->statisticsVacationRecord($vacationRecords,$checkInOuts);
                $checkinoutStatistics = $serviceCheckInOut->statisticsCheckInOut($checkInOuts,$workDay);
                $startDay = $nextDay;
            }
            $userModel = new User();
            $users = $userModel->getAllValidUsers();

            $data = array();
            $absenceType = OAConfig::$absenceType;
            $absenceType[VACATION_BINGXIUJIA] = '病休假';
            $absenceType[VACATION_CHUCHAI] = '出差';
            $absenceType[VACATION_YINGONGWAICHU] = '因公外出';
            $absenteeismType = OAConfig::$absenteeismType;
            $absenteeismType[SYSTEM_QITA] = '考勤异常';
            $workDayNum = 0; //工作日天数
            foreach($workDay as $val){
                if($val){
                    $workDayNum += 1;
                }
            }
            foreach($users as $id => $user){
                if(!isset($checkinoutStatistics[$id]) && !isset($vacationStatistics[$id])){
                    continue;
                }    
                $data[$id]['id_card'] = $user['id_card'];
                $data[$id]['name'] = $user['name'];
                $data[$id]['gongzuori'] = $checkinoutStatistics[$id]['gongzuori'];
                $data[$id]['jiejiari'] = $checkinoutStatistics[$id]['jiejiari'];
                $chuqin = $shijifaxin = $data[$id]['gongzuori'];
                $shijiabingxiu = 0;
                foreach($absenteeismType as $k1 => $v1){
                    if($k1 != SYSTEM_QITA){ //计算出勤天数
                        $chuqin += $checkinoutStatistics[$id][$k1];
                    }
                    if($k1 != SYSTEM_QITA){//实际发薪天数
                        $shijifaxin += $checkinoutStatistics[$id][$k1];
                    }
                    $data[$id][$v1] =  $checkinoutStatistics[$id][$k1];
                }
                foreach($absenceType as $k2 => $v2){
                    if($k2 != VACATION_SHIJIA && $k2 != VACATION_BINGXIUJIA && $k2 != 11){
                        $shijifaxin -= $vacationStatistics[$id][$k2];
                    }else{
                        $shijiabingxiu += $vacationStatistics[$id][$k2]; //事假、病休假
                    }
                    if(isset($data[$id][$v2])){
                        $data[$id][$v2] -= $vacationStatistics[$id][$k2];
                    }else{
                        $data[$id][$v2] = -$vacationStatistics[$id][$k2];
                    }
                }
                $data[$id]['zaocan'] = $checkinoutStatistics[$id]['zaocan'];
                $data[$id]['chuqin'] = $chuqin;
                $data[$id]['shijifaxin'] = $shijifaxin;
                $data[$id]['yingfaxin'] = $workDayNum + $shijiabingxiu;
            }
            $data = array_values($data);

            $excelTitle = array('员工号','姓名','正常打卡','节假日出勤','未带卡','忘打卡','外出培训','卡丢失','其他分部办公','在家办公','迟到','早退','加班至次日凌晨','部门排班','考勤异常','事假','病假','年假','婚假','丧假','调休','产假','陪产假','产检假','哺乳假','其他','病休假','出差','因公外出','早餐','出勤天数','实际发薪天数','应发薪天数');

            $arrLetter = array();
            for ($i = 65; $i < 65+26; $i++){
                $arrLetter[] = chr($i); 
            }
            $arrLetter[] = 'AA';
            $arrLetter[] = 'AB';
            $arrLetter[] = 'AC';
            $arrLetter[] = 'AD';
            $arrLetter[] = 'AE';
            $arrLetter[] = 'AF';
            $arrLetter[] = 'AG';

            set_include_path(get_include_path() . PATH_SEPARATOR . Config :: $basePath . "/trunk/rong360/shared/excel/");
            include_once 'PHPExcel.php';
            include_once 'PHPExcel/Writer/Excel5.php';

            $objPHPExcel = new PHPExcel(); 

            $objPHPExcel->getProperties()->setCreator("融360");
            $objPHPExcel->getProperties()->setLastModifiedBy("融360");
            $objPHPExcel->getProperties()->setTitle("考勤统计".$beginDay."_".$endDay);
            $objPHPExcel->getProperties()->setSubject("考勤统计".$beginDay."_".$endDay);
            $objPHPExcel->getProperties()->setDescription("考勤统计".$beginDay."_".$endDay);

            $objPHPExcel->setActiveSheetIndex(0);

            foreach($excelTitle as $k => $title){
               $objPHPExcel->getActiveSheet()->SetCellValue($arrLetter[$k].'1',$title); 
            }

            foreach($data as $n => $val){
                $index = 0;
                foreach($val as $item){
                    $key = $arrLetter[$index].($n+2);
                    if($arrLetter[$index] == 'A'){
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit($key,$item,PHPExcel_Cell_DataType::TYPE_STRING); 
                    }else{
                        $objPHPExcel->getActiveSheet()->SetCellValue($key,$item); 
                    }
                    $index++;
                }
            }

            $company = 'kaoqintongji'.date('Y-m-d',strtotime($beginDay))."-".date('Y-m-d',strtotime($endDay)).".xls";
            $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
            header("Content-Type: application/force-download"); 
            header("Content-Type: application/octet-stream"); 
            header("Content-Type: application/download"); 
            header('Content-Disposition:inline;filename="'.$company.'"'); 
            header("Content-Transfer-Encoding: binary"); 
            header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
            header("Pragma: no-cache"); 
            $objWriter->save("php://output");

        }
        $this->renderFile('./export/statistics.tpl');
    }

    /**
     * 导出报销时间
     **/
    function actionCancelAbsence() {


        $query = $arrPrams = Yii::app()->request->getQuery('query');
        $starDate     = $arrPrams['start_time'];
        $endDate      = $arrPrams['end_time'];
        $starTime     = strtotime( $starDate );
        $endTime      = strtotime( $endDate );
        $name         = $arrPrams['name'];
        $section_name = $arrPrams['section_name'];
        $idCard       = $arrPrams['id_card'];
        $saveCsv      = $arrPrams['saveCsv'];

        //isset第一次访问不希望出这个提示
        if(isset($arrPrams['start_time']) && 
            (empty($starTime) || empty($endTime) ) ) {

            $arrTpl['msg'] = "时间必须填写完整！";

        } 

        if(!empty($starTime) && !empty($endTime) ) {
            
            $conditionInfo = array(

                'uName'     => $name,
                'sName'     => $section_name,
                "startTime" => $starTime,
                "endTime"   => $endTime,
                "id_card"   => $idCard

                );
            $cancelModel = new CancelAbsence();

            $data = $cancelModel->getExcelPrint($conditionInfo);
            $arrTpl['savelink'] = Yii::app()->request->getUrl()."&query%5BsaveCsv%5D=1";
            if(empty($saveCsv)){
                $arrTpl['arrList'] = $data;
                $arrTpl['query'] = $query;
                $this->renderFile("cancel_absence.tpl",$arrTpl);
                Yii::app()->end();
            }
            
            $data = array_values($data);
            $res  = array();
            foreach($data as $row){
                $row['audit_status'] = OAConfig::$audit_status[$row['audit_status']] ;
                $row['start_time'] = date('Y-m-d H:i:s',$row['start_time']);
                $row['end_time'] = date('Y-m-d H:i:s',$row['end_time']);
                $res[] = $row;
            }

            $excelInfo = array(
                'file'  => "销假{$starDate}-{$endDate}.xls",
                'title' => '销假导出',
                'excelTitle' => array(
                    '员工号','姓名', '标题', '开始时间','结束时间', '所属部门', '业务线','销假原因', '天数','状态' ),
                'res'   => $res,
                
                );

            $this->printExcel($excelInfo);

        }else{
            $query['start_time'] = date('Y-m-26',strtotime("-1 month"));
            $query['end_time']   = date('Y-m-d',time());
            $arrTpl['query'] = $query;
            $this->renderFile("cancel_absence.tpl",$arrTpl);
        }


    }


    /**
     * 导出销假信息
     **/
    function actionUserInfo() {

        $userModel = new User();

        $data = $userModel->getAllUserInfo();
        $data = array_values($data);

        $excelInfo = array(
            'file'  => '所有员工信息'.date('Y-m-d').".xls",
            'title' => '所有员工信息',
            'excelTitle' => array(
                '员工号','姓名','性别', '邮箱', '手机号','所属部门', '业务线','是否在职', '状态', '参加工作时间', '入职时间', '离职时间', '结薪时间' ),
            'res'   => $data,
            
            );

        $this->printExcel($excelInfo);

    }

    function printExcel($excelInfo) {

        $data = $excelInfo['res'];
        $file = $excelInfo['file'];
        $excelTitle =  $excelInfo['excelTitle'];
        $cols = count($excelTitle);
        $title = $excelInfo['title'];

        $arrLetter = array();

        for ($i = 65; $i < 65+$cols; $i++){

            $arrLetter[] = chr($i); 

        }
        set_include_path(get_include_path() . PATH_SEPARATOR . Config :: $basePath . "/common/shared/excel/");
        include_once 'PHPExcel.php';
        include_once 'PHPExcel/Writer/Excel5.php';

        $objPHPExcel = new PHPExcel(); 

        $objPHPExcel->getProperties()->setCreator("融360");
        $objPHPExcel->getProperties()->setLastModifiedBy("融360");
        $objPHPExcel->getProperties()->setTitle($title);
        $objPHPExcel->getProperties()->setSubject($title);
        $objPHPExcel->getProperties()->setDescription($title);

        $objPHPExcel->setActiveSheetIndex(0);

        foreach($excelTitle as $k => $title){

            $objPHPExcel->getActiveSheet()->SetCellValue($arrLetter[$k].'1',$title); 

        }
        if($this->action->id == 'apply'){
            $i = 2;
            foreach($data as $item){ 
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, date("Y-m-d",$item['create_time'])); 
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $item['user']); 
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $item['sname']); 
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $item['staff_type']); 
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $item['staff']); 
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $item['staff_ver']); 
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $item['staff_price']); 
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $item['number']); 
                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $item['staff_total']); 
                $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $item['usage']); 
                $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, date("Y-m-d",$item['create_time'])); 
                $i ++; 
            }
        }
        else{
            foreach($data as $n => $val){
                $index = 0;
                foreach($val as $item){
                    $key = $arrLetter[$index].($n+2);
                    if($arrLetter[$index] == 'A'){
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit($key,$item,PHPExcel_Cell_DataType::TYPE_STRING); 
                    }else{
                        $objPHPExcel->getActiveSheet()->SetCellValue($key,$item); 
                    }
                    $index++;
                }
            }
        }
        $company = $file;
        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        header("Content-Type: application/force-download"); 
        header("Content-Type: application/octet-stream"); 
        header("Content-Type: application/download"); 
        header('Content-Disposition:inline;filename="'.$file.'"'); 
        header("Content-Transfer-Encoding: binary"); 
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
        header("Pragma: no-cache"); 
        $objWriter->save("php://output");

    }

    /**
     * 高级列表
     **/
    public function actionSuperList() {

            $select_num = Yii::app()->request->getParam('select_num',2);
            $search = Yii::app()->request->getParam('search');
            $start_date = Yii::app()->request->getParam('start_date');
            $end_date = Yii::app()->request->getParam('end_date');

            $model = new VacationRecord();
            $condition['select_num'] = $select_num;

            if(empty($start_date) || empty($end_date) ) {

                $end_date = date("Y-m-d");
                $start_time   = strtotime("-30day");
                $start_date = date("Y-m-d", $start_time);

            }


            $condition['start_date'] = $start_date; 
            $condition['end_date']   = $end_date; 
            $condition['search'] = $search; 

            $result = $model->getListByAll($condition);
            $data = $result['ret'];
            // echo "<pre>";
            // var_dump($data);
            // die();


            $week = array('日','一','二','三','四','五','六');

            $arrData['option_list'] = array(
                '1' => "全部流水",
                '2' => "年假流水",
                '3' => "病假流水",
            );

            $arrData['week'] = $week;
            $arrData['select_num'] = $select_num;

            $title  = "年假";


            if($select_num == 1) {

                $title  = "年假、病假";

            } else if($select_num == 3) {

                $title  = "病假";

            }
            $title_conf = array(
                '1'=>'病假',
                '2'=>'年假',
                '13'=>'事假'

            );

            if(empty($data)){
                throw new CHttpException(404,"没有数据");
            }

            $filename = "{$title}流水({$start_date}-{$end_date}).csv";

            header('Content-Type: application/csv');
            header("Content-Disposition: attachment; filename=".$filename."");

            $csvTitle = array('工号','姓名','部门','年假','日期', '天数');
            $output = fopen('php://output','w') or die("Can't open php://output");

            for($intNum = 0; $intNum< 11;$intNum++){
                $csvTitle[$intNum] = iconv("UTF-8","gbk",$csvTitle[$intNum]);
            }

            fputcsv($output,$csvTitle);

            foreach($data as $index => $row){
                $ret = array();
                $ret[0] = $row['id_card']; 
                $ret[1] = iconv("UTF-8","gbk",$row['uname']);
                $ret[2] = iconv("UTF-8","gbk",$row['sname']);
                $ret[3] = iconv("UTF-8","gbk",$title_conf[$row['absence_type']]);
                $ret[4] = $row['date'];
                $ret[5] = $row['day_num'];

                fputcsv($output,$ret);	

            }

            fclose($output) or die("Can't close php://output");            
            Yii::app()->end();


    }

    public function actionDinner() {

        $homeService = new HomeService();
        $data = $homeService->exportDinner();
        $total= count($data);
        $data = array_values($data);

        $excelInfo = array(
            'file'  => "订餐记录({$total})条".date('Y-m-d').".xls",
            'title' => '订餐记录({$total})条',
            'excelTitle' => array(
                '员工号','姓名', '邮箱', '办公地点', '订餐时间' ),
            'res'   => $data,
            
            );

        $this->printExcel($excelInfo);

    }

    public function actionExpress() {

        $start_date = Yii::app()->request->getParam('start_date');
        $end_date   = Yii::app()->request->getParam('end_date');
        $office = Yii::app()->request->getParam('office');

        $expressModel = new Express();
        if(!empty($start_date)){
            $whereSql .= " e.date >= '{$start_date}' AND";
        }
        if(!empty($end_date)){
            $whereSql .= " e.date <= '{$end_date}' ";
        }


        $homeService = new HomeService();
        $goodsConf   = $homeService->goodsConf;
        $expressConf = $homeService->expressConf;
        $officeConf  = $homeService->officeEmsConf;

        $officeName  = "";

        if(!empty($office) ) {

            $whereSql .= "AND e.office = {$office}";
            $officeName = "-".$officeConf[$office];

        }

        $userRes = $expressModel->getAllExpress($whereSql, $start, $per);
        $data = array();


        foreach($userRes['data'] as $value) {

            $tmp_data = array();
            $tmp_data['create_time'] = date("Y-m-d", $value['create_time'] );
            $tmp_data['name'] = $value['name'];
            $tmp_data['team_name'] = $value['team_name'];
            $tmp_data['section_name'] = $value['section_name'];
            $tmp_data['office'] = $officeConf[$value['office']];
            $tmp_data['goods']  = $goodsConf[$value['goods_type']];
            $tmp_data['to_where'] = $value['to_where'];
            $tmp_data['to_city'] = $value['to_city'];
            $tmp_data['express_name'] = $expressConf[$value['express_type']];
            $tmp_data['express_num'] = $value['express_num'];
            $data[] = $tmp_data;

        }
        $data = array_values($data);

        $total= count($data);
        $excelInfo = array(
            'file'  => "对公快递{$officeName}记录({$total})条".date('Y-m-d').".xls",
            'title' => "快递记录({$total})条",
            'excelTitle' => array(
                '日期','姓名', '业务线', '部门', '办公区', '物品类型', '收件人或公司', '城市', '快递名称', '快递号' ),
            'res'   => $data,
            
        );

        $this->printExcel($excelInfo);
    }

    public function actionExpense()
    {
        $arrData['start_day'] = date('Y-m-01');
        $arrData['end_day'] = date('Y-m-d');
        if(Yii::app()->request->getIsPostRequest()){
            $start_day = Yii::app()->request->getParam('start_day');
            $end_day = Yii::app()->request->getParam('end_day');  
            $startTime = strtotime($start_day);
            $endTime = strtotime($end_day) + 86400;

            $model = new AdministrativeExpense();
            $result = $model->getListByTime($startTime,$endTime);
            if(is_array($result)){
                foreach($result as $item){
                    $tmp = array();
                    $tmp['create_date'] = date('Y-m-d',$item['create_time']);
                    $tmp['expense_type'] = $item['expense_type'];
                    $tmp['user_name'] = $item['user_name'];
                    $tmp['section_name'] = $item['section_name'];
                    $tmp['summary'] = $item['summary'];
                    $tmp['amount'] = $item['amount'];
                    $tmp['payment_nature'] = $item['payment_nature'];
                    $tmp['account_name'] = $item['account_name'];
                    $tmp['remark'] = $item['remark'];  
                    $tmp['audit_status'] = OAConfig::$audit_status[$item['audit_status']];
                    $data[] = $tmp;
                }
            }

            $excelInfo = array(
                'file'  => "行政费用付款申请单".".xls",
                'title' => "行政费用付款申请单".".xls",
                'excelTitle' => array(
                    '申请日期','费用类型', '申请人', '部门', '付款事项简介', '金额', '付款性质', '账户名称', '备注信息', '审批状态' ),
                'res'   => $data,

            );

            $this->printExcel($excelInfo);
        }
        $this->renderFile('./export/expense.tpl',$arrData);
    }

    public function actionApply(){
        if(Yii::app()->request->getIsPostRequest()){
            $startDay = Yii::app()->request->getParam('start_day');
            $endDay = Yii::app()->request->getParam('end_day');;
            $sectionName = Yii::app()->request->getParam('section');
            $userName = Yii::app()->request->getParam('name');
            $idCard = Yii::app()->request->getParam('id_card');
            $model = new AdministrativeApply();
            $arrData['list'] = $model->getListByQuery($startDay,$endDay,$idCard,$sectionName,$userName);
            $excelInfo = array(
                'file'  => "物品申请数据".".xls",
                'title' => "物品申请数据".".xls",
                'excelTitle' => array(
                    '申请日期','申请人', '部门', '物品类型', '物品名称', '物品型号', '单价', '数量', '合计', '申请用途', '领用日期' ),
                'res'   => $arrData['list'],

            );

            $this->printExcel($excelInfo);
        }
        $this->renderFile('./export/apply.tpl',$arrData);
    }

    public function actionCard(){
        if(Yii::app()->request->getIsPostRequest()){
            $startDay = Yii::app()->request->getParam('start_day');
            $endDay = Yii::app()->request->getParam('end_day');;
            $sectionName = Yii::app()->request->getParam('section');
            $userName = Yii::app()->request->getParam('name');
            $idCard = Yii::app()->request->getParam('id_card');
            $model = new BusinessCard();
            $arrData['list'] = $model->getListByQuery($startDay,$endDay,$idCard,$sectionName,$userName);
            // var_dump($arrData['list']);
            $excelInfo = array(
                'file'  => "名片制作数据".".xls",
                'title' => "名片制作数据".".xls",
                'excelTitle' => array(
                    '名片提交日期','工号', '姓名', '部门', '职位', '电话','手机', '邮箱', '印刷数量', '行政审批时间' ),
                'res'   => $arrData['list'],

            );

            $this->printExcel($excelInfo);
        }
        $this->renderFile('./export/card.tpl',$arrData);
    }

}
