<?php
class AdministrativeapplyController extends Controller
{
    public static $staff=array(
        1 => array(
            'main' => "礼品卡",
            'sub' => array("京东100元购物卡","京东200元购物卡","京东500元购物卡","京东1000元购物卡","其他")
        ),
        2 => array(
            'main' => "非标配设备"
        ),
        3 => array(
            'main' => "小礼品",
            'sub' => array("雨伞","水杯","LED灯","环保记事本","其他")
        ),
        4 => array(
            'main' => "其他"
        )
    );
    public function actionAdd()
    {
        $user = Yii::app()->params['user'];//from base Controller
        $workflow_id = Yii::app()->request->getParam('workflow_id');
        $workflow_id = intval($workflow_id);
        $sectionModel = new Section();
        $section = $sectionModel->getItemByPk($user['section_id']);
        $sections = $sectionModel->getParentSectionBySectionId($section['id']);
        $sections = array_merge(array($section),$sections);
        $arrData['sections'] = $sections;
        $workflowModel = new WorkFlow();
        $workflow = $workflowModel->getItemByPk($workflow_id);

        if(empty($workflow_id) || empty($workflow)){
            throw new CHttpException(404,'缺少参数');
        }

        $arrData['user'] = $user;
        $arrData['section'] = $section;
        $arrData['workflow_id'] = $workflow_id;
        $arrData['workflow'] = $workflow;
        $arrData['staff']= self::$staff;
        $this->renderFile('./administrative_apply/add.tpl',$arrData);
    }
     public function actionAddPost()
    {
        $user = Yii::app()->params['user'];//from base Controller
        $workflow_id = Yii::app()->request->getParam('workflow_id');
        $sectionModel = new Section();
        $section = $sectionModel->getItemByPk($user['section_id']);
        $sections = $sectionModel->getParentSectionBySectionId($section['id']);
        $sections = array_merge(array($section),$sections);
        if(Yii::app()->request->getIsPostRequest()){
            $arrPost = array();
            $index1=Yii::app()->request->getParam('staff_main');
            if($index1 == 4){
                $arrPost['staff'] = Yii::app()->request->getParam('staff_other');
            }elseif ($index1 ==2) {
                $arrPost['staff'] = Yii::app()->request->getParam('staff_sub_other_name').Yii::app()->request->getParam('staff_sub_other_type');
            }
            else{
                $index2 = Yii::app()->request->getParam('staff_sub');
                if(($index1==1 && $index2==5)||($index1==3 &&$index2==5)){
                    $arrPost['staff'] = Yii::app()->request->getParam('staff_other');
                }
                else{
                    $arrPost['staff'] = self::$staff[$index1]['sub'][$index2-1];
                }
            }
            $arrPost['number'] = Yii::app()->request->getParam('number');
            $arrPost['usage'] = Yii::app()->request->getParam('usage');
            $arrPost['create_time'] = time();
            $arrPost['user_id'] = $user['id'];

            $model = new AdministrativeApply();
            $id = $model->save($arrPost);

            $workflowModel = new WorkFlow();
            $workflow =  $workflowModel->getItemByPk($workflow_id);
            
            $userWorkflowModel = new UserWorkFlow();
            $userWorkflowId = $userWorkflowModel->saveUserWorkflow($workflow,$id);

            $workflowService = new WorkflowService($userWorkflowId);
            $workflowService->workflowProcess();//audit operation

            $this->redirect(Yii::app()->createURL('userworkflow/my'));
        }
    }
   
}


