<?php 
class UsertaskController extends Controller
{
    public static $staff_type=array(1=>"非标配固定资产",2=>"礼品",3=>"耗材");

    //审批列表页
    public function actionList()
    {
        $userId = Yii::app()->session['oa_user_id'];
        $audit_status = Yii::app()->request->getParam('audit_status');
        $obj_type = Yii::app()->request->getParam('obj_type');
        $keyword = Yii::app()->request->getParam('keyword');
        $pn = Yii::app()->request->getParam('pn',1);
        $rn = Yii::app()->request->getParam('rn',DEFAULT_PAGE_NUMBER);
        $start = ($pn-1)*$rn;

        $userTaskService = new UserTaskService();
        $result = $userTaskService->search($userId,$audit_status,$keyword,$obj_type,$start,$rn);
        $arrData['pn'] = $pn;
        $arrData['rn'] = $rn;
        $arrData['url'] = $audit_status ? '/usertask/list?audit_status='.$audit_status."&" : '/usertask/list?';
        $arrData['url'] .= $obj_type ? ('obj_type=' . $obj_type . '&') : '';
        $arrData['url'] .= $keyword ? ('keyword=' . $keyword . '&') : '';
        $arrData['audit_status'] = intval($audit_status);
        $arrData['obj_type'] = $obj_type;
        $arrData['keyword'] = $keyword;
        $arrData['userTaskCnt'] = $result['userTaskCnt'];
        $arrData['current_audit_status'] = $audit_status;
        $arrData['list'] = $result['list'];
        $arrData['user_task_audit_status'] = OAConfig::$user_task_audit_status;
        $arrData['uwf_audit_status'] = OAConfig::$audit_status;
        $arrData['workflow_type'] = OAConfig::$workflow_type;
        $arrData['workorderType'] = OAConfig::$workorderType; 

        if(Yii::app()->request->isAjaxRequest){
            $arrData['shenpi'] = 1;
            $this->renderFile('./widget/daishenpi_content.tpl',$arrData);
        }else{
            $this->renderFile('./daishenpi.tpl',$arrData);
        }
    } 

    //审批详情页
    public function actionDetail()
    {
        $id = Yii::app()->request->getParam('user_task_id'); 

        $model = new UserTask();
        $userTask = $model->getWorkflowByUserTaskId($id);
        $workflowId = $userTask['user_workflow_id'];
        $objWorkflow = new UserWorkFlow();
        $objUserWorkflow = $objWorkflow->getItemByPk($workflowId);
        $applyUserId = $objUserWorkflow['user_id'];
        $userModel = new User();
        $applyUser = $userModel->getItemByPk($applyUserId);
        $arrData['applyUser'] = $applyUser;
        $arrData['userTask'] = $userTask;
        $sectionModel = new Section();
        $tmpSection = $sectionModel->getItemByPk($applyUser['section_id']);
        $sections = $sectionModel->getParentSectionBySectionId($applyUser['section_id']);
        $sections = array_merge(array($tmpSection),$sections);
        $arrData['sections'] = $sections;

        $this->checkRole($userTask);

        switch($userTask['obj_type']){
        case 'daily_reimbursement':
            $dModel = new DailyReimbursement();
            $arrData['daily_reimbursement'] = $dModel->getItemsByParentId($userTask['obj_id']);
            $arrData['daily_reimbursementType'] = OAConfig::$daily_reimbursementType; 
            break;
        case 'business_advance':
            $bModel = new BusinessAdvance();
            $arrData['business_advance'] = $bModel->getItemsByParentId($userTask['obj_id']);
            $arrData['business_advanceType'] = OAConfig::$business_advanceType;
            break;
        case 'business_reimbursement':
            $bModel = new BusinessReimbursement();
            $arrData['business_reimbursement'] = $bModel->getItemsByParentId($userTask['obj_id']);
            $arrData['business_reimbursementType'] = OAConfig::$business_reimbursementType;
            break;
        case 'absence' : 
            $userWorkflowModel = new UserWorkFlow(); 
            $arrData['absenceType'] = OAConfig::$absenceType;
            $arrData['timeNode'] = OAConfig::$time_node;
            break;
        case 'cancel_absence' :
            $userWorkflowModel = new UserWorkFlow(); 
            $arrData['timeNode'] = OAConfig::$time_node;
            break;
        case 'cancel_business' :
            $userWorkflowModel = new UserWorkFlow(); 
            $arrData['timeNode'] = OAConfig::$time_node;
            break;
        case 'online_order':
            $uwfModel = new UserWorkFlow();
            $userWorkflow = $uwfModel->getAllOfUserWorkflowById($userTask['user_workflow_id']);
            $arrData['userWorkflow'] = $userWorkflow;
            $arrData['function_type'] = OAConfig::$function_type;
            $arrData['qa_type'] = OAConfig::$qa_type;
            $arrData['business_role'] = OAConfig::$business_role;
            $steps = str_replace(PHP_EOL, '<br>', $userWorkflow['ext']['online_steps']);
            $arrData['steps'] = $steps;
            $arrData['filenames'] = OnlineOrder::getFileName($userWorkflow['ext']['attachment_address']);
            break;
        case 'entry':
            $uwfModel = new UserWorkFlow();
            $userWorkflow = $uwfModel->getAllOfUserWorkflowById($userTask['user_workflow_id']);
            $sectionModel = new Section();
            $arrData['userWorkflow'] = $userWorkflow;
            $arrData['businessLine'] = $sectionModel->getItemByPk($arrData['userWorkflow']['ext']['business_line_id']);
            $arrData['businessLine'] = $arrData['businessLine']['name'];
            $arrData['section1'] = $sectionModel->getItemByPk($arrData['userWorkflow']['ext']['section_id']);
            $arrData['section1'] = $arrData['section1']['name'];
            $arrData['subSection'] = $sectionModel->getItemByPk($arrData['userWorkflow']['ext']['subsection_id']);
            $arrData['subSection'] = $arrData['subSection']['name'];
            $arrData['business_role'] = OAConfig::$business_role;
            $arrData['hireTypes'] = OAConfig::$hireTypes;
            $arrData['directManager'] = $userModel->getItemByPk($arrData['userWorkflow']['ext']['manager_id']);
            $arrData['directManager'] = $arrData['directManager']['name'];
            if($arrData['subSection'] != 'Estaff'){
                $arrData['targetSection'] = $arrData['subSection'];
            }elseif($arrData['section1'] != 'Estaff'){
                $arrData['targetSection'] = $arrData['section1'];
            }elseif($arrData['businessLine'] != 'Estaff'){
                $arrData['targetSection'] = $arrData['businessLine'];
            }else{
                $arrData['targetSection'] = 'Estaff';
            }
            $arrData['interviewers'] = array();
            $entryId = $userTask['obj_id'];
            $departmentAssessmentModel = new DepartmentAssessment();
            $departmentAssessments = $departmentAssessmentModel->getDepartmentAssessmentsByEntryId($entryId);
            foreach ($departmentAssessments as &$value){
                $interviewerId = $value['user_id'];
                $objInterviewer = $userModel->getItemByPk($interviewerId);
                $value['name'] = $objInterviewer['name'];
                $value['interviewers'] = $objInterviewer['name'];
            }
            if($userWorkflow['audit_status'] != OA_WORKFLOW_AUDIT_PASS){
                $arrData['interviewers'] = $userModel->getInterviewersByWorkflowId($userWorkflow['id']);
            }
            $arrData['interviewers'] = implode(', ', $arrData['interviewers']);
            $arrData['departmentAssessments'] = $departmentAssessments;
            $hrAssessmentModel = new HrAssessment();
            $hrAssessments = $hrAssessmentModel->getHrAssessmentsByEntryId($entryId);
            foreach ($hrAssessments as &$value){
                $hrId = $value['user_id'];
                $objHr = $userModel->getItemByPk($hrId);
                $value['name'] = $objHr['name'];
            }
            $arrData['hrAssessments'] = $hrAssessments;
            break;
        case 'dimission':
            $uwfModel = new UserWorkFlow();
            $userWorkflow = $uwfModel->getAllOfUserWorkflowById($userTask['user_workflow_id']);
            $sectionModel = new Section();
            $arrData['userWorkflow'] = $userWorkflow;
            $arrData['business_role'] = OAConfig::$business_role;
            $dimissionUser = $userModel->getItemByPk($userWorkflow['user_id']);
            $dimissionUserDepartment = $sectionModel->getItemByPk($dimissionUser['section_id']);
            $dimissionUser['department'] = $dimissionUserDepartment['name'];
            $dimissionUserManager = $userModel->getItemByPk($dimissionUserDepartment[manager_id]);
            $dimissionUser['dimissionUserManager'] = $dimissionUserManager;
            $arrData['dimissionUser'] = $dimissionUser;
            break;
        case 'workorder':
            $userWorkflowModel = new UserWorkFlow();
            $userWorkflow = $userWorkflowModel->getAllOfUserWorkflowById($userTask['user_workflow_id']);
            $obj_id = $userWorkflow['obj_id'];
            $workorderModel = new WorkOrder();
            $workorderItem = $workorderModel->getItemByPk($obj_id);
            $user_id = $workorderItem['user_id'];
            $userModel = new User();
            $workorderUser = $userModel->getItemByPk($user_id);
            $arrData['workordertype'] = $workorderItem['type'];
            $arrData['problem_reason'] = $workorderItem['problem_reason'];
            $arrData['workorderlevel'] = $workorderItem['level'];
            $arrData['do_time'] = $workorderItem['do_time'];
            $arrData['pause_time'] = $workorderItem['pause_time'];
            $arrData['workorderType'] = OAConfig::$workorderType;
            $arrData['workorderLevel'] = OAConfig::$workorderLevel;
            $arrData['workorderUser'] = $workorderUser;
            break;
        case 'papp_vpn':
            $userWorkflowModel = new UserWorkFlow();
            $userWorkflow = $userWorkflowModel->getAllOfUserWorkflowById($userTask['user_workflow_id']);
            $obj_id = $userWorkflow['obj_id'];
            $pappvpnModel = new PappVpn();
            $pappvpnItem = $pappvpnModel->getItemByPk($obj_id);
            $user_id = $pappvpnItem['user_id'];
            $userModel = new User();
            $pappvpnUser = $userModel->getItemByPk($user_id);
            $arrData['pappvpnUser'] = $pappvpnUser;
            $arrData['reason'] = $pappvpnItem['reason'];
            $arrData['type']   = $pappvpnItem['type'];
            $arrData['vpnType'] = OAConfig::$vpnType;
            $arrData['business_role'] = OAConfig::$business_role;
            break;

        case 'papp_db':
            $userWorkflowModel = new UserWorkFlow();
            $userWorkflow = $userWorkflowModel->getAllOfUserWorkflowById($userTask['user_workflow_id']);
            $obj_id = $userWorkflow['obj_id'];
            $pappdbModel = new PappDB();
            $pappdbItem = $pappdbModel->getItemByPk($obj_id);
            $user_id = $pappdbItem['user_id'];
            $userModel = new User();
            $pappdbUser = $userModel->getItemByPk($user_id);
            $arrData['pappdbUser'] = $pappdbUser;
            $arrData['pappdbItem'] = $pappdbItem;
            break;
        case 'office_supply':
            $userWorkflowModel = new UserWorkFlow();
            $userWorkflow = $userWorkflowModel->getAllOfUserWorkflowById($userTask['user_workflow_id']);
            $arrData['userWorkflow'] = $userWorkflow;
            $arrData['officeLocation'] = OAConfig::$officeLocation;
            $arrData['officeSupply'] = OAConfig::$officeSupply;
            $arrSupplyStrings = explode('|', $userWorkflow['ext']['supply_type_detail_amount']);
            $arrData['supplyDetails'] = array();
            if(is_array($arrSupplyStrings)){
                foreach($arrSupplyStrings as $key => $value){
                    $arrData['supplyDetails'][] = explode(',', $value);
                }
            }
            break;
        case 'lunch_change':
            $userWorkflowModel = new UserWorkFlow();
            $userWorkflow = $userWorkflowModel->getAllOfUserWorkflowById($userTask['user_workflow_id']);
            $arrData['userWorkflow'] = $userWorkflow;
            $arrData['lunchLocation'] = OAConfig::$lunchLocation;
            $arrData['lunchChangeType'] = OAConfig::$lunchChangeType;
            break;
        case 'administrative_apply':
            $userWorkflowModel = new UserWorkFlow();
            $arrData['staff_type']=self::$staff_type;
            break;
        case 'finance_loan':
            $userWorkflowModel = new UserWorkFlow();
            $arrData['pay_types'] = OAConfig::$pay_types;
            break;
        case 'finance_payment':
            $userWorkflowModel = new UserWorkFlow();
            $arrData['pay_types'] = OAConfig::$pay_types;
            $arrData['payment_natures'] = OAConfig::$payment_natures;
            break;
        case 'finance_offset':
            $userWorkflowModel = new UserWorkFlow(); 
            $arrData['payment_natures'] = OAConfig::$payment_natures;
            break;
        default:
            $userWorkflowModel = new UserWorkFlow(); 
            break;

        }
        if($userWorkflowModel)
        {
            $extension = $userWorkflowModel->getExtByUserWorkflow($userTask['obj_type'],$userTask['obj_id']);
            $arrData['extension'] = $extension;
            if($userTask['obj_type'] == 'business_card'){//名片显示部门
                $sectionModel = new Section();  
                $section = $sectionModel->getItemByPk($userTask['section_id']);
                $sections = $sectionModel->getParentSectionBySectionId($section['id']);
                $sections = array_merge(array($section),$sections);
                $arrData['sections'] = $sections;
            }elseif($userTask['obj_type'] == 'team_building'){
                //获取经费金额
                $tbRecordModel = new TeamBuildingRecord();
                $arrData['balance'] = $tbRecordModel->getTotalAmountBySectionId($extension['section_id']);
            }elseif($userTask['obj_type'] == 'team_encourage'){
                //获取经费金额
                $tbRecordModel = new TeamEncourageRecord();
                $arrData['balance'] = $tbRecordModel->getTotalAmountBySectionId($extension['section']);

            }elseif (strpos($userTask['obj_type'], 'ticket') === 0){
                $roleModel = new Role();
                $objRole = $roleModel->getItemByPk($userTask['role_id']);
                $arrData['userTask']['role_name'] = $objRole['name'];
            }elseif($userTask['obj_type'] == 'finance_payment'){
                $arrData['extension']['img_address'] = explode(";",$arrData['extension']['img_address']);
            }
        }
        $userWorkflowModel = new UserWorkFlow(); 
        $userWorkflowItem = $userWorkflowModel->getItemByPk($userTask['user_workflow_id']);
        $arrData['workflow_type'] = OAConfig::$workflow_type;
        $workflowConfig = WorkflowConfig::getWorkFlowConfigByObjType($userWorkflowItem);
        $arrData['workflowConfig'] = $workflowConfig;
        //审批历史
        $arrData['userTasks'] = $model->getItemsByUserWorkflowId($userTask['user_workflow_id']);
        $arrData['absenceType'][VACATION_CHUCHAI] = '出差';
        $arrData['absenceType'][VACATION_YINGONGWAICHU] = '因公外出';
        if( $userTask['obj_type'] == "workorder"){
            $this->renderFile('./workorder_daishenpi_detail.tpl',$arrData);
        }else{
            $this->renderFile('./daishenpi_detail.tpl',$arrData);
        }
    }

    //分发处理
    public function actionWorkorder()
    {
        $user_task_id = Yii::app()->request->getParam('user_task_id');
        $user_workflow_id = Yii::app()->request->getParam('user_workflow_id');
        $obj_id = Yii::app()->request->getParam('obj_id');

        //check role;
        $usertaskModel = new UserTask();
        $usertaskCheck = $usertaskModel->getWorkflowByUserTaskId($user_task_id);
        $this->checkRole($usertaskCheck);

        $objModel = new WorkOrder();
        $objInfo = $objModel->getItemByPk($obj_id);
        if(empty($objInfo)){
            Yii::log('工作流信息获取失败。');
            throw new CHttpException(404,'工作流信息获取失败');
        }

        $userModel = new User();
        $userInfo = $userModel->getItemByPk($objInfo['user_id']);
        if(empty($userInfo)){
            Yii::log('用户信息获取失败。');
            throw new CHttpException(404,'用户信息获取失败');
        }

        $sectionModel = new Section();
        $section = $sectionModel->getItemByPk($userInfo['section_id']);
        if(empty($section)){
            Yii::log('部门信息获取失败。');
            throw new CHttpException(404,'部门信息获取失败');
        }


        $arrData['workorderSolve'] = OAConfig::$workorderSolve;
        $arrData['workorderType'] = OAConfig::$workorderType;
        $arrData['workorderLevel'] = OAConfig::$workorderLevel;
        $arrData['user_workflow_id'] = $user_workflow_id;
        $arrData['obj_id'] = $obj_id;
        $arrData['user'] = $userInfo;
        $arrData['section'] = $section;
        $arrData['obj_info'] = $objInfo;
        $arrData['user_task_id'] = $user_task_id;
        $this->renderFile('./workflow/workorder_issue.tpl',$arrData);
    }

    //产生下一个审批人
    public function actionWorkorderaudit()
    {   
        $obj_id = Yii::app()->request->getParam('obj_id');
        $user_workflow_id = Yii::app()->request->getParam('user_workflow_id');
        $user_task_id = Yii::app()->request->getParam('user_task_id');
        $solver = Yii::app()->request->getParam('solve');
        $audit_status = Yii::app()->request->getParam('audit_status');
        $issue_desc = Yii::app()->request->getParam('issue_desc');
        $user_id = OAConfig::$solver[$solver];

        //check role;
        $usertaskModel = new UserTask();
        $usertaskCheck = $usertaskModel->getWorkflowByUserTaskId($user_task_id);
        $this->checkRole($usertaskCheck);


        $objModel = new WorkOrder();
        $objInfo = $objModel->getItemByPk($obj_id);
        if(empty($objInfo)){
            Yii::log('工作流信息获取失败。');
            throw new CHttpException(404,'工作流信息获取失败');
        }

        if( $objInfo['issue'] == 1){
            throw new CHttpException(404,'任务已分配');
        }else{
            //update issue tag;
            $workorderModel = new WorkOrder();
            $workorderInfo = $workorderModel->getItemByPk($obj_id);
            $workorderInfo['issue'] = 1;
            $workorderInfo['issue_time'] = time();
            $workorderInfo['issue_desc'] = $issue_desc;
            $workorderInfo['issue_id'] = Yii::app()->session['oa_user_id'];
            //$isUpdate = $workorderModel->updateIssueValue($obj_id);
            if( !$workorderModel->save($workorderInfo) ){
                Yii::log('任务分发失败。');
                throw new CHttpException(404,'任务分发失败');
            }
        }

        if($audit_status == OA_TASK_TONGGUO){
            //update user_task status
            $usertaskModel = new UserTask();
            $usertaskItem = $usertaskModel->getItemByPk($user_task_id);
            $usertaskItem['audit_status'] = OA_TASK_TONGGUO;
            $usertaskItem['modify_time'] = time();
            if(!$usertaskModel->save($usertaskItem)){
                Yii::log('user task update failed.');
                throw new CHttpException(404,'user task update failed.');
            }

            $arrData['user_id'] = $user_id; 
            $arrData['user_workflow_id'] = $user_workflow_id;
            $nodeTag = "B";
            $arrData['workflow_step'] = $nodeTag;
            $arrData['create_time'] = time();
            $arrData['status'] = STATUS_VALID;
            $model = new AuditList();
            if(! $model->save($arrData)){
                Yii::log('save audit list  failed.');
                throw new CHttpException(404,'save audit list  failed.');
            }

            $workflowService = new WorkflowService($user_workflow_id);
            $workflowService->workflowProcess();//audit operation, new node
        }else{
            //update user_task status
            $usertaskModel = new UserTask();
            $usertaskItem = $usertaskModel->getItemByPk($user_task_id);
            $usertaskItem['audit_status'] = OA_TASK_JUJUE;
            $usertaskItem['modify_time'] = time();
            if(!$usertaskModel->save($usertaskItem)){
                Yii::log('user task update failed.');
                throw new CHttpException(404,'user task update failed.');
            }

            //update userworkflow
            $userWorkflow['id'] = $user_workflow_id;
            $userWorkflow['audit_status'] = OA_WORKFLOW_AUDIT_REJECT;
            $userWorkflow['modify_time'] = time();
            $userWorkflowModel = new UserWorkFlow(DBModel::MODE_RW);
            if(!$userWorkflowModel->save($userWorkflow)){
                Yii::log('error: update user_workflow fail.');
            }
        }
        $this->redirect(Yii::app()->createURL('userworkflow/my'));

    }


    //审批
    public function actionAudit()
    {
        $id = Yii::app()->request->getParam('user_task_id'); 
        $audit_status = Yii::app()->request->getParam('audit_status');
        $audit_desc = Yii::app()->request->getParam('audit_desc');

        $model = new UserTask();
        $userTask = $model->getWorkflowByUserTaskId($id);
        $roleModel = new Role();
        $objRole = $roleModel->getItemByPk($userTask['role_id']);
        $userTask['role_name'] = $objRole['name'];


        $this->checkRole($userTask);
        $myId = Yii::app()->session['oa_user_id'];
        $arrData['audit_user_id'] = $myId;
        $arrData['audit_desc'] = $audit_desc;

        $workflowModel = new UserWorkFlow();
        $user_workflow_id = $userTask['user_workflow_id'];
        $userWorkflowInfo = $workflowModel->getItemByPk($user_workflow_id);

        $woModel = new WorkOrder();
        $woItem = $woModel->getItemByPk($userWorkflowInfo['obj_id']);

        $statusInfo['status_workflow'] = $userWorkflowInfo['audit_status'];
        $statusInfo['status_task']     = $userTask['audit_status'];
        $this->_validate($statusInfo);

        if($userTask['role_name'] == '票务专员' && strpos($userTask['obj_type'], 'ticket') === 0){
            $arrTicketData = array();
            $arrTicketData['id'] = $userTask['obj_id'];
            $arrTicketData['flight_number'] = Yii::app()->request->getParam('flight_number', '');
            $arrTicketData['flight_price'] = Yii::app()->request->getParam('flight_price', 0);
            $arrTicketData['extra_fee'] = Yii::app()->request->getParam('extra_fee', 0);
            $ticketBookModel = new TicketBook();
            $ticketBookModel->save($arrTicketData);
        }

        if (isset($userTask['audit_desc']) && $userTask['audit_desc'] == 'interviewer'){
            $departmentAssessmentModel = new DepartmentAssessment();
            $arrDepartmentAssessment = Yii::app()->request->getParam('data');
            $arrDepartmentAssessment['entry_id'] = $userTask['obj_id'];
            $arrDepartmentAssessment['user_id'] = $userTask['user_id'];
            $arrDepartmentAssessment['status'] = STATUS_VALID;
            $arrDepartmentAssessment['update_user_id'] = $myId;
            $arrDepartmentAssessment['create_time'] = time();
            $arrDepartmentAssessment['update_time'] = time();
            $departmentAssessmentModel->save($arrDepartmentAssessment);
        }
        if (isset($userTask['audit_desc']) && $userTask['audit_desc'] == 'hr'){
            $hrAssessmentModel = new HrAssessment();
            $arrHrAssessment = Yii::app()->request->getParam('data');
            $arrHrAssessment['entry_id'] = $userTask['obj_id'];
            $arrHrAssessment['user_id'] = $userTask['user_id'];
            $arrHrAssessment['status'] = STATUS_VALID;
            $arrHrAssessment['update_user_id'] = $myId;
            $arrHrAssessment['create_time'] = time();
            $arrHrAssessment['update_time'] = time();
            $hrAssessmentModel->save($arrHrAssessment);
        }
        if($userTask['obj_type'] == 'dimission'){
            $arrNewData = Yii::app()->request->getParam('data');
            $arrNewData['id'] = $userTask['obj_id'];
            if($userTask['workflow_step'] == 'C'){
                $arrNewData['department_receive_time'] = strtotime($arrNewData['department_receive_time']);
                $arrNewData['department_watch_time'] = strtotime($arrNewData['department_watch_time']);
            }elseif($userTask['workflow_step'] == 'D'){
                $arrNewData['system_account_receive_time'] = strtotime($arrNewData['system_account_receive_time']);
            }elseif($userTask['workflow_step'] == 'E'){
                $arrNewData['risk_account_receive_time'] = strtotime($arrNewData['risk_account_receive_time']);
            }elseif($userTask['workflow_step'] == 'F'){
                $arrNewData['mail_account_receive_time'] = strtotime($arrNewData['mail_account_receive_time']);
            }elseif($userTask['workflow_step'] == 'G'){
                $arrNewData['finance_receive_time'] = strtotime($arrNewData['finance_receive_time']);
            }elseif($userTask['workflow_step'] == 'H'){
                $arrNewData['administration_receive_time'] = strtotime($arrNewData['administration_receive_time']);
            }elseif($userTask['workflow_step'] == 'I'){
                $arrNewData['hr_dimission_take_effect_time'] = strtotime($arrNewData['hr_dimission_take_effect_time']);
                $arrNewData['hr_wuxianyijin_stop_time'] = strtotime($arrNewData['hr_wuxianyijin_stop_time']);
                $arrNewData['hr_salary_stop_time'] = strtotime($arrNewData['hr_salary_stop_time']);
                $arrNewData['hr_confirm_time'] = strtotime($arrNewData['hr_confirm_time']);
            }elseif($userTask['workflow_step'] == 'J'){
                $arrNewData['hrd_confirm_time'] = strtotime($arrNewData['hrd_confirm_time']);
            }
            $arrNewData['update_time'] = time();
            $dimissionModel = new Dimission();
            $dimissionModel->save($arrNewData);
        }
        if($audit_status == OA_TASK_TONGGUO){

            //修改该记录状态，生成工作流的下一个节点。更改userworkflow状态 
            $arrData['id'] = $userTask['id'];
            $arrData['audit_status'] = OA_TASK_TONGGUO;
            $arrData['modify_time'] = time();
            $model->save($arrData);

            if($userWorkflowInfo['obj_type'] == 'administrative_expense'){
                $expense_type = Yii::app()->request->getParam('expense_type');
                $expenseModel = new AdministrativeExpense();
                $expenseItem = $expenseModel->getItemByPk($userWorkflowInfo['obj_id']);
                $expenseItem['expense_type'] = $expense_type;
                $expenseModel->save($expenseItem);
            }
            if($userWorkflowInfo['obj_type'] == 'administrative_apply'){

                if($userTask['workflow_step']== 'A'){
                    $staff_type2 = Yii::app()->request->getParam('staff_type');
                    $staff_ver = Yii::app()->request->getParam('staff_ver');
                    $staff_price = Yii::app()->request->getParam('staff_price');
                    $staff_total = Yii::app()->request->getParam('staff_total');
                    $applyModel = new AdministrativeApply();
                    $applyItem = $applyModel->getItemByPk($userWorkflowInfo['obj_id']);
                    $applyItem['staff_type'] = self::$staff_type[$staff_type2];
                    $applyItem['staff_ver'] = $staff_ver;
                    $applyItem['staff_price'] = $staff_price;
                    $applyItem['staff_total'] = $staff_total;
                    $applyModel->save($applyItem);
                    if($applyItem['staff_total'] >= 5000){
                        $userWorkflowInfo['userworkflow_config'] = 'administrative_apply_5000'; 
                        $workflowModel->save($userWorkflowInfo);
                    }
                }
                if($userTask['workflow_step']== 'D'){
                    $commit_time = time();
                    $applyModel = new AdministrativeApply();
                    $applyItem = $applyModel->getItemByPk($userWorkflowInfo['obj_id']);
                    $applyItem['commit_time'] = $commit_time;
                    $applyModel->save($applyItem);
                }
            }
            if($userWorkflowInfo['obj_type'] == 'daily_reimbursement' && $userTask['workflow_step'] == 'B'){
                $dailyReimbursementModel = new DailyReimbursement();
                $dailyReimbursementItem = $dailyReimbursementModel->getItemByPk($userWorkflowInfo['obj_id']);
                $dailyReimbursementItem['amount_check'] = Yii::app()->request->getParam('amount_check');
                $dailyReimbursementModel->save($dailyReimbursementItem);
            }

            if($userWorkflowInfo['obj_type'] == 'business_reimbursement' && $userTask['workflow_step'] == 'B'){
                $businessReimbursementModel = new BusinessReimbursement();
                $businessReimbursementItem = $businessReimbursementModel->getItemByPk($userWorkflowInfo['obj_id']);
                $businessReimbursementItem['amount_check'] = Yii::app()->request->getParam('amount_check');
                $businessReimbursementModel->save($businessReimbursementItem);
            }

            if($userWorkflowInfo['obj_type'] == "workorder"){
                $woItem['end_time'] = time();
                $woItem['handle_id'] = $myId;
                $woModel->save($woItem);
            }
            $auditlistModel = new AuditList();
            $usertaskModel  = new UserTask();
            $auditNum = $auditlistModel->countAstepNumByUserWorkflowId($userWorkflowInfo['id']);
            $taskNum  = $usertaskModel->countAstepAuditNumByUserWorkflowId($userWorkflowInfo['id']);

            $compareRes = false;
            if($auditNum && $taskNum && $auditNum == $taskNum && $userTask['workflow_step'] == "A"){
                $compareRes = true;
            }

            //上线平台走下面的流程
            if($userWorkflowInfo['obj_type'] == "online_order"  && $userWorkflowInfo['deploy_id'] > 0 && $compareRes == true)
            {
                //回调上线平台接口

                $deploy_id = $userWorkflowInfo['deploy_id'];
                $auditListService = new AuditListService();
                $data = $auditListService->onLinePlatformAPI($userWorkflowInfo['obj_type'],$deploy_id,"T");
            }else{
                $workflowService = new WorkflowService($user_workflow_id);
                $workflowService->workflowProcess();
            }


            //判断是否第三方来源
            $sourceInfo = $model->isOnlineOrderSource($id);
            if($sourceInfo) {
                $auditListModel = new AuditList();
                $isOp = $auditListModel->checkingOpDone($user_id, $user_workflow_id);

                //是op签字
                if($isOp) {
                    $data = array();
                    $data['projectId'] = $sourceInfo['source_id'];
                    $rtnArr = $this->toChanDaoFromMessage($data);
                    $projectId = $data['projectId'];
                    $msg = $rtnArr['msg']; 
                    if($rtnArr['ackCode'] == 1) {
                        $msgstr = "source_id:{$projectId} success!";
                    }else {
                        $msgstr = "source_id:{$projectId} {$msg}";
                    }

                    //给禅道系统反馈
                    $dataFile =  Config::$basePath."/oa/config/log/chandao_online.txt";
                    file_put_contents($dataFile, $msgstr."\n", FILE_APPEND);
                }
            }
        }elseif($audit_status == OA_TASK_JUJUE){
            //修改该记录状态，工作流置为结束
            $arrData['id'] = $userTask['id'];
            $arrData['audit_status'] = OA_TASK_JUJUE;
            $arrData['modify_time'] = time();
            $model->save($arrData);

            if($userWorkflowInfo['obj_type'] == "workorder"){
                $woItem['end_time'] = time();
                $woItem['handle_id'] = $myId;
                $woItem['handle_desc'] = $audit_desc;
                $woModel->save($woItem);
            }

            //回调上线平台接口
            $deploy_id = $userWorkflowInfo['deploy_id'];
            $auditListService = new AuditListService();
            $auditListService->onLinePlatformAPI($userWorkflowInfo['obj_type'],$deploy_id,"J");//在里面分析是否要调用接口

            $userWorkflow['id'] = $userTask['user_workflow_id'];
            $userWorkflow['audit_status'] = OA_WORKFLOW_AUDIT_REJECT;
            $userWorkflow['modify_time'] = time();

            $workflowService = new WorkflowService($userTask['user_workflow_id']);
            $workflowService->updateUserWorkflow($userWorkflow);

        }elseif($audit_status == WORKORDER_START){//only for workorder
            $arrData['id'] = $userTask['id'];
            $arrData['audit_status'] = OA_TASK_CHUANGJIAN;
            $arrData['modify_time'] = time();
            $model->save($arrData);

            $woItem['do_time'] = time();
            $woItem['handle_id'] = $myId;
            $woModel->save($woItem);

            $userWorkflow['id'] = $userTask['user_workflow_id'];
            $userWorkflow['audit_status'] = WORKORDER_START;
            $userWorkflow['modify_time'] = time();
            $workflowService = new WorkflowService($userTask['user_workflow_id']);
            $workflowService->updateUserWorkflow($userWorkflow);

        }elseif($audit_status == WORKORDER_PAUSE){//only for workoder

            $arrData['id'] = $userTask['id'];
            $arrData['audit_status'] = OA_TASK_CHUANGJIAN;
            $arrData['modify_time'] = time();
            $model->save($arrData);

            $woItem['pause_time'] = time();
            $woItem['handle_id'] = $myId;
            $woItem['pause_desc'] = $audit_desc;
            $woModel->save($woItem);

            $userWorkflow['id'] = $userTask['user_workflow_id'];
            $userWorkflow['audit_status'] = WORKORDER_PAUSE;
            $userWorkflow['modify_time'] = time();
            $workflowService = new WorkflowService($userTask['user_workflow_id']);
            $workflowService->updateUserWorkflow($userWorkflow);
        }


        if(Yii::app()->request->isAjaxRequest){
            echo json_encode(array('code'=>0));  
            Yii::app()->end();
        }
        $this->redirect(Yii::app()->createURL('usertask/list'));
    }

    //查看当前用户对该条信息是否有权限
    public function checkRole($userTask)
    {
        $myId = Yii::app()->session['oa_user_id'];
        if($userTask['user_id'] != 0){
            if($userTask['user_id'] != $myId){
                throw new CHttpException(404,'您没有权限查看');
            }
        }elseif($userTask['role_id']){
            $roleModel = new Role();
            $roleIds = $roleModel->getRoleIdByUserId($myId);
            if(!in_array($userTask['role_id'],$roleIds)){
                throw new CHttpException(404,'您没有权限查看');
            } 
        }
    }

    //反馈给风控禅道系统
    public function toChanDaoFromMessage($data) {

        //映射禅道系统的参数
        $projectId = intval($data['projectId']);

        if($projectId < 1) {

            return false;

        }

        $url = "http://pms.rong360.com/api/onzentao?type=1&projectId={$projectId}"; //默认type=1
        $res = CurlHttp::get($url);
        $arr = json_decode($res, true);

        return $arr;

    }

    public function createAuditList($userworkflowId,$amount,$config)
    {
        $workflowModel = new UserWorkFlow();
        $userWorkflowInfo = $workflowModel->getItemByPk($userworkflowId);
        $userId = $userWorkflowInfo['user_id'];
        $auditList = array();
        if(!empty($config) && is_array($config)){
            foreach($config as $item){
                if(isset($item['min']) && $amount >= $item['min']){
                    if(is_null($item['max'])){
                        $auditList = $item['audit']; 
                    }elseif($amount < $item['max']){
                        $auditList = $item['audit']; 
                    }    
                }
            }
        }


        if(empty($auditList)){
            Yii::log('administrative expense config fail userId:'.$userId,'error');
        }
        $userModel = new User();
        $auditListModel = new AuditList();
        $arrData = array();
        $arrData['user_workflow_id'] = $userworkflowId;
        $arrData['workflow_step'] = 'C';
        $arrData['create_time'] = time();
        $arrData['status'] = STATUS_VALID;
        foreach($auditList as $key => $name){
            $user = $userModel->getItemByName($name);
            if(empty($user)){
                Yii::log('配置用户名不存在:'.$name,'error'); 
            }
            $arrData['user_id'] = $user['id'];
            $arrData['pos'] = $key+1;
            $id = $auditListModel->save($arrData);
        }
    }


    /**
     * 验证当前流程和任务状态是否正确
     **/
    private function _validate($statusInfo) {

        $status_workflow = $statusInfo['status_workflow'];
        $status_task     = $statusInfo['status_task'];
        $isAjax = false;

        $ret_msg = null;
        $code = 0;//0：正常情况

        if(Yii::app()->request->isAjaxRequest){

            $isAjax = true;

        }

        if($status_workflow == OA_WORKFLOW_AUDIT_CANCEL) {

            $code = OA_WORKFLOW_AUDIT_CANCEL;
            $ret_msg = '该审批状态已经发生改变了!';

        } elseif ($status_task != OA_TASK_CHUANGJIAN) { //当前审核状态不是初始化0

            $code = 1; //1:异常操作
            $ret_msg = '该审批状态已经发生改变了!';

        }

        if($code != 0 || !empty($ret_msg) ) {

            if($isAjax) {

                echo json_encode(array('code'=>$code) );  
                die();
            }

            throw new CHttpException(404, $ret_msg);

        }



    }

}
?>
