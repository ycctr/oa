<?php
class BusinessController extends Controller {

    //出差
    public function actionList() {
        
        $query = Yii::app()->request->getQuery('query');
        $startDay = $query['start_time'];
        $endDay = $query['end_time'];
        $sectionName = $query['section_name'];
        $userName = $query['name'];
        $idCard = $query['id_card'];
        $intPageSize = 50;
        $intPage = intval(Yii::app()->request->getQuery('page')) < 1 ? 1 : intval(Yii::app()->request->getQuery('page'));
        $offset = ($intPage-1)*$intPageSize;
        $model = new Absence(); 
        $arrTplData['arrList'] = $model->getListByDateCondition($startDay,$endDay,$idCard,$sectionName,$userName,$offset,$intPageSize,VACATION_CHUCHAI);

        $arrTplData['arrPager'] = array(
            'count'     => $model->getCountByDateCondition($startDay,$endDay,$idCard,$sectionName,$userName,VACATION_CHUCHAI),
            'pagesize'  => $intPageSize,
            'page'      => $intPage,
            'pagelink'  => empty($query) ? 'list?page=%d' : 'list?' . urldecode(Yii::app()->request->getQueryString()) . '&page=%d',
        );
        $arrTplData['savelink'] = "SaveAsCsv?".urldecode(Yii::app()->request->getQueryString());
        if(empty(Yii::app()->request->getQuery('query')))
        {
            $query['start_time'] = date('Y-m-26',strtotime('-1 month'));
            $query['end_time'] = date('Y-m-25');
            $arrTplData['initTime'] = $query;
        }
        $this->renderFile("business.tpl",$arrTplData);
    } 


    //销假
    public function actionCancel() {

        $arrPrams     = Yii::app()->request->getQuery('query');
        $starDate     = $arrPrams['start_time'];
        $endDate      = $arrPrams['end_time'];
        $starTime     = strtotime( $starDate );
        $endTime      = strtotime( $endDate );
        $name         = $arrPrams['name'];
        $section_name = $arrPrams['section_name'];
        $idCard       = $arrPrams['id_card'];
        $saveCsv      = $arrPrams['saveCsv'];

        //isset第一次访问不希望出这个提示
        if(isset($arrPrams['start_time']) && 
            (empty($starTime) || empty($endTime) ) ) {
            $arrTpl['msg'] = "时间必须填写完整！";
        } 

        if(!empty($starTime) && !empty($endTime) ) {
            $conditionInfo = array(
                'uName'     => $name,
                'sName'     => $section_name,
                "startTime" => $starTime,
                "endTime"   => $endTime,
                "idCard"    => $idCard
                );

            $cancelModel = new CancelBusiness($conditionInfo);
            $data = $cancelModel->getExcelPrint($conditionInfo);
            if(empty($saveCsv)){
                $arrTpl['savelink'] = Yii::app()->request->getUrl()."&query%5BsaveCsv%5D=1";
                $arrTpl['arrList'] = $data; 
                $this->renderFile("cancel_business.tpl",$arrTpl);
                Yii::app()->end();
            } 

            if(empty($data) ) {
                $arrTpl['msg'] = "当前操作没有任何数据可导出！";
            }else {
                $data = array_values($data);

                $excelInfo = array(
                    'file'  => "销出差{$starDate}-{$endDate}.xls",
                    'title' => '销出差导出',
                    'excelTitle' => array(
                        '员工号','姓名', '部门', '开始时间','结束时间', '销出差天数', '销出差原因','审核状态' ),
                    'res'   => $data,
                    
                    );

                $this->printExcel($excelInfo);
                Yii::app()->end();
            }

        }else{
            $query['start_time'] = date('Y-m-26',strtotime('-1 month'));
            $query['end_time'] = date('Y-m-25');
            $arrTpl['initTime'] = $query;
            $this->renderFile("cancel_business.tpl",$arrTpl);
        }
    } 


    public function actionSpellSql()
    {
        //$model = new Absence();
        $arrQuery = Yii::app()->request->getQuery('query');
        if (!empty($arrQuery))
        {  
            if( $arrQuery['start_time'] == "" || $arrQuery['end_time'] == "")
            {
                //self::actionInValidate("时间为必输入项，请检查是否输入完整。");
                return "1=1";
            }
           $where = "absence.start_time >=". self::translateTime($arrQuery['start_time'])." and absence.start_time <".(self::translateTime($arrQuery['end_time']) + 86400)." ";
           $id = "";
           if(!empty($arrQuery['name']))
           {
               $model = new User();
               $ret = $model->getIdsByName($arrQuery['name']);
               // $ret = (array)$model->db()->query("select id from user where name='".$arrQuery['name']."'");
                if(!empty($ret))
                {
                   for($intCurrent = 0;$intCurrent < (count($ret) - 1);$intCurrent++)
                       $id = $id.$ret[$intCurrent]['id'].",";
                    $id = $id.$ret[count($ret) - 1]['id'];
                }
                else
                {
                    return "1=2";
                }
           }
           if(!empty($arrQuery['section_name']))
           {
               $model = new Section();
               $ret =  $model->getUserIdsBySectionName($arrQuery['section_name']);
               //$ret = $model->db()->query("select user.id from user,section where user.section_id = section.id and section.name='".$arrQuery['section_name']."'");
               if(!empty($ret))
               {
                   if($id != "")
                       $id = $id.",";
                   for($intCurrent = 0;$intCurrent < (count($ret) - 1);$intCurrent++)
                       $id = $id.$ret[$intCurrent]['id'].",";
                    $id = $id.$ret[count($ret) - 1]['id'];
               }
               else{
                    return "1=2";
               }
           }
           if($id != "")
               $where = $where." and absence.user_id in (".$id.")";
        }
        else
        {
            $time = self::GetInitTimeSpanValue();
            $where = "absence.start_time >=". strtotime($time['start_time_str'])." and absence.start_time <".(strtotime($time['end_time_str']) + 86400)." ";
        }
        $where .= " AND type= 13 and user_workflow.audit_status != ".OA_WORKFLOW_AUDIT_CHUANGJIAN." and user_workflow.audit_status != ". OA_WORKFLOW_AUDIT_CAOGAO;
        return $where;
    }


    public function GetInitTimeSpanValue()
    {
        $date = 26;

        $month = date("m");
        $month_start = (date("d")>=26) ? $month : ( ($month==12) ? 1:($month-1) );
        $month_end = (date("d")>=26) ? ( ($month==12) ? 1:($month+1)) : $month;

        $year = date("y");
        $year_start = ($month_start != $month)&&($month_start==12) ? ($year-1) : ($year);
        $year_end = ($month_end != $month)&&($month==12) ? ($year+1) : ($year);
        
        if($month_start < 10)
            $month_start = "0".intval($month_start);
        if($month_end < 10)
            $month_end = "0".intval($month_end);
        
        $time['start_time'] = "20".$year_start."-".$month_start."-26";
        $time['end_time'] = "25/".$month_end."/20".$year_end;
        $time['end_time'] = "20".$year_end."-".$month_end."-25";
        $time['start_time_str'] = "20".$year_start."-".$month_start."-26";
        $time['end_time_str'] = "20".$year_end."-".$month_end."-25";
        return $time;
    }

    /**
     * 保存csv
     **/
    public function actionSaveAsCsv() {
        $query = Yii::app()->request->getQuery('query');
        $startDay = $query['start_time'];
        $endDay = $query['end_time'];
        $sectionName = $query['section_name'];
        $userName = $query['name'];
        $idCard = $query['id_card'];
        $model = new Absence(); 
        $dataList = $model->getListByDateCondition($startDay,$endDay,$idCard,$sectionName,$userName,$offset,$intPageSize,VACATION_CHUCHAI);

        header('Content-Type: application/csv');

        $arrQuery = Yii::app()->request->getQuery('query');
        if(empty(Yii::app()->request->getQuery('query')))
        {
            $time = self::GetInitTimeSpanValue();  
            $filename = "出差统计(".$time['start_time_str']."-".$time['end_time_str'].").csv";
        }
        else
        {
            $filename = "出差统计(".date('Y/m/d', strtotime($arrQuery['start_time']))."-".date('Y/m/d', strtotime($arrQuery['end_time'])).").csv";
        }

        header("Content-Disposition: business; filename=".$filename."");
        
        $csvTitle = array( '工号','姓名','部门','开始时间','结束时间','出差天数','出差地址','出差原因','审核状态' );
        $output = fopen('php://output','w') or die("Can't open php://output");
    
       for($intNum = 0; $intNum< 9;$intNum++){
            $csvTitle[$intNum] = iconv("UTF-8","gbk",$csvTitle[$intNum]);
        }

        fputcsv($output,$csvTitle);


        foreach ($dataList as  $val){

            $index = 0;
            $data = array();

            $data[1] = (string)iconv("UTF-8","gbk",$val['id_card']);
            $data[2] = (string)iconv("UTF-8","gbk",$val['name']);
            $data[3] = (string)iconv("UTF-8","gbk",$val['section_name']);
            $data[4] = iconv("UTF-8","gbk",date('Y-m-d', $val['start_time']).OAConfig::$time_node[date('H:i:s', $val['start_time'])]);
            $data[5] = iconv("UTF-8","gbk",date('Y-m-d', $val['end_time']).OAConfig::$time_node[date('H:i:s', $val['end_time'])]);
            $data[6] = (string)iconv("UTF-8","gbk",$val['day_number']);
            $data[7] = (string)iconv("UTF-8","gbk",$val['city']);
            $data[8] = (string)iconv("UTF-8","gbk",$val['leave_reason']);

            switch($val['audit_status']) {

                case OA_WORKFLOW_AUDIT_CHUANGJIAN:
                    $data[9] = iconv("UTF-8","gbk","创建工作流");break;
                case OA_WORKFLOW_AUDIT_SHENPIZHONG:
                    $data[9] = iconv("UTF-8","gbk","审批中");break;
                case OA_WORKFLOW_AUDIT_PASS:
                    $data[9] = iconv("UTF-8","gbk","通过");break;
                case OA_WORKFLOW_AUDIT_REJECT:
                    $data[9] = iconv("UTF-8","gbk","拒绝");break;
                case OA_WORKFLOW_AUDIT_CANCEL:
                    $data[9] = iconv("UTF-8","gbk","取消审核");break;
            }

		    fputcsv($output,$data);	

		}

        fclose($output) or die("Can't close php://output");            

    } 

    public function translateTime($time)
    {
        $val = strtotime($time);
        if($val)
            return $val;
        $arr = explode('/',$time);
        $tmp = $arr[2]."-".$arr[1]."-".$arr[0];
        $val = strtotime($tmp);
        return $val;
    }


    function printExcel($excelInfo) {

        $data = $excelInfo['res'];
        $file = $excelInfo['file'];
        $excelTitle =  $excelInfo['excelTitle'];
        $cols = count($excelTitle);
        $title = $excelInfo['title'];

        $arrLetter = array();

        for ($i = 65; $i < 65+$cols; $i++){

            $arrLetter[] = chr($i); 

        }

        set_include_path(get_include_path() . PATH_SEPARATOR . Config :: $basePath . "/trunk/rong360/shared/excel/");
        include_once 'PHPExcel.php';
        include_once 'PHPExcel/Writer/Excel5.php';

        $objPHPExcel = new PHPExcel(); 

        $objPHPExcel->getProperties()->setCreator("融360");
        $objPHPExcel->getProperties()->setLastModifiedBy("融360");
        $objPHPExcel->getProperties()->setTitle($title);
        $objPHPExcel->getProperties()->setSubject($title);
        $objPHPExcel->getProperties()->setDescription($title);

        $objPHPExcel->setActiveSheetIndex(0);

        foreach($excelTitle as $k => $title){

            $objPHPExcel->getActiveSheet()->SetCellValue($arrLetter[$k].'1',$title); 

        }

        foreach($data as $n => $val){
            $index = 0;
            foreach($val as $item){
                $key = $arrLetter[$index].($n+2);
                if($arrLetter[$index] == 'A'){
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit($key,$item,PHPExcel_Cell_DataType::TYPE_STRING); 
                }else{
                    $objPHPExcel->getActiveSheet()->SetCellValue($key,$item); 
                }
                $index++;
            }
        }

        $company = $file;
        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
        header("Content-Type: application/force-download"); 
        header("Content-Type: application/octet-stream"); 
        header("Content-Type: application/download"); 
        header('Content-Disposition:inline;filename="'.$file.'"'); 
        header("Content-Transfer-Encoding: binary"); 
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
        header("Pragma: no-cache"); 
        $objWriter->save("php://output");

    }


}
