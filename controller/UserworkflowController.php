<?php
// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\Exception;
//
// require dirname(__FILE__)."/../common/shared/phpmailer/autoload.php";

class UserworkflowController extends Controller
{
    public static $conTypes = array(
        1 => 'WFOE-北京融联世纪信息技术有限公司',
        2 => 'ICP-北京融世纪信息技术有限公司',
        3 => '北京融汇金融信息服务有限公司',
        4 =>'其他'   
    );
    public static $paymentMethods = array(
        1 => '银行电汇',
        2 => '支票',
        3 => '现金',
        4 => '其他'   
    );

    public function createAbsence()
    {
        if(!empty($_POST['leave_reason']) && !empty($_POST['start_day']) && !empty($_POST['end_day']) && !empty($_POST['start_node']) && !empty($_POST['end_node'])){
            $btime = Yii::app()->request->getParam('start_day');
            $etime = Yii::app()->request->getParam('end_day');
            $start_node = Yii::app()->request->getParam('start_node');
            $end_node = Yii::app()->request->getParam('end_node');
            $type = Yii::app()->request->getParam('type');
            $city = Yii::app()->request->getParam('city');


            $start_time = OAConfig::nodeToTimeStamp($btime, $start_node);
            $end_time = OAConfig::nodeToTimeStamp($etime, $end_node);
            $dayNum = $this->calculateDays($btime, $etime, $start_node, $end_node, $type);

            if(!$this->checkQingjiaDate($start_time,$end_time,$type)){
                //报错。说明补假超过当前考勤月了 
                throw new CHttpException(404,'请假最多可提前30日申请，补假, 补出差, 补因公外出只能在当前考勤月!');
            }

            if(!$this->calculateSpanPhp($btime,$etime,$start_node,$end_node,$type)){
                //报错。说明已有请假记录 
                throw new CHttpException(404,'已有请假记录，不能再次申请');
            }
            if($dayNum == 0){
                //报错。说明请假天数为0 
                throw new CHttpException(404,'请假天数不能为零');
            }

            $time = time(); 
            $arrData = array();
            $arrData['user_id'] = Yii::app()->session['oa_user_id'];
            $arrData['create_time'] = $time;
            $arrData['start_time'] = $start_time;
            $arrData['end_time'] = $end_time;
            $arrData['status'] = STATUS_VALID;
            $arrData['day_number'] = $dayNum;
            $arrData['leave_reason'] = $_POST['leave_reason'];
            $arrData['type'] = $_POST['type'];
            $arrData['title'] = $_POST['title'];
            $arrData['city'] = $city;
            $model = new Absence();
            $id = $model->save($arrData);

            if($id < 1){
                Yii::log('absence save fail:'.print_r($arrData,true));
                throw new CHttpException(404,'报错数据失败,请重新提交');
            }
            return $id;
        }else{
            //报错，资料不全
            throw new CHttpException(404,'请将资料补充完整');
        }
    }

    public function createWorkOrder()
    {
        if(!empty($_POST['problem_reason']) )
		{
            $arrData = array();
            $arrData['user_id'] = Yii::app()->session['oa_user_id'];
            $arrData['create_time'] = time();
            $arrData['status'] = STATUS_VALID;
            $arrData['problem_reason'] = $_POST['problem_reason'];
            $arrData['type'] = $_POST['type'];
            $arrData['level'] = $_POST['level'];
            $model = new WorkOrder();
            $id = $model->save($arrData);
            if($id < 1){
                Yii::log('workorder save fail:'.print_r($arrData,true));
                throw new CHttpException(404,'报错数据失败,请重新提交');
            }
            return $id;
        }else{
            //报错，资料不全
            throw new CHttpException(404,'请将资料补充完整');
        }
    }

    public function createPappVpn()
    {
        if(!empty($_POST['reason']) )
		{
            $arrData = array();
            $arrData['user_id'] = Yii::app()->session['oa_user_id'];
            $arrData['create_time'] = time();
            $arrData['status'] = STATUS_VALID;
            $arrData['reason'] = $_POST['reason'];
			$arrData['type']   = $_POST['type'];
            $model = new PappVpn();
            $id = $model->save($arrData);
            if($id < 1){
                Yii::log('workorder save fail:'.print_r($arrData,true));
                throw new CHttpException(404,'报错数据失败,请重新提交');
            }
            return $id;
        }else{
            //报错，资料不全
            throw new CHttpException(404,'请将资料补充完整');
        }
    }

    public function createEntry()
    {
        $sectionModel = new Section();
        $userModel = new User();

        $arrData = array();
        $arrData['name'] = Yii::app()->request->getParam('employeeName');
        $arrData['name_spell'] = Yii::app()->request->getParam('nameSpell');
        $arrData['mobile'] = Yii::app()->request->getParam('mobile');
        $arrData['approx_arrive_time'] = Yii::app()->request->getParam('approxArriveTime');
        $arrData['approx_arrive_time'] = strtotime($arrData['approx_arrive_time']);
        $arrData['interview_time'] = Yii::app()->request->getParam('interviewTime');
        $arrData['interview_time'] = strtotime($arrData['interview_time']);
        $arrData['workplace'] = Yii::app()->request->getParam('workplace');
        $arrData['private_email'] = Yii::app()->request->getParam('privateEmail');
        $arrData['resume_source'] = Yii::app()->request->getParam('resumeSource');
        $arrData['hire_type'] = Yii::app()->request->getParam('hireType');
        $arrData['business_line_id'] = Yii::app()->request->getParam('businessLineId');
        $arrData['section_id'] = Yii::app()->request->getParam('sectionId');
        $arrData['subsection_id'] = Yii::app()->request->getParam('subsectionId');
        $arrData['manager_id'] = Yii::app()->request->getParam('managerId');
        $arrData['position'] = Yii::app()->request->getParam('position');
        $arrData['level'] = Yii::app()->request->getParam('level');
        $arrData['salary'] = Yii::app()->request->getParam('salary');
        $arrData['stock_option'] = Yii::app()->request->getParam('stockOption');
        $arrData['remark'] = Yii::app()->request->getParam('remark');
        $arrData['user_id'] = Yii::app()->session['oa_user_id'];
        $arrData['update_user_id'] = Yii::app()->session['oa_user_id'];
        $arrData['status'] = STATUS_VALID;
        $arrData['create_time'] = time();
        $arrData['update_time'] = time();

        if(empty($arrData['name'])
            || empty($arrData['mobile'])
            || empty($arrData['approx_arrive_time'])
            || empty($arrData['private_email'])
            || empty($arrData['salary'])){
            //信息填写不完整
            throw new CHttpException(404,'请将资料补充完整');
        }

        $auditList = Yii::app()->request->getParam('auditlist');
        $auditList['A']['interviewer'] = OAConfig::checkUserAccount($auditList['A']['interviewer']);
        $auditList['A']['hr'] = OAConfig::checkUserAccount($auditList['A']['hr']);

        //找到hrd
        $hrd = '';
        $roleModel = new Role();
        $hrdRole = $roleModel->getItemByName('人力主管');
        $userRoleModel = new UserRole();
        $hrds = $userRoleModel->getUserIdsByRoleId($hrdRole['id']);
        if(isset($hrds[0])){
            $hrd = $userModel->getItemByPk($hrds[0]);
            $hrd = $hrd['email'];
        }

        //找到vp
        $vp = $userModel->getItemByPk(Yii::app()->request->getParam('VPId'));
        $vp = $vp['email'];
        $tmpBusinessLineManager = $businessLineManager;
//        $vp = '';
//        while(!$vp){
//            if($tmpBusinessLineManager['level'] == 0) {
//                Yii::log('entry save fail:'.print_r($arrData,true));
//                throw new CHttpException(404,'请更新相关经理职级');
//            }elseif($tmpBusinessLineManager['level'] < AUDIT_VP){
//                $tmpSection = $sectionModel->getItemByPk($tmpBusinessLineManager['section_id']);
//                $tmpManager = $userModel->getItemByPk($tmpSection['manager_id']);
//                $tmpBusinessLineManager = $tmpManager;
//            }elseif ($tmpBusinessLineManager['level'] == AUDIT_VP){
//                $vp = $tmpBusinessLineManager['email'];
//            }else{
//                $vp = '';
//                break;
//            }
//        }
        //找到ceo
        $ceo = $userModel->getCeo();
        $ceo = isset($ceo[0]) ? $ceo[0]['email'] : '';

        //得到各级经理
        $arrManagersTmp = array();
        //直属经理
        $directManager = $userModel->getItemByPk($arrData['manager_id']);
        $directManager = $directManager['email'];
        $arrManagersTmp['directManager'] = $directManager;
        //分部经理
        $subSection = $sectionModel->getItemByPk($arrData['subsection_id']);
        $subSectionManager = $userModel->getItemByPk($subSection['manager_id']);
        $subSectionManager = $subSectionManager['email'];
        $arrManagersTmp['subSectionManager'] = $subSectionManager;
        //部门经理
        $section = $sectionModel->getItemByPk($arrData['section_id']);
        $sectionManager = $userModel->getItemByPk($section['manager_id']);
        $sectionManager = $sectionManager['email'];
        $arrManagersTmp['sectionManager'] = $sectionManager;
        //业务线经理
        $businessLine = $sectionModel->getItemByPk($arrData['business_line_id']);
        $businessLineManagerId = $businessLine['manager_id'];
        $businessLineManager = $userModel->getItemByPk($businessLineManagerId);
        $arrManagersTmp['businessLineManager'] = $businessLineManager['email'];

        $arrManagers = array();
        foreach ($arrManagersTmp as $key => $value){
            if($value && $value != $hrd && $value != $vp && $value != $ceo && !in_array($value, $arrManagers)){
                $arrManagers[$key] = $value;
            }
        }
        if($hrd && $hrd != $vp && $hrd != $ceo){
            $arrManagers['HRD'] = $hrd;
        }
        if($vp && $vp != $ceo){
            $arrManagers['VP'] = $vp;
        }
        if($ceo){
            $arrManagers['CEO'] = $ceo;
        }

        foreach ($arrManagers as $key => $value){
            $auditList['A'][$key] = $value;
        }


        $_POST['auditlist'] = $auditList;

        $entryModel = new Entry();
        $id = $entryModel->save($arrData);
        if($id < 1){
            Yii::log('entry save fail:'.print_r($arrData,true));
            throw new CHttpException(404,'保存数据失败,请检查数据类型并重新提交');
        }
        return $id;
    }

    public function createDimission()
    {
        if(!empty($_POST['leave_reason']) )
        {
            $arrData = array();
            $arrData['user_id'] = Yii::app()->session['oa_user_id'];
            $userModel = new User();
            $objUser = $userModel->getItemByPk($arrData['user_id']);
            $arrData['create_time'] = time();
            $arrData['update_time'] = time();
            $arrData['status'] = STATUS_VALID;
            $arrData['leave_reason'] = $_POST['leave_reason'];
            $arrData['position'] = $_POST['position'];
            $dimissionModel = new Dimission();
            $id = $dimissionModel->save($arrData);
            if($id < 1){
                Yii::log('dimission save fail:'.print_r($arrData,true));
                throw new CHttpException(404,'报错数据失败,请重新提交');
            }
            $_POST['auditlist'] = array("K" => array("self" => $objUser['name']));
            return $id;
        }else{
            //报错，资料不全
            throw new CHttpException(404,'请将资料补充完整');
        }
    }

    public function createTicketBook()
    {
        $arrData = Yii::app()->request->getParam('data');
        $status = Yii::app()->request->getParam('status','1');
        $workflow_id = Yii::app()->request->getParam('workflow_id');
        $workflowModel = new WorkFlow();
        $workflow = $workflowModel->getItemByPk($workflow_id);
        $arrData['user_id'] = Yii::app()->session['oa_user_id'];
        $arrData['trip_date'] = strtotime($arrData['trip_date']);
        $arrData['create_time'] = time();
        $arrData['update_time'] = time();
        $arrData['status'] = $status;
        /*
        if($workflow['obj_type'] == 'ticket_book'){
            $arrData['status'] = TICKET_BOOK;
        }elseif ($workflow['obj_type'] == 'ticket_refund'){
            $arrData['status'] = TICKET_REFUND;
        }elseif ($workflow['obj_type'] == 'ticket_endorse'){
            $arrData['status'] = TICKET_ENDORSE;
        }
         */
        $ticketBookModel = new TicketBook();
        $id = $ticketBookModel->save($arrData);
        if($id < 1){
            Yii::log('ticket_book save fail:'.print_r($arrData,true));
            throw new CHttpException(404,'报错数据失败,请重新提交');
        }
        return $id;
    }

    public function createPappDB()
    {
        if(!empty($_POST['reason']) && !empty($_POST['dbs_name']) && !empty($_POST['tables_name']) )
		{
            $arrData = array();
            $arrData['user_id'] = Yii::app()->session['oa_user_id'];
            $arrData['create_time'] = time();
			$arrData['dbs_name'] = $_POST['dbs_name'];
			$arrData['tables_name'] = $_POST['tables_name'];
            $arrData['status'] = STATUS_VALID;
            $arrData['reason'] = $_POST['reason'];
            $model = new PappDB();
            $id = $model->save($arrData);
            if($id < 1){
                Yii::log('workorder save fail:'.print_r($arrData,true));
                throw new CHttpException(404,'报错数据失败,请重新提交');
            }
            return $id;
        }else{
            //报错，资料不全
            throw new CHttpException(404,'请将资料补充完整');
        }
    }

    public function createOfficeSupply(){
        if(isset($_POST['supply_type'])){
            $arrData = array();
            $arrData['user_id'] = Yii::app()->session['oa_user_id'];
            $arrData['create_time'] = $arrData['update_time'] = time();
            $arrData['status'] = STATUS_VALID;
            $arrData['office_location'] = Yii::app()->request->getParam('office_location');
            $arrData['other_location'] = Yii::app()->request->getParam('other_location');
            $officeSupplyModel = new OfficeSupply();
            $arrSupplyDetails = array();
//            for($i = 0; $i < count($_POST['supply_type']); ++$i){
//                $strSupplyDetail = $_POST['supply_type'][$i] . ',' . $_POST['supply_detail'][$i] . ',' . $_POST['supply_amount'][$i];
//                $arrSupplyDetails[] = $strSupplyDetail;
//            }
            foreach ($_POST['supply_type'] as $key => $value){
                $strSupplyDetail = $_POST['supply_type'][$key] . ',' . $_POST['supply_detail'][$key] . ',' . $_POST['supply_amount'][$key];
                $arrSupplyDetails[] = $strSupplyDetail;
            }
            $arrData['supply_type_detail_amount'] = implode('|', $arrSupplyDetails);
            $id = $officeSupplyModel->save($arrData);
            if($id < 1){
                Yii::log('office_supply save fail:'.print_r($arrData,true));
                throw new CHttpException(404,'报错数据失败,请重新提交');
            }
            return $id;
        }else{
            //报错，资料不全
            throw new CHttpException(404,'请将资料补充完整');
        }
    }

    public function createLunchChange(){
        if(isset($_POST['lunch_location'])){
            $today = date('d');
            if($today < 25 || $today > 30){
                throw new CHttpException(404,'未在规定日期内提交申请！');
                return; 
            }
            $arrData = array();
            $arrData['user_id'] = Yii::app()->session['oa_user_id'];
            $arrData['create_time'] = $arrData['update_time'] = time();
            $arrData['status'] = STATUS_VALID;
            $arrData['lunch_location'] = Yii::app()->request->getParam('lunch_location');
            $arrData['lunch_change_type'] = Yii::app()->request->getParam('lunch_change_type');
            $lunchChangeModel = new LunchChange();
            $id = $lunchChangeModel->save($arrData);
            if($id < 1){
                Yii::log('lunch_change save fail:'.print_r($arrData,true));
                throw new CHttpException(404,'报错数据失败,请重新提交');
            }
            return $id;
        }else{
            //报错，资料不全
            throw new CHttpException(404,'请将资料补充完整');
        }
    }

    public function createDailyReimbursement()
    {
        $rows = $_POST['row'];
        $parent_id = '';
        $model = new DailyReimbursement();
        $url = Yii::app()->request->getUrlReferrer();
        if(!empty($rows)){
            foreach($rows as $row){
                $arrData = array();
                if(empty($row['type'])){ //认为此行是空行
                    continue;
                }
                $row['parent_id'] = $parent_id;
                $row['user_id'] = Yii::app()->session['oa_user_id'];
                $row['create_time'] = time();
                $row['status'] = STATUS_VALID;
                if($row['type'] == REIMBURSEMENT_TYPE_TAXI){
                    if(empty($row['date']) || empty($row['start_point']) || empty($row['end_point']) || empty($row['time']) || empty($row['amount'])){
                        $msg = '表格信息填写错误，请重新填写';
                        $this->redirect($url);
                    }
                    $row['date_time'] = strtotime($row['date'].$row['time']);
                    $id = $model->save($row);
                }elseif($row['type'] == REIMBURSEMENT_TYPE_ENTERTAINMENT){
                    if(empty($row['date']) || empty($row['address']) || empty($row['customer_unit']) || empty($row['customer_name']) || empty($row['customer_name'])){
                        $msg = '表格信息填写错误，请重新填写';
                        $this->redirect($url);
                    }
                    $row['date_time'] = strtotime($row['date']);
                    $id = $model->save($row);
                }else{
                    if(empty($row['amount']) || empty($row['remark'])){
                        $msg = '表格信息填写错误，请重新填写';
                        $this->redirect($url);
                    }
                    $id = $model->save($row);
                }
                if($parent_id < 1 && $id > 0){
                    $parent_id = $id;
                }
            }
            if($parent_id < 1){
                Yii::log('daily_reimbursement save fail:'.print_r($_POST,true));
                throw new CHttpException(404,'保存数据失败,请重新提交');
            }
            return $parent_id;
        }else{
            $this->redirect($url);
        }
    }

    public function createBusinessAdvance()
    {
        $rows = $_POST['row'];
        $parent_id = '';
        $model = new BusinessAdvance();
        $url = Yii::app()->request->getUrlReferrer();
        if(!empty($rows)){
            foreach($rows as $row){
                $arrData = array();
                if(empty($row['type'])){ //认为此行是空行
                    continue;
                }
                $row['card_no'] = Yii::app()->request->getParam('card_no');
                $row['business_place'] = Yii::app()->request->getParam('business_place');
                $row['business_purpose'] = Yii::app()->request->getParam('business_purpose');
                $row['start_day'] = strtotime(Yii::app()->request->getParam('start_day'));
                $row['end_day'] = strtotime(Yii::app()->request->getParam('end_day'));
                $row['start_node'] = Yii::app()->request->getParam('start_node');
                $row['end_node'] = Yii::app()->request->getParam('end_node');
                $row['parent_id'] = $parent_id;
                $row['user_id'] = Yii::app()->session['oa_user_id'];
                $row['create_time'] = time();
                $row['status'] = STATUS_VALID;
                if($row['type'] == BUSINESS_ADVANCE_TYPE_RENT){
                    if(empty($row['amount']) || empty($row['standard']) || empty($row['day_no'])){
                        $msg = '表格信息填写错误，请重新填写';
                        $this->redirect($url);
                    }
                    $id = $model->save($row);
                }elseif($row['type'] == BUSINESS_ADVANCE_TYPE_ENTERTAINMENT){
                    if(empty($row['amount']) || empty($row['day_no']) || empty($row['standard'])){
                        $msg = '表格信息填写错误，请重新填写';
                        $this->redirect($url);
                    }
                    $id = $model->save($row);
                }else{
                    if(empty($row['amount'])){
                        $msg = '表格信息填写错误，请重新填写';
                        $this->redirect($url);
                    }
                    $id = $model->save($row);
                }
                if($parent_id < 1 && $id > 0){
                    $parent_id = $id;
                }
            }
            if($parent_id < 1){
                Yii::log('businsess_advance save fail:'.print_r($_POST,true));
                throw new CHttpException(404,'保存数据失败,请重新提交');
            }
            return $parent_id;
        }else{
            $this->redirect($url);
        }
    }

    public function createBusinessReimbursement()
    {
        $rows = $_POST['row'];
        $parent_id = '';
        $model = new BusinessReimbursement();
        $url = Yii::app()->request->getUrlReferrer();
        if(!empty($rows)){
            foreach($rows as $row){
                $arrData = array();
                if(empty($row['type'])){ //认为此行是空行
                    continue;
                }
                $row['bstart_day'] = strtotime(Yii::app()->request->getParam('start_day'));
                $row['bend_day'] = strtotime(Yii::app()->request->getParam('end_day'));
                $row['bstart_node'] = Yii::app()->request->getParam('start_node');
                $row['bend_node'] = Yii::app()->request->getParam('end_node');
                $row['card_no'] = Yii::app()->request->getParam('card_no');
                $row['business_place'] = Yii::app()->request->getParam('business_place');
                $row['business_purpose'] = Yii::app()->request->getParam('business_purpose');
                $row['parent_id'] = $parent_id;
                $row['user_id'] = Yii::app()->session['oa_user_id'];
                $row['create_time'] = time();
                $row['status'] = STATUS_VALID;
                if($row['type'] == BUSINESS_REIMBURSEMENT_TYPE_TRAFFIC){
                    if(empty($row['date']) || empty($row['vehicle']) || empty($row['start_point']) || empty($row['end_point']) || empty($row['time']) || empty($row['amount'])){
                        $msg = '表格信息填写错误，请重新填写';
                        $this->redirect($url);
                    }
                    $row['date'] = strtotime($row['date']);
                    $row['time'] = strtotime($row['time']);
                    $id = $model->save($row);
                }elseif($row['type'] == BUSINESS_REIMBURSEMENT_TYPE_TAXI){
                    if(empty($row['amount'])){
                        $msg = '表格信息填写错误，请重新填写';
                        $this->redirect($url);
                    }
                    $id = $model->save($row);
                }elseif($row['type'] == BUSINESS_REIMBURSEMENT_TYPE_RENT){
                    if(empty($row['start_day']) ||empty($row['end_day']) || empty($row['start_node']) || empty($row['end_node']) || empty($row['hname']) || empty($row['amount'])){
                        $msg = '表格信息填写错误，请重新填写';
                        $this->redirect($url);
                    }
                    $row['start_day'] = strtotime($row['start_day']);
                    $row['end_day'] = strtotime($row['end_day']);

                    $id = $model->save($row);
                }else{
                    if(empty($row['date']) || empty($row['hname']) ||empty($row['goal']) || empty($row['company']) ||empty($row['name']) || empty($row['participant']) || empty($row['amount'])){
                        $msg = '表格信息填写错误，请重新填写';
                        $this->redirect($url);
                    }
                    $row['date'] = strtotime($row['date']);
                    $id = $model->save($row);
                }
                if($parent_id < 1 && $id > 0){
                    $parent_id = $id;
                }
            }
            if($parent_id < 1){
                Yii::log('businsess_reimbursement save fail:'.print_r($_POST,true));
                throw new CHttpException(404,'保存数据失败,请重新提交');
            }
            return $parent_id;
        }else{
            $this->redirect($url);
        }
    }

    public function createFinanceLoan(){
        if($_POST['con_type']!=0){
            $user = Yii::app()->params['user'];
            if(Yii::app()->request->getParam('pay_type') == 3){
                $sectionModel = new Section();
                $sectionId = $sectionModel->getItemByPk($user['section_id'])['id'];
                $sections = array();
                $parentSections = $sectionModel->getParentSectionBySectionId($sectionId);
                foreach ($parentSections as $section) {
                    array_push($sections,$section['id']);
                }
                $sections = array_merge(array($sectionId),$sections);
                if(!(array_intersect($sections,array(132)))){
                    throw new CHttpException(404,'您没有权限发起票务类借款申请');
                }
            }
            $arrData['user_id'] = $user['id'];
            $con_type = Yii::app()->request->getParam('con_type');
            if($con_type == 4){
                $arrData['con_type'] = Yii::app()->request->getParam('con_type_other');
            }else{
                $arrData['con_type'] = self::$conTypes[$con_type];
            }
            $arrData['summary'] = nl2br(Yii::app()->request->getParam('summary'));
            $arrData['amount'] = Yii::app()->request->getParam('amount');
            $arrData['pay_type'] = Yii::app()->request->getParam('pay_type');
            $arrData['bank_account'] = Yii::app()->request->getParam('bank_account');
            $arrData['account_name'] = Yii::app()->request->getParam('account_name');
            $arrData['account_number'] = Yii::app()->request->getParam('account_number');
            $arrData['remark'] = nl2br(Yii::app()->request->getParam('remark'));
            $arrData['status'] = STATUS_VALID;
            $arrData['create_time'] = time();
            $model = new FinanceLoan();
            $id = $model->save($arrData);
            if($id < 1){
                Yii::log('finance_loan save fail:'.print_r($_POST,true));
                throw new CHttpException(404,'保存数据失败,请重新提交');
            }
            return $id;
        }else{
            //报错，资料不全
            throw new CHttpException(404,'请将资料补充完整');
        }
    }

    public function createFinancePayment(){
        if($_POST['con_type']!=0 && $_POST['payment_method']!=0 && $_POST['payment_nature']!=0){
            $user = Yii::app()->params['user'];
            if(Yii::app()->request->getParam('pay_type') == 3){
                $sectionModel = new Section();
                $sectionId = $sectionModel->getItemByPk($user['section_id'])['id'];
                $sections = array();
                $parentSections = $sectionModel->getParentSectionBySectionId($sectionId);
                foreach ($parentSections as $section) {
                    array_push($sections,$section['id']);
                }
                $sections = array_merge(array($sectionId),$sections);
                if(!(array_intersect($sections,array(132)))){
                    throw new CHttpException(404,'您没有权限发起票务类借款申请');
                }
            }
            $arrData['user_id'] = $user['id'];
            $con_type = Yii::app()->request->getParam('con_type');
            if($con_type == 4){
                $arrData['con_type'] = Yii::app()->request->getParam('con_type_other');
            }else{
                $arrData['con_type'] = self::$conTypes[$con_type];
            }
            $payment_method = Yii::app()->request->getParam('payment_method');
            if($payment_method == 4){
                $arrData['payment_method'] = Yii::app()->request->getParam('payment_method_other');
            }else{
                $arrData['payment_method'] = self::$paymentMethods[$payment_method];
            }
            $arrData['title'] = Yii::app()->request->getParam('title');
            $arrData['payment_nature'] = Yii::app()->request->getParam('payment_nature');      
            $arrData['summary'] = nl2br(Yii::app()->request->getParam('summary'));
            $arrData['supplier'] = Yii::app()->request->getParam('supplier');
            $arrData['amount'] = Yii::app()->request->getParam('amount');
            $arrData['pay_type'] = Yii::app()->request->getParam('pay_type');
            $arrData['bank_account'] = Yii::app()->request->getParam('bank_account');
            $arrData['account_name'] = Yii::app()->request->getParam('account_name');
            $arrData['account_number'] = Yii::app()->request->getParam('account_number');
            $arrData['remark'] = nl2br(Yii::app()->request->getParam('remark'));
            $arrData['img_address'] = trim(Yii::app()->request->getParam('img_address'),';');
            $arrData['status'] = STATUS_VALID;
            $arrData['create_time'] = time();
            $loan_id = Yii::app()->request->getParam('loan_id');
            if($loan_id){
                $arrData['loan_id'] = $loan_id;
            }           
            $model = new FinancePayment();
            $id = $model->save($arrData);
            if($id < 1){
                Yii::log('finance_payment save fail:'.print_r($_POST,true));
                throw new CHttpException(404,'保存数据失败,请重新提交');
            }
            return $id;
        }else{
            //报错，资料不全
            throw new CHttpException(404,'请将资料补充完整');
        }
    }

    public function actionAdd()
    {
        $user = Yii::app()->params['user'];//from base Controller
        $workflow_id = Yii::app()->request->getParam('workflow_id');
        $workflow_id = intval($workflow_id);
        $sectionModel = new Section();
        $section = $sectionModel->getItemByPk($user['section_id']);
        $sections = $sectionModel->getParentSectionBySectionId($section['id']);
        $sections = array_merge(array($section),$sections);
        $arrData['sections'] = $sections;
        $workflowModel = new WorkFlow();
        $workflow = $workflowModel->getItemByPk($workflow_id);

        if(empty($workflow_id) || empty($workflow)){
            throw new CHttpException(404,'缺少参数');
        }

        $arrData['user'] = $user;
        $arrData['section'] = $section;
        $arrData['workflow_id'] = $workflow_id;
        $arrData['workflow'] = $workflow;

        if(Yii::app()->request->getIsPostRequest()){
            $tplData = array();
            switch($workflow['obj_type']){
                case 'absence':
                    $id = $this->createAbsence();
                    break;
                case 'cancel_absence':
                    $id = $this->createCancelAbsence();
                    break;
                case 'cancel_business':
                    $id = $this->createCancelBusiness();
                    break;
                case 'daily_reimbursement':
                    $id = $this->createDailyReimbursement();
                    break;
                case 'business_advance':
                    $id = $this->createBusinessAdvance();
                    break;
                 case 'business_reimbursement':
                    $id = $this->createBusinessReimbursement();
                    break;
                case 'business_card':
                    $id = $this->createBusinessCard();
                    break;
                case 'online_order':
                    $id = $this->createOnlineOrder();
                    break;
                case 'team_building':
                    $id = $this->createTeamBuilding();
                    break;
                case 'team_encourage':
                    $id = $this->createTeamEncourage();
                    break;
                case 'workorder':
                    $id = $this->createWorkOrder();
                    break;
                case 'papp_vpn':
                    $id = $this->createPappVpn();
                    break;
                case 'papp_db':
                    $id = $this->createPappDB();
                    break;
                case 'entry':
                    $id = $this->createEntry();
                    break;
                case 'dimission':
                    $id = $this->createDimission();
                    break;
                case 'ticket_book':
                    $id = $this->createTicketBook();
                    $days = Yii::app()->request->getParam('daysInterval');
                    if(!is_null($days)){
                        $days = intval($days);
                        $workflow['userworkflow_config'] = $days < 3 ? 'ticket_book_hurry' : '';
                    }
                    break;
                case 'office_supply':
                    $id = $this->createOfficeSupply();
                    break;
                case 'lunch_change':
                    $id = $this->createLunchChange();
                    break;
                case 'finance_loan':
                    $id = $this->createFinanceLoan();
                    break;
                case 'finance_offset':
                    $id = $this->createFinancePayment();
                    break;
                case 'finance_payment':
                    $id = $this->createFinancePayment();
                    break;
            }

            if($id > 0)
            {
                $userWorkflowId = $this->saveUserWorkflow($workflow,$id);//save new info to user work flow, return id value
                $workflowService = new WorkflowService($userWorkflowId);
                $workflowService->workflowProcess();//audit operation
            }


            $this->redirect(Yii::app()->createURL('userworkflow/my'));
        }
        switch($workflow['obj_type']){
            case 'absence':
                $vacationModel = new VacationRecord();
                $arrData['nianPerYear'] = $this->getNianjiaNumber($user['work_date']);
                $arrData['available_nianjia'] = $vacationModel->getHolidayQuantity($user['id'],VACATION_NIANJIA);
                $arrData['available_bingjia'] = $vacationModel->getHolidayQuantity($user['id'],VACATION_BINGJIA);
                $arrData['abs_array'] = array('nian'=>$arrData['available_nianjia'],'bing'=>$arrData['available_bingjia'],'nianPerYear'=>$arrData['nianPerYear']);
                $arrData['absenceType'] = OAConfig::$absenceType;
                if($workflow['name'] == "出差"){
                    $tplPath = "./workflow/business.tpl";
                }elseif($workflow['name'] == "因公外出"){
//                    if(!in_array('businessout', Yii::app()->params['header_list'])){
//                        throw new CHttpException(404,'该功能暂未开通:)');
//                    }
                    $tplPath = "./workflow/business_out.tpl";
                }else{
                    $tplPath = "./workflow/qingjia.tpl";
                }
                break;

            case 'cancel_business':

                $tplPath = "./workflow/cancel_business.tpl";
                break;

            case 'cancel_absence':
                $absenceModel = new Absence();
                $arrData['absenceType'] = OAConfig::$absenceType;
                if(Yii::app()->getRequest()->getQuery('abs')){
                    $arrData['absence'] = $absenceModel->getItemByPk(Yii::app()->getRequest()->getQuery('abs'));
                    $tplPath = "./workflow/xiaojia_detail.tpl";
                }else{
                    $arrData['absenceTotal'] = $absenceModel->getAbsenceListByUser($user['id']);
                    $tplPath = "./workflow/xiaojia.tpl";
                }
                
                break;
            case 'daily_reimbursement':
                $tplPath = "./workflow/daily_reimbursement.tpl";
                break;
            case 'business_advance':
                $tplPath = "./workflow/business_advance.tpl";
                break;
             case 'business_reimbursement':
                $tplPath = "./workflow/business_reimbursement.tpl";
                break;
            case 'business_card':
                $arrData['officetelephone'] = OAConfig::officeTelephone();
                $sections = $sectionModel->getParentSectionBySectionId($section['id']);
                $sections = array_merge(array($section),$sections);
                $arrData['sections'] = $sections;
                $tplPath = "./workflow/business_card.tpl";
                break;
            case 'online_order':
                $arrData['function_type'] = OAConfig::$function_type;
                $arrData['qa_type'] = OAConfig::$qa_type;
                $tplPath = "./workflow/online_order.tpl";
                break;
            case 'team_building':
                $tplPath = "./workflow/team_building.tpl";
                $arrData['activity_type'] = OAConfig::$activity_type;
                $arrData['sections'] = $sections = $sectionModel->getSectionsByManagerId($user['id']);
                if(empty($sections)){//权限判断 只有"有团建权限部门"的负责人有权限申请
                    throw new CHttpException(404,'你没有权限发起团建报销'); 
                }
                break;

            case 'team_encourage':
                $tplPath = "./workflow/team_encourage.tpl";
                $arrData['sections'] = $sections = $sectionModel->getEncourageByManagerId($user['id']);
                if(empty($sections)){//权限判断 只有"有团队激励权限部门"的负责人有权限申请
                    throw new CHttpException(404,'你没有权限发起激励经费报销'); 
                }
                break;

		    case 'workorder':
			    $tplPath = "./workflow/workorder.tpl";
				$arrData['workorderType'] = OAConfig::$workorderType;
				$arrData['workorderLevel'] = OAConfig::$workorderLevel;
                break;

            case 'papp_vpn':
		        $arrData['vpnType'] = OAConfig::$vpnType;
			    $tplPath = "./workflow/papp_vpn.tpl";
                break;
  
            case 'papp_db':
			    $tplPath = "./workflow/papp_db.tpl";
                break;

            case 'express':
                $this->redirect(Yii::app()->createURL('home/selfems') );
                break;

            case 'entry':
                $tplPath = "./workflow/entry.tpl";
                $arrData['hireTypes'] = OAConfig::$hireTypes;
                $arrData['sections'] = $sectionModel->getSectionIdNameList(false);
                $modelUser = new User();
                $arrData['managers'] = $modelUser->getManagers();
                if(!in_array('hr', Yii::app()->params['header_list'])){//权限判断 只有人力部门
                    throw new CHttpException(404,'你没有权限发起员工入职流程');
                }
                break;

            case 'dimission':
                $tplPath = "./workflow/dimission.tpl";
                $userModel = new User();
                $user['sectionName'] = $section['name'];
                $objManager = $userModel->getItemByPk($section['manager_id']);
                $user['directManager'] = $objManager['name'];
                $arrData['user'] = $user;
                if(!in_array('hr', Yii::app()->params['header_list'])){//权限判断 只有人力部门
                    throw new CHttpException(404,'该功能尚未开通:)');
                }
                break;
            case 'transfer':
//                $tplPath = "./workflow/entry.tpl";
//                $arrData['hireTypes'] = OAConfig::$hireTypes;
//                $arrData['sections'] = $sectionModel->getSectionIdNameList(false);
//                $modelUser = new User();
//                $arrData['managers'] = $modelUser->getManagers();
//                if(!in_array('hr', Yii::app()->params['header_list'])){//权限判断 只有人力部门
//                    throw new CHttpException(404,'你没有权限发起员工入职流程');
//                }
                throw new CHttpException(404,'该功能暂未开通:)');
                break;
            case 'regular':
//                $tplPath = "./workflow/entry.tpl";
//                $arrData['hireTypes'] = OAConfig::$hireTypes;
//                $arrData['sections'] = $sectionModel->getSectionIdNameList(false);
//                $modelUser = new User();
//                $arrData['managers'] = $modelUser->getManagers();
//                if(!in_array('hr', Yii::app()->params['header_list'])){//权限判断 只有人力部门
//                    throw new CHttpException(404,'你没有权限发起员工入职流程');
//                }
                throw new CHttpException(404,'该功能暂未开通:)');
                break;
            case 'ticket_book':
                $sections = $sectionModel->getParentSectionBySectionId($section['id']);
                $sections = array_merge(array($section),$sections);
                $arrData['sections'] = $sections;
                $arrData['ticketRefund'] = TICKET_REFUND;
                $arrData['ticketEndorse'] = TICKET_ENDORSE;
                $status = Yii::app()->request->getParam('status');
                if($status == TICKET_REFUND){
                    $tplPath = "./workflow/ticket_refund.tpl";
                }elseif($status == TICKET_ENDORSE){
                    $tplPath = "./workflow/ticket_endorse.tpl";
                }else{
                    $tplPath = "./workflow/ticket_book.tpl";
                }
                break;
            case 'office_supply':
                $tplPath = "workflow/office_supply.tpl";
                $arrData['officeSupply'] = OAConfig::$officeSupply;
                $arrData['officeLocation'] = OAConfig::$officeLocation;
                break;
            case 'lunch_change':
                $today = date('Y-m-d', time());
                $day = explode('-', $today)[2];
                if($day < '25'){
                    throw new CHttpException(404,'每月午餐变更时间为当月25日-30日之间:)');
                }
                $tplPath = "workflow/lunch_change.tpl";
                $arrData['lunchLocation'] = OAConfig::$lunchLocation;
                $arrData['lunchChangeType'] = OAConfig::$lunchChangeType;
                break;
            case 'finance_loan':
                $arrData['con_types'] =self::$conTypes;
                $arrData['pay_types'] = OAConfig::$pay_types;
                $tplPath = "workflow/finance_loan.tpl";
                break;
            case 'finance_offset':
                $arrData['con_types'] = self::$conTypes;
                $arrData['payment_methods'] = self::$paymentMethods;
                $arrData['payment_natures'] = OAConfig::$payment_natures;
                $financeLoanModel = new FinanceLoan();
                $loan_id = Yii::app()->getRequest()->getQuery('loanid');
                if($loan_id){
                    $arrData['loan'] = $financeLoanModel->getItemByPk($loan_id);
                    $arrData['loan']['summary'] = ereg_replace("<br(.)*>","",$arrData['loan']['summary']);
                    switch ($arrData['loan']['con_type']) {
                        case 'WFOE-北京融联世纪信息技术有限公司':
                            $arrData['loan']['con_type_key'] = 1;
                            break;
                        case 'ICP-北京融世纪信息技术有限公司':
                            $arrData['loan']['con_type_key'] = 2;
                            break;
                        case '北京融汇金融信息服务有限公司':
                            $arrData['loan']['con_type_key'] = 3;
                            break;
                        default :
                            $arrData['loan']['con_type_key'] = 4;
                    }
                    //获取财务审批通过借款单的时间
                    $userWorkflowModel = new UserWorkFlow();
                    $userWorkflowId = $userWorkflowModel->getUserWorkflowId($loan_id,'finance_loan');
                    $arrData['modify_time'] = $userWorkflowModel->getItemByPk($userWorkflowId)['modify_time'];
                    $tplPath = "./workflow/finance_payment.tpl";
                }else{
                    $arrData['loanTotal'] = $financeLoanModel->getLoanListByUser($user['id']);
                    $tplPath = "./workflow/finance_offset.tpl";
                }
                break;
            case 'finance_payment':
                $arrData['con_types'] = self::$conTypes;
                $arrData['pay_types'] = OAConfig::$pay_types;
                $arrData['payment_methods'] = self::$paymentMethods;
                $arrData['payment_natures'] = OAConfig::$payment_natures;
                $tplPath = "./workflow/finance_payment.tpl";
        }
        $this->renderFile($tplPath,$arrData);
    }

    public function saveUserWorkflow($workflow,$obj_id)
    {
        $arrData = array();
        $arrData['user_id'] = Yii::app()->session['oa_user_id'];
        $arrData['workflow_id'] = $workflow['id'];
        $arrData['category'] = $workflow['category'];
        $arrData['obj_type'] = $workflow['obj_type'];
        $arrData['obj_id'] = $obj_id;
        $arrData['userworkflow_config'] = $workflow['userworkflow_config'];
        $arrData['create_time'] = time();
        $arrData['audit_status'] = OA_WORKFLOW_AUDIT_CHUANGJIAN;
        $arrData['status'] = STATUS_VALID;
        $model = new UserWorkFlow();
        $userWorkflowId = $model->save($arrData);
        if($userWorkflowId < 1){
            Yii::log('userworkflow save fail:'.print_r($arrData,true));
            throw new CHttpException(404,"保存资料失败");
        }
        return $userWorkflowId; 
    }

    public function actionMy()
    {
        $userId = Yii::app()->session['oa_user_id']; 
        $audit_status = Yii::app()->request->getParam('audit_status');
        $workflow_type = Yii::app()->request->getParam('workflow_type');
        $pn = Yii::app()->request->getParam('pn',1);
        $rn = Yii::app()->request->getParam('rn',DEFAULT_PAGE_NUMBER);
        
        $start = ($pn-1)*$rn;
        $model = new UserWorkFlow();
        $userWorkflows = $model->getItemsByUserId($userId,$audit_status,$workflow_type,$start,$rn);
        $userWorkflowCnt = $model->getItemCntByUSerId($userId,$audit_status,$workflow_type);

        foreach($userWorkflows as &$workFlow) {

            if($workFlow['obj_type'] == 'absence' || $workFlow['obj_type'] == 'ticket_book' || $workFlow['obj_type'] == 'finance_payment') {

                $obj_id = $workFlow['obj_id'];
                $obj_type = $workFlow['obj_type'];
                $modelWorkFlow = new WorkFlow();
                $title = $modelWorkFlow->getObjType($obj_id, $obj_type);
                $workFlow['name'] = $title;
            }

        }
        $arrData['pn'] = $pn;
        $arrData['rn'] = $rn;
        $arrData['url'] = $audit_status ? '/userworkflow/my?audit_status='.$audit_status.'&' : '/userworkflow/my?';
        $arrData['current_audit_status'] = $audit_status;
        $arrData['userWorkflows'] = $userWorkflows;
        $arrData['userWorkflowTotalCnt'] = $userWorkflowCnt;
        $arrData['auditStatus'] = OAConfig::$audit_status;
        $arrData['user'] = Yii::app()->params['user'];
        $arrData['workflowTypes'] = OAConfig::$workflow_type;
        $arrData['workflow_type'] = $workflow_type;

        if(Yii::app()->request->isAjaxRequest){
            $this->renderFile("./widget/liucheng_content.tpl",$arrData); 
        }else{
            $this->renderFile("./liucheng.tpl",$arrData); 
        }
    }

    public function actionList()
    {
        $model = new WorkFlow();
        $workFlows = $model->getItemsOrderByCategory();//base config
        //把人力排在行政前面
        $workFlowsNew = array();
        $workFlowsNew[WORKFLOW_RENLI] = $workFlows[WORKFLOW_RENLI];
        $workFlowsNew[WORKFLOW_XINGZHENG] = $workFlows[WORKFLOW_XINGZHENG];
        unset($workFlows[WORKFLOW_RENLI]);
        unset($workFlows[WORKFLOW_XINGZHENG]);
        $workFlows = $workFlowsNew + $workFlows;
        foreach($workFlows as $category => &$items){
            if($category == WORKFLOW_XINGZHENG){
                foreach($items as &$item){
                    if($item['obj_type'] == 'administrative_expense'){
                        $item['url'] = 'administrativeexpense/add';
                    } 
                    else if($item['obj_type'] == 'administrative_apply'){
                        $item['url'] = 'administrativeapply/add';
                    } 
                }
            }
        }
        $workflowCategory = OAConfig::$workflow_category;//base config

        $arrData['workflows'] = $workFlows;
        $arrData['workflowCategory'] = $workflowCategory;
        $this->renderFile("./faqishenpi.tpl",$arrData);
    }

    //流程审批人
    public function auditUserIds($user_workflow_id)
    {
        $model = new UserTask();
        $auditIds = $model->getAuditUserIdsByUwfId($user_workflow_id);
        return $auditIds;
    }

    public function actionLook()
    {
        $id = Yii::app()->request->getParam('id');
        $userId = Yii::app()->session['oa_user_id'];
        $print = Yii::app()->request->getParam('print');

        $absence_id = Yii::app()->request->getParam('absence_id'); 

        $userWorkflowModel = new UserWorkFlow();

        if(!empty($absence_id) ) {
            $id = $userWorkflowModel -> getUserWorkflowId($absence_id, "absence");     
            var_dump($id);
            die;
            if(!$id) {
                throw new CHttpException(404,"没有获取到记录!");
            }
        }

        $arrData['id'] = $id;

        $userWorkflow = $userWorkflowModel->getAllOfUserWorkflowById($id);

        $userModel = new User();
        $user = $userModel->getItemByPk($userWorkflow['user_id']);

        $auditIds = $this->auditUserIds($id);
        if($userWorkflow['user_id'] != $userId && !in_array($userId,$auditIds) &&(!$this->checkRole($userId))){
            throw new CHttpException(404,"你没有权限查看");
        }

        $sectionModel = new Section();
        $section = $sectionModel->getItemByPk($user['section_id']);

        $userTaskModel = new UserTask();
        $userTasks = $userTaskModel->getItemsByUserWorkflowId($id);
        $workflowConfig = WorkflowConfig::getWorkFlowConfigByObjType($userWorkflow);

        $arrData['auditStatus'] = OAConfig::$audit_status;
        $arrData['userTasks'] = $userTasks;
        $arrData['workflowConfig'] = $workflowConfig;
        $arrData['section'] = $section;
        $arrData['userWorkflow'] = $userWorkflow;
        $arrData['user'] = $user;

        if($userWorkflow['obj_type'] == 'absence' || $userWorkflow['obj_type'] == 'cancel_absence' || $userWorkflow['obj_type'] == 'cancel_business'){
            $arrData['absenceType'] = OAConfig::$absenceType;
            $arrData['timeNode'] = OAConfig::$time_node;

            $strType = "请假";
            if($arrData['userWorkflow']['ext']['type'] == VACATION_CHUCHAI)  {
                $strType = "出差";
            } elseif($arrData['userWorkflow']['ext']['type'] == VACATION_YINGONGWAICHU)  {
                $strType = "因公外出";
            } elseif($userWorkflow['obj_type'] == 'cancel_business') {
                $strType = "销出差";
                $arrData['userWorkflow']['ext']['type'] = 14;
            }elseif($userWorkflow['obj_type'] == 'cancel_absence'){
                $strType = "销假";
            }

            $arrData['absenceType'][VACATION_CHUCHAI] = "出差";
            $arrData['absenceType'][14]              = "销出差";
            $arrData['absenceType'][VACATION_YINGONGWAICHU] = "因公外出";
            $arrData['str_type'] = $strType;
            if($arrData['userWorkflow']['ext']['type']==VACATION_SHIJIA || $arrData['userWorkflow']['ext']['type']==VACATION_BINGJIA || $arrData['userWorkflow']['ext']['type']==VACATION_NIANJIA){
                $vacationModel = new VacationRecord();
                $arrData['deduction'] = $vacationModel->getdeductionNianOrBing($arrData['userWorkflow']['ext']['id']);
            }
            $this->renderFile('./look/qingjia_detail.tpl',$arrData);
        }elseif($userWorkflow['obj_type'] == 'business_card') {
            $sections = $sectionModel->getParentSectionBySectionId($section['id']);
            $sections = array_merge(array($section), $sections);
            $arrData['sections'] = $sections;
            $this->renderFile('look/business_card_detail.tpl', $arrData);
        }elseif ($userWorkflow['obj_type'] == 'ticket_book'){
            $sections = $sectionModel->getParentSectionBySectionId($section['id']);
            $sections = array_merge(array($section), $sections);
            $arrData['sections'] = $sections;
            $ticketBookModel = new TicketBook();
            $objTicketBook = $ticketBookModel->getItemByPk($userWorkflow['obj_id']);
            $arrData['ticket_book'] = $objTicketBook;
            if($objTicketBook['status'] == TICKET_BOOK){
                $tpl = 'look/ticket_book.tpl'; 
            }elseif($objTicketBook['status'] == TICKET_REFUND){
                $tpl = 'look/ticket_refund.tpl';
            }elseif($objTicketBook['status'] == TICKET_ENDORSE){
                $tpl = 'look/ticket_endorse.tpl';
            }
            $this->renderFile($tpl, $arrData);
        }elseif($userWorkflow['obj_type'] == 'team_encourage'){

            $arrData['building_section'] = $sectionModel->getItemByPk($userWorkflow['ext']['section']);
            $arrData['activity_type'] = OAConfig::$activity_type;
            $tbRecordModel = new TeamEncourageRecord();
            $arrData['balance'] = $tbRecordModel->getTotalAmountBySectionId($userWorkflow['ext']['section']);
            if(!$print){
                $arrData['canPrint'] = $userTasks[count($userTasks)-1]['workflow_step'] == 'D' ? 1:0; 
                $this->renderFile('look/team_encourage_detail.tpl',$arrData);
            }else{
                $userModel = new User();
                foreach($userTasks as $userTask){
                    $entrust = false;
                    if($userTask['entrust_user_id'] > 0 ){ //委托人
                        $entrust = $userModel->getItemByPk($userTask['entrust_user_id']); 
                        $entrust['user_name'] = $entrust['name'];
                    }
                    $tmp = $entrust ? $entrust : $userTask; 

                    if($tmp['workflow_step'] == "A"){
                        if($tmp['audit_user_id']){
                            $auditUser = $userModel->getItemByPk($tmp['audit_user_id']);
                            $tmp['user_name'] = $auditUser['name'];
                            if($auditUser['level'] >= 90) {
                                $arrData['vp_name'] = $tmp;
                            } 
                        }
                    }
                    if($tmp['workflow_step'] == "B"){
                        if($tmp['audit_user_id']){
                            $auditUser = $userModel->getItemByPk($tmp['audit_user_id']);
                            $tmp['user_name'] = $auditUser['name'];
                        }
                        $arrData['renli'] = $tmp;
                    }
                    if($tmp['workflow_step'] == "C"){
                        if($tmp['audit_user_id']){
                            $auditUser = $userModel->getItemByPk($tmp['audit_user_id']);
                            $tmp['user_name'] = $auditUser['name'];
                        }
                        $arrData['renlizhuguan'] = $tmp;
                    }

                }
                // 不需要部门经理审批的，部门负责人是发起人本人
                if(empty($manage)){
                    $manage = $userModel->getItemByPk($userWorkflow['user_id']);
                    $manage['user_name'] = $manage['name'];
                }
                $arrData['manage'] = $manage;
                $this->renderFile('./widget/team_encourage/print.tpl',$arrData);
            }
        }elseif($userWorkflow['obj_type'] == 'team_building'){
            $arrData['building_section'] = $sectionModel->getItemByPk($userWorkflow['ext']['section_id']);
            $arrData['activity_type'] = OAConfig::$activity_type;
            $tbRecordModel = new TeamBuildingRecord();
            $arrData['balance'] = $tbRecordModel->getTotalAmountBySectionId($userWorkflow['ext']['section_id']);

            if(!$print){

                $arrData['canPrint'] = $userTasks[count($userTasks)-1]['workflow_step'] == 'D' ? 1:0; 

                if($arrData['canPrint'] != 1) {

                    $arrData['canPrint'] = $userTasks[count($userTasks)-1]['workflow_step'] == 'E' ? 1:0; 


                }

                $this->renderFile('look/team_building_detail.tpl',$arrData);

            }else{
                $userModel = new User();
                foreach($userTasks as $userTask){
                    $entrust = false;
                    if($userTask['entrust_user_id'] > 0 ){ //委托人
                        $entrust = $userModel->getItemByPk($userTask['entrust_user_id']); 
                        $entrust['user_name'] = $entrust['name'];
                    }
                    $tmp = $entrust ? $entrust : $userTask; 

                    // if($tmp['level'] >= AUDIT_MANAGER){
                    //     $manage = $tmp;
                    //     continue;
                    // } 
                    if($tmp['workflow_step'] == "B"){
                        if($tmp['audit_user_id']){
                            $auditUser = $userModel->getItemByPk($tmp['audit_user_id']);
                            $tmp['user_name'] = $auditUser['name'];
                        }
                        $arrData['renli'] = $tmp;
                    }
                    if($tmp['workflow_step'] == "C"){
                        if($tmp['audit_user_id']){
                            $auditUser = $userModel->getItemByPk($tmp['audit_user_id']);
                            $tmp['user_name'] = $auditUser['name'];
                        }
                        $arrData['renlizhuguan'] = $tmp;
                    }

                    //兼容老的
                    if($tmp['workflow_step'] == "E"){
                        if($tmp['audit_user_id']){
                            $auditUser = $userModel->getItemByPk($tmp['audit_user_id']);
                            $tmp['user_name'] = $auditUser['name'];
                        }
                        $arrData['vp_name'] = $tmp;
                    }

                    //2015-12-31
                    if($tmp['workflow_step'] == "A"){
                        if($tmp['audit_user_id']){
                            $auditUser = $userModel->getItemByPk($tmp['audit_user_id']);
                            $tmp['user_name'] = $auditUser['name'];

                            if($auditUser['level'] >= 90) {

                                $arrData['vp_name'] = $tmp;

                            } 
                        }
                    }
                }
                //不需要部门经理审批的，部门负责人是发起人本人
                if(empty($manage)){
                    $manage = $userModel->getItemByPk($userWorkflow['user_id']);
                    $manage['user_name'] = $manage['name'];
                }
                $arrData['manage'] = $manage;
                $this->renderFile('./widget/team_building/print.tpl',$arrData);
            }
        }elseif($userWorkflow['obj_type'] == 'daily_reimbursement'){
            $model = new DailyReimbursement();
            $arrData['daily_reimbursement'] = $model->getItemsByParentId($userWorkflow['obj_id']);
            $arrData['daily_reimbursementType'] = OAConfig::$daily_reimbursementType;
            $arrData['parentSection'] = $sectionModel->getParentSectionBySectionId($section['id']);
            $arrData['create_time'] = $userWorkflow['create_time']; 
            $parent = $model->getParentItemById($userWorkflow['obj_id']);
            $arrData['totalCnt'] = $parent['amount_check'] ==0.00?$model->getSumAmountByParentId($userWorkflow['obj_id']):$parent['amount_check'];
            if(!$print){
                //审批已经完成，支持打印
                $arrData['canPrint'] = $userTasks[count($userTasks)-1]['user_id'] == 0 ? 1:0; 
                $this->renderFile('look/daily_reimbursement_detail.tpl',$arrData);
            }else{//打印页,单独的模板 
                $userModel = new User();
                foreach($userTasks as $userTask){
                    $entrust = false;
                    if($userTask['entrust_user_id'] > 0 ){ //委托人
                        $entrust = $userModel->getItemByPk($userTask['entrust_user_id']); 
                        $entrust['user_name'] = $entrust['name'];
                    }
                    $tmp = $entrust ? $entrust : $userTask; 

                    if($tmp['level'] == AUDIT_MANAGER){
                        $arrData['manage'] = $tmp;
                        continue;
                    } 
                    if($tmp['level'] == AUDIT_VP){
                        $arrData['vicePresident'] = $tmp;
                        continue;
                    }
                    if($tmp['level'] == AUDIT_CEO){
                        $arrData['ceo'] = $tmp;
                    }
                }
                $this->renderFile('./widget/dailyreimbursement/print.tpl',$arrData);
            }
        }elseif($userWorkflow['obj_type'] == 'business_advance'){
            $model = new BusinessAdvance();
            $arrData['business_advance'] = $model->getItemsByParentId($userWorkflow['obj_id']);
            $arrData['business_advanceType'] = OAConfig::$business_advanceType;
            if(!$print){
                //审批已经完成，支持打印
                $arrData['canPrint'] = $userTasks[count($userTasks)-1]['user_id'] == 0 ? 1:0; 
                $this->renderFile('look/business_advance_detail.tpl',$arrData);
            }else{//打印页,单独的模板 
                $arrData['totalCnt'] = $model->getSumAmountByParentId($userWorkflow['obj_id']);
                $userModel = new User();
                foreach($userTasks as $userTask){
                    $entrust = false;
                    if($userTask['entrust_user_id'] > 0 ){ //委托人
                        $entrust = $userModel->getItemByPk($userTask['entrust_user_id']); 
                        $entrust['user_name'] = $entrust['name'];
                    }
                    $tmp = $entrust ? $entrust : $userTask; 

                    if($tmp['level'] == AUDIT_MANAGER){
                        $arrData['manage'] = $tmp;
                        continue;
                    } 
                    if($tmp['level'] == AUDIT_VP){
                        $arrData['vicePresident'] = $tmp;
                        continue;
                    }
                    if($tmp['level'] == AUDIT_CEO){
                        $arrData['ceo'] = $tmp;
                    }
                }
                $this->renderFile('./widget/businessadvance/print.tpl',$arrData);
            }
        }elseif($userWorkflow['obj_type'] == 'business_reimbursement'){
            $model = new BusinessReimbursement();
            $arrData['business_reimbursement'] = $model->getItemsByParentId($userWorkflow['obj_id']);
            $arrData['business_reimbursementType'] = OAConfig::$business_reimbursementType;
            $arrData['parentSection'] = $sectionModel->getParentSectionBySectionId($section['id']);
            $parent = $model->getParentItemById($userWorkflow['obj_id']);
            $arrData['totalCnt'] = $parent['amount_check'] ==0.00?$model->getSumAmountByParentId($userWorkflow['obj_id']):$parent['amount_check'];

            if(!$print){
                //审批已经完成，支持打印
                $arrData['canPrint'] = $userTasks[count($userTasks)-1]['user_id'] == 0 ? 1:0; 
                $this->renderFile('look/business_reimbursement_detail.tpl',$arrData);
            }else{//打印页,单独的模板 
                $userModel = new User();
                foreach($userTasks as $userTask){
                    $entrust = false;
                    if($userTask['entrust_user_id'] > 0 ){ //委托人
                        $entrust = $userModel->getItemByPk($userTask['entrust_user_id']); 
                        $entrust['user_name'] = $entrust['name'];
                    }
                    $tmp = $entrust ? $entrust : $userTask; 

                    if($tmp['level'] == AUDIT_MANAGER){
                        $arrData['manage'] = $tmp;
                        continue;
                    } 
                    if($tmp['level'] == AUDIT_VP){
                        $arrData['vicePresident'] = $tmp;
                        continue;
                    }
                    if($tmp['level'] == AUDIT_CEO){
                        $arrData['ceo'] = $tmp;
                    }
                }
                $this->renderFile('./widget/businessreimbursement/print.tpl',$arrData);
            }
        }elseif($userWorkflow['obj_type'] == 'online_order'){
            $auditListModel = new AuditList();
            $auditList = $auditListModel->getAuditListStatusByUserWorkflowId($id);
            $cutTask = $userTaskModel->getLastTaskByUwfId($id);
            $arrData['cutTask'] = $cutTask;
            $arrData['auditList'] = $auditList;
            $arrData['deploy_id']    = $userWorkflow['deploy_id'];
            $arrData['function_type'] = OAConfig::$function_type;
            $arrData['qa_type'] = OAConfig::$qa_type;
            $arrData['business_role'] = OAConfig::$business_role;
            $steps = str_replace(PHP_EOL, '<br>', $userWorkflow['ext']['online_steps']);
            $arrData['steps'] = $steps;
            $arrData['filenames'] = OnlineOrder::getFileName($userWorkflow['ext']['attachment_address']);
            $this->renderFile('look/online_order.tpl',$arrData);
        }elseif($userWorkflow['obj_type'] == 'entry'){
            $auditListModel = new AuditList();
            $auditList = $auditListModel->getAuditListStatusByUserWorkflowId($id);
            $cutTask = $userTaskModel->getLastTaskByUwfId($id);
            $arrData['userTasks'] = $userTasks;
            $arrData['cutTask'] = $cutTask;
            $arrData['auditList'] = $auditList;
            $arrData['business_role'] = OAConfig::$business_role;
            $arrData['hireTypes'] = OAConfig::$hireTypes;
            $arrData['businessLine'] = $sectionModel->getItemByPk($arrData['userWorkflow']['ext']['business_line_id']);
            $arrData['businessLine'] = $arrData['businessLine']['name'];
            $arrData['section1'] = $sectionModel->getItemByPk($arrData['userWorkflow']['ext']['section_id']);
            $arrData['section1'] = $arrData['section1']['name'];
            $arrData['subSection'] = $sectionModel->getItemByPk($arrData['userWorkflow']['ext']['subsection_id']);
            $arrData['subSection'] = $arrData['subSection']['name'];
            $arrData['directManager'] = $userModel->getItemByPk($arrData['userWorkflow']['ext']['manager_id']);
            $arrData['directManager'] = $arrData['directManager']['name'];
//            $arrData['canPrint'] = $userWorkflow['audit_status'] == OA_WORKFLOW_AUDIT_PASS ? 1 : 0;
            if($print){
                $this->renderFile('./widget/entry/print.tpl',$arrData);
            }else {
                $this->renderFile('look/entry.tpl', $arrData);
            }
        }elseif($userWorkflow['obj_type'] == 'dimission'){
            $dimissionUser = $userModel->getItemByPk($userWorkflow['user_id']);
            $dimissionUserDepartment = $sectionModel->getItemByPk($dimissionUser['section_id']);
            $dimissionUser['department'] = $dimissionUserDepartment['name'];
            $dimissionUserManager = $userModel->getItemByPk($dimissionUserDepartment[manager_id]);
            $dimissionUser['dimissionUserManager'] = $dimissionUserManager;
            $arrData['dimissionUser'] = $dimissionUser;
            $auditListModel = new AuditList();
            $auditList = $auditListModel->getAuditListStatusByUserWorkflowId($id);
            $cutTask = $userTaskModel->getLastTaskByUwfId($id);
            $arrData['cutTask'] = $cutTask;
            $arrData['auditList'] = $auditList;
            $arrData['business_role'] = OAConfig::$business_role;
            $arrData['hireTypes'] = OAConfig::$hireTypes;
            $arrData['businessLine'] = $sectionModel->getItemByPk($arrData['userWorkflow']['ext']['business_line_id']);
            $arrData['businessLine'] = $arrData['businessLine']['name'];
            $arrData['section1'] = $sectionModel->getItemByPk($arrData['userWorkflow']['ext']['section_id']);
            $arrData['section1'] = $arrData['section1']['name'];
            $arrData['subSection'] = $sectionModel->getItemByPk($arrData['userWorkflow']['ext']['subsection_id']);
            $arrData['subSection'] = $arrData['subSection']['name'];
            $arrData['directManager'] = $userModel->getItemByPk($arrData['userWorkflow']['ext']['manager_id']);
            $arrData['directManager'] = $arrData['directManager']['name'];
//            $arrData['canPrint'] = $userWorkflow['audit_status'] == OA_WORKFLOW_AUDIT_PASS ? 1 : 0;
            if($print){
                $this->renderFile('./widget/dimission/print.tpl',$arrData);
            }else {
                $this->renderFile('look/dimission.tpl', $arrData);
            }
        }elseif($userWorkflow['obj_type'] == 'workorder'){
            $sections = $sectionModel->getParentSectionBySectionId($section['id']);
            $sections = array_merge(array($section),$sections);
            $arrData['sections'] = $sections;
			$problem = $userWorkflow['ext']['problem_reason'];
			$arrData['problem'] = $problem;
			$create_time = $userWorkflow['ext']['create_time'];
			$arrData['create_time'] = $create_time;
			$type = $userWorkflow['ext']['type'];
			$arrData['type'] = $type;
			$level = $userWorkflow['ext']['level'];
			$arrData['level'] = $level;
			$arrData['issue_time'] = $userWorkflow['ext']['issue_time'];
			$arrData['issue_desc'] = $userWorkflow['ext']['issue_desc'];
			$arrData['do_time'] = $userWorkflow['ext']['do_time'];
			$arrData['pause_desc'] = $userWorkflow['ext']['pause_desc'];
			$arrData['pause_time'] = $userWorkflow['ext']['pause_time'];
			$arrData['handle_desc'] = $userWorkflow['ext']['handle_desc'];

			$workorderType = OAConfig::$workorderType;
			$workorderLevel = OAConfig::$workorderLevel;
			$arrData['workorderType'] = $workorderType;
			$arrData['workorderLevel'] = $workorderLevel;
            $this->renderFile('look/workorder_detail.tpl',$arrData);    
        }elseif($userWorkflow['obj_type'] == "papp_vpn"){
            $sections = $sectionModel->getParentSectionBySectionId($section['id']);
            $sections = array_merge(array($section),$sections);
            $arrData['sections'] = $sections;
			$arrData['problem'] = $userWorkflow['ext']['reason'];
			$arrData['create_time'] = $userWorkflow['ext']['create_time'];
			$arrData['type']   = $userWorkflow['ext']['type'];
			$arrData['vpnType'] = OAConfig::$vpnType;

            $this->renderFile('look/papp_vpn_detail.tpl',$arrData);    
		}elseif($userWorkflow['obj_type'] == "papp_db"){
            $sections = $sectionModel->getParentSectionBySectionId($section['id']);
            $sections = array_merge(array($section),$sections);
            $arrData['sections'] = $sections;
			$problem = $userWorkflow['ext']['reason'];
			$arrData['problem'] = $problem;
			$create_time = $userWorkflow['ext']['create_time'];
			$arrData['create_time'] = $create_time;
			$arrData['dbs_name'] = $userWorkflow['ext']['dbs_name'];
			$arrData['tables_name'] = $userWorkflow['ext']['tables_name'];

            $this->renderFile('look/papp_db_detail.tpl',$arrData);    


		}elseif($userWorkflow['obj_type'] == "office_supply"){
            $sections = $sectionModel->getParentSectionBySectionId($section['id']);
            $sections = array_merge(array($section),$sections);
            $arrData['sections'] = $sections;
            $arrData['officeLocation'] = OAConfig::$officeLocation;
            $arrData['officeSupply'] = OAConfig::$officeSupply;
            $arrSupplyStrings = explode('|', $userWorkflow['ext']['supply_type_detail_amount']);
            $arrData['supplyDetails'] = array();
            if(is_array($arrSupplyStrings)){
                foreach($arrSupplyStrings as $key => $value){
                    $arrData['supplyDetails'][] = explode(',', $value);
                }
            }
            $this->renderFile('look/office_supply.tpl',$arrData);
        }elseif($userWorkflow['obj_type'] == "lunch_change"){
            $sections = $sectionModel->getParentSectionBySectionId($section['id']);
            $sections = array_merge(array($section),$sections);
            $arrData['sections'] = $sections;
            $arrData['lunchLocation'] = OAConfig::$lunchLocation;
            $arrData['lunchChangeType'] = OAConfig::$lunchChangeType;
            $this->renderFile('look/lunch_change.tpl',$arrData);
        }elseif($userWorkflow['obj_type'] == 'administrative_expense'){
            if($print){
                foreach($userTasks as $item){
                    if(!empty($item['user_id'])){
                        if($item['user_name'] == '董科'){
                            $item['level'] = AUDIT_VP;
                        }
                        if($item['level'] == AUDIT_CEO){
                            $arrData['CEO'] = $item['user_name'];
                        }elseif($item['level'] == AUDIT_VP){
                            $arrData['VP'] = $item['user_name'];
                        }elseif(isset($manager_level)){
                            if($item['level'] > $manager_level){
                                $arrData['manager'] = $item['user_name'];
                                $manager_level = $item['level'];
                            }
                        }else{
                            $manager_level = $item['level'];
                            $arrData['manager'] = $item['user_name'];
                        }
                        if($item['user_name'] == '郭慧文'){
                            $arrData['RENLI'] = '郭慧文';
                        }
                    }            
                }
                $this->renderFile('./administrative_expense/print.tpl',$arrData);
            }else{
                $arrData['canPrint'] = $userTasks[count($userTasks)-1]['workflow_step'] == 'C' ? 1:0; 
                $this->renderFile('look/administrative_expense.tpl',$arrData);
            }
        }elseif($userWorkflow['obj_type'] == 'administrative_apply'){
            $Lasttask = $userTaskModel->getTaskByStep("D",$id);
            $Lasttask?$arrData['finish']=1:$arrData['finish']=0;
            $this->renderFile('look/administrative_apply.tpl',$arrData);
        }elseif($userWorkflow['obj_type'] == 'finance_loan'){
            $arrData['pay_types'] = OAConfig::$pay_types;
            if($print){
                foreach($userTasks as $item){
                    if(!empty($item['user_id'])){
                        if(!empty($item['audit_desc'])){
                            if(isset($arrData['RENLI'])){
                                $arrData['RENLI'] = $arrData['RENLI'].','.$item['user_name'];
                            }else{
                                $arrData['RENLI'] = $item['user_name'];
                            }
                        }else{
                            if($item['level'] == AUDIT_CEO){
                                $arrData['CEO'] = $item['user_name'];
                            }elseif($item['level'] == AUDIT_VP){
                                $arrData['VP'] = $item['user_name'];
                            }elseif(isset($manager_level)){
                                if($item['level'] > $manager_level){
                                    $arrData['manager'] = $item['user_name'];
                                    $manager_level = $item['level'];
                                }
                            }else{
                                $manager_level = $item['level'];
                                $arrData['manager'] = $item['user_name'];
                            }
                        }
                    }            
                } 
                $this->renderFile('./widget/finance_loan/print.tpl',$arrData);
            }else{
                $arrData['canPrint'] = $userTasks[count($userTasks)-1]['workflow_step'] == 'B' ? 1:0;
                $arrData['extension'] = $userWorkflowModel->getExtByUserWorkflow($userWorkflow['obj_type'],$userWorkflow['obj_id']);
                $this->renderFile('look/finance_loan.tpl',$arrData);
            }
        }elseif($userWorkflow['obj_type'] == 'finance_offset' || $userWorkflow['obj_type'] == 'finance_payment'){
            $arrData['payment_natures'] = OAConfig::$payment_natures;
            $arrData['pay_types'] = OAConfig::$pay_types;
            if($print){
                foreach($userTasks as $item){
                    if(!empty($item['user_id'])){
                        if(!empty($item['audit_desc'])){
                            if(isset($arrData['RENLI'])){
                                $arrData['RENLI'] = $arrData['RENLI'].','.$item['user_name'];
                            }else{
                                $arrData['RENLI'] = $item['user_name'];
                            }
                        }else{
                            if($item['level'] == AUDIT_CEO){
                                $arrData['CEO'] = $item['user_name'];
                            }elseif($item['level'] == AUDIT_VP){
                                $arrData['VP'] = $item['user_name'];
                            }elseif(isset($manager_level)){
                                if($item['level'] > $manager_level){
                                    $arrData['manager'] = $item['user_name'];
                                    $manager_level = $item['level'];
                                }
                            }else{
                                $manager_level = $item['level'];
                                $arrData['manager'] = $item['user_name'];
                            }
                        }
                    }            
                } 
                $this->renderFile('./widget/finance_payment/print.tpl',$arrData);
            }else{       
                $arrData['canPrint'] = $userTasks[count($userTasks)-1]['workflow_step'] == 'B' ? 1:0;
                $arrData['extension'] = $userWorkflowModel->getExtByUserWorkflow($userWorkflow['obj_type'],$userWorkflow['obj_id']);
                $arrData['extension']['img_address'] = explode(";",$arrData['extension']['img_address']);
                $this->renderFile('look/finance_payment.tpl',$arrData);
            }
        }
    }

    public function checkQingjiaDate($start_time,$end_time,$type=0)
    {
        //今天的零点
        $now = strtotime(date('Y-m-d',time())); 

        //补假 只可以在这个考勤月
        if($start_time < $now){
            $today = date('d',time());  
            $kaoqin_day = date('Y-m-01',time() );
            // $kaoqin_day = date('Y-m-26',time() );
            if($today < 26){ //上个考勤月截止日期
                // $kaoqin_end = strtotime("-1 month",strtotime($kaoqin_day)); 
                $kaoqin_end = date("Y-m-d", strtotime("-1 day",strtotime($kaoqin_day)));
            }else{ //上个考勤月截止日期
                // $kaoqin_end = strtotime($kaoqin_day); 
                $kaoqin_end = date('Y-m-d', strtotime("+1 month -1 day",strtotime($kaoqin_day)));    
            }

            //关闭补假时间点,考勤截止日的 15点 
            $closeTime = strtotime(date('Y-m-d',$kaoqin_end)." 15:00:00");
            if($start_time < $kaoqin_end && time() > $closeTime){
                return false;
            }
        }else{//请假最多可以提前30天
            $future = strtotime('+1 month',$now); 
            if($type != 7){
                if($end_time > $future){
                    return false;
                }
            }
        }
        return true;
    }
    //计算请假的天数
    public function actionCalculateSpan()
    {
        $btime = Yii::app()->request->getParam('start_day');
        $etime = Yii::app()->request->getParam('end_day');
        $from = Yii::app()->request->getParam('start_node');
        $end = Yii::app()->request->getParam('end_node');
        $absenceType = Yii::app()->request->getParam('absence_type');
        $type = Yii::app()->request->getParam('type');
        $ret['msg'] = '';
        $user_id = Yii::app()->session['oa_user_id'];
        $start_time = OAConfig::nodeToTimeStamp($btime,$from);
        $end_time = OAConfig::nodeToTimeStamp($etime,$end);

        $absenceModel = new Absence();
        $absences = $absenceModel->getValidItemsByDay($user_id,$start_time,$end_time);
        if(!empty($absences)){


            //判断用户是否已经销过假
            $model = new CancelAbsence();
            if(!$model->isHaveRecord($user_id,$start_time,$end_time)){

                $ret['dayNum'] = 0;
                $ret['msg'] = '已有请假记录，不能再次申请';
                echo json_encode($ret);
                Yii::app()->end();

            }else{
                foreach ($absences as $v) {
                    if($v['is_cancel'] == 0){
                        $ret['dayNum'] = 0;
                        $ret['msg'] = '已有请假记录，不能再次申请';
                        echo json_encode($ret);
                        Yii::app()->end();
                    }
                }
            }
        }

        $ret['dayNum'] = $this->calculateDays($btime,$etime,$from,$end,$absenceType);
        echo json_encode($ret);
        Yii::app()->end();
    }

    //计算请假的天数-php校验
    public function calculateSpanPhp($btime,$etime,$from,$end,$type)
    {
        // $btime = Yii::app()->request->getParam('start_day');
        // $etime = Yii::app()->request->getParam('end_day');
        // $from = Yii::app()->request->getParam('start_node');
        // $end = Yii::app()->request->getParam('end_node');
        // $absenceType = Yii::app()->request->getParam('absence_type');
        // $type = Yii::app()->request->getParam('type');
        $user_id = Yii::app()->session['oa_user_id'];
        $start_time = OAConfig::nodeToTimeStamp($btime,$from);
        $end_time = OAConfig::nodeToTimeStamp($etime,$end);

        $absenceModel = new Absence();
        $absences = $absenceModel->getValidItemsByDay($user_id,$start_time,$end_time);
        if(!empty($absences)){

            //判断用户是否已经销过假
            $model = new CancelAbsence();
            if(!$model->isHaveRecord($user_id,$start_time,$end_time)){
                return false;
            }
        }
        return true;
    }

    //计算订票提前的天数
    public function actionCalculateSpanTicket()
    {
        $tripDate = Yii::app()->request->getParam('trip_date');
        $today = date('Y-m-d');
        $ret['msg'] = '';
        if($tripDate < $today){
            $ret['msg'] = '出行日期不能小于今天~';
        }
        $dateTripDate = date_create($tripDate);
        $dateToday = date_create($today);
        $diff = date_diff($dateTripDate, $dateToday);
        $ret['dayNum'] = $diff->days;
        $ret['strToday'] = $today;
        echo json_encode($ret);
        Yii::app()->end();
    }

    //计算出行时间段是否大于半小时
    public function actionCalculateTimeSpan()
    {
        $tripTime1 = Yii::app()->request->getParam('trip_time1');
        $tripTime2 = Yii::app()->request->getParam('trip_time2');
        $ret['msg'] = '';
        if($tripTime1 > $tripTime2){
            $ret['msg'] = '出行时间段错误, 请重新输入';
            echo json_encode($ret);
            Yii::app()->end();
        }
        $tripTime1 = new DateTime($tripTime1);
        $tripTime2 = new DateTime($tripTime2);
        $diff = date_diff($tripTime1, $tripTime2);
        $diffMins = ($diff->h) * 60 + $diff->i;
        if($diffMins < 30){
            $ret['msg'] = '出行时间段不能小于30分钟, 请重新输入';
        }
        $ret['minInterval'] = $diffMins;
        echo json_encode($ret);
        Yii::app()->end();
    }

    public function calculateDays($startDay,$endDay,$from,$end,$absence_type='')
    {
        $xiujia = array();
        $tmpDay = $startDay = date('Ymd',strtotime($startDay));
        $endDay = date('Ymd',strtotime($endDay));
        $vService = new VacationService();

        while($tmpDay < date('Ymd',strtotime("$endDay +1 day"))){

            if($absence_type == 4 || $absence_type == 7 || $absence_type == VACATION_CHUCHAI) //婚假、产假算自然日
            {
                $xiujia[$tmpDay] = 1;

            }elseif(!$vService->isHoliday($tmpDay)){

                $xiujia[$tmpDay] = 1;

            }

            $tmpDay = date('Ymd',strtotime("$tmpDay +1 day"));
        }
        $dayNum = count($xiujia);
        if(isset($xiujia[$startDay])){
            if($from == 2){
                $dayNum -= 0.5;
            }elseif($from == 3){
                $dayNum -= 1;
            } 
        }

        if(isset($xiujia[$endDay])){
            if($end == 1){
                $dayNum -= 1; 
            }elseif($end == 2){
                $dayNum -= 0.5;
            }
        }
        return $dayNum;
    }


    public function createCancelAbsence()
    {
        $id = Yii::app()->request->getParam('absence_id');
        $model = new Absence();
        $arrData = $model->getItemByPk($id);
        $start_time = $arrData['start_time'];
        $end_time = $arrData['end_time'];
        if(!$this->checkQingjiaDate($start_time,$end_time)){
            //报错。说明销假超过当前考勤月了 
            throw new CHttpException(404,'销假只能在当前考勤月!');
        }
        $arrData['is_cancel'] = 1;
        $arrData['cancel_reason'] = Yii::app()->request->getParam('cancel_reason');
        $model->save($arrData);
        if($id < 1){
            Yii::log('cancel_absence save fail:'.print_r($arrData,true));
            throw new CHttpException(404,'保存数据失败,请重新提交');
        }
        return $id;
        
    }

    public function createCancelBusiness()
    {
        if(!empty($_POST['title']) && !empty($_POST['leave_reason']) && !empty($_POST['start_day']) && !empty($_POST['end_day']) && !empty($_POST['start_node']) && !empty($_POST['end_node'])){

            $btime = Yii::app()->request->getParam('start_day');
            $etime = Yii::app()->request->getParam('end_day');
            $start_node = Yii::app()->request->getParam('start_node');
            $end_node = Yii::app()->request->getParam('end_node');

            $start_time = OAConfig::nodeToTimeStamp($btime, $start_node);
            $end_time = OAConfig::nodeToTimeStamp($etime, $end_node);
            $dayNum = $this->calculateDays($btime, $etime, $start_node, $end_node);

            $user_id = Yii::app()->session['oa_user_id'];
            if(!$this->checkQingjiaDate($start_time,$end_time)){
                //报错。说明销假超过当前考勤月了 
                throw new CHttpException(404,'销出差只能在当前考勤月!');
            }

            $vacationService = new VacationService();
            if(!$vacationService->isVacation($user_id,$_POST['start_day'],$_POST['end_day'], VACATION_CHUCHAI)){

                throw new CHttpException(404,'没有出差记录的，不需要销假');

            }

            $time = time();
            $arrData = array();
            $arrData['user_id'] = $user_id;
            $arrData['create_time'] = $time;
            $arrData['start_time'] = $start_time;
            $arrData['end_time'] = $end_time;
            $arrData['status'] = STATUS_VALID;
            $arrData['day_number'] = $dayNum;
            $arrData['leave_reason'] = $_POST['leave_reason'];
            $arrData['title'] = $_POST['title'];
            $model = new CancelBusiness();
            $id = $model->save($arrData);

            if($id < 1){

                Yii::log('CancelBusiness save fail:'.print_r($arrData,true));
                throw new CHttpException(404,'保存数据失败,请重新提交');

            }

            return $id;

        }else{

            //报错，资料不全
            throw new CHttpException(404,'请将资料补充完整');

        }
    }

    //生成名片信息
    public function createBusinessCard()
    {
        $arrData = array();
        $arrData['user_name'] = $user_name = Yii::app()->request->getParam('user_name');
        $arrData['position'] = $position = Yii::app()->request->getParam('position');    
        $telephone = Yii::app()->request->getParam('telephone');
        $arrData['extension_number'] = $extension_number = Yii::app()->request->getParam('extension_number');
        $arrData['mobile'] = $mobile = Yii::app()->request->getParam('mobile');
        $arrData['count'] = $count = Yii::app()->request->getParam('count');
        $arrData['remark'] = $remark = Yii::app()->request->getParam('remark');

        if(empty($user_name) || empty($position) || empty($telephone) || empty($mobile) || empty($count)){
            //信息填写不完整
            throw new CHttpException(404,'请将资料补充完整');
        }
        $officeTelephone = OAConfig::officeTelephone();
        $arrData['telephone'] = $officeTelephone[$telephone];
        $arrData['user_id'] = Yii::app()->session['oa_user_id'];
        $arrData['status'] = STATUS_VALID;
        $model = new BusinessCard();
        $id = $model->save($arrData);
        if($id < 1){
            Yii::log('business_card save fail:'.print_r($arrData,true));
            throw new CHttpException(404,'保存数据失败,请重新提交');
        }
        return $id;
    }

    //上线单信息
    public function createOnlineOrder()
    {
        $arrData = array();
        $arrData['title'] = $title = Yii::app()->request->getParam('title');
        $arrData['upgrade_type'] = $upgrade_type = Yii::app()->request->getParam('upgrade_type');
        $arrData['description'] = $description = Yii::app()->request->getParam('description');
        $arrData['correction_bug'] = Yii::app()->request->getParam('correction_bug');
        $arrData['qa_type'] = $qa_type = Yii::app()->request->getParam('qa_type');
        $arrData['source_code'] = Yii::app()->request->getParam('source_code');
        $arrData['program_location'] = Yii::app()->request->getParam('program_location');
        $arrData['online_package_md5'] = Yii::app()->request->getParam('online_package_md5');
        $arrData['online_steps'] = $online_steps = Yii::app()->request->getParam('online_steps');
        $arrData['rollback_plan'] = Yii::app()->request->getParam('rollback_plan');
        $arrData['monitoring_plan'] = Yii::app()->request->getParam('monitoring_plan');
        $arrData['attachment_address'] = Yii::app()->request->getParam('file_path');
        $arrData['remark'] = Yii::app()->request->getParam('remark');
        $arrData['user_id'] = Yii::app()->session['oa_user_id'];
        $arrData['status'] = STATUS_VALID;
        $arrData['create_time'] = time();

        $auditList = Yii::app()->request->getParam('auditlist');

        $verificationNameArr['rd'] =  $auditList['A']['rd'];
        $auditList['A']['rd']      = OAConfig::checkUserAccount($verificationNameArr['rd'] );
        

        $verificationNameArr['pm'] = $auditList['A']['pm'];
        $auditList['A']['pm']       = OAConfig::checkUserAccount($verificationNameArr['pm'] );

        $verificationNameArr['qa'] = $auditList['A']['qa'];
        $auditList['A']['qa']       = OAConfig::checkUserAccount($verificationNameArr['qa'] );

        $verificationNameArr['rd_manger'] = $auditList['A']['rd_manger'];
        $auditList['A']['rd_manger']       = OAConfig::checkUserAccount($verificationNameArr['rd_manger'] );

        $verificationNameArr['yunying'] = $auditList['A']['yunying'];
        $auditList['A']['yunying']       = OAConfig::checkUserAccount($verificationNameArr['yunying'] );
        
        $verificationNameArr['ue'] = $auditList['A']['ue'];
        $auditList['A']['ue']       = OAConfig::checkUserAccount($verificationNameArr['ue'] );

        $verificationNameArr['op'] = $auditList['B']['op'];
        $auditList['B']['op']       = OAConfig::checkUserAccount($verificationNameArr['op'] );


        if(empty($title) || empty($upgrade_type) || empty($description) || empty($qa_type) || empty($online_steps) || empty($auditList['A']['rd']) || empty($auditList['A']['pm']) || empty($auditList['A']['qa']) || empty($auditList['A']['rd_manger']) || empty($auditList['B']['op'])){
            //信息填写不完整
            throw new CHttpException(404,'请将资料补充完整');
        }

        $_POST['auditlist'] = $auditList;

        $model = new OnlineOrder();
        $id = $model->save($arrData);
        if($id < 1){
            Yii::log('online_order save fail:'.print_r($arrData,true));
            throw new CHttpException(404,'保存数据失败,请重新提交');
        }
        return $id;
    }

    //团建报销
    public function createTeamBuilding()
    {
        $arrData['section_id'] = $sectionId = Yii::app()->request->getParam('section_id');  
        $arrData['amount'] = $amount = Yii::app()->request->getParam('amount');
        $arrData['activity_time'] = $activityTime = strtotime(Yii::app()->request->getParam('activity_time'));
        $arrData['title'] = $title = Yii::app()->request->getParam('title');
        $arrData['desc'] = $desc = Yii::app()->request->getParam('desc');
        $arrData['participant'] = $participant = Yii::app()->request->getParam('participant');
        $arrData['debt'] = $debt = Yii::app()->request->getParam('debt');
        $arrData['activity_type'] = $activity_type = Yii::app()->request->getParam('activity_type');
        $arrData['activity_address'] = Yii::app()->request->getParam('activity_address');
        $arrData['activity_story'] = Yii::app()->request->getParam('activity_story');

        //人均消费还是后端计算
        $arrData['person_num'] = $person_num = Yii::app()->request->getParam('person_num');
        $person_num = intval($person_num);

        if($person_num > 0) {
            
            $avg_amount = number_format($amount / $person_num, 2);

        }else {

            $avg_amount = 0;

        }
        $arrData['avg_amount'] = $avg_amount;
        

        if(empty($amount) || empty($avg_amount) || empty($person_num) || empty($sectionId) || empty($activityTime)){
            throw new CHttpException(404,'请将资料补充完整');
        }


        $arrData['user_id'] = Yii::app()->session['oa_user_id'];
        $arrData['create_time'] = time();
        $arrData['remark'] = Yii::app()->request->getParam('remark');
        $arrData['status'] = STATUS_VALID;

        $model = new TeamBuilding();

        $id = $model->save($arrData);
        if($id < 1){
            Yii::log('online_order save fail:'.print_r($arrData,true));
            throw new CHttpException(404,'保存数据失败,请重新提交');
        }
        return $id;
    }

    /**
     * 创建团建激励
     *
     **/
    public function createTeamEncourage() {

        $arrData['section'] = $sectionId = Yii::app()->request->getParam('section_id');  
        $arrData['amount'] = $amount = Yii::app()->request->getParam('amount');
        $arrData['remark'] = $remark = Yii::app()->request->getParam('remark');

        if(empty($amount) ||  empty($sectionId) || empty($remark) ){

            throw new CHttpException(404,'请将资料补充完整');

        }

        $arrData['user_id'] = Yii::app()->session['oa_user_id'];
        $arrData['create_time'] = time();
        $arrData['status'] = STATUS_VALID;

        $model = new TeamEncourage();
        $id = $model->save($arrData);

        if($id < 1){
            Yii::log('online_order save fail:'.print_r($arrData,true));
            throw new CHttpException(404,'保存数据失败,请重新提交');
        }

        return $id;

    }


 //    public function createAuditList($userWorkflowId)
 //    {
 //        $userWorkflowModel = new UserWorkFlow();
 //        $userWorkflow = $userWorkflowModel->getItemByPk($userWorkflowId);
 //        $userWorkflowObjType = $userWorkflow['obj_type'];
 //        $auditList = Yii::app()->request->getParam('auditlist');
 //        $oa_user_id = Yii::app()->session['oa_user_id'];

 //        $userModel = new User();
 //        $taskModel = new UserTask();
 //        $model = new AuditList();
 //        $index = 0;


        
 //        foreach($auditList as $step => $row){

 //            if(empty($row)){
 //                continue;
 //            }

 //            foreach($row as $business_role => $val){

 //                if(empty($val)){
 //                    continue;
 //                }
    
 //                ++ $index;
 //                if($business_role == "op")
 //                {
 //                    $index = 1;
 //                }

 //                $name = $val;
 //                //支持分割符号：中文逗号、|、换行符、制表符、下划线
 //                $name = str_replace(array("，", "|", "\n", "\t", "_"," "), ',', $name);

 //                //判断是否多个用户邮箱
 //                if( strpos($name, ',') !== false ) {
                    
 //                    $rd_arr = array();
 //                    $rd_arr = explode(",", $name);
 //                    foreach($rd_arr as $rd_val) {

 //                        if(empty($rd_val)){
 //                            continue;
 //                        }

 //                        $is_add = $this->addRdQueueList($step, $business_role, $rd_val, $userWorkflowId, $index);
 //                        if(!$is_add){

 //                            continue;

 //                        }

 //                    }


 //                }else {

 //                    $is_add = $this->addRdQueueList($step, $business_role, $val, $userWorkflowId, $index); 

 //                    if(!$is_add){

 //                        continue;

 //                    }

 //                }

 //            }

 //        }

 //        if($userWorkflowObjType == 'online_order') {
 //            //复制C节点的值,有些细节处理，可能代码有相同
 //            $index = 0;
 //            foreach ($auditList as $step => $row) {

 //                if (empty($row)) {
 //                    continue;
 //                }

 //                foreach ($row as $business_role => $val) {

 //                    if (empty($val) || $business_role == "ue" || $business_role == "rd_manger" || $business_role == "op") {
 //                        continue;
 //                    }

 //                    ++$index;
 //                    $name = $val;
 //                    //支持分割符号：中文逗号、|、换行符、制表符、下划线
 //                    $name = str_replace(array("，", "|", "\n", "\t", "_", " "), ',', $name);

 //                    //判断是否多个用户邮箱
 //                    if (strpos($name, ',') !== false) {

 //                        $rd_arr = array();
 //                        $rd_arr = explode(",", $name);
 //                        foreach ($rd_arr as $rd_val) {

 //                            if (empty($rd_val)) {
 //                                continue;
 //                            }
 //                            $users = $userModel->getItemByEmailOrName(trim($rd_val));
 //                            $user = reset($users);
 //                            if (empty($user)) {
 //                                continue;
 //                            }

 //                            $this->addCStepQueueList($user['id'], $business_role, $index, $userWorkflowId);

 //                        }


 //                    } else {
 //                        $users = $userModel->getItemByEmailOrName(trim($val));
 //                        $user = reset($users);
 //                        if (empty($user)) {
 //                            continue;
 //                        }
 //                        $this->addCStepQueueList($user['id'], $business_role, $index, $userWorkflowId);

 //                    }

 //                }

 //            }

 //        }

 //    }

 //    public function addCStepQueueList($user_id,$business_role,$index,$userWorkflowId)
	// { 
 //        $model = new AuditList();
 //        $tmp = array();
 //        $tmp['pos'] = $index;
 //        $tmp['user_id'] = $user_id;
 //        $tmp['business_role'] = $business_role;
 //        $tmp['user_workflow_id'] = $userWorkflowId;
 //        $tmp['workflow_step'] = 'C';
 //        $tmp['create_time'] = time();
 //        $tmp['status'] = STATUS_VALID; 
 //        $model->save($tmp);
	// }

 //    public function addRdQueueList($step, $business_role, $name, $userWorkflowId, $index) {

 //        $model = new AuditList();
 //        $userModel = new User();
 //        $taskModel = new UserTask();
 //        $oa_user_id = Yii::app()->session['oa_user_id'];
 //        $email = OAConfig::checkUserAccount($name);
 //        $users = $userModel->getItemByEmailOrName(trim($name));
 //        $user = reset($users);

 //        if( empty($user) ) {

 //            return false;

 //        }

 //        $tmp = array();
 //        $tmp['pos'] = $index;
 //        $tmp['user_id'] = $user['id'];
 //        $tmp['business_role'] = $business_role;
 //        $tmp['user_workflow_id'] = $userWorkflowId;
 //        $tmp['workflow_step'] = $step;
 //        $tmp['create_time'] = time();

 //        if($business_role == 'rd' && $user['id'] == $oa_user_id){

 //            //生成已经审批的记录 
 //            $tmp['status'] = STATUS_INVALID;
 //            $row = array();
 //            $row['user_id'] = $user['id'];
 //            $row['user_workflow_id'] = $userWorkflowId;
 //            $row['audit_user_id'] = $user['id'];
 //            $row['workflow_step'] = $step;
 //            $row['audit_status']  = OA_TASK_TONGGUO;
 //            $row['audit_desc']    = 'rd';
 //            $row['create_time']   = $row['modify_time'] = time();
 //            $taskModel->save($row);

 //        }else{

 //            $tmp['status'] = STATUS_VALID; 

 //        }

 //        $model->save($tmp);

 //    }

    //流程搜索页
    public function actionSuperadmin() {
        
        $check_flag = Yii::app()->request->getParam('check_flag');//审核中
        $obj_type   = Yii::app()->request->getParam('obj_type' );
        $name      = Yii::app()->request->getParam('name' );
        $check_name = Yii::app()->request->getParam('check_name', '');
        $rn         = Yii::app()->request->getParam('rn', 50);
        $pn         = Yii::app()->request->getParam('pn', 1);

        $tplData['name']         = $name;
        $tplData['check_name']   = $check_name;

        $obj_type_list = array(

            'absence'        => '请假',
            'cancel_absence' => '销假',
            'team_building'  => '团建',
            'daily_reimbursement'  =>'日常报销',
            'online_order'   => '上线单'

        );

        $tplData['obj_type']     = $obj_type;
        $tplData['obj_type_list']= $obj_type_list;


        if(empty($check_flag)){

            $this->renderFile("./superAdmin.tpl",$tplData);
            die();

        }

        if(empty($name) && empty($check_name) ) {

            $tplData['msg'] = "必须填写申请人或者审核人."; 
            $this->renderFile("./superAdmin.tpl",$tplData);
            die();

        }

        //init
        $modelUserWorkFlow = new UserWorkFlow();
        $modelFlow = new WorkFlow();
        $modelUserRole = new UserRole();
        $modelUser = new User();


        //申请人
        if( !empty($name) )  {
            $userData = $modelUser->getItemByEmailOrName($name);

            if(empty($userData) ) {

                $tplData['msg'] = "没有获取到申请人!"; 
                $this->renderFile("./superAdmin.tpl",$tplData);
                die();
            }
        } 

        $userId = $userData[0]['id'];
        $condition['user_id'] = $userId;
        $condition['check_flag'] = $check_flag;

        if(!empty($obj_type) ) {

            $condition['obj_type']   = $obj_type;

        }

        //审批人
        if(!empty($check_name)) {
            $checkUserData = $modelUser->getItemByEmailOrName($check_name);
            if(empty($checkUserData) ) {

                $tplData['msg'] = "没有获取到审核人!"; 
                $this->renderFile("./superAdmin.tpl",$tplData);

            }

            $check_user_id = $checkUserData[0]['id'];
            $roleData = $modelUserRole -> getUserRoleByUserId($check_user_id);

            if(!empty($roleData)) {
                $roleData = array_unique($roleData);
                $roleStr   = implode(",", $roleData);
            }
            $condition['role_id']          = $roleStr;
            $condition['check_user_id']    = $check_user_id;
        }

        //查询返回结果
        $data = $modelUserWorkFlow -> getUserWorkFlow($condition, $pn, $rn);

        if(empty($data) ) {
            $tplData['msg'] = "没有获取到审核中记录!"; 
            $this->renderFile("./superAdmin.tpl",$tplData);
        }

        $tplData['page_count'] = $data['total']; 
        unset($data['total']);
        $tplData['userWorkflows']   = $data;
        $tplData['cur_page'] = $pn; 
        $tplData['per'] = $rn; 
        $obj_type_arr = $obj_type_list; 
        $obj_type_arr['business'] = '出差';
        $tplData['obj_type_arr'] = $obj_type_arr; 

        $this->renderFile("./superAdmin.tpl",$tplData);
    }

    //借款管理
    public function actionManageLoan(){
        $status = Yii::app()->request->getParam('status');
        $model = new FinanceLoan();
        if($status == 1){
            $arrData['list'] = $model->getPayedLoanList();
        }elseif($status == 2){
            $arrData['list'] = $model->getHrLoanList();
        }else{
             $arrData['list'] = $model->getUnpayedLoanList();
        }
        $arrData['status'] = $status;

        $this->renderFile("./loan_list.tpl",$arrData);
    }

    public function actionCalculateBusiness() {

        $btime = Yii::app()->request->getParam('start_day');
        $etime = Yii::app()->request->getParam('end_day');
        $from = Yii::app()->request->getParam('start_node');
        $end = Yii::app()->request->getParam('end_node');
        $absenceType = Yii::app()->request->getParam('absence_type');
        $type = Yii::app()->request->getParam('type');
        $ret['msg'] = '';
        $user_id = Yii::app()->session['oa_user_id'];
        $start_time = OAConfig::nodeToTimeStamp($btime,$from);
        $end_time = OAConfig::nodeToTimeStamp($etime,$end);

        
        if($type == 'cancel_business'){

            $vService = new VacationService();
            $is_chucha = $vService->isVacation($user_id,$btime,$etime,VACATION_CHUCHAI);

            if(!$is_chucha){
                $ret['dayNum'] = 0;
                $ret['msg'] = '没有出差记录不需要销假'; 
                echo json_encode($ret);
                Yii::app()->end();
            } 

            //判断用户是否已经
            $model = new CancelBusiness();
            $is_cancel = $model->isHaveRecord($user_id,$start_time,$end_time);

            if($is_cancel){
                $ret['dayNum'] = 0;
                $ret['msg'] = '已有销出差记录，不能再次销出差'; 
                echo json_encode($ret);
                Yii::app()->end();
            }

        }else{


            $absenceModel = new Absence();
            $absences = $absenceModel->getValidItemsByDay($user_id,$start_time,$end_time);

            if(!empty($absences)){

                //判断用户是否已经销过假
                $canCelmodel = new CancelAbsence();
                if(!$canCelmodel->isHaveRecord($user_id,$start_time,$end_time)){
                    $ret['dayNum'] = 0;
                    $ret['msg'] = '已有请假记录，不能再次申请';
                    echo json_encode($ret);
                    Yii::app()->end();

                }

            }

            $chucha = $absenceModel->getValidItemsByDay($user_id,$start_time,$end_time, VACATION_CHUCHAI);
            if(!empty($chucha)){

                $canCelmodel = new CancelBusiness();
                $is_cancel = $canCelmodel->isHaveRecord($user_id,$start_time,$end_time);

                if(!$is_cancel){

                    $ret['dayNum'] = 0;
                    $ret['msg'] = '已有出差记录，不能再次申请';
                    echo json_encode($ret);
                    Yii::app()->end();

                }


            }

        }



        $ret['dayNum'] = $this->calculateDays($btime,$etime,$from,$end,$absenceType);
        echo json_encode($ret);
        Yii::app()->end();

    }

    public function actionCalculateBusinessOut() {

        $btime = Yii::app()->request->getParam('start_day');
        $etime = Yii::app()->request->getParam('end_day');
        $from = Yii::app()->request->getParam('start_node');
        $end = Yii::app()->request->getParam('end_node');
        $absenceType = Yii::app()->request->getParam('absence_type');
        $type = Yii::app()->request->getParam('type');
        $ret['msg'] = '';
        $user_id = Yii::app()->session['oa_user_id'];
        $start_time = OAConfig::nodeToTimeStamp($btime,$from);
        $end_time = OAConfig::nodeToTimeStamp($etime,$end);


        if($type == 'cancel_business_out'){

            $vService = new VacationService();
            $is_chucha = $vService->isVacation($user_id,$btime,$etime,VACATION_CHUCHAI);

            if(!$is_chucha){
                $ret['dayNum'] = 0;
                $ret['msg'] = '没有出差记录不需要销假';
                echo json_encode($ret);
                Yii::app()->end();
            }

            //判断用户是否已经
            $model = new CancelBusiness();
            $is_cancel = $model->isHaveRecord($user_id,$start_time,$end_time);

            if($is_cancel){
                $ret['dayNum'] = 0;
                $ret['msg'] = '已有销出差记录，不能再次销出差';
                echo json_encode($ret);
                Yii::app()->end();
            }

        }else{


            $absenceModel = new Absence();
            $absences = $absenceModel->getValidItemsByDay($user_id,$start_time,$end_time);

            if(!empty($absences)){

                //判断用户是否已经销过假
                $canCelmodel = new CancelAbsence();
                if(!$canCelmodel->isHaveRecord($user_id,$start_time,$end_time)){

                    $ret['dayNum'] = 0;
                    $ret['msg'] = '已有请假记录，不能再次申请';
                    echo json_encode($ret);
                    Yii::app()->end();

                }

            }

            $chucha = $absenceModel->getValidItemsByDay($user_id,$start_time,$end_time, VACATION_CHUCHAI);
            if(!empty($chucha)){

                $canCelmodel = new CancelBusiness();
                $is_cancel = $canCelmodel->isHaveRecord($user_id,$start_time,$end_time);

                if(!$is_cancel){

                    $ret['dayNum'] = 0;
                    $ret['msg'] = '已有出差记录，不能再次申请';
                    echo json_encode($ret);
                    Yii::app()->end();

                }


            }

            $businessOut = $absenceModel->getValidItemsByDay($user_id,$start_time,$end_time, VACATION_YINGONGWAICHU);
            if(!empty($businessOut)){
                $ret['dayNum'] = 0;
                $ret['msg'] = '已有因公外出记录，不能再次申请';
                echo json_encode($ret);
                Yii::app()->end();
            }

        }



        $ret['dayNum'] = $this->calculateDays($btime,$etime,$from,$end,$absenceType);
        echo json_encode($ret);
        Yii::app()->end();

    }

    public function actionDelFlow() {

        $id = Yii::app()->request->getParam('id');
        $flowModel = new UserWorkFlow();
        $oa_user_id  = intval(Yii::app()->session['oa_user_id']);

        $flowInfo = $flowModel->getItemByPk($id);

        $ret['code'] = "-1";

        if( $flowInfo['user_id'] != $oa_user_id) {

            $msg = "操作失败，当前流程没有操作权限!";
            $ret['msg'] = $msg;
            echo json_encode($ret, true);
            die();

        }

        if( $flowInfo['audit_status'] != OA_WORKFLOW_AUDIT_SHENPIZHONG ) {

            $msg = "操作失败，当前流程不处于审核中，请刷新页面!";
            $ret['msg'] = $msg;
            echo json_encode($ret, true);
            die();

        }

        $taskModel = new UserTask();
        $userModel = new User();
        $flow = $taskModel->getLastTaskByUwfId($id);

        //删除审批记录
        // $is_task_del = $taskModel->delByFlowId($id);

        //删除申请流程
        $is_flow_del = $flowModel->delByFlowId($id);

        //对销假流程做特殊处理
        if($flowInfo['obj_type'] == 'cancel_absence'){
            $absenceModel = new Absence();
            $arrData = $absenceModel->getItemByPk($flowInfo['obj_id']);
            $arrData['is_cancel'] = 0;
            $absenceModel->save($arrData);
        }

        //是否发送邮件给相关人
        if($is_flow_del && !empty($flow['user_id']) ) {

            $user_id = $flow['user_id'];
            $userInfo = $userModel->getItemByPk($user_id);
            $ownInfo = $userModel->getItemByPk($oa_user_id);

            $tmpModel = new WorkFlow();
            $flowName = $tmpModel->getObjType($flowInfo['obj_id'], $flowInfo['obj_type']);
            if($flowName) {

                $flowName = "({$flowName})";

            }else {

                $flowName = "";

            }

            $userName = $userInfo['name'];
            if(empty($userName) ) {

                $userName = "";

            }

            $email = $userInfo['email']."@rong360.com";
            //发邮件
            // $mailer = Yii::app()->mailer->_mailer;
            // $mailer->Subject = "取消申请流程提醒";
            // $mailer->AddAddress($email);
            // $mailer->Body = "您OA上最近一个需要审批流程{$flowName}，由于申请人<strong>{$ownInfo['name']}</strong>自己取消申请流程,请忽略之前提醒审批的邮件！";
            // $mailer->send();

        }
    
        $ret['code'] = "1";
        echo json_encode($ret, true);
        die();


    }

    public function checkRole($user_id){

        $userRoleModel = new UserRole();
        $roleIds = $userRoleModel->getUserRoleByUserId($user_id);
        if (!empty(array_intersect($roleIds,array(1,2)))){
            return true;
        }else{
            return false;
        }

    }

    //根据工龄给出员工年假天数
    public function getNianjiaNumber($work_date) {

        $num = 0;
        $year = date('Y',time()) - date('Y',strtotime($work_date));
        $month = date('m',time()) - date('m',strtotime($work_date));
        $monthTotal = $year*12 + $month;

        if($monthTotal > 20*12){
            $num = 15;
        }else{
            $num = 10;
        }

        return $num;

    }

    public function actionSend() {


        $mail = Yii::app()->mailer->_mailer;
        $email = "netinfo@i-yc.com";
        $mail->Subject = "申请流程提醒";
        $mail->AddAddress($email);
        $mail->Body = "您OA上最近一个需要审批流程，由于申请人<strong>陈小艺</strong>自己取消申请流程,请忽略之前提醒审批的邮件！";
        echo "<pre>";
        var_dump($mail);

        $isSend = $mail->send();
        var_dump($isSend);
        die();

    }


}

?>
