<?php
class SiteController extends Controller
{
    public function actions()
    {

    }
    public function actionIndex()
    {
        $this->redirect(Yii::app()->createURL('userworkflow/list'));
    }
    public function actionLogin()
    {
        if(Yii::app()->session['oa_user_id']){
            $this->redirect(Yii::app()->createURL('userworkflow/list'));
        }
        if(Yii::app()->request->getIsPostRequest()){

            $userModel = new User();
            $email = Yii::app()->request->getPost('email'); 
            $password = Yii::app()->request->getPost('password');
            $user = $userModel->getItemByEmail($email);

            if($user['is_leave'] == 2 && strtotime($user['leave_date'])<=time()){
                $msg = "您没有权限登录OA"; 
                Yii::app()->request->redirect(Yii::app()->createURL('site/login')."?msg=".urlencode($msg));
            }
            if(strpos($email,'@')){
                $email =substr($email,0,strpos($email,'@'));
            }
            if(empty($email) || empty($password)){
                $msg = "用户名、密码错误请重新登陆"; 
                Yii::app()->request->redirect(Yii::app()->createURL('site/login')."?msg=".urlencode($msg));
            }
            $accountModel = new AccountModel();
            $valid = $accountModel->validEmailAccount($email,$password);
            if( !$valid ){
                Yii::log('email or password is invalid','info');
            }
            if( !$user ){
                Yii::log('user is invalid/'.$email,'info');
            }
            if(!$valid || !$user){
                $msg = "用户名、密码错误请重新登陆"; 
                Yii::app()->request->redirect(Yii::app()->createURL('site/login')."?msg=".urlencode($msg));
            }
            // elseif(empty($user['register_home'])){
            //     //注册home用户
            //     $path = dirname(__FILE__).'/../../discuz/';
            //     require_once($path.'config/config_ucenter.php');
            //     require_once($path.'uc_client/client.php');
            //
            //     $flag = uc_user_checkname($user['email']);
            //     if($flag == 1){
            //         $uid = uc_user_register($user['email'], $password, $user['email'].'@rong360.com');
            //         if($uid > 0){
            //             $user['register_home'] = 1;
            //             $userModel->save($user);
            //         }
            //     }elseif($flag == -3){//用户已经存在
            //         $user['register_home'] = 1;
            //         $userModel->save($user);
            //     }
            // }
            /*
            else{
                $path = dirname(__FILE__).'/../../discuz/';
                require_once($path.'config/config_ucenter.php');
                require_once($path.'uc_client/client.php');
                list($uid, $username, $password, $email) = uc_user_login($user['email'], $password); 
                if($uid > 0){
                    $success_content = uc_user_synlogin($uid);
                }
            }
             */
            Yii::app()->session['oa_user_id'] = $user['id'];
            Yii::app()->request->redirect(Yii::app()->createURL('userworkflow/my'));
        }else{
            $arrData['msg'] = Yii::app()->request->getParam('msg');
            $this->renderFile('login.tpl',$arrData); 
        }
    }

    public function actionLogout()
    {
        Yii::app()->session['oa_user_id'] = '';    
        Yii::app()->request->redirect(Yii::app()->createURL('site/login'));
    }
}
