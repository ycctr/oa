<?php
class RoleController extends Controller
{
    private $_model;

    public function __construct()
    {
        parent :: __construct('role', null);
        $this->_model = new Role();
    }
    public function actionIndex()
    {

    }
    
    public function actionList()
    {
        $arrCondition = array();
        $arrQuery = Yii::app()->request->getQuery('query');
        if (!empty($arrQuery))
        {
            foreach ($arrQuery as $key => $value)
            {
                if (!empty($value))
                {
                    $arrCondition[$key] = $value;
                }
            }
        }
        $arrCondition['status'] = 1;
        //$intPageSize = Yii::app()->params['pageSize'];
        $intPageSize = 50;
        $intPage = intval(Yii::app()->request->getQuery('page')) < 1 ? 1 : intval(Yii::app()->request->getQuery('page'));

        $arrParams = array(
            'condition' => $arrCondition,
            'offset'    => ($intPage - 1) * $intPageSize,
            'limit'     => $intPageSize,
            'where'  => '1=1',
        );
        $this->_model->setParams($arrParams);

        $arrTplData['arrPager'] = array(
            'count'     => $this->_model->getCount(),
            'pagesize'  => $intPageSize,
            'page'      => $intPage,
            'pagelink'  => empty($arrQuery) ? '?page=%d' : '?' . urldecode(Yii::app()->request->getQueryString()) . '&page=%d',
        );

        $arrTplData['arrList'] = $this->_model->getList();

        // *** 用来控制前端权限显示
        $oa_user_id = intval(Yii::app()->session['oa_user_id']);
        $right = new Right();
        $right_list = $right->getPrivilegeByUserID($oa_user_id);

        $arrTplData['add_p'] = 0;
        $arrTplData['view_p'] = 0;
        $arrTplData['edit_p'] = 0;
        $arrTplData['del_p'] = 0;
        $arrTplData['recover_p'] = 0;
        $controllerName = strtolower($this->id);
        foreach($right_list as $key=>$value)
        {
            if($value['controller'] == $controllerName)
            {
                if($value['action'] == 'add')
                {
                    $arrTplData['add_p'] = 1;
                }
                if($value['action'] == 'view')
                {
                    $arrTplData['view_p'] = 1;
                }
                if($value['action'] == 'edit')
                {
                    $arrTplData['edit_p'] = 1;
                }
                if($value['action'] == 'del')
                {
                    $arrTplData['del_p'] = 1;
                }
                if($value['action'] == 'recover')
                {
                    $arrTplData['recover_p'] = 1;
                }
            }
        }
        $arrTplData['error'] = Yii::app()->request->getParam("error");
        //    var_dump($arrTplData['error']);
        //*****************
        //var_dump($arrTplData);exit;
        $this->renderFile('role_list.tpl', $arrTplData);

    }
    public function packageRights($role_id)
    {
        $rights = $this->_model->getRight($role_id);
        foreach($rights as $key=>$value)
        {
            $tmp = array();
             if($value['action'] == 'list' || $value['action'] == 'manage')
             {
                $arrRight[$value['controller']]['id'] = $value['id'];
                $arrRight[$value['controller']]['title'] = $value['title'];
                $arrRight[$value['controller']]['selected'] = $value['is_checked'];
             }else if($value['action'] != 'editpost') //对于editppost单独处理，和edit为同一功能，应该绑定在一起
             {
                $tmp['id'] = $value['id'];
                $tmp['title'] = $value['title'];
                $tmp['selected'] = $value['is_checked'];
                $arrRight[$value['controller']]['op'][] = $tmp;
             }
        }
        return $arrRight;
    }
    public function actionAdd()
    {

        if (Yii::app()->request->getIsPostRequest())
        {
            $arrRightPost = (array)Yii::app()->request->getPost('right');
            $arrRolePost = (array)Yii::app()->request->getPost('role');
            $arrRolePost['status']  = 1;
            $arrRolePost['create_time'] = time();
            $arrRolePost['modify_time'] = time();
             // 存之前判断是否重复
            $role_id = $this->_model->save($arrRolePost); //先存角色记录
            //对于edit editpost进行处理，之前代码留下来的不一致，只有两处。不再使用之前的方法。
            if(in_array("7",$arrRightPost))
            {
                array_push($arrRightPost,8);
            }
            if(in_array("11",$arrRightPost))
            {
                array_push($arrRightPost,12);
            }
            $this->_model->saveRoleRight($arrRightPost, $role_id); // 再存角色权限记录
            $logModel = new logModel();
            $logModel->RoleRightSave(intval(Yii::app()->session['oa_user_id']),$role_id,'',null,$arrRolePost['name'],$arrRightPost,ROLE_ADD);
            Yii::app()->request->redirect(Yii::app()->createURL('role/list'));
            return;
        }
        
        $arrRight = self::packageRights(-1);
        $arrTplData['is_edit'] = 0;
        $arrTplData['arrList'] = $arrRight;
        $this->renderFile('role_edit.tpl', $arrTplData);
    }

    public function actionDel()
    {
        $role_id = intval(Yii::app()->request->getQuery('id'));
        $arrParams['condition']['role_id'] = $role_id;
        $UserRole = new UserRole();
        $UserRole->setParams($arrParams);
        $num = $UserRole->getCount();
        if( $num != 0)
        {
            Yii::app()->request->redirect(Yii::app()->createURL("Role/list")."?error=conflict");
            return;
        }

        $arrInfo = array();
        $arrInfo['id'] = intval(Yii::app()->request->getQuery('id'));  
        $arrInfo['status'] = 0;
        $arrInfo['modify_time'] = time();
        $this->_model->save($arrInfo);

        $role_id = intval(Yii::app()->request->getQuery('id')); 
        $this->_model->DeleteItemByRoleId($role_id);

        $RoleRight = new RoleRight();
        $arrRight = $RoleRight->getRoleRights($role_id);
        $RoleRight->DeleteRoleRight($role_id);
/*
        $role_id = intval(Yii::app()->request->getQuery('id'));  
        $strSql = sprintf("update user_role set status=0 where role_id=%d", $role_id);
        $this->_model->db()->query($strSql);

        $role_id = intval(Yii::app()->request->getQuery('id'));
        $strSql = sprintf("select right_id from role_right where role_id=%d", $role_id);
        $arrRight =  $this->_model->db()->query($strSql);
        $strSql = sprintf("delete from role_right where role_id=%d", $role_id);
        $this->_model->db()->query($strSql);
*/
        $role_name = Yii::app()->request->getQuery('name');
        $logModel = new logModel();
        $logModel->RoleRightSave(intval(Yii::app()->session['oa_user_id']),$role_id,$role_name,$arrRight,'',null,ROLE_DEL);
        Yii::app()->request->redirect(Yii::app()->createURL('role/list'));
        //Yii::app()->end();
    }
    public function actionEdit()
    {
        if (Yii::app()->request->getIsPostRequest())
        {
            $arrRolePost = (array)Yii::app()->request->getPost('role');
            $arrRightPost = (array)Yii::app()->request->getPost('right');

            $role_id = intval($arrRolePost['id']);
            $role_old_name = $this->_model->getRoleNameByRoleId($role_id);
            $arrRole_old_rights = $this->_model->getRightListByRoleId($role_id);

            //对于edit editpost进行处理，之前代码留下来的不一致，只有两处。不再使用之前的方法。
            if(in_array("7",$arrRightPost))
            {
                array_push($arrRightPost,8);
            }
            if(in_array("11",$arrRightPost))
            {
                array_push($arrRightPost,12);
            }

            if($role_old_name != $arrRolePost['name'] ||
                (count(array_diff($arrRole_old_rights,$arrRightPost)) != 0 
                    || count(array_diff($arrRightPost,$arrRole_old_rights))))
            {
                $this->_model->setRiskCheck(0);
                $arrRolePost['modify_time'] = time();
                $this->_model->save($arrRolePost); 
            
                $this->_model->saveRoleRight($arrRightPost, $role_id); 
                $logModel = new logModel();
                $logModel->RoleRightSave(intval(Yii::app()->session['oa_user_id']),$role_id,$role_old_name,$arrRole_old_rights,$arrRolePost['name'],$arrRightPost,ROLE_EDIT);
            }
            Yii::app()->request->redirect(Yii::app()->createURL('role/list'));
            return;
        }
        $arrTplData = self::getTplData(intval(Yii::app()->request->getParam('id'))); 
        $arrTplData['is_edit'] = 1;
        $this->renderFile('role_edit.tpl', $arrTplData); 
    }

    public function getTplData($role_id = -1)
    {
        $arrTplData = array();
        $arrRight = array();

        //$role_id = intval(Yii::app()->request->getParam('id'));
        $arrRight = self::packageRights($role_id); 
        $this->_model->setParams(array('id' => $role_id));
        $role = $this->_model->getItem();

        $arrTplData['role_id'] = $role_id;
        $arrTplData['role_name'] = $role['name'];
        $arrTplData['arrList'] = $arrRight;
        return  $arrTplData;
    }
    public function actionView()
    {
        $arrTplData = self::getTplData(intval(Yii::app()->request->getParam('id')));
        $arrTplData['is_view'] = 1;
        $this->renderFile('role_edit.tpl', $arrTplData); 

    }
    public function actionRecover()
    {

    }
    public function actionRoleValidate()
    {
        $role_name = Yii::app()->request->getParam("role_name");
        $role_id = Yii::app()->request->getParam("role_id");
        if(empty($role_name))
        {
            return false;
        }
        $arrCondition = array();
        $arrParams = array();

        if(!empty($role_id) ) {

            $arrParams['where']  = sprintf(" id != %d AND name = '%s'",intval($role_id), $role_name);

        } else {

            $arrParams['where']  = sprintf(" name = '%s'", $role_name);

        }

        $this->_model->setParams($arrParams);
        $num = $this->_model->getCount();
        $ret = true;

        if($num != 0) {
            $ret = false;
        }

        echo json_encode($ret);
        Yii::app()->end(); 
    }
}
