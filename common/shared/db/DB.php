<?php
/*
* class DB
*
* by zdj
*
*
 * */
class DB
{
    // hook types
    const HK_BEFORE_QUERY = 0;
    const HK_AFTER_QUERY = 1;

    // query result types
    const FETCH_RAW = 0;    // return raw mysqli_result
    const FETCH_ROW = 1;    // return numeric array
    const FETCH_ASSOC = 2;  // return associate array
    const FETCH_OBJ = 3;    // return DBResult object

    private $mysql = NULL;
    private $dbConf = NULL;
    private $isConnected = false;  //目前这个值不准了。
    
    private $checkClosed = false;
    
    private $lastSQL = NULL;
    private $mode = 'rw'; // rw, ro

    private $enableProfiling = false;
    private $arrCost = NULL;
    private $lastCost = 0;
    private $totalCost = 0;

    private $hkBeforeQ = array();
    private $hkAfterQ = array();
        
    private $tablehkBeforeQ = array();
    private $tablehkAfterQ = array();
    
    private $onfail = NULL;

    private $sqlAssember = NULL;

    private $txStatus = 0;//transaction status 事务状态 1:开启 0:关闭
    private $acStatus = 1;//autocommit  status 自动提交 1:开启 0:关闭,默认值依赖数据库配置的

    private $shardingStrategy = array();
    //////////////////////////// init ////////////////////////////

    public function __construct($enableProfiling = false,$mysqli=false, $mode='rw', $shardingStrategy=false)
    {
        if($mysqli !== false)
        {
            $this->mysql = $mysqli;
        }
        else{
            $this->mysql = mysqli_init();
        }
        if($enableProfiling)
        {
            $this->enableProfiling(true);
        }

        if($mode=='ro') $this->mode='ro';
        if($shardingStrategy) {
        	$this -> shardingStrategy = $shardingStrategy;
        }
    }

    public function __destruct()
    {
        $this->close();
    }

    public function setOption($optName, $value)
    {
        return $this->mysql->options($optName, $value);
    }

    // use it before connect and reconnect
    public function setConnectTimeOut($seconds)
    {
        if($seconds <= 0)
        {
            return false;
        }
        return $this->setOption(MYSQLI_OPT_CONNECT_TIMEOUT, $seconds);
    }

    public function __get($name)
    {
        switch($name)
        {
            case 'error':
                return $this->mysql->error;
            case 'errno':
                return $this->mysql->errno;
            case 'insertID':
                return $this->mysql->insert_id;
            case 'affectedRows':
                return $this->mysql->affected_rows;
            case 'lastSQL':
                return $this->lastSQL;
            case 'lastCost':
                return $this->lastCost;
            case 'totalCost':
                return $this->totalCost;
            case 'isConnected':
                return $this->isConnected;
            case 'db':
                return $this->mysql;
            default:
                return NULL;
        }
    }

    //////////////////////////// connection ////////////////////////////

    public function connect($host, $uname = NULL, $passwd = NULL, $dbname = NULL, $port = NULL, $flags = 0)
    {
        $port = intval($port);
        if(!$port)
        {
            $port = 3306;
        }

        $this->dbConf = array(
            'host' => $host,
            'port' => $port,
            'uname' => $uname,
            'passwd' => $passwd,
            'flags' => $flags,
            'dbname' => $dbname,
        );

        $this->isConnected = $this->mysql->real_connect(
            $host, $uname, $passwd, $dbname, $port, NULL, $flags
        );

        return $this->isConnected;
    }
    
    public function setCheckClosed($bol)
    {
        $this->checkClosed = $bol;
    }

    public function reconnect()
    {
        if($this->dbConf === NULL)
        {
            return false;
        }
        $conf = $this->dbConf;
        $this->isConnected = $this->mysql->real_connect(
            $conf['host'], $conf['uname'], $conf['passwd'],
            $conf['dbname'], $conf['port'], NULL, $conf['flags']
        );

        return $this->isConnected;
    }

    public function close()
    {
        if(!$this->isConnected)
        {
            return;
        }
        $this->isConnected = false;
        $this->mysql->close();
    }

    // note: mysqli.reconnect should be off
    public function isConnected($bolCheck = false)
    {
        if($this->isConnected && $bolCheck && !$this->mysql->ping())
        {
            $this->isConnected = false;
        }
        return $this->isConnected;
    }
    
    /**
     * 执行多条SQL
     * 返回最后一条SQL的结果集
     */
    public function multi_query() {
    	$args = func_get_args();
    	$sql = $args[0];
    	if ($this -> mysql -> multi_query($sql)) {
    		do {
    			$lastResult = array();
    			/* store first result set */
    			if ($result = $this -> mysql -> store_result()) {
    				while ($row = $result -> fetch_row()) {
    					$lastResult[] = $row;
    				}
    				$result->free();
    			}
    		} while ($this -> mysql -> more_results() && $this -> mysql -> next_result());
    	}
    	return $lastResult;
    }

    /**
     * variable length parameters
     * @param $sql
     * @param $types
     * @param $bindParams
     */
    public function query()
    {
        $args = func_get_args();
        $sql = $args[0];
        $bindParams = array_slice($args, 1);
        
        //只读方式检查。
        if($this->mode=='ro' && preg_match('/^\s*(insert|update|delete)\s/i', $sql)){
            throw new SystemException("当前数据库连接为只读，不可执行INSERT,UPDATE,DELETE操作: " . $sql);
        }

        $this->lastSQL = $sql;
        if(is_array($bindParams) && !empty($bindParams))
            $this->lastSQL .= ' params:' .json_encode($bindParams);

        // execute hooks before query
        foreach($this->hkBeforeQ as $arrCallback)
        {
            $func = $arrCallback[0];
            $extArgs = $arrCallback[1];
            if(call_user_func_array($func, array($this, &$this->lastSQL, $extArgs)) === false)
            {
                return false;
            }
        }
        
        // execute table hooks before query
        foreach($this->tablehkBeforeQ as $arrCallback)
        {
            $func = $arrCallback[0];
            $extArgs = $arrCallback[1];
            if(call_user_func_array($func, array($this, &$sql, $extArgs)) === false)
            {
                return false;
            }
        }
		//echo $sql;
		
        if(!empty($bindParams)){ //为测试可用性，临时全部使用这个 
            if(!($dbStmt = $this->prepare($sql)))
                $ret = false;
            if($ret!==false)
                $ret = $dbStmt->bindParam($bindParams);
            if($ret!==false)
                $ret = $dbStmt->execute();
        }else{
            if($this->shardingStrategy){
                $myParser = new ShardingParser($this -> shardingStrategy);
                $shardingSql = $myParser -> filterBySharding($sql);
                if(is_array($shardingSql)) {
                    if ($this -> mysql -> multi_query(implode(";", $shardingSql))) {
                        do {
                            $lastResult = array();
                            if ($result = $this -> mysql -> store_result()) {
                                while ($row = $result -> fetch_row()) {
                                    $lastResult[] = $row;
                                }
                                $result->free();
                            }
                        } while ($this -> mysql -> next_result());
                    }
                    return $lastResult;
                } else {
                    //正式上线后放开
                    $sql = $shardingSql;
                }
                $this->lastSQL = $sql;
            }
            $res = $this->mysql->query($sql);
            $ret = false;

            // res is NULL if mysql is disconnected
            if(is_bool($res) || $res === NULL){
                $ret = ($res == true);
                // call fail handler
                if(!$ret && $this->onfail !== NULL){
                    call_user_func_array($this->onfail, array($this, &$ret));
                }
            }else{// we have result
                $ret = array();
                while($row = $res->fetch_assoc()){
                    $ret[] = $row;
                }
                $res->free();
            }
        }

        // do profiling
        if($this->enableProfiling)
        {
            $this->arrCost[] = array($sql, $this->lastCost);
        }

        // execute hooks after query
        foreach($this->hkAfterQ as $arrCallback){
            $func = $arrCallback[0];
            $extArgs = $arrCallback[1];
            call_user_func_array($func, array($this, &$ret, $extArgs));
        }
        
        // execute table hooks after query
        foreach($this->tablehkAfterQ as $arrCallback){
            $func = $arrCallback[0];
            $extArgs = $arrCallback[1];
            call_user_func_array($func, array($this, &$ret, $extArgs));
        }
        
        return $ret;
    }

    private function __getSQLAssember()
    {
        if($this->sqlAssember == NULL)
        {
            require_once('SQLAssember.php');
            $this->sqlAssember = new SQLAssember($this);
        }
        return $this->sqlAssember;
    }

    public function insert($table, $row, $options = NULL, $onDup = NULL)
    {
        $this->__getSQLAssember();
        $sql = $this->sqlAssember->getInsert($table, $row, $options, $onDup);
        if(!$sql || !$this->query($sql))
        {
            return false;
        }
        return $this->mysql->affected_rows;
    }

    public function update($table, $row, $conds = NULL, $options = NULL, $appends = NULL)
    {
        $this->__getSQLAssember();
        $sql = $this->sqlAssember->getUpdate($table, $row, $conds, $options, $appends);
        if(!$sql || !$this->query($sql))
        {
            return false;
        }
        return $this->mysql->affected_rows;
    }

    public function delete($table, $conds = NULL, $options = NULL, $appends = NULL)
    {
        $this->__getSQLAssember();
        $sql = $this->sqlAssember->getDelete($table, $conds, $options, $appends);
        if(!$sql || !$this->query($sql))
        {
            return false;
        }
        return $this->mysql->affected_rows;
    }

    public function prepare($query, $getRaw = false)
    {
        $stmt = $this->mysql->prepare($query);
        if($stmt === false)
        {
            return false;
        }
        if($getRaw)
        {
            return $stmt;
        }
        else
        {
            require_once('DBStmt.php');
            return new DBStmt($stmt);
        }
    }

    public function getLastSQL()
    {
        return $this->lastSQL;
    }

    public function getInsertID()
    {
        return $this->mysql->insert_id;
    }

    public function getAffectedRows()
    {
        return $this->mysql->affected_rows;
    }

    public function getLastQueryInfo()
    {
        return $this->mysql->info;
    }

    //////////////////////////// hooks ////////////////////////////

    public function addHook($where, $id, $func, $extArgs = NULL)
    {
        switch($where)
        {
            case self::HK_BEFORE_QUERY:
                $dest = &$this->hkBeforeQ;
                break;
            case self::HK_AFTER_QUERY:
                $dest = &$this->hkAfterQ;
                break;
            default:
                return false;
        }
        if(!is_callable($func))
        {
            return false;
        }
        $dest[$id] = array($func, $extArgs);
        return true;
    }
    
    public function addTableHook($where, $id, $func, $extArgs = NULL)
    {
        switch($where)
        {
            case self::HK_BEFORE_QUERY:
                $dest = &$this->tablehkBeforeQ;
                break;
            case self::HK_AFTER_QUERY:
                $dest = &$this->tablehkAfterQ;
                break;
            default:
                return false;
        }
        if(!is_callable($func))
        {
            return false;
        }
        $dest[$id] = array($func, $extArgs);
        return true;
    }
    
    public function clearTableHook() {
        $this->tablehkBeforeQ = array();
        $this->tablehkAfterQ = array();
    }

    public function onFail($func = 0)
    {
        if($func === 0)
        {
            return $this->onfail;
        }
        if($func === NULL)
        {
            $this->onfail = NULL;
            return true;
        }
        if(!is_callable($func))
        {
            return false;
        }
        $this->onfail = $func;
        return true;
    }

    public function removeHook($where, $id)
    {
        switch($where)
        {
            case self::HK_BEFORE_QUERY:
                $dest = &$this->hkBeforeQ;
                break;
            case self::HK_AFTER_QUERY:
                $dest = &$this->hkAfterQ;
                break;
            default:
                return false;
        }
        if(!array_key_exists($id, $dest))
        {
            return false;
        }
        unset($dest[$id]);
        return true;
    }

    //////////////////////////// profiling ////////////////////////////

    public function getLastCost()
    {
        return $this->lastCost;
    }

    public function getTotalCost()
    {
        return $this->totalCost;
    }

    public function getProfilingData()
    {
        return $this->arrCost;
    }

    public function cleanProfilingData()
    {
        $this->arrCost = NULL;
    }

    public function enableProfiling($enable = NULL)
    {
        if($enable === NULL)
        {
            return $this->enableProfiling;
        }
        $this->enableProfiling = ($enable == true);
    }

    //////////////////////////// transaction ////////////////////////////

    public function autoCommit($bolAuto = NULL)
    {
        if($bolAuto === NULL)
        {
            $sql = 'SELECT @@autocommit';
            $res = $this->query($sql);
            if($res === false)
            {
                return NULL;
            }
            return $res[0]['@@autocommit'] == '1';
        }

        $this->acStatus = $bolAuto;
        return $this->mysql->autocommit($bolAuto);
    }

    public function startTransaction()
    {
        if($this->txStatus)//避免事务嵌套的问题
        {
            if(class_exists('Yii',false))
            {
                $trace=debug_backtrace();
                $caller=$trace[1];
                Yii::log('W1111111111 '.'transaction nested in function '.$caller['function'].' file'.$caller['file'], 'warning');
            }
            return true;
        }
        $this->txStatus = 1;
        $sql = 'START TRANSACTION';
        return $this->query($sql);
    }

    public function getTransactionStatus()
    {
        if($this->txStatus === 1 || !$this->acStatus)
            return true;
        else
            return false;
    }

    public function commit()
    {
        $this->txStatus = 0;
        //2016.06.07, add by zouyoufang, 对于检查过的已经断开的连接，不再提交。
        if($this->checkClosed){
            return true;
        }
        return $this->mysql->commit();
    }
    
    /**
     * ADD BY shuguo (suggested by haojie)
     * 开放一个ping方法，如果需要判断当前连接是否可用时可以调用
     */
    public function ping() {
        return $this->mysql->ping();
    }

    public function rollback()
    {
        $this->txStatus = 0;
        return $this->mysql->rollback();
    }

    //////////////////////////// util ////////////////////////////

    public function escapeString($string)
    {
        return $this->mysql->real_escape_string($string);
    }

    public function selectDB($dbname)
    {
        if($this->mysql->select_db($dbname))
        {
            $this->dbConf['dbname'] = $dbname;
            return true;
        }
        return false;
    }

    public function getTables($pattern = NULL, $dbname = NULL)
    {
        $sql = 'SHOW TABLES';
        if($dbname !== NULL)
        {
            $sql .= ' FROM '.$this->escapeString($dbname);
        }
        if($pattern !== NULL)
        {
            $sql .= ' LIKE \''.$this->escapeString($pattern).'\'';
        }

        if(!($res = $this->query($sql)))
        {
            return false;
        }

        $ret = array();
        while($row = $res->fetch_row())
        {
            $ret[] = $row[0];
        }
        $res->free();
        return $ret;
    }

    public function isTableExists($name, $dbname = NULL)
    {
        $tables = $this->getTables($name, $dbname);
        if($tables === false)
        {
            return NULL;
        }
        return count($tables) > 0;
    }


    public function charset($name = NULL)
    {
        if($name === NULL)
        {
            return $this->mysql->character_set_name();
        }
        return $this->mysql->set_charset($name);
    }

    public function getConnConf()
    {
        if($this->dbConf == NULL)
        {
            return NULL;
        }

        return array(
            'host' => $this->dbConf['host'],
            'port' => $this->dbConf['port'],
            'uname' => $this->dbConf['uname'],
            'dbname' => $this->dbConf['dbname']
            );
    }

    //////////////////////////// error ////////////////////////////

    public function errno()
    {
        return $this->mysql->errno;
    }

    public function error()
    {
        return $this->mysql->error;
    }
}


?>
