<?php
/**
 * @brief php7下的mongodb简单封装，实现增删改查
 * @author hewei
 * @date 2016.10.31
 */
class MongoDB{
	const protocol = 'mongodb://';
	protected $connectionString;
	protected $dbname;
	protected $writeConcernLevel = 1;
	protected $writeConcern;
	private static $dbObjMap = array();
	
	public function __construct($config = array()){
		if(empty($config) || !is_array($config)){
			throw new SystemException('[mongodb]实例化mongodb数据库，配置为空');
		}
		$connectionString = '';
		$auth = $config['auth'];
		$machines = $config['machine'];
		$options = $config['options'];
		$connectionString .= rawurlencode($auth['dbuser']) . ':' . rawurlencode($auth['dbpass']) . '@';
		
		foreach ($machines as $machine){
			$connectionString .= $machine['host'] . ':' . $machine['port'] . ',';
		}
		$connectionString = trim($connectionString, ',');
		$connectionString .= '/' . $auth['dbname'];

		if($options){
			$connectionString .= '?' . http_build_query($options);
		}
		$this->connectionString = $connectionString;
		$this->writeConcern = new MongoDB\Driver\WriteConcern($this->writeConcernLevel, $config['connectTimeoutMS']);
		$this->dbname = $auth['dbname'];
	}
	
	/**
	 * @brief 连接mongodb数据库
	 */
	protected function connect(){
		if(empty($this->connectionString)){
			throw new SystemException('[mongodb]实例化mongodb数据库，拼接后的字符串为空');
		}
		try {
			$mongo = new MongoDB\Driver\Manager(self::protocol . $this->connectionString);
		} catch (Exception $e){
			Yii::log('[mongodb]mongodb连接失败,connectionString:'.$this->connectionString, 'error');
			return false;
		}
		return $mongo;
	}
	
	/**
	 * @brief 封装对外db入口
	 */
	public function db(){
		if(isset(self::$dbObjMap[$this->connectionString])){
			return self::$dbObjMap[$this->connectionString];
		}
		$mongoClient = $this->connect();
		self::$dbObjMap[$this->connectionString] = $mongoClient;
		return $mongoClient;
	}
	
	private function _toString($params = array()){
		if(empty($params)){
			return array();
		}
		foreach ($params as $key=>$value){
			if(is_array($value)){
				$params[$key] = $this->_toString($value);
			} else {
				$params[$key] = (string)$value;
			}
		}
		return $params;
	}

    /*
     * 复杂查询的mongodb函数
     * 用于定义区间查询的过滤条件*/
    public function complixFind($filter)
    {

        $query = new MongoDB\Driver\Query($filter);
        $db = $this->db();
        $cursor = $db->executeQuery("{$this->dbname}.{$this->table}", $query);
        return $cursor;
    }

    /**
     * 定义命令行执行函数
     * @param $cmd 命令行语句
     * */
    public function runCommand($cmd){
        $command = new MongoDB\Driver\Command($cmd);

        $db = $this->db();
        $result = $db->executeCommand("{$this->dbname}", $command);
        return $result;
    }

    /**
     * @param $arrWhere 查询条件 一维关联数组 KV
     * @param $arrFields 查询返回字段 一维索引数组
     * @param $arrOrderBy 排序数组 @eg:[id] => DESC
     * @param $limit 限制查询出的数据条数
     * @param $skip 结果集中要跳转的行数
     */
    public function find($arrWhere = array(), $arrFields = array(), $arrOrderBy = array(),$limit=0,$skip=0)
    {
    	$arrWhere = $this->_toString($arrWhere);
    	$options = array();
    	//查询指定字段
    	if($arrFields){
    		foreach ($arrFields as $field){
    			$options['projection'][$field] = 1;
    		}
    	}
    	//指定字段排序 1=>升序 -1=>降序
    	if($arrOrderBy){
    		foreach ($arrOrderBy as $key=>$sort){
    			$options['sort'][$key] = strtolower($sort) == 'desc' ? -1 : 1;
    		}
    	}
        //增加mongo的分页
        if($limit){
            $options['limit']=$limit;
        }
        if($skip){
            $options['skip']=$skip;
        }
    	$db = $this->db();
    	$query = new MongoDB\Driver\Query($arrWhere, $options);
    	$arrResult = $db->executeQuery("{$this->dbname}.{$this->table}", $query);
    	
    	Yii::log('[mongodb-find] [table:'.$this->dbname.'.'.$this->table.'] arrWhere=>' .json_encode($arrWhere) .' arrFields=>' . json_encode($arrFields) . ' arrOrderBy=>' . json_encode($arrOrderBy));
        if (!empty($arrResult))
        {
        	$arrRows = array();
        	foreach ($arrResult as $key=>$document){
        		$arrRows[$key] = (array)$document;
        		//MongoDB\BSON\ObjectID转成字符串一维数组
        		$arrRows[$key]['_id'] = (string)$arrRows[$key]['_id'];
        	}
            return $this->_object_array($arrRows);
        }
        else
        {
            return array();
        }
    }
    
    private function _object_array($array) {
    	if(is_object($array)) {
    		$array = (array)$array;
    	} if(is_array($array)) {
    		foreach($array as $key=>$value) {
    			$array[$key] = $this->_object_array($value);
    		}
    	}
    	return $array;
    }
    
    /**
     * @param $arrWhere 查询条件 一维关联数组 KV
     * @param $arrFields 查询返回字段 一维索引数组
     * @return 返回一维数组
     */
    public function findOne($arrWhere, $arrFields = array()){
    	if(empty($arrWhere)){
    		return array();
    	}
    	 $arrRows = $this->find($arrWhere, $arrFields);
    	 if($arrRows){
    	 	return $arrRows[0];
    	 }
    	 return array();
    }
    
    public function save($arrSaveData, $uniqField){
    	if(!$uniqField || !isset($arrSaveData[$uniqField]) || empty($arrSaveData[$uniqField])){
    		Yii::log('[mongodb] [table:'.$this->dbname.'.'.$this->table.'] 没有找到唯一键，无法继续往下操作, data:' . json_encode($arrSaveData), 'error');
    		return false;
    	}
    	$arrRealWhere = array($uniqField => $arrSaveData[$uniqField]);
    	$bolIsExist = $this->findOne($arrRealWhere);
    	if($bolIsExist){
    		$bolResult = $this->update($arrRealWhere, $arrSaveData, $uniqField);
    	} else {
    		$bolResult = $this->insert($arrSaveData, $uniqField);
    	}
    	return $bolResult;
    }
    
    /**
     * @brief 插入数据
     * @dupCheck 是否需要唯一去重
     */
    public function insert($arrSaveData, $uniqField, $dupCheck = true){
    	if(empty($arrSaveData) || !is_array($arrSaveData)){
    		Yii::log('[mongodb-insert] [table:'.$this->dbname.'.'.$this->table.'] 插入数据时，数据为空', 'error');
    		return false;
    	}
    	
    	if(!$uniqField || !isset($arrSaveData[$uniqField]) || empty($arrSaveData[$uniqField])){
    		Yii::log('[mongodb-insert] [table:'.$this->dbname.'.'.$this->table.'] 没有找到唯一键，无法继续往下操作, data:' . json_encode($arrSaveData), 'error');
    		return false;
    	}
    	
    	if($dupCheck){
    		$bolIsExist = $this->findOne(array($uniqField => $arrSaveData[$uniqField]));
    		if($bolIsExist){
    			Yii::log('[mongodb-insert] [table:'.$this->dbname.'.'.$this->table.'] 插入已经存在的数据，where:' . json_encode($arrSaveData), 'error');
    			return false;
    		}
    	}
    	 
    	$bulk = new MongoDB\Driver\BulkWrite(array('ordered' => true));
    	//强制转化成string，mongodb区分类型，不然后续查询很痛苦
    	$bulk->insert($this->_toString($arrSaveData));
    	$result = $this->_save($bulk);
    	 
    	if($result->getInsertedCount()){
    		Yii::log('[mongodb-insert] [table:'.$this->dbname.'.'.$this->table.'] insert success data:' . json_encode($arrSaveData), 'info');
    		return true;
    	}
    	Yii::log('[mongodb-insert] [table:'.$this->dbname.'.'.$this->table.'] insert fail data:' . json_encode($arrSaveData), 'error');
    	return false;
    }
    
    /**
     * @brief 更新数据
     */
    public function update($arrWhere, $arrUpdate, $uniqField){
    	if(empty($arrWhere) || empty($arrUpdate)){
    		Yii::log('[mongodb-update][table:'.$this->dbname.'.'.$this->table.']更新数据时，数据为空', 'error');
    		return false;
    	}
    	if(!$uniqField || !isset($arrWhere[$uniqField]) || empty($arrWhere[$uniqField])){
    		Yii::log('[mongodb-update][table:'.$this->dbname.'.'.$this->table.']没有找到唯一键，无法继续往下操作, data:' . json_encode($arrWhere), 'error');
    		return false;
    	}
    	$result = $this->findOne(array($uniqField => $arrWhere[$uniqField]));
    	if(empty($result)){
    		Yii::log('[mongodb-update][table:'.$this->dbname.'.'.$this->table.']更新的时候，根据条件无法查询要更新的数据，where:' . json_encode($arrWhere), 'error');
    		return false;
    	}
    	$arrUpdate = $this->_toString($arrUpdate);
    	if(isset($arrUpdate['_id'])){
    		Yii::log('[mongo-update]数据里面有_id，做删除处理, order_id:' . $arrWhere[$uniqField], 'info');
    		unset($arrUpdate['_id']);
    	}
    	$arrRealWhere = array($uniqField => (string)$arrWhere[$uniqField]);
    	$bulk = new MongoDB\Driver\BulkWrite(array('ordered' => true));
    	$bulk->update($arrRealWhere, array('$set' => $arrUpdate));
    	
    	$result = $this->_save($bulk);
    	
    	if($result->getModifiedCount() || $result->getMatchedCount()){
    		Yii::log('[mongodb-update] [table:'.$this->dbname.'.'.$this->table.'] saveData success where:' . json_encode($arrWhere) . ' - update:' . json_encode($arrUpdate), 'info');
    		return true;
    	}
    	Yii::log('[mongodb-update] [table:'.$this->dbname.'.'.$this->table.'] saveData fail where:' . json_encode($arrWhere) . ' - update:' . json_encode($arrUpdate), 'error');
    	return false;
    }
    
    /**
     * @brief 写数据的统一入口
     */
    protected function _save($bulk){
    	try {
    		$db = $this->db();
    		$result = $db->executeBulkWrite("{$this->dbname}.{$this->table}", $bulk, $this->writeConcern);
    	} catch (MongoDB\Driver\Exception\BulkWriteException $e) {
    		$result = $e->getWriteResult();
    		if ($writeConcernError = $result->getWriteConcernError()) {
    			Yii::log(sprintf("[mongodb-save][table:'.$this->dbname.'.'.$this->table.']insert error|%s (%d): %s\n",
    			$writeConcernError->getMessage(),
    			$writeConcernError->getCode(),
    			var_export($writeConcernError->getInfo(), true)), 'error');
    		}
    		foreach ($result->getWriteErrors() as $writeError) {
    			Yii::log(sprintf("[mongodb-save][table:'.$this->dbname.'.'.$this->table.']insert error|operation#%d: %s (%d)\n",
    			$writeError->getIndex(),
    			$writeError->getMessage(),
    			$writeError->getCode()), 'error');
    		}
    		return false;
    	} catch (MongoDB\Driver\Exception\Exception $e) {
    		Yii::log(sprintf("[mongodb-save] [table:'.$this->dbname.'.'.$this->table.'] insert other error: %s\n", $e->getMessage()), 'error');
    		return false;
    	}
    	if($result){
    		Yii::log('[mongodb-save] [table:'.$this->dbname.'.'.$this->table.'] operation success', 'info');
    		return $result;
    	}
    	Yii::log('[mongodb-save] [table:'.$this->dbname.'.'.$this->table.'] operation fail data:' . json_encode($bulk), 'error');
    	return false;
    }
    
    /**
     * @brief 删除数据
     */
    public function delete($arrDeleteData, $uniqField){
    	if(empty($arrDeleteData) || !is_array($arrDeleteData)){
    		Yii::log('[mongodb-delete] [table:'.$this->dbname.'.'.$this->table.'] 删除数据时，数据为空', 'error');
    		return false;
    	}
    	
    	if(!$uniqField || !isset($arrWhere[$uniqField]) || empty($arrDeleteData[$uniqField])){
    		Yii::log('[mongodb-delete] [table:'.$this->dbname.'.'.$this->table.'] 没有找到唯一键，无法继续往下操作, data:' . json_encode($arrDeleteData), 'error');
    		return false;
    	}
    	
    	$result = $this->findOne(array($uniqField => $arrDeleteData[$uniqField]));
    	if(empty($result) || !isset($result[$uniqField])){
    		Yii::log('[mongodb-delete] [table:'.$this->dbname.'.'.$this->table.'] 删除的时候，根据条件无法查询要更新的数据，where:' . json_encode($arrDeleteData), 'error');
    		return false;
    	}
    	$arrRealWhere = array($uniqField => $result[$uniqField]);
    	$bulk = new MongoDB\Driver\BulkWrite(array('ordered' => true));
    	$bulk->remove($arrRealWhere);
    	
    	$result = $this->_save($bulk);
    	
    	if($result->getDeletedCount()){
    		Yii::log('[mongodb-delete] [table:'.$this->dbname.'.'.$this->table.'] delete success:' . json_encode($arrDeleteData), 'info');
    		return true;
    	}
    	Yii::log('[mongodb-delete] [table:'.$this->dbname.'.'.$this->table.'] delete error:' . json_encode($arrDeleteData), 'error');
    	return false;
    }
    
}
