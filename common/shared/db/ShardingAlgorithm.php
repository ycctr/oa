<?php

/**
 * 具体分表规则的实现
 * 目前支持两种：
 * 指定列取mod
 * 按指定列时间
 * @author zhangshuguo
 */
class ShardingAlgorithm {
	
	/**
	 * 当where条件中存在分表列，且其比较条件是=时
	 * @param unknown $strategy:配置信息
	 * @param unknown $whereValue：where语句中对应sharding列的值
	 * @return number|string|multitype:mixed：所有匹配到的表
	 */
	public function doEqualSharding($strategy, $whereValue, $tableOffset=0) {
		$shardingExpression = $strategy['strategy']['expression'];
		$shardingType = $strategy['maptype'];
		
		$matchedTables = array();
		if($shardingType == 'mod') {
			//用以匹配：message_receive_${message_send_id%10}
			$matchedTables[] = preg_replace_callback('/(\$\{)(\w+\%)(\d+)(\})/', function($match) use ($whereValue){
				return $whereValue % $match[3];}, $shardingExpression);
		} else if($shardingType == 'date'){
		    //如果$whereValue明显不是时间戳值（暂定为公司成立时间之前的所有数据），那一定是历史id值
		    if($whereValue < mktime(0, 0, 0, 10, 1, 2011)){
		        $tableSuffix = $this -> getMatchedTableSuffixFromHistoryId($strategy, $whereValue);
		        if($tableSuffix) {
                    $matchedTables[] = preg_replace('/(\$\{.+\})/', $tableSuffix, $strategy['actualtable']);
		        } else {
		            //无后缀时，根据配置直接返回为原始表
		            if($strategy['keepoldtable']) {
                        $matchedTables[] = preg_replace('/(_\$\{.+\})/', '', $strategy['actualtable']);
		            } 
		        }
		    } else {
    			//用以匹配：message_receive_${create_time|Ym}
    			$matchedTables[] = preg_replace_callback('/(\$\{)(\w+\|)(.+)(\})/', function($match) use ($whereValue){
    				return date($match[3], $whereValue);}, $shardingExpression);
    			
    			//如果需要设置偏移值
    			if($tableOffset) {
    			    //允许最大2个表的偏移
    			    preg_match('/(\$\{)(\w+\|)(.+)(\})/', $shardingExpression, $matches);
    			    $dateFormat = $matches[3];
    			    $currentMonth = date($dateFormat, $whereValue);
    			    
    		        while($tableOffset > 0) {
    		            if(substr($dateFormat, strlen($dateFormat)-1) == 'm'){
    		                //如果是按月划分
    		                $currentMonth = date($dateFormat, strtotime("+1 month", strtotime($currentMonth."01")));
    		                $matchedTables[] = preg_replace('/(\$\{.+\})/', $currentMonth, $strategy['actualtable']);
    		            } else if(substr($dateFormat, strlen($dateFormat)-1) == 'd') {
    		                //如果是按日划分
    		                $currentMonth = date($dateFormat, strtotime("+1 day", strtotime($currentMonth."01")));
    		                $matchedTables[] = preg_replace('/(\$\{.+\})/', $currentMonth, $strategy['actualtable']);
    		            }
    		            $tableOffset--;
    		        }
    		        
    		        while($tableOffset < 0) {
    		            //用以匹配类似：push_product_to_user_${201601..}
    		            preg_match('/(\$\{)(\d+)(\.\.)(\d+)?(\})/', $strategy['actualtable'], $minTimeValueMatches);
    		            $minTimeValue = $minTimeValueMatches[2];
    		            
    		            if(substr($dateFormat, strlen($dateFormat)-1) == 'm'){
    		                //如果是按月划分
    		                $currentMonth = date($dateFormat, strtotime("-1 month", strtotime($currentMonth."01")));
    		            } else if(substr($dateFormat, strlen($dateFormat)-1) == 'd') {
    		                //如果是按日划分
    		                $currentMonth = date($dateFormat, strtotime("-1 day", strtotime($currentMonth."01")));
    		            }
    		            if(intval($currentMonth) < intval($minTimeValue)) {
    		                //记录最小表，如果小于此值，则根据配置返回原始表
    		                if($strategy['keepoldtable']) {
                                $matchedTables[] = preg_replace('/(_\$\{.+\})/', '', $strategy['actualtable']);
                                break;
    		                }
    		            } else {
                            $matchedTables[] = preg_replace('/(\$\{.+\})/', $currentMonth, $strategy['actualtable']);
    		            }
    		            $tableOffset++;
    		        }
    			}
		    }
		    //如果返回的是空值，则默认取最新一个表
		    if(count($matchedTables) < 1) {
		        $matchedTables[] = preg_replace_callback('/(\$\{)(\w+\|)(.+)(\})/', function($match) use ($whereValue){
    				return date($match[3], time());}, $shardingExpression);
		    }
		}
		return  $matchedTables;
	}
	
	/**
	 * 取所有表做为结果集
	 * @param unknown $strategy
	 * @param unknown $whereValue
	 * @return string|multitype:mixed
	 */
	public function doNotinSharding($strategy, $whereValue) {
		$shardingExpression = $strategy['strategy']['expression'];
		$shardingType = $strategy['maptype'];
		$shardingTables = $strategy['actualtable'];
		$matchedTables = array();
		//如果需要，则返回原始表
		if($strategy['keepoldtable']) {
            $matchedTables[] = preg_replace('/(_\$\{.+\})/', '', $shardingTables);
		}
		//用以匹配: message_send_log_${201604..201701} 或 message_send_log_${201604..} 或 message_send_log_${0..9}
		if(preg_match('/(\$\{)(\d+)(\.\.)(\d+)?(\})/', $shardingTables, $matches)) {
			$start = $matches[2];
			$end = $matches[4];
			if($shardingType == 'mod') {
			    //映射类型为mod时，必然存在$end值
				for($i = intval($start); $i <= intval($end); $i++) {
					//用以匹配: message_send_log_${0..9}并替换为真实表名，如果起末值长度不一致，用0补齐
					$matchedTables[] = preg_replace_callback('/(\$\{.+\})/', function($match) use ($i){
						return str_pad($i, strlen($end), "0", STR_PAD_LEFT);
					}, $shardingTables);
				}
			} else if($shardingType == 'date'){
			    //用以匹配: message_receive_${create_time|Ym}，从而获取日期格式
			    preg_match('/(\$\{)(\w+\|)(.+)(\})/', $shardingExpression, $dateformatResult);
			    $dataFormat = $dateformatResult[3];
			    //可能存在无最大值的情况，此时按expression的值中的表达式自动转换当前时间戳
			    if(!$end) {
			        $end = date($dataFormat, time());
			    }
			    
				for($i = intval($start); $i <= intval($end); ) {
					//用以匹配：message_send_log_${201604..201701}中的${201604..201701}并替换为当前的循环值
					$matchedTables[] = preg_replace_callback('/(\$\{.+\})/', function($match) use ($i){
						return str_pad($i, strlen($end), "0", STR_PAD_LEFT);
					}, $shardingTables);
					
					if(substr($dataFormat, strlen($dataFormat)-1) == 'm'){
						//如果是按月划分
						$i = date($dataFormat, strtotime("+1 month", strtotime($i."01")));
					} else if(substr($dataFormat, strlen($dataFormat)-1) == 'd') {
						//如果是按日划分
						$i = date($dataFormat, strtotime("+1 day", strtotime($i."01")));
					}
				}
			}
		}
		return $matchedTables;
	}
	
	public function doGreaterThenSharding($strategy, $whereValue, $tableOffset=0) {
		$shardingExpression = $strategy['strategy']['expression'];
		$shardingType = $strategy['maptype'];
		$shardingTables = $strategy['actualtable'];
		$matchedTables = array();
		
		if($shardingType == 'mod') {
			//如果是mod，则不论操作符是>，或者<，其结果均是返回所有表
			//用以匹配：message_receive_${0..9}
			if(preg_match('/(\$\{)(\d+)(\.\.)(\d+)(\})/', $shardingTables, $matches)) {
				$start = $matches[2];
				$end = $matches[4];
				for($i = intval($start); $i <= intval($end); $i++) {
					//用以匹配:message_receive_${0..9}
					$matchedTables[] = preg_replace_callback('/(\$\{.+\})/', function($match) use ($i){
						return str_pad($i, strlen($end), "0", STR_PAD_LEFT);
					}, $shardingTables);
				}
			}
		} else if($shardingType == 'date') {
		    //用以匹配：message_receive_${create_time|Ym}
		    preg_match('/(\$\{)(\w+\|)(.+)(\})/', $shardingExpression, $dateFormatMatches);
		    $dateFormat = $dateFormatMatches[3];
		    
			//返回比较值所在表及更大表
		    if($whereValue < mktime(0, 0, 0, 10, 1, 2011)){
			    //如果$whereValue明显不是时间戳值（暂定为公司成立时间之前的所有数据），那一定是历史id值
		        $dividingValue = $this -> getMatchedTableSuffixFromHistoryId($strategy, $whereValue);
		    } else {
		        $dividingValue = date($dateFormat, $whereValue);
		        //如果有offset且offset < 0,则直接修改$dividingValue
		        while($tableOffset < 0) {
		            
		            
		            if(substr($dateFormat, strlen($dateFormat)-1) == 'm'){
		                //如果是按月划分
		                $dividingValue = date($dateFormat, strtotime("-1 month", strtotime($dividingValue."01")));
		            } else if(substr($dateFormat, strlen($dateFormat)-1) == 'd') {
		                //如果是按日划分
		                $dividingValue = date($dateFormat, strtotime("-1 day", strtotime($dividingValue."01")));
		            }
		            $tableOffset++;
		        }
		    }

		    //用以匹配：message_send_log_${201604..201701}并从中取出日期的开始值及结束值(允许不存在)
		    preg_match('/(\$\{)(\d+)(\.\.)(\d+)?(\})/', $strategy['actualtable'], $rangeMatches);
		    
		    //如果此时的$dividingValue为空，则说明最终结果集为原始表和所有分表
		    if(!$dividingValue || intval($dividingValue) < intval($rangeMatches[2])) {
		        if($strategy['keepoldtable']) {
                    $matchedTables[] = preg_replace('/(_\$\{.+\})/', '', $strategy['actualtable']);
		        }
		        //同时设置$dividingValue为最小真实表
		        preg_match('/(\$\{)(\d+)(\.\.)(\d+)?(\})/', $shardingTables, $startMatchesResult);
		        $dividingValue = $startMatchesResult[2];
		    }
		    
            $end = $rangeMatches[4];
            //如果不存在结束值，则按日期格式取当前值
            if(!$end) {
                $end = date($dateFormat, time());
            }
        
            for($i = intval($dividingValue); $i <= intval($end); ) {
                //用以匹配：message_send_log_${201604..201701}中的${201604..201701}并替换为当前的循环值
                $matchedTables[] = preg_replace_callback('/(\$\{.+\})/', function($match) use ($i){
                    return str_pad($i, strlen($end), "0", STR_PAD_LEFT);}, $shardingTables);
                if(substr($dateFormat, strlen($dateFormat)-1) == 'm'){
                    //如果是按月划分
                    $i = date($dateFormat, strtotime("+1 month", strtotime($i."01")));
                } else if(substr($dateFormat, strlen($dateFormat)-1) == 'd') {
                    //如果是按日划分
                    $i = date($dateFormat, strtotime("+1 day", strtotime($i."01")));
                }
            }
		}
		return $matchedTables;
	}
	
	public function doLessThenSharding($strategy, $whereValue, $tableOffset=0) {
		$shardingExpression = $strategy['strategy']['expression'];
		$shardingType = $strategy['maptype'];
		$shardingTables = $strategy['actualtable'];
		$matchedTables = array();
		
		if($shardingType == 'mod') {
			//如果是mod，则不论操作符是>，或者<，其结果均是返回所有表
			//用以匹配：message_receive_${0..9}
			if(preg_match('/(\$\{)(\d+)(\.\.)(\d+)(\})/', $shardingTables, $matches)) {
				$start = $matches[2];
				$end = $matches[4];
				for($i = intval($start); $i <= intval($end); $i++) {
					//用以匹配：message_receive_${0..9}
					$matchedTables[] = preg_replace_callback('/(\$\{.+\})/', function($match) use ($i){
						return str_pad($i, strlen($end), "0", STR_PAD_LEFT);
					}, $shardingTables);
				}
			}
		} else if($shardingType == 'date') {
			// <返回比较值所在表及更小表，原始表是一定会出现在结果集中的
		    if($strategy['keepoldtable']) {
                $matchedTables[] = preg_replace('/(_\$\{.+\})/', '', $strategy['actualtable']);
		    }
		    
		    //如果$whereValue明显不是时间戳值（暂定为公司成立时间之前的所有数据），则直接从原始表中查询
		    if($whereValue < mktime(0, 0, 0, 10, 1, 2011)){
		        $dividingValue = $this -> getMatchedTableSuffixFromHistoryId($strategy, $whereValue);
		    } else {
		        // 用以匹配：message_receive_${create_time|Ym}中的${create_time|Ym}，从而找出日期格式
		        if(preg_match('/(\$\{)(\w+\|)(.+)(\})/', $shardingExpression, $matches)){
		            $dateFormat = $matches[3];
		            $dividingValue = date($dateFormat, $whereValue);
		            //如果有offset且offset > 0,则直接修改$dividingValue
		            while($tableOffset > 0) {
		                if(substr($dateFormat, strlen($dateFormat)-1) == 'm'){
		                    //如果是按月划分
		                    $dividingValue = date($dateFormat, strtotime("+1 month", strtotime($dividingValue."01")));
		                } else if(substr($dateFormat, strlen($dateFormat)-1) == 'd') {
		                    //如果是按日划分
		                    $dividingValue = date($dateFormat, strtotime("+1 day", strtotime($dividingValue."01")));
		                }
		                $tableOffset--;
		            }
		        }
		    }
		    
		    //如果$dividingValue为空，说明结果集中只需要原始表就可以了
		    if($dividingValue) {
		        //用以匹配：message_send_log_${201604..201701}中的${201604..201701}，并找出日期的起始值及结束值（允许不存在）
		        preg_match('/(\$\{)(\d+)(\.\.)(\d+)?(\})/', $shardingTables, $rangeMatches);
		        $start = $rangeMatches[2];
		        $end = $rangeMatches[4];
		        
		        //如果不存在结束值，则按日期格式取当前值
		        if(!$end) {
		            $end = date($matches[3], time());
		        }
		        
		        for($i = intval($start); $i <= intval($dividingValue); ) {
		            //用以匹配：message_send_log_${201604..201701}中的${201604..201701}并替换为当前的循环值
		            $matchedTables[] = preg_replace_callback('/(\$\{.+\})/', function($match) use ($i){
		                return str_pad($i, strlen($end), "0", STR_PAD_LEFT);}, $shardingTables);
	                if(substr($matches[3], strlen($matches[3])-1) == 'm'){
	                    //如果是按月划分
	                    $i = date($matches[3], strtotime("+1 month", strtotime($i."01")));
	                } else if(substr($matches[3], strlen($matches[3])-1) == 'd') {
	                    //如果是按日划分
	                    $i = date($matches[3], strtotime("+1 day", strtotime($i."01")));
	                }
		        }
		    }	
		    //如果未找到表，则返回一个最新的表
		    if(count($matchedTables) < 1) {
		        $matchedTables[] = preg_replace('/(\$\{.+\})/', date($dateFormat, time()), $strategy['actualtable']);
		    }
		}
		return $matchedTables;
	}
	
	/**
	 * 当where条件中存在分表列，且其比较条件是in时
	 * @param unknown $strategy
	 * @param unknown $whereValue
	 * @return number|string|multitype:mixed
	 */
	public function doInSharding($strategy, $whereValue, $tableOffset=0){
		//RULE: in只支持数值
		$shardingType = $strategy['maptype'];
		$shardingExpression = $strategy['strategy']['expression'];
		$matchedTables = array();
		//用以匹配：in (3,5,7)
		if(preg_match('/^\(([\d,]+)\)$/', $whereValue, $matches)) {
			foreach(explode(',', $matches[1]) as $singleValue) {
				if($shardingType == 'mod') {
					//用以匹配：message_receive_${message_send_id%10}
					$matchedTables[] = preg_replace_callback('/(\$\{)(\w+\%)(\d+)(\})/', function($match) use ($singleValue){
						return $singleValue % $match[3];}, $shardingExpression);
				} else if($shardingType == 'date'){
				    //重置$whereValue值
				    if(intval($singleValue/1000) > mktime(0, 0, 0, 1, 19, 2038)) {
				        //如果值超过时间戳最大值，则重置为当前时间，避免后面替换为一个不存在的表引发SQL错误
				        $singleValue = time();
				    } else if($singleValue > mktime(0, 0, 0, 10, 1, 2011)) {
				        //根据唯一ID的生成规则，这里需要除以1000，得到unix时间戳
				        $singleValue = intval($singleValue/1000);
				    }
				    
				    //如果$whereValue明显不是时间戳值（暂定为公司成立时间之前的所有数据），则直接从原始表中查询
				    if($singleValue < mktime(0, 0, 0, 10, 1, 2011)){
				        $tableSuffix = $this -> getMatchedTableSuffixFromHistoryId($strategy, $singleValue);
				        if(!$tableSuffix) {
				            if($strategy['keepoldtable']) {
                                $matchedTables[] = preg_replace('/(_\$\{.+\})/', '', $strategy['actualtable']);
				            }
				        } else {
				            $matchedTables[] = preg_replace('/(\$\{.+\})/', $tableSuffix, $strategy['actualtable']);
				        }
				    } else {
				        //用以匹配：message_receive_${create_time|Ym}
				        $matchedTables[] = preg_replace_callback('/(\$\{)(\w+\|)(.+)(\})/', function($match) use ($singleValue){
				            return date($match[3], $singleValue);}, $shardingExpression);
				    }
				    //如果需要设置偏移值
				    if($tableOffset) {
				        //允许最大2个表的偏移
				        preg_match('/(\$\{)(\w+\|)(.+)(\})/', $shardingExpression, $matches);
				        $dateFormat = $matches[3];
				        $currentMonth = date($dateFormat, $whereValue);
				         
				        while($tableOffset > 0) {
				            if(substr($dateFormat, strlen($dateFormat)-1) == 'm'){
				                //如果是按月划分
				                $currentMonth = date($dateFormat, strtotime("+1 month", strtotime($currentMonth."01")));
				                $matchedTables[] = preg_replace('/(\$\{.+\})/', $currentMonth, $strategy['actualtable']);
				            } else if(substr($dateFormat, strlen($dateFormat)-1) == 'd') {
				                //如果是按日划分
				                $currentMonth = date($dateFormat, strtotime("+1 day", strtotime($currentMonth."01")));
				                $matchedTables[] = preg_replace('/(\$\{.+\})/', $currentMonth, $strategy['actualtable']);
				            }
				            $tableOffset--;
				        }
				    
				        while($tableOffset < 0) {
				            //用以匹配类似：push_product_to_user_${201601..}
				            preg_match('/(\$\{)(\d+)(\.\.)(\d+)?(\})/', $strategy['actualtable'], $minTimeValueMatches);
				            $minTimeValue = $minTimeValueMatches[2];
				            
				            if(substr($dateFormat, strlen($dateFormat)-1) == 'm'){
				                //如果是按月划分
				                $currentMonth = date($dateFormat, strtotime("-1 month", strtotime($currentMonth."01")));
				            } else if(substr($dateFormat, strlen($dateFormat)-1) == 'd') {
				                //如果是按日划分
				                $currentMonth = date($dateFormat, strtotime("-1 day", strtotime($currentMonth."01")));
				            }
				            if(intval($currentMonth) < intval($minTimeValue)) {
				                //记录最小表，如果小于此值，则返回原始表
				                if($strategy['keepoldtable']) {
				                    $matchedTables[] = preg_replace('/(_\$\{.+\})/', '', $strategy['actualtable']);
				                    break;
				                }
				            } else {
				                $matchedTables[] = preg_replace('/(\$\{.+\})/', $currentMonth, $strategy['actualtable']);
				            }
				            $tableOffset++;
				        }
				    }
				    //如果结果为空数组，则默认取最新表
				    if(count($matchedTables) < 1) {
				        $matchedTables[] = preg_replace('/(\$\{.+\})/', date($dateFormat, time()), $strategy['actualtable']);
				    }
				}
			}
		}
		return array_unique($matchedTables);
	}
	
	/**
	 * 当$whereValue不是时间戳时，根据历史id范围映射表获取对应的表后缀
	 * 如果未找到值，则返回''
	 * @param unknown $strategy
	 * @param unknown $whereValue
	 */
	private function getMatchedTableSuffixFromHistoryId($strategy, $whereValue) {
	    //如果存在历史id对应表的关系
	    $tableSuffix = '';
	    if($strategy['strategy']['idhistoryrange']) {
	        foreach ($strategy['strategy']['idhistoryrange'] as $startId => $tableName) {
	            if($startId <= $whereValue) {
	                $tableSuffix = $tableName;
	            } else {
	                break;
	            }
	        }
	    }
	    return $tableSuffix;
	}
	
}

?>