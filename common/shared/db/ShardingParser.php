<?php
Yii::setPathOfAlias('PHPSQLParser', SHARED_PATH . '/PHPSQLParser');
use PHPSQLParser\builders\SelectBuilder;
use PHPSQLParser\builders\OrderByBuilder;
use PHPSQLParser\builders\LimitBuilder;
use PHPSQLParser\PHPSQLParser;
use PHPSQLParser\PHPSQLCreator;
use PHPSQLParser\builders\GroupByBuilder;
/**
 * 分表相关功能
 * 包括SQL解析、SQL路由、SQL重写等功能
 * @author zhangshuguo
 *
 */

class ShardingParser {
	
	private $strategy = array();
	
	public function __construct($strategy){
		$this -> strategy = $strategy;
	}
	
	/**
	 * 对原sql进行过滤，如果有分表配置，则返回替换后的新SQL，否则返回原sql
	 * 不支持between and语句；
	 * 聚合函数仅支持count(), sum(), min(), max()
	 * 支持group by
	 * 主键必须为id
	 * UPDATE和DELETE操作条件如果无主键时，其返回值将未定义
	 * 原始表必须存在（其可以无记录）
	 * @param unknown $sql
	 */
	public function filterBySharding($sql) {
		//如果无分表策略配置，直接返回
		if(count($this -> strategy) < 1) {
			return $sql;
		}
		$statementType = $this -> isDmlSql($sql);
		//只处理SELECT|DELETE|UPDATE|INSERT语句
		if(!$statementType) {
			return $sql;
		}
		$parser = new PHPSQLParser();
		//解析sql
		try {
			$parsed = $parser -> parse($sql);
			
			//找到sql中已配置了分表策略的表，即需要被替换的表
			$targetTables = $this -> getShardingTablesFromSql($sql, $statementType, $parsed);
			if(count($targetTables) < 1){
				return $sql;
			}
			//规范sql， 未配置分表策略的表不需要执行此操作
			$this -> normalizeSql($parsed);		
			
			//如果存在需要替换的表，根据操作类型分别改写sql
			return $this -> doRewriteSql($sql, $parsed, $targetTables);
		} catch (Exception $e) {
			Yii::log('[SHARDING-EXCEPTION][filterBySharding] => ' . $e->getMessage(), 'error');
		}
		return $sql;
	}
	
	/**
	 * 判断sql是否是DML语句：select|update|delete|insert
	 */
	private function isDmlSql($sql) {
		//只处理SELECT|INSERT|UPDATE|DELETE这四种语句
		if(preg_match('/^(SELECT|INSERT|UPDATE|DELETE)\s+/i', trim($sql), $statementType)){
			return strtoupper($statementType[1]);
		}
		return false;
	}
	
	/**
	 * 规范化SQL，目前处理以下CASE:
	 * 1. 用来排序/分组的列未出现在结果列中，比如：SELECT a FROM t WHERE ... ORDER/GROUP BY b
	 */
	private function normalizeSql(&$parsed) {
	    if(!$parsed['SELECT'] && !$parsed['ORDER'] && !$parsed['GROUP']) {
            return;
	    }
	    //找到SELECT中的所有列名（不包括聚合方法）
	    $selectedColumns = array();
	    foreach($parsed['SELECT'] as $item) {
	        if($item['expr_type'] == 'colref') {
	            //只要待选的列中存在一个*， 就可以直接return了
	            if($item['base_expr'] == '*') {
	                return;
	            }
	            $selectedColumns[$item['no_quotes']['parts'][0]] = $item['no_quotes']['parts'][0];
	        }
	    }
	    $orderGroupColumns = array_merge($parsed['ORDER'] ? $parsed['ORDER'] : array(), $parsed['GROUP'] ? $parsed['GROUP'] : array());
	    foreach($orderGroupColumns as $item) {
	        if($item['expr_type'] != 'colref') {
	            continue;
	        }
	        if($selectedColumns[$item['no_quotes']['parts'][0]]) {
	            continue;
	        }
	        //如果用来排序、分组的列不存在SELECT列集合中，则需要将其补到SELECT中
	        $columnCount = count($parsed['SELECT']);

	        unset($item['direction']);
	        $item['delim'] = '';
	        $parsed['SELECT'][$columnCount] = $item;
	        //前一个列后面要加上逗号
	        $parsed['SELECT'][$columnCount - 1]['delim'] = ',';
	    }
	}
	
	/**
	 * 从sql中解析出已配置分表策略的表
	 * @param unknown $statementType: SELECT|DELETE|UPDATE|INSERT
	 * @param unknown $parsed：解析后的sql结构数组
	 * @return multitype:|multitype:unknown：原sql中已经配置了分表规则的表
	 */
	private function getShardingTablesFromSql($sql, $statementType, $parsed) {
		$tables = array();
		
		if(strnatcasecmp($statementType, "SELECT") === 0 || strnatcasecmp($statementType, "DELETE") === 0) {
			if(!isset($parsed['FROM'])) {
				return $tables;
			}
			foreach ($parsed['FROM'] as $key => $item) {
				$trimedTableName = str_replace("`", "", $item['table']); 
				if($item['expr_type'] == 'table' && isset($this -> strategy[$trimedTableName])) {
					if(isset($item['alias']['name']) && !empty($item['alias']['name'])) {
						$tables[$item['alias']['name']] = $trimedTableName;
					} else {
						$tables[] = $trimedTableName;
					}
				}
			}
		} else if(strnatcasecmp($statementType, "INSERT") === 0) {
			/** 不支持复合语句插入，比如：insert into ... select * from..
			 * 仅支持简单的INSERT INTO，比如：
			 * INSERT INTO tables (column1, column2) values (value1, value2)
			 */
			if(!isset($parsed['INSERT']) || $parsed['INSERT'][1]['expr_type'] != 'table') {
				return $tables;
			}
			$insertTo = $parsed['INSERT'][1]['table'];
			$trimedTableName = str_replace("`", "", $insertTo);
			if(isset($this -> strategy[$trimedTableName])) {
				$tables[] = $trimedTableName;
			}
		} else if(strnatcasecmp($statementType, "UPDATE") === 0) {
			if(!isset($parsed['UPDATE']) || $parsed['UPDATE'][0]['expr_type'] != 'table') {
				return $tables;
			}
			$updateTarget = $parsed['UPDATE'][0]['table'];
			$trimedTableName = str_replace("`", "", $updateTarget);
			$alias = $parsed['UPDATE'][0]['alias'];
			if($this -> strategy[$trimedTableName]) {
				if(isset($alias['name']) && !empty($alias['name'])) {
					$tables[$alias['name']] = $trimedTableName;
				} else {
					$tables[] = $trimedTableName;
				}
			}
		} 
		if(count($tables)) {
			//Yii::log("[SHARDING][getShardingTablesFromSql] => $sql, [tables] => " . implode(",", $tables), 'info');
		}
		return $tables;
	}
	
	/**
	 * 如果是SELECT|DELETE|UPDATE操作，获取where条件中所有的列，并将其后的操作符和值关联到一起
	 * 如果是INSERT操作，获取插入列和对应的值
	 * 此处会对$parsed数组做修改，当insert语句中没有指定ID值时，会自动补一个全局生成ID
	 * @param unknown $sql:原sql
	 * @param unknown $parsed:解析后的数组
	 * @return multitype:multitype:string NULL  multitype:NULL
	 */
	private function getColumnsAndCondition($sql, &$parsed) {
		$columns = array();
		
		if($parsed['INSERT']) {
			//如果是INSERT操作，必须有列声明，不支持insert into table values (value1, value2)
			if($parsed['INSERT'][2]['expr_type'] == 'column-list') {
				$columnsDeclare = $parsed['INSERT'][2]['sub_tree'];
				$valuesDeclare = $parsed['VALUES'][0]['data'];
				$columnCount = count($columnsDeclare);
				foreach($columnsDeclare as $columnIndex => $columnItem) {
					$columns[$columnItem['no_quotes']['parts'][0]][] = array('operator' => '=', 
							'value' => $valuesDeclare[$columnIndex]['base_expr']);
				}
				
				//如果没有指定ID，则需要全局生成一个ID
				if(!isset($columns['id'])) {
					$tableName = $parsed['INSERT'][1]['no_quotes']['parts'][0];
					$sequenceTableName = $this -> strategy[$tableName]['sequence'];
					$sequnceModel = new Sequence($sequenceTableName);
					$newGeneratedId = $sequnceModel -> getId();
					
					//如果是mod分表，则需要对全局ID进行改造
					if($this -> strategy[$tableName]['maptype'] == 'mod') {
                        $shardingColumnName = $this -> strategy[$tableName]['strategy']['column'];
                        $shardingColumnValue = str_replace(array('"', "'"), "", $columns[$shardingColumnName][0]['value']);
                        if(!$shardingColumnValue) {
                            Yii::log("[SHARDING][UPDATE-GENID] column => $shardingColumnName, [SQL] => $sql", 'error');
                            return array();
                        }
                        //取mod规则
                        $shardingExpression = $this -> strategy[$tableName]['strategy']['expression'];
                        preg_match('/(\$\{)(\w+\%)(\d+)(\})/', $shardingExpression, $modMatches);
                        if(!$modMatches[3] || $modMatches[3] < 1) {
                            Yii::log("[SHARDING][UPDATE-GENID] modnumber => " . $modMatches[3], " [SQL] => $sql", 'error');
                            return array();
                        }
                        $append = str_pad($shardingColumnValue%$modMatches[3], strlen($modMatches[3]) - 1, "0", STR_PAD_LEFT);
                        $newGeneratedId .= $append;
                        Yii::log("[SHARDING][UPDATE-GENID] newgen => $newGeneratedId, append: $append, column: $shardingColumnName, value: $shardingColumnValue", 'info');
					}
					
					
					$columns['id'][] = array('operator' => '=', 'value' => $newGeneratedId);
					
					//更新$parsed数组，将id列及值加入
					$oldColumnList = $parsed['INSERT'][2]['base_expr'];
					$oldColumnValues = $parsed['VALUES'][0]['base_expr'];
					$parsed['INSERT'][2]['base_expr'] = str_replace(")", ", id)", $oldColumnList);
					$parsed['VALUES'][0]['base_expr'] = str_replace(")", ", $newGeneratedId)", $oldColumnValues);
					//先copy最后一个元素，然后只修改需要修改的key
					$columnsDeclare[$columnCount] = $columnsDeclare[$columnCount-1];
					$columnsDeclare[$columnCount]['base_expr'] = 'id';
					$columnsDeclare[$columnCount]['no_quotes']['parts'][0] = 'id';
					$parsed['INSERT'][2]['sub_tree'] = $columnsDeclare;
					//先copy最后一个元素，然后只修改需要修改的key
					$valuesDeclare[$columnCount] = $valuesDeclare[$columnCount-1];
					$valuesDeclare[$columnCount]['base_expr'] = $newGeneratedId;
					$parsed['VALUES'][0]['data'] = $valuesDeclare;
				}
			}
		} else {
			if(!isset($parsed['WHERE'])) {
				return $columns;
			}
			$this -> checkElementsInWhere($parsed['WHERE'], $columns);
		}
		//将`进行转义，对sql中的带引号的常量值做去引号处理
		$newColumns = array();
		foreach($columns as $key => $value) {
		    $newKey = str_replace("`", "", $key);
		    foreach($value as $subKey => $item) {
		        //$item['value'] = preg_replace('/^("|')/', "", $item['value']);
		        $pattern = '/^("|' . "'". ')(.+)("|' . "'" . ')$/';
		        $item['value'] = preg_replace_callback($pattern, function($match) {return $match[2];}, $item['value']);
		        $value[$subKey] = $item;
		    }
		    $newColumns[$newKey] = $value;
		}
		
		//Yii::log("[SHARDING][getColumnsAndCondition] => $sql, [columns] => " . serialize($newColumns), 'info');
		return $newColumns;
	}
	
	private function checkElementsInWhere($where, &$columns) {
	    if(!is_array($where)) {
	        return;
	    }
	    foreach($where as $key => $item) {
	        if(!is_array($item)) {
	            continue;
	        }
	        /**
	         * name >= 3 或者
	         * id in (3,4)
	         */
	        if($item['expr_type'] == 'colref'
	            && ($where[$key+1]['expr_type'] == 'operator' && strcasecmp($where[$key+1]['base_expr'], 'and') != 0)
	            && ($where[$key+2]['expr_type'] == 'in-list' || $where[$key+2]['expr_type'] == 'const')) {
                $columns[$item['base_expr']][] = array('operator' => $where[$key+1]['base_expr'], 'value' =>  $where[$key+2]['base_expr']);
            }
            /**
             * 3 <= age
             */
            if($item['expr_type'] == 'const'
                && ($where[$key+1]['expr_type'] == 'operator' && strcasecmp($where[$key+1]['base_expr'], 'and') != 0)
                && ($where[$key+2]['expr_type'] == 'colref')) {
                $columns[$where[$key+2]['base_expr']][] = array('operator' => $where[$key+1]['base_expr'], 'value' =>  $item['base_expr']);
            }
            /**
             * age not in (3,4)
             */
            if($item['expr_type'] == 'colref'
                && (strcasecmp($where[$key+1]['base_expr'], 'not') === 0 && strcasecmp($where[$key+2]['base_expr'], 'in') === 0)
                && $where[$key+3]['expr_type'] == 'in-list') {
                $columns[$item['base_expr']][] = array('operator' => 'not in', 'value' =>  $where[$key+3]['base_expr']);
            }
            
            if($item['expr_type'] == 'bracket_expression' && is_array($item['sub_tree'])) {
                $this -> checkElementsInWhere($item['sub_tree'], $columns);
            }
	    }
	}

	/*
	 * 获取多维数组的迪卡尔积结果（保留原始数组的key值）
	 * 例如：
	 * $input = array(
		    'arm' => array('A', 'B', 'C'),
		    'gender' => array('Female', 'Male'),
		    'location' => array('Vancouver', 'Calgary'),
		);
		将返回
		Array
		(
		    [0] => Array
		        (
		            [arm] => A
		            [gender] => Female
		            [location] => Vancouver
		        )
		
		    [1] => Array
		        (
		            [arm] => A
		            [gender] => Female
		            [location] => Calgary
		        )
		
		    [2] => Array
		        (
		            [arm] => A
		            [gender] => Male
		            [location] => Vancouver
		        )
		 ...
		 )
	 */
	private function cartesian($input) {
		// filter out empty values
		$input = array_filter($input);
		$result = array(array());
		foreach($input as $key => $values){
			$append = array();
			foreach($result as $product) {
				foreach($values as $item) {
					$product[$key] = $item;
					$append[] = $product;
				}
			}
			$result = $append;
		}
		return $result;
	}
	
	/**
	 * 将parsed解析结果中的表名替换成新的表名
	 * 这里要考虑SQL拼接中生成的转义字符：`
	 */
	private function updateTableName(&$parsed, $actualTable, $newTable) {
		if(isset($parsed['FROM']) && !empty($parsed['FROM'])) {
			//如果是SELECT|DELETE语句
			foreach($parsed['FROM'] as $key => $table) {
				$trimedTableName = str_replace("`", "", $table['table']);
				if($table['expr_type'] == 'table' && $trimedTableName == $actualTable) {
					$table['table'] = str_replace($actualTable, $newTable, $table['table']);   
					$table['no_quotes']['parts'][0] = str_replace($actualTable, $newTable, $table['no_quotes']['parts'][0]);
					$table['base_expr'] = str_replace($actualTable, $newTable, $table['base_expr']);
				}
				$parsed['FROM'][$key] = $table;
			}
		} else if(isset($parsed['INSERT']) && !empty($parsed['INSERT'])) {
			//如果是INSERT语句
			$trimedTableName = str_replace("`", "", $parsed['INSERT'][1]['table']);
			if($trimedTableName == $actualTable) {
				$parsed['INSERT'][1]['table'] = str_replace($actualTable, $newTable, $parsed['INSERT'][1]['table']);   
				$parsed['INSERT'][1]['no_quotes']['parts'][0] = str_replace($actualTable, $newTable, $parsed['INSERT'][1]['no_quotes']['parts'][0]);
				$parsed['INSERT'][1]['base_expr'] = str_replace($actualTable, $newTable, $parsed['INSERT'][1]['base_expr']);
			}
		} else if(isset($parsed['UPDATE']) && !empty($parsed['UPDATE'])) {
			//如果是UPDATE语句
			$trimedTableName = str_replace("`", "", $parsed['UPDATE'][0]['table']);
			if($trimedTableName == $actualTable) {
				$parsed['UPDATE'][0]['table'] = str_replace($actualTable, $newTable, $parsed['UPDATE'][0]['table']);
				$parsed['UPDATE'][0]['no_quotes']['parts'][0] = str_replace($actualTable, $newTable, $parsed['UPDATE'][0]['no_quotes']['parts'][0]);
				$parsed['UPDATE'][0]['base_expr'] = str_replace($actualTable, $newTable, $parsed['UPDATE'][0]['base_expr']);
			}
		}
	}
	
	/**
	 * 根据分表规则，改写sql语句
	 * @param unknown $sql: 原sql
	 * @param unknown $parsed：原sql解析后的数组
	 * @param unknown $targetTables:原sql中的需要被替换的表
	 * @return string
	 */
	private function doRewriteSql($sql, $parsed, $targetTables) {
		$creator = new PHPSQLCreator();
		$algorithmModel = new ShardingAlgorithm();
		$replacedSql = $sql;
		try {
			$whereColumes = $this -> getColumnsAndCondition($sql, $parsed);
			$allReplaceTables = array();
			foreach($targetTables as $alias => $tableName) {
				$strategy = $this -> strategy[$tableName];
				//原sql的where语句中是否包含配置中的分表列，因sql语句中表名可能会用别名，所以列名前也需要做判断
				$columnPrefix = is_numeric($alias) ? '' : $alias . '.';
				$columnConditions = $whereColumes[$columnPrefix . $strategy['strategy']['column']];
				if(!isset($columnConditions) || empty($columnConditions)) {
				    //id列上有隐藏的分表规则，如果此规则是基于日期分表的
				    if($strategy['maptype'] == 'date') {
				        //再检查一下id是否出现在where条件中
				        $columnConditions = $whereColumes[$columnPrefix . 'id'];
				        if($columnConditions) {
				            /**
				             * 为了后续统一处理，这里将根据全局id的生成方案将id值映射为时间戳
				             * 如果映射后这里的id明显非时间戳，则需要额外处理
				             */
				            foreach($columnConditions as $tempKey => $tempItem) {
				                //仅处理数值类型的 value
				                if(is_numeric($tempItem['value'])) {
				                    if(intval($tempItem['value']/1000) > mktime(0, 0, 0, 1, 19, 2038)) {
				                        //如果值超过时间戳最大值，则重置为当前时间，避免后面替换为一个不存在的表引发SQL错误
				                        $tempItem['value'] = time();
				                    } else if($tempItem['value'] > mktime(0, 0, 0, 10, 1, 2011)) {
				                        //根据唯一ID的生成规则，这里需要除以1000，得到unix时间戳
				                        $tempItem['value'] = intval($tempItem['value']/1000);
				                    }
				                }
				                $columnConditions[$tempKey] = $tempItem;
				            }
				        }
				    } else if($strategy['maptype'] == 'mod') {
				        $columnConditions = $whereColumes[$columnPrefix . 'id'];
				    }
				}
				
				//如果id列无法被用于分表，则查看“近似列”
				$tableOffset = 0;
				if(!isset($columnConditions) || empty($columnConditions)) {
				    //格式为：列名|表偏移个数  
				    $approximate = $strategy['strategy']['approximate'];
				    if($approximate) {
				        $info = explode('|', $approximate);
				        $tableOffset = intval($info[1]);
				        $columnConditions = $whereColumes[$columnPrefix . $info[0]];
				    }
				}
				
				//如果sql的where语句中即没有用于分表的列，也没有id，也没有近似列，则只能返回所有表了
				if(!isset($columnConditions) || empty($columnConditions)) {
					//如果是insert操作，则直接break退出，$allReplaceTables将保持为空（因为当前循环肯定只会运行一次）
					if($parsed['INSERT']) {
						Yii::log("[SHARDING-EXCEPTION][doRewriteSql] => cannot find sharding column in sql: $sql, sharding column: " . $strategy['strategy']['column'], 'error');
						break;
					}
					//如果用来指定分表规则的列，没有出现在sql中，则应该遍历所有表
					$allReplaceTables[$tableName] = $algorithmModel -> doNotinSharding($strategy, '');
					continue;
				}
			
				//保存将要被替换成的新表名
				$replaceTablesForCurrentTable = array();
				foreach ($columnConditions as $condition) {
					switch ($condition['operator']) {
						case '=':
							$replaceTables = $algorithmModel -> doEqualSharding($strategy, $condition['value'], $tableOffset);
							break;
						case 'in':
							$replaceTables = $algorithmModel -> doInSharding($strategy, $condition['value'], $tableOffset);
							break;
						case 'not in':
						case '!=':
							$replaceTables = $algorithmModel -> doNotinSharding($strategy, $condition['value']);
							break;
						case '>':
						case '>=':
							$replaceTables = $algorithmModel -> doGreaterThenSharding($strategy, $condition['value'], $tableOffset);
							break;
						case '<':
						case '<=':
							$replaceTables = $algorithmModel -> doLessThenSharding($strategy, $condition['value'], $tableOffset);
							break;
						default:
							$replaceTables = array();
							break;
					}
					
					//初始化时，需要将其赋值
					if(!count($replaceTablesForCurrentTable)) {
					    $replaceTablesForCurrentTable = $replaceTables;
					}
					$replaceTablesForCurrentTable = array_intersect($replaceTablesForCurrentTable, $replaceTables);
				}
				Yii::log("[SHARDING][doRewriteSql] => $sql, [tableName] => $tableName, [replacedTables] => " . implode(",", $replaceTablesForCurrentTable), 'debug');
				if(!count($replaceTablesForCurrentTable)) {
				    //如果某个表找不到符合条件的分表，则当前sql应该无返回结果；因为这是由筛选条件导致的，所以不建议返回异常；所以这里默认返回原SQL
				    $allReplaceTables = array();
				    break;
				}
				$allReplaceTables[$tableName] = $replaceTablesForCurrentTable;
			}
			
			$allReplacedSql = array();
			if(count($allReplaceTables)) {
				//改写原SQL中的表名，如果涉及到多个表，则需要取它们的迪卡尔积
				$finalReplaceCombination = $this -> cartesian($allReplaceTables);
				foreach($finalReplaceCombination as $item) {
					$originalParsed = $parsed;
					//去掉order by 及limit语句，便于后续执行union操作
					/**
					 * UNION时每个子语句也可以保留其order limit，但需要将每个子句用()包起来
					 */
					//先去掉，到最后一步执行union前再补回来，这样便于对原SQL做某些操作（不报错。比如短路逻辑会在原SQL后拼接AND）
					if($originalParsed['ORDER'] || $originalParsed['LIMIT']) {
						unset($originalParsed['ORDER']);
						unset($originalParsed['LIMIT']);
					}
						
					foreach($item as $actualTable => $newTable) {
						$this -> updateTableName($originalParsed, $actualTable, $newTable);
					}
					//此时将得到一个替换后的sql语句
					$allReplacedSql[] = $creator -> create($originalParsed);
				}
			}
			
			if(count($allReplacedSql) > 0) {
				//SELECT语句需要一些特殊处理：order by|limit
				if($parsed['SELECT']) {
				    /**
				     * TODO
				     * 以下情况可以使用NOT/ EXISTS 实现短路操作
				     * 1. WHERE中条件包含id值且操作符为=
				     * 2. WHERE条件中有唯一索引列且操作符为=
				     */
				    if($strategy['strategy']['unique_index']) {
				        $shortCircuit = false;
				        foreach($strategy['strategy']['unique_index'] as $item) {
				            //如果唯一索引列出现在where条件中
				            if(in_array($item, array_keys($whereColumes))) {
				                //并且其操作符为=
				                foreach($whereColumes[$item] as $conditionItem) {
				                    if($conditionItem['operator'] == '=') {
				                        //表明结果集只需要一条
				                        $shortCircuit = true;
				                        break;
				                    }
				                }
				            }
				        }
				        //执行短路逻辑，重构SQL
				        if($shortCircuit) {
				            for($i = 0; $i < count($allReplacedSql) - 1; $i++) {
				                $notExists = array();
				                for($j = $i+1; $j < count($allReplacedSql); $j++) {
				                    $notExists[] = "NOT EXISTS (" . $allReplacedSql[$j] . ")";
				                }
				                $allReplacedSql[$i] .= " AND " . implode(" AND ", $notExists);
				            }
				            //反转原表，保证先从最近得到的表开始查找
				            $allReplacedSql = array_reverse($allReplacedSql);
				        }
				    }
				    //将每个子语句用()包起来，并补上原有的ORDER|LIMIT
				    if($parsed['ORDER']) {
    				    $orderBuilder = new OrderByBuilder();
    				    $newOrderSql = $orderBuilder -> build($parsed['ORDER']);				        
				    }
				    if($parsed['LIMIT']) {
				        //这里需要特殊处理，如果子union语句中是 limit m,n的格式，则需要改为limit m+n，这样才能保证结果正确
    				    $builder = new LimitBuilder();
    				    $interLimit = $parsed['LIMIT'];
    				    if($interLimit['offset']) {
    				        $interLimit['rowcount'] +=  $interLimit['offset'];
    				        $interLimit['offset'] = "";
    				    }
    				    $newLimitSql = $builder ->build($interLimit);
				    }
				    
				    foreach($allReplacedSql as $key => $item) {
				        $item = "($item $newOrderSql $newLimitSql)";
				        $allReplacedSql[$key] = $item;
				    }
				    
					//支持count(*)
					$selectSql = $this -> refactorSelectFields($parsed);
					$unionSql = "SELECT $selectSql FROM (" . implode(" UNION ALL ", $allReplacedSql) . ") AS derived";
					//如果有GROUP
					$unionSql .= " " . $this -> appendGroupToUnionSql($parsed['GROUP']);
					//如果有ORDER
					$unionSql .= " " . $this -> appendOrderToUnionSql($parsed['ORDER']);
					//如果有LIMIT
					$unionSql .= " " . $this -> appendLimitToUnionSql($parsed['LIMIT']);
					$replacedSql = $unionSql;
				} else if($parsed['UPDATE']) {
				    //UPDATE操作需要的一些特殊处理
				    if(count($allReplacedSql) > 1) {
				        Yii::log("[SHARDING-EXCEPTION][doRewriteSql] => update sharding multiple tables: $sql, 【replacedSql】: " . implode(";", $allReplacedSql), 'error');
				        $replacedSql = $allReplacedSql;
				    } else {
				        $replacedSql = $allReplacedSql[0];
				    }
				} else if($parsed['INSERT']) {
				    if(count($allReplacedSql) > 1) {
				        Yii::log("[SHARDING-EXCEPTION][doRewriteSql] => insert sharding multiple tables: $sql, 【replacedSql】: " . implode(";", $allReplacedSql), 'error');
				    } else {
				        $replacedSql = $allReplacedSql[0];
				    }
				} else if($parsed['DELETE']) {
				    if(count($allReplacedSql) > 1) {
				        Yii::log("[SHARDING-EXCEPTION][doRewriteSql] => delete sharding multiple tables: $sql, 【replacedSql】: " . implode(";", $allReplacedSql), 'error');
				        $replacedSql = $allReplacedSql;
				    } else {
				        $replacedSql = $allReplacedSql[0];
				    }
				}
				
    			/**
    			 * TODO
    			 * 如果要求生成的ID强有序（即后插入的ID值一定比当前表中最大的ID值大）
    			 */
    			foreach($allReplaceTables as $oriTable => $newTable) {
    				$strategy = $this -> strategy[$oriTable];
    				if($strategy['force_ordered']) {
    					//取出当前最大值
    					
    				}
    				//因为INSERT操作只能对应一张表，所以直接退出
    				break;
    			}
			}
		} catch (Exception $e) {
			Yii::log("[SHARDING-EXCEPTION][doRewriteSql] => " . $e->getMessage() . ", sql: $sql", 'error');
		}
		if($replacedSql != $sql) {
			Yii::log("[SHARDING-RESULT]-----[oldsql] => $sql -----[newsql] => $replacedSql", 'info');
		} else {
			Yii::log("[SHARDING-RESULT][UNCHANGED] => $sql", 'warning');
		}
		return $replacedSql;
	}
	
	/**
	 * 支持count, min, max, sum
	 * 支持group by
	 */
	private function refactorSelectFields($parsed) {
		$selectedSql = "*"; 
		
	    $fieldsName = array();
	    foreach($parsed['SELECT'] as $item) { 
	        if($item['expr_type'] == 'colref') {
	            if($item['alias']) {
	                $fieldsName[] = "`" . $item['alias']['name'] . "`";
	            } else {
	                $columnExpression = $item['no_quotes']['parts'];
	                if(count($columnExpression)) {
	                   $fieldsName[] = "`" . $item['no_quotes']['parts'][count($columnExpression)-1] . "`";
	                } else {
	                   $fieldsName[] = "*";
	                }
	            }
	        } else if($item['expr_type'] == 'aggregate_function') {
	            if(strcasecmp($item['base_expr'], 'count') === 0 || strcasecmp($item['base_expr'], 'sum') === 0) {
	                //先取出结果别名，如果没有别名，取直接取count(1)即可
	                $selectedFieldAlias = $item['alias']['name'];
	                if(!isset($selectedFieldAlias) || empty($selectedFieldAlias)) {
	                    //用sql转义字符包围
	                    $selectedFieldAlias = '`count(1)`';
	                }
	                $fieldsName[] = "SUM($selectedFieldAlias) AS $selectedFieldAlias" ;
	            }
	            if(strcasecmp($item['base_expr'], 'max') === 0 || strcasecmp($item['base_expr'], 'min') === 0) {
	                $functionName = strtolower($item['base_expr']);
	            
	                $selectedFieldAlias = $item['alias']['name'];
	                if(!isset($selectedFieldAlias) || empty($selectedFieldAlias)) {
	                    //取到列名
	                    $columnExpr = $item['sub_tree'][0]['base_expr'];
	                    $selectedFieldAlias = "`$functionName($columnExpr)`";
	                }
	                $fieldsName[] = "$functionName($selectedFieldAlias) AS $selectedFieldAlias" ;
	            }
	        }
	    }
	    if(count($fieldsName)) {
	        $selectedSql = implode(",", $fieldsName);
	    }		    
		return $selectedSql;
	}
	
	private function appendLimitToUnionSql($parsedLimit) {
		$newLimitSql = "";
		if($parsedLimit) {
			$builder = new LimitBuilder();
			$newLimitSql = $builder ->build($parsedLimit);
		}
		return $newLimitSql;
	}
	
	private function appendGroupToUnionSql($parsedGroup) {
	    $newGroupSql = "";
	    if($parsedGroup) {
	        foreach($parsedGroup as $key => $eachColumn) {
	            if($eachColumn['expr_type'] == 'colref' && $eachColumn['no_quotes']['delim']) {
	                $eachColumn['base_expr'] = "`" . $eachColumn['no_quotes']['parts'][1] . "`";
	                unset($eachColumn['no_quotes']['parts'][0]);
	                $eachColumn['no_quotes']['delim'] = '';
	            }
	            $parsedGroup[$key] = $eachColumn;
	        }
	        $groupBuilder = new GroupByBuilder();
	        $newGroupSql = $groupBuilder -> build($parsedGroup);
	    }
	    return $newGroupSql;
	}
	
	private function appendOrderToUnionSql($parsedOrder) {
		$newOrderSql = "";
		if($parsedOrder) {
			foreach($parsedOrder as $orderKey => $eachOrder) {
				//如果是带表别名的格式：ORDER BY m.id > 0 ASC
				if($eachOrder['expr_type'] == 'colref' && $eachOrder['no_quotes']['delim']) {
					$eachOrder['base_expr'] = "`" . $eachOrder['no_quotes']['parts'][1] . "`";
					unset($eachOrder['no_quotes']['parts'][0]);
					$eachOrder['no_quotes']['delim'] = '';
				}
					
				if($eachOrder['expr_type'] == 'aggregate_function' && $eachOrder['sub_tree'][0]['no_quotes']['delim']) {
					$eachOrder['sub_tree'][0]['base_expr'] = $eachOrder['sub_tree'][0]['no_quotes']['parts'][1];
					unset($eachOrder['sub_tree'][0]['no_quotes']['parts'][0]);
					$eachOrder['sub_tree'][0]['no_quotes']['delim'] = '';
				}
				$parsedOrder[$orderKey] = $eachOrder;
			}
			$orderBuilder = new OrderByBuilder();
			$newOrderSql = $orderBuilder -> build($parsedOrder);
		}
		return $newOrderSql;
	}
	
}

?>
