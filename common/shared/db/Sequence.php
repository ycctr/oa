<?php
/**
 * 用于全局ID生成的Sequence表
 * @author zhangshuguo
 *
 */
class Sequence extends DBModel{
	
	public function __construct($sequenceName=false)
	{
	    if(!$sequenceName) {
	        //暂时使用此表；结合最近半年的数据，当前的序列每个月只用到总额度的3.6%。所以开放给其它业务直接使用
	        $sequenceName = 'kefu_push_sequence';
	    }
		$this->table = $sequenceName;
		parent::__construct(Config::$sequenceMysqlConfigRw, Config::$sequenceMysqlConfigRw, DBModel::MODE_RW);
	}

	/**
	 * 在生成规则中混入时间信息，这样按时间分表时就可以直接定位到表
	 * 
	 */
	public function getId() {
		$result = $this -> db() -> multi_query("REPLACE INTO " . $this->table . " (stub) VALUES ('a');SELECT LAST_INSERT_ID();");
		$generatedId = $result[0][0];
		//每个月的第一天重置起始值（这里假设每一天至少都会有1次的调用，依目前的业务，这点应该是可以保证的）
		if(date('d') == '01') {
		    //时间戳*1000，这样一个月内的可用值达到20亿级别，即使所有业务使用同一张sequence表，此值的可用数仍然是当前业务需求的5倍以上
		    $monthFirstId = strtotime(date('Y-m-d 00:00:00'))*1000;
		    //保证只修复一次
		    if($generatedId < $monthFirstId) {
		        //如果当前生成的是奇数，则必须修改起始值，保证其后续仍然生成的是奇数
		        if($generatedId&1 == 1) {
		            $monthFirstId ++;
		        }
		        //修改自增起始值为新的值，需要验证一下数据库时间，避免客户端时间污染
		        $dataBaseTime = $this -> db() -> query("SELECT DATE_FORMAT(NOW(), '%Y-%m-%d') AS databaseTime FROM DUAL");
		        if($dataBaseTime) {
		            $dataBaseTime = $dataBaseTime[0]['databaseTime'];
		            if($dataBaseTime != date('Y-m-d')) {
		                return $generatedId;
		            }
		        }
		        
		        $this -> db() -> query("ALTER TABLE " . $this->table . "  AUTO_INCREMENT = $monthFirstId");
		        $result = $this -> db() -> multi_query("REPLACE INTO " . $this->table . " (stub) VALUES ('a');SELECT LAST_INSERT_ID();");
		        $generatedId = $result[0][0];
		    }
		}
		return $generatedId;
	}
	
	public function getIdWithUpdate() {
		$result = $this -> db() -> multi_query("UPDATE message_sequence_u SET id = LAST_INSERT_ID(id+2); SELECT LAST_INSERT_ID()");
		return $result[0][0];
	}
}

?>