<?php
/**
* brief of DBStmt.php:
*
*by zdj
*/

require_once('DB.php');

// never create instance by yourself
class DBStmt
{
    private $stmt;

    public function __construct(mysqli_stmt $stmt)
    {
        $this->stmt = $stmt;
    }

    public function __destruct()
    {
        if($this->stmt !== NULL)
        {
            $this->close();
        }
    }

    public function close()
    {
        $this->stmt->close();
        $this->stmt = NULL;
    }

    public function __get($name)
    {
        switch($name)
        {
            case 'stmt':
                return $this->stmt;
            case 'error':
                return $this->stmt->error;
            case 'errno':
                return $this->stmt->errno;
            case 'affectedRows':
                return $this->stmt->affected_rows;
            case 'paramCount':
                return $this->stmt->param_count;
            default:
                return NULL;
        }
    }
    
    /**
     * args[0] : types
     * args[1...] : bind value
     */
    public function bindParam($args)
    {
        if(!is_array($args) || count($args)<2)
            return true;

        //参数错误。
        if(!preg_match('/^[idsb]+$/',$args[0]) || count($args)!=strlen($args[0])+1){
            Yii::log('W1415122967 '.'bind argument error:'.json_encode($args), 'error');
            return false;
        }

        $args = $this->intLimitCheck($args);

        if (strnatcmp(phpversion(),'5.3')>=0) {
            return call_user_func_array(array($this->stmt, 'bind_param'), $this->makeValuesRef($args));
        }
        return call_user_func_array(array($this->stmt, 'bind_param'), $args);
    }
    //对于传了多个参数,php5.3以上版本特殊处理,改为引用类型
    private function makeValuesRef(&$arr){
        $refs = array();
        foreach($arr as $key => $value)
            $refs[$key] = &$arr[$key];
        return $refs;
    }
    
    /**
     * 使用整数类型(i)时，可能超过32位长度。在此可以调整为s。
     *
     * http://www.php.net/manual/en/mysqli-stmt.bind-param.php#89544
     * If you specify type "i" (integer), the maximum value it allows you to have is 2^32-1 or 2147483647. So, if you are using UNSIGNED INTEGER or BIGINT in your database, then you are better off using "s" (string) for this.
     * 
     * Here's a quick summary:
     * (UN)SIGNED TINYINT: I
     * (UN)SIGNED SMALLINT: I
     * (UN)SIGNED MEDIUMINT: I
     * SIGNED INT: I
     * UNSIGNED INT: S
     * (UN)SIGNED BIGINT: S
     *
     * (VAR)CHAR, (TINY/SMALL/MEDIUM/BIG)TEXT/BLOB should all have S.
     *
     * FLOAT/REAL/DOUBLE (PRECISION) should all be D.
     */
    private function intLimitCheck($args){
        $types = $args[0];
        $needAdj = false;
        for($i=0;$i<strlen($types);$i++){
            if($types[$i]=='i'){
                //frank change : allow "i" bigger than  2^32-1 or 2147483647
                //if(!is_numeric($args[$i+1]) || $args[$i+1]>=(pow(2,31)-1))
                if(!is_numeric($args[$i+1]))
                {
                    $types[$i]='s';  //替换成字符串。
                    $needAdj = true;
                }
            }
        }

        if($needAdj){
            $args[0]=$types;

			$debug_backtrace = debug_backtrace();
			$debug_path = '';
			foreach($debug_backtrace as $path){
				$debug_path .= $path['file'].'('.$path['line'].');';
			}
            Yii::log('W1415123018 '.'need change bind_stmt types to:'.json_encode($args).' 调用路径：'.$debug_path, 'warning');
        }

        return $args;
    }

    public function execute()
    {
        if(!$this->stmt->execute()){
            return false;
        }
        // get metadata
        $res = $this->stmt->result_metadata();
        // no result
        if(!$res){
            return true;
        }
        $finfo = $res->fetch_fields();
        $res->free();

        // store result
        if(!$this->stmt->store_result())
        {
            return false;
        }

        /////////////////////////////
        //来自：http://www.php.net/manual/en/mysqli-stmt.bind-result.php#102179
        $variables = array();
        $data = array();
        $meta = $this->stmt->result_metadata();
        while($field = $meta->fetch_field()){
            $variables[] = &$data[$field->name]; // pass by reference
        }
        call_user_func_array(array($this->stmt, 'bind_result'), $variables);
        
        $i=0;
        $ret = array();
        while($this->stmt->fetch())
        {
            $ret[$i] = array();
            foreach($data as $k=>$v)
                $ret[$i][$k] = $v;
            $i++;
            // don't know why, but when I tried $array[] = $data, I got the same one result in all rows
        }

        ////////////////////////////

        $this->stmt->free_result();
        return $ret;
    }

    public function getAffectedRows()
    {
        return $this->stmt->affected_rows;
    }

    public function getParamCount()
    {
        return $this->stmt->param_count;
    }

    public function errno()
    {
        return $this->stmt->errno;
    }

    public function error()
    {
        return $this->stmt->error;
    }
}

?>
