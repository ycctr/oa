<?php
/**
 * TODO: 部分和mis等相关的东西，需要移走。
 */
abstract class DBModel
{
    const MODE_RW = 'rw';//read write db connection to dbname xx
    const MODE_RO = 'ro';//read only connection when no read write connection to db has established
    const MODE_RA = 'ra';//read only connection anyway,no mater is there read write connection has been established
    
    //当前对象的数据库名称，用于从静态数据库对象中选取。
    protected $dbname; 
    //由于多个子类都使用同一个静态对象，需要根据dbname做成map --zyf
    //为了mis的慢查询问题，对一个数据库支持两个连接，一个读写，一个只读 --frank
    private static $dbObjMap = array();
   
    //ro : 只读,但是有了读写连接后直接使用读写连接 
    //rw : 读写
    //ra : 只读,即使有了读写连接仍然建立只读连接
    protected $mode = 'rw';

    protected $riskCheck = 1;
    protected $riskLimit = 70;
    protected $strictCheck = 1;
    protected $useMysqlMis = 0;
    
    //继承说明：必须在构造函数中指定。
    protected $rwConfig = array();
    protected $roConfig = array();

    private static $_fields = array();
    private static $_tableSchemas = array();  //存储表结构，用户获取默认值，用于数据拉链。
    public $arrParams = array();

    //增加一个默认值，可以指定对字符串字段的查询是否使用like
    protected $searchWithLikeCondition = true;
    
    private $shardingStrategy = false;

    //sql risk check functionality removed
    public function setRiskCheck($riskCheck)
    {
        $this->riskCheck = $riskCheck;
    }
    
    private $checkConn = false;

    //为了性能的考虑,将DESC table 从构造函数中提出来
    public function __get($item)
    {

        if (strtolower($item) == 'fields' && $this->table  )
        {
            if(empty(self::$_fields[$this->table])){

                $strSql = 'DESC `' . $this->table . '`';
                $arrRows = $this->db()->query($strSql);

                if(empty($arrRows)){
                    Yii::log('W1415154275 '.'TABLE NOT EXIST: ' . $this -> table, 'error');
                }else{
                    self::$_tableSchemas[$this->table] = $arrRows;
                }
                 
                foreach ($arrRows as $item)
                {
                    self::$_fields[$this->table][] = $item['Field'];
                }
            }
            return self::$_fields[$this->table];
        }else{
            Yii::log('W1415154284 '."invoke __get,item=$item,table=" . $this->table,'error');
        }
        return null;
    }
    public function getTableSchema()
    {
        if(empty(self::$_tableSchemas[$this->table])){
            $tmp = $this->fields;  //重新获取一下。
        }
        return self::$_tableSchemas[$this->table];
    }

    /*
        支持自定义查询
        $city = new City();
        $ret = $city->select('id,name,domain') -> condition( ['is_open' => 1])->orderby(['domain'=>'asc']) ->limit(0,10) -> all();
        print_r($ret);
     */
    public function  __call($funcname,$args){
        $this->searchWithLikeCondition = false;
        $funcname = strtolower($funcname);
        if($funcname == 'count')
        {
            $result =  $this->getCountSimple();
            $this->arrParams = [];
            return $result;
        }elseif($funcname == 'all')
        {
            $result = $this->getListSimple();
            $this->arrParams = [];
            return $result;
        }elseif($funcname == 'one')
        {
            $result = $this->getListSimple();
            $this->arrParams = [];
            if(!empty($result))
                return $result[0];
            return [];
        }
        if(count($args) == 0){
            Yii::log('W1434511762 invalid param where invoke __call,name=' .$funcname,'warning');
            return $this;
        }
        if($funcname == 'select')
            $this->arrParams['fields'] = $args[0];
        elseif($funcname == 'condition'){
                $this->arrParams['condition'] = $args[0];
        }elseif($funcname == 'where'){
            if(defined('STRICT_SQL') && STRICT_SQL==1 && !empty($args[0]) )
            {
                    throw new SystemException('外部访问站点不容许使用where拼写sql:' . $args[0]);
            }
            $this->arrParams['where'] = $args[0];
        }
        elseif($funcname == 'orderby')
            $this->arrParams['orderby'] = $args[0];
        elseif($funcname == 'limit'){
            if(count($args) ==1){
                $this->arrParams['offset'] =0;
                $this->arrParams['limit'] = $args[0];
            }else{
                $this->arrParams['offset'] = $args[0];
                $this->arrParams['limit'] = $args[1];
            }
        }else{
            Yii::log('W1434520007 invalid function where invoke __call,name=' .$funcname,'warning');
        }
        return $this;
    }


    public function __construct($rwConfig, $roConfig, $mode, $shardingStrategy=false)
    {
        $this->mode = $mode;
        $this->rwConfig = $rwConfig;
        $this->roConfig = $roConfig;
		
        //提取dbname
        if(!empty($rwConfig)) {
            $this->dbname = $rwConfig['auth']['dbname'];
        }elseif(!empty($roConfig)){
            $this->dbname = $roConfig['auth']['dbname'];
        }

        if(empty($this->dbname)){
            throw new SystemException("数据库配置中未指定数据库名称!");
        }
        if($shardingStrategy) {
        	$this -> shardingStrategy = $shardingStrategy;
        }
    }
    
    //2016.06.07, add by zouyoufang, 是否在使用前检查连接。
    public function setCheckConn($checkConn)
    {
        $this->checkConn = $checkConn;
    }
    
    //检查当前数据库连接状态，假如连接失败，重置连接单例数组。
    private function checkConnection()
    {
        $dbLinkRW = self::$dbObjMap[$this->dbname][self::MODE_RW];
        $dbLinkRO = self::$dbObjMap[$this->dbname][self::MODE_RO];
        //检查$dbLinkRO
        if(($this->mode==self::MODE_RO || $this->mode==self::MODE_RA) && !empty($dbLinkRO)){
            if(!@$dbLinkRO->ping()){
                Yii::log($this->dbname . " dbLinkRO连接检查失败，清空数组中内容", 'info');
                //$this->_clearTableHook($dbLinkRO);
                $dbLinkRO->setCheckClosed(true);
                unset(self::$dbObjMap[$this->dbname][self::MODE_RO]);   
            }
        }elseif(!empty($dbLinkRW) && $this->mode!==self::MODE_RA){ 
            if(!@$dbLinkRW->ping()){
                Yii::log($this->dbname . " dbLinkRW连接检查失败，清空数组中内容", 'info');
                //$this->_clearTableHook($dbLinkRW);
                $dbLinkRW->setCheckClosed(true);
                unset(self::$dbObjMap[$this->dbname][self::MODE_RW]);
            }
        }        
    }

    public function db()
    {
        //2016.06.07, add by zouyoufang, 批处理任务，长时间不使用，使用前检查一下。
        if($this->checkConn){
            $this->checkConnection();
        }
        
        $action = 0;//0 无动作 1 建立读写连接  2 建立只读连接 --frank
        //当前数据库连接的读写方式。由于db为静态的，会被多个实例共享，会碰到从只读模式提升到读写模式的情况。
        $dbLinkRW = self::$dbObjMap[$this->dbname][self::MODE_RW];
        $dbLinkRO = self::$dbObjMap[$this->dbname][self::MODE_RO];
        if($this->mode==self::MODE_RW && empty($dbLinkRW))
        {
            $action = 1;
            if(isset($dbLinkRO))
                Yii::log("数据库连接需要从只读模式提升到写模式",'info');
        }
        elseif($this->mode==self::MODE_RA && empty($dbLinkRO))
        {
            $action = 2;
        }
        elseif($this->mode==self::MODE_RO && empty($dbLinkRO) && empty($dbLinkRW))
        {
            $action = 2;
        }

        if($action == 2 && empty($this->roConfig))//如果没有配置只读的集群，改为建立读写连接
        {
            $action = 1;
            Yii::log("缺少只读集群配置信息，请检查类".get_class($this),'error');
        }

        if($action > 0)
        {
            if($action == 1)
                $config = $this->rwConfig;
            else
                $config = $this->roConfig;

            if($this->mode == self::MODE_RA)//mis等只读从库，采用先连接只读从库，失败才连接其他数据库
                $balance = 0;
            else
                $balance = mt_rand(1,count($config['machine']));
            $handle = ConnectionMan::getMysqli($selServer, $config['machine'],$config['auth'],
                $config['talk']['connect_timeout_ms'],$config['talk']['strategy'], $balance);
            if($handle != false){
                $linkType = $this->mode == self::MODE_RW ? self::MODE_RW : self::MODE_RO;
                if($linkType == self::MODE_RW)
                {
                    self::$dbObjMap[$this->dbname][$linkType] = $dbLinkRW = new DB(false,$handle, $linkType, $this -> shardingStrategy);
                    $this->addHook($dbLinkRW);
                }
                else
                {
                    self::$dbObjMap[$this->dbname][$linkType] = $dbLinkRO = new DB(false,$handle, $linkType, $this -> shardingStrategy);
                    $this->addHook($dbLinkRO);
                }
            }else{
                throw new SystemException('连接数据库出错!!!');
            }
        }

            /* 
            //php5.5默认使用mysqlnd，可以在php.ini中设置 mysqlnd.net_read_timeout，达到类似的效果
            //trick补丁, zouyoufang, 2014.10.16：将一些参数打入auth中，用做连接数据库的mysql_options
            $patchConf = array('read_timeout_ms');
            foreach($patchConf as $patchK){
                if(!isset($config['auth'][$patchK]) && isset($config['talk'][$patchK]))
                    $config['auth'][$patchK] = $config['talk'][$patchK];
            }
            //end trick
            */

        /**
         * add by shuguo
         * 默认需要有riskCheck，但有些特殊情况需要屏蔽此检查
         * 现在的实现因为具体的DB只会实例化一次，所以在调用时通过setRiskCheck()来控制是否增加rishCheck hook其实已经没有用了
         * 做为一种过渡，这里增加一个检查，可将控制粒度细化到每一次调用时，而不是第一次实例化一个DB实例时
         */
        //为不改变目前的方式，对于RO的有读写连接是返回读写连接，无读写连接时才返回只读连接
        //而RA模式之返回只读连接,RW模式之返回读写连接--frank
        if($this->mode==self::MODE_RW || ($this->mode==self::MODE_RO && !empty($dbLinkRW)) || empty($dbLinkRO) )
            $retLink = $dbLinkRW;
        else
            $retLink = $dbLinkRO;
        
        //因为$retLink缓存有可能被缓存需要重置table hook
        if ($retLink) {
            $this->_clearTableHook($retLink);
            $this->_addTableHook($retLink);
        }
        
        return $retLink;
    }
    
    protected function _addTableHook($db) {
        return true;
    }
    
    protected function _clearTableHook($db) {
        $db->clearTableHook();
    }
    
    //子类可以继承
    protected function addHook($db)
    {
        $db->query('set names utf8');
        $db->addHook(DB::HK_BEFORE_QUERY,1,array($this,'logDebug'));
        $db->addHook(DB::HK_AFTER_QUERY,1,array($this,'logError'));
        register_shutdown_function(array($db,'commit'));
    }
   
    public function logDebug($db,&$sql,$args)
    {
        Yii::trace("execute sql=".$sql);
        return true;
    }

    public function logError($db,&$ret,$args)
    {
        if($ret === false)
        {
            Yii::log('W1415154403 '."failed sql=".$db->lastSQL."  error msg:".$db->error,'error');
        }
    }
    

    public function setParams($arrParams)
    {
        if(defined('STRICT_SQL') && STRICT_SQL==1 && $this->strictCheck==1){
            if(!empty($arrParams['where'])){
                throw new SystemException('外部访问站点不容许使用where拼写sql:' . $arrParams['where']);
            }
        }
        if(isset($arrParams['likeSearch'])){
            $this->searchWithLikeCondition = $arrParams['likeSearch'];
            unset($arrParams['likeSearch']);
        }
        $this->arrParams = $arrParams;
    }

    /**
    * @brief    获取记录数，主要用于分页
    * 前端禁止: where
    */
    public function getCount()
    {
        $intCount = 0;
        $strSql = 'SELECT COUNT(*) AS total FROM `' . $this->table . '` WHERE 1 = 1';

        //存在is_del字段
        if (in_array('is_del', $this->fields))
        {
            if (intval($this->arrParams['condition']['is_del']) === 1)
            {
            }
            else
            {
                $strSql.= ' AND `is_del` = 0';
            }
        }

        if (!empty($this->arrParams['condition']))
        {
            foreach ($this->arrParams['condition'] as $key => $value)
            {
                //2013.9.9 modify by caoxialin，bug fix，只对字符串进行trim
                if (!is_array($value))
                {
                    $value = trim($value);
                }

                if ($key === 'is_del')
                {
                    continue;
                }

                if (is_array($value))
                {
                    $strSql.= ' AND `' . $key . '` IN ("' . implode('","', $value) . '")';
                }
                elseif (intval($value) > 0 || strval($value) == "0")
                {
                    $strSql.= ' AND `' . $key . '` = "' . $value . '"';
                }
                else
                {
                    //为了防止 like ’%%‘这样的语句，做一个特殊处理。不过%%这个坑还要埋多久？……
                    //by xiaochang 2015-05-28
                    if($value==''){
                        $strSql.= ' AND `' . $key . '`=""';
                    }
                    else{
                        if($this->searchWithLikeCondition){
                            $strSql.= ' AND `' . $key . '` LIKE "%' . $value . '%"';
                        }
                        else{
                            $strSql.= ' AND `' . $key . '` = "' . $this->db()->escapeString($value) . '"';
                        }
                    }
                }
            }
        }
		
		if (!empty($this->arrParams['where']))
		{
			$strSql .= " AND (".$this->arrParams['where'].")";
		}
        //对于mis，记录日志。
		if(!empty($_SESSION['mis_user_id'])){
			Yii::log(sprintf('sql_log : mis_user_id=%s ; mis_user_name=%s ; sql=[%s]', $_SESSION['mis_user_id'], $_SESSION['mis_user_name'], $strSql),'info');
		}else{
			Yii::log(sprintf('sql_log : sql=[%s]', $strSql),'info');
		}

        $arrRows = $this->db()->query($strSql);
        return $arrRows[0]['total'];
    }
    
    public function getCountSimple(){
        $intCount = 0;
        $strSql = 'SELECT COUNT(*) AS total FROM `' . $this->table . '` WHERE 1 = 1';
        
        if (!empty($this->arrParams['condition']))
        {
            foreach ($this->arrParams['condition'] as $key => $value)
            {
                //2013.9.9 modify by caoxialin，bug fix，只对字符串进行trim
                if (!is_array($value))
                {
                    $value = trim($value);
                }
                if (is_array($value))
                {
                    $strSql.= ' AND `' . $key . '` IN ("' . implode('","', $value) . '")';
                }
                elseif (intval($value) > 0 || strval($value) == "0")
                {
                    $strSql.= ' AND `' . $key . '` = "' . $value . '"';
                }
                else
                {
                    //为了防止 like ’%%‘这样的语句，做一个特殊处理。不过%%这个坑还要埋多久？……
                    //by xiaochang 2015-05-28
                    if($value==''){
                        $strSql.= ' AND `' . $key . '`=""';
                    }
                    else{
                        if($this->searchWithLikeCondition){
                            $strSql.= ' AND `' . $key . '` LIKE "%' . $value . '%"';
                        }
                        else{
                            $strSql.= ' AND `' . $key . '` = "' . $this->db()->escapeString($value) . '"';
                        }
                    }
                }
            }
        }
        
        if (!empty($this->arrParams['where']))
        {
            $strSql .= " AND (".$this->arrParams['where'].")";
        }
        //对于mis，记录日志。
        if(!empty($_SESSION['mis_user_id'])){
            Yii::log(sprintf('sql_log : mis_user_id=%s ; mis_user_name=%s ; sql=[%s]', $_SESSION['mis_user_id'], $_SESSION['mis_user_name'], $strSql),'info');
        }else{
            Yii::log(sprintf('sql_log : sql=[%s]', $strSql),'info');
        }

        $arrRows = $this->db()->query($strSql);
        return $arrRows[0]['total'];
    }
    
    /**
     * 建议使用这个方法，避免is_del字段的错误使用
     */
    public function getListSimple(){
        $strFields = isset($this->arrParams['fields']) ? $this->arrParams['fields'] : '*';
        $strSql = 'SELECT ' . $strFields . ' FROM `' . $this->table . '` WHERE 1 = 1';
        
        $strOrderBy = '';
        
        if (!empty($this->arrParams['condition']))
        {
            foreach ($this->arrParams['condition'] as $key => $value)
            {
                //2013.9.9 modify by caoxialin，bug fix，只对字符串进行trim
                if (!is_array($value))
                {
                    $value = trim($value);
                }

                if (is_array($value))
                {
                    $strSql.= ' AND `' . $key . '` IN ("' . implode('","', $value) . '")';
                }
                elseif (intval($value) > 0 || $value == '0')
                {
                    $strSql.= ' AND `' . $key . '` = "' . $this->db()->escapeString($value) . '"';
                }
                else
                {
                    //为了防止 like ’%%‘这样的语句，做一个特殊处理。不过%%这个坑还要埋多久？……
                    //by xiaochang 2015-05-28
                    if($value==''){
                        $strSql.= ' AND `' . $key . '`=""';
                    }
                    else{
                        if($this->searchWithLikeCondition){
                            $strSql.= ' AND `' . $key . '` LIKE "%' . $this->db()->escapeString($value) . '%"';
                        }
                        else{
                            $strSql.= ' AND `' . $key . '` = "' . $this->db()->escapeString($value) . '"';
                        }
                    }

                }
            }
        
        }
        
        if (!empty($this->arrParams['where']))
        {
            $strSql .= " AND (".$this->arrParams['where'].")";
        }
        if (isset($this->arrParams['groupby']))
        {
            $strSql.= ' GROUP BY ' . $this->arrParams['groupby'];
            if(isset($this->arrParams['having'])){
                $strSql.= ' HAVING ' . $this->arrParams['having'];
            }
        }
        if (!empty($this->arrParams['orderby']))
        {
            $arrOrderBy = array();
            foreach ($this->arrParams['orderby'] as $key => $value)
            {
                $arrOrderBy[] = '`' . $key . '` ' . $value;
            }
            if (!empty($strOrderBy))
            {
                $strOrderBy.= ',' . implode(',', $arrOrderBy);
            }
            else
            {
                $strOrderBy = ' ORDER BY ' . implode(',', $arrOrderBy);
            }
        }
        $strSql.= $strOrderBy;
        if (isset($this->arrParams['offset']) && isset($this->arrParams['limit']))
        {
            $strSql.= ' LIMIT ' . intval($this->arrParams['offset']) . ',' . intval($this->arrParams['limit']);
        }
        
        //对于mis，记录日志。
        if(!empty($_SESSION['mis_user_id'])){
            Yii::log(sprintf('sql_log : mis_user_id=%s ; mis_user_name=%s ; sql=[%s]', $_SESSION['mis_user_id'], $_SESSION['mis_user_name'], $strSql),'info');
        }else{
            Yii::log(sprintf('sql_log : sql=[%s]', $strSql),'info');
        }
        if (($arrResult = $this->db()->query($strSql)) !== false)
        {
            return $arrResult;
        }
        else
        {
            return array();
        }
    }

    /*
    * @brief    获取数据列表
    * 这里的is_del很让人崩溃
    */
    public function getList()
    {
        $strFields = isset($this->arrParams['fields']) ? $this->arrParams['fields'] : '*';
        $strSql = 'SELECT ' . $strFields . ' FROM `' . $this->table . '` WHERE 1 = 1';

        $strOrderBy = '';
        //存在is_del字段
        if ( is_array($this->fields) && in_array('is_del', $this->fields))
        {
            if (intval($this->arrParams['condition']['is_del']) === 1)
            {
            }
            else
            {
                $strSql.= ' AND `is_del` = 0';
            }

            $strOrderBy = ' ORDER BY `is_del` ASC';
        }
        
        if (!empty($this->arrParams['condition']))
        {
            foreach ($this->arrParams['condition'] as $key => $value)
            {
                if ($key === 'is_del')
                {
                    continue;
                }
                //2013.9.9 modify by caoxialin，bug fix，只对字符串进行trim
                if (!is_array($value))
                {
                    $value = trim($value);
                }
                
                //2013.8.18 add by caoxiaolin，支持array
                if (is_array($value))
                {
                    $strSql.= ' AND `' . $key . '` IN ("' . implode('","', $value) . '")';
                }
                elseif (intval($value) > 0 || $value == '0')
                {
                    $strSql.= ' AND `' . $key . '` = "' . $this->db()->escapeString($value) . '"';
                }
                else
                {
                    //为了防止 like ’%%‘这样的语句，做一个特殊处理。不过%%这个坑还要埋多久？……
                    //by xiaochang 2015-05-28
                    if($value==''){
                        $strSql.= ' AND `' . $key . '`=""';
                    }
                    else{
                        if($this->searchWithLikeCondition){
                            $strSql.= ' AND `' . $key . '` LIKE "%' . $this->db()->escapeString($value) . '%"';
                        }
                        else{
                            $strSql.= ' AND `' . $key . '` = "' . $this->db()->escapeString($value) . '"';
                        }
                    }

                }
            }

        }

        if (!empty($this->arrParams['where']))
        {
            $strSql .= " AND (".$this->arrParams['where'].")";
        }
        if (isset($this->arrParams['groupby']))
        {
        	$strSql.= ' GROUP BY ' . $this->arrParams['groupby'];
        	if(isset($this->arrParams['having'])){
        		$strSql.= ' HAVING ' . $this->arrParams['having'];
        	}
        }
        if (!empty($this->arrParams['orderby']))
        {
            if (is_array($this->arrParams['orderby'])){
                $arrOrderBy = array();
                foreach ($this->arrParams['orderby'] as $key => $value)
                {
                    $arrOrderBy[] = '`' . $key . '` ' . $value;
                }
                if (!empty($strOrderBy))
                {
                    $strOrderBy.= ',' . implode(',', $arrOrderBy);
                }
                else
                {
                    $strOrderBy = ' ORDER BY ' . implode(',', $arrOrderBy);
                }
            }else{
                $strOrderBy = ' ORDER BY '.$this->arrParams['orderby'];
            }
        }
        $strSql.= $strOrderBy;
        if (isset($this->arrParams['offset']) && isset($this->arrParams['limit']))
        {
            $strSql.= ' LIMIT ' . intval($this->arrParams['offset']) . ',' . intval($this->arrParams['limit']);
        }
        
        //对于mis，记录日志。
		if(!empty($_SESSION['mis_user_id'])){
	      Yii::log(sprintf('sql_log : mis_user_id=%s ; mis_user_name=%s ; sql=[%s]', $_SESSION['mis_user_id'], $_SESSION['mis_user_name'], $strSql),'info');
		}else{
	      Yii::log(sprintf('sql_log : sql=[%s]', $strSql),'info');
		}
    //echo $strSql;
        if (($arrResult = $this->db()->query($strSql)) !== false)
        {
            return $arrResult;
        }
        else
        {
            return array();
        }
    }
    
    /**
     * 获取整形id对应的记录
     * 返回结果不会保留记录的原始php类型，以区别于getItemByPk()
     * @param int $id
     */
    public function getItemById($id) {
        $strSql = 'SELECT * FROM `' . $this->table . '` WHERE id = ' . intval($id);
        Yii::log(sprintf('sql_log : sql=[%s]', $strSql),'info');
        $arrRows = $this->db()->query($strSql);
        if (!empty($arrRows)){
            return $arrRows[0];
        }else{
            return array();
        }
    }

    /**
     * 暂固定主键为id。后续可以支持从memcache中获取。
     * 
     */
    public function getItemByPk($id, $pk='id', $lock=false){
        $strSql = 'SELECT * FROM `' . $this->table . '` WHERE ' . $pk . '=?';
        if($lock===true) 
            $strSql .= ' FOR UPDATE';
        
        //对于mis，记录日志。
        if(!empty($_SESSION['mis_user_id'])){
        	Yii::log(sprintf('sql_log : mis_user_id=%s ; mis_user_name=%s ; sql=[%s]', $_SESSION['mis_user_id'], $_SESSION['mis_user_name'], $strSql),'info');
        }else{
        	Yii::log(sprintf('sql_log : sql=[%s]', $strSql),'info');
        }
        
        $arrRows = $this->db()->query($strSql, 'i', $id);
        if (!empty($arrRows)){
            return $arrRows[0];
        }else{
            return array();
        }
    }

    /**
     * 暂固定主键为id。后续可以支持从memcache中获取。
     * 
     */
    public function getItemByUk($field, $v, $lock=false){
        $strSql = 'SELECT * FROM `' . $this->table . '` WHERE ' . $field . '=?';
        if($lock===true) 
            $strSql .= ' FOR UPDATE';
        
        $arrRows = $this->db()->query($strSql, 's', $v);
        if (!empty($arrRows)){
            return $arrRows[0];
        }else{
            return array();
        }
    }

    /*
    * @brief    获取单条记录信息
    *
    * 备注：前端不能where参数。
    */
    public function getItem()
    {
        $strFields = isset($this->arrParams['fields']) ? $this->arrParams['fields'] : '*';
        $strSql = 'SELECT ' . $strFields . ' FROM `' . $this->table . '` WHERE 1 = 1';
		unset($this->arrParams['fields']);//zhangji -add，如果字段里有fields就惨了。
        if (!empty($this->arrParams['where']))
        {
            $strSql .= " AND (".$this->arrParams['where'].")";
            unset($this->arrParams['where']);
        }
        if (!empty($this->arrParams))
        {
            foreach ($this->arrParams as $key => $value)
            {
                if(!is_scalar($value)){
                    $debug_backtrace = debug_backtrace();
                    $debug_path = '';
                    foreach($debug_backtrace as $path){
                        $debug_path .= $path['file'].'('.$path['line'].');';
                    }

                    Yii::log('W1445415724 '.get_class($this).'::getItem中传入值不是一个标量变量，因此被忽略。查询可能与预期不符。问题参数：'
                        .$key.'='.serialize($this->arrParams[$key]).' | 传入参数：'
                        .serialize($this->arrParams).'调用路径：'.$debug_path,'error');
                    continue;
                }
                $value = trim($value);
                $strSql.= ' AND `' . $key . '` = "' . $this->db()->escapeString($value) . '"';
            }
        }
        $strSql.= ' LIMIT 1';
        //对于mis，记录日志。
        if(!empty($_SESSION['mis_user_id'])){
        	Yii::log(sprintf('sql_log : mis_user_id=%s ; mis_user_name=%s ; sql=[%s]', $_SESSION['mis_user_id'], $_SESSION['mis_user_name'], $strSql),'info');
        }else{
        	Yii::log(sprintf('sql_log : sql=[%s]', $strSql),'info');
        }
        $arrRows = $this->db()->query($strSql);
        if (!empty($arrRows))
        {
            return $arrRows[0];
        }
        else
        {
            return array();
        }
    }
    
    /*
    * @brief    获取单条记录信息并锁表，等待更新(select ... for update)
    *
    * 备注：前端不能where参数。
    * added by guoxubin at 2014-04-18
    */
    public function getLockedItem()
    {
        $strFields = isset($this->arrParams['fields']) ? $this->arrParams['fields'] : '*';
        $strSql = 'SELECT ' . $strFields . ' FROM `' . $this->table . '` WHERE 1 = 1';
        unset($this->arrParams['fields']);
        if (!empty($this->arrParams['where']))
        {
            $strSql .= " AND (".$this->arrParams['where'].")";
            unset($this->arrParams['where']);
        }
        if (!empty($this->arrParams))
        {
            foreach ($this->arrParams as $key => $value)
            {
                if(!is_scalar($value)) continue;   
                $value = trim($value);
                $strSql.= ' AND `' . $key . '` = "' . $this->db()->escapeString($value) . '"';
            }
        }
        $strSql.= ' LIMIT 1';
        $strSql.=' FOR UPDATE ';//这个方法的关键
        //对于mis，记录日志。
        if(!empty($_SESSION['mis_user_id'])){
            Yii::log(sprintf('sql_log : mis_user_id=%s ; mis_user_name=%s ; sql=[%s]', $_SESSION['mis_user_id'], $_SESSION['mis_user_name'], $strSql),'info');
        }else{
            Yii::log(sprintf('sql_log : sql=[%s]', $strSql),'info');
        }
        $arrRows = $this->db()->query($strSql);
        if (!empty($arrRows))
        {
            return $arrRows[0];
        }
        else
        {
            return array();
        }
    }

    protected  function serialize(&$row)
    {
        if(is_array($row))
        {
            foreach($row as &$item)
            {
                if(is_array($item))
                {
                    $item = serialize($item);
                }
            }
        }
    }
    
    /**
     * 保存信息
     * @param string $tname 表名，考虑到分库分表新增。by zouyoufang, 20150913 
     */
    public function save($arrData,$insertWithId = 0, $tname='')
    {
        if(empty($tname)){
            $tname = $this->table;
        }

        $this->serialize($arrData);
        $arrValues = array();
        $where_id = 0;
        if (intval($arrData['id']) > 0 && $insertWithId == 0)
        {
            $where_id = $arrData['id'] + 0;

            foreach ($arrData as $key => $value)
            {
                if (in_array($key, $this->fields))
                {
                    if(!is_scalar($value)) continue;   
                    $value = trim($value);
                    $arrValues[] = '`' . $key . '` = "' . $this->db()->escapeString($value) . '"';
                }
            }
            $strSql = 'UPDATE `' . $tname . '` SET ' . implode(',', $arrValues) . ' WHERE id = ' . $where_id;
        }
        else
        {

            if($insertWithId == 0)
                unset($arrData['id']);
            $arrFields = array();
            foreach ($arrData as $key => $value)
            {
                if (in_array($key, $this->fields))
                {
                    $value = trim($value);
                    $arrFields[] = '`' . $key . '`';
                    $arrValues[] = '"' . $this->db()->escapeString($value) . '"';
                }
            }
            $strSql = 'INSERT INTO `' . $tname . '` (' . implode(',', $arrFields) . ') VALUES (' . implode(',', $arrValues) . ')';

        }

		//if(!empty($_SESSION['mis_user_id'])){
	      Yii::log(sprintf('sql_log : mis_user_id=%s ; mis_user_name=%s ; sql=[%s]', $_SESSION['mis_user_id'], $_SESSION['mis_user_name'], $strSql),'info');
		//}
        $mixResult = $this->db()->query($strSql);
		if ($mixResult !== false)
        {
            if (intval($arrData['id']) === 0)
            {
                return $this->db()->getInsertID();
            }
            else
            {
                return intval($arrData['id']);
            }
        }
        else
        {
            Yii::log(sprintf('write sql error: sql=[%s]', $strSql), 'error');
            return false;
        }
    }
}
