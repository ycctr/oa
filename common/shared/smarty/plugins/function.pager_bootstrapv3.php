<?php

/**
 * Smarty plugin
 * bootstrap v3 分页
 * @author Huangjiyun <huangjiyun@rong360.com>
 * @date 2016-03-23 07:01:30
 * @version 
 */
function smarty_function_pager_bootstrapv3($arrParams, &$smarty)
{
    if (intval($arrParams['page']) === 0)
    {
        $arrParams['page'] = 1;     //default page is 1
    }
    $arrParams['pagesize'] = isset($arrParams['pagesize'])?$arrParams['pagesize']: 20;
    $intPages = ceil($arrParams['count']/$arrParams['pagesize']);   //总页数
	if( ! isset($arrParams['page_limit'])) {
		$arrParams['page_limit'] = 0;
	}
    if(($intPageLimit = intval($arrParams['page_limit'])) > 0)
    {
        $intPages = $intPages > $intPageLimit ? $intPageLimit : $intPages;
    }

    if($arrParams['list']>0)
    {
        $intPageStart = $arrParams['page'] > $arrParams['list'] ? $arrParams['page'] - $arrParams['list'] : 1;
        $intPageEnd   = $arrParams['page'] + $arrParams['list'] > $intPages ? $intPages : $arrParams['page'] + $arrParams['list'];
    }
    else
    {
        $intPageStart = 1;
        $intPageEnd = $intPages;
    }

    preg_match('/page=(\d+)/', $arrParams['pagelink'], $arrPage);
    if (!empty($arrPage))
    {
        $arrParams['pagelink'] = str_replace(array('&' . $arrPage[0], $arrPage[0]), '', $arrParams['pagelink']);
    }

    $strPager = '';
    if ($arrParams['page'] > 1)
    {
//        $strPager.= '<a class="next-page" style="margin-right:0" href="' . _sf_pager2_get_url($arrParams, 1) . '">首页</a>';
//        $strPager.= '<a class="next-page" style="margin-right:0" href="' . _sf_pager2_get_url($arrParams, $arrParams['page']-1) . '">上一页</a>';
        $strPager.= '<li><a aria-label="Previous" href="' . _sf_pager2_get_url($arrParams, $arrParams['page']-1). '"><span aria-hidden="true">&laquo;</span></a></li>';
    }
    for ($i = $intPageStart; $i <= $intPageEnd; $i++)
    {
        if ($arrParams['page'] == $i)
        {
            $strPager .= '<li class="active"><a href="javascript::void();">' . $i . '</a></li>';
        }
        else
        {
            //seo需求，在网点的分页出增加rel="nofollow"
            $nofollow = '';
            if ($arrParams['nofollow']) {
                $nofollow = 'rel="nofollow"';
            }
            $strPager .= '<li><a href="' . _sf_pager2_get_url($arrParams, $i). '" ' . $nofollow .'>' . $i . '</a></li>';
        }
    }
    if ($arrParams['page'] < $intPages)
    {
        
        $strPager.= '<li><a aria-label="Next" href="' . _sf_pager2_get_url($arrParams, $intPages). '"><span aria-hidden="true">&raquo;</span></a></li>';
    }

    return $strPager;
}

function _sf_pager2_get_url($arrParams, $page)
{
    if( isset( $arrParams['nosprintf']) && $arrParams['nosprintf']){
        return $arrParams['pagelink'].$page;
    }else if (strpos($arrParams['pagelink'], '{page}')!==false){
        return str_replace('{page}', $page, $arrParams['pagelink']);
    }else{
        return sprintf($arrParams['pagelink'], $page);
    }
}
?>
