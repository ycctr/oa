<?php
class CalculatorConfig{

	//利率修改时间
	static $updateDate = "2015年10月24日";

	//搜狗内嵌计算器、以及移动版计算器下面的说明文字
	static public function getLilvTip(){
		return '采用央行'.self::$updateDate.'最新基准利率';
	}

	//贷款类计算器利率说明文案
	static public function getDkLilvTip(){
		return self::$updateDate.'最新商贷利率4.90%，公积金利率3.25%';
	}

	//存款计算器利率说明文案
	static public function getCkLilvTip(){
		return self::$updateDate.'央行最新存款基准利率，一年定期存款 1.50%，活期存款 0.35%';
	}

	// PC端贷款计算器利率文章链接
	static public function getPCDkLilvUrl(){
		return "https://www.rong360.com/gl/2015/10/23/79543.html";
	}

	//Wap端贷款计算器利率文章链接
	static public function getWapDkLilvUrl(){
		return "m.rong360.com/article/view/79543.html";
	}

	// PC端存款计算器利率文章链接
	static public function getPCCkLilvUrl(){
		return "https://www.rong360.com/gl/2015/10/23/79552.html";
	}

	//Wap端存款计算器利率文章链接
	static public function getWapCkLilvUrl(){
		return "m.rong360.com/article/view/79552.html";
	}

    //商贷基准利率
    static public function getBaseInterestRate()
    {
        return array(
            array('term'=> array(0,12),'rate' => 4.35, 'text' => '1年以内(含1年)'),
            array('term'=> array(12,60),'rate' => 4.75, 'text' => '1年到5年(含5年)'),
            array('term'=> array(60,1000),'rate' => 4.90, 'text' => '5年以上'),
        );
	}

	//公积金基准利率
    static public function getGjjBaseInterestRate()
    {
        return array(
            array('term'=> array(0,60),'rate' => 2.75, 'text' => '5年以内(含5年)'),
            array('term'=> array(60,1000),'rate' => 3.25, 'text' => '5年以上')
        );
    }

	//存款利率
    static public function getCunkuanBaseInterestRate()
    {
        return array(
            array('term'=> 0, 'rate' => 0.35, 'text' => '活期'),
            array('term'=> 3, 'rate' => 1.10, 'text' => '3个月定期'),
            array('term'=> 6, 'rate' => 1.30, 'text' => '6个月定期'),
            array('term'=> 12, 'rate' => 1.50, 'text' => '1年定期'),
            array('term'=> 24, 'rate' => 2.10, 'text' => '2年定期'),
            array('term'=> 36, 'rate' => 2.75, 'text' => '3年定期'),
        );
    }

	//零存整取利率
    static public function getLCZQBaseInterestRate()
    {
        return array(
            array('term'=> 12, 'rate' => 1.35, 'text' => '1年'),
            array('term'=> 36, 'rate' => 1.55, 'text' => '3年'),
        );
    }

    //北京落户积分计算器问题列表：
    static public function getResidenceQuestion(){
        return array(
            "1"=>array(
                "id"=>1,
                "page_num"=>"page_1",
                "name" => "持有北京居住证",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"",
                        "to" =>"page_2",
                    ),
                    "0" =>array(
                        "des"=>"",
                        "to" =>"page_error",
                    ),

                ),
            ),
            "2"=>array(
                "id"=>2,
                "page_num"=>"page_1",
                "name" => "年龄不到45岁",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"",
                        "to" =>"page_2",
                    ),
                    "0" =>array(
                        "des"=>"",
                        "to" =>"page_error",
                    ),
                ),
            ),
            "3"=>array(
                "id"=>3,
                "page_num"=>"page_1",
                "name" => "在京连续缴纳社保满7年",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"",
                        "to" =>"page_2",
                    ),
                    "0" =>array(
                        "des"=>"",
                        "to" =>"page_error",
                    ),
                ),
            ),
            "4"=>array(
                "id"=>4,
                "page_num"=>"page_1",
                "name" => "符合本市计划生育政策",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"",
                        "to" =>"page_2",
                    ),
                    "0" =>array(
                        "des"=>"",
                        "to" =>"page_error",
                    ),
                ),
            ),
            "5"=>array(
                "id"=>5,
                "page_num"=>"page_1",
                "name" => "无违法犯罪记录",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"",
                        "to" =>"page_2",
                    ),
                    "0" =>array(
                        "des"=>"",
                        "to" =>"page_error",
                    ),
                ),
            ),
            "6"=>array(
                "id"=>6,
                "page_num"=>"page_2",
                "name" => "在京缴纳社保年限",
                "type" => "between",
                "answer_unit"=>"年",
                "answer" => array(
                    "7-27" => "page_3",
                ),
            ),

            "7"=>array(
                "id"=>7,
                "page_num"=>"page_3",
                "name" => "您的居住情况",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"在京自有住房",
                        "to" =>"page_4",
                    ),
                    "2" =>array(
                        "des"=>"在京租房",
                        "to" =>"page_4",
                    ),
                    "3" =>array(
                        "des"=>"在公司宿舍",
                        "to" =>"page_4",
                    ),
                    "4" =>array(
                        "des"=>"其他",
                        "to" =>"page_5",
                    ),
                ),
            ),

            "8"=>array(
                "id"=>8,
                "page_num"=>"page_4",
                "name" => "居住年限",
                "type" => "between",
                "answer_unit"=>"年",
                "answer" => array(
                    "1-27" => array(
                        "des"=>"",
                        "to" =>"page_5",
                    ),
                ),
            ),
            "9"=>array(
                "id"=>9,
                "page_num"=>"page_5",
                "name" => "最高学历",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"大学专科(含职高)",
                        "to" =>"page_7",
                    ),
                    "2" => array(
                        "des"=>"大学本科",
                        "to" =>"page_7",
                    ),
                    "3" => array(
                        "des"=>"硕士",
                        "to" =>"page_6",
                    ),
                    "4" => array(
                        "des"=>"博士",
                        "to" =>"page_6",
                    ),
                    "5" => array(
                        "des"=>"大专以下学历",
                        "to" =>"page_6",
                    ),
                ),
            ),
            "10"=>array(
                "id"=>10,
                "page_num"=>"page_6",
                "name" => "您的学位数量",
                "type" => "between",
                "answer_unit"=>"个",
                "answer" => array(
                    "1-50" => array(
                        "des"=>"",
                        "to" =>"page_7",
                    ),
                ),
            ),
            "11"=>array(
                "id"=>11,
                "page_num"=>"page_7",
                "name" => "是否要搬迁",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"居住地搬进城六区",
                        "to" =>"page_8",
                    ),
                    "2" => array(
                        "des"=>"居住地和工作地都搬进城六区",
                        "to" =>"page_8",
                    ),
                    "3" => array(
                        "des"=>"居住地搬出城六区",
                        "to" =>"page_8",
                    ),
                    "4" => array(
                        "des"=>"居住地和工作地都搬出城六区",
                        "to" =>"page_8",
                    ),
                    "5" => array(
                        "des"=>"不搬迁",
                        "to" =>"page_9",
                    ),

                ),
            ),
            "12"=>array(
                "id"=>12,
                "page_num"=>"page_8",
                "name" => "若已搬迁，已搬迁满几年",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"一年",
                        "to" =>"page_9",
                    ),
                    "2" => array(
                        "des"=>"两年",
                        "to" =>"page_9",
                    ),
                    "3" => array(
                        "des"=>"三年及以上",
                        "to" =>"page_9",
                    ),

                ),
            ),

            "13"=>array(
                "id"=>13,
                "page_num"=>"page_9",
                "name" => "您在以下哪类企业工作",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"在区域性专业市场、一般制造业、《北京是工业污染行业、生产工艺调整退出及设备淘汰目录》范围企业",
                        "to" =>"page_10",
                    ),
                    "2" => array(
                        "des"=>"科技企业孵化器及众创空间中的创业企业",
                        "to" =>"page_10",
                    ),
                    "3" => array(
                        "des"=>"科技企业孵化器及众创空间、技术专业服务机构、相关专业科技服务机构",
                        "to" =>"page_10",
                    ),
                    "4" => array(
                        "des"=>"国家高新技术企业、中关村高新技术企业",
                        "to" =>"page_10",
                    ),
                    "5" => array(
                        "des"=>"其他",
                        "to" =>"page_11",
                    ),

                ),
            ),
            "14"=>array(
                "id"=>14,
                "page_num"=>"page_10",
                "name" => "您在该企业的工作年限",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"一年",
                        "to" =>"page_11",
                    ),
                    "2" => array(
                        "des"=>"两年",
                        "to" =>"page_11",
                    ),
                    "3" => array(
                        "des"=>"三年及以上",
                        "to" =>"page_11",
                    ),
                ),
            ),

            "15"=>array(
                "id"=>15,
                "page_num"=>"page_11",
                "name" => "您在科技、文化领域所获奖项",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"获得过国家级奖项",
                        "to" =>"page_12",
                    ),
                    "2" => array(
                        "des"=>"获得过北京市奖项",
                        "to" =>"page_12",
                    ),
                    "3" => array(
                        "des"=>"没有",
                        "to" =>"page_12",
                    ),
                ),
            ),
            "16"=>array(
                "id"=>16,
                "page_num"=>"page_12",
                "name" => "您是否获得过以下职称",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"中级专业技术职务",
                        "to" =>"page_13",
                    ),
                    "2" => array(
                        "des"=>"高级专业技术职务",
                        "to" =>"page_13",
                    ),
                    "3" => array(
                        "des"=>"没有",
                        "to" =>"page_13",
                    ),
                ),
            ),
            "17"=>array(
                "id"=>17,
                "page_num"=>"page_13",
                "name" => "您的纳税情况",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"上班族，近3年连续纳税，且平均每年纳税额在10万元及以上",
                        "to" =>"page_14",
                    ),
                    "2" => array(
                        "des"=>"个体户，近3年连续纳税，且平均每年纳税额在15万元及以上",
                        "to" =>"page_14",
                    ),
                    "3" => array(
                        "des"=>"企业主，近3年连续纳税，且平均每年纳税15万元及以上",
                        "to" =>"page_14",
                    ),
                    "4" => array(
                        "des"=>"其他",
                        "to" =>"page_14",
                    ),
                ),
            ),

            "18"=>array(
                "id"=>18,
                "page_num"=>"page_14",
                "name" => "自本办法实施日起，是否有违法行为",
                "type" => "equal",
                "answer" => array(
                    "1" => array(
                        "des"=>"有",
                        "to" =>"page_15",
                    ),
                    "0" => array(
                        "des"=>"没有",
                        "to" =>"page_16",
                    ),

                ),
            ),
            "19"=>array(
                "id"=>19,
                "page_num"=>"page_15",
                "name" => "有偷税漏税违法记录数量",
                "type" => "between",
                "answer_unit"=>"个",
                "answer" => array(
                    "0-50" => array(
                        "des"=>"",
                        "to" =>"page_16",
                    ),
                ),
            ),
            "20"=>array(
                "id"=>20,
                "page_num"=>"page_15",
                "name" => "由行政处罚信息、不良司法信息记录数量",
                "type" => "between",
                "answer_unit"=>"个",
                "answer" => array(
                    "0-50" => array(
                        "des"=>"",
                        "to" =>"page_16",
                    ),
                ),
            ),
            "21"=>array(
                "id"=>21,
                "page_num"=>"page_15",
                "name" => "在本市因违法被行政拘留",
                "type" => "equal",
                "answer" => array(
                    "0" => array(
                        "des"=>"",
                        "to" =>"page_16",
                    ),
                    "1" => array(
                        "des"=>"",
                        "to" =>"page_16",
                    ),
                ),
            ),
        );
    }
}
 
