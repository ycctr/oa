<?php
/* {{{ 全站相关路径 */
if (!defined('WWW_PATH')) {
    // 全站相关常量应在 /home/rong/www/config/define.php 中定义
    // 进入此处说明入口文件未包含 /home/rong/www/config/define.php
    $files = get_included_files();
    trigger_error('检测到全站相关路径未定义：'.$files[0], E_USER_WARNING);
    include __DIR__ . '/global.define.php';
}
/* }}} 全站相关路径 */

//rong360 系统常量:
define('APPLY_TYPE_JINGYING',   1); //经营
define('APPLY_TYPE_XIAOFEI',    2); //消费
define('APPLY_TYPE_CHEDAI',     3); //车贷
define('APPLY_TYPE_FANGDAI',    4); //房贷
define('APPLY_TYPE_OTHER',      5); //其他
define('APPLY_TYPE_GUWEN',      6); //贷款顾问
define('APPLY_TYPE_NORMAL',     9); //不限(消费和经营合并)

define('PRODUCT_JINGYING',      1);//经营
define('PRODUCT_DAEFENQI',      2);//大额分期
define('PRODUCT_CHEDAI',        3);//车贷
define('PRODUCT_QIYE',          4);//企业
define('PRODUCT_FANGDAI',       5);//房贷
define('PRODUCT_XIAOFEI',       6);//消费
define('PRODUCT_PRIVATE_LOAN',  100);//普通贷款服务
define('PRODUCT_PRIVATE_CAR',   101);//汽车贷款服务
define('PRODUCT_PRIVATE_HOUSE', 102);//房产贷款服务

define('SERVICE_TYPE_LOAN', 1);//出借服务
define('SERVICE_TYPE_AGENT',2);//中介服务
define('SERVICE_TYPE_GUA',  3);//担保服务

define('LOAN_TERM_TYPE_RANGE',  1);//贷款期限为范围
define('LOAN_TERM_TYPE_FIXED',  2);//贷款期限为固定的一些数值
define('LOAN_QUOTA_TYPE_RANGE', 1);//贷款额度为范围
define('LOAN_QUOTA_TYPE_FIXED', 2);//贷款额度为固定的一些数值
//frank 14-01-13 贷款搜索排序
define('RANK_BY_DEFAULT',   0);//综合排序
define('RANK_BY_INTEREST',  1);//按利率
define('RANK_BY_LOANTIME',  2);//放款时间
define('RANK_BY_MONTHPAY',  4);//月供

/** 产品线 */
define('PRODUCT_LINE_UNDEFINE',  0); /** 未定义 */
define('PRODUCT_LINE_RONG360',  10); /** 主站 */
define('PRODUCT_LINE_BD',       20); /** bd system */
define('PRODUCT_LINE_JINQIAO',  30); /** jinqiao system */
define('PRODUCT_LINE_MIS',      40); /** mis system*/
define('PRODUCT_LINE_DPF',      50); /** dpf */
define('PRODUCT_LINE_CREDIT',   60); /** 信用卡 */
define('PRODUCT_LINE_CRM',      70); /** crm */
define('PRODUCT_LINE_BATCH',    80); /** 批处理 */
define('PRODUCT_LINE_RYJ_APP',  90); /** 融易记 app */
define('PRODUCT_LINE_APP_STATISTICS',  100); /** app 统计SDK 的服务端 */
define('PRODUCT_LINE_GENDAN',  110); /** 跟单系统 */
define('PRODUCT_LINE_GENDAN_GANJI_PUSH_USER',  120); /** 跟单系统手机推送客服手推用户*/
define('PRODUCT_LINE_LICAI',  130); /** 理财*/
define('PRODUCT_LINE_SEM',       140); /** SEM系统 add by caoxiaolin at 2016.7.28*/
define('PRODUCT_LINE_RISK',      150); /** 风控系统 */
define('PRODUCT_LINE_KEFU',      160); /** 客服系统 */
define('PRODUCT_LINE_MBD_APP',   170); /** MBD app */
define('PRODUCT_LINE_HELP',      180); /** Help系统 */
define('PRODUCT_LINE_IS',      	200); /** 风控IS系统 */
define('PRODUCT_LINE_OPENAPI',      210); /** OPENAPI */
define('PRODUCT_LINE_TOOLS',     290); /** tools系统 */
define('PRODUCT_LINE_PAY2',      300); /** 新浪支付的对接系统 */
define('PRODUCT_LINE_PAY2SIGN',  301); /** 支付签名系统 */
define('PRODUCT_LINE_ICREDIT_WEB',310);//极速贷前端
define('PRODUCT_LINE_PASSPORT', 320);  /** Passport  */
define('PRODUCT_LINE_EC', 350);  /** 信用卡账单抓取  */
define('PRODUCT_LINE_RC', 360);	/** 风控模型服务 */
define('PRODUCT_LINE_WD',370); /** WD服务 */
define('PRODUCT_LINE_ICREDITB',380); /** icreditb服务 */
define('PRODUCT_LINE_CRAWLER',385); /** crawler服务(java) */
define('PRODUCT_LINE_FASTLEND', 390);/** 极速贷2.0 */

define('PRODUCT_LINE_BD_AUTOLOGIN',      190); /** BD免登陆 */
define('PRODUCT_LINE_MONEY',      302); /** BD免登陆 */
define('PRODUCT_LINE_PAY',      303); /** pay */

define('PRODUCT_LINE_PUSHSERVICE',      490); /** 对外部消息推送服务 */
define('PRODUCT_LINE_MESSAGE', 500); /**消息中心 */
define('PRODUCT_LINE_TIANJI', 510); /**天机 */
define('PRODUCT_LINE_TIANJI_REPORT', 520); /**天机报告 */

define('PRODUCT_LINE_DM', 530); /**数据监控系统*/

define('PRODUCT_LINE_TJY', 540); /**淘金云系统*/

define('PRODUCT_LINE_JIFEI', 550); /**计费系统*/
define('PRODUCT_LINE_CUISHOU', 560); /**催收系统*/

//担保方式 
define('GUARANTEE_DIYA', 1); //房屋抵押
define('GUARANTEE_XINYONG', 2); //信用贷
define('GUARANTEE_DANBAO', 3); //担保贷
define('GUARANTEE_CAR', 4); //车辆抵押
define('GUARANTEE_OTHER', 5); //需特殊抵押
define('GUARANTEE_XINYONGKA', 6); //信用卡

//直通车类型
define('EXPRESS_TYPE_NORMAL', 0);  //默认类型
define('EXPRESS_TYPE_DIYA', 1);  //抵押贷

//检索结果页大小
define('SEARCH_PAGE_SIZE', 20);
//车贷结果页大小
define('CHEDAI_PAGE_SIZE',10);

//情境定义。不同情境下，搜索、推荐的策略有一定的区别。
define('SCENARIO_SEARCH', 1); //搜索页
define('SCENARIO_DETAIL_PAGE', 2); //详情页
define('SCENARIO_QASK_FAIL', 3); //标准产品申请快问失败
define('SCENARIO_ALA_SEARCH', 4); //aladdin搜索页
define('SCENARIO_OPEN_PRODUCT_QASK_FAIL', 5); //个人产品申请快问失败
define('SCENARIO_BANKER_QASK_FAIL', 6); //信贷顾问咨询快问失败
define('SCENARIO_PRODUCT_ZHITONGCHE', 10); //直通车精准推荐
define('SCENARIO_AFTER_SUCCESS_APPLY', 11); //申请标准产品、个人产品或者信贷员顾问，成功后的精准推荐
define('SCENARIO_QASK_FAIL_NORMAL', 12); //申请标准产品、个人产品或者信贷员顾问，快问失败后的精准推荐


//搜索类型，目前和竞价相对应。
define('SEARCH_STD_PRODUCT', 1); //开放产品的搜索
define('SEARCH_OPEN_PRODUCT', 2); //开放产品的搜索
define('SEARCH_BANKER', 3); //信贷顾问的搜索
define('SEARCH_PRODUCT_AGENT', 4); //抵押贷产品推荐中介的搜索
define('SEARCH_FANGDAI_AGENT', 5); //房贷产品推荐中介的搜索
define('SEARCH_SPECIAL_AGENT', 6); //特殊产品强绑定中介的搜索

define('ZHIYE_TYPE_QIYEZHU',        1); //企业主
define('ZHIYE_TYPE_GETI',           2); //个体工商户
define('ZHIYE_TYPE_ZAIZHIYUANGONG', 4); //工薪族
define('ZHIYE_TYPE_XUESHENG', 6); //学生
define('ZHIYE_TYPE_WUYE',           10); //无固定职业

define('DIYAWU_TYPE_ZHUZHAI',     1); //住宅
define('DIYAWU_TYPE_SHANGPU',     2); //商铺
define('DIYAWU_TYPE_BANGONGLOU',  3); //办公楼
define('DIYAWU_TYPE_CHANGFANG',   4); //厂房
define('DIYAWU_TYPE_QITA',        5); //其他资产
define('DIYAWU_TYPE_CAR',         6); // 车辆抵押
define('DIYAWU_TYPE_NO',          0); //无抵押物

//spam数据的类型。
define('ACTION_ORDER',          2001); //订单申请
define('ACTION_EVALUATE',       2002); //专业评估
define('ACTION_BD_LOGIN',       2003); //信贷员登录
define('ACTION_INVALID_ORDER',  2004); //信贷员对无效订单的反馈

//spam相关频率配置
define('ORDER_FREQ_DAY',        4); //一个用户该自然天申请上限
define('ORDER_FREQ_WEEK',       8); //一个用户该自然周申请上限,暂时不用
define('ORDER_FREQ_MONTH',      7); //一个用户该自然月申请上限
define('ORDER_IP_5MIN',         5); //一个ip5分钟申请上限

define('CAR_PURPOSE_JIAYONG',   2); //家用
define('CAR_PURPOSE_YUNYING',   3); //运营

//付款方式
define('PAYMENT_ALIPAY',        1); //线下支付宝
define('PAYMENT_BANK',          2); //线下银行转账
define('PAYMENT_ACCOUNTANT',    3); //财务代充值
define('PAYMENT_ONLINE',        4); //在线支付充值
define('PAYMENT_ENTRUST',       5); // 委托合作
define('PAYMENT_SYSTEM',        6); //系统自动充值
define('PAYMENT_BZJ_ALIPAY',    7); //保证金线下支付宝
define('PAYMENT_BZJ_BANK',      8); //保证金线下银行转账

//财务系统事务类型
define('TRAN_ORDER_BAOYUE',3);      //包月

define('TRAN_RYJ_BUY_SCORE',    5); //购买融易记积分
define('TRAN_SPEC_FLY_ORDER',    8); //特殊飞单
define('TRAN_OFFLINE_DIANXIAO_T2',    9); //线下电销T2推单
//10,11,16,17,19
define('TRAN_ORDER_LIZI',       10); //成交按例子扣费(含竞价)
define('TRAN_ORDER_XIAOGUO',    11); //按照效果扣费
define('TRAN_ORDER_FREE',       12); //成交免费
define('TRAN_ORDER_SPECIAL',    13); //特殊付费
define('TRAN_ORDER_TIAOZHENG',  14); //调整付费
define('TRAN_SCORE_EXCHANGE',   15); //现金或代金券兑换积分
define('TRAN_DEDUCTE_COUPON',   16); //扣除代金券
define('TRAN_DISNEY_CHARGE',    17); //迪斯尼领取客户扣款
define('TRAN_SPEC_COUPON_EXPIRE',   18); //特殊代金券过期作废

define('TRAN_HIGH_DISNEY_CHARGE',    19); //高端会所竞价


define('TRAN_RECHARGE',         20); //信贷员充值
//20一下的是消费，20以上的是充值 
define('TRAN_SYS_RECHARGE',     21); //销售代充值
define('TRAN_ONLINE_RECHARGE',  22); //信贷员线上支付充值
define('TRAN_INTEGRAL_REDEEM',  23); //信贷员积分兑换

define('TRAN_ORDER_REFUND',     40); //无效订单退款
define('TRAN_BANKER_REFUND',    41); //用户余额退款
define('TRAN_ORDER_REFUND_TIAOZHENG',42);//订单调账的退款
define('TRAN_ORDER_REFUND_SPECIAL_BUY',     43); //特殊渠道扣费退款
define('TRAN_ENTRUST_RETURN',   50); // 委托合作返点

define('TRAN_GET_SPEC_COUPON',   60); //获得特殊代金券
define('TRAN_GUAZHANG',70); //信贷员挂账
define('TRAN_JIEGUA',71); //解除挂账



//收费方式
define('CHARGE_METHOD_XIAOGUO', 1); //按照效果付费
define('CHARGE_METHOD_LIZI',    2); //按照例子付费
define('CHARGE_METHOD_FREE',    3); //免费使用
define('CHARGE_METHOD_SPECIAL', 4); //特殊付费
define('CHARGE_METHOD_ZHONGJIE',    100); //中介信贷经理的单子

//竞价类型(在这里增加新定义时，别忘了同时更新CRongConfig中的BiddingTypeNameMap)
define('BIDDING_TYPE_STD_PRODUCT',      1); //标准产品推广（目前线上的竞价）
define('BIDDING_TYPE_OPEN_PRODUCT',     2); //开放个人产品推广（其他贷款，推荐贷款下方）
define('BIDDING_TYPE_AGENT',            3); //中介担保推广（抵押贷）
define('BIDDING_TYPE_BANKER',           4); //贷款顾问推广（贷款顾问频道）
define('BIDDING_TYPE_FANGDAI',          5); //房贷中介的竞价
define('BIDDING_TYPE_SPECIAL_AGENT',    6); //绑定指定中介推广
define('BIDDING_TYPE_BANK_PUSH_ORDER',  7); //安家银行（推送给指定银行订单竞价位-安家世行项目）
define('BIDDING_TYPE_CONSULTANT_ORDER', 8); //安家咨询（C端咨询窗口产生订单竞价位-安家世行项目）
define('BIDDING_TYPE_CALL_OUT',         9); //上海外呼（外呼中心产生订单的扣费模式）
define('BIDDING_TYPE_AJSH_PUSH_ORDER',  10); //安家手推（手推订单-安家世行二期合作）
define('BIDDING_TYPE_RISK_FLY_ORDER',   11); //风控手推订单-风控系统飞到BD的订单

define('BIDDING_TYPE_RISK_FLY_USERMARKET',   12); //风控系统飞到BD高端会所的订单
define('BIDDING_TYPE_DIANXIAO_FLY_USERMARKET',   13); //电销系统飞到BD高端会所的订单
define('BIDDING_TYPE_DIANXIAO_T2',14);//电销系统T2策略飞单
define('BIDDING_TYPE_DIANXIAO_SP_FLY_8YUAN',16);//电销系统8元飞单

define('BIDDING_TYPE_CARD',             100); //信用卡标准产品推广

//短信分类
define('SMS_CATEGORY_10',	10);		//验证码,有时效性
define('SMS_CATEGORY_20',	20);		//通知1类,仅仅白天发送,比如信贷员的新客户通知
define('SMS_CATEGORY_30',	30);		//通知2类,立即发送,比如用户订单通知
define('SMS_CATEGORY_40',	40);		//关怀营销类,暂时立即发送,一般会有人为控制

//短信相关配置
define('APPLICATION_CUSTOMER',	1);		//普通用户短信-申请类型(异步发送)
define('APPLICATION_BANKER',	2);		//业务员短信-申请类型(异步发送)
define('REGISTER',              3);		//用户注册类型(立即发送)
define('SEND_BY_USER',          4);		//用户主动发送(异步发送)
define('APPLY_VERIFY',          5);		//用户申请订单(立即发送)
define('DIAL_AFTER_SEND',		6);		//用户或者信贷员拨打电话发送短信(立即发送)
define('RECHARGE',              7);     //充值确认或者拒绝(立即发送)
define('MSG_MONITOR',           8);     //报警监控
define('CALL_FAILED',			9);		//用户拔打信贷员失败后，系统自动给用户和信贷员分别发送短信
define('SUCCESS_LOAN',			10); 	//用户成功放款后，系统自动发送短信
define('NOTIFY_BANKER_MONEY',	12);	//信贷员余额不足提醒 
define('NOTIFY_BANKER_RANK',	11);	//信贷员排名下降提醒
define('BD_FIND_PASSWORD',      13);    //bd信贷员找回密码
define('BANKER_VERIFY',         14);    //跟进BC将客户从潜在客户转为注册客户后发送初始密码等信息给信贷员
define('CALL_FAILED_TO_USER',	15);	//用户拔打信贷员失败后，系统自动给用户发送短信
define('CALL_FAILED_TO_BANKER',	16);	//用户拔打信贷员失败后，系统自动给信贷员发送短信
define('TO_FAIL_USER',          17);    //鼠洞用户被信贷员选中后，短信通知用户
define('BANKER_SERVICE_SURVEY', 18);    //发送给下单用户的信贷员服务调查短信，需要回复
define('MIS_SENT_MAIL_4_BANKER',19);    // 管理员发送站内信的同时发短信提醒信贷员
define('SEND_QA_ANSWER_TO_USER',20);	//MIS中客服审核信贷员针对问题的回答，如果合格，则将此答案发给用户
define('JINQIAO_NOTICE',        21);    // 金桥系统中短信通知
define('JINQIAO_FIND_PASSWORD', 22);    // 金桥找回密码
define('JINQIAO_SEND_PASSWORD', 23);    // 金桥中为r360信贷员生成密码
define('BANKER_OFFLINE',        24);    // banker信贷员下线发送短信
define('SEND_QA_TO_BANKER',     25);    // BD后台审核人员发送'用户提问'给信贷员时,将问题短信发给信贷员
define('BANKER_SET_ONLINE',     26);    // banker信贷员上线发送短信
define('USER_FEEDBACK_TO_BANKER', 27);  //2013.8.1 add by caoxiaolin，用户短信对信贷员服务质量进行反馈后，发短信给信贷员
define('RECHARGE_NOTICE',       28);     //信贷员充值成功通知BC
define('BD_REGISTER_CAPTCHA',   29);    // BD信贷经理注册时，发送的手机验证码
define('BANKER_SERVICE_SURVEY_LOAN', 30);    //发送给贷款用户的调查短信
define('BANKER_SERVICE_SURVEY_CREDIT', 31);    //发送给信用卡用户的调查短信
define('CITY_OPEN_NOTIFY', 32);        //城市开通后给信贷员发送通知短信
define('NOTICE_BANKER_BC', 33); //信贷员发自BD平台的认证请求被处理后，发短信通知该信贷员的跟进BC
define('CHEDAI_FOR_4S', 34);    //车贷4s店短信
define('CHEDAI_FOR_USER', 35);  //车贷用户短信
define('NOTICE_BANK_PUSH_ORDER', 34); //系统生成订单推给安家世行时，发送提醒短信
define('BANKER_GUIDE_NO_TASK_FINISHED',     35);    //新手任务-一周没有完成任何一项任务
define('BANKER_GUIDE_ALL_TASK_FINISHED',    36);    //新手任务-完成所有任务
define('BANKER_GUIDE_AUTH_DONE',            37);    //新手任务-完成认证任务
define('CREDIT_APPLICATION_BEEN_CLAIMED',   38);    //2014.01.06 add by shupan,当信贷员领取订单后会给用户发一条短信提示用户已经被服务（信用卡）
define('BANKER_AUTH_AUDIT_BD', 39); //信贷员在BD提交的认证申请被处理后，发送给信贷员的短信通知
define('BANKER_AUTH_AUDIT_CRM', 40); //从CRM平台发起的信贷员认证请求，如果通过审核，则发给信贷员短信通知
define('SMS_TYPE_RISK',         41);    //2014.03.11 add by caoxiaolin，风控系统发送短信
define('RYJ_COMMON_CAPTCHA',    42); // 融易记通用验证码注册、登录、上传名片
define('RYJ_RECOMMEND_SUCCESS', 43); // 融易记推荐成功
define('REMIND_BANKER_FEEDBACK', 44); // 订单反馈提醒
define('LEND_FORGET_PASSWORD',   45);    //LEND找回密码
define('USER_ORDER_RATING_LINK',   46);    //发送给用户订单评价页面链接的短信
define('CONTROL_BANKER_BC',47);//针对受管控信贷员的跟进BC的管控短信提醒
define('IS_FORGET_PASSWORD',   48);    //IS系统找回密码
define('RISK_FORGET_PASSWORD', 49);    //risk系统找回密码
define('BAN-BANKER',50);//信贷员封禁短信提醒
define('FREE-BANKER',51);//信贷员解禁短信提醒
define('KEFU_AUDIT_SUCCESS_ORDER', 52);//客服审核成功订单成功放款时给信贷员发短信
define('KEFU_AUDIT_INVALID_ORDER', 53);//客服审核无效订单通过时给信贷员发短信
define('BD_INFOMATION_TO_BANKER', 54);//BD人工发送短信给信贷员 by xiaochang 2014-10-29
define('DEFAULT_MSG_TYPE', 0);    //默认短信类型
define('SMS_TYPE_HELP',         55);    //help系统发送短信
define('ICREDIT_MSG',56);//极速贷短信
define('BANKER_USERMARKET_ORDER',57);//高端会所获得订单通知
define('BANKER_USERMARKET_ORDER_NOTICE',58);//通知BC信贷员高端会所获得订单
define('BANKER_USERMARKET_DROP_ORDER',59);//高端会所获得订单通知
define('KEFU_FEIDAN_LIZI', 60); //客服系统飞单例子通知信贷员通知
define('APPLY_GET_CAPTCHA',62); //风控下单系统下单短信验证码
define('YINGXIAO_DX_RECOMMEND_DOWNLOAD_APP',63); //电销拨通用户电话后推荐大app下载(通过独立营销短信通道发送)
define('FAKE_ORDER_SMS',64);//生成假订单发送的普通用户类型短信
define('REAL_ORDER_NOBANKER_SMS',65);//生成真订单但是不发送信贷员只发送普通用户类型短信
define('SMS_MARKETING',99);//广告类型的短信，定义到最大

//短信异步发送方式控制
define('SEND_NOW',0);				//短信发送模式(1全部立即发送,0按优先级异步发送);
define('SYN_SEND_MSG',1);			//开启异步发送模式(默认1开启会循环发送;0关闭后异步循环发送功能停止);
define('SYN_SEND_MSG_AGAIN',1);		//表示是否启动重发机制(参数说明同上);
define('SYN_MSG_MONITOR',1);		//启动短信监控程序

//短信监控控制
define('MSG_SUCCESS_100',0.95);	//每5min检查一次短信的发送成功率,如果超过5条未能发送;

//短信异步发送时间控制
define('SYN_SEND_MSG_SLEEP',5);		//两次短信发送时间间隔
define('SYN_SEND_MSG_AGAIN_SLEEP',10);	//两次重发停留时间(s)
define('SYN_SEND_MSG_AGAIN_MIN_INTERVAL',300);	//超过多大的间隔(s)未响应就认为是失败;
define('SYN_SEND_MSG_AGAIN_MAX_INTERVAL',3600*24);	//超过多大的间隔(s)未响应就认为是失败;
define('SYN_MSG_MONITOR_SLEEP',60);		//监控停留时间(s)

//第三方服务监控报警;
define('GDPP_MSG_WARNING_NUM',20000);	//国都互联短信报警阈值(短信条数);
define('GDPP_MSG_MONITOR',0);			//监控开关(0关闭,1开启);
define('CSMD_MSG_WARNING_NUM',2000);	//创世漫道短信报警阈值;
define('CSMD_MSG_MONITOR',1);			//监控开关(0关闭,1开启);	
define('JYTX_400_AGENT_WARNING',5000);		//吉亚通信报警阈值;(总账号余额,除分公司之外的资金)
define('JYTX_400_COMPANY_WARNING',1000);		//吉亚通信报警阈值;(核心企业资金)
define('JYTX_400_MONITOR',1);			//监控开关(0关闭,1开启);

//短信状态
define('MSG_OK',                0);		//短信发送成功;
define('MSG_ERROR',             1);		//短信发送失败;
define('MSG_OPERATOR_OK',       10);	//到达运营商;
define('MSG_OPERATOR_ERROR',    11);	//到达运营商失败;
define('MSG_PREPARE',			20);	//记录到数据库中待发送;

//短信网关定义
define('GATEWAY_GDPP',  1);     //国都互联
define('GATEWAY_CSMD',  2);     //创世漫道
define('GATEWAY_RONG',  3);     //Rong360

//网关状态
define('GATEWAY_STATUS_OK',     0); //网关状态正常
define('GATEWAY_STATUS_ERROR',  1); //网关工作异常
define('RONG_STATUS_UNKNOW',  100); //网关状态未知

//400回收
define('SYN_400_BIND',0);				//用户短信采用绑定400的方式进行发送;
define('TR400_RECOVERY',1);				//开启天润400号码的回收;
define('TR400_RECOVERY_RATIO',0.8);		//已使用的号码超过这个比率后开始回收;
define('TR400_RECOVERY_TIME',0);		//超过这个天数未使用的号码,进行回收;

//400 TYPE字段说明
define('C400_TYPE_TRRT',	1);			//天润融通;
define('C400_TYPE_JYTX',	2);			//吉亚通讯;
define('C400_TYPE_MAX_TRRT',	100);		//记录天润当前最大的400号码;
define('C400_TYPE_MAX_JYTX',	200);		//记录吉亚当前最大的400号码;

//400通话记录相关
define('C400_DETAIL_CONTROL',1);				//用户短信采用绑定400的方式进行发送;
define('C400_DETAIL_TRRT',	1000);		//记录天润当前处理的时间;
define('C400_DETAIL_JYTX',	2000);		//记录吉亚当前处理的时间;
define('C400_DETAIL_TRRT_INTERVAL', 300);		//天润融通通话信息时间间隔(s);
define('C400_DETAIL_JYTX_INTERVAL', 300);		//吉亚通讯通话信息时间间隔(s);
define('C400_DETAIL_SLEEP_TIME', 60);		//吉亚通讯通话信息时间间隔(s);

//400录音数据
define('C400_AUDIO_JYTX_INTERVAL', 24 * 60 * 60);   //吉亚通讯通话录音获取间隙(最小单位以天记)
define('C400_AUDIO_INTERVAL', 10);   //400录音下载间隙,不能下载太频繁,会对网络造成影响

//400通话记录状态
define('C400_DETAIL_TYPE_CALL_IN',1);	//呼入
define('C400_DETAIL_TYPE_CALL_OUT',2);	//呼出

define('C400_DETAIL_STATUS_CHENGGONG',				0);			//成功;
define('C400_DETAIL_STATUS_BUCHENGGONG',			-1);		//不成功;
define('C400_DETAIL_STATUS_MANG',					1);			//忙;
define('C400_DETAIL_STATUS_WUYINGDA',				2);			//无应答;
define('C400_DETAIL_STATUS_KEHUTIQIANGUAJI',		3);			//客户提前挂机;
define('C400_DETAIL_STATUS_ZUOXIJUJUE',				9);			//坐席拒绝;
define('C400_DETAIL_STATUS_WAIHUSHIBAI',			10);		//外呼失败;
define('C400_DETAIL_STATUS_WUXIAOFENJIHAO',			201);		//无效分机号;
define('C400_DETAIL_STATUS_WAIHUJUJUEJIETING', 		444);		//外呼拒绝接听;
define('C400_DETAIL_STATUS_HEIMINGDAN',				555);		//黑名单;
define('C400_DETAIL_STATUS_QIANFEI',				666);		//欠费;
define('C400_DETAIL_STATUS_HUHUIWAIXIANSHIBAI',		777);		//回呼外线失败;
define('C400_DETAIL_STATUS_FEIGONGZUOSHIJIANHUJIAO',1000);		//非工作时间呼叫;

//400分机号状态
define('EXT_STATUS_CURRENT',	1);			//当前正在使用;
define('EXT_STATUS_RECOVERY',	2);			//已回收;
define('EXT_STATUS_NOASSIGNED',	3);			//未分配;
define('EXT_STATUS_SPECIAL',	10);		//特殊字段不在循环使用内;

//根据400统计记录控制短信发送
define('SEND_400_MESSAGE_CONTROL',1);		//400发送短信开关
define('SEND_400_MESSAGE_INTERVAL',600);		//处理间隔,时效性

define('CITYID_OF_QUANGUO', 10000);
define('CITYID_OF_Q', 10001);
define('CITYID_OF_UNOPEN', 9999);
define('CITYNAME_OF_QUANGUO', '全国');
define('DOMAIN_OF_QUANGUO', 'q');
define('CITYNAME_OF_UNOPEN', '未开通');

//add by caoxiaolin at 2012.7.27，竞价系统，订单没有扣费的原因，main/mis/bd都有用到，所以放在这个配置里
define('JINGJIA_INVALID_REASON_NOSET',          1); //信贷员没有出价
define('JINGJIA_INVALID_REASON_BANKER',         2); //信贷员手工设置出价为0
define('JINGJIA_INVALID_REASON_BALANCE',        3); //账户余额不足
define('JINGJIA_INVALID_REASON_CONSUME',        4); //消费超出上限
define('JINGJIA_INVALID_REASON_ORDERS',         5); //订单超出上限
define('JINGJIA_INVALID_REASON_SAMEUSER',       6); //30天内同一个用户下单
define('JINGJIA_INVALID_REASON_BANKER_DEL',     7); //信贷员下架
define('JINGJIA_INVALID_REASON_PRODUCT_DEL',    8); //信贷员产品下架
define('JINGJIA_INVALID_REASON_BANKER_MODE_ERR',9); //信贷员产品模式不正确，不是竞价模式
//add by frank, 准入基本类型
define('PERMIT_TYPE_IS',        1);//是否类
define('PERMIT_TYPE_ENUMERATE', 2);//枚举类
define('PERMIT_TYPE_COMPARE',   3);//比较类
define('PERMIT_TYPE_RANGE',     4);//范围类
define('PERMIT_TYPE_TEXT',      5);//输入类
// add by wulinhai, 风控系统政策模板的类型,从loan的common/define.php中迁移过来
define('PERMIT_TYPE_IMAGE',     6);//图片类型
define('PERMIT_TYPE_FILE',      7);//非图片类文件
define('PERMIT_TYPE_DATE',      8);//日期
define('PERMIT_TYPE_NUM',       9);//数字
define('PERMIT_TYPE_HBASE',     10);//抓取的html，存入hbase，add by caoxiaolin at 2015.7.15
define('PERMIT_TYPE_BANK_CARD',       11);//银行卡号
define('PERMIT_TYPE_CREDIT_CARD',       12);//信用卡有效期
define('PERMIT_TYPE_ALL_FILE', 13);//图片、文档、HBASE合并文件
//add by frank, 准入基本类型
define('ELEMENT_TYPE_ENUM',     2);//枚举类
define('ELEMENT_TYPE_COMPARE',  3);//比较类

//add by zouyoufang at 2013.01.10
//spam检查判定的具体结论
define('SPAM_OK', 0); //SPAM策略认定为正常访问。
define('SPAM_UID_SAME_PRODUCT', 1); //同UID同产品不同信贷员,已经拆分为10、20两种情况了
define('SPAM_UID_DAY', 2); //同UID当天申请次数
define('SPAM_UID_MONTH', 3); //同UID当月申请次数
define('SPAM_MOBILE_BLACKLIST', 4); //手机号码在黑名单中。TODO: 后续可以考虑如何和信贷员作弊合并到一块。
define('SPAM_INVALID_INPUT', 5); //输入不符合要求。
define('SPAM_IP_5MIN', 6); //5分钟内订单的申请次数大于阈值。
define('SPAM_UID_SAME_PRODUCT_BANKER', 10); //同UID申请同产品同信贷员
define('SPAM_UID_SAME_PRODUCT_BANK', 20); //同UID申请同产品同机构信贷员，针对绑定中介
define('SPAM_UID_CONFLICT', 8); //用户标识前后不一致，例如当天同rongid但是手机号不一致。 
define('SPAM_BANKER_FRAUD', 9); //信贷员作弊 
define('SPAM_PRODUCT_PROTECT', 11); //被重点保护的产品。 
define('SPAM_INVALID_ORDER', 12); //曾经有被反馈为无效订单。 
define('SPAM_MOBILE_GEO_CONFLICT', 13); //手机号归属地和用户申请的不一致。 
define('SPAM_REPEAT_USER', 14); //根据手机号码、rongid判定是否重复用户。 
define('SPAM_UNTRUST_CHANNEL', 15); //非信任渠道。 
define('SPAM_SPECIAL_TIME_YIDI', 16); //特殊时间段内异地申请 
define('SPAM_HAS_IS_ORDER_14DAY',17); //14天内有申请过IS订单

// spam建议应对措施
define('ANTISPAM_OK', 0); //无。
define('ANTISPAM_DENY', 1); //拒绝申请
define('ANTISPAM_VERIFY_MOBILE', 2); //需要手机验证码
define('ANTISPAM_FAKE_ORDER', 3); //做假订单，麻痹用户

//不同的访问平台。
define('PLATFORM_PC', 'pc'); //pc
define('PLATFORM_MOBILE', 'mobile'); //移动端的触屏版
define('PLATFORM_WAP', 'wap'); //移动端的简版
define('PLATFORM_APP', 'app'); //移动端app
define('PLATFORM_MINIAPP', 'miniapp');//微信小程序


//add by caoxiaolin at 2012.11.26, same with gendan config
DEFINE('ORDER_STATUS_ENTER_DIANXIAO',               0);     //进入电销,未下单
DEFINE('ORDER_STATUS_TIJIAOSHENQING',               10);     //提交申请
DEFINE('ORDER_STATUS_WEILIANXI_1',                  20);     //超过1天未联系
DEFINE('ORDER_STATUS_WEILIANXI_2',                  30);     //超过2天未联系
DEFINE('ORDER_STATUS_KONGHAO',                      34);     //空号
DEFINE('ORDER_STATUS_LIANXIBUSHANG',                37);     //联系不上
DEFINE('ORDER_STATUS_YILIANXI',                     40);     //双方已联系
DEFINE('ORDER_STATUS_SHIYONG',                      50);     //用户试用
DEFINE('ORDER_STATUS_WEIMAO',                       60);     //用户伪冒
DEFINE('ORDER_STATUS_BEIJUJUE',                     70);     //被拒绝
DEFINE('ORDER_STATUS_BEIJUJUE_FEIDAN',              80);     //被拒绝-飞单
DEFINE('ORDER_STATUS_FANGQI',                       90);     //用户放弃
DEFINE('ORDER_STATUS_FANGQI_FEIDAN',                100);    //用户放弃-飞单
DEFINE('ORDER_STATUS_FANGQI_WEILIANXI',             110);    //用户放弃-未联系
DEFINE('ORDER_STATUS_CHUBUMANZU',                   120);    //初步满足
DEFINE('ORDER_STATUS_YONGHUKAOLV',                  130);    //用户考虑
DEFINE('ORDER_STATUS_YONGHUKAOLVDAOQI',             140);    //用户考虑到期
DEFINE('ORDER_STATUS_SHENPIZHONG',                  150);    //审批中
DEFINE('ORDER_STATUS_SHENPIBEIJU',                  155);    //审批被拒
DEFINE('ORDER_STATUS_SHENPITONGGUO',                160);    //审批通过
DEFINE('ORDER_STATUS_CHENGGONGFANGKUAN',            170);    //成功放款
DEFINE('ORDER_STATUS_FANGKUANSHIBAI',        		165);    //放款失败

define('ENTRUST_ORDER_STAT_FOLLOW_UP',              5);      // 委托订单跟进中
define('ENTRUST_ORDER_STAT_NOT_MATCH',              180);    // 完成合作,但资质不符
define('ENTRUST_ORDER_STAT_NO_COOP',                190);    // 完成合作,但不可飞

//add by caoxiaolin at 2012.11.29，权限系统
DEFINE('RIGHT_SYSTEM_MIS',      0); //mis
DEFINE('RIGHT_SYSTEM_GENDAN',   1); //gendan

define('DISTRICT_TYPE_OUTSKIRTS',           2);     //郊区

//add by zouyoufang at 2013.02.18，信贷员的合作方式
DEFINE('BANKER_COOPERATION_PERSONAL', 1);//对信贷员个人
DEFINE('BANKER_COOPERATION_COMPANY',  2);//对公合作  -> 20150106后对应 对公例子合作
DEFINE('BANKER_COOPERATION_ISCOMPANY', 3); //对公IS合作
DEFINE('BANKER_COOPERATION_TAOJINYUN', 4); //淘金云合作 

//app用户订单更新提示
DEFINE('APP_USER_ORDER_STATUS_UPDATE', 'app_order_status_update_user_');

//add by zhangji at 2012.12.11 ,mis bd. banker- status信贷员状态
DEFINE('BANKER_STATUS_DAISHENHE',     1);//待审核
DEFINE('BANKER_STATUS_DAISHANGXIAN',  2);//待上线
DEFINE('BANKER_STATUS_XIANSHANG',     3);//线上
DEFINE('BANKER_STATUS_XIANXIA',       4);//线下
DEFINE('BANKER_STATUS_TEMP_XIAXIAN',  5);//临时下线
/** 信贷经理审核状态 */
define('BANKER_VERIFY_NO_PASS',       5); // 审核不通过
define('BANKER_JUST_REGISTER',       -1); // 刚注册



/*
//add by liuyue 2016-10-24
//信贷员审核 一级原因
define('BANKER_SOURCE_PASS',       1); // 通过
define('BANKER_SOURCE_CERTIFICATE_ERROR',       2); // 证件资料有误
define('BANKER_SOURCE_INFO_ERROR',       3); // 提供虚假资料
define('BANKER_SOURCE_REPEAT_ACCOUNT',       4); // 重复账号
//信贷员审核 通过 二级原因
define('BANKER_SOURCE_CHECK_SUCCESS',       1); // 通过

//信贷员审核 证件资料有误 二级原因
define('BANKER_SOURCE_IDCARD_ERROR',       1); // 身份证资料有误
define('BANKER_SOURCE_WORKCARD_ERROR',       2); // 工牌有误/不符合要求
define('BANKER_SOURCE_BUSINESS_ERROR',       3); // 名片有误/不符合要求
define('BANKER_SOURCE_LABOR_ERROR',       4); // 在职证明/劳动合同不符合要求

//信贷员审核 重复账号 二级原因
define('BANKER_SOURCE_TWO_REPAET',       1); // 重复账号

//信贷员审核 提供虚假资料 二级原因
define('BANKER_SOURCE_SALER_CHECK',       1); // 请信贷员联系商务顾问核实信息
define('BANKER_SOURCE_BANKERINFO_ERROR',       2); // 信贷员信息虚假

//信贷员电话审核座机 一级原因
define('BANKER_MOBILE_CHECK_SUCC',       1); // 通过
define('BANKER_MOBILE_CHECK_ERROR',       2); // 座机有误

//信贷员座机电话审核座机通过 二级原因
define('BANKER_MOBILE_CHECK_OFFICIAL',       1); // 官网\400客服核实
define('BANKER_MOBILE_CHECK_THIRD',       2); // 第三方渠道所得座机核实
define('BANKER_MOBILE_CHECK_SYSTEM',       3); // 系统连号座机核实
define('BANKER_MOBILE_CHECK_UNABLE',       4); // 无法确定座机核实


//信贷员座机电话审核座机有误 二级原因
define('BANKER_MOBILE_CHECK_LEAVE',       1); // 核实查无此人/离职
define('BANKER_MOBILE_CHECK_NO_ANSWER',       2); // 座机拨打无人接听
define('BANKER_MOBILE_CHECK_FIXED',       3); // 座机不是固定座机
define('BANKER_MOBILE_CHECK_NO_AGENTCY',       4); // 座机拨打不是该机构
define('BANKER_MOBILE_CHECK_NO_FIT',       5); // 座机不配合审核

//信贷员手机号审核 一级原因
define('BANKER_PHONE_CHECK_SUCC',       1); // 通过
define('BANKER_PHONE_CHECK_ERROR',       2); // 手机号码有误

//信贷员手机号通过 二级原因
define('BANKER_PHONE_CHECK_SUCCESS',       1); // 通过

//信贷员手机号审核 二级原因
define('BANKER_PHONE_CHECK_NONE',       1); // 手机拨打无人接听
define('BANKER_PHONE_CHECK_NOT_I',       2); // 手机号不是本人接听
define('BANKER_PHONE_CHECK_EMPTY',       3); // 手机号空号、关机
define('BANKER_PHONE_CHECK_NO_FIT',       4); // 信贷员不配合审核
*/
// 新的原因 update by zhaoaofei 2016/12/11
//信贷员资料审核 一级原因
define('BANKER_SOURCE_PASS',       1); // 通过
define('BANKER_SOURCE_UNPASS',     2); //不通过
//信贷员资料审核 通过 二级原因
define('BANKER_SOURCE_CHECK_SUCCESS',       1); // 通过
//信贷员资料审核  不通过二级原因
define('BANKER_SOURCE_UNCLEAR_ERROR',       1); // 资料照片不清楚
define('BANKER_SOURCE_WORKCARD_ERROR',      2); // 工牌/名片不符合要求
define('BANKER_SOURCE_SHIXI_ERROR',         3); // 实习期内不予认证
define('BANKER_SOURCE_IDCARD_ERROR',        4); // 证件资料虚假
define('BANKER_SOURCE_REPAET_REAPT',        5); // 重复账号
//信贷员电话审核座机 一级原因
define('BANKER_MOBILE_CHECK_SUCC',       1); // 通过
define('BANKER_MOBILE_CHECK_ERROR',       2); // 座机不通过

//信贷员座机电话审核座机通过 二级原因
define('BANKER_MOBILE_CHECK_OFFICIAL',     1); // 官网\400客服核实
define('BANKER_MOBILE_CHECK_THIRD',        2); // 第三方渠道所得座机核实
define('BANKER_MOBILE_CHECK_SYSTEM',       3); // 系统连号座机核实
define('BANKER_MOBILE_CHECK_UNABLE',       4); // 无法确定座机核实
define('BANKER_CHECK_TIAOGUO',            99); // 暂时跳过 

//信贷员座机电话审核座机不通过 二级原因
define('BANKER_MOBILE_CHECK_NO_ANSWER',   1); // 座机拨打无人接听
define('BANKER_MOBILE_CHECK_INVALID',     2); //座机空号、停机、欠费、传真
define('BANKER_MOBILE_CHECK_LEAVE',       3); // 核实查无此人/离职
define('BANKER_MOBILE_CHECK_NO_AGENTCY',  4); // 座机拨打不是该机构
define('BANKER_MOBILE_CHECK_NO_FIT',      5); // 座机不配合审核
define('BANKER_MOBILE_CHECK_CITY_CHANGE', 6); // 更换城市
//信贷员手机号审核 一级原因
define('BANKER_PHONE_CHECK_SUCC',       1); // 通过
define('BANKER_PHONE_CHECK_ERROR',       2); // 手机不通过

//信贷员手机号通过 二级原因
define('BANKER_PHONE_CHECK_SUCCESS',       1); // 通过

//信贷员手机号审核 二级原因
define('BANKER_PHONE_CHECK_NONE',       1); // 手机拨打无人接听
define('BANKER_PHONE_CHECK_EMPTY',      2); // 手机号空号、停机、欠费
define('BANKER_PHONE_CHECK_LEAVE',       3); // 核实已离职
define('BANKER_PHONE_CHECK_NO_FIT',       4); // 信贷员不配合审核
define('BANKER_PHONE_CHECK_NOT_I',       5); // 手机号非本人使用
define('BANKER_PHONE_CHECK_CITY_CHANGE', 6); // 更换城市
//信贷员手机号审核 一级原因
define('BANKER_RISK_CHECK_SUCC',       1); // 通过
define('BANKER_RISK_CHECK_ERROR',       2); // 风险认证不通过



//信贷员手机号通过 二级原因
define('BANKER_RISK_CHECK_SUCCESS',       1); // 通过
//信贷员风险认证不通过二级原因
define('BANKER_RISK_SHUXIN_BLACK_LIST',  1);//数信网查实网络黑名单
define('BANKER_RISK_FAHAI_CASING',       2);//法海网查实执行中
define('BANKER_RISK_JIAO_UNMATCH',       3);//手机实名制不一致
define('BANKER_RISK_LIVENESS_ERROR',     4);//活体识别不一致
define('BANKER_RISK_FAHAI_JINJI',		5);//法海经济案件复核,
define('BANKER_RISK_FAHAI_QITA',		6);//法海其他案件复核,
define('BANKER_RISK_JIAO_BC',			7);//手机实名制不一致，BC复核通过
define('BANKER_RISK_PASS_BC',			8);//风险认证复核通过


//信贷员认证审核  最终
define('BANKER_CHECK_NO_OPTION',       1); // 综合判定
define('BANKER_CHECK_SAME_ACCOUNT',       2); // 重复账号
define('BANKER_CHECK_RISK_ACCOUNT',       3); // 风险账号


//add by shuguo:信贷员来源
DEFINE('BANKER_SOURCE_OTHER', 1);   //其它
DEFINE('BANKER_SOURCE_SALERSELF', 2); //销售自己找到
DEFINE('BANKER_SOURCE_BANKERZIZHU', 3); //自主入驻
DEFINE('BANKER_SOURCE_OTHERSALER', 4); //同行推荐
DEFINE('BANKER_SOURCE_COMPANY', 5); //公司资源  

//move from BankerSettle.php by shuguo：潜在客户的跟进状态
define('COOPERATION_STAT_NONE',             0);
define('COOPERATION_STAT_NEW',              1);  //新客户
define('COOPERATION_STAT_REFUSE',           2);  //我司拒绝
define('COOPERATION_STAT_BANKER_REFUSE',    3);  //信贷员拒绝
define('COOPERATION_STAT_NOT_ONLINE',       4);  //暂未联通
define('COOPERATION_STAT_FAKE_OR_STRAYED',  5);  //假冒误入
define('COOPERATION_STAT_COOPERATION',      6);  //联系合作中
define('COOPERATION_STAT_WAIT_ONLINE',      7);  //达成合作意向
define('COOPERATION_STAT_WAIT_OTHER',       8);  //其它

//潜在客户，注册客户共用cooperation_stat
define('BANKERAPPLY_STATUS_CREDIT',       19);  //信用卡业务员          => 信用卡业务员
define('BANKERAPPLY_STATUS_DOWNTIME',       61);  //手机停机          => 手机停机
define('BANKERAPPLY_STATUS_EMPTY',       62);  //手机空号          => 手机空号
define('BANKER_WAIT_RECHARGE_MARGIN',       63);  //等待充值保证金          => 等待充值保证金
define('BANKER_WAIT_REFUND_MARGIN',       64);  //等待退款保证金          => 等待退款保证金

//潜在客户的状态集合（开放平台版本）  => 后面为CRM2.0版本的定义  => CRM3.0之后，这些状态统一从数据库表mission_feedback中取，便于灵活添加
define('BANKERAPPLY_STATUS_NEW',              11);  //新客户                            => 未联系
define('BANKERAPPLY_STATUS_CONNECTING',       12);  //联系合作中                     => 客户考虑
define('BANKERAPPLY_STATUS_FAKE',             13);  //非目标客户                     => 无效客户
define('BANKERAPPLY_STATUS_BANKER_REFUSE',    14);  //信贷员拒绝                     => 客户拒绝
define('BANKERAPPLY_STATUS_WAIT_REG',         15);  //合作达成（等待注册）    => 等待注册
define('BANKERAPPLY_STATUS_REG',              16);  //合作达成（转入已注册） => 潜客转入注册
define('BANKERAPPLY_STATUS_WAIT_OTHER',       18);  //其它                                => 其它



//潜在客户跟进期限
define('FOLLOWTIMELIMIT_BANKERAPPLY_REG', 30); //潜在客户如果在此天数后还未能转为注册客户，则。。。
//客户跟进期限
define('FOLLOWTIMELIMIT_BANKER_TUIGUANG', 30); //客户如果在此天数后还未推广过，则。。。

//客户的业务跟进状态集合 （BC触发）
define('BANKER_FOLLOW_CONNECTFAIL',           23); //联系不上
define('BANKER_FOLLOW_WAITAUTH',              24); //等待认证
define('BANKER_FOLLOW_WAITCHARGE',            25); //等待充值
define('BANKER_FOLLOW_WAITTUIGUANG',          26); //等待推广
define('BANKER_FOLLOW_PENDING',               27); // 业务暂停（休假等）
define('BANKER_FOLLOW_QUIT',                  28); // 客户退出
define('BANKER_FOLLOW_IN_COOPERATION',        29); // 稳定合作中
//客户的业务跟进状态集合 （系统触发）
define('BANKER_FOLLOW_REG',                   21); //客户在BD平台自主注册
define('BANKER_FOLLOW_APPLY_REG',             22); //潜在客户在BD平台自主注册
define('BANKER_FOLLOW_CHENGSHU',              30); //客户转为成熟客户
//客户转介状态
define('BANKER_TO_LOAN', 40); //转介给贷款
define('BANKER_TO_CREDIT', 50); //转介给信用卡

define('BANKER_DETAIL_MISSED', 51); //资料缺失
define('BANKER_DETAIL_MISMATCH', 52); //资料不符
define('BANKER_DETAIL_NOTQUALIFIED', 53); //资质不足
define('BANKER_DETAIL_PENDINGAUTH', 54); //等待再次认证
define('BANKER_DETAIL_EBANKISSUE', 55); //网银问题
define('BANKER_DETAIL_OPERATIONISSUE', 56); //操作问题
define('BANKER_DETAIL_CHANGSHI', 57); //客户仅尝试
define('BANKER_DETAIL_ACCOUNTANTISSUE', 58); //财务确认问题
define('BANKER_BC_CONFIRM_TUIFEI', 59); //bc/bc组长退费确认
define('BANKER_DETAIL_DAIKUAN_USER', 60); //贷款用户

//TODO

//信贷员任务场景
define('BANKER_TASK_STAGE_REGISTERED', 1); //已注册未认证
define('BANKER_TASK_STAGE_AUTHED', 2); //已认证无产品
define('BANKER_TASK_STAGE_HAVEPRODUCT', 3); //有产品未推广
define('BANKER_TASK_STAGE_BIDDING', 4); //已推广无首单
define('BANKER_TASK_STAGE_HAVEORDER', 5); //完成首单之后

//客户的生命周期节点定义
define('BANKER_LIFECYCLE_INVALID', 0); //无效客户（包括伪冒的、无跟进人的）
define('BANKER_LIFECYCLE_POTENTIAL', 1); //潜在客户
define('BANKER_LIFECYCLE_DAITUIGUANG', 2); //待推广
define('BANKER_LIFECYCLE_CHANGSHI', 3); //尝试
define('BANKER_LIFECYCLE_WENDING', 4); //稳定
define('BANKER_LIFECYCLE_LIUSHI', 5); //流失
define('BANKER_LIFECYCLE_WANHUI', 6); //挽回
define('BANKER_LIFECYCLE_EXIT', 7); //退出


//客户价值分级类型（打分结果按 消费/最后时间/订单数 排序）
define('BANKER_IMPORTANT_KEEP',     1); //重要保持客户（高/高/高）  A类保持
define('BANKER_IMPORTANT_DEVELOP',  2); //重要发展客户（高/高/低）  B类发展
define('BANKER_IMPORTANT_RETRIEVE', 3); //重要挽留客户（高/低/高）  A类挽留
define('BANKER_GENERAL',            4); //一般客户       （高/低/低）  B类挽留
define('BANKER_GENERAL_IMPORTANT',  5); //一般重要客户（低/高/高）  C类发展   
define('BANKER_VALUELESS',          6); //无价值客户   （低/低/高）  C类挽留
define('BANKER_NEWDEV',          7); //无价值客户   （低/高/低）  新发展客户  
define('BANKER_LOWVALUE',          8); //无价值客户   （低/低/低）  低价值客户

//add by caoxiaolin at 2012.12.12，无效订单状态
DEFINE('INVALID_ORDER_STATUS_NEW',          0); //信贷员提交
DEFINE('INVALID_ORDER_STATUS_KEFU_NOTSURE', 1); //客服不确定,modify by zhangji at 2013.4.27
DEFINE('INVALID_ORDER_STATUS_KEFU_REJECT',  2); //客服拒绝
DEFINE('INVALID_ORDER_STATUS_ADMIN_PASS',   3); //销售主管通过
DEFINE('INVALID_ORDER_STATUS_ADMIN_REJECT', 4); //销售主管拒绝
DEFINE('INVALID_ORDER_STATUS_KEFU_PASS',    5); //客服通过，modify by zhangji 2013.4.27
DEFINE('INVALID_ORDER_STATUS_SYSTEM_PASS',  6); //系统审核通过，modify by caoxiaolin at 2013.5.23
DEFINE('INVALID_ORDER_STATUS_KEFU_SHENHEZHONG',  7); //客服审核中
DEFINE('INVALID_ORDER_STATUS_KEFU_WUXUSHENHE',   8); //无需审核
DEFINE('INVALID_ORDER_STATUS_SYSTEM_REJECT',   9); //自动审核拒绝
DEFINE('INVALID_ORDER_STATUS_DONGJIE',  101); //冻结

//2013.5.23 add by caoxiaolin，如果order的spam_score>=该设置，则视为无效订单，系统可以直接退单
DEFINE('MIN_ORDER_SPAM_SCORE', 49);

//无效订单审批，审核中原因一级原因
DEFINE('INVALID_ORDER_PASS_REASON_KONGHAO',1);//无法连通-空号、欠费、关机，（三次均为该状态)
DEFINE('INVALID_ORDER_SHENHEZHONG_REASON_LUYINQUESHI', 41);//录音缺失，待核实

//无效订单审批，通过原因一级原因
//DEFINE('INVALID_ORDER_PASS_REASON_CHONGFUSHENQING_TONGJIGOU',2);//通过原因-重复申请同一个机构第二个信贷员
DEFINE('INVALID_ORDER_PASS_REASON_WAIDIHAO',3);//异地工作
//DEFINE('INVALID_ORDER_PASS_REASON_WEISHENQING',4);//未申请贷款
//DEFINE('INVALID_ORDER_PASS_REASON_NO_NEED',9);//不需要贷款
//DEFINE('INVALID_ORDER_PASS_REASON_SHIYONG',5);//用户试用
DEFINE('INVALID_ORDER_PASS_REASON_ZHONGJIE_TONGHANG',6);//中介、同行试用
DEFINE('INVALID_ORDER_PASS_REASON_CHONGFUSHENQING',7);//重复申请-同一信贷员
DEFINE('INVALID_ORDER_PASS_REASON_QITA',8);//其他
DEFINE('INVALID_ORDER_PASS_REASON_WEIMAO',10);//伪冒
DEFINE('INVALID_ORDER_PASS_REASON_ZIZHIBUFU',11);//资质不符

//无效订单审批，拒绝原因一级原因
//DEFINE('INVALID_ORDER_REJECT_REASON_ZIZHIBUFU',21);//拒绝原因：用户资质不符
//DEFINE('INVALID_ORDER_REJECT_REASON_KONGHAO',22);//拒绝原因：空号、欠费、关机，（客服仍可接通，不管是否有人接听）
DEFINE('INVALID_ORDER_REJECT_REASON_QITA',23);//拒绝原因：其他
//DEFINE('INVALID_ORDER_REJECT_REASON_NO_NEED',24);//拒绝原因：不需要
//DEFINE('INVALID_ORDER_REJECT_REASON_WEISHENQING',25);//拒绝原因：未申请
DEFINE('INVALID_ORDER_REJECT_REASON_BENDIGONGZUO',26);//本地工作
DEFINE('INVALID_ORDER_REJECT_REASON_FEIZHONGJIETONGHANG',27);//非中介同行
DEFINE('INVALID_ORDER_REJECT_REASON_CHONGFUSHENQING',28);//非本网站重复申请
DEFINE('INVALID_ORDER_REJECT_REASON_ZHENSHIYONGHU',29);//真实用户
DEFINE('INVALID_ORDER_REJECT_REASON_KEYIJIETONG',30);//可以接通
DEFINE('INVALID_ORDER_REJECT_REASON_ZIZHIFUHE',31);//资质符合
DEFINE('INVALID_ORDER_REJECT_REASON_RECORD_KEYIJIETONG',32);//双方录音有接通状态
DEFINE('INVALID_ORDER_REJECT_REASON_RECORD_BUMANZUBODATIAOJIAN',33);//虚拟录音不满足拨打条件
DEFINE('INVALID_ORDER_REJECT_REASON_RECORD_NONE',34);//双方无拨打记录

//无效订单审核通过二级的原因
DEFINE('INVALID_ORDER_PASS_LEVEL2_REASON_YONGHUBUREN',101);//未申请贷款
DEFINE('INVALID_ORDER_PASS_LEVEL2_REASON_WUXUQIU',102);//无贷款需求
DEFINE('INVALID_ORDER_PASS_LEVEL2_REASON_DUOCISHENQING',103);//单人申请同一机构第二个信贷员
DEFINE('INVALID_ORDER_PASS_LEVEL2_REASON_CHAOQI',104);//申请时间超过两个月
DEFINE('INVALID_ORDER_PASS_LEVEL2_REASON_TONGYIXINDAIYUAN',105);//同一信贷员

//无效订单审核拒绝二级的原因
DEFINE('INVALID_ORDER_REJECT_LEVEL2_REASON_YOUXUQIU',201);//有贷款需求
DEFINE('INVALID_ORDER_REJECT_LEVEL2_REASON_BENRENJIETING',202);//本人接听

//无效订单审核审核中二级的原因
DEFINE('INVALID_ORDER_SHENHEZHONG_LEVEL2_REASON_KONGHAO',301);//空号，停机，关机，来电提醒
DEFINE('INVALID_ORDER_SHENHEZHONG_LEVEL2_REASON_BOTONGWURENJIETING',302);//拨通、但无人接听

//add by xiaoyaxin at 2013.11.13，逾期订单状态
DEFINE('OVERDUE_ORDER_STATUS_NEW',          0); //信贷员提交，客服未审核
DEFINE('OVERDUE_ORDER_STATUS_KEFU_DOING',   1); //客服审核中
DEFINE('OVERDUE_ORDER_STATUS_KEFU_REJECT',  2); //客服审核拒绝
DEFINE('OVERDUE_ORDER_STATUS_KEFU_PASS',    3); //客服审核通过
//add by xiaoyaxin at 2013.11.14，逾期订单审核原因
DEFINE('OVERDUE_ORDER_CHECK_NO_ANSWER',  1); //无法连通
DEFINE('OVERDUE_ORDER_CHECK_REFUSED',    2); //用户拒绝回访
DEFINE('OVERDUE_ORDER_CHECK_SURPASS',    3); //仅逾期
DEFINE('OVERDUE_ORDER_CHECK_BADACCOUNT', 4); //已坏账


//add by frank
DEFINE('ORG_TYPE_BANK',         1); //银行
DEFINE('ORG_TYPE_XIAODAI',      2); //小贷
DEFINE('ORG_TYPE_DIANDANG',     3); //典当
DEFINE('ORG_TYPE_ZHONGJIE',     4); //中介
DEFINE('ORG_TYPE_OTHER',        5); //其他
DEFINE('ORG_TYPE_CARD',         6); //信用卡
DEFINE('ORG_TYPE_ASSURE',       7); //担保

//add by zhangji 2013.2.1,信贷员-产品 绑定状态
DEFINE('BANKER_PRODUCT_BIND',1);       //绑定
DEFINE('BANKER_PRODUCT_UNBIND',2);     //正常解绑,即未绑定
DEFINE('BANKER_PRODUCT_UNBIND_TEMP',3);//临时解绑

//问答平台 add by tianweiying 2013-04-12 
//回复者类型
DEFINE('RESPONDENT_BY_ADMIN',3); //管理员回复
DEFINE('RESPONDENT_BY_BANKER',2); //信贷员回复
DEFINE('RESPONDENT_BY_USER',1);
//问题及回答审核状态
DEFINE('AUDIT_STATUS_NEW',0);//新提交
DEFINE('AUDIT_STATUS_PASS',1);//审核通过
DEFINE('AUDIT_STATUS_FAIL',2);//审核不通过或删除
DEFINE('AUDIT_STATUS_REC',3);//审核通过并推荐
DEFINE('AUDIT_STATUS_SEX',10);//完成敏感词扫描
//问答平台搜索及列表页展示数目
DEFINE('ASK_SEARCH_PAGE_SIZE',10);
//问答平台对应信贷员积分奖励规则类型
DEFINE('ASK_BANKER_SCORE_OP_TYPE_HOT',202);
DEFINE('ASK_BANKER_SCORE_OP_TYPE_NORMAL',201);
DEFINE('ASK_BANKER_SCORE_OP_TYPE_ADOPTE',203);
//对积分操作的上限的限制
DEFINE('ASK_BANKER_SCORE_LMIMIT',20);
DEFINE('ASK_BANKER_SCORE_ADOPTED_LIMIT',30);//信贷员回答被认同每日加积分上限

DEFINE('EDM_TUISONG_TYPE_PRODUCT',1);//产品推送
DEFINE('EDM_TUISONG_TYPE_SUCC'   ,5);//成功放款
DEFINE('EDM_TUISONG_TYPE_ANSWER' ,9);//问题回答，产品页
DEFINE('EDM_TUISONG_TYPE_HUODONG',10);//活动营销类
DEFINE('EDM_TUISONG_TYPE_SHOUCANG',11);//收藏
DEFINE('EDM_TUISONG_TYPE_CAILIAO',12);//初满，通知准备材料
DEFINE('EDM_TUISONG_TYPE_SHENQING',13);//申请成功
DEFINE('EDM_TUISONG_TYPE_WENDA',  14);//问答页回答
DEFINE('EDM_TUISONG_TYPE_SAVEARTICLE',15); //保存房贷文章
DEFINE('EDM_TUISONG_TYPE_SAVECALRESULT',16);//计算器保存结果
DEFINE('EDM_TUISONG_TYE_BANKRATECHANGE',17);//银行利率变化
DEFINE('EDM_TUISONG_TYE_CITYRATECHANGE',18); //城市利率变化
DEFINE('EDM_TUISONG_TYE_FANGDAI_SHOUCANG',19); //房贷产品收藏
DEFINE('EDM_TUISONG_TYE_FANGDAI_SHENGQING',20); //房贷申请成功
DEFINE('EDM_TUISONG_TYE_ASK_PASS',21); //问答审核成功
DEFINE('EDM_TUISONG_TYPE_SHOUDONG_EDM',22);//手动发送的EDM邮件
DEFINE('EDM_TUISONG_TYPE_MATERIAL_NOTICE',23);//风控贷款办理材料和地址通知

//金桥-申请提现的状态
define('WAIT_FOR_REVIEW',1);//等待付款
define('HAS_PAID',2);//已打款
define('REJECT',3);//拒绝提现

/****
 *金桥-账户交易的类型
 */
define('TRAN_ORDER_GET', 20);//飞单者的合作收款
define('TRAN_ORDER_PAY', 10);//接单者的合作付款
define('TRAN_TIXIAN'   , 30);//提现
define('TRAN_PAYOUT_FUNDS', 40);    // 合作赔付
define('TRAN_AMOUNT_PAYMENT_COOP', 50); // 使用帐户金额支付合作费用


define("QUESTION_REPLY_CLOSE_THRESHOLD" ,15);//回答超过关闭的数目

//订单类型，对应safe.apply_order的division字段
DEFINE('LOAN_ORDER',        1);
DEFINE('CREDIT_CARD_ORDER', 2);
/** 
 * From: rong360/bd/model/BankerSettle.php
 * By: yangyj
 * at: 2013.05
 * */
define('SETTLE_INDUSTRY_YEAR_NONE',         0);
define('SETTLE_INDUSTRY_YEAR_LESS_ONE',     1);
define('SETTLE_INDUSTRY_YEAR_ONE',          2);
define('SETTLE_INDUSTRY_YEAR_ONE2TWO',      3);
define('SETTLE_INDUSTRY_YEAR_TWO2THREE',    4);
define('SETTLE_INDUSTRY_YEAR_MORE_THREE',   5);

define('REFUSE_REASON_NOT_OPEND',           1);
define('REFUSE_REASON_NEED_QUALIFICATION',  2);
define('REFUSE_REASON_NEED_WORK_YEAR',      3);
define('REFUSE_REASON_NO_ANSWER',           4);
define('REFUSE_REASON_OTHER',               5);
define('REFUSE_REASON_NOT_WISE_PAY',        6);
define('REFUSE_REASON_WORRY_COOP',          7);
define('REFUSE_REASON_NOT_TO_WORK',         8);
define('REFUSE_REASON_WATITING_INTERVIEW',  9);
define('REFUSE_REASON_UPLOADING_SOURCE',    10);
define('REFUSE_REASON_AUDITING',            11);
define('CONTACT_BANKER_CONSIDERATION',      12);

define('VERIFY_METHOD_NO', 0); //无需验证
define('VERIFY_METHOD_CAPTCHA', 1); //图片验证码
define('VERIFY_METHOD_MOBILE', 2); //手机验证码，包括短信途径或者电话途径发送。
define('VERIFY_METHOD_BOTH', 3); //手机验证码+图片验证码，包括短信途径或者电话途径发送。

//ASK QUESTION TYPE ADD BY ZHANGTAO
define('ASK_QUESTION_NORMAL',1);
define('ASK_QUESTION_PRODUCT',2);
define('ASK_QUESTION_GONGLUE',3);
define('ASK_QUESTION_CREDIT',4);
define('ASK_QUESTION_LICAI', 5);

/**
 * 通用 API 客户端接口
 */
define('IMG_AUTH_URL_PREFIX',               'https://www.rong360.com/image/f?token=');
define('IMG_AUTH_URL_PREFIX_FOR_LOAN',      'http://is.rong360.com/image/f?token=');//风控系统图片访问prefix
define('IMG_AUTH_URL_PREFIX_FOR_MONEYMIS',      'http://moneymis.rong360.com/image/f?token=');//理财直购后台管理系统图片访问prefix
define('IMG_AUTH_URL_PREFIX_FOR_COOPERATOR',      'http://cooperator.rong360.com/image/f?token=');//理财机构后台管理系统图片访问prefix
define('IMG_AUTH_URL_PREFIX_FOR_MONEY_MAIN',      'https://www.rong360.com/zhigou/image/f?token=');//理财直购平台图片访问prefix,https
define('IMAGE_SHARE_KEY',   "rong@com#0kl\0\0\0\0");

define('DOC_AUTH_URL_PREFIX_FOR_LOAN',      'http://is.rong360.com/image/doc?token=');//风控系统文件访问prefix
define('DOC_AUTH_URL_PREFIX_FOR_MONEYMIS',      'http://moneymis.rong360.com/image/doc?token=');//理财直购后台管理系统文件访问prefix
define('DOC_AUTH_URL_PREFIX_FOR_CRM',      'http://crm.rong360.com/image/doc?token=');//CRM中对公总行中的产品大纲文档
define('DOC_SHARE_KEY',   "rong@com#doc\0\0\0\0");


/** 图片都会统一放到以下三个父目录中 */
define('IMAGE_PUBLISHED',   'published');
define('IMAGE_PENDING',     'ia');
define('IMAGE_TRASH',       'trash');
define('IMAGE_ORIGINAL',    'original');
define('DOC_ORIGINAL',      'doc');
define('MP3_ORIGINAL',      'mp3');

define('IMAGE_STAT_PENDING',    1);
define('IMAGE_STAT_PUBLISHED',  2);
define('IMAGE_STAT_DELETED',    3);
define('IMAGE_STAT_ORIGINAL',   4);

define('IMAGE_NAME_METHOD_KEEP', 'keep');
define('IMAGE_NAME_METHOD_HASH', 'hash');

define('IMAGE_USAGE_TYPE_UNDEFINED',        0);
define('IMAGE_USAGE_TYPE_LOGO',             1);
define('IMAGE_USAGE_TYPE_LOGO_ICON',        2);
define('IMAGE_USAGE_TYPE_LOGO_32PX',        3);
define('IMAGE_USAGE_TYPE_UPLOAD_ORG_IMG',   4);
define('IMAGE_USAGE_TYPE_HEAD_PORTRAIT',    5);
define('IMAGE_USAGE_TYPE_IDENTITY_CARD',    1001);
define('IMAGE_USAGE_TYPE_WORK_CARD',        1002);
define('IMAGE_USAGE_TYPE_ORIGINAL',         9);
define('IMAGE_USAGE_TYPE_BANK_OTHER',       6);
define('IMAGE_USAGE_TYPE_LOUPAN',           10);
define('IMAGE_USAGE_TYPE_RISK_CONTROL',     7);   //add by caoxialin at 2014.2.28
define('DOC_USAGE_TYPE_RISK_CONTROL',       8);   //add by guoxubin at 2014.04.10
define('IMAGE_LICAI_UPLOAD',                1003);   //add by zhanglijie at 2014.06.20
define('IMAGE_USAGE_TYPE_HELP_PROBLEM',     11);  //help系统问题图片
define('IMAGE_USAGE_TYPE_ICREDIT',12);
define('DOC_USAGE_TYPE_MONEY_CONTROL',   1004);   //add by shupan at 2014.12.19
define('DOC_USAGE_TYPE_ISORG',    1005); //add by zhangshuguo at 2015.02.12 对公总行
define('IMAGE_USAGE_FOR_HUXING',    2001);//户型所用图片
define('IMAGE_USAGE_FOR_PRODUCT',    1006); 
define('IMAGE_USAGE_TYPE_ISONLINE',	1007);
define('IMAGE_USAGE_FOR_REFUND',	1008);
define('IMAGE_USAGE_FOR_LIVENESS',	1011);
define('IMAGE_USAGE_FOR_MAIN_WECHAT', 1012);//add by sunjiapeng at 2016.12.02 官微所用图片
define('IMAGE_USAGE_TYPE_LIVENESS',         1020);//活体识别照片 by xiaochang 2016-12-12
define('DOC_USAGE_TYPE_LIVENESS',    1021);//活体识别文件 by xiaochang 2016-12-12
define('VIDEO_USAGE_TYPE_TAOJINYUN', 1022);     // 淘金云小视频


define('IMAGE_VERIFY_STAT_DOING',           0);
define('IMAGE_VERIFY_STAT_PASS',            1);

define('IMAGE_CREDIT',                      20);
define('IMAGE_CREDIT_DEFAULT',              21);

define('IMAGE_HEAD_IMAGE',                  22);

/** 上传文件大小限制, nginx: 8M, php: 8M, 统一为 8M*/
define('UPLOAD_FILESIZE_LIMIT',            50);
/** end } */

//产品状态
define('PRODUCT_STATUS_WAIT', 0);   //待上线
define('PRODUCT_STATUS_ONLINE', 1); //在线
define('PRODUCT_STATUS_OFFLINE', 2); //线下

//bank表auth字段状态，机构的审核状态
define('BANK_AUTH_NOT_PASS', 0); //未认证
define('BANK_AUTH_PASS', 1);     //已认证

//BD个人主页引导流程完成状态
define('NOT_CREATE_PERSONAL_PAGE', 0); //未开始创建个人主页
define('FINISH_CREATE_STAGE', 1);   //创建完成，添加产品和上传头像都用这个状态标识创建完成

//banker表auth字段状态，信贷员的审核状态
define('BANKER_AUTH_NOT_PASS', 0); //未认证
define('BANKER_AUTH_PASS', 1); //通过认证（个人在企业的身份认证）  (update by shuguo)
//define('BANKER_AUTH_PERSONAL', 1); //通过个人认证
//define('BANKER_AUTH_ORG',      2); //通过机构认证

//2013.8.12 add by caoxiaolin，产品类型，个人产品或标准产品
define('PRODUCT_STANDARD_TYPE_STANDARD', 1); //标准产品
define('PRODUCT_STANDARD_TYPE_PERSONAL', 2); //个人产品


//BD用户申请取消推广后，申请的状态 。added by guoxubin
define('QUIT_BIDDING_UNPROCESS',    0);//待处理
define('QUIT_BIDDING_PASS',         1);//申请通过
define('QUIT_BIDDING_REJECT',       2);//申请被拒
//BD用户申请认证之后，认证的状态。added by guoxubin
define('AUTH_UNPROCESS',                0);//待处理
define('AUTH_PASS',                     1);//申请通过
define('AUTH_REJECT',                   2);//申请被拒
define('CANCEL_AUTH_FOR_ORG_UNAUTH',    3);//因机构取消认证，而信贷员认证被取消
define('CANCEL_AUTH_FOR_INFOR_CHANGED', 4);//因信贷员个人信息修改，被取消认证

//准入抽取到字段，不要求的定义为特殊值
define('PERMIT_FIELD_EMPTY',    -999);

/** 
 * 成功案例和产品数限制
 * */
define('SUCCESS_CASE_LIMIT',    3);
define('CUSTOM_PRODUCT_LIMIT',  5);

/**
 * 信贷经理问答排名控制
 * */
define('QA_RANK_WEEK_KEY',      'QaRankStatis_week_pass_total');
define('QA_RANK_DAY_KEY',       'QaRankStatis_day_pass_total');
define('QA_RANK_FRIDAY',        5);
define('QA_RANK_HOUR_LIMIT',    18);
define('QA_RANK_NUMBER_LIMIT',  10);
define('QA_RANK_PASS_THRESHOLD',99);    /** 排名显示 99+ 的临界值 */

//BD和CRM交互使用的两种场景标记，这两个值需要和CRM保持统一
define('CRM_WORKFLOW_TYPE_AUTHBANKER',      48);//认证信贷员
define('CRM_WORKFLOW_TYPE_UNBINDFANGDAI',   49);//解绑中介担保和房贷的竞价
define('CRM_WORKFLOW_TYPE_UNBINDSTANDARD',  2); //解绑标准产品
//BD和CRM交互时用于描述信贷员认证信息的标记
define('AUTH_CHANGE_BY_BANKREVOKEAUTH', 3); //机构取消认证导致信贷员需要重新认证
define('AUTH_CHANGE_BY_BANKERREVOKEAUTH', 4); //信贷员信息变更等导致被取消认证

//显示中介担保竞价位对信贷员所在机构的要求
define('ZHONGJIE_BIDDING_SETUP_YEAR', 2);//成立年限大于等于2
define('ZHONGJIE_BIDDING_REG_CAPITAL',50);//注册资金大于等于50

define('UNOPEN_ZONE', 99); //未开通城市归属区域
define('UNOPEN_CITY_CODE', 9999); //未开通城市的代码

/** 
 * 逾期/坏帐控制开关
 * 采用 memcache
 * */
define('APPLY_ORDER_OVERDUE_SWITCH', 'APPLY_ORDER_OVERDUE_SWITCH');

//查询订单列表时，按照订单状态集合进行查询
DEFINE('ORDER_STATUS_GROUP_FINAL_STATUS',            1);//最终状态的订单
DEFINE('ORDER_STATUS_GROUP_NOT_FINAL_STATUS',        2);//非最终状态的订单
DEFINE('ORDER_STATUS_GROUP_NOT_NEW',                 3);//非新订单

//给信贷员展示的客服电话
//define('RONG_400_SERVICE', '4008-901-360');
define('RONG_400_SERVICE', '10100-360');
define('RONG_400_USER', '4008-905-360');
define('RONG_400_USER_SMS', '4008905360');
define('RONG_400_BANKER', '4006-200-360');
define('RONG_400_BANKER_SMS', '4006200360');
define('CUSTOMER_QUICK_QUERY_COUNT_KEY', 'CUSTOMER_QUICK_QUERY_COUNT_');

//电话语音验证码来电号码
define('VERIFY_CODE_CALLS_NUMBER', '010-84359010');

//直通车标示
define('ZHITONGCHE',  'zhitongche');

/** 积分激励 */
// 即时
define('INCITE_LOGIN',                  'incite_login_');
define('INCITE_FEEDBACK_4_ORDER',       'incite_feedback_4_order_');
define('INCITE_SUBMIT_CUSTOMER_INFO',   'incite_submit_customer_info_');
define('INCITE_ANSWER_QUESTION',        'incite_answer_question_');
define('INCITE_HOT_QUESTION',           'incite_hot_question_');
// 延时
define('INCITE_SUCCESS_LOAN',           'incite_success_loan_');
define('INCITE_ANSWER_WAS_ADOPTED',     'incite_answer_was_adopted_');
define('INCITE_OVERDUE_AUDIT_PASSED',   'incite_overdue_audit_passed_');

//站内信优化建议类型
define('TIP_HAS_PERSONAL_PRODUCT',  1);
define('TIP_AUTH_DOING',            2);
define('TIP_CRM_AUTH_DOING',        3);
define('TIP_AUTH_PASS_APPLY',       4);
define('TIP_AUTH_FAIL',             5);
define('TIP_UNBIND_PRODUCT_WAITING',6);
define('TIP_UNBIND_PRODUCT_SUCCESS',7);
define('TIP_UNBIND_PRODUCT_FAIL',   8);
define('TIP_ORDER_LOAN_SUCCESS',    9);
define('TIP_ORDER_LOAN_UNSUCCESS',  10);
define('TIP_INVALID_ORDER_ACCEPT',  11);
define('TIP_INVALID_ORDER_REJECT',  12);

//客户跟进状态变化的触发类型
define('BANKER_FOLLOW_TRIGGER_ADMIN', 1); //系统管理员（BC等）
define('BANKER_FOLLOW_TRIGGER_SYSTEM', 2);  //系统触发(由于数据变化，系统自动写入一条跟进记录)
define('BANKER_FOLLOW_TRIGGER_CUSTOMER', 3);  //客户自行触发(信贷员)

//客户类型
define('CUSTOMER_TYPE_BANKER', 1); //注册客户，对应banker表中的记录
define('CUSTOMER_TYPE_BANKERAPPLY', 2); //潜在客户，对应banker_apply表中的记录

//站内链接推荐页面类型
define('NOT_PAGE',0);
define('CONTENT_PAGE',  1);//内容聚合页为1 
define('HOT_PRODUCT_PAGE',  2);//产品聚合页为2
define('AGENT_CITY_PAGE',  3);//机构聚合页城市页为3
define('STANDARD_PRODUCT_PAGE',  4);//标准产品页为4
define('AGENT_COUNTRY_PAGE',  5);//机构聚合全国无属性页为5需要考虑分页情况
define('AGENT_COUNTRY_MORTGAGE_PAGE',  6);//机构聚合页抵押类型属性页为6需要考虑分页情况
define('AGENT_COUNTRY_APPLICATION_PAGE',  7);//机构聚合页用途类型属性页为7
define('CREDIT_BANK_PAGE',  8);//信用卡银行专题页为8
define('CREDIT_APPLY_PAGE',  9);//信用卡申请列表页为9
define('CREDIT_COUPON_PAGE',  10);//信用卡优惠列表为10
define('OFFER_SHOP_PAGE',  11);//优惠商户聚合页为11
define('LOUPAN_DISTRICT_PAGE',12);//楼盘区域列表页12
define('LOUPAN_DETAIL_PAGE',13);//楼盘详情页13
define('BANK_PAGE',14);//银行网点 14
define('BANK_BRANCH_PAGE',15);//银行支行 15
define('CREDIT_DETAIL_PAGE',16);//信用卡详情 16
define('CREDIT_INTEGRAL_PAGE',17);//信用卡积分商城 17
define('COMMON_STRATEGY_PAGE',  21);
define('CREDIT_STRATEGY_PAGE',  22);

//融易记推荐/认证成功奖励的金额
define('RONGYIJI_RECOMMEND_AWARD',  10);//认证成功奖励10元

//r360.org表中的is_test字段，0线上机构  1测试机构
define('ORG_FOR_REAL', 0);
define('ORG_FOR_TEST', 1);

//公积金基准利率
define('GJJ_LE5_rate',2.75);
define('GJJ_GT5_rate',3.25);
//房贷基准利率
define('FANGDAI_RATE',4.90);

define('FANGDAI_GJJ_PRODUCT_TYPE',2);
define('FANGDAI_NORMAL_PRODUCT_TYPE',1);

//和融360特殊合作的安家世行id
define('AN_JIA_SHI_HANG_ID',           14222);//'14222', '付京川', '13051739121'
//银行推送订单不显示用户手机号，生成一个400电话
define('PUSH_ORDER_400_PHONE','4006-170-360');

//融易记极光推送配置
define('MESSAGE_TYPE_NOTIFY',       1); //通知
define('MESSAGE_TYPE_COMPLEX_MSG',  2); //自定义信息
define('RECEIVER_TYPE_IMEI',        1); //极光推送接收类型


define('BANK_PUSH_ORDER_COST',    500);//推送订单的价格
define('CONSULTANT_ORDER_COST',   1200);//咨询订单的价格

define('BANK_PUSH_ORDER_MOBILE_NEW',    0);//推送订单状态，待处理
define('BANK_PUSH_ORDER_MOBILE_SEND',   1);//推送订单状态，手机号已发送
define('BANK_PUSH_ORDER_GIVE_UP',       2);//推送订单状态，放弃此订单
define('BANK_PUSH_ORDER_OVERDUE',       3);//订单已过期，回收400电话
define('CONSULTANT_ORDER_NEW',          0);//商务通咨询创建的订单状态，待处理
define('CONSULTANT_ORDER_CONTACTED',    1);//商务通咨询创建的订单状态，已联系

define('GUIDE_TASK_AUTH',       1);//BD新手任务-认证
define('GUIDE_TASK_PRODUCT',    2);//BD新手任务-添加产品
define('GUIDE_TASK_SUCCESS',    3);//BD新手任务-添加成功案例
define('GUIDE_TASK_ANSWERS',    4);//BD新手任务-回答问题
define('GUIDE_TASK_BIDDING',    5);//BD新手任务-参与推广
define('GUIDE_TASK_RECHARGE',   6);//BD新手任务-充值

define('GUIDE_TASK_AUTH_SCORE_TYPE',       9);//BD新手任务奖励分类型-认证，common/model/MailTips.php中有一个相关操作
define('GUIDE_TASK_PRODUCT_SCORE_TYPE',    10);//BD新手任务奖励分类型-添加产品
define('GUIDE_TASK_SUCCESS_SCORE_TYPE',    11);//BD新手任务奖励分类型-添加成功案例
define('GUIDE_TASK_ANSWERS_SCORE_TYPE',    12);//BD新手任务奖励分类型-回答问题
define('GUIDE_TASK_BIDDING_SCORE_TYPE',    13);//BD新手任务奖励分类型-参与推广
define('GUIDE_TASK_RECHARGE_SCORE_TYPE',   14);//BD新手任务奖励分类型-充值

//batch脚本生成的临时文件存放位置
define('BATCH_REPORT_PATH', '/home/rong/www/trunk/rong360/batch/reports/');

//楼盘tag类型
define('LOUPAN_PUTONGZHUZHAI',       1);//普通住宅
define('LOUPAN_BIESHU',              2);//别墅
define('LOUPAN_GONGYU',              3);//公寓
define('LOUPAN_QITA',                4);//其他

//呼叫中心
define('CC_SYSTEM_RONG360', 1);
define('CC_SYSTEM_TIANRUN', 2);
define('CC_SYSTEM_TIANRUN_1', 3);
define('CC_SYSTEM_XUNNIAO', 4);
define('CC_SYSTEM_TIANRUN_2', 5); //天润大平台，用于催收
define('CC_SYSTEM_VLINK', 6);//预测外呼 VLINK

//呼叫中心所属公司
define('CC_COMPAMY_TIANRUN', 1);
//天润系统参数定义
define('VLINK_APPID_VALUE_XIAN',                        5000376);//预测外呼vlink参数appid
define('VLINK_TOKEN_VALUE_XIAN',                        '5c251d5e19293792d93de3e361e0c3d3');//预测外呼vlink参数appid
define('VLINK_APPID_VALUE_NON_XIAN',                        5000362);//预测外呼vlink参数appid
define('VLINK_TOKEN_VALUE_NON_XIAN',                        '688da743c53564649c0ba4c95d2dffa2');//预测外呼vlink参数appid

//呼叫类型
define('CC_CALL_TYPE_IN', 1);//呼入
define('CC_CALL_TYPE_OUT', 2);//呼出

define('VLINK_CALL_RECORD_URL', 'http://api.vlink.cn/interface/open/v2/cdrObInterface'); //vlink获取通话记录url
define('VLINK_CALL_URL', 'http://api.vlink.cn/interface/open/v1/webcall'); //vlink拨打电话url
define('CC_CALL_URL', '/interface/PreviewOutcall'); //拨打电话url
define('CC_CALL_OUT_RECORD_URL', '/interfaceAction/cdrObInterface!listCdrOb.action'); //获取外呼通话记录url
define('CC_CALL_IN_RECORD_URL', '/interfaceAction/cdrIbInterface!listCdrIb.action'); //获取呼入通话记录url
define('CC_ALL_QUEUE_URL', '/interfaceAction/queueInterface!list.action'); //查看所有队列url
define('CC_CALL_OUT_DETAIL_URL', '/interfaceAction/cdrObInterface!listCdrObDetail.action'); //获取外呼通话记录详情url
define('CC_CALL_IN_DETAIL_URL','/interfaceAction/cdrIbInterface!listCdrIbDetail.action'); //获取呼入通话记录详情url
define('CC_CALL_OUT_QUEUE_INFO_URL', '/interfaceAction/queueInterface!clientInfoByQueue.action'); //获取队列下面所有座席接口url
define('CC_CALL_OFF_LINE', '/interface/client/ClientOffline'); //坐席下线
define('CC_CALL_ON_LINE', '/interface/client/ClientOnline'); //坐席上线
define('CC_CALL_QUEUE_MONITOR', '/interface/queueMonitoring/QueueMonitoring');//查看坐席状态
define('CC_CALL_CUSTOMER_SATISFACTION', '/interface/entrance/OpenInterfaceEntrance');//查看坐席状态

//电话对接系统
define('CALL_SYSTEM_RISK',     '1'); //风控系统
define('CALL_SYSTEM_KEFU',     '2'); //客服系统
define('CALL_SYSTEM_DIANXIAO', '3'); //外包电销系统
define('CALL_SYSTEM_CRM',      '4'); //CRM系统
define('CALL_SYSTEM_CUISHOU',  '5'); //催收系统

//电话的业务类型(用于区分透传号码)
define('CALL_BUSINESS_TYPE_DIANXIAO',       '1'); //客服-电销
define('CALL_BUSINESS_TYPE_RISK_DIANXIAO',  '2'); //客服-电销业务
define('CALL_BUSINESS_TYPE_YUCEWAIHU',      '3'); //客服-预测外呼
define('CALL_BUSINESS_TYPE_CONTACT_BANKER', '4'); //CRM-联系信贷员
define('CALL_BUSINESS_TYPE_CUISHOU',        '5'); //催收-催收

//成功放款审核状态
define('ORDER_REVIEW_CHECH_STATUS_WEISHEN',       0);//未审核
define('ORDER_REVIEW_CHECH_STATUS_SHENHEZHONG',   1);//审核中
define('ORDER_REVIEW_CHECH_STATUS_WUJIEGUO',      2);//审核无结果
define('ORDER_REVIEW_CHECH_STATUS_TONGGUO',       3);//审核通过
define('ORDER_REVIEW_CHECH_STATUS_BUTONGGUO',     4);//审核不通过
define('ORDER_REVIEW_CHECH_STATUS_WUXUSHENHE',    5);//无需审核
define('ORDER_REVIEW_CHECH_STATUS_DONGJIE',     101);//审核冻结

//成功放款审核原因
define('ORDER_REVIEW_CHECH_REASON_CHENGGONGFANGKUAN',  1);//成功放款
define('ORDER_REVIEW_CHECH_REASON_YONGHUKAOLV',        2);//用户考虑
define('ORDER_REVIEW_CHECH_REASON_YONGHUFANGQI',       3);//用户放弃
define('ORDER_REVIEW_CHECH_REASON_WEIMAO',             4);//伪冒
define('ORDER_REVIEW_CHECH_REASON_LIANXIBUSHANG',      5);//空号、停机
define('ORDER_REVIEW_CHECH_REASON_YONGHUJUJUEHUIFANG', 6);//用户拒绝回访
define('ORDER_REVIEW_CHECH_REASON_WUFALIANTONG',       7);//无法连通
define('ORDER_REVIEW_CHECH_REASON_SHAOHOUCHULI',       8);//稍后处理

//呼入通话电话状态结果
define('CALL_STATUS_ZUOXIJIETING',    1);//座席接听
define('CALL_STATUS_ZUOXIWEIJIETING', 2);//座席未接听
define('CALL_STATUS_XITONGJIETING',   3);//系统接听
define('CALL_STATUS_PEIZHICUOWU',     4);//系统未接ivr配置错误
define('CALL_STATUS_TINGJI',          5);//系统未接-停机
define('CALL_STATUS_QIANFEI',         6);//系统未接-欠费
define('CALL_STATUS_HEIMINGDAN',      7);//系统未接-黑名单
define('CALL_STATUS_WEIZHUCE',        8);//系统未接-未注册
define('CALL_STATUS_CAILING',         9);//系统未接-彩铃
define('CALL_STATUS_400WEIJIESHOU',   10);//网上400未接受
define('CALL_STATUS_QITACUOWU',       11);//其他错误
define('CALL_STATUS_VLINKWEIJIETONG',       12);//未接听
define('CALL_STATUS_VLINKYIJIETONG',       13);//已接听
define('CALL_STATUS_VLINKYIZHUANJIE',       14);//已呼转
define('CALL_STATUS_VLINKHUZHUANYIQIAOJIE',       15);//已桥接

//外呼通话电话状态结果
define('CALL_STATUS_NOT_ANSWER_TIMEOUT', 21);//座席接听，客户未接听(超时)
define('CALL_STATUS_NOT_ANSWER_EMPTY',   22);//座席接听，客户未接听(空号拥塞)
define('CALL_STATUS_AT_NOT_ANSWER',      24);//座席未接听
define('CALL_STATUS_TWO_SIDE_ANSWER',    28);//双方接听

//充值类型
define('RECHARGE_TYPE_LIZI',             1); //例子充值
define('RECHARGE_TYPE_XIAOGUOYUCHONG',   2); //效果预充
define('RECHARGE_TYPE_XUNI',             3); //QC虚拟充值
define('RECHARGE_TYPE_TUIFEI',           4); //退费
define('RECHARGE_TYPE_QIANFEIBUJIAO',    5); //欠费补缴
define('RECHARGE_TYPE_DEPOSIT',          6); //充值保证金
define('RECHARGE_TYPE_REFUND_DEPOSIT',   7); //保证金退款

//机构系数默认值
define('BANK_KPI_REFACTOR_DEFAULT' , 0.67); //默认的机构系数

//预充值状态
define('BANKER_PRECHARGE_STATUS_OPEN',0); //开放
define('BANKER_PRECHARGE_STATUS_CLOSE_BY_BC',1); //BC关闭
define('BANKER_PRECHARGE_STATUS_CLOSE_BY_BD',2); //信贷员完成充值关闭

//退费原因
define('TUIFEI_REASON_LIZHI', 1);           //信贷员离职,
define('TUIFEI_REASON_ZHUANHANG', 2);       //信贷员转行业,
define('TUIFEI_REASON_ZHANGHUZHUANYI', 3);  //账户转移（废弃）,
define('TUIFEI_REASON_POLICY', 4);          //对公或政策原因',
define('TUIFEI_REASON_HEGUI', 5);           //合规问题,
define('TUIFEI_REASON_BAN', 6);             //非本人或被封禁,
define('TUIFEI_REASON_TEST', 7);            //平台测试（废弃）,
define('TUIFEI_REASON_HUANCHENGSHI', 8);    //换城市（废弃）,
define('TUIFEI_REASON_WUDAN', 9);           //无单下线,
define('TUIFEI_REASON_ZHILIANGCHA', 10);    //质量差下线,
define('TUIFEI_REASON_CHONGZHILOW',  11);   //充值金额不达最低要求
define('TUIFEI_REASON_OTHER', 12);          //其它（请在备注中说明）,
define('TUIFEI_REASON_WUCAOZUO', 13);       //误操作,
define('TUIFEI_REASON_PINGTAIBUMANYI', 14); //对平台规则不了解或不满意
define('TUIFEI_REASON_GUWENBUMANYI', 15);   //对商务顾问不满意
define('TUIFEI_REASON_BUTUPIAN', 101);      //待补充图片(用于客服BD)
define('TUIFEI_REASON_WEILIANTONG', 102);   //未接通（用于客服审核BD）
define('TUIFEI_REASON_PERREASON', 103);   //信贷员个人原因，无法继续合作

//用户订单评价状态 UserOrderRating
define('USER_ORDER_RATING_INIT', 0); //'初始化',
define('USER_ORDER_RATING_SMS_SENT', 1); //'已发短信',
define('USER_ORDER_RATING_COMPLETE', 2); //'评价完成',

//KPI计算时标识某机构是新机构还是老机构
define('BANK_FACTOR_NEW', 1); //新机构
define('BANK_FACTOR_OLD', 2); //老机构

//岗位类型
define('ROLE_TYPE_COMMON', 1); //一般岗位
define('ROLE_TYPE_SALER', 2);  //销售岗位
define('ROLE_TYPE_MANAGER', 3);//管理岗位

//团队中角色类型(最大值为40，后续如需增加，请夹缝里添加)
define('COUNTRY_LEADER', 10); //全国LEADER
define('ZONE_LEADER', 20); //区LEADER
define('GROUP_LEADER', 30); //组长
define('SELF_LEADER', 40); //BC

//城市开放类型
define('OPEN_TYPE_OPEN', 1); //开放城市
define('OPEN_TYPE_EXCLUSIVE' , 2); //专属城市 

//BC可接收客户的标记
define('BC_ASSIGN_FALG_YES', 1); //可接收
define('BC_ASSIGN_FALG_NO', 2); //不可接收

//banker显示给BC的种类
define('ONLINE_BC_SHOW', 1);  //展示给线上BC
define('OFFLINE_BC_SHOW', 2);  //展示给线下BC
define('BOTH_BC_SHOW', 3); //线上，线下BC都展示
define('BOTH_BC_NO_SHOW',4); //线上，线下BC都永不展示，

//接收客户的BC队列
define('QUEUE_GROUP_FORE', 1);  //前端队列
define('QUEUE_GROUP_BACK', 2);  //后端队列
define('QUEUE_GROUP_CREDIT', 3); //信用卡队列
define('QUEUE_GROUP_FORE_COMMONBANKER', 4); //公共客户登录BD队列

//角色ID的常量定义(每次增加角色时，需要同步更新此配置)
define('ROLE_SYS_ADMIN', 1); //系统管理员
define('ROLE_BC_MANAGER', 3); //BC主管
define('ROLE_EDITOR_MANAGER', 7); //编辑主管
define('ROLE_MARKET_DBBD', 10); //市场（数据库营销）
define('ROLE_BC', 11); //BC
define('ROLE_QC_MANAGER', 12); //品控主管
define('ROLE_QC', 15); //品控
define('ROLE_PM_AI', 17); //aiPM
define('ROLE_FINANCE', 18);// 财务
define('ROLE_CUSTOMERCARE', 21); //客服
define('ROLE_HR', 22); //HR(招聘)
define('ROLE_DATA_ANALY', 26);//数据分析
define('ROLE_CUSTOMERCARE_MANAGER', 37);//客服主管
define('ROLE_ZONE_ADMIN', 39); //区行政
define('ROLE_QC_ZHUANSHEN', 41);//专审品控
define('ROLE_EDITOR_QA', 42);//问答编辑
define('ROLE_PM_CREDITCARD', 43); //信用卡PM
define('ROLE_EDITOR_FANGDAI', 44);//房贷编辑
define('ROLE_EDITOR_CREDITCARD', 45);//信用卡编辑
define('ROLE_BDMANAGER_CREDITCARD', 46); //信用卡运营主管
define('ROLE_MARKET_MANAGER', 47);//市场主管
define('ROLE_TUIJIAN_CREDITCARD', 48);//信用卡推荐
define('ROLE_SENIOR_EDITOR', 49); //高级编辑
define('ROLE_PM_UP', 50); //UP组PM
define('ROLE_PM_AM', 51); //AM组PM
define('ROLE_MARKET_SEM', 52);//市场（SEM）
define('ROLE_MARKET_SEO', 53); //市场（SEO）
define('ROLE_MARKET_BD', 54); //市场（BD）
define('ROLE_MARKET_MOBILEBD', 55); //市场（移动BD）
define('ROLE_MARKET_MOBILERESULTBD', 56); //市场（移动效果营销）
define('ROLE_BC_CREDITCARD', 64); //信用卡BC
define('ROLE_BC_BACKGROUND', 65); //后端BC
define('ROLE_BC_FANGDAI', 66); //房贷BC
define('ROLE_FRONT_MANAGER', 71); //前端经理
define('ROLE_BACK_MANAGER', 72); //后端经理
define('ROLE_RISK', 76); //风控
define('ROLE_RISK_MANAGER',77);//风控品控
define('ROLE_BC_OFFLINE', 85); //线下BC
define('ROLE_BC_GROUPLEADER', 88); //BC团队组LEADER
define('ROLE_IS_ORG_BC', 91); //i.	IS洽谈合作BC
define('ROLE_IS_ORG_BC_LEADER', 92); //i.	IS洽谈合作BC Leader
define('ROLE_TASK_POOL', 97); //审批任务池
define('ROLE_MENDIAN', 98); //门店
define('ROLE_QC_FUSHEN', 104); //复审品控
define('ROLE_BC_IS', 89); //BC(IS岗)
define('ROLE_YUNYING_MARGIN', 120); //运营中心(保证金审核)
define('ROLE_BD_ORG_LEADER', 123); //机构BD 邵老板  总leader
define('ROLE_BD_ORG', 124); //机构BD 区leader
define('ROLE_BD_ORG_TJ', 127); //机构BD天机审批
define('ROLE_BD_ORG_TJY', 128); //机构BD淘金云审批
define('ROLE_BD_ORG_LPP', 129); //机构BD类拍拍审批
define('ROLE_BD_ORG_GDT', 130); //机构BD广点通审批
define('ROLE_BD_ORG_PLZ', 131); //机构BD对公例子审批
define('ROLE_BD_ORG_IS', 137); //机构BD
define('ROLE_GONGAN_AUDIT',144);//公安验证专审品控
define('ROLE_BANKER_MANAGE',145);//信贷员审核主管
define('ROLE_BANKER_PHONE',146);//电话审核专员


//拍拍贷默认全国产品产品id（编码过）10058313
define('PAIPAIDAI_ENCODE_PRODUCT_ID', '1b37815fihnotsig');
//拍拍贷每天限制订单数（仅限迪斯尼拍拍贷申请） 已废弃 配置迁移至 /trunk/rong360/main/service/enum/DoudiProductEnum.php
define('PAIPAIDAI_MAX_ORDER_COUNT', 8500);
//友信兜底每天限制订单数  已废弃
define('YOUXIN_MAX_ORDER_COUNT', 1000);

//手机贷产品编号
define('SHOUJIDAI_PRODUCT_ID',10103840);
//手机贷跳转地址
define('SHOUJIDAI_JUMP_URL','https://m.rong360.com/openloan');

//原子贷机构id
define('YUANZIDAI_ORG_ID',9606);

//小米黄页utm_source
define('XIAOMI_HUANGYE_UTM_SOURCE', 'xiaomi');
define('XIAOMI_HUANGYE_UTM_MEDIUM', 'huangye');

//默认产品机构logo
define('DEFAULT_BANK_LOGO', 'upload/png/5b/2f/5b2f9140e533b00f08adbec3c64e3f51.png');

//微信红包活动设置
define('WEIXIN_HONGBAO_USE_TYPE', 1); //红包批号
define('WEIXIN_HONGBAO_CLICK_NUM', 5); //红包需要几个好友打开才能生效

//APP 类型
define('APP_ID_R360', 1);
define('APP_ID_JSD', 2);
define('APP_ID_LOAN_OLD', 3);
define('APP_ID_CREDIT', 4);
define('APP_ID_R360PRO', 11);
define('APP_ID_LOAN', 13);

//APP PUSH SDK 提供商信息
define('APP_PUSH_PROVIDER_JPUSH', 'jpush');
define('APP_PUSH_PROVIDER_JPUSH_VALUE', 1);
define('APP_PUSH_PROVIDER_XIAOMI', 'xiaomi');
define('APP_PUSH_PROVIDER_XIAOMI_VALUE', 2);
define('APP_PUSH_PROVIDER_GETUI', 'getui');
define('APP_PUSH_PROVIDER_GETUI_VALUE', 3);
define('APP_PUSH_PROVIDER_UMENG', 'umeng');
define('APP_PUSH_PROVIDER_UMENG_VALUE', 4);


//发送短信涉及的平台
define('SEND_MESSAGE_PLATFORM_KEFU',   1); //客服系统
define('SEND_MESSAGE_PLATFORM_HELP',   2); //HELP系统
define('SEND_MESSAGE_PLATFORM_RISK',   3); //风控系统

//每位跟进BC可保留的客户数
define('BC_RESERVED_BANKER_COUNT', 5);

//经营贷中介单量
define('JINGYINGDAI_AGENT_ORDER_LIMIT', 3);

//站内信type
define('MAILINFO_TYPE_NORMAL', 0);
define('MAILINFO_TYPE_NOTICE', 1);

//个人产品未推广情况下拿单上线
define('PERSONAL_PRODUCT_UPPERLIMIT', 4);

//BC的跟进任务节点，对应crm.mission_node中的id
define('MISSION_NODE_BANKERREG', 1); //信贷员注册
define('MISSION_NODE_CLAIM_COMMENBANKER', 2); //认领公共客户(未认证)
define('MISSION_NODE_CONVERT_TO_BANKER', 3); //潜客转为注册
define('MISSION_NODE_LABEL_FAKE_BANKER', 4); //标记无效客户
define('MISSION_NODE_FOLLOW_3TIMES', 5); //跟进3次
define('MISSION_NODE_BANKER_REQUEST_AUTH', 6); //信贷员申请认证（BD+CRM）
define('MISSION_NODE_FOLLOWER_TRANSFER', 7); //解除跟进关系
define('MISSION_NODE_BIDDING_BALANCE', 8); //推广客户的余额低于设定值（推广客户帐户余额报警）
define('MISSION_NODE_NORECHARGEWITHBC', 9); //已欠费认证客户未在跟进BC名下发生过充值
define('MISSION_NODE_BANKERAUTH_REJECT', 10); //客户认证请求被拒绝
define('MISSION_NODE_BANKERAUTH_PASS', 11); //客户认证请求被通过
define('MISSION_NODE_ONLINERECHARGE_SUC', 12); //线上充值成功
define('MISSION_NODE_OFFLINERECHARGE_CON', 13); //财务确认线下充值
define('MISSION_NODE_ONLINEHARGE_TIMEOUT', 14); //在线充值超时未入账
define('MISSION_NODE_OFFLINERECHARGE_OVERTIME', 15); //线下充值逾期未确认
define('MISSION_NODE_TWOLIZHIRECHARGE', 16); //生命周期内完成两次例子充值
define('MISSION_NODE_CHARACTORIZE_BANKER', 17); //标注客户特征
define('MISSION_NODE_ONLINERECHARGE_REQ', 18); //BD发起线上充值
define('MISSION_NODE_OFFLINERECHARGE_REQ', 19); //BD发起线下充值
define('MISSION_NODE_OFFLINERECHARGESPECIAL_CON', 20); //财务确认线下充值（低于城市最低额度的线下充值）
define('MISSION_NODE_BC_CONFIRM_RECHARGE', 21); //BC确认充值
define('MISSION_NODE_SELF_REMIND', 22); //BC主动设置跟进提醒（在客户列表中更新跟进状态时设置提醒）
define('MISSION_NODE_FOLLOW_WITHOUTREMIND', 23); //BC在任务下更新跟进状态但不设置提醒时间
define('MISSION_NODE_FOLLOW_UPDATE', 24); //修改跟进记录
define('MISSION_NODE_NOBALANCEBANKER_PASSAUTH', 25); //客户通过认证（包括申请和直接认证），且余额小于等于0
define('MISSION_NODE_CLAIM_AUTH_NOBALANCE_BANKER', 26); //认领已认证且余额小于等于 0的公共客户(2015-04-10先鹏确认改为50)
define('MISSION_NODE_ONLINERECHARGE_SUC_NOBIDDING', 27); //线上充值成功（未推广客户）
define('MISSION_NODE_OFFLINERECHARGE_CON_NOBIDDING', 28); //财务确认线下充值（未推广客户）
define('MISSION_NODE_CLAIM_AUTH_BALANCE_NOBIDDING_BANKER', 29); //认领已认证且有余额的未推广公共客户
define('MISSION_NODE_BIDDING', 30); //客户参与推广
define('MISSION_NODE_BANKERREG_BD', 31); //客户通过BD平台注册
define('MISSION_NODE_BANKER_REQUEST_AUTH_BD', 32); //信贷员通过BD申请认证
define('MISSION_NODE_COMMONBANKER_LOGIN_BD', 33); //公共信贷员登录BD平台
define('MISSION_NODE_RPIZE_COMMONBANKER', 34); //系统奖励公共客户给BC
define('MISSION_NODE_NEW_MATURE_BANKER', 35); //新成熟客户
define('MISSION_NODE_OWE_AUTOMATIC_UNBUND', 36); //欠费自动解绑
define('MISSION_NODE_UNANSWERED_CALL_IN', 37); //未接来电
define('MISSION_NODE_BANKER_APPLY_REFUND', 38); //BD信贷员申请退款
define('MISSION_NODE_HELP_BANKER_AUTH_BANK', 39); //帮助信贷员认证机构
define('MISSION_NODE_TRANS_BANKER_CREDIT_TO_LOAN', 40); //信用卡踢转到贷款的客户，任务提醒
define('MISSION_NODE_HELP_REVIEW_BANKER_EDIT_INFO', 41); //帮助复审品控复审信贷员（修改资料），任务提醒
define('MISSION_NODE_BANKER_REFUND_NOTICE_BC', 42); //信贷员退款的任务	转到bc，bc处理并填写反馈

define('MISSION_NODE_BANKER_WEITUIGUANG_BACKGROUND', 43); //20天未推广，任务提醒
define('MISSION_NODE_BANKER_LIUSHI_BACKGROUND', 44); //流失客户跟进，任务提醒
define('MISSION_NODE_BANKER_CHARACTOR_TODO', 45); //特征资料不完善，任务提醒
define('MISSION_NODE_BANKER_CHARACTOR_MAITAIN', 46); //维护客户特征，任务提醒
define('MISSION_NODE_BANKER_50_WEITUIGUANG_BACKGROUND', 47); //50天未推广，任务提醒
define('MISSION_NODE_BANKER_CHARACTOR_SCORE_TODO', 48); //成熟客户特征未评分
define('MISSION_NODE_OFFLINERECHARGE_DENY', 49); //线下充值异常
define('MISSION_NODE_BANKER_ACTIVE_3_ORDERS', 50); //新进活跃客户接3单提醒 
define('MISSION_NODE_BANKER_ACTIVE_QUIT_BIDDING_15', 51); //活跃客户取消推广15天
define('MISSION_NODE_BANKER_LIUSHI_15_DAY', 52); //流失挽回客户15天要流失的客户
define('MISSION_NODE_BANKER_HELP_SYNC_TO_BC', 53); // 	CRM任务关联help系统问题同步处理
define('MISSION_NODE_BANKER_CLICK_HLEP_LINK', 54); //点击链接进入help系统
define('MISSION_NODE_REMIND_RECHARGE_MARGIN', 59); //信贷员超单提醒充值保证金
define('MISSION_NODE_BANKER_IS_MARGIN', 60); //信贷员充值保证金
define('MISSION_NODE_CONFIRM_RECHARGE_MARGIN', 61); //BC确认保证金充值
define('MISSION_NODE_ERROR_RECHARGE_MARGIN', 62); //保证金充值异常
define('MISSION_NODE_FINAL_RECHARGE_MARGIN_SU', 63); //财务确认保证充值
define('MISSION_NODE_BANKER_REFUND_MARGIN', 64); //信贷员退款保证金
define('MISSION_NODE_CONFIRM_REFUND_MARGIN', 65); //BC确认保证金退款
define('MISSION_NODE_BANKER_CONSUME_NOT_ENOUGH', 66); //信贷员充值后4天内消费未满
define('MISSION_NODE_BANKER_LOAD_NEW_APP', 67); //提醒信贷员下载新版APP
define('MISSION_NODE_BANKER_LOGIN_NEW_APP', 68); //登录新版APP
define('MISSION_NODE_BAOYUE_BANKER_BC_REMIND',69); // BC通知信贷员包月到期
define('MISSION_NODE_BANKER_MODIFY_INFO',70);//信贷员自助修改资料
define('MISSION_NODE_BANKER_GONGAN_BAN',74);//信贷员永久拒绝合作

define('IS_ORG_STATUS_NEW', 1); //新录入
define('IS_ORG_STATUS_CHUBULIANXI', 2); //初步联系
define('IS_ORG_STATUS_PRODUCT_SUMMARY', 3); //已获取产品大纲
define('IS_ORG_STATUS_QIATAN', 4); //洽谈合作方案
define('IS_ORG_STATUS_SIGN', 5); //已签约
define('IS_ORG_STATUS_REJECT', 6); //拒绝合作
define('IS_ORG_STATUS_LIZI', 7); //转对公例子合作

define('IS_ORG_CONTACT_STATUS_NEW', 1); //新录入
define('IS_ORG_CONTACT_STATUS_CHUBULIANXI', 2); //初步联系
define('IS_ORG_CONTACT_STATUS_WAITING', 3); //等待答复
define('IS_ORG_CONTACT_STATUS_QIATAN', 4); //洽谈中
define('IS_ORG_CONTACT_STATUS_SIGN', 5); //洽谈成功
define('IS_ORG_CONTACT_STATUS_TRANSFER', 6); //转介给别人
define('IS_ORG_CONTACT_STATUS_REJECT', 7); //拒绝合作

define('IS_ORG_TRIP_STATUS_PLANING', 0); //计划中',
define('IS_ORG_TRIP_STATUS_PROCESSING', 1); //拜访进行中',
define('IS_ORG_TRIP_STATUS_FINISHED', 2); //拜访已结束',

//高端会所订单来源
define('MARKET_ORDER_FROM_RISK', 0);
define('MARKET_ORDER_FROM_KEFU', 1);

//高端会所订单竞价状态
define('MARKET_ORDER_STATUS_WEISHEZHI', 		10);
define('MARKET_ORDER_STATUS_WEIKAISHI', 		20);
define('MARKET_ORDER_STATUS_JINGJIAZHONG', 		30);
define('MARKET_ORDER_STATUS_JINGJIACHENGGONG', 		40);
define('MARKET_ORDER_STATUS_LIUPAI', 			50);

define('MARKET_ORDER_STATUS_LIUPAI_HOLD', 			55);//流拍过程拦截

define('MARKET_ORDER_STATUS_QUXIAO', 			60);
define('MARKET_ORDER_STATUS_FEIDAN', 			70);//飞单

//高端会所订单流入项目
define('MARKET_ORDER_FLOW_DISNY', 	1);


/**
 * obj type credit: from 1 to 1000
 */
define('OBJ_TYPE_CREDIT_ARTICLE', 1);
define('OBJ_TYPE_CREDIT_OFFER', 2);
define('OBJ_TYPE_CREDIT_CARD', 4);
define('OBJ_TYPE_CREDIT_ASK', 5);

/**
 * obj type licai: from 10000 to 20000
 */
define('OBJ_TYPE_LICAI_ARTICLE', 10001);

define('MARKET_ORDER_FROM_FENGKONG', 1);	//薪火订单来源于风控
define('MARKET_ORDER_FROM_DIANXIAO', 2);	//薪火订单来源于电销

/**
 * 客户分配管理
 * @var liuchi
 */
define('BANKER_ASSIGN_TYPE_DISTRIBUTE_BD', 1); //分配新BD,信用卡/贷款踢转,成熟转后端
define('BANKER_ASSIGN_TYPE_ADD_BANKER_APPLY', 2); //录入潜客
define('BANKER_ASSIGN_TYPE_GET_BANKER_APPLY', 3); //领取潜客
define('BANKER_ASSIGN_TYPE_GET_BANKER_COMMON', 4); //领取公共客户
define('BANKER_ASSIGN_TYPE_DISTRIBUTE_BANKER', 5); //分配客户
//define('BANKER_ASSIGN_TYPE_CREDIT_LOAN_CHANGE', 6); //信用卡/贷款踢转
//define('BANKER_ASSIGN_TYPE_CHENGSHU_BACK_CHANGE', 7); //成熟转后端

define('USERMARKET_1_OFFLINE_SELL_BOTTOM_PRICE', 50); //锤客线下销售底价分界线
define('USERMARKET_1_OFFLINE_SELL_PRICE', 30); //锤客线下销售直售价


define('ORDER_FEATURE_HIDDEN_TO_USER', 250); // 订单特性 - 对用户隐藏

/**
 * 客户来源
 * 1新BD、2潜客、3公共池领取、4公共池分配、5划转
 * @var liuchi
 */
define('BANKER_FROM_TYPE_NEW_BD', 1); //新BD
define('BANKER_FROM_TYPE_BANKER_APPLY', 2); //潜客
define('BANKER_FROM_TYPE_CLAIM_BANKER_COMMON', 3); //公共池领取
define('BANKER_FROM_TYPE_ASSIGN_BANKER_COMMON', 4); //公共池分配
define('BANKER_FROM_TYPE_TRANSFROM_BANKER', 5); //划转
define('BANKER_FROM_TYPE_ASSIGN_BANKER_COMMON_DOUBLEV', 6); //公共池分配—双V
define('BANKER_FROM_TYPE_ASSIGN_BANKER_COMMON_APPROVING', 7); //公共池分配—审核中
define('BANKER_FROM_TYPE_ASSIGN_BANKER_COMMON_HIGHSCORE', 8); //公共池分配—高评分
define('BANKER_FROM_TYPE_ASSIGN_BANKER_COMMON_LOGIN', 9); //公共池分配—登录
define('BANKER_FROM_TYPE_ASSIGN_BANKER_COMMON_AUTH', 10); //公共池分配—30天内有认证操作
define('BANKER_FROM_TYPE_ASSIGN_BANKER_COMMON_REGISTER', 11); //公共池分配—60天内注册

/**
 * 潜客添加来源（领取、分配、自己录入）
 * @var liuchi
 */
define('BANKER_APPLY_FROM_TYPE_CLAIM_BANKER_APPLY', 1); //领取
define('BANKER_APPLY_FROM_TYPE_ASSIGN_BANKER_APPLY', 2); //分配
define('BANKER_APPLY_FROM_TYPE_NEW_BANKER_APPLY', 3); //自己录入
define('BANKER_APPLY_FROM_TYPE_TEST_ASSIGN_BANKER', 4); //测试分配

//信贷员退款审核状态
define('BANKER_REFUND_STATUS_NEW', 1); //待处理（bd）
define('BANKER_REFUND_STATUS_BC_DEAL', 2); //bc处理中（kefu）
define('BANKER_REFUND_STATUS_BC_LEADER_DEAL', 3); //bcleader处理中（kefu）
define('BANKER_REFUND_STATUS_REVIEW', 4); //待复审（crm）
define('BANKER_REFUND_STATUS_OWN_CANCEL', 5); //信贷员取消退款（bd）
define('BANKER_REFUND_STATUS_VISIT', 6); //待回访（kefu）
define('BANKER_REFUND_STATUS_WORKING', 7); //退款中（kefu）
define('BANKER_REFUND_STATUS_CANCEL', 8); //取消退款（kefu）
define('BANKER_REFUND_STATUS_SUCCESS', 9); //退款成功（mis）
define('BANKER_REFUND_STATUS_FAILURE', 10); //退款失败（mis）



//地推城市生命周期天数-city 信贷员跟进时间： 普通城市30天，地推城市60天
define('BANKER_CITY_COMMON_CYCLE', 30); //普通城市30天
define('BANKER_CITY_DITUI_CYCLE', 60); //地推城市60天

//抓取类型
define('ZHIMAXINYONG',1);
define('YINHANGLIUSHUI',2);
define('YUNYINGSHANG',3);
define('YOUXIANG',4);
define('SHEBAO',5);
define('GONGJIJIN',6);
define('ZHENGXIN',7);
define('JINGDONG',8);
define('YINHANGYANKA',9);
define('ALIPAY',10);
define('ZHIMA',11);

//抓取结果状态
define('VERIFY_SUCCESS',2); //抓取成功
define('VERIFY_FAIL',3);    //抓取失败
define('VERIFY_JINXING',1); //抓取中
define('VERIFY_PARTIALLY',4);    //抓取部分完成


//产品业务类型
define('WITH_RISK_SERVICE_LIZI',      0); //例子
define('WITH_RISK_SERVICE_IS',        1); //IS
define('WITH_RISK_SERVICE_ISALL',     2); //ISALL
define('WITH_RISK_SERVICE_TAOJINYUN', 3); //淘金云

//个人产品策略优化，无效退费最低额度(针对南昌,太原,温州)
define('REFUNDBASEDPRICE', 15); //无效订单退费：15元及以上方可退费

//crm&bd例子产品类型
define('PRODUCT_LIZI_TYPE_PERSONAL', 1); //个人例子
define('PRODUCT_LIZI_TYPE_COMPANY', 2); //对公例子
define('PRODUCT_LIZI_TYPE_PER_COM', 3); //个人和对公例子

//bd 消息队列search_fail
define('MQ_BD_SEARCH_FAIL_UPDATE', 'all-bd_search_fail_update_mq');
//金融百科
//目录-推荐状态
define('BAIKE_CATALOG_RECOMMEND_PC', 0); //PC首页推荐
define('BAIKE_CATALOG_RECOMMEND_MOBILE', 1); //Mobile首页推荐

//关键词-自定义属性
define('BAIKE_KEYWORD_PC_PPT', 0); //PC-幻灯片
define('BAIKE_KEYWORD_PC_RECOMMEND', 1); //PC-首页推荐
define('BAIKE_KEYWORD_PC_THUMBNAIL', 2); //PC-首页缩略图
define('BAIKE_KEYWORD_MOBILE_PPT', 3); //Moblie-幻灯片
define('BAIKE_KEYWORD_MOBILE_HOTRECOMMEND', 4); //Mobile-热门推荐

//关键词-状态
define('BAIKE_KEYWORD_DRAFT', 1); //草稿
define('BAIKE_KEYWORD_ONLINE', 2); //上线
define('BANKER_TUIFEI_STATUS_NO', 0); //无退费
define('BANKER_TUIFEI_STATUS_DOING', 1);//退费中
define('BANKER_TUIFEI_STATUS_SUCCESS', 2);//退费成功

//攻略文章统计缓存Key
define('REDIS_GL_CLICK_RESULT', 'redis_gl_click_result');

//信贷员保证金类型
define('BANKER_DEPOSIT_PRODUCT_ORDERS', 1);

//保证金充值状态
define('BANKER_DEPOSIT_COMMIT', 0);//信贷员提交充值信息
define('BANKER_DEPOSIT_BC_COMMIT', 1);//bc审核通过
define('BANKER_DEPOSIT_BC_LEADER_COMMIT', 2);//crm Bcleader审核通过
define('BANKER_DEPOSIT_CRM_COMMIT', 3);//运营审核通过
define('BANKER_DEPOSIT_MIS_COMPLETE', 4);//mis财务批准
define('BANKER_DEPOSIT_MIS_WEISHOUDAOKUAN', 5);//mis财务审核未收到款
define('BANKER_DEPOSIT_MIS_JINEBUFU', 6);//mis财务审核金额不符
define('BANKER_DEPOSIT_MIS_REFUND_FAIL', 7);//mis财务审核金退款失败
define('BANKER_DEPOSIT_CRM_LEADER_CANCEL', 8);//crm Bcleader取消审批
define('BANKER_DEPOSIT_CRM_CANCEL', 9);//crm运营中心取消审批
define('BANKER_DEPOSIT_CRM_STATUS_CHANGE', 10);//crmBC解除跟进关系

//API error统一定义
define('API_ERROR_SYSTEM', -1); //网络或系统异常（当前http请求未能正常完成，即curl_errno不为0）
define('API_ERROR_OK', 0); //正常
define('API_ERROR_TOKEN_INVALID', 2); //token值无效
define('API_ERROR_MISSING_CONFIG_MACHINE', 3); //未配置machine
define('API_ERROR_MISSING_CONFIG_PLATFORMLINE', 4); //未配置PLATFORM_LINE
define('API_ERROR_IP_NOT_ALLOWED', 5); //ip禁止访问
define('API_ERROR_TOKEN_REQUIRED', 6); //token值缺失
define('API_ERROR_JSON_ERROR', 7); //服务器端json处理异常


//mission id
define('MISSION_CONFIRM_BANKER_MARGIN', 39);//核实信贷员充值保证金
define('MISSION_CONFIRM_BANKER_REMARGIN', 41);//核实信贷员退款保证金


//机构BD 跟进进程
define('BD_ORG_POTENTIAL', 3);//潜客确立
define('BD_ORG_ENTRY', 6);//录入机构
define('BD_ORG_SEE_TOP', 9);//见决策人和上层
define('BD_ORG_UNBUSINESS', 12);//非商务见面决策人
define('BD_ORG_ACC_GIFT', 15);//礼品接受
define('BD_ORG_TECH_MEET', 18);//技术见面
define('BD_ORG_START_DEVELOP',21);//对方开始开发
define('BD_ORG_CON_CONFIRM', 24);//商务条件确认
define('BD_ORG_CONTRACT_SIGNED', 27);//合同签署
define('BD_ORG_ADVANCE_PAY', 30);//预付款
define('BD_ORG_ONLINE', 33);//上线
define('BD_ORG_ALL', 36);//全部

//机构BD 区域
define('BD_ORG_AREA_EAST', 2);//华东
define('BD_ORG_AREA_SOUTH', 6);//华南
define('BD_ORG_AREA_NORTH', 15);//华北
define('BD_ORG_AREA_ALL', -1);//华北

//机构BD bd_org 业务线
define('BD_ORG_LINE_TIANJI', 1);//天机
define('BD_ORG_LINE_TAOJINYUN', 2);//淘金云
define('BD_ORG_LINE_LEIPAIPAI', 3);//类拍拍
define('BD_ORG_LINE_GUANGDIANTONG', 4);//广点通
define('BD_ORG_LINE_LIZI', 5);//对公例子

//机构BD bd_org PAGESIZE
define('BD_ORG_PAGE_SIZE', 10);//每页显示数
define('BD_ORG_CONTACT_PAGE_SIZE', 5);//联系人每页显示数

//机构BD status
define('BD_ORG_DEL_EXAM', -1);//删除
define('BD_ORG_WAIT_EXAM', 0);//未审批
define('BD_ORG_ACCESS_EXAM', 1);//审批通过
define('BD_ORG_REJECT_EXAM', 2);//审批拒绝
define('BD_ORG_OVER_EXAM', 3);//审批过期
define('BD_ORG_OTHER_EXAM', 4);//审批中（其他人审批）

//机构BD score_status
define('BD_ORG_NO_MARKING', 0);//未打分
define('BD_ORG_GET_SCORE', 1);//打分并且得分
define('BD_ORG_NO_GET_SCORE', 2);//打分但未得分
define('BD_ORG_SAME_ORG_NO_GET_SCORE', 3);//因同一机构触发特殊逻辑，将其他状态设置为3


//复议常量定义
define('REVIEW_FEIBENRENSHENQING', 0);//非本人申请
define('REVIEW_DAIKUANEDUBUFU', 1);//贷款额度不符
define('REVIEW_LIXIYUEGONGYOUYIYI', 2);//利息月供有异议
define('REVIEW_ZUOXIQIANGTUI', 3);//座席强推
define('REVIEW_ZIZHIXINXILUANTIAN', 4);//资质信息乱填
define('REVIEW_WEIHESHIZHIJIEXIADAN', 5);//未核实直接下单
define('REVIEW_QITA', 6);//其他

define('REVIEW_YIDI', 10);//异地用户
define('REVIEW_WUDAIKUANYIXIANG', 11);//无贷款意向


define('REVIEW_RESULT_ON', 0);//待复议
define('REVIEW_RESULT_PASS', 1);//复议通过
define('REVIEW_RESULT_NONPASS', 2);//复议不通过

define('PERSON_ORDER', 0);//人工单
define('SYSTEM_ORDER', 1);//系统单

//未接听限定时间的长度
define('WEIJIETING_LIMIT_TIME', 30);
//首通录音电话号码类型
define('TYPE_TIANRUN', 1);//天润平台固话号码
define('TYPE_YKT', 2);//联通移客通手机号码
//首通录音监控参数设置
define('TIANRUN_BANKER_COUNT_LIMIT', 25);
define('TIANRUN_USER_COUNT_LIMIT', 5);
define('YKT_BANKER_COUNT_LIMIT', 1);
define('YKT_USER_COUNT_LIMIT', 1);

//可否反馈无效退费原因
define('ORDER_RETURNS_REASON_COMMON_BANKER',1);//对公例子
define('ORDER_RETURNS_REASON_PRODUCT_BAOYUE',2);//包月标准产品
define('ORDER_RETURNS_REASON_STANDARD_DISCOUNT',3);//降价对私标准产品
define('ORDER_RETURNS_REASON_PRIVATE_PRODUCT',4);//对私个人产品例子
