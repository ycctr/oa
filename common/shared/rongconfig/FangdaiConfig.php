<?php
/**
 * 手机请求pc主站域名需要跳转对应手机页面(m.rong360.com)的跳转配置 
 * @author qiaoyuan <qiaoyuan@rong360.com> 
 *
 */
class FangdaiConfig {
     //跳转规则判断
    public static $FANGDAI_KEWORDS_CONF = array(
        '房', '按揭', '公积金', '商贷', '组合贷', '楼', '屋', '装修', '户型', '商铺', '首套', '二套', '户型', '等额本金', '等额本息','税'); 

    public static $FANGDAI_ARTICLE_TYPEID = 19;


    public static $FANGDAI_ARTICLE_CITY = array(
        'bj' => '北京',        
        'sh' => '上海',    
        'gz' => '广州',     
        'sz' => '深圳',     
        'cd' => '成都',     
        'wh' => '武汉',     
        'cs' => '长沙',     
        'zz' => '郑州',     
        'nj' => '南京',     
        'sjz'=>'石家庄',   
        'ty' =>'太原',    
        'hz' =>'杭州',     
        'cc' =>'长春',     
        'dl' =>'大连',     
        'gy' =>'贵阳',     
        'shaoxing'=>'绍兴',     
        'zhuzhou'=>'株洲',     
        'km' =>'昆明',     
        'sy' =>'沈阳',     
        'nn' =>'南宁',     
        'tangshan'=>'唐山',     
        'weihai'=>'威海',     
        'nb'=>'宁波',     
        'cq'=>'重庆',     
        'zs'=>'中山',     
        'zh'=>'珠海',     
        'huizhou'=>'惠州',     
        'dg'=>'东莞',     
        'fs'=>'佛山',     
        'suzhou'=>'苏州',     
        'wuxi'=>'无锡',     
        'changzhou'=>'常州',     
        'tianjin'=>'天津',     
        'yangzhou'=>'扬州',     
        'xuzhou'=>'徐州',    
        'yantai'=>'烟台',     
        'nanchang'=>'南昌',     
        'hefei'=>'合肥',     
        'haerbin'=>'哈尔滨',   
        'xian'=>'西安',     
        'fuzhou'=>'福州',    
        'xiamen'=>'厦门',     
        'qinhuangdao'=>'秦皇岛',   
        'baoding'=>'保定',     
        'jn'=>'济南',     
        'qd'=>'青岛',     
        'tz'=>'台州',     
        'wenzhou'=>'温州',     
        'ganzhou'=>'赣州',     
        'jiujiang'=>'九江',     
        'luoyang'=>'洛阳',     
        'jiaxing'=>'嘉兴',     
        'hhht'=>'呼和浩特', 
        'yinchuan'=>'银川',    
        'lanzhou'=>'兰州',    
        'langfang'=>'廊坊',     
        'baotou'=>'包头',    
        'xining'=>'西宁',     
        'wulumuqi'=>'乌鲁木齐', 

    );
    // public static $FANGDAI_SHOUFU = array('10099519');
}

