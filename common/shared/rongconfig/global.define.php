<?php
/**
 *
 * @author wukezhan<wukezhan@gmail.com>
 * 2015-12-09 17:09
 *
 */

define('WWW_PATH', '/home/rong/www');
define('FRAMEWORK_PATH', WWW_PATH . '/framework');
define('CONFIG_PATH', WWW_PATH . '/config');
define('DATA_PATH', WWW_PATH . '/data');

define('TRUNK_PATH', WWW_PATH . '/trunk');
define('TRUNK_RONG360_PATH', TRUNK_PATH . '/rong360');

define('COMMON_PATH', WWW_PATH . '/common');
define('SHARED_PATH', COMMON_PATH . '/shared');

define('CREDIT_PATH', WWW_PATH . '/credit');
define('LICAI_PATH', WWW_PATH . '/licai');
define('MAIN_PATH', WWW_PATH . '/main');
define('MIS_PATH', WWW_PATH . '/mis');
define('BD_PATH', WWW_PATH . '/bd');
define('OA_PATH', WWW_PATH.'/oa');
define('BATCH_PATH', TRUNK_RONG360_PATH . '/batch');
define('PASSPORT_WEB_PATH', WWW_PATH . '/passport_web');
define('MESSAGE_PATH', WWW_PATH . '/message');
define('COMMON_WEBROOT_PATH', COMMON_PATH . '/webroot');
define('GATEWAY_PATH', WWW_PATH . '/rongsmsgateweb');

define('ZIXUN_PATH', WWW_PATH . '/zixun');
define('TJY_PATH',WWW_PATH . '/tjy');
