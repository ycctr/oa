<?php
/**
 * 手机请求pc主站域名需要跳转对应手机页面(m.rong360.com)的跳转配置 
 * @author qiaoyuan <qiaoyuan@rong360.com> 
 *
 */
class RenderMoblieConfig
{
     //跳转规则判断
    public static $redirectMobile = array(

            "calculator" => array(
                    
                    "all" => 1,

                ), 
            "product"    => array(

                    "detail" => 1,

                ),

            "question"    =>array(

                "detail" =>1,

                ),
            
            "bank"     => array(

                "org"   => 1, 
            
                ),

            
        ); 

    
    public static $cancelrectMobile = array(
        
            "calculator" => array(
                    
                   'fangchanshui'=> 1,
                   'fangchanshui_chongqing'=> 1,
                   'fangchanshui_shanghai'=> 1,

                ), 

        );

    /**
     * 是否跳转判断
     * @param string $contrl  控制器
     * @param string $action  操作
     * @return bool  
     **/
    public static function isRedirectMobile($contrl, $action) {

        if( empty($contrl) || empty($action) ) {

            return false;

        }

        $cancelrectMobile = SELF::$cancelrectMobile;

        //如果匹配成功，过滤跳转
        if( $cancelrectMobile[$contrl][$action] ) {

            return false;

        }

        
        $redirectMobile = SELF::$redirectMobile;

        if( !isset($redirectMobile[$contrl])  ) {

            return false;

        }


        if( isset($redirectMobile[$contrl][$action]) ) { //action配置


            return true;

        }

        if(isset($redirectMobile[$contrl]["all"]) ) {//该control下

            return true;

        }  

        return false;

    }


    //根据url跳转
}

