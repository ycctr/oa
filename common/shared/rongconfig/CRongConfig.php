<?php
class CRongConfig
{
    public $max_apply_num = 4;

    //最低充值金额
    public $min_recharge_amount = 500;
    //信用卡最低充值金额
    public $credit_min_recharge_amount=100;
	//2017国家假期安排，1表示该日放假, 0表示不放假.只需记录周六日补班或工作日放假部分
	public static $holiday = array(
            "20150406" => 1,
            "20150501" => 1,
            "20150622" => 1,
            "20150903" => 1,
            "20150904" => 1,
            "20150906" => 0,
            "20151001" => 1,
            "20151002" => 1,
            "20151005" => 1,
            "20151006" => 1,
            "20151007" => 1,
            "20151010" => 0,
            "20160101" => 1,
            "20160206" => 0,
            "20160208" => 1,
            "20160209" => 1,
            "20160210" => 1,
            "20160211" => 1,
            "20160212" => 1,
            "20160214" => 0,
            "20160404" => 1,
            "20160502" => 1,
            "20160609" => 1,
            "20160610" => 1,
            "20160612" => 0,
            "20160915" => 1,
            "20160916" => 1,
            "20160918" => 0,
            "20161003" => 1,
            "20161004" => 1,
            "20161005" => 1,
            "20161006" => 1,
            "20161007" => 1,
            "20161008" => 0,
            "20161009" => 0,
            "20170102" => 1,
            "20170122" => 0,
            "20170127" => 1,
            "20170130" => 1,
            "20170131" => 1,
            "20170201" => 1,
            "20170202" => 1,
            "20170204" => 0,
            "20170401" => 0,
            "20170403" => 1,
            "20170404" => 1,
            "20170501" => 1,
            "20170527" => 0,
            "20170529" => 1,
            "20170530" => 1,
            "20170930" => 0,
            "20171002" => 1,
            "20171003" => 1,
            "20171004" => 1,
            "20171005" => 1,
            "20171006" => 1,
	);
    
	public function init()
    {
    }

	//检查当天是否是节假日，true 表示是假期
	public function checkHoliday()
	{
		$holiday = self::$holiday;
		$day=date("Ymd",time());
		$week=date("w");
		
		if(isset($holiday[$day]))
		{
			if($holiday[$day] == 1)
				return  1;
			else
				return 0;
		}
		else
		{
			if($week == 0 || $week ==6 )
				return  1;
			else
				return 0;
		}
	}
	
    //对资金进行归一化(万)
    public function getMoney($money) 
    {
        if($money==null)
            return "";
        else 
            return intval($money) / 10000;

    }

    //性别，C
    public function getPersonalSex()
    {
        return array(
            '1' => '先生',
            '2' => '女士',
        );
    }

    //婚姻状况，C
    public function getPersonalIsMarry()
    {
        return array(
            '1' => '已婚无子女',
            '2' => '已婚有子女',
            '3' => '未婚',
            '4' => '离异',
            '5' => '丧偶',
        );
    }

    //是否有贷款经历，C
    public function getPersonalLoanExperience()
    {
        return array(
            '0' => '没有信用记录',
            '1' => '有信用记录(有信用卡或成功办理过贷款)',
            '2' => '不清楚',
        );
    }

    //企业经营时间，C
    public function getCompanyOpPeriod()
    {
        return array(         
            '0' => '不满六个月',
            '0.5' => '满六个月',
            '1' => '满1年',
            '2' => '满2年',
            '3' => '满3年',
            '5' => '满5年',
            '10' => '满10年',
        );
    }
    public function getCreditFailureNum()
    {
        return array( 
                '0'     =>  '0～3次',
                '5'     =>  '4～6次',
                '8'     =>  '7～9次',
                '15'    =>  '10～20次',
                '21'    =>  '21次及以上',
        );
    }

    //企业连续逾期次数，C
    public function  getCompanyCreditContinuousFailure()
    {
        return array(
            '0' => '没有连续逾期',
            '2' => '连续两期',
            '3' => '连续三期',
            '4' => '连续四期',
            '5' => '连续五期及以上',
        );
    }

    //贷款类型，C
    static public function getApplicationType()
    {
        return array(
            '9' => '不限',
            '1' => '经营周转',
            '2' => '个人消费',
            '3' => '贷款买车',
            '4' => '按揭买房',
            '5' => '其他用途',
        );
    }

   //房产类型，Ｃ
	public function getHouseType()
	{
		return array(
			'1' => '第一套住宅',
			'2' => '第二套住宅',
			'3' => '第三套及以上住宅',
			'4' => '商铺',
			'5' => '商住两用',
			'6' => '写字楼',
		);
	}


    //企业经营类型，C
    public function getApplicationOpType()
    {
        return array(
            '1' => '民营企业主',
            '2' => '个体工商户',
            '4' => '在职员工',
            //'4' => '自由职业',
        );
    }

    //买车贷款期限，C
    public function getCarLoanTerm()
    {
        return array(
            '1' =>  '6个月',
            '2' =>  '一年',
            '3' =>  '两年',
            '4' =>  '三年',
            '5' =>  '五年',
        );
    }

    //房龄，C
    public function getHouseAgeTerm()
    {
        return array(
            '5' =>   '1-5年',
            '10' =>  '6-10年',
            '15' =>  '11-15年',
            '20' =>  '16-20年',
            '100' =>  '20年以上',
        );
    }

    //买房贷款期限，C
    public function getHouseLoanTerm()
    {
        return array(
            '3' =>   '3年',
            '5' =>   '5年',
            '10' =>  '10年',
            '15' =>  '15年',
            '20' =>  '20年',
            '30' =>  '30年',
        );
    }

    //收入证明:个人收入准入
    public function getIncomeProof()
    {
        return array(
            //'1' => '必须提供纳税单',
            '1' => '必须提供纳税单、银行流水证明之一',
            '2' => '除纳税单、银行单外，也接受其他收入证明方式',
        );
    }

    //收入方式:用于额度计算
    public function getIncomeType()
    {
        return array(
            '1' => '个人月入卡收入',
            '2' => '个人月入卡加现金收入',
            '3' => '个人月入卡加企业月进账',
            '4' => '企业月进账',
        );
    }

    //户籍所在地，B
    public function getNationality()
    {
        return array(
            '1' => '本地',
            '2' => '外地',
            '3' => '港澳台',
            '4' => '外籍'
        );
    }

    //当前逾期，B
    public function getCurrentCredit()
    {
        return array(
            '1' => '可以有当前逾期', 
            '2' => '不得有当前逾期，但500元以下小额逾期不计',
            '3' => '不得有当前逾期',
        );
    }

    //多少个月逾期少于，B
    public function getCreditFailureMonth()
    {
        return array(
            '6'  => '6个月',
            '12' => '12个月',
            '24' => '24个月',
            '36' => '36个月',
        );
    }

    //连续逾期次数，B
    public function getCreditContinuousFailure()
    {
        return array(
            '2' => '2期',
            '3' => '3期',
            '4' => '4期',
            '5' => '5期',
        );
    }

    //从业年限，B
    public function getTenure()
    {
        return array(
            '0.1' => '未满3个月',
            '0.3' => '3个月以上',
            '0.6' => '半年以上',
            '1'   => '一年以上',
        );
    }


    //本地社保年限，B/C
    public function getSocialSecurity()
    {
        return array(
            '1'  => '未满3个月',
            '3'  => '3个月以上',
            '6'  => '半年以上',
            '12' => '一年以上',
        );
    }

    public function getSocialSecurityForB()
    {
        return array(
            '0'  => '无统一要求',
            '1'  => '有缴纳即可',
            '3'  => '满3个月',
            '6'  => '满半年',
            '12' => '满1年',
        );
    }

    //公司类型，B/C
    public function getCompanyType()
    {
        return array(
            '1' => array(
                'title' => '公务员',
            ),
            '2' => array(
                'title' => '事业编制',
                'category' => array(
                    '20' => '医生',
                    '21' => '教师',
                    '23' => '其它事业编制人员',
                ),
            ),
            '3' => array(
                'title' => '国有垄断行业',
            ),
            '4' => array(
                'title' => '大型企业/上市公司',
            ),
            '5' => array(
                'title' => '普通企业',
            ),
            '6' => array(
                'title' => '律师/注册会计师/高级专业人士',
            ),
            '7' => array(
                'title' => '个体户',
            ),
            '8' => array(
                'title' => '民营企业主',
            ),
        );
    }

    //职位信息，B/C
    public function getPosition()
    {
        return array(
            '30' => '高层管理者、处级以上、高级职称',
            '20' => '中层管理者、科级、中级职称',
            '10' => '普通员工、初级职称或无职称',
        );
    }

    //企业注册时间，B
    public function getRegisterPeriod()
    {
        return array(
            '0.5' => '六个月',
            '1' => '一年',
            '2' => '两年',
            '3' => '三年',
            '5' => '五年',
            '10' => '十年',
        );
    }

    //抵押物类型，B
    public function getCollateralTypeForB()
    {
        return array(
            '1' => '住宅',
            '2' => '商铺',
            '3' => '办公楼',
            '4' => '厂房',
            '5' => '存货',
            '6' => '其他资产',
        );
    }

    //抵押物类型，C
    public function getCollateralType()
    {
        return array(
            '1' => '住宅',
            '2' => '商铺',
            '3' => '办公楼',
            '4' => '厂房',
            '5' => '其他资产',
            '0' => '无抵押物',
        );
    }

    //抵押物位置，B/C
    public function getCollateralPlace()
    {
        return array(
            '1' => '本地市区',
            '2' => '本地郊区',
            '3' => '外地',
        );
    }

    //抵押物未成年人
    public function getCollateralYouth()
    {
        return array(
            '1' => '不接受该抵押物',
            '2' => '接受该抵押物但扣除未成年人份额',
            '3' => '完全接受该抵押物',
        );
    }

    //担保企业类型，C
    public function getGuaranteeCompanyType()
    {
        return array(
            '1' => '有本地住房的个人',
            '2' => '同行业的个人',
            '3' => '家人',
            '4' => '其他个人',
            '5' => '同行业的企业',
            '6' => '其它企业',
            '7' => '担保公司',
        );
    }

    //额度范围个人收入类型
    public function getQuotaPersonalIncomeType()
    {
        return array(
            '1' => '年入卡收入',
            '2' => '年纳税收入',
            '3' => '年实际（经营）收入',
        );
    }

    //额度范围个人收入类型
    public function getQuotaCompanyIncomeType()
    {
        return array(
            '1' => '年入卡收入',
            '2' => '年实际控制人入卡收入',
            '3' => '年纳税收入',
            '4' => '年实际经营收入',
        );
    }

    //金融机构类型
    static public function getOrgType()
    {
        return array(
            '1' => '银行贷款',
            '2' => '小额贷款',
            '3' => '典当行',
            '4' => '中介',
            '5' => '其他出借机构',
            '6' => '信用卡中心',
            '7' => '担保公司',
        );
    }
    
    static public function getOrgTypeName($bank_type)
    {
        if(empty($bank_type))
            return "";
        $bank_types = CRongConfig::getOrgType();
        if(!array_key_exists($bank_type, $bank_types))
            return "";
        return $bank_types[$bank_type];
    }
    
    //信贷员的信息来源
    static public function getBankerSourceConfig(){
        return array(
		    BANKER_SOURCE_SALERSELF => '销售经理自己找',
			BANKER_SOURCE_BANKERZIZHU => '线上自主申请',
			BANKER_SOURCE_OTHERSALER => '同行推荐',
		    BANKER_SOURCE_OTHER => '其他，请说明',
		    BANKER_SOURCE_COMPANY => '公司资源',
		);
    }

    public function getOrgTypeShort()
    {
        return array(
            '1' => '银行',
            '2' => '小贷',
            '3' => '典当',
            '4' => '中介',
            '5' => '其他',
        );
    }


    //产品担保类型,由于历史原因产生了下面两个类似定义，每次修改请一并完成
    //SEO要求改字 又多了一个类似定义
    static public function getGuaranteeType()
    {
        return array(
            GUARANTEE_DIYA      => '房屋抵押',
            GUARANTEE_CAR       => '车辆抵押',
            GUARANTEE_OTHER     => '特殊抵押物',
            GUARANTEE_XINYONG   => '纯信用',
            GUARANTEE_DANBAO    => '担保贷',
            GUARANTEE_XINYONGKA => '信用卡',
        );
    }
    public function getProductGuaranteeType()
    {
        return array(
            GUARANTEE_DIYA      => '房屋抵押',
            GUARANTEE_CAR       => '车辆抵押',
            GUARANTEE_OTHER     => '特殊抵押物',
            GUARANTEE_XINYONG   => '信用类',
            GUARANTEE_DANBAO    => '担保类',
            GUARANTEE_XINYONGKA => '信用卡',
        );
    }
    public function getSeoGuaranteeType()
    {
        return array(
            GUARANTEE_DIYA      => '房产抵押',
            GUARANTEE_CAR       => '汽车抵押',
            GUARANTEE_OTHER     => '特殊抵押物',
            GUARANTEE_DANBAO    => '担保贷',
            GUARANTEE_XINYONGKA => '信用卡',
        );
    }
    ////////////////////////////////////////////////////////////////////////


    //产品附加图标
    public function getProductAddIcon()
    {
        return array(
            '1' => '热门图标',
        );
    }
    //产品类型
    public static function getProductType()
    {
        return array(
            '1' => '经营类',
            '6' => '消费类',
            '2' => '大额分期',
            '4' => '企业类',
            '3' => '车贷',
            '5' => '房屋按揭',
        );
    }
    //个人产品类型
    public static function getPrivateProductType()
    {
        return array(
            PRODUCT_PRIVATE_LOAN    => '普通贷款',
            PRODUCT_PRIVATE_CAR     => '购车贷款',
            PRODUCT_PRIVATE_HOUSE   => '购房贷款',
        );
    }
    //个人产品服务类型
    public static function getProductServiceType()
    {
        return array(
            SERVICE_TYPE_LOAN   => '出借服务',
            SERVICE_TYPE_AGENT  => '中介服务',
            SERVICE_TYPE_GUA    => '担保服务',
        );
    }

    //产品还款方式
    public function getProductRepaymentType()
    {
        return array(
            '1' => '分期还款',
            '2' => '到期还款',
            '3' => '循环授信,随借随还',
        );
    }

    //接受的用途证明
    public function getAcceptUseProof()
    {
        return array(
            '1' => '合同',
            '2' => '发票原件',
            '3' => '发票复印件',
            '4' => '其它',
        );
    }
    //排序方法
    public function getSortType()
    {
        return array(
            '1' => '默认排序',
            '2' => '成功率',
            '3' => '利率',
            '4' => '放款速度',
        );
    }

    //车贷--车型要求
    public function getChedaiModelsRequirements()
    {
        return array(
            '1' => '支持所有车型',
            '2' => '只支持轿车',
            '3' => '只支持部分品牌车型',
        );
    }

    //车贷--用途要求
    public function getChedaiUseRequirements()
    {
        return array(
            '1' => '家用、商用都可以',
            '2' => '只支持家用车',
            '3' => '只支持商用车',
        );
    }

    //车贷--用途要求
    public function getChedaiUseRequirement()
    {
        return array(
            '1' => '家用',
            '2' => '运营',
        );
    }
    
    //车贷--新旧
    public function getChedaiCondition()
    {
        return array(
            '1' => '新车',
            '2' => '二手车',
        );
    }
	//车牌--本地/外地
	public function getChedaiLicensePlace(){
		return array(
			'1'=>'本地',
			'2'=>'外地'
		);
	}
    
    //车贷--付款比例
    public function getChedaiPaymentRatio()
    {
        return array(
            '10' => '10%',
            '20' => '20%',
            '30' => '30%',
            '40' => '40%',
            '50' => '50%',
            '60' => '60%',
        );
    }

    
    //车贷--新版开通城市
    public function getChedaiNewOpenCitys()
    {
        // return array(1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,18,19,20,23,24,27,29,30,31,32,33,36,37,48,49,51,52,54,56,58,63,72,90,96,99,110,115,143,164,165,168,187,190,193,198,200,223,233,234,236,237,245,246,259,261,270,331,326,263,87);
        //2014-12-22  通用车贷合作暂停
        return array();
    }

    //房贷--房屋性质要求
    public function getFangdaiHouseNature()
    {
        return array(
            '1' => '住宅和商业房产都做',
            '2' => '只做住宅',
            '3' => '只做商业房产',
        );
    }

    //基准利率
    static public function getBaseInterestRate()
    {
        return array(
            '1' => array('term'=> array(0,6),'rate' => 4.35),
            '2' => array('term'=> array(6,12),'rate' => 4.35),
            '3' => array('term'=> array(12,36),'rate' => 4.75),
            '4' => array('term'=> array(36,60),'rate' => 4.75),
            '5' => array('term'=> array(60,1000),'rate' => 4.90),
        );
    }
    
    //利率更新时间
    static public function getRateUpdataDate()
    {
        return "1445616000";  //2015-10-24
    }

    public static function getFangBaseRate(){
        return array(
            array('term' =>'5~30年','comm' => '4.90%','gjj' => '3.25%'),
            array('term' =>'3~5(含)年','comm' => '4.75%','gjj' => '2.75%'),
            array('term' =>'1~3(含)年','comm' => '4.75%','gjj' => '2.75%'),
            array('term' =>'1年','comm' => '4.35%','gjj' => '2.75%'),
            array('term' =>'6个月','comm' => '4.35%','gjj' => '2.75%'),
        );
    }
	
	//公积金基准利率
    static public function getGjjBaseInterestRate()
    {
        return array(
            '1' => array('term'=> array(0,60),'rate' => 2.75),
            '2' => array('term'=> array(60,1000),'rate' => 3.25)
        );
    }

    public function getController()
    {
        return array(
            'site'      => array('index','daikuan'),
            'sitemap'   => array('beijing','shanghai'),
            'search'    => array('index','huodong'),
            'jingying'  => array('index'),
            'xiaofei'   => array('index'),
            'chedai'    => array('index','newchedaientry'),
            'fangdai'   => array('index','search'),
        );
    }

    //手工设置权重列表
    public function getWeightList()
    {
        return array(1=>1, 2=>2, 3=>3, 4=>4, 5=>5, 6=>6, 7=>7, 8=>8, 9=>9,10=>10);
    }

    //机构是否内部测试配置
    public function getIsTestList()
    {
        return array(0=>"线上机构", 1=>"内部测试");
    }

    public function getChargeMode()
    {
        return array(
            CHARGE_METHOD_XIAOGUO => '按效果收费',
            CHARGE_METHOD_LIZI => '按例子付费(含竞价)',
            CHARGE_METHOD_FREE => '免费',
            CHARGE_METHOD_SPECIAL => '特殊付费',
        );
    }
    
     public function getSimpleChargeMode()
	{
		return array(
		    CHARGE_METHOD_XIAOGUO   => '效果',
			CHARGE_METHOD_LIZI      => '例子',
			CHARGE_METHOD_FREE      => '免费',
            CHARGE_METHOD_SPECIAL   => '特殊',
		);
	}
	
    static public function getStrApplyType($intApplyType)
    {
        $strApplyType = '';
        switch ($intApplyType)
        {
            case APPLY_TYPE_JINGYING:
                $strApplyType = 'operation';
                break;
            case APPLY_TYPE_XIAOFEI:
                $strApplyType = 'consump';
                break;
            case APPLY_TYPE_CHEDAI:
                $strApplyType = 'car';
                break;
            case APPLY_TYPE_FANGDAI:
                $strApplyType = 'house';
                break;
            case APPLY_TYPE_OTHER:
                $strApplyType = 'other';
                break;
            case APPLY_TYPE_NORMAL:
                $strApplyType = 'normal';
                break;
            case APPLY_TYPE_GUWEN:
                $strApplyType = 'guwen';
        }
        return $strApplyType;
    }

    static public function getQaskConfig()
    {
        return array(
            'user_income_by_card'   => array(
                'display' => 1,
                'data' => array(
                    array("desc" => "只发现金", "value" => "0"),
                    array("desc" => "1500以下", "value" => "1"),
                    array("desc" => "1500元","value" => "1500"),
                    array("desc" => "2000元","value" => "2000"),
                    array("desc" => "2500元","value" => "2500"),
                    array("desc" => "3000元","value" => "3000"),
                    array("desc" => "3500元","value" => "3500"),
                    array("desc" => "4000元","value" => "4000"),
                    array("desc" => "4500元","value" => "4500"),
                    array("desc" => "5000元","value" => "5000"),
                    array("desc" => "6000元","value" => "6000"),
                    array("desc" => "7000元","value" => "7000"),
                    array("desc" => "8000元","value" => "8000"),
                    array("desc" => "9000元","value" => "9000"),
                    array("desc" => "10000元","value" => "10000"),
                    array("desc" => "12000元","value" => "12000"),
                    array("desc" => "15000元","value" => "15000"),
                    array("desc" => "18000元","value" => "18000"),
                    array("desc" => "20000元","value" => "20000"),
                    array("desc" => "20000以上","value" => "30000"),
                ),
            ),
            'com_month_flow'        => array(
                'display' => 1,
                'data' => array(
                    array("desc" => "无","value" => "0"),
                    array("desc" => "2万以下","value" => "1"),
                    array("desc" => "2万","value" => "2"),
                    array("desc" => "3万","value" => "3"),
                    array("desc" => "4万","value" => "4"),
                    array("desc" => "5万","value" => "5"),
                    array("desc" => "6万","value" => "6"),
                    array("desc" => "7万","value" => "7"),
                    array("desc" => "8万","value" => "8"),
                    array("desc" => "9万","value" => "9"),
                    array("desc" => "10万","value" => "10"),
                    array("desc" => "11-20万","value" => "20"),
                    array("desc" => "21-30万","value" => "30"),
                    array("desc" => "31-50万","value" => "50"),
                    array("desc" => "51-100万","value" => "100"),
                    array("desc" => "101-200万","value" => "200"),
                    array("desc" => "201-300万","value" => "300"),
                    array("desc" => "301-400万","value" => "400"),
                    array("desc" => "401-500万","value" => "500"),
                    array("desc" => "500万以上","value" => "600"),
                ),
            ),
            'com_taxable_income'    => array(
                'display' => 1,
                'data' => array(
                    array("desc" => "无","value" => "0"),
                    array("desc" => "2万以下","value" => "1"),
                    array("desc" => "2万","value" => "2"),
                    array("desc" => "3万","value" => "3"),
                    array("desc" => "4万","value" => "4"),
                    array("desc" => "5万","value" => "5"),
                    array("desc" => "6万","value" => "6"),
                    array("desc" => "7万","value" => "7"),
                    array("desc" => "8万","value" => "8"),
                    array("desc" => "9万","value" => "9"),
                    array("desc" => "10万","value" => "10"),
                    array("desc" => "11-20万","value" => "20"),
                    array("desc" => "21-30万","value" => "30"),
                    array("desc" => "31-50万","value" => "50"),
                    array("desc" => "51-100万","value" => "100"),
                    array("desc" => "101-200万","value" => "200"),
                    array("desc" => "201-300万","value" => "300"),
                    array("desc" => "301-400万","value" => "400"),
                    array("desc" => "401-500万","value" => "500"),
                    array("desc" => "500万以上","value" => "600"),
                ),
            ),
            'com_register_fund'     => array(
                'display' => 1,
                'data' => array(
                    array("desc" => "3万以下","value" => "0"),
                    array("desc" => "3万","value" => "3"),
                    array("desc" => "5万","value" => "5"),
                    array("desc" => "10万","value" => "10"),
                    array("desc" => "30万","value" => "30"),
                    array("desc" => "50万","value" => "50"),
                    array("desc" => "100万","value" => "100"),
                    array("desc" => "200万","value" => "200"),
                    array("desc" => "300万","value" => "300"),
                    array("desc" => "500万","value" => "500"),
                    array("desc" => "800万","value" => "800"),
                    array("desc" => "1000万以上","value" => "2000"),
                ),
            ),
            /*
            'col_value'     => array(
                'display' => 1,
                'data' => array(
                    array("desc" => "100万以下","value" => "100"),
                    array("desc" => "100～199万","value" => "200"),
                    array("desc" => "200万及以上","value" => "250"),
                ),
            ),*/
            'house_family_income'     => array(
                'display' => 1,
                'data' => array(
                    array("desc" => "≤2000","value" => "2000"),
                    array("desc" => "2000~3000","value" => "3000"),
                    array("desc" => "3000~4000","value" => "4000"),
                    array("desc" => "4000~5000","value" => "5000"),
                    array("desc" => "5000~8000","value" => "8000"),
                    array("desc" => "8000~10000","value" => "10000"),
                    array("desc" => "10000~15000","value" => "15000"),
                    array("desc" => "≥15000","value" => "20000"),
                ),
            ),
            'operate_days'      => array(
                'display' => 1,
                'data' => array(
                    array("desc" => "1至3天","value" => "3"),
                    array("desc" => "1周之内","value" => "7"),
                    array("desc" => "2周之内","value" => "14"),
                    array("desc" => "1个月之内","value" => "30"),
                    array("desc" => "3个月之内","value" => "90"),
                ),
            ),
            'cash_receipts' => array(
                'display' => 1,
                'data' => array(
                    array("desc" => "不发现金", "value" => "0"),
                    array("desc" => "2000以下", "value" => "1"),
                    array("desc" => "2000元","value" => "2000"),
                    array("desc" => "3000元","value" => "3000"),
                    array("desc" => "4000元","value" => "4000"),
                    array("desc" => "5000元","value" => "5000"),
                    array("desc" => "6000元","value" => "6000"),
                    array("desc" => "8000元","value" => "8000"),
                    array("desc" => "10000元","value" => "10000"),
                    array("desc" => "15000元","value" => "15000"),
                    array("desc" => "15000以上","value" => "20000"),
                )
            ),
            'cash_settlement' => array(
                'display' => 1,
                'data' => array(
                    array("desc" => "无","value" => "0"),
                    array("desc" => "2万以下","value" => "1"),
                    array("desc" => "2万","value" => "2"),
                    array("desc" => "3万","value" => "3"),
                    array("desc" => "4万","value" => "4"),
                    array("desc" => "5万","value" => "5"),
                    array("desc" => "6万","value" => "6"),
                    array("desc" => "7万","value" => "7"),
                    array("desc" => "8万","value" => "8"),
                    array("desc" => "9万","value" => "9"),
                    array("desc" => "10万","value" => "10"),
                    array("desc" => "11-20万","value" => "20"),
                    array("desc" => "21-30万","value" => "30"),
                    array("desc" => "31-50万","value" => "50"),
                    array("desc" => "51-100万","value" => "100"),
                    array("desc" => "101-200万","value" => "200"),
                    array("desc" => "201-300万","value" => "300"),
                    array("desc" => "301-400万","value" => "400"),
                    array("desc" => "401-500万","value" => "500"),
                    array("desc" => "500万以上","value" => "600"),
                ),
            ),
        );
    }

    static public function getCoverQaskConfig()
    {
        return array(
            'qid44'=>array(
                'display' => 1,
                'data' => array(
                    array("desc" => "不要求","value" => "0"),
                    array("desc" => "3个月","value" => "3"),
                    array("desc" => "6个月","value" => "6"),
                    array("desc" => "9个月","value" => "9"),
                    array("desc" => "12个月","value" => "12"),
                    array("desc" => "12个月及以上","value" => "18")
                )
            ),
            'qid99'=>array(
                'display' => 1,
                'data' => array(
                    array("desc" => "不要求","value" => "0"),
                    array("desc" => "3个月","value" => "3"),
                    array("desc" => "6个月","value" => "6"),
                    array("desc" => "9个月","value" => "9"),
                    array("desc" => "12个月","value" => "12"),
                    array("desc" => "12个月及以上","value" => "18")
                )
            ),
            'qid79'=>array(
                'display' => 1,
                'data' => array(
                    array("desc" => "不要求","value" => "0"),
                    array("desc" => "1000及以上","value" => "1000"),
                    array("desc" => "2000及以上","value" => "2000"),
                    array("desc" => "3000及以上","value" => "3000"),
                    array("desc" => "4000及以上","value" => "4000"),
                    array("desc" => "5000及以上","value" => "5000"),
                    array("desc" => "6000及以上","value" => "6000"),
                    array("desc" => "7000及以上","value" => "7000"),
                    array("desc" => "8000及以上","value" => "8000"),
                    array("desc" => "9000及以上","value" => "9000"),
                    array("desc" => "1万及以上","value" => "10000"),
                    array("desc" => "2万及以上","value" => "10000"),
                    array("desc" => "3万及以上","value" => "30000"),
                    array("desc" => "4万及以上","value" => "40000"),
                    array("desc" => "5万及以上","value" => "50000"),
                    array("desc" => "6万及以上","value" => "60000"),
                    array("desc" => "7万及以上","value" => "70000"),
                    array("desc" => "8万及以上","value" => "80000"),
                    array("desc" => "9万及以上","value" => "90000"),
                    array("desc" => "10万及以上","value" => "10000"),
                )
            ),
            'qid159'=>array(
                'display' => 1,
                'data' => array(
                    array("desc" => "6个月以下","value" => "0"),
                    array("desc" => "6个月","value" => "6"),
                    array("desc" => "12个月","value" => "12"),
                    array("desc" => "24个月","value" => "24"),
                    array("desc" => "36个月","value" => "36"),
                    array("desc" => "36个月以上","value" => "48")
                )
            ),
            'qid161'=>array(
                'display' => 1,
                'data' => array(
                    array("desc" => "50","value" => "50"),
                    array("desc" => "100","value" => "100"),
                    array("desc" => "150","value" => "150"),
                    array("desc" => "200","value" => "200"),
                    array("desc" => "250","value" => "250"),
                    array("desc" => "300","value" => "300"),
                    array("desc" => "350","value" => "350"),
                    array("desc" => "400","value" => "400"),
                    array("desc" => "450","value" => "450"),
                    array("desc" => "500","value" => "500")
                )
            ),
            'qid162'=>array(
                'display' => 1,
                'data' => array(
                    array("desc" => "1万及以下","value" => "0"),
                    array("desc" => "1万","value" => "10000"),
                    array("desc" => "2万","value" => "20000"),
                    array("desc" => "3万","value" => "30000"),
                    array("desc" => "4万","value" => "40000"),
                    array("desc" => "5万","value" => "50000"),
                    array("desc" => "6万","value" => "60000"),
                    array("desc" => "7万","value" => "70000"),
                    array("desc" => "8万","value" => "80000"),
                    array("desc" => "9万","value" => "90000"),
                    array("desc" => "10万","value" => "100000"),
                    array("desc" => "15万","value" => "150000"),
                    array("desc" => "20万","value" => "200000"),
                    array("desc" => "25万","value" => "250000"),
                    array("desc" => "30万","value" => "300000"),
                    array("desc" => "35万","value" => "350000"),
                    array("desc" => "40万","value" => "400000"),
                    array("desc" => "45万","value" => "450000"),
                    array("desc" => "50万","value" => "500000"),
                    array("desc" => "60万","value" => "600000"),
                    array("desc" => "70万","value" => "700000"),
                    array("desc" => "80万","value" => "800000"),
                    array("desc" => "90万","value" => "900000"),
                    array("desc" => "100万","value" => "1000000"),
                    array("desc" => "100万及以上","value" => "1500000")
                )
            ),
            'col_value'=>array(
                'display' => 1,
                'data' => array(
                    array("desc" => "10万及以下","value" => "5"),
                    array("desc" => "10万","value" => "10"),
                    array("desc" => "20万","value" => "20"),
                    array("desc" => "30万","value" => "30"),
                    array("desc" => "40万","value" => "40"),
                    array("desc" => "50万","value" => "50"),
                    array("desc" => "60万","value" => "60"),
                    array("desc" => "70万","value" => "70"),
                    array("desc" => "80万","value" => "80"),
                    array("desc" => "90万","value" => "90"),
                    array("desc" => "100万","value" => "100"),
                    array("desc" => "110万","value" => "110"),
                    array("desc" => "120万","value" => "120"),
                    array("desc" => "130万","value" => "130"),
                    array("desc" => "140万","value" => "140"),
                    array("desc" => "150万","value" => "150"),
                    array("desc" => "160万","value" => "160"),
                    array("desc" => "170万","value" => "170"),
                    array("desc" => "180万","value" => "180"),
                    array("desc" => "190万","value" => "190"),
                    array("desc" => "200万","value" => "200"),
                    array("desc" => "200万及以上","value" => "210"),
                )
            ),
            'car_value'=>array(
                'display' => 1,
                'data' => array(
                    array("desc" => "3万及以下","value" => "2"),
                    array("desc" => "3万","value" => "3"),
                    array("desc" => "4万","value" => "4"),
                    array("desc" => "5万","value" => "5"),
                    array("desc" => "6万","value" => "6"),
                    array("desc" => "7万","value" => "7"),
                    array("desc" => "8万","value" => "8"),
                    array("desc" => "9万","value" => "9"),
                    array("desc" => "10万","value" => "10"),
                    array("desc" => "11万","value" => "11"),
                    array("desc" => "12万","value" => "12"),
                    array("desc" => "13万","value" => "13"),
                    array("desc" => "14万","value" => "14"),
                    array("desc" => "15万","value" => "15"),
                    array("desc" => "16万","value" => "16"),
                    array("desc" => "17万","value" => "17"),
                    array("desc" => "18万","value" => "18"),
                    array("desc" => "19万","value" => "19"),
                    array("desc" => "20万","value" => "20"),
                    array("desc" => "20万以上","value" => "22"),

                )
            ),
            'qid216'=>array(
                'display' => 1,
                'data' => array(
                    array("desc" => "3万及以下","value" => "2"),
                    array("desc" => "3万","value" => "3"),
                    array("desc" => "4万","value" => "4"),
                    array("desc" => "5万","value" => "5"),
                    array("desc" => "6万","value" => "6"),
                    array("desc" => "7万","value" => "7"),
                    array("desc" => "8万","value" => "8"),
                    array("desc" => "9万","value" => "9"),
                    array("desc" => "10万","value" => "10"),
                    array("desc" => "11万","value" => "11"),
                    array("desc" => "12万","value" => "12"),
                    array("desc" => "13万","value" => "13"),
                    array("desc" => "14万","value" => "14"),
                    array("desc" => "15万","value" => "15"),
                    array("desc" => "16万","value" => "16"),
                    array("desc" => "17万","value" => "17"),
                    array("desc" => "18万","value" => "18"),
                    array("desc" => "19万","value" => "19"),
                    array("desc" => "20万","value" => "20"),
                    array("desc" => "21万","value" => "21"),
                    array("desc" => "22万","value" => "22"),
                    array("desc" => "23万","value" => "23"),
                    array("desc" => "24万","value" => "24"),
                    array("desc" => "25万","value" => "25"),
                    array("desc" => "26万","value" => "26"),
                    array("desc" => "27万","value" => "27"),
                    array("desc" => "28万","value" => "28"),
                    array("desc" => "29万","value" => "29"),
                    array("desc" => "30万","value" => "30"),
                    array("desc" => "30万以上","value" => "40"),

                )
            ),
        );
    }
    
    //产品特点
    static public function getProductSpecialFeature()
    {
        //type => 1:简单类型 2:复杂类型
        //icon name rule: sm_n.png m:key n:product input
        return array(
            '100' => array(//最高n成
                'type'      => 2,
                'name'      => '抵押成数高',
                'default'   => 85,
                'child'     => array(
                    '80'    => array(
                        'weight'=> 0.9,
                        'name'  => '最高8成',
                        'desc'  => '放款额度最高可达抵押物价值的8成',
                    ),
                    '85'    => array(
                        'weight'=> 0.8,
                        'name'  => '最高8.5成',
                        'desc'  => '放款额度最高可达抵押物价值的8.5成',
                    ),
                    '100'   => array(
                        'weight'=> 0.7,
                        'name'  => '最高10成',
                        'desc'  => '放款额度最高可达抵押物价值的10成',
                    ),
                ),
            ),
            '200' => array(//n年授信
                'type'      => 2,
                'name'      => '授信产品',
                'default'   => 2,
                'child'     => array(
                    '2'     => array(
                        'weight'=> 0.9,
                        'name'  => '2年授信',
                        'desc'  => '最长可以获得2年授信，在授信额度内可反复多次使用贷款',
                    ),
                    '3'     => array(
                        'weight'=> 0.7,
                        'name'  => '3年授信',
                        'desc'  => '最长可以获得3年授信，在授信额度内可反复多次使用贷款',
                    ),
                    '5'     => array(
                        'weight'=> 0.7,
                        'name'  => '5年授信',
                        'desc'  => '最长可以获得5年授信，在授信额度内可反复多次使用贷款',
                    ),
                    '10'    => array(
                        'weight'=> 0.7,
                        'name'  => '10年授信',
                        'desc'  => '最长可以获得10年授信，在授信额度内可反复多次使用贷款',
                    ),
                ),
            ),
            '300' => array(//二次抵押
                'type'      => 1,
                'name'      => '可二次抵押',
                'desc'      => '不必先还清以前的贷款，可利用剩余价值再次抵押',
                'weight'    => 0.7,
            ),
            '400' => array(//放款快速
                'type'      => 2,
                'name'      => '放款快速',
                'default'   => 2,
                'child'     => array(
                    '0'     => array(
                        'weight'=> 0.6,
                        'name'  => '最快当天放款',
                        'desc'  => '放款快，最短1个工作日放款',
                    ),
                    '2'     => array(
                        'weight'=> 0.7,
                        'name'  => '最快2天放款',
                        'desc'  => '放款快，最短2个工作日放款',
                    ),
                    '7'     => array(
                        'weight'=> 0.7,
                        'name'  => '最快7天放款',
                        'desc'  => '放款快，最短7个工作日放款',
                    ),
                    '15'     => array(
                        'weight'=> 0.9,
                        'name'  => '最快15天放款',
                        'desc'  => '放款快，最短15个工作日放款',
                    ),
                ),
            ),
            '500' => array(//条件宽松
                'type'      => 1,
                'name'      => '条件宽松',
                'desc'      => '审批条件相对宽松',
                'weight'    => 0.6,
            ),
            '600' => array(//可无信用记录
                'type'      => 1,
                'name'      => '接受信用空白人士申请',
                'desc'      => '没有贷款经历及信用卡使用经历的客户也可以申请本贷款',
                'weight'    => 0.7,
            ),
            '640' => array(//
                'type'      => 1,
                'name'      => '手续简便',
                'desc'      => '所需材料简便，无需繁琐流程',
                'weight'    => 0.8,
            ),
            '650' => array(//
                'type'      => 1,
                'name'      => '可上门服务',
                'desc'      => '信贷员上门服务，省时方便',
                'weight'    => 0.9,
            ),
            '700' => array(//特定人群
                'type'      => 2,
                'name'      => '仅特定人群',
                'weight'    => 2.4,
                'desc'      => '本贷款仅提供给公务员、事业单位员工，及普通企业高层',
                'default'   => 1,
                'child'     => array(
                    '1'     => array(
                        'weight'=> 2.4,
                        'name'  => '要求较严格',
                        'desc'  => '本贷款仅提供给公务员、事业单位员工，及普通企业高层',
                    ),
                    '2'     => array(
                        'weight'=> 1.2,
                        'name'  => '要求较宽松',
                        'desc'  => '本贷款仅提供给白领人士',
                    ),
                ),
            ),
            '800' => array(//工资要求高
                'type'      => 2,
                'name'      => '工资要求高',
                'default'   => 40,
                'child'     => array(
                    '40'     => array(
                        'weight'=> 1.2,
                        'name'  => '工资要求4千',
                        'desc'  => '申请人工资卡上每月收入需超过4千元',
                    ),
                    '50'     => array(
                        'weight'=> 1.3,
                        'name'  => '工资要求5千',
                        'desc'  => '申请人工资卡上每月收入需超过5千元',
                    ),
                    '60'     => array(
                        'weight'=> 1.4,
                        'name'  => '工资要求6千',
                        'desc'  => '申请人工资卡上每月收入需超过6千元',
                    ),
                ),
            ),
            '900' => array(//本地房产
                'type'  => 1,
                'name'  => '需本地房产',
                'desc'  => '申请人需已经在本地购买房产，并提供证明(无需抵押)',
                'weight'=> 1.1,
            ),
            '1000' => array(//流水要求高
                'type'      => 2,
                'name'      => '流水要求高',
                'default'   => 10,
                'child'     => array(
                    '10'     => array(
                        'weight'=> 1.2,
                        'name'  => '月流水10万',
                        'desc'  => '申请人平均每月银行卡上经营流水收入需高于10万',
                    ),
                    '20'     => array(
                        'weight'=> 1.2,
                        'name'  => '月流水20万',
                        'desc'  => '申请人平均每月银行卡上经营流水收入需高于20万',
                    ),
                    '30'     => array(
                        'weight'=> 1.2,
                        'name'  => '月流水30万',
                        'desc'  => '申请人平均每月银行卡上经营流水收入需高于30万',
                    ),
                    '50'     => array(
                        'weight'=> 1.2,
                        'name'  => '月流水50万',
                        'desc'  => '申请人平均每月银行卡上经营流水收入需高于50万',
                    ),
                ),
            ),
            '1100' => array(//需要担保
                'type'      => 1,
                'name'      => '需担保',
                'desc'      => '申请人需有第三方提供担保',
                'weight'    => 1.4,
            ),
            '1200' => array(//需要互保
                'type'      => 1,
                'name'      => '需互相担保',
                'desc'      => '需同商圈3名以上经营者互相担保，同时获得贷款',
                'weight'    => 1,
            ),
            '1300' => array(//须房屋按揭记录
                'type'      => 1,
                'name'      => '须房屋按揭记录',
                'desc'      => '申请人现在（或近1年内）名下必须有房贷按揭或者房产抵押贷款',
                'weight'    => 1,
            ),
            '1400' => array(//须证券质押
                'type'      => 1,
                'name'      => '须证券质押',
                'desc'      => '需要提供银行指定的质押物',
                'weight'    => 1.2,
            ),
            '1600' => array(//支持二手车
                'type'      => 1,
                'name'      => '支持二手车',
                'desc'      => '购买二手车也可以贷款',
                'weight'    => 0.9,
            ),
            '1610' => array(//
                'type'      => 1,
                'name'      => '本地牌照',
                'desc'      => '仅限本地牌照申请',
                'weight'    => 1.2,
            ),
            '1620' => array(//
                'type'      => 1,
                'name'      => '不支持二手车',
                'desc'      => '仅限新车贷款',
                'weight'    => 1.1,
            ),
            '1630' => array(//
                'type'      => 1,
                'name'      => '支持公司牌照',
                'desc'      => '公司牌照可以申请',
                'weight'    => 0.9,
            ),
            '1700' => array(//
                'type'      => 1,
                'name'      => '网络贷款',
                'desc'      => '该贷款为网络P2P贷款，提供网络自助贷款服务',
                'weight'    => 1,
            ),
			'1800' => array(
					'type'	=>1,
					'name'	=>'非金融机构',
					'desc'	=>'非银行类、小贷公司、典当行等无金融业务许可证的机构，如担保公司、中介、P2P',
					'weight'=>1
					),
			'1900'	=> array(
					'type'	=>	1,
					'name'	=>'信用要求松',
					'desc'	=>'接受连续信用逾期，接受当前逾期',
					'weight'	=>1
					),
			'1910'	=>	array(
					'type'	=> 1,
					'name'	=>'信用要求严',
					'desc'	=>'不接受当前逾期，不接受连续信用逾期，对逾期记录次数容忍度低',
					'weight'	=>1
					),
			'2000'	=>array(
					'type'=>1,
					'name'=>'有房即可贷',
					'desc'=>'凭本人名下房产就可以申请贷款',
					'weight'=>1
					),
			'2010'	=>array(
					'type'=>2,
					'name'=>'房龄要求高',
					'default'   => 15,
					'child'=>array(
						'10'=>array(
							'name'=>'小于10年',
							'desc'=>'房龄最高不超过10年'
							),
						'15'=>array(
							'name'=>'小于15年',
							'desc'=>'房龄最高不超过15年'
							)
						)
					),
			'2020'=>array(
				'type'=>2,
				'name'=>'面积要求高',
				'default'   => 70,
				'child'=>array(
					'70'=>array(
						'name'=>'不低于70平米',
						'desc'=>'房产面积不低于70平米'
					),
					'100'=>array(
						'name'=>'不低于100平米',
						'desc'=>'房产面积不低于100平米'
					),
					'120'=>array(
						'name'=>'不低于120平米',
						'desc'=>'房产面积不低于120平米'
					)
				)
			),
			'2030'=>array(
				'type'=>2,
				'name'=>'房价要求高',
				'default'   => 80,
				'child'=>array(
					'80'=>array(
						'name'=>'不低于80万',
						'desc'=>'房产价值不低于80万'
					),
					'100'=>array(
						'name'=>'不低于100万',
						'desc'=>'房产价值不低于100万'
					),
					'120'=>array(
						'name'=>'不低于120万',
						'desc'=>'房产价值不低于120万'
					)
				)
			),
			'2040'=>array(
				'type'=>1,
				'name'=>'不接受二抵',
				'desc'=>'房产的按揭或抵押贷款已结清'
			),
			'2050'=>array(
				'type'=>1,
				'name'=>'房产要求严',
				'desc'=>'房龄、面积、价值要求严格'
			),
			'2060'=>array(
				'type'=>'1',
				'name'=>'限中心区域',
				'desc'=>'仅接受城中心区域的房产为抵押物（具体区域根据不同城市变化）'
			),
			'2070'=>array(
				'type'=>'1',
				'name'=>'指定区域',
				'desc'=>'仅接受某一特定区域的房产为抵押物'
			),
			'2080'=>array(
				'type'=>'1',
				'name'=>'仅接受新房',
				'desc'=>'只接受新房'
			),
			'2090'=>array(
				'type'=>'1',
				'name'=>'限二手房',
				'desc'=>'只接受二手房申请，不接受新房'
			),
			'2100'=>array(
				'type'=>1,
				'name'=>'要求备用房',
				'desc'=>'除抵押房产外，必须提供另一套房产为备用房'
			),
			'2200'=>array(
				'type'=>2,
				'name'=>'车龄要求高',
				'default'=>'5',
				'child'=>array(
					'3'=>array(
						'name'=>'不超过3年',
						'desc'=>'新车或车龄最高不超过3年'
					),
					'5'=>array(
						'name'=>'不超过5年',
						'desc'=>'新车或车龄最高不超过5年'
					)
				)
			),
			'2210'=>array(
				'type'=>2,
				'name'=>'车价要求高',
				'default'=>8,
				'child'=>array(
					'8'=>array(
						'name'=>'最低8万',
						'desc'=>'购车价格最低8万（仅裸车价格，不含税费和保险）'
					),
					'10'=>array(
						'name'=>'最低10万',
						'desc'=>'购车价格最低10万（仅裸车价格，不含税费和保险）'
					),
					'15'=>array(
						'name'=>'最低15万',
						'desc'=>'购车价格最低15万（仅裸车价格，不含税费和保险）'
					)
				)
			),
			'2300'=>array(
				'type'=>2,
				'name'=>'放款速度慢',
				'default'=>10,
				'child'=>array(
					'10'=>array(
						'name'=>'10天以上',
						'desc'=>'完成放款时间需10天以上'
					),
					'20'=>array(
						'name'=>'20天以上',
						'desc'=>'完成放款时间需20天以上'
					)
				)
			),
			'2310'=>array(
				'type'=>1,
				'name'=>'手续繁琐',
				'desc'=>'需提供多项资料，手续繁琐'
			),
			'2320'=>array(
				'type'=>1,
				'name'=>'需上门查实',
				'desc'=>'信贷员需上门查看工作单位和企业经营状况'
			),
			'2330'=>array(
				'type'=>'1',
				'name'=>'需本地户籍',
				'desc'=>'申请人必须为本地户籍'
			),
			'2340'=>array(
				'type'=>1,
				'name'=>'有行业限制',
				'desc'=>'不接受特定行业人群（具体行业可以让品控输入）的申请'
			),
			'2350'=>array(
				'type'=>1,
				'name'=>'不接受现金',
				'desc'=>'接受工资打卡发放或自己存入银行，不接受纯现金发放的收入'
        	),
			'2360'=>array(
				'type'=>1,
				'name'=>'需打卡工资',
				'desc'=>'只接受打入工资卡的工资发放形式'
        	),
			'2370'=>array(
				'type'=>2,
				'name'=>'高工作年限',
				'default'=>1,
				'child'=>array(
					'1'=>array(
						'name'=>'1年以上',
						'desc'=>'工作年限最低1年以上'
					),
					'2'=>array(
						'name'=>'2年以上',
						'desc'=>'工作年限最低2年以上'
					),
					'3'=>array(
						'name'=>'3年以上',
						'desc'=>'工作年限最低3年以上'
					)
				)
			),
			'2380'=>array(
				'type'=>1,
				'name'=>'需营业执照',
				'desc'=>'必须提供有效期内的营业执照'
			),
			'2390'=>array(
				'type'=>1,
				'name'=>'固定经营场所',
				'desc'=>'申请人必须要有固定的经营场所，能提供店铺或办公楼租赁合同'
			),
			'2400'=>array(
				'type'=>2,
				'name'=>'高经营年限',
				'default'=>3,
				'child'=>array(
					'3'=>array(
						'name'=>'3年以上',
						'desc'=>'经营年限最低3年以上'
					),
					'5'=>array(
						'name'=>'5年以上',
						'desc'=>'经营年限最低5年以上'
					)
				)
			),
			'2410'=>array(
				'type'=>2,
				'name'=>'高负债限制',
				'default'=>100,
				'child'=>array(
					'50'=>array(
						'name'=>'不超过50%',
						'desc'=>'申请人信用卡和贷款还款金额不得超过月收入的50%'
					),
					'70'=>array(
						'name'=>'不超过70%',
						'desc'=>'申请人信用卡和贷款还款金额不得超过月收入的70%'
					),
					'100'=>array(
						'name'=>'不超过100%',
						'desc'=>'申请人信用卡和贷款还款金额不得超过月收入的100%'
					)
				)
			)
		);
    }

    //详情页特点
    static public function getProductDetailFeature()
    {
        return array(
            '100' => array(//最高n成
                'type'      => 2,
                'name'      => '抵押成数高',
                'default'   => 85,
                'child'     => array(
                    '70'    => array(
                        'name'  => '可抵押7成',
                        'desc'  => '额度最高可达抵押物价值的7成',
                    ),
                    '80'    => array(
                        'name'  => '可抵押8成',
                        'desc'  => '额度最高可达抵押物价值的8成',
                    ),
                    '85'    => array(
                        'name'  => '可抵押8.5成',
                        'desc'  => '额度最高可达抵押物价值的8.5成',
                    ),
                    '100'   => array(
                        'name'  => '可抵押10成',
                        'desc'  => '额度最高可达抵押物价值的10成',
                    ),
                ),
            ),
            '200' => array(//n年授信
                'type'      => 2,
                'name'      => '授信产品',
                'default'   => 2,
                'child'     => array(
                    '2'     => array(
                        'name'  => '2年授信',
                        'desc'  => '在授信额度内可循环多次贷款',
                    ),
                    '3'     => array(
                        'name'  => '3年授信',
                        'desc'  => '在授信额度内可循环多次贷款',
                    ),
                    '5'     => array(
                        'name'  => '5年授信',
                        'desc'  => '在授信额度内可循环多次贷款',
                    ),
                    '10'    => array(
                        'name'  => '10年授信',
                        'desc'  => '在授信额度内可循环多次贷款',
                    ),
                ),
            ),
            '300' => array(//二次抵押
                'type'      => 1,
                'name'      => '可二次抵押',
                'desc'      => '可用抵押物剩余价值再次抵押',
            ),
            '400' => array(//放款快速
                'type'      => 2,
                'name'      => '放款快速',
                'default'   => 2,
                'child'     => array(
                    '0'     => array(
                        'name'  => '最快当天放款',
                        'desc'  => '放款快，最短1个工作日放款',
                    ),
                    '2'     => array(
                        'name'  => '最快2天放款',
                        'desc'  => '放款快，最短2个工作日放款',
                    ),
                    '3'     => array(
                        'name'  => '最快3天放款',
                        'desc'  => '放款快，最短3个工作日放款',
                    ),
                    '7'     => array(
                        'name'  => '最快7天放款',
                        'desc'  => '放款快，最短7个工作日放款',
                    ),
                    '15'     => array(
                        'name'  => '最快15天放款',
                        'desc'  => '放款快，最短15个工作日放款',
                    ),
                    '50'     => array(
                        'name'  => '放款速度快',
                        'desc'  => '放款速度较快',
                    ),
                ),
            ),
            '500' => array(//条件宽松
                'type'      => 1,
                'name'      => '条件宽松',
                'desc'      => '审批条件相对宽松',
            ),
            '600' => array(//可无信用记录
                'type'      => 1,
                'name'      => '可信用空白',
                'desc'      => '接受信用空白人士申请',
            ),
            '620' => array(//
                'type'      => 1,
                'name'      => '利率低',
                'desc'      => '贷款利率较为低廉',
            ),
            '630' => array(//
                'type'      => 1,
                'name'      => '放款额度高',
                'desc'      => '放款额度较高，有效满足资金需求',
            ),
            '640' => array(//
                'type'      => 1,
                'name'      => '手续简便',
                'desc'      => '所需材料简便，无需繁琐流程',
            ),
            '650' => array(//
                'type'      => 1,
                'name'      => '可上门服务',
                'desc'      => '信贷员上门服务，省时方便',
            ),
            '660' => array(//
                'type'      => 1,
                'name'      => '无抵押、无担保',
                'desc'      => '信用贷款，无需抵押或担保',
            ),
            '665' => array(//
                'type'      => 1,
                'name'      => '可提前还款',
                'desc'      => '支持提前还款，需支付违约金',
            ),
            '666' => array(//
                'type'      => 1,
                'name'      => '用途多样',
                'desc'      => '支持多种用途，满足您不同需要',
            ),
        );
    }

	public static function applyFrom(){
		return array(
				"shopex"=>0,
				"edb"=>0,
				"yesky"=>1,
				"funshion"=>1,
				"uucun"=>1,
				"eshopun"=>1
		);
	}

    public static function getJingjiaRankingReason()
    {
        return array(
            JINGJIA_INVALID_REASON_NOSET            => '信贷员未出价',
            JINGJIA_INVALID_REASON_BANKER           => '信贷员出价为0',
            JINGJIA_INVALID_REASON_BALANCE          => '信贷员余额不足',
            JINGJIA_INVALID_REASON_CONSUME          => '消费超出上限',
            JINGJIA_INVALID_REASON_ORDERS           => '订单数超过上限',
            JINGJIA_INVALID_REASON_SAMEUSER         => '30天内相同用户申请',
            JINGJIA_INVALID_REASON_BANKER_DEL       => '信贷员下架',
            JINGJIA_INVALID_REASON_PRODUCT_DEL      => '信贷员产品下架',
            JINGJIA_INVALID_REASON_BANKER_MODE_ERR  => '信贷员产品模式不正确，不是竞价模式',
        );
    }

	public static function getZone()
	{
		return array(
		        '1'=>'华北' ,
				'2'=>'华东' ,
                '3'=>'华南' ,
                //'9'=>'快速开城',
				//'4'=>'半开通城' ,
				//'5'=>'捉贷' ,
                //'99'=>'未开通',
			);
	}

     /*
	 *add by zhangji ,城市的展示区域
	 */
	public static function getShowArea()
	{
		return array(
		    '1'=>'华北东北',
			'2'=>'华东地区',
			'3'=>'华南华中',
			'4'=>'西部地区',
			
		);
	}

    /**
     * mis 中特殊区域管理
     * */
    public static $mis_area_config = array(
        '1' => array(
            'name' => '华北',
            'province' => array(
                '北京',
                '天津',
                '河北',
                '辽宁',
                '山东',
                '河南',
            ),
        ),
        '2' => array(
            'name' => '华东',
            'province' => array(
                '江苏',
                '安徽',
                '浙江',
                '上海',
                '湖北',
            ),
        ),
        '3' => array(
            'name' => '华南',
            'province' => array(
                '广东',
                '湖南',
            ),
        ),
        '4' => array(
            'name' => '快速开城',
            'province' => array(
                '重庆',
                '内蒙古',
                '四川',
                '福建',
                '黑龙江',
                '海南',
                '安徽',
                '吉林',
                '云南',
                //'河南',
                '广西',
                '江西',
                '河北',
                '山西',
                //'湖北',
                '陕西',
            ),
        ),
        '5' => array(
            'name' => '其他',
            'province' => array(
                '贵州',
                '甘肃',
                '青海',
                '宁夏',
            ),
        ),
    );

	public static function getZoneWithFullInformation()
	{
		return array(
				'1'=> array('name' =>'华北', 'abbr' =>'huabei'),
				'2'=> array('name' =>'华东', 'abbr' =>'huadong'),
                '3'=> array('name' =>'华南', 'abbr' =>'huanan'),
                '9'=> array('name' =>'快速开城','abbr'=>'kuaisukaicheng'),
				'4'=> array('name' =>'半开通城', 'abbr' =>'baikaitongcheng'),
				'5'=> array('name' =>'捉贷', 'abbr' =>'zhuodai'),
		        '99'=>array('name' =>'未开通', 'abbr' => 'weikaitong')
		);
	}

    //房贷分期期限
    public static function getHouseTerms(){
        return array(
                "1"=>12,
                "2"=>24,
                "3"=>36,
                "4"=>48,
                "5"=>60,
                "10"=>120,
                "15"=>180,
                "20"=>240,
                "25"=>300,
                "30"=>360,
        );
    }

    //房贷选项

    //房屋类型
    public function getFangdaiHouseType()
    {
        return array(
            '1' =>  '商品房(>90㎡)',
            '2' =>  '商品房(≤90㎡)',
            '8' =>  '整体式商铺',
            '7' =>  '隔断式商铺',
            '9' =>  '商住两用房',
            '10' => '写字楼',
            '5' =>  '小产权房',
            '3' =>  '房改房',
            '4' =>  '经济适用房',
            '6' =>  '危改房',
        );
    }

    //交易性质
    public function getFangdaiTradeType()
    {
        return array(
            '1' =>  '新房',
            '2' =>  '二手房',
        );
    }

    //购买性质
    public function getFangdaiBuyType()
    {
        return array(
            '1' =>  '首套住宅',
            '2' =>  '二套住宅(及以上)',
        );
    }

    //房屋房龄
    public function getFangdaiHouseYears()
    {
        return array(
            '1' =>  '≤20年',
            '2' =>  '20~30年',
            '3' =>  '30~40年',
            '4' =>  '40~50年',
            '5' =>  '≥50年',
        );
    }

    //家庭月收入
    public function getFangdaiFamilyIncome()
    {
        return array(
            '1' =>  '≤2000',
            '2' =>  '2000~3000',
            '3' =>  '3000~4000',
            '4' =>  '4000~5000',
            '5' =>  '5000~8000',
            '6' =>  '8000~10000',
            '7' =>  '10000~15000',
            '8' =>  '≥15000',
        );
    }

    //房贷利率折扣
    public function getFangdaiRateDiscounts()
    {
        return array(
            '-30' =>  '7折',
            '-25' =>  '7.5折',
            '-20' =>  '8折',
            '-15' =>  '8.5折',
            '-10' =>  '9折',
            '-5' =>  '9.5折',
            '0' =>  '基准利率',
            '5' =>  '1.05倍',
            '10' =>  '1.1倍',
            '15' =>  '1.15倍',
            '20' =>  '1.2倍',
			'30' => '1.3倍'
        );
    }

    //2013.9.10 modify by caoxiaolin，增加之前遗漏的配置（兑换积分）
    public function getTransactionType()
    {
        return array(
            TRAN_ORDER_LIZI             =>  '例子付费', //10
            TRAN_ORDER_XIAOGUO          =>  '效果付费', //11
            TRAN_ORDER_FREE             =>  '订单免费', //12
            TRAN_ORDER_SPECIAL          =>  '特殊付费', //13
            TRAN_ORDER_TIAOZHENG        =>  '调整付费', //14
            TRAN_SCORE_EXCHANGE         =>  '兑换积分', //15
            TRAN_RECHARGE               =>  '信贷员线下打款', //20
            TRAN_SYS_RECHARGE           =>  '财务代充值', //21
			TRAN_ONLINE_RECHARGE        =>  '信贷员线上充值', //22
            TRAN_INTEGRAL_REDEEM        =>  '积分兑换', //23
            TRAN_ORDER_REFUND           =>  '无效订单退款',//40
            TRAN_BANKER_REFUND          =>  '余额退款', //41
            TRAN_ORDER_REFUND_TIAOZHENG =>  '无效订单退款(调整)',//42
            TRAN_ENTRUST_RETURN         =>  '委托合作返点', //50
            TRAN_DISNEY_CHARGE   =>  '迪斯尼', // 17 迪斯尼领取客户扣款
            TRAN_OFFLINE_DIANXIAO_T2   =>  '融客', // 9 //线下电销T2推单
            TRAN_HIGH_DISNEY_CHARGE   =>  '拍客或锤客', // 19 //高端会所竞价
            
        );
    }

    public function getPaymentType()
    {
        return array(
            PAYMENT_ALIPAY      =>  '支付宝',
            PAYMENT_BANK        =>  '银行汇款',
            PAYMENT_ACCOUNTANT  =>  '财务代充值', //非信贷员自己充值，财务后台调整账号
            PAYMENT_ONLINE      =>  '线上充值', //信贷员通过支付网关充值
            PAYMENT_ENTRUST     =>  '委托合作',
            PAYMENT_BZJ_ALIPAY  =>  '支付宝',
            PAYMENT_BZJ_BANK    =>  '银行汇款',
        );
    }

    public function getRechargeStatus()
    {
        return array(
            '0' =>  '待审核',
            '1' =>  '充值成功',
            '2' =>  '未收到款',
            '3' =>  '金额不符',
            //add by xiaochang 2014-3-27
            // 金额不足最低限额退款
            '4' => '退费',
        );
    }

    public function getInvalidOrderReason()
    {
        return array(
            '2' =>  '外地号码，用户手机号为外地号并且在外地工作',
            '8' =>  '异地用户：用户工资发放、社保公积金缴纳及经营注册地均在外地',
            '1' =>  '无法接通：连续5日每日在不同时间段拨打均无法接通，空号，停机及无人接听',
            '3' =>  '无贷款意向：仅限于人工推送时录音中用户明确回答暂不需要贷款或无贷款意向', 
            '5' =>  '中介、同行试用：选择该项时，请填写申请人机构名称和姓名，仅限于贷款机构',
            '6' =>  '重复客户：仅限于用户通过融360平台，重复向您或您所在机构的其他信贷员申请贷款',
            //'9' =>  '伪冒，用户试用，非本人申请，无贷款意向', // 修改文案 by xiaochang 2014-10-24
            '9' =>  '恶意申请：仅限于首次联系时用户明确表示完全无真实贷款需求的伪冒，用户试用，非本人申请', //去掉 by xiaochang 2015-02-10
            '7' =>  '用户试用，标注用户明显回复为试用网站',
			'10' => '资质与订单资质详情不符：仅限于人工推送时录音中用户回答的资质与订单资质详情明显不符',
            '11'=>  '空号或停机',//added by zhaoaofei 2016-7-4  仅用于被封禁的信贷员
            '4' =>  '其他，请补充说明',
            '12'=>  '无法接通：使用注册手机号，连续3日每日拨打至少2次（无人应答的呼叫时长须大于20s），均无法接通，空号，停机及无人接听'
        );
    }
    public static function getBdOrderFeedbackNoReplyReason()
    {
        return array(
            '1'=>'空号或停机',
            '2'=>'连续5日每日在不同时间段拨打均无人接听（获取订单后满5*24小时方可反馈）',
        );
    }
    public static function getBdOrderFeedbackEYiReason()
    {
        return array(
            '1'=>'首次联系时用户明确表示完全无真实贷款需求的伪冒',
            '2'=>'用户试用',
            '3'=>'非本人申请',
        );
    }
    /**
     * 用户反馈详情页，不再给用户展示“其它，请补充说明”
     * */
    public function getLimitInvalidOrderReason()
    {
        $invalidOrderArr=$this->getInvalidOrderReason();
		//unset($invalidOrderArr['4'], $invalidOrderArr['7']);
        unset($invalidOrderArr['4'], $invalidOrderArr['2'], $invalidOrderArr['7']);
        return $invalidOrderArr;
    }
    
    /**
     * 手机端的用户反馈页，不展示2,7
     * */
    public function getLimitInvalidOrderReasonForMobile()
    {
        $configModel = new CRongConfig();
        $invalidOrderArr=$configModel->getInvalidOrderReason();
        unset($invalidOrderArr['2'], $invalidOrderArr['7']);
        return $invalidOrderArr;
    }
    
    /**
     * 手机端信用卡无效电话用户反馈页展示
     * */
    public function getCreditInvalidOrderReasonForMobile()
    {
        $configModel = new CRongConfig();
        $invalidOrderArr=$configModel->getCreditCardInvalidOrderReason();
        return $invalidOrderArr;
    }

    //在线支付支持的银行,图片按照名称规则来。
    public function getOnlinePayB2cBanks()
    {
        return array(
            'ICBC' =>  '工商银行',
            'ABC'  =>  '农业银行',
            'CCB'  =>  '建设银行',
            'BOC'  =>  '中国银行',
            'CMB'  =>  '招商银行',
            'HXB' =>  '华夏银行',  //hnapay未开通
            'PAB' =>  '平安银行',  //hnapay未开通
            'NJCB' =>  '南京银行', //hnapay未开通
            'SPDB' =>  '浦发银行',
            'BCOM' =>  '交通银行',
            'CMBC' =>  '民生银行',
            'SDB' =>  '深圳发展银行',
            'GDB' =>  '广东发展银行',
            'CITIC' =>  '中信银行',
            'CIB' =>  '兴业银行', //hnapay未开通
            //'GZRCC' =>  '广东农村商业银行',
            //'GZCB' =>  '广州银行',
           // 'SRCB' =>  '上海农村商业银行',
           // 'POST' =>  '中国邮政',
            'BOB' =>  '北京银行', //hnapay未开通
           // 'CBHB' =>  '渤海银行',
           // 'BJRCB' =>  '北京农商银行',
            'CEB' =>  '中国光大银行',
           // 'BEA' =>  '东亚银行',
            'NBCB' =>  '宁波银行', //hnapay未开通
           // 'HZB' =>  '杭州银行',
           // 'HSB' =>  '徽商银行',
           // 'CZB' =>  '浙商银行',
           // 'SHB' =>  '上海银行',
            'PSBC' =>  '中国邮政储蓄银行'
           // 'UPOP' =>  '银联在线支付',
        );
    }
    
    /**
     * PNR的B2C支付网关列表
     * @author xiaochang 2014-5-12
     * @return
     */
    public function getOnlinePayB2cBanksPNR(){
    	return array(
			"09" => "兴业银行",//CIB
			"12" => "民生银行",//CMBC
			"13" => "华夏银行",//HXB
			"15" => "北京银行",//BOB
			"16" => "浦发银行",//SPDB
			"19" => "广东发展银行",//GDB
			"21" => "交通银行",//BCOM
			"25" => "工商银行",//ICBC
			"27" => "建设银行",//CCB
			"28" => "招商银行",//CMB
			"29" => "农业银行",//ABC
			"33" => "中信银行",//CITIC
			"36" => "光大银行",//CEB
			"40" => "北京农商银行",//BJRCB
			"45" => "中国银行",//BOC
			"46" => "中国邮政储蓄银行",//PSBC
			"49" => "南京银行",//NJCB
			"50" => "平安银行",//PAB
			"51" => "杭州银行",//HZB
			"53" => "浙商银行",//CZB
			"54" => "上海银行",//SHB
			"55" => "渤海银行",//CBHB
			//"60" => "货币通",
			//"61" => "PNR钱管家",
			"69" => "上海农村商业银行",//SRCB
		);
    }

    //在线支付支持的银行
    public function getOnlinePayB2bBanks()
    {
        return array(
            'CMB_B2B'  =>  '招商银行-企业网银',
            'ICBC_B2B' =>  '工商银行-企业网银',
            'ABC_B2B'  =>  '农业银行-企业网银',
            'CCB_B2B'  =>  '建设银行-企业网银',
            'BOC_B2B'  =>  '中国银行-企业网银',
            'SPDB_B2B' =>  '浦发银行-企业网银',
        );
    }
    /**
     * PNR的B2B支付网关列表
     * @author xiaochang 2014-5-12
     * @return
     */
    public function getOnlinePayB2bBanksPNR(){
    	return array(
			"70" => "工商银行-企业网银",//ICBC
			"71" => "农业银行-企业网银",//ABC
			"74" => "光大银行-企业网银",//CEB
			"75" => "北京农商银行-企业网银",//BJRCB
			"76" => "浦发银行-企业网银",//SPDB
			"78" => "招商银行-企业网银",//CMB
			"81" => "深圳发展银行-企业网银",//SDB
			"82" => "建设银行-企业网银",//CCB
		);
    }
    /**
     * 连连支付快捷支付银行列表
     * @return multitype:multitype:string
     */
    public function getOnlinePayBankLLpay_quick(){
		return array (
				'01020000' => array (
						'name' => '中国工商银行',
						'code' => 'ICBC',
						'credit' => '1',
						'debit' => '1' 
				),
				'01030000' => array (
						'name' => '中国农业银行',
						'code' => 'ABC',
						'credit' => '1',
						'debit' => '1' 
				),
				'01040000' => array (
						'name' => '中国银行  ',
						'code' => 'BOC',
						'credit' => '1',
						'debit' => '1' 
				),
				'03100000' => array (
						'name' => '浦发银行',
						'code' => 'SPDB',
						'credit' => '1',
						'debit' => '1' 
				),
				'03040000' => array (
						'name' => '华夏银行',
						'code' => 'HXB',
						'credit' => '1',
						'debit' => '1' 
				),
				'03070000' => array (
						'name' => '平安银行',
						'code' => 'PAB',
						'credit' => '1',
						'debit' => '1' 
				),
				'03030000' => array (
						'name' => '光大银行',
						'code' => 'CEB',
						'credit' => '1',
						'debit' => '1' 
				),
				'04233310' => array (
						'name' => '杭州银行',
						'code' => 'HZB',
						'credit' => '1',
						'debit' => '1' 
				),
				'01050000' => array (
						'name' => '建设银行',
						'code' => 'CCB',
						'credit' => '1',
						'debit' => '1' 
				),
				'03090000' => array (
						'name' => '兴业银行',
						'code' => 'CIB',
						'credit' => '1',
						'debit' => '1' 
				),
				'03020000' => array (
						'name' => '中信银行',
						'code' => 'CITIC',
						'credit' => '1',
						'debit' => '1' 
				),
				'04012900' => array (
						'name' => '上海银行',
						'code' => 'SHB',
						'credit' => '1',
						'debit' => '1' 
				),
				'05083000' => array (
						'name' => '江苏银行',
						'code' => 'JSB',
						'credit' => '1',
						'debit' => '1' 
				),
				'04243010' => array (
						'name' => '南京银行',
						'code' => 'NJCB',
						'credit' => '1',
						'debit' => '1' 
				),
				'03050000' => array (
						'name' => '民生银行',
						'code' => 'CMBC',
						'credit' => '1',
						'debit' => '1' 
				),
				'03060000' => array (
						'name' => '广发银行',
						'code' => 'GDB',
						'credit' => '1',
						'debit' => '0' 
				),
				'03010000' => array (
						'name' => '交通银行',
						'code' => 'BCOM',
						'credit' => '1',
						'debit' => '0' 
				),
				'03080000' => array (
						'name' => '招商银行',
						'code' => 'CMB',
						'credit' => '1',
						'debit' => '0' 
				),
				'01000000' => array (
						'name' => '邮政储蓄银行',
						'code' => 'POST',
						'credit' => '1',
						'debit' => '0' 
				),
				'04031000' => array (
						'name' => '北京银行',
						'code' => 'BOB',
						'credit' => '1',
						'debit' => '0' 
				) 
		);
    }
    
    /**
     * 获取PNR支付网关对应的银行缩写
     * @author xiaochang 2014-5-12
     * @param string $bankID
     * @return 返回缩写，如果找不到则原样返回
     */
    public function getBankShortNamePNR($bankID){
    	$arr = array(
			"09" => "CIB",
			"12" => "CMBC",
			"13" => "HXB",
			"15" => "BOB",
			"16" => "SPDB",
			"19" => "GDB",
			"21" => "BCOM",
			"25" => "ICBC",
			"27" => "CCB",
			"28" => "CMB",
			"29" => "ABC",
			"33" => "CITIC",
			"36" => "CEB",
			"40" => "BJRCB",
			"45" => "BOC",
			"46" => "PSBC",
			"49" => "NJCB",
			"50" => "PAB",
			"51" => "HZB",
			"53" => "CZB",
			"54" => "SHB",
			"55" => "CBHB",
			"69" => "SRCB",
			"70" => "ICBC",
			"71" => "ABC",
			"74" => "CEB",
			"75" => "BJRCB",
			"76" => "SPDB",
			"78" => "CMB",
			"81" => "SDB",
			"82" => "CCB",
		);
		if(isset($arr[$bankID])){
			return $arr[$bankID];
		}
		else{
			return $bankID;
		}
    }
    /**
     * 返回银行限额情况，0表示不限，-表示不支持
     * by xiaochang 2014-07-30
     * @param string $bank 银行缩写
     * @param string $card debit或者credit
     */
    public function getBankLimit($bank,$card){
    	$arr = array(
				// 工商银行
				'ICBC' => array (
						'debit' => '2000',
						'credit' => '未开通短信认证单笔限额500元，开通短信认证单笔限额2000元' 
				),
				// 建设银行
				'CCB' => array (
						'debit' => '0',
						'credit' => '非签约用户暂不支持，开通短信验证单笔限额1000元' 
				),
				// 招商银行
				'CMB' => array (
						'debit' => '1000',
						'credit' => '500' 
				),
				// 农业银行
				'ABC' => array (
						'debit' => '1000',
						'credit' => '-' 
				),
				// 交通银行
				'BCOM' => array (
						'debit' => '5000',
						'credit' => '单笔限额50000元或卡本身限额' 
				),
				// 民生银行
				'CMBC' => array (
						'debit' => '5000',
						'credit' => '大众版单笔限额300元，贵宾版单笔限额5000元' 
				),
				// 中国银行
				'BOC' => array (
						'debit' => '10000',
						'credit' => '非签约用户不支持，签约用户单笔限额5000元' 
				),
				// 上海银行
				'SHB' => array (
						'debit' => '5000',
						'credit' => '动态密码单笔限额6000元' 
				),
				// 平安银行
				'PAB' => array (
						'debit' => '50000',
						'credit' => '非签约用户暂不支持，签约用户单笔限额5000元' 
				),
				// 兴业银行
				'CIB' => array (
						'debit' => '1000',
						'credit' => '1000' 
				),
				// 华夏银行
				'HXB' => array (
						'debit' => '1000',
						'credit' => '-' 
				),
				// 北京银行
				'BOB' => array (
						'debit' => '10000',
						'credit' => '-' 
				),
				// 浦发银行
				'SPDB' => array (
						'debit' => '50000',
						'credit' => '非签约用户暂不支持，签约用户单笔限额500元' 
				),
				// 广发银行
				'GDB' => array (
						'debit' => '1500',
						'credit' => '1500' 
				),
				// 中信银行
				'CITIC' => array (
						'debit' => '0',
						'credit' => '加强版文件证书单笔限额500元，加强版数字证书单笔限额1000元' 
				),
				// 光大银行
				'CEB' => array (
						'debit' => '5000',
						'credit' => '300' 
				),
				// 北京农村商业银行
				'BJRCB' => array (
						'debit' => '0',
						'credit' => '-' 
				),
				// 东亚银行
				'BEA' => array (
						'debit' => '5000',
						'credit' => '-' 
				),
				// 南京银行
				'NJCB' => array (
						'debit' => '10000',
						'credit' => '-' 
				),
				// 杭州银行
				'HZB' => array (
						'debit' => '0',
						'credit' => '500' 
				),
				// 宁波银行
				'NBCB' => array (
						'debit' => '50000',
						'credit' => '电子支付密码用户单笔限额999元，动态密码用户单笔限额5000元' 
				),
				// 浙商银行
				'CZB' => array (
						'debit' => '0',
						'credit' => '-' 
				),
				// 邮储银行
				'PSBC' => array (
						'debit' => '5000',
						'credit' => '-' 
				),
				// 渤海银行
				'CBHB' => array (
						'debit' => '20000',
						'credit' => '' 
				),
				// 上海农村商业银行
				'SRCB' => array (
						'debit' => '0',
						'credit' => '' 
				),
				// 深圳发展银行
				'SDB' => array (
						'debit' => '-',
						'credit' => '-' 
				),
    	);
    	if(isset($arr[$bank]) && isset($arr[$bank][$card])){
    		return $arr[$bank][$card];
    	}
    	else{
    		return '';
    	}
    }

    //信贷员反馈的订单状态, add by caoxiaolin
    static public function getOrderStatus()
    {
        return array(
            ORDER_STATUS_TIJIAOSHENQING     => '新订单',            //2，尚未联系。跟单：提交申请
            ORDER_STATUS_YONGHUKAOLV        => '用户考虑中',         //跟单：用户考虑  
            ORDER_STATUS_BEIJUJUE           => '资质不符',        //4，不符合条件；8，未批准。跟单：被拒绝
            ORDER_STATUS_FANGQI             => '客户放弃',          //9，申请人放弃。跟单：用户放弃
            ORDER_STATUS_CHUBUMANZU         => '待跟客户',//3，已联系；5，资料准备中。跟单：初步满足
            ORDER_STATUS_SHENPIZHONG        => '进件审批',            //6，申请中。跟单：
            ORDER_STATUS_SHENPIBEIJU        => '审批被拒',          // 审批后被拒绝，未通过
            ORDER_STATUS_KONGHAO            => '无效电话',          //跟单：空号
            ORDER_STATUS_SHENPITONGGUO      => '审批通过',          //7，已批准。跟单：审批通过
            ORDER_STATUS_FANGKUANSHIBAI => '放款失败',         //放款失败
            ORDER_STATUS_CHENGGONGFANGKUAN  => '成功放款',          //跟单：成功放款
        );
    }

    //风控产品的订单状态, add by yangwenqing
    static public function getRiskOrderStatus()
    {
        return array(
            '10' => '新订单',               
            '40' => '不符合条件',
            '50' => '用户放弃',
            '70' => '申请进件',
            '80' => '待补充材料',
            '90' => '审批中',
            '100'=> '审批通过',
            '110'=> '审批拒绝',
            '140'=> '待签合同',
            '151'=> '待放款',
            '152'=> '合同作废',
            '161'=> '贷款取消',
            '162'=> '已放款待确认',
            '170'=> '放款成功',
            '171'=> '放款失败'
        ); 
    }

    static public function getOrderStatusForUserView()
    {
        return array(
            ORDER_STATUS_TIJIAOSHENQING => '待审核',// "提交申请",
            ORDER_STATUS_WEILIANXI_1 => '待审核',// "超过1天未联系",
            ORDER_STATUS_WEILIANXI_2 => '待审核',// "超过2天未联系",
            ORDER_STATUS_KONGHAO => '无效申请',//"无效电话",
            ORDER_STATUS_LIANXIBUSHANG => '无效申请',// "联系不上",
            ORDER_STATUS_YILIANXI => '待审核',//"双方已联系",
            ORDER_STATUS_SHIYONG => '无效申请',//"用户试用",
            ORDER_STATUS_WEIMAO => '无效申请',//"用户伪冒",
            ORDER_STATUS_BEIJUJUE => '审核未通过',//"被拒绝",
            ORDER_STATUS_BEIJUJUE_FEIDAN => '无效申请',//"被拒绝",
            ORDER_STATUS_FANGQI => '放弃申请',//"用户放弃",
            ORDER_STATUS_FANGQI_FEIDAN => '放弃申请',//"用户放弃",
            ORDER_STATUS_FANGQI_WEILIANXI => '放弃申请',//"用户放弃-未联系",
            ORDER_STATUS_CHUBUMANZU => '审核中',//"初步满足",
            ORDER_STATUS_YONGHUKAOLV => '待审核',//"用户考虑",
            ORDER_STATUS_YONGHUKAOLVDAOQI => '无效申请',//"用户考虑到期",
            ORDER_STATUS_SHENPIZHONG => '审核中',//"审批中",
            ORDER_STATUS_SHENPIBEIJU => '审核未通过',//"审批被拒",
            ORDER_STATUS_SHENPITONGGUO => '审核通过',//"审批通过",
            ORDER_STATUS_CHENGGONGFANGKUAN => '成功放款',//"成功放款",
            ORDER_STATUS_FANGKUANSHIBAI => '审核未通过',//"放款失败",
        );
    }

    /**
     * CRongConfig::getCreditOrderStatus()
     * 返回全部信用卡订单状态
     * @param bool $withAll 是否包含“全部”
     * @return array 信用卡订单状态数组
     */
    public static function getCreditOrderStatus($withAll=false){
        $status = array(
            //0                               => '全部',
            ORDER_STATUS_TIJIAOSHENQING     => '新订单',
            ORDER_STATUS_KONGHAO            => '无效电话',
            ORDER_STATUS_YONGHUKAOLV        => '用户考虑中',
            ORDER_STATUS_BEIJUJUE           => '不符合条件',
            ORDER_STATUS_FANGQI             => '用户放弃',
            ORDER_STATUS_CHUBUMANZU         => '待跟进用户（初满）',
            ORDER_STATUS_SHENPIZHONG        => '审批中',
            ORDER_STATUS_SHENPIBEIJU        => '审批被拒',
            ORDER_STATUS_CHENGGONGFANGKUAN  => '审批通过/已激活卡',
        );
        if($withAll){
            $data = array(0=>'全部');
            foreach($status as $k=>$v){
                $data[$k]=$v;
            }
            return $data;
        }
        else{
            //array_shift($status);
            return $status;
        }
    }
    //用户被拒原因, add by caoxiaolin
    public function getRejectReason()
    {
        return array(
            10  => '信用记录差',
            20  => '负债比例高',
            30  => '无工资流水',
            40  => '收入/流水过低',
            50  => '购买外地房',
            60  => '异地工作',
            70  => '信用空白',
            80  => '工作/企业时间短',
            //90  => '连续两天未联系',
            110 => '低龄申请',
            120 => '高危行业',
            130 => '无业',
            140 => '仅现金流水',
            150 => '本地无房产',
            160 => '非特定人群',
        	//170 => '本地无房产',
        	180 => '车辆按揭/抵押中',
        	190 => '房产按揭/抵押中',
            100 => '其他'
        );
    }
    

    //用户放弃原因, add by caoxiaolin
    public static function getAbandonReason()
    {
        return  array(
            10  => '利息高',
            20  => '额度少',
            30  => '嫌麻烦',
            40  => '已自筹',
            60  => '不需要了',
            70  => '其他中介平台',
            50  => '其他',
        );
    }
    
    
    //信用卡订单，无效电话选项
    public function getCreditCardInvalidOrderReason()
    {
        return array(
            '2' =>  '外区号码，用户不在可提供服务的城市或区域',
            '1' =>  '空号、停机、关机',
            '5' =>  '中介、同行试用网站效果',
            '6' =>  '重复客户，用户已向您或您所在机构的其他信贷员申请过信用卡',
            '4' =>  '其他，请补充说明',
        );
    }
    
    
    //信用卡订单，用户放弃原因，added by guoxubin @2013-06-20
    public static function getCreditCardAbandonReason()
    {
        return  array(
            10  => '已经办卡',
            20  => '自行解决',
            30  => '不需要了',
            40  => '嫌麻烦',
        );
    }
    
    
    //信用卡订单反馈，不符合条件，added by guoxubin @2013-06-20
    public static function getCreditCardRejectReason()
    {
        return array(
            10  => '有逾期记录',
            20  => '有本行信用卡',
            30  => '无工作',
            40  => '无社保',
            50  => '无公积金',
            60  => '高危行业',
            70  => '其它',
        );
    }

    //用户考虑原因, add by caoxiaolin
    public static function getConsiderReason()
    {
        return  array(
            10  => '利息高',
            20  => '额度少',
            30  => '嫌麻烦',
            40  => '考虑自筹',
            50  => '其他',
        );
    }

    //add by caoxiaolin at 2012.12.12，无效订单审核状态
    public static function getInvalidOrderStatus()
    {
        return array(
            INVALID_ORDER_STATUS_NEW             => '信贷员提交',
            INVALID_ORDER_STATUS_KEFU_NOTSURE    => '客服不确定',//add by zhangji, at 2013.4.26
            INVALID_ORDER_STATUS_KEFU_PASS       => '客服通过',
            INVALID_ORDER_STATUS_KEFU_REJECT     => '客服拒绝',
            INVALID_ORDER_STATUS_ADMIN_PASS      => '销售主管通过',
            INVALID_ORDER_STATUS_ADMIN_REJECT    => '销售主管拒绝',
            INVALID_ORDER_STATUS_SYSTEM_PASS     => '系统自动通过',
	    INVALID_ORDER_STATUS_KEFU_SHENHEZHONG => '客服审核中',
            INVALID_ORDER_STATUS_DONGJIE          => '客服冻结',
            INVALID_ORDER_STATUS_KEFU_WUXUSHENHE => '无需审核',
            INVALID_ORDER_STATUS_SYSTEM_REJECT => '系统自动拒绝',
        );
    }
    //add by zhangji at 2013.4.26,无效订单审核-拒绝原因-通过原因
    public static function getInvalidOrderPassOrRejectReason()
    {
        return array(
			'invalid_order_shenhezhong_reason'=>array(
                INVALID_ORDER_PASS_REASON_KONGHAO=>'无法接通',
				INVALID_ORDER_SHENHEZHONG_REASON_LUYINQUESHI => '录音缺失，待核实',
            ),
            'invalid_order_pass_reason'=>array(
                INVALID_ORDER_PASS_REASON_WAIDIHAO=>'异地工作',
                //INVALID_ORDER_PASS_REASON_WEISHENQING=>'未申请贷款',
                //INVALID_ORDER_PASS_REASON_NO_NEED=>'不需要',
                //INVALID_ORDER_PASS_REASON_SHIYONG=>'用户试用',
                INVALID_ORDER_PASS_REASON_ZHONGJIE_TONGHANG=>'中介、同行试用',
                INVALID_ORDER_PASS_REASON_CHONGFUSHENQING=>'重复申请',
				INVALID_ORDER_PASS_REASON_WEIMAO=>'伪冒',
                INVALID_ORDER_PASS_REASON_QITA=>'其他',
				INVALID_ORDER_PASS_REASON_ZIZHIBUFU=>'资质不符',
			),
            'invalid_order_reject_reason'=>array(
                //INVALID_ORDER_REJECT_REASON_NO_NEED=>'不需要',
                //INVALID_ORDER_REJECT_REASON_WEISHENQING=>'未申请',
                //INVALID_ORDER_REJECT_REASON_ZIZHIBUFU=>'资质问题',
                //INVALID_ORDER_REJECT_REASON_KONGHAO=>'可以接通',
                INVALID_ORDER_REJECT_REASON_QITA=>'其他',
				INVALID_ORDER_REJECT_REASON_BENDIGONGZUO => '本地工作',
				INVALID_ORDER_REJECT_REASON_FEIZHONGJIETONGHANG => '非中介同行',
				INVALID_ORDER_REJECT_REASON_CHONGFUSHENQING => '非本网站重复申请',
				INVALID_ORDER_REJECT_REASON_ZHENSHIYONGHU => '真实用户',
				INVALID_ORDER_REJECT_REASON_KEYIJIETONG => '可以接通',
				INVALID_ORDER_REJECT_REASON_ZIZHIFUHE => '资质符合',
                INVALID_ORDER_REJECT_REASON_RECORD_KEYIJIETONG => '双方有接听通话记录',
                INVALID_ORDER_REJECT_REASON_RECORD_BUMANZUBODATIAOJIAN => '不满足拨打条件',
                INVALID_ORDER_REJECT_REASON_RECORD_NONE => '双方无拨打记录'
			),
		);
    }
    
    /**
     * 退款审批通过和拒绝的二级原因
     * */
    public static function getInvalidOrderPassOrRejectReasonLevel2()
    {
        return array(
            INVALID_ORDER_PASS_REASON_KONGHAO=>array(
                INVALID_ORDER_SHENHEZHONG_LEVEL2_REASON_KONGHAO=>'空号，停机，关机，来电提醒',
                INVALID_ORDER_SHENHEZHONG_LEVEL2_REASON_BOTONGWURENJIETING=>'拨通、但无人接听',
            ),
            INVALID_ORDER_PASS_REASON_WEIMAO=>array(
                INVALID_ORDER_PASS_LEVEL2_REASON_YONGHUBUREN=>'未申请贷款',
				INVALID_ORDER_PASS_LEVEL2_REASON_WUXUQIU=>'无贷款需求',
            ),
            INVALID_ORDER_PASS_REASON_CHONGFUSHENQING=>array(
				INVALID_ORDER_PASS_LEVEL2_REASON_TONGYIXINDAIYUAN=>'同一信贷员',
                INVALID_ORDER_PASS_LEVEL2_REASON_DUOCISHENQING=>'单人申请同一机构第二个信贷员',
                INVALID_ORDER_PASS_LEVEL2_REASON_CHAOQI=>'申请时间超过两个月'
            ),
			INVALID_ORDER_REJECT_REASON_ZHENSHIYONGHU=>array(
                INVALID_ORDER_REJECT_LEVEL2_REASON_YOUXUQIU=>'有贷款需求'
            ),
			INVALID_ORDER_REJECT_REASON_KEYIJIETONG=>array(
                INVALID_ORDER_REJECT_LEVEL2_REASON_BENRENJIETING=>'本人接听'
            ),
        );
    }
	
	//add by xiaoyaxin at 2013.11.13，逾期订单审核状态
    public static function getOverdueOrderStatus()
    {
        return array(
            OVERDUE_ORDER_STATUS_NEW          => '未审核',
            OVERDUE_ORDER_STATUS_KEFU_DOING   => '审核中',
            OVERDUE_ORDER_STATUS_KEFU_REJECT  => '审核拒绝',
            OVERDUE_ORDER_STATUS_KEFU_PASS    => '审核通过',
        );
    }
	public static function getOverdueOrderCheck()
    {
        return array(
            OVERDUE_ORDER_STATUS_KEFU_DOING   => array(
				OVERDUE_ORDER_CHECK_NO_ANSWER => '无法连通',
				OVERDUE_ORDER_CHECK_REFUSED => '用户拒绝回访',
			),
            OVERDUE_ORDER_STATUS_KEFU_PASS   => array(
				OVERDUE_ORDER_CHECK_SURPASS => '仅逾期',
				OVERDUE_ORDER_CHECK_BADACCOUNT => '已坏账',
			),
        );
    }

    /**
	 * 获取申请类型的默认值。
	 */
    public static function getApplyDefaultValue(){
		return array(
			APPLY_TYPE_JINGYING => array ('loan_limit'=>10, 'loan_term'=>12),
			APPLY_TYPE_XIAOFEI => array ('loan_limit'=>5, 'loan_term'=>12),
			APPLY_TYPE_CHEDAI => array ('car_price'=>20, 'shoufu_ratio'=>3, 
				'loan_limit'=>14, 'loan_term'=>36),
			APPLY_TYPE_FANGDAI => array ('loan_limit'=>10, 'loan_term'=>12),
			APPLY_TYPE_OTHER => array ('loan_limit'=>10, 'loan_term'=>12),
			APPLY_TYPE_NORMAL => array ('loan_limit'=>10, 'loan_term'=>12),
		);
	}

    /**
	 * 获取产品的默认申请类型。在用户未输入申请类型时使用。
	 */
	public static function getProductToApplyType(){
		return array(
			PRODUCT_JINGYING => APPLY_TYPE_JINGYING,
			PRODUCT_DAEFENQI => APPLY_TYPE_XIAOFEI,
			PRODUCT_CHEDAI => APPLY_TYPE_CHEDAI,
			PRODUCT_QIYE => APPLY_TYPE_CHEDAI,
			PRODUCT_FANGDAI => APPLY_TYPE_FANGDAI,
			PRODUCT_XIAOFEI => APPLY_TYPE_XIAOFEI
		);
	}
	public static function getProductDefaultApplyInfo($ptype){
		$col = self::getProductToApplyType();
		$atype = $col[$ptype];
		if(!isset($atype))
		{
			return false;
		}

		$col = self::getApplyDefaultValue();
		return array('application_type' => $atype, 'dft' => $col[$atype]);

	}
    public static function googleAnalyticsGetImageUrl($url_append='', $ga_account='', $ga_subdomain='ga', $ga_protocol='http') {
        //手机版GA跟踪代码
        if(empty($ga_account)){
            $ga_account = "MO-34990941-1"; //m.rong360.com
        }
        $host = strtolower($_SERVER['HTTP_HOST']);
        $domain = substr($host,0,strpos($host,'.rong360'));

        //$GA_PIXEL = "http://$ga_subdomain.rong360.com/ga.php";
        //国外服务器无法访问，暂时挪到国内服务器做统计 2012-03-11

        $strProtocol = 'http';
        if($ga_protocol==='https'){
            $strProtocol = 'https';
        }
        $GA_PIXEL = $strProtocol."://www.rong360.com/ga";
        if($domain=='m' || $domain=='3g' || $domain=='mo')
        {
            $GA_PIXEL = "/ga";
        }

        $url = "";
        $url .= $GA_PIXEL . "?";
        $url .= "utmac=" . $ga_account;
        $url .= "&utmn=" . rand(0, 0x7fffffff);
        $referer = $_SERVER["HTTP_REFERER"];
        $query = $_SERVER["QUERY_STRING"];
        $path = $_SERVER["REQUEST_URI"];
        if (empty($referer)) {
          $referer = "-";
        }
        $url .= "&utmr=" . urlencode($referer);
        if (!empty($path)) {
          $path .= $url_append;
          $url .= "&utmp=" . urlencode($path);
        }
        $url .= "&guid=ON";
        return $url;
    }

	public static function getBankerStatusConfig()
	{
		return array(
            BANKER_JUST_REGISTER    => '已注册',
		    BANKER_STATUS_DAISHENHE =>'待审核',
			BANKER_STATUS_DAISHANGXIAN=>'待上线',
			BANKER_STATUS_XIANSHANG=>'线上',
			BANKER_STATUS_XIANXIA  =>'线下',
            BANKER_VERIFY_NO_PASS  => '审核不通过', 
		);
	}
	
	//此状态用于rong360主站的信贷员。区别于用于金桥的getBankerStatusConfig
	public static function getRongBankerStatusConfig()
	{
	    return array(
	            BANKER_STATUS_DAISHENHE =>'待审核',
	            BANKER_STATUS_DAISHANGXIAN=>'待上线',
	            BANKER_STATUS_XIANSHANG=>'线上',
	            BANKER_STATUS_XIANXIA  =>'线下',
	            BANKER_STATUS_TEMP_XIAXIAN  => '临时下线',
	    );
	}

	public static function getBankerCooperationConfig()
	{
		return array(
		    BANKER_COOPERATION_PERSONAL =>'个人',
			BANKER_COOPERATION_COMPANY =>'对公例子合作',
		    BANKER_COOPERATION_ISCOMPANY => '对公IS合作',
			BANKER_COOPERATION_TAOJINYUN => '淘金云合作',
		);
	}

        /**
     *by liuyue 2016-10-24
     *  信贷员审核资料一级原因
     * update by zhaoaofei 2016-12-11
     */
    public static function getBankerSourceCheckOne()
    {
        /*
        $config = array(
            BANKER_SOURCE_PASS => '通过',
            BANKER_SOURCE_CERTIFICATE_ERROR => '证件资料有误',
            BANKER_SOURCE_INFO_ERROR => '提供虚假资料',
            BANKER_SOURCE_REPEAT_ACCOUNT => '重复账号',

        );*/
        $config = array(
            BANKER_SOURCE_PASS => '通过',
            BANKER_SOURCE_UNPASS => '不通过',
        );
        return $config;
    }
    /**
     *by liuyue 2016-10-24
     *  信贷员审核资料 证件资料有误  二级原因
     */
    public static function getBankerSourceCertificateCheckTwo()
    {
        /*$config = array(
            BANKER_SOURCE_IDCARD_ERROR => '身份证资料有误',
            BANKER_SOURCE_WORKCARD_ERROR => '工牌有误/不符合要求',
            BANKER_SOURCE_BUSINESS_ERROR => '名片有误/不符合要求',
            BANKER_SOURCE_LABOR_ERROR => '在职证明/劳动合同不符合要求',
        );*/
        $config = array(
            BANKER_SOURCE_UNCLEAR_ERROR => '资料照片不清楚',
            BANKER_SOURCE_WORKCARD_ERROR => '工牌/名片不符合要求',
            BANKER_SOURCE_SHIXI_ERROR => '实习期内不予认证',
            BANKER_SOURCE_IDCARD_ERROR => '证件资料虚假',
            BANKER_SOURCE_REPAET_REAPT => '重复账号',
        );
        return $config;
    }
    /**
     *by liuyue 2016-10-24
     *  信贷员审核资料 提供虚假资料  二级原因
     */
    public static function getBankerSourceFalseCheckTwo()
    {
        $config = array(
            BANKER_SOURCE_SALER_CHECK => '请信贷员联系商务顾问核实信息',
            BANKER_SOURCE_BANKERINFO_ERROR => '信贷员信息虚假',
        );
        return $config;
    }
    /**
     *by liuyue 2016-10-24
     *  信贷员审核资料 重复账号  二级原因
     */
    public static function getBankerSourceRepeatCheckTwo()
    {
        $config = array(
            BANKER_SOURCE_TWO_REPAET => '重复账号',
        );
        return $config;
    }

    /**
     *by liuyue 2016-10-24
     *  信贷员审核资料 通过  二级原因
     */
    public static function getBankerSourceSuccCheckTwo()
    {
        $config = array(
            BANKER_SOURCE_CHECK_SUCCESS => '通过',
        );
        return $config;
    }
    /**
     *by liuyue 2016-10-25
     *  信贷员座机电话审核 一级原因
     */
    public static function getBankerMobileCheckOne()
    {
        $config = array(
            BANKER_MOBILE_CHECK_SUCC => '通过',
            BANKER_MOBILE_CHECK_ERROR => '座机不通过',
        );
        return $config;
    }
    /**
     *by liuyue 2016-10-25
     *  信贷员座机电话审核通过 二级原因
     *
     */
    public static function getBankerMobileSuccCheckTwo()
    {
        $config = array(
            BANKER_MOBILE_CHECK_OFFICIAL => '官网\400客服核实',
            BANKER_MOBILE_CHECK_THIRD => '第三方渠道所得座机核实',
            BANKER_MOBILE_CHECK_SYSTEM => '系统连号座机核实',
            BANKER_MOBILE_CHECK_UNABLE => '无法确定座机核实',
            BANKER_CHECK_TIAOGUO       => '暂未审核状态',
        );
        return $config;
    }

    /**
     *by liuyue 2016-10-25
     *  信贷员座机电话审核座机有误 二级原因
     * 核实查无此人/离职，座机拨打无人接听，座机不是固定座机，座机拨打不是该机构，座机不配合审核
     */
    public static function getBankerMobileFalseCheckTwo()
    {
        $config = array(
            BANKER_MOBILE_CHECK_NO_ANSWER => '座机无人接听',
            BANKER_MOBILE_CHECK_INVALID => '座机空号、停机、欠费、传真',
            BANKER_MOBILE_CHECK_LEAVE => '核实查无此人/离职',
            BANKER_MOBILE_CHECK_NO_AGENTCY => '座机拨打不是该机构',
            BANKER_MOBILE_CHECK_NO_FIT => '座机不配合审核',
            BANKER_MOBILE_CHECK_CITY_CHANGE=>'更换城市'
        );
        return $config;
    }

    /**
     *by liuyue 2016-10-25
     *  信贷员手机号审核 一级原因
     */
    public static function getBankerPhoneCheckOne()
    {
        $config = array(
            BANKER_PHONE_CHECK_SUCC => '通过',
            BANKER_PHONE_CHECK_ERROR => '手机不通过',
        );
        return $config;
    }

    /**
     *by liuyue 2016-10-25
     *  信贷员手机号审核 二级原因
     * 手机拨打无人接听，手机号不是本人接听，手机号空号、关机，信贷员不配合审核
     */
    public static function getBankerPhoneSuccCheckTwo()
    {
        $config = array(
            BANKER_PHONE_CHECK_SUCCESS => '通过',
            BANKER_CHECK_TIAOGUO => '暂未审核状态'
        );
        return $config;
    }

    /**
     *by liuyue 2016-10-25
     *  信贷员手机号审核 二级原因
     * 手机拨打无人接听，手机号不是本人接听，手机号空号、关机，信贷员不配合审核
     */
    public static function getBankerPhoneFalseCheckTwo()
    {
        $config = array(
            BANKER_PHONE_CHECK_NONE => '手机拨打无人接听',
            BANKER_PHONE_CHECK_EMPTY => '手机号空号、停机、欠费',
            BANKER_PHONE_CHECK_LEAVE=>'核实已离职',
            BANKER_PHONE_CHECK_NO_FIT => '信贷员不配合审核',
            BANKER_PHONE_CHECK_NOT_I => '手机号非本人使用',
            BANKER_PHONE_CHECK_CITY_CHANGE => '更换城市',
        );
        return $config;
    }
    /**
     *by zhaoaofei 2016-12-11
     *  信贷员风险认证 一级原因
     */
    public static function getBankerRiskCheckOne()
    {
        $config = array(
            BANKER_RISK_CHECK_SUCC => '通过',
            BANKER_RISK_CHECK_ERROR => '风险验证不通过',
        );
        return $config;
    }
    public static function getBankerRiskSuccCheckTwo()
    {
        $config = array(
            BANKER_RISK_CHECK_SUCCESS => '通过',
        );
        return $config;
    }
    public static function getBankerRiskFalseCheckTwo()
    {
        $config = array(
            BANKER_RISK_SHUXIN_BLACK_LIST => '数信网查实网络黑名单',
            BANKER_RISK_FAHAI_CASING => '法海网查实执行中',
            BANKER_RISK_JIAO_UNMATCH=>'手机实名制不一致',
            BANKER_RISK_LIVENESS_ERROR => '活体识别不一致',
			BANKER_RISK_FAHAI_JINJI=>'法海经济案件复核',
			BANKER_RISK_FAHAI_QITA=>'经复核可补充材料',
			BANKER_RISK_JIAO_BC=>'手机实名制复核通过',//手机实名制不一致，BC复核通过
			BANKER_RISK_PASS_BC=>'风险认证复核通过',//可以认为风险通过的状态
        );
        return $config;
    }

    /**
     *by liuyue 2016-10-25
     *  信贷员认证审核
     */
    public static function getBankerSourceEndReason()
    {
        $config = array(
            BANKER_CHECK_NO_OPTION => '综合判定',
            BANKER_CHECK_SAME_ACCOUNT => '重复账号',
            BANKER_CHECK_RISK_ACCOUNT => '风险账号',
        );
        return $config;
    }



    public static function getPermitType()
    {
        return array(
                PERMIT_TYPE_IS => '是否',
                PERMIT_TYPE_ENUMERATE => '枚举',
                PERMIT_TYPE_COMPARE => '比较',
                PERMIT_TYPE_RANGE => '范围',
                PERMIT_TYPE_TEXT  => '输入',
            );
    }

	//中介产品类型
	public  static function getAgentType(){
		return array(
			'1'=>'房产抵押贷款',
			'2'=>'汽车抵押贷款',
			'3'=>'个人信用贷款',
			'4'=>'企业信用贷款',
			'5'=>'企业抵押贷款',
			'6'=>'其它贷款业务'
		);
	}

	//中介产品办理天数
	public static function getAgentOperateDays(){
		return array(
			'3'=>'1至3天',
			'7'=>'1周之内',
			'14'=>'2周之内',
			'30'=>'1个月之内',
			'90'=>'3个月之内'
		);
	}
	//中介人员规模
	public static function getAgentArrScale(){
		return array(
			"5"=>"5人以下",
			"10"=>"5-10人",
			"30"=>"10-30人",
			"50"=>"30-50人",
			"100"=>"50-100人",
			"150"=>"100-150人",
			"200"=>"150人以上"
		);
	}

    //2013.3.1 add by caoxiaolin,金桥公司类型
    public static function getJinqiaoCompanyType(){
        return array(
            1 => '银行',
            2 => '小贷公司',
            3 => '房屋中介',
            4 => '汽车4S店',
            5 => '贷款担保公司',
        );
    }

	//专题推荐产品类别
	public static function getLoantopicType(){
		return array(
			'1'=>'抵押房产',
			'2'=>'抵押车辆',
			'3'=>'无抵押',
			'4'=>'中介代办',
			'5'=>'质押股票'
		);
	}

        //鼠洞收集信息,职业身份对应的收入类型,BD 客户领取
        public static $income_type = array(
            '1' => array(
                'var_name' => 'com_taxable_income',
                'desc' => '对公账户经营收入/月'
            ),
            '2' => array(
                'var_name' => 'com_month_flow',
                'desc' => '个人账户经营收入/月' 
            ), 
            '4' => array(
                'var_name' => 'user_income_by_card',
                'desc' => '银行卡发放工资/月'
            ),
            '10' => array(
                'var_name' => 'cash_receipts',
                'desc' => '现金收入/月'
            )
        );

    //问答平台贷款知识类别
    public static function getAskCategory(){
        return array(
            '1'=>'常用知识',
            '2'=>'贷款流程',
            '3'=>'资质咨询',
            '4'=>'利息咨询'
        );
    }
    public static function getAskguaranteeType(){
        return array(
            '1'=>'无抵押贷',
            '2'=>'抵押贷款',
            '3'=>'按揭贷款',
            '4'=>'担保咨询',
            '5'=>'信用卡'
        );
    }
    public static function getAskapplicationType(){
        return array(
            '1'=>'消费贷款',
            '2'=>'经营贷款',
            '3'=>'买车贷款',
            '4'=>'买房贷款'
        );
    }

    //add by zhangji at 2013.5.9,edm推送配置
    public static function getEdmTuisongConfig()
    {
        $arr_chufa = self :: getEdmTuisongConfig_chufa();
        $arr_feichufa = self :: getEdmTuisongConfig_feichufa();
        $arr = $arr_chufa + $arr_feichufa;        
        //$arr = array_unique($arr);        
        return $arr;
    }
    //触发类邮件edm
    public static function getEdmTuisongConfig_chufa()
    {
        return array(
            EDM_TUISONG_TYPE_PRODUCT => '产品推送',
            EDM_TUISONG_TYPE_SUCC    => '成功放款',
            EDM_TUISONG_TYPE_ANSWER  => '产品页问题回答',
            EDM_TUISONG_TYPE_SHOUCANG=> '收藏',
            EDM_TUISONG_TYPE_CAILIAO => '通知准备材料',
            EDM_TUISONG_TYPE_SHENQING=> '通知申请成功',
            EDM_TUISONG_TYPE_WENDA   => '问答页回答',
            EDM_TUISONG_TYPE_SAVEARTICLE => '保存房贷文章',
            EDM_TUISONG_TYPE_SAVECALRESULT => '计算器保存结果',
            EDM_TUISONG_TYE_BANKRATECHANGE => '银行利率变化',
            EDM_TUISONG_TYE_CITYRATECHANGE => '城市利率变化',
            EDM_TUISONG_TYE_FANGDAI_SHOUCANG => '房贷产品收藏',
            EDM_TUISONG_TYE_FANGDAI_SHENGQING => '房贷申请成功',
            EDM_TUISONG_TYE_ASK_PASS => '问答审核成功',
        );
    }
    //非触发类邮件edm
    public static function getEdmTuisongConfig_feichufa()
    {
        return array(
            EDM_TUISONG_TYPE_HUODONG => '活动营销',            
        );
    }

    public static function getElementType()
    {
        return  array(
            ELEMENT_TYPE_ENUM    =>'枚举',
            ELEMENT_TYPE_COMPARE =>'比较',
        );
    }
    /*//配置城市，是否显示信贷员服务质量评价,以下城市暂时不上前端
    public static function cityBlackListForBankerSurveyScore()
    {
        //return array(32,6,8,15,18,29,10000,28,31,164,245);
        return array(6,8,15,18,28,29,31,32,164,245,10000);
    }
    //*/
    //配置城市，是否显示信贷员服务质量评价,以下城市上前端(标准产品)
    public static function cityWhiteListForBankerSurveyScore()
    {
        //2013.8.5 add by caoxiaolin，增加太原/郑州/南宁/潍坊
        return array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,22,23,24,25,26,27,29,30,31,32,33,164,99,198,245,19);
    }

    /*
     * 身份证过去时间下拉框的年份选项
     * */
    public static function idCardExpireYear()
    {
    	return array(
            2013,
            2014,
            2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,
            2025,2026,2027,2028,2029,2030,2031,2032,2033,2034,
            2035,2036,2037,2038,2039,2040,2041,2042,2043,2044,
            2045,2046,2047,2048,2049,2050
        );
    }
    
    /*
     * 身份证过去时间下拉框的月份选项
     * */
    public static function idCardExpireMonth()
    {
        return array(
            1,2,3,4,5,6,7,8,9,10,11,12
        );
    }
    
    /*
     * 身份证过去时间下拉框的 日选项
     * */
    public static function idCardExpireDay()
    {
        return array(
            1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31
        );
    }
    
    /*
     * 针对产品出价的出价类型
     * */
    public static function productBiddingTypes()
    {
    	return array(
            BIDDING_TYPE_STD_PRODUCT,
            BIDDING_TYPE_OPEN_PRODUCT,
            BIDDING_TYPE_CARD,
            BIDDING_TYPE_SPECIAL_AGENT);
    }
    
    /*
     * 针对城市出价的出价类型
     * */
    public static function cityBiddingTypes()
    {
        return array(BIDDING_TYPE_FANGDAI,BIDDING_TYPE_AGENT,BIDDING_TYPE_BANKER);
    }
    
    /*
     * 取消推广之后，需要走CRM审核流程的两类推广
     * 1.房贷担保
     * 2.标准贷款
     * */
    public static function QuitWithAuthBiddingTypes()
    {
        return array(BIDDING_TYPE_FANGDAI,BIDDING_TYPE_STD_PRODUCT);
    }
    
    /*
     * 直接取消推广，不需要走CRM审核
     * 1.中介代理
     * 2.开放产品
     * 3.顾问推广
     * 4.信用卡
     * */
    public static function QuitBiddingDirectlyTypes()
    {
        return array(
            BIDDING_TYPE_CARD,
            BIDDING_TYPE_AGENT,
            BIDDING_TYPE_SPECIAL_AGENT,
            BIDDING_TYPE_OPEN_PRODUCT,
            BIDDING_TYPE_BANKER,
        	BIDDING_TYPE_STD_PRODUCT
        );
    }
    
    /*
     * 竞价位类型和前端展示的竞价位名称的对应关系
     * */
    public static function BiddingTypeNameMap()
    {
    	return array(
            BIDDING_TYPE_STD_PRODUCT    =>'机构产品推广',
            BIDDING_TYPE_OPEN_PRODUCT   =>'个人产品推广',
            BIDDING_TYPE_AGENT          =>'抵押中介推广',
            BIDDING_TYPE_BANKER         =>'店铺推广',
            BIDDING_TYPE_FANGDAI        =>'房贷中介推广',
            BIDDING_TYPE_SPECIAL_AGENT  =>'专属中介推广',
            BIDDING_TYPE_CARD           =>'信用卡推广',
        );
    }
    
    /**
     * 有些竞价位不方便展示给所有外部用户（bd平台和前端），所以这里单独列出一个方法
     * @return multitype:string
     */
   public static function BiddingTypeNameMapForCrm()
    {
        return array(
            BIDDING_TYPE_STD_PRODUCT        =>'机构产品推广',
            BIDDING_TYPE_OPEN_PRODUCT       =>'个人产品推广',
            BIDDING_TYPE_AGENT              =>'抵押中介推广',
            BIDDING_TYPE_BANKER             =>'店铺推广',
            BIDDING_TYPE_FANGDAI            =>'房贷中介推广',
            BIDDING_TYPE_SPECIAL_AGENT      =>'专属中介推广',
            BIDDING_TYPE_BANK_PUSH_ORDER    =>'安家银行',
            BIDDING_TYPE_CONSULTANT_ORDER   =>'安家咨询',
            BIDDING_TYPE_CALL_OUT           =>'上海外呼',
            BIDDING_TYPE_AJSH_PUSH_ORDER    =>'安家手推',
            BIDDING_TYPE_RISK_FLY_ORDER     => '风控飞单',
            BIDDING_TYPE_CARD               =>'信用卡推广',
        );
    }
   
     /*
     * 展示BD新页面的城市id
     * 以下这些城市，有得分的信贷员数量>=50个 and 有得分的信贷员比例>=20%
     * 会根据PM需要添加新城市进来
     * */
    public static function BdNewIndexCitys()
    {
        $citys=array();
        $citys[]=1;//1   上海
        $citys[]=2;//2   北京
        $citys[]=3;//3   深圳
        $citys[]=4;//4   广州
        $citys[]=5;//5   天津
        $citys[]=6;//6   佛山
        $citys[]=7;//7   南京
        $citys[]=9;//9   苏州
        $citys[]=10;//10  无锡
        $citys[]=11;//11  济南
        $citys[]=12;//12  青岛
        $citys[]=16;//16  东莞
        $citys[]=18;//18  中山
        $citys[]=21;//21  杭州
        $citys[]=22;//22  宁波
        $citys[]=23;//23  沈阳
        $citys[]=25;//25  重庆
        $citys[]=26;//26  成都
        $citys[]=27;//27  厦门
        $citys[]=29;//29  长沙
        $citys[]=30;//30  西安
        $citys[]=31;//31  福州
        $citys[]=33;//33  武汉
        $citys[]=164;//164 合肥
        
        return $citys;
    }
    
    /**
     * 信贷员的生命周期定义
     */
    public static function getLifecycleStatusArray(){
        return array(
            BANKER_LIFECYCLE_INVALID => '无效客户',
            BANKER_LIFECYCLE_POTENTIAL => '潜在客户',
            BANKER_LIFECYCLE_DAITUIGUANG => '待推广',
            BANKER_LIFECYCLE_CHANGSHI => '尝试',
            BANKER_LIFECYCLE_WENDING => '稳定',
            BANKER_LIFECYCLE_LIUSHI => '流失',
            BANKER_LIFECYCLE_WANHUI => '挽回',
            BANKER_LIFECYCLE_EXIT => '退出'
        );
    }
    
    /**
     * 信贷员的跟进状态(CRM2.0)
     * 处于不同生命周期的信贷员有不同的跟进状态集合
     */
    public static function getFollowStatusArray($lifeCyclePhase=0){
        $statusWithLifephaseArray =  array(
            BANKER_LIFECYCLE_POTENTIAL => array(
                BANKERAPPLY_STATUS_NEW => '未联系',
                BANKER_FOLLOW_CONNECTFAIL => '联系不上',
                BANKERAPPLY_STATUS_CONNECTING => '客户考虑',
                BANKERAPPLY_STATUS_WAIT_REG => '等待注册',
                BANKERAPPLY_STATUS_REG => '潜客转入注册',
                BANKERAPPLY_STATUS_BANKER_REFUSE => '客户拒绝',
                BANKERAPPLY_STATUS_FAKE => '无效客户',
                BANKERAPPLY_STATUS_DOWNTIME => '手机停机',
                BANKERAPPLY_STATUS_EMPTY => '手机空号',
                BANKERAPPLY_STATUS_CREDIT => '信用卡业务员',
                    BANKER_DETAIL_MISSED => '资料缺失',
                    BANKER_DETAIL_MISMATCH => '资料不符',
                    BANKER_DETAIL_NOTQUALIFIED => '资质不足',
                    BANKER_DETAIL_PENDINGAUTH => '等待再次认证',
                    BANKER_DETAIL_EBANKISSUE => '网银问题',
                    BANKER_DETAIL_OPERATIONISSUE => '操作问题',
                    BANKER_DETAIL_CHANGSHI => '客户仅尝试',
                    BANKER_DETAIL_ACCOUNTANTISSUE => '财务确认问题',
                    BANKER_BC_CONFIRM_TUIFEI=>'退费确认',
                    BANKER_DETAIL_DAIKUAN_USER=>'贷款用户',
                    BANKER_WAIT_RECHARGE_MARGIN=>'等待充值保证金',
                    BANKER_WAIT_REFUND_MARGIN=>'等待退款保证金',
            ),
            BANKER_LIFECYCLE_DAITUIGUANG => array(
                BANKERAPPLY_STATUS_NEW => '未联系',
                BANKER_FOLLOW_CONNECTFAIL => '联系不上',
                BANKERAPPLY_STATUS_CONNECTING => '客户考虑',
                BANKER_FOLLOW_WAITAUTH => '等待认证',
                BANKER_FOLLOW_WAITCHARGE => '等待充值',
                BANKER_FOLLOW_WAITTUIGUANG => '等待推广',
                BANKERAPPLY_STATUS_BANKER_REFUSE => '客户拒绝',
                BANKERAPPLY_STATUS_FAKE => '无效客户',
                BANKERAPPLY_STATUS_DOWNTIME => '手机停机',
                BANKERAPPLY_STATUS_EMPTY => '手机空号',
                BANKERAPPLY_STATUS_CREDIT => '信用卡业务员',
                BANKER_TO_LOAN => '转介给贷款',
               // BANKER_TO_CREDIT => '转介给信用卡',
                    BANKER_DETAIL_MISSED => '资料缺失',
                    BANKER_DETAIL_MISMATCH => '资料不符',
                    BANKER_DETAIL_NOTQUALIFIED => '资质不足',
                    BANKER_DETAIL_PENDINGAUTH => '等待再次认证',
                    BANKER_DETAIL_EBANKISSUE => '网银问题',
                    BANKER_DETAIL_OPERATIONISSUE => '操作问题',
                    BANKER_DETAIL_CHANGSHI => '客户仅尝试',
                    BANKER_DETAIL_ACCOUNTANTISSUE => '财务确认问题',
                    BANKER_BC_CONFIRM_TUIFEI=>'退费确认',
                    BANKER_DETAIL_DAIKUAN_USER=>'贷款用户',
                BANKER_WAIT_RECHARGE_MARGIN=>'等待充值保证金',
                BANKER_WAIT_REFUND_MARGIN=>'等待退款保证金',
            ),
            BANKER_LIFECYCLE_CHANGSHI => array(
                BANKER_FOLLOW_CONNECTFAIL => '联系不上',
                BANKER_FOLLOW_WAITCHARGE => '等待充值',
                BANKER_FOLLOW_PENDING => '业务暂停(休假)',
                BANKER_FOLLOW_QUIT => '客户退出',
                BANKERAPPLY_STATUS_FAKE => '无效客户',
                BANKERAPPLY_STATUS_DOWNTIME => '手机停机',
                BANKERAPPLY_STATUS_EMPTY => '手机空号',
				BANKER_FOLLOW_IN_COOPERATION => '稳定合作中',
                BANKERAPPLY_STATUS_CREDIT => '信用卡业务员',
                    BANKER_TO_LOAN => '转介给贷款',
                   // BANKER_TO_CREDIT => '转介给信用卡',
                    BANKER_DETAIL_MISSED => '资料缺失',
                    BANKER_DETAIL_MISMATCH => '资料不符',
                    BANKER_DETAIL_NOTQUALIFIED => '资质不足',
                    BANKER_DETAIL_PENDINGAUTH => '等待再次认证',
                    BANKER_DETAIL_EBANKISSUE => '网银问题',
                    BANKER_DETAIL_OPERATIONISSUE => '操作问题',
                    BANKER_DETAIL_CHANGSHI => '客户仅尝试',
                    BANKER_DETAIL_ACCOUNTANTISSUE => '财务确认问题',
                    BANKER_BC_CONFIRM_TUIFEI=>'退费确认',
                    BANKER_DETAIL_DAIKUAN_USER=>'贷款用户',
                BANKER_WAIT_RECHARGE_MARGIN=>'等待充值保证金',
                BANKER_WAIT_REFUND_MARGIN=>'等待退款保证金',
            ),
            BANKER_LIFECYCLE_WENDING => array(
                BANKER_FOLLOW_CONNECTFAIL => '联系不上',
                BANKER_FOLLOW_WAITCHARGE => '等待充值',
                BANKER_FOLLOW_PENDING => '业务暂停(休假)',
                BANKER_FOLLOW_QUIT => '客户退出',
                BANKERAPPLY_STATUS_FAKE => '无效客户',
                BANKERAPPLY_STATUS_DOWNTIME => '手机停机',
                BANKERAPPLY_STATUS_EMPTY => '手机空号',
				BANKER_FOLLOW_IN_COOPERATION => '稳定合作中',
                BANKER_FOLLOW_CHENGSHU => '新成熟客户',
                BANKERAPPLY_STATUS_CREDIT => '信用卡业务员',
                    BANKER_TO_LOAN => '转介给贷款',
                   // BANKER_TO_CREDIT => '转介给信用卡',
                    BANKER_DETAIL_MISSED => '资料缺失',
                    BANKER_DETAIL_MISMATCH => '资料不符',
                    BANKER_DETAIL_NOTQUALIFIED => '资质不足',
                    BANKER_DETAIL_PENDINGAUTH => '等待再次认证',
                    BANKER_DETAIL_EBANKISSUE => '网银问题',
                    BANKER_DETAIL_OPERATIONISSUE => '操作问题',
                    BANKER_DETAIL_CHANGSHI => '客户仅尝试',
                    BANKER_DETAIL_ACCOUNTANTISSUE => '财务确认问题',
                    BANKER_BC_CONFIRM_TUIFEI=>'退费确认',
                    BANKER_DETAIL_DAIKUAN_USER=>'贷款用户',
                BANKER_WAIT_RECHARGE_MARGIN=>'等待充值保证金',
                BANKER_WAIT_REFUND_MARGIN=>'等待退款保证金',
            ),
            BANKER_LIFECYCLE_WANHUI => array(
                BANKER_FOLLOW_CONNECTFAIL => '联系不上',
                BANKER_FOLLOW_WAITCHARGE => '等待充值',
                BANKER_FOLLOW_PENDING => '业务暂停(休假)',
                BANKER_FOLLOW_QUIT => '客户退出',
                BANKERAPPLY_STATUS_FAKE => '无效客户',
                BANKERAPPLY_STATUS_DOWNTIME => '手机停机',
                BANKERAPPLY_STATUS_EMPTY => '手机空号',
                BANKERAPPLY_STATUS_CREDIT => '信用卡业务员',
				BANKER_FOLLOW_IN_COOPERATION => '稳定合作中',
                    BANKER_TO_LOAN => '转介给贷款',
                  //  BANKER_TO_CREDIT => '转介给信用卡',
                    BANKER_DETAIL_MISSED => '资料缺失',
                    BANKER_DETAIL_MISMATCH => '资料不符',
                    BANKER_DETAIL_NOTQUALIFIED => '资质不足',
                    BANKER_DETAIL_PENDINGAUTH => '等待再次认证',
                    BANKER_DETAIL_EBANKISSUE => '网银问题',
                    BANKER_DETAIL_OPERATIONISSUE => '操作问题',
                    BANKER_DETAIL_CHANGSHI => '客户仅尝试',
                    BANKER_DETAIL_ACCOUNTANTISSUE => '财务确认问题',
                    BANKER_BC_CONFIRM_TUIFEI=>'退费确认',
                    BANKER_DETAIL_DAIKUAN_USER=>'贷款用户',
                BANKER_WAIT_RECHARGE_MARGIN=>'等待充值保证金',
                BANKER_WAIT_REFUND_MARGIN=>'等待退款保证金',
            ),  
            BANKER_LIFECYCLE_LIUSHI => array(
                    BANKER_FOLLOW_CONNECTFAIL => '联系不上',
                    BANKERAPPLY_STATUS_CONNECTING => '客户考虑',
                    BANKER_FOLLOW_WAITAUTH => '等待认证',
                    BANKER_FOLLOW_WAITCHARGE => '等待充值',
                    BANKER_FOLLOW_WAITTUIGUANG => '等待推广',
                    BANKERAPPLY_STATUS_BANKER_REFUSE => '客户拒绝',
                    BANKERAPPLY_STATUS_FAKE => '无效客户',
                    BANKERAPPLY_STATUS_DOWNTIME => '手机停机',
                    BANKERAPPLY_STATUS_EMPTY => '手机空号',
                    BANKERAPPLY_STATUS_CREDIT => '信用卡业务员',
                    BANKER_TO_LOAN => '转介给贷款',
                //    BANKER_TO_CREDIT => '转介给信用卡',
                    BANKER_DETAIL_MISSED => '资料缺失',
                    BANKER_DETAIL_MISMATCH => '资料不符',
                    BANKER_DETAIL_NOTQUALIFIED => '资质不足',
                    BANKER_DETAIL_PENDINGAUTH => '等待再次认证',
                    BANKER_DETAIL_EBANKISSUE => '网银问题',
                    BANKER_DETAIL_OPERATIONISSUE => '操作问题',
                    BANKER_DETAIL_CHANGSHI => '客户仅尝试',
                    BANKER_DETAIL_ACCOUNTANTISSUE => '财务确认问题',
                    BANKER_BC_CONFIRM_TUIFEI=>'退费确认',
                    BANKER_DETAIL_DAIKUAN_USER=>'贷款用户',
                BANKER_WAIT_RECHARGE_MARGIN=>'等待充值保证金',
                BANKER_WAIT_REFUND_MARGIN=>'等待退款保证金',
            ),
            BANKER_LIFECYCLE_EXIT => array(
                    BANKERAPPLY_STATUS_NEW => '未联系',
                    BANKERAPPLY_STATUS_CONNECTING => '客户考虑',
                    BANKER_FOLLOW_CONNECTFAIL => '联系不上',
                    BANKERAPPLY_STATUS_BANKER_REFUSE => '客户拒绝',
                    BANKERAPPLY_STATUS_FAKE => '无效客户',
                    BANKERAPPLY_STATUS_DOWNTIME => '手机停机',
                    BANKERAPPLY_STATUS_EMPTY => '手机空号',
                    BANKERAPPLY_STATUS_CREDIT => '信用卡业务员',
                    BANKER_FOLLOW_WAITAUTH => '等待认证',
                    BANKER_TO_LOAN => '转介给贷款',
                 //   BANKER_TO_CREDIT => '转介给信用卡',
                    BANKER_DETAIL_MISSED => '资料缺失',
                    BANKER_DETAIL_MISMATCH => '资料不符',
                    BANKER_DETAIL_NOTQUALIFIED => '资质不足',
                    BANKER_DETAIL_PENDINGAUTH => '等待再次认证',
                    BANKER_DETAIL_EBANKISSUE => '网银问题',
                    BANKER_DETAIL_OPERATIONISSUE => '操作问题',
                    BANKER_DETAIL_CHANGSHI => '客户仅尝试',
                    BANKER_DETAIL_ACCOUNTANTISSUE => '财务确认问题',
                    BANKER_BC_CONFIRM_TUIFEI=>'退费确认',
                    BANKER_DETAIL_DAIKUAN_USER=>'贷款用户',
                BANKER_WAIT_RECHARGE_MARGIN=>'等待充值保证金',
                BANKER_WAIT_REFUND_MARGIN=>'等待退款保证金',
            ),
        );
        if($lifeCyclePhase){
            return $statusWithLifephaseArray[$lifeCyclePhase];
        }
        $fullsetFollowStatus = array();
        foreach($statusWithLifephaseArray as $item){
            foreach($item as $key => $subitem){
                $fullsetFollowStatus[$key] = $subitem;
            }
        }
        return $fullsetFollowStatus;
    }

    /*
     * 查询订单列表时，会用到集合状态的查询，这里提供两个集合，最终状态订单和非最终状态订单
     * */
    public static function OrderStatusGroups()
    {
    	return array(
            ORDER_STATUS_GROUP_FINAL_STATUS=>array(
                ORDER_STATUS_KONGHAO,
                ORDER_STATUS_LIANXIBUSHANG,
                ORDER_STATUS_SHIYONG,
                ORDER_STATUS_WEIMAO,
                ORDER_STATUS_FANGQI,
                ORDER_STATUS_BEIJUJUE,
                ORDER_STATUS_BEIJUJUE_FEIDAN,
                ORDER_STATUS_FANGQI_FEIDAN,
                ORDER_STATUS_FANGQI_WEILIANXI,
                ORDER_STATUS_SHENPIBEIJU,
                ORDER_STATUS_SHENPITONGGUO,
                ORDER_STATUS_SHENPIZHONG,
                ORDER_STATUS_CHENGGONGFANGKUAN
            ),
            ORDER_STATUS_GROUP_NOT_FINAL_STATUS=>array(
                ORDER_STATUS_TIJIAOSHENQING,
                ORDER_STATUS_WEILIANXI_1,
                ORDER_STATUS_WEILIANXI_2,
                ORDER_STATUS_YILIANXI,
                ORDER_STATUS_CHUBUMANZU,
                ORDER_STATUS_YONGHUKAOLV,
                ORDER_STATUS_YONGHUKAOLVDAOQI,
            ),
            ORDER_STATUS_GROUP_NOT_NEW=>array(
                ORDER_STATUS_KONGHAO,
                ORDER_STATUS_LIANXIBUSHANG,
                ORDER_STATUS_SHIYONG,
                ORDER_STATUS_WEIMAO,
                ORDER_STATUS_FANGQI,
                ORDER_STATUS_BEIJUJUE,
                ORDER_STATUS_BEIJUJUE_FEIDAN,
                ORDER_STATUS_FANGQI_FEIDAN,
                ORDER_STATUS_FANGQI_WEILIANXI,
                ORDER_STATUS_SHENPIBEIJU,
                ORDER_STATUS_SHENPITONGGUO,
                ORDER_STATUS_SHENPIZHONG,
                ORDER_STATUS_CHENGGONGFANGKUAN,
                ORDER_STATUS_WEILIANXI_1,
                ORDER_STATUS_WEILIANXI_2,
                ORDER_STATUS_YILIANXI,
                ORDER_STATUS_CHUBUMANZU,
                ORDER_STATUS_YONGHUKAOLV,
                ORDER_STATUS_YONGHUKAOLVDAOQI,
            ),
        );
    }

    /**
     * 车贷流程
     * add by caoxiaolin at 2013.9.12
     */
    public static function ChedaiProcess()
    {
        return array(
            1 => '看车->签合同->办理贷款->放款->提车',
            2 => '看车->签合同->办理贷款->提车'
        );
    }

    /**
     * 车贷支持品牌
     * add by caoxiaolin at 2013.9.12
     */
    public static function ChedaiBrand()
    {
        return array(
            1  => '大众',
            2  => '丰田',
            3  => '本田',
            4  => '日产',
            5  => '现代',
            6  => '宝马',
            7  => '奔驰',
            8  => '福特',
            9  => '起亚',
            10 => '别克',
            11 => '奥迪',
            12 => '雪佛兰',
            13 => '路虎',
            14 => '奇瑞',
            15 => '马自达',
            16 => '雪铁龙',
            17 => '长安轿车',
            18 => '标致',
            19 => '斯柯达',
            20 => '吉利',
            21 => '三菱',
            22 => '荣威',
            23 => '中华',
            24 => 'MG',
            25 => '斯巴鲁',
            26 => '沃尔沃',
            27 => '凯迪拉克',
            28 => '东南',
            29 => 'MINI',
            30 => '英菲尼迪',
            31 => '五菱',
            32 => '比亚迪',
            33 => '长城',
            34 => '理念',
            35 => '哈弗',
            36 => '奔腾',
            37 => '海马',
            38 => '吉普',
            39 => '雷克萨斯',
            40 => '众泰',
            41 => '宝骏',
            42 => '力帆',
            43 => '道奇',
            44 => '克莱斯勒',
            45 => '欧朗',
            46 => '启辰',
            47 => '纳智捷',
            48 => '华泰',
            49 => '开瑞',
            50 => '陆风',
            51 => '菲亚特',
            52 => '雷诺',
            53 => '北汽',
            54 => '瑞麒',
            55 => '猎豹汽车',
            56 => '林肯',
            57 => 'Smart',
            58 => '双龙',
            59 => '莲花',
            60 => '红旗',
        );
    }

    public static function getExtraPermitConfig()
    {
        return array(
            123,//您的文化程度
            135,//公司规模
            39, //年龄
            139,//您的居住类型
            137,//现居住地居住时间
            9,  //您的职位
            89, //就职公司类型
            122 //您的婚姻状况
        ); 
    }

	 /**
     * 代金券活动支持的城市
     */
	public static function getCouponActiveCity()
    {
        return array(
            5   => '天津',
            11  => '济南',
            12  => '青岛',
            13  => '烟台',
            19  => '潍坊',
            20  => '淄博',
            23  => '沈阳',
            24  => '大连',
            63  => '临沂',
            90  => '石家庄',
            198 => '郑州',
            7   => '南京',
            9   => '苏州',
            10  => '无锡',
            14  => '常州',
            17  => '南通',
            21  => '杭州',
            22  => '宁波',
            33  => '武汉',
            158 => '绍兴',
            164 => '合肥',
            6   => '佛山',
            16  => '东莞',
            18  => '中山',
            29  => '长沙',
            25  => '重庆',
            27  => '厦门',
            30  => '西安',
            31  => '福州',
            99  => '太原',
            143 => '哈尔滨',
            187 => '南昌',
            245 => '南宁'
        );
    }
    
    public static function getIsorgContactStatus(){
    	return array(
    			IS_ORG_CONTACT_STATUS_NEW => '新录入',
    			IS_ORG_CONTACT_STATUS_CHUBULIANXI => '初步联系',
    			IS_ORG_CONTACT_STATUS_WAITING => '等待答复',
    			IS_ORG_CONTACT_STATUS_QIATAN => '洽谈中',
    			IS_ORG_CONTACT_STATUS_SIGN => '洽谈成功',
    			IS_ORG_CONTACT_STATUS_TRANSFER => '转介给别人',
    			IS_ORG_CONTACT_STATUS_REJECT => '拒绝合作'
    	);
    }
    
    public static function getIsorgStatus(){
    	return array(
    			IS_ORG_STATUS_NEW => '新录入',
    			IS_ORG_STATUS_CHUBULIANXI => '初步联系',
    			IS_ORG_STATUS_PRODUCT_SUMMARY => '已获取产品大纲',
    			IS_ORG_STATUS_QIATAN => '洽谈合作方案',
    			IS_ORG_STATUS_SIGN => '已签约',
    			IS_ORG_STATUS_REJECT => '拒绝合作',
    			IS_ORG_STATUS_LIZI => '转对公例子合作',
    	);
    }
    
    public static function getIsorgTripStatus(){
    	return array(
    		IS_ORG_TRIP_STATUS_PLANING => '计划中',
    		IS_ORG_TRIP_STATUS_PROCESSING => '拜访进行中',
    		IS_ORG_TRIP_STATUS_FINISHED => '拜访已结束',
    	);
    }
	
	 /**
     * 代金券活动过滤的机构名称中关键字
     */
	public static function getCouponFilterBank()
    {
        return array(
			'宜信',
			'平安',
			'维信',
			'友信',
			'证大',
			'中安信业',
			'中兴微贷',
			'亚联财',
			'融易宝',
			'深圳安信',
			'巨汇财富',
			'江苏新景创投投资管理有限公司'
        );
    }
    
    /**
     * 获取城市分级管理后各级城市的机构准入
     * 1线城市准入为： 注册时间2年以上且注册资金50万元以上   或者注册时间1年以上且注册资金500万元以上   
     * 
     * 新规则 20151113
     * 级别录入机构产品条件（或加白名单）
                        一级城市1线城市准入为： 注册时间2年以上且注册资金50万元以上 或者注册时间1年
                        以上且注册资金500万元以上
                        二级城市2线城市准入为： 注册时间1年以上且注册资金50万元以上 或者注册时间0.5
                        年以上且注册资金100万元以上
                        三级城市3线城市准入为： 注册时间0.5年以上且注册资金100万元以上
                        四级城市4线城市准入为： 注册时间3个月以上
     */
    public static function getBankPermitByCityLevel(){
        return array(
                '1' => array(array('regTime' => 2, 'regCapital' => 50), array('regTime' => 1, 'regCapital' => 500)),
                '2' => array(array('regTime' => 1, 'regCapital' => 50), array('regTime' => 0.5, 'regCapital' => 100)),
                '3' => array(array('regTime' => 0.5, 'regCapital' => 100)),
                '4' => array(array('regTime' => 0.25, 'regCapital' => 0)),
        );
    }

	/**
     * 代金券活动过滤的不合作的信贷员
     */
	public static function getCouponFilterBanker()
    {
        return array(
			9430,
        );
    }

    public function getThroughTrainBasicPermitConfig()
    {
        return array(
            '88', //职业身份    
            '12', //打卡收入
            '71', //现金收入
            '58', //工作年限
            '77', //是否缴纳公积金
            '43', //是否缴存本地社保
            '49', //经营总流水
            '50', //对公流水
            '91', //现金结算
            '59', //经营年限
            '46', //经营注册地
            '93', //是否有营业执照
            '5',  //两年内信用情况
        );
    }

    public function getThroughTrainCarPermitConfig()
    {
        return array(
            '20', //您的房产类型
            '22', //住宅房龄
            '64', //商铺房龄
            '56', //房产估值
            '90', //是否有车辆
        );

    }
    public function getMobileExpressZhiyeshouruPermitConfig()
    {
        return array(
            '88', //职业身份    
            '12', //打卡收入
            '71', //现金收入
            '58', //工作年限
            '77', //是否缴纳公积金
            '43', //是否缴存本地社保
            '49', //经营总流水
            '50', //对公流水
            '91', //现金结算
            '59', //经营年限
            '46', //经营注册地
            '93', //是否有营业执照
        );
    }

    public function getMobileExpressZichanxinyongPermitConfig()
    {
        return array(
            '20', //您的房产类型
            '22', //住宅房龄
            '64', //商铺房龄
            '56', //房产估值
            '90', //是否有车辆
            '5',  //两年内信用情况
        );
    }

    public function getMobileExpressDiyadaiZichanxinyongPermitConfig()
    {
        return array(
            '20', //您的房产类型
            '21', //房产位置
            '22', //住宅房龄
            '23', //是否已被抵押
            '56', //房产估值
            '64', //商铺房龄
            '66', //是否有房产证
            '5',  //两年内信用情况
        );
    }
    public function getMobileExpressDiyadaiZhiyeshenfenPermitConfig()
    {
        return array(
            '88', //职业身份    
            '12', //打卡收入
            '71', //现金收入
            '58', //工作年限
            '77', //是否缴纳公积金
            '43', //是否缴存本地社保
            '49', //经营总流水
            '50', //对公流水
            '91', //现金结算
            '59', //经营年限
            '46', //经营注册地
            '93', //是否有营业执照
            '39', //年龄
            '45', //户籍
        );
    }

    public function getLoanNeedTime()
    {
        return array(
            '90' => '仅咨询，无所谓',
            '60' => '2个月以上',
            '30' => '1个月',
            '14' => '1-2周',
            '7' => '1周以内',
            //'3' => '3-5天',
        );
    }

    //double check 准入项(相当于产品的double check准入项)
    public function getDoubleCheckPermit()
    {
        return array(
            'single' => array(
                '0' => Array(
                    'id' => 108,
                    'type' => 2,
                    'data' => Array(
                        2
                    )
                ),
                '1' => Array(
                    'id' => 109,
                    'type' => 2,
                    'data' => Array(
                        2
                    )
                ),
                '2' => Array(
                    'id' => 112,
                    'type' => 2,
                    'data' => Array(
                        2
                    )
                ),
                '3' => Array(
                    'id' => 114,
                    'type' => 2,
                    'data' => Array(
                        2
                    )
                ),
                '4' => Array(
                    'id' => 117,
                    'type' => 2,
                    'data' => Array(
                        2
                    )
                ),
                '5' => Array(
                    'id' => 116,
                    'type' => 2,
                    'data' => Array(
                        1,
                        2,
                        3,
                        4,
                        14
                    )
                )
            )
        );
    }

    //外呼中心 信贷员列表配置
    public static function callOutConfig()
    {
        return array(
            '1' => array(
                '519' => array(
                    36
                ),
                '5'   => array(
                    8460   
                ),
                '8'   => array(
                    2463    
                ),
                '414' => array(
                    1393    
                )
            ),    
            '2' => array(
                '47' => array(
                    1648,  
                    1127,
                    1999
                )
            ),
            '3' => array(
                '84' => array(
                    2071    
                ),
            ),
            '25'=> array(
                '461'=> array(
                    3238,
                    4069 
                ),
                '529'=> array(
                    2701    
                )    
            ),
            '21'=> array(
                '413'=> array(
                    2484    
                )    
            )
        );
    }
    
    /**
     * 规定允许上传的文件扩展名
     * */
    public static function getAllowedFileTypes()
    {
        //2016-12-12 增加dat扩展名 by xiaochang
    	return array('zip','csv','txt','html','htm','xls','xlsx','pdf','doc','docx','dat', 'mp4');
    }

    //车贷优惠分类
    public static function chedaiTagCategory()
    {
        //2指利率类 1是首付类  0什么都不是 
        return array(
            '1'  => 2,
            '2'  => 2,
            '10' => 2,
            '20' => 2,
            '30' => 1,
            '40' => 1,
            '50' => 0,
            '60' => 0,
            '70' => 0
        );
    }

    //tag名称
    public static function tagNameMap()
    {
        return array(
            '1'  => '最低0利率',
            '2'  => '低首付低月供',
            '10' => '免息',    
            '20' => '贴息',
            '30' => '零首付',
            '40' => '低首付',
            '50' => '期限长',
            '60' => '申请人灵活',
            '70' => '手续简单'
        );
    }

	//呼入通话电话状态结果
	public static function getCallInStatus(){
		return array(
			CALL_STATUS_ZUOXIJIETING => '座席接听',
			CALL_STATUS_ZUOXIWEIJIETING => '座席未接听',
			CALL_STATUS_XITONGJIETING => '系统接听',
			CALL_STATUS_PEIZHICUOWU => '系统未接ivr配置错误',
			CALL_STATUS_TINGJI => '系统未接-停机',
			CALL_STATUS_QIANFEI => '系统未接-欠费',
			CALL_STATUS_HEIMINGDAN => '系统未接-黑名单',
			CALL_STATUS_WEIZHUCE => '系统未接-未注册',
			CALL_STATUS_CAILING => '系统未接-彩铃',
		        CALL_STATUS_400WEIJIESHOU => '网上400未接受',
			CALL_STATUS_QITACUOWU => '其他错误',
                        CALL_STATUS_VLINKWEIJIETONG => '未接听',
		        CALL_STATUS_VLINKYIJIETONG => '已接听',
			CALL_STATUS_VLINKYIZHUANJIE => '已呼转',
			CALL_STATUS_VLINKHUZHUANYIQIAOJIE => '已桥接'
		);
	}

	//外呼通话结果状态
	public static function getCallStatus(){
		return array(
			CALL_STATUS_NOT_ANSWER_TIMEOUT => '客户未接听',
			CALL_STATUS_NOT_ANSWER_EMPTY => '客户未接听',
			CALL_STATUS_AT_NOT_ANSWER => '座席未接听',
			CALL_STATUS_TWO_SIDE_ANSWER => '双方接听',
		);
	}

    //成功订单审核状态
    public static function getOrderReviewStatus()
    {
        return array(
            ORDER_REVIEW_CHECH_STATUS_WEISHEN       => '未审核',
            ORDER_REVIEW_CHECH_STATUS_SHENHEZHONG   => '审核中',
            ORDER_REVIEW_CHECH_STATUS_WUJIEGUO      => '审核无结果',
            ORDER_REVIEW_CHECH_STATUS_TONGGUO       => '审核通过',
            ORDER_REVIEW_CHECH_STATUS_BUTONGGUO     => '审核不通过',
        	ORDER_REVIEW_CHECH_STATUS_DONGJIE       => '审核冻结',
        	ORDER_REVIEW_CHECH_STATUS_WUXUSHENHE    => '无需审核',
        );
    }

	//成功订单审核原因
    public static function getOrderReviewCheckReason()
    {
        return array(
            ORDER_REVIEW_CHECH_STATUS_SHENHEZHONG => array(
				ORDER_REVIEW_CHECH_REASON_WUFALIANTONG      => '无法连通',
				ORDER_REVIEW_CHECH_REASON_SHAOHOUCHULI      => '稍后处理',
			),
            ORDER_REVIEW_CHECH_STATUS_WUJIEGUO => array(
				ORDER_REVIEW_CHECH_REASON_WUFALIANTONG       => '无法连通',
				ORDER_REVIEW_CHECH_REASON_LIANXIBUSHANG      => '空号、停机',
				ORDER_REVIEW_CHECH_REASON_YONGHUJUJUEHUIFANG => '用户拒绝回访',
			),
            ORDER_REVIEW_CHECH_STATUS_TONGGUO => array(
				ORDER_REVIEW_CHECH_REASON_CHENGGONGFANGKUAN  => '成功放款',
				ORDER_REVIEW_CHECH_REASON_YONGHUKAOLV        => '用户考虑',
				ORDER_REVIEW_CHECH_REASON_YONGHUFANGQI       => '用户放弃',
			),
            ORDER_REVIEW_CHECH_STATUS_BUTONGGUO  =>  array(
				ORDER_REVIEW_CHECH_REASON_WEIMAO             => '伪冒',
			)
        );
    }

    //楼盘筛选价格区间
    public static function getLoupanPriceConfig()
    {
        return array(
            '1' => array(
                'maxPrice' => 4000,
                'minPrice' => '',
                'name'    =>'4000元以下' 
            ),     
            '2' => array(
                'maxPrice' => 6000,
                'minPrice' => 4000,
                'name'    =>'4000-6000元' 
            ),     
            '3' => array(
                'maxPrice' => 8000,
                'minPrice' => 6000,
                'name'    =>'6000-8000元' 
            ),     
            '4' => array(
                'maxPrice' => 10000,
                'minPrice' => 8000,
                'name'    =>'8000-1万' 
            ),     
            '5' => array(
                'maxPrice' => 12000,
                'minPrice' => 10000,
                'name'    =>'1-1.2万' 
            ),     
            '6' => array(
                'maxPrice' => 15000,
                'minPrice' => 12000,
                'name'    =>'1.2-1.5万' 
            ),     
            '7' => array(
                'maxPrice' => 20000,
                'minPrice' => 15000,
                'name'    =>'1.5-2万' 
            ),     
            '8' => array(
                'maxPrice' => 30000,
                'minPrice' => 20000,
                'name'    =>'2-3万' 
            ),     
            '9' => array(
                'maxPrice' => '',
                'minPrice' => 30000,
                'name'    =>'3万以上' 
            )     
        );    
    }

    public static function getLoupanTags()
    {
        return $tagIdNameMap = array(
            LOUPAN_PUTONGZHUZHAI => '普通住宅',
            LOUPAN_BIESHU => '别墅',
            LOUPAN_GONGYU => '公寓',
            LOUPAN_QITA => '其他'
        );
    }
    
    public static function getRechargeTypes(){
        return array(
                RECHARGE_TYPE_LIZI => '例子充值', 
                RECHARGE_TYPE_QIANFEIBUJIAO => '欠费补缴', 
                RECHARGE_TYPE_TUIFEI => '退费', 
                RECHARGE_TYPE_XIAOGUOYUCHONG => '效果预充', 
                RECHARGE_TYPE_XUNI => '虚拟充值');
    }

    public static $loupan_open_citys = array(
        1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,21,22,23,24,25,26,27,29,30,31,32,33,48,52,53,54,60,72,90,91,95,96,99,110,134,143,156,158,164,187,198,200,218,245,259,261,270,303,326
    );
    
    public static function getTuifeiReasonWithTimes() {
        return array(
                '充值金额不达最低要求' => 0,
                '信贷员离职' => 1,
                '信贷员转行业' => 1,
                '账户转移' => 1,
                '对公或政策原因' => 1,
                '合规问题' => 1,
                '非本人或被封禁' => 1,
                '平台测试' => 1,
                '换城市'  => 1,
                '无单下线' => 1.2,
                '质量差下线' => 1.2,
                '其它（请在备注中说明）' => 1.2,
				'误操作' => 0,
			    '对平台规则不了解或不满意' => 1.2,
			    '对商务顾问不满意' => 1.2,
                '信贷员个人原因，无法继续合作' => 1,
        );
    }
    
    public static function getTuifeiReason() {
        return array(
            TUIFEI_REASON_CHONGZHILOW => '充值金额不达最低要求', 
            TUIFEI_REASON_LIZHI => '信贷员离职', 
            TUIFEI_REASON_ZHUANHANG => '信贷员转行业', 
            TUIFEI_REASON_ZHANGHUZHUANYI => '账户转移', 
            TUIFEI_REASON_POLICY => '对公或政策原因',
            TUIFEI_REASON_HEGUI => '合规问题',
            TUIFEI_REASON_BAN => '非本人或被封禁',
            TUIFEI_REASON_TEST => '平台测试',
            TUIFEI_REASON_HUANCHENGSHI => '换城市',
            TUIFEI_REASON_WUDAN => '无单下线',
            TUIFEI_REASON_ZHILIANGCHA => '质量差下线',
            TUIFEI_REASON_OTHER => '其它（请在备注中说明）',
			TUIFEI_REASON_WUCAOZUO => '误操作',
			TUIFEI_REASON_PINGTAIBUMANYI => '对平台规则不了解或不满意',
			TUIFEI_REASON_GUWENBUMANYI => '对商务顾问不满意',
            );
    }

    //默认金额下拉选项
    public static $default_select_limits = array(
        '0.3' => '0.3万元',
        '1' => '1万元',   
        '3' => '3万元',   
        '5' => '5万元',   
        '10' => '10万元',   
        '20' => '20万元',   
        '50' => '50万元',   
        '100' => '100万元',   
        'input' => '其他' 
    );
    //默认期限下拉选项
    public static $default_select_terms = array(
        '3' => '3个月',    
        '6' => '6个月',    
        '12' => '12个月',    
        '24' => '2年',    
        '36' => '3年',    
        '60' => '5年',    
        '120' => '10年',    
    );
	
	public static function getBankerTaskStages(){
		return array(
			BANKER_TASK_STAGE_REGISTERED =>'已注册未认证',
			BANKER_TASK_STAGE_AUTHED=>'已认证无产品',
			BANKER_TASK_STAGE_HAVEPRODUCT =>'有产品未推广', //
			BANKER_TASK_STAGE_BIDDING =>'已推广无首单', //
			BANKER_TASK_STAGE_HAVEORDER=>'完成首单之后',
		);
	}
	public static function getBankerTaskStageShows(){
		return array(
			0 => '免费注册',
			BANKER_TASK_STAGE_REGISTERED =>'轻松认证',
			BANKER_TASK_STAGE_AUTHED=>'完善店铺',
			BANKER_TASK_STAGE_HAVEPRODUCT =>'设置推广', //
			BANKER_TASK_STAGE_BIDDING =>'快速拿单', //
			BANKER_TASK_STAGE_HAVEORDER=>'任务完成',
		);
	}
	
	public static function getRoleTypes(){
	    return array(
	            ROLE_TYPE_COMMON => '一般岗',
	            ROLE_TYPE_MANAGER => '管理岗',
	            ROLE_TYPE_SALER => '销售岗'
	            );
	}
	
	public static function getTeamRoles(){
	    return array(
	            COUNTRY_LEADER => '全国LEADER',
	            ZONE_LEADER => '区LEADER', 
	            GROUP_LEADER => '组LEADER',
	            SELF_LEADER => 'BC',
	            );
	    
	}
	
	public static function  getCityOpenTypes(){
	    return array(
	            OPEN_TYPE_OPEN => '开放',
	            OPEN_TYPE_EXCLUSIVE => '专属',
	            );
	}
	
    //成熟信贷员的特征
	//目前状态
	public static function getBankerCharactorTodo(){
	    return array(
	            1 => '机构转换',
	            2 => '争议退费处理',
	            3 => '产品修改',
	            4 => '非以上原因的投诉处理',
	            5 => '其他，请具体说明',
	            6 => '无跟进事项',
	            7 => '城市转换',
	            );
	}
	//从业经历
	public static function getBankerCharactorLevel(){
	    return array(
	            1 => '团队经理',
	            2 => 'TOP Sales',
	            3 => '行业经验一年以上普通信贷员',
	            4 => '行业经验一年以下普通信贷员',
	    );
	}
	//充值习惯
	public static function getBankerCharactorRechargeHabit(){
	    return array(
	        1 => '不需要催每次自动充值',
    	    2 => '提醒后马上充值',
    	    3 => '提醒多次才会充值',
	    );
	}
	
	//能力
	public static function getBankerCharactorAbility(){
	    return array(
	        1 => '其他（需备注）',
    	    2 => '批出能力强',
    	    3 => '飞单能力强',
    	    4 => '消费能力强',
	    );
	}
	//反馈情况
	public static function getBankerCharactorFeedback(){
	    return array(
	        1 => '其他，请说明',
    	    2 => '订单均真实反馈',
    	    3 => '订单几乎不反馈',
    	    4 => '仅反馈无效订单及成功放款订单',
	    );
	}
	
	
	//对平台的了解
	public static function getBankerCharactorUnderstandingOfThePlatform(){
	    return array(
	        1 => '其他，请说明',
    	    2 => '平台规则已全部讲解，并了解',
    	    3 => '平台规则已讲解，但不够了解',
    	    4 => '针对有疑问规则已讲解，并了解',
    	    5 => '对平台规则基本了解',
    	    6 => '对平台规则完全不了解',
	    );
	}
	
	//对产品的了解
	public static function getBankerCharactorUnderstandingOfTheProduct(){
	    return array(
	        1 => '其他，请说明',
        	2 => '全部产品已讲解，并了解',
        	3 => '全部产品已讲解，部分已选择尝试',
        	4 => '全部产品已讲解，但仍会提出较多疑问',
        	5 => '对产品基本了解',
        	6 => '对产品完全不了解',
	    );
	}
	
	//投诉情况
	public static function getBankerCharactorComplaints(){
	    return array(
	        1 => '其他，请说明',
    	    2 => '对订单质量质疑',
    	    3 => '对平台规则有质疑',
    	    4 => '投诉过商务顾问',
    	    5 => '更换过商务顾问',
	        6 => '无投诉',
	    );
	}
	//沟通方式
	public static function getBankerCharactorCommunicationMode(){
	    return array(
	        1 => '其他，请说明',
    	    2 => '基本微信沟通，重要事情打电话',
    	    3 => '基本电话沟通',
    	    4 => '随时可以联系到',
    	    5 => '只有上班时间能联系到',
    	    6 => '晚上才会联系',
    	    7 => '周末会经常联系',
	    );
	}
	//对新业务的态度
	public static function getBankerCharactorNewBusinessAttitude(){
	    return array(
	        1 => '其他，请说明',
	        2 => '会自主尝试新业务',
	        3 => '在商务顾问引导下会尝试新业务',
	        4 => '不接受新业务',
	    );
	}
	
	//抱怨情况
	public static function getBankerCharactorComplained(){
	    return array(
	        1 => '其他，请说明',
	        2 => '时常抱怨',
	        3 => '偶尔抱怨',
	    );
	}
	//作弊情况
	public static function getBankerCharactorCheating(){
	    return array(
	        1 => '其他',
	        2 => '无作弊',
	        3 => '曾出现无效订单作弊',
	        4 => '曾出现成功放款订单作弊',
	        5 => '曾出现过无效和成功放款均作弊',
	    );
	}
	//评分
	public static function getBankerCharactorScore(){
	    return array(
	        1 => '100分，信息完全符合',
	        2 => '80分，信息基本让人满意，2处错误以内',
	        3 => '60分，信息需要更改超过3处',
	        4 => '40分，缺信息和错误超过5处',
	        5 => '20分，填写信息基本错误',
	        6 => '0分，未填写任何信息',
	    );
	}
	//是否被用户投诉过
	public static function getBankerUserComplained(){
	    return array(
	        0 => '否',
	        1 => '是',
	        
	    );
	}
	//字段名字
	public static function getBankerCharactorName(){
	    return array(
	        'charactor_level' => '从业经历',
	        'charactor_ability' => '能力',
	        'charactor_recharge_habit' => '充值习惯',
	        'charactor_feedback' => '反馈情况',
	        'charactor_cheating' => '作弊情况',
	        'charactor_understanding_of_the_product' => '对产品的了解',
	        'charactor_understanding_of_the_platform' => '对平台规则的了解',
	        'charactor_complained' => '抱怨情况',
	        'charactor_user_complained' => '是否被用户投诉过',
	        'charactor_complaints' => '投诉情况',
	        'charactor_new_business_attitude' => '对新业务的态度',
	        'charactor_communication_mode' => '沟通方式',
	        'charactor_todo' => '目前状态',
	        'charactor_note' => '备注说明',
	        'charactor_score' => '评分', 
	    );
	}

	public static function getBankerCharactorCreditLevel(){
	    return array(
	            1 => '从不欠费，不需要催每次自动充值',
	            2 => '从不欠费，每次提醒后马上充值',
	            3 => '偶尔欠费，但提醒后马上充值',
	            4 => '每次都欠费，但提醒后马上充值',
	            5 => '每次都欠费，且每次需要多次提醒后才充值',
	    );
	}
	
	public static function getBankerCharactorAmountlimit(){
	    return array(
	            1 => '可以欠费，但不能超过500元',
	            2 => '可以欠费，但不能超过400元',
	            3 => '可以欠费，但不能超过300元',
	            4 => '可以欠费，但不能超过200元',
	            5 => '可以欠费，但不能超过100元',
	            6 => '绝对不能欠费',
	    );
	}
	
	public static function getBankerInvalidReason(){
	    return array(
	            1 => '联系不上',
	            2 => '非信贷员',
	            3 => '非本人',
	            4 => '转行',
	            5 => '非正规公司',
	            6 => '对公业务',
	            7 => '重复账号',
	            8 => '其它（请注明）',
	            );
	}
	
	//国庆节假期（因为14年十一数据做过手工校对，给误流入公共池客户及进入退出、流失客户增加了起始跟进时间，所以今年将不再做十一假期数据检查，否则会重复）
	//到2015年后，将再次放开此检查
	public static $celebrateNationaldayArray = array('10-01', '10-02', '10-03', '10-04', '10-05', '10-06', '10-07');
	//public static $celebrateNationaldayArray = array('02-18', '02-19', '02-20', '02-21', '02-22', '02-23', '02-24');
	
	//2017年春节假期，每年需要更新
	public static $springFestivalVocationArray = array('01-27', '01-28', '01-29', '01-30', '01-31', '02-01', '02-02');
	
	//花旗对公账号所属机构列表 add by xiaochang 2014-10-30
	public static function getHuaqiCooperationIds(){
		return array(7,38,111,364,452,5314,24945);
	}
	//ab测试全开城市列表 add by xiaochang 2014-10-30
	public static function getBdABTestOpenCityIds(){
		return array();//功能下线
		return array(9,26);
	}
	//ab测试样本信贷员名单 add by xiaochang 2014-10-30
	public static function getBdABTestOpenCityBankerIds(){
		return array();//功能下线
		return array(
				//深圳
				21882,9463,7533,20734,42869,3953,37059,25712,47944,4894,45666,55920,54374,9655,
				17530,34182,25869,12982,13241,21487,41095,55944,1462,24694,49809,41443,42345,23963,
				44294,35596,24293,18817,5797,4173,23339,32201,55946,21658,33693,37007,32442,33266,22621,
				43201,18738,29124,29925,9236,49713,43496,9734,26456,22206,36658,43692,4505,23660,32649,
				30484,19769,41380,33908,21200,23334,22133,22935,40960,44880,31866,44964,32693,42042,
				38632,34838,29781,22028,9466,16083,7327,25021,27756,37206,38803,43152,35619,47473,33446,
				46790,28497,31407,20598,41544,15086,22692,18886,24307,17051,29423,28237,32405,32999,35826,
				43687,3156,11497,15917,19767,20484,16769,21481,22208,22447,22267,22624,20503,22026,23353,
				18036,25686,26180,27074,25631,27729,28088,28198,29705,27289,24249,2213,19343,22560,31696,
				22729,28658,28624,26763,27964,34062,27375,34695,34804,7296,35908,37429,23866,32763,37983,
				14711,34845,38001,40774,35676,41823,42732,38337,42895,42491,44895,13499,42054,39595,18182,
				46578,49394,48230,50014,46751,50399,53310,18623,18131,848,9472,5187,6973,11375,12266,11810,
				14066,2721,1977,6959,15038,1452,13045,2817,11139,11157,16560,17085,18619,17727,3950,8853,
				5070,9582,4906,10921,10029,13539,13526,11714,14787,3163,14781,17050,18109,18434,9920,17720,
				16746,19571,19367,3398,3395,2665,8911,4954,886,6364,6105,4818,4697,7147,7562,7648,5159,8498,
				1969,9013,5125,5173,3911,5379,5572,4825,5946,6183,6220,5628,6379,6573,6601,6693,6709,6777,
				4164,7040,7279,7357,7407,7607,7613,7764,2889,7892,7917,8343,7401,8593,8808,9095,9143,9208,
				9285,9357,9535,9607,9617,9761,9779,9811,9905,10058,10131,10134,10203,10299,10330,10372,10508,
				10770,10883,10545,11029,11046,11171,11347,11417,11440,11508,11554,11710,11860,11955,12073,
				12134,11079,12502,12544,12728,12850,13161,13198,13291,11737,13422,13653,13842,13899,14003,
				14041,14218,14279,14303,14334,13823,14436,14508,6905,14791,15089,15583,15655,15698,15745,15883,
				15983,16188,16229,16322,16382,16419,16617,16630,16761,16852,17041,17122,17232,17294,17321,
				17216,17559,17582,17605,17721,17781,17886,17955,18001,18121,18152,18322,18497,18571,18597,
				18718,18752,18771,18822,18868,18896,19147,19297,19325,19409,19474,19490,10887,19732,19814,
				19928,19955,19820,20197,10845,20290,20424,20654,20715,20196,20800,20933,20971,21036,21093,
				19133,21186,21219,21216,21351,21360,21436,21520,21596,21679,21770,21799,21925,22122,22222,
				21826,22378,22519,22582,22673,5375,22827,22846,22934,23192,23363,22661,23490,23567,23574,23616,
				23731,23745,23858,22490,24186,24290,24323,24336,24344,24602,24628,24644,24708,24775,24809,
				24903,24959,25194,25227,25319,25457,25553,19958,25327,25643,25678,25715,25765,25823,25969,
				26049,26076,26340,26366,26415,3342,26549,26624,26689,26816,26853,20706,27027,27110,27201,26966,
				27280,27442,27518,27022,14762,27868,27889,28170,28217,26127,28429,27943,28583,28739,28921,
				29196,29414,26097,29555,29720,28455,29883,29958,29995,30045,30307,30324,30507,30633,30434,
				30787,30839,30903,30944,31015,31192,31387,31571,31197,31812,31851,31920,31969,32114,32169,
				32305,32332,32426,32620,10923,32824,32872,32919,32979,32206,33088,26688,34082,19819,18479,
				35283,35288,35362,35455,36538,36688,32268,37238,37138,37544,38173,38241,38259,39153,39055,
				39150,39541,39932,37686,40096,40575,40959,17732,37625,33387,15800,42944,43610,43621,43597,
				44389,12811,42079,44190,44433,36959,43883,45591,46007,43277,46324,45681,45700,45749,46224,
				33158,46707,47457,46768,47962,48444,48695,48369,14430,48944,48987,49873,50191,50504,29959,
				50288,47239,47446,51564,51629,51537,45720,50084,52269,47620,53554,53752,43792,54203,54537,
				34538,22360,55347,55417,55637,55914,53960,22736,56256,56794,56647,49678,
				//上海
				23973,16528,29138,39400,3785,45642,38320,37602,12902,28208,27751,47343,39839,46782,3480,48029,
				49619,44286,41406,37657,15779,53447,22396,28591,24166,1671,55553,36705,37112,2040,26547,5282,
				2095,30202,17032,13776,22254,6437,11833,1551,18402,4478,19411,6409,2428,4800,7951,9103,2782,
				11149,7210,6405,15647,17944,8588,18370,10871,24518,24292,29968,37319,22553,45653,12212,3715,
				11179,3183,4172,13543,3164,5549,5144,14464,17574,25774,16023,3914,49658,5819,4752,193,2225,3964,
				1453,4198,3731,6716,5213,4801,9038,24015,11935,28339,9218,11269,8513,23809,32663,5864,43711,
				6330,6956,22727,7012,36,10800,17270,11491,24001,4811,31674,36545,43019,46175,4549,1046,3908,
				9160,8151,6970,21,1594,5072,11429,3043,3968,14007,12139,14305,14506,17595,17798,18308,18811,
				17392,19200,875,19466,17684,21085,2263,23221,25106,25088,13685,25419,18394,8691,5419,24697,29566,
				25829,28988,28985,33506,33263,21344,10991,37510,37970,19914,43022,44076,33486,50998,34301,52527,
				50045,50627,55199,5111,1043,5178,5196,5212,5211,5258,5301,5314,5330,5370,5182,5420,5386,4055,
				5521,5612,5653,2930,5247,5739,5765,5822,5848,5895,5984,5238,6092,6199,6237,6252,6292,6442,6478,
				6499,6507,6571,5955,5347,6631,5761,1540,4634,6833,6874,6890,6939,167,7104,7024,7172,7194,7249,
				7278,7295,5881,7449,7508,2677,7622,7629,7645,7651,7687,7727,7778,1500,7821,7970,8096,8220,8276,
				8351,8442,8484,8538,7986,8688,8767,5435,8819,8876,8922,8986,9087,9121,8418,9295,9488,9671,5217,
				9864,9952,10106,10266,10294,10368,10375,9335,10011,10542,10564,10283,10763,10829,10953,11039,11087,
				11172,11227,11278,11348,11459,11552,11621,11681,11627,11752,11767,11934,11968,11499,12023,12089,
				12230,12322,12372,12496,12389,12629,12662,12670,12742,12769,12794,12899,12729,13005,13194,13398,
				13436,13518,13589,13730,13740,5806,5417,14001,14062,14165,14253,14347,13213,14453,14496,14527,14660,
				14620,14895,14964,15058,15078,15551,15589,15661,15732,15833,15921,15987,16135,16200,15538,16335,
				4613,16544,16582,16635,16729,16792,16899,16935,16955,16962,16847,17075,17121,17196,17229,15636,17254,
				17313,17355,17319,17551,17591,17639,17757,17916,17923,17940,18039,18073,18153,18176,18226,18208,
				18297,18340,18344,18444,17753,18514,18230,18748,18830,18852,18889,18926,18947,19158,19052,19187,
				3148,19278,19357,19406,19398,19529,19562,19633,19671,19735,19813,19853,19886,19923,19973,20005,20012,
				20183,20279,20332,20369,20420,20556,20637,20695,20750,20824,20842,20884,20996,21005,21051,21076,
				21126,21179,21210,21252,21316,21420,21450,21486,21519,21577,21614,21742,21795,21823,21991,22086,
				22159,22211,22336,22458,22549,22577,22623,22741,22949,23005,23049,23084,23115,22132,23233,23304,
				23313,23327,23415,23451,23475,23661,23707,23718,23767,23896,23945,23955,23171,24267,24313,24340,
				24443,5097,24653,24702,24816,24875,24925,24949,25014,3756,25119,25165,25206,25229,24040,25363,25405,
				25456,25524,25575,23262,25782,25883,25971,26028,26074,26159,26261,26346,26380,26501,26577,26707,
				26736,26818,26848,26906,26961,26969,26996,27019,27072,27113,27140,27306,27372,27333,27536,27625,
				27703,27774,27794,27791,28031,28095,28163,28207,28228,28406,28411,28542,28605,28748,28776,28819,
				28993,29052,29103,29166,29163,29161,29338,29352,29397,29483,29535,29206,24438,29763,29906,30031,
				30094,29970,30162,30221,30301,30238,28317,30525,30662,30729,30763,30882,24826,31171,31454,31484,
				31619,31730,31769,31869,2371,31959,32122,32209,32272,32358,5423,32507,32554,32648,32713,32921,32962,
				13473,3792,30218,32474,33934,33458,33699,31713,31796,34828,34909,35167,35756,34879,36893,23641,37938,
				26229,32289,38862,39341,38949,40241,40913,41262,33195,41751,41196,42305,41651,42327,41890,40305,
				43179,25992,40313,43184,43918,43029,43915,42568,42848,43949,39969,42301,45384,45429,45761,45122,
				45371,46952,16375,47667,36154,47778,47269,46520,46632,47348,48335,47866,47401,32767,49053,49500,
				48677,49742,50128,50313,49685,49124,50349,51291,52811,51155,53427,53634,53916,53763,54471,8891,51838,
				54900,54577,55179,55104,29753,53878,55456,55412,49863,56216,
		);
	}
	/**
	 * BD对东莞，石家庄，温州隐藏特殊选项
	 * @return array:
	 */
	public function getBdAbTestFeedbackLockCities(){
		//return array(90,26,5,29);
		return array(0);
	}
	/**
	 * 支付网关名称
	 * @return array:string
	 */
	public function getOnlinePayGatewayName(){
		return array(
			'99bill' => '快钱',
			'pnr'=>'汇付天下',
			'llpayQuick'=>'连连支付快捷支付',
			'llpayWap'=>'连连支付Wap支付',
			'llpay'=>'连连支付',
			'hnapay'=>'海航支付',
			'rongpay'=>'融宝支付'
		);
	}

        /*
         * p2p跳转地址、tel、key配置
         */
        static public function getP2pConfig(){
            return $arrBankUrl = array(
                "拍拍贷" => array(
                    "url"		=> "http://ac.ppdai.com/registercheck",
                    "key"		=> "",
                    "tel" => "400-184-8888",
                    "partnerId" => "",
                    "login"		=> "http://ac.ppdai.com/login?fromurl=rong360",
                    "logName"	=> "ppdai",
                    "jumpUrl"   => "http://ac.ppdai.com/register?regsourceid=rong360",
                ),
                "融360测试P2P总行" => array(
                    "url" => "https://www.rong360.com/about/contact.html",
                    "tel" => "400-620-0360",
                    "key" => "ae33f52c2",
                    "partnerId" => "",
                    "login"     => "https://www.rong360.com/about/contact.html",
                    "logName"   => "ceship2p",
                    "jumpUrl"   => "https://www.rong360.com/about/contact.html",
                ),
            );
        }
    
	public static function getLimitCouponCity(){
		return array(
			'6'=>array('city_name'=>'佛山','value'=>500,),
			'66'=>array('city_name'=>'滨州','value'=>500,),
			'193'=>array('city_name'=>'赣州','value'=>500,),
			'183'=>array('city_name'=>'漳州','value'=>450,),
			'19'=>array('city_name'=>'潍坊','value'=>350,),
			'3'=>array('city_name'=>'深圳','value'=>300,),
			'4'=>array('city_name'=>'广州','value'=>300,),
			'2'=>array('city_name'=>'北京','value'=>300,),
			'28'=>array('city_name'=>'温州','value'=>300,),
			'186'=>array('city_name'=>'宁德','value'=>250,),
			'143'=>array('city_name'=>'哈尔滨','value'=>250,),
			'15'=>array('city_name'=>'惠州','value'=>250,),
			'261'=>array('city_name'=>'贵阳','value'=>200,),
			'25'=>array('city_name'=>'重庆','value'=>200,),
			'26'=>array('city_name'=>'成都','value'=>200,),
			'198'=>array('city_name'=>'郑州','value'=>200,),
			'16'=>array('city_name'=>'东莞','value'=>200,),
			'18'=>array('city_name'=>'中山','value'=>200,),
			'36'=>array('city_name'=>'江门','value'=>200,),
			'91'=>array('city_name'=>'保定','value'=>150,),
			'99'=>array('city_name'=>'太原','value'=>150,),
			'88'=>array('city_name'=>'邯郸','value'=>150,),
			'63'=>array('city_name'=>'临沂','value'=>150,),
			'110'=>array('city_name'=>'呼和浩特','value'=>150,),
			'29'=>array('city_name'=>'长沙','value'=>150,),
			'32'=>array('city_name'=>'泉州','value'=>100,),
			'24'=>array('city_name'=>'大连','value'=>100,),
			'270'=>array('city_name'=>'昆明','value'=>100,),
			'31'=>array('city_name'=>'福州','value'=>100,),
			'27'=>array('city_name'=>'厦门','value'=>100,),
			'51'=>array('city_name'=>'盐城','value'=>100,),
			'303'=>array('city_name'=>'兰州','value'=>100,),
			'60'=>array('city_name'=>'威海','value'=>100,),
			'200'=>array('city_name'=>'洛阳','value'=>100,),
			'90'=>array('city_name'=>'石家庄','value'=>100,),
			'23'=>array('city_name'=>'沈阳','value'=>100,),
			'96'=>array('city_name'=>'唐山','value'=>100,),
			'185'=>array('city_name'=>'龙岩','value'=>100,),
			'30'=>array('city_name'=>'西安','value'=>100,),
			'8'=>array('city_name'=>'珠海','value'=>100,),
			'13'=>array('city_name'=>'烟台','value'=>100,),
			'187'=>array('city_name'=>'南昌','value'=>100,),
			'11'=>array('city_name'=>'济南','value'=>100,),
			'5'=>array('city_name'=>'天津','value'=>100,),
			'12'=>array('city_name'=>'青岛','value'=>100,),
			'20'=>array('city_name'=>'淄博','value'=>100,),
			'245'=>array('city_name'=>'南宁','value'=>100,),
			'246'=>array('city_name'=>'柳州','value'=>100,),
			'134'=>array('city_name'=>'长春','value'=>100,),
			'232'=>array('city_name'=>'株洲','value'=>100,),
		);
	}
	public static function getISLoanUse(){
		return array(
			1=>'短期周转 ',
			2=>'购物',
			3=>'装修',
			4=>'买房',
			5=>'买车',
			6=>'旅游',
			7=>'医疗',
			8=>'教育',
			9=>'还款',
			10=>'为他人贷款',
			11=>'结婚',
			12=>'其他'
		);
	}
	public static function getISUserJob(){
		return array(
				1=>"公务员/事业单位员工",
				2=>"大型垄断国企员工",
				3=>"世界500强企业员工",
				4=>"上市企业员工",
				5=>"普通企业员工",
				6=>"其他",
		);
	}
	public static function getQsearchUserJob(){
		return array(
				1=>"企业主",
				2=>"个体户",
				4=>"上班族",
				10=>"无固定职业",
		);
	}
	//高端会所开放城市 by xiaochang 2015-03-05
	public static function getUsermarketProOpenCity(){
		return array(1,2,3,4,5,6,7,9,11,12,16,18,21,25,26,27,29,30,32,33,164,198,270,63,6,143,96,22,15,10,23,13,31,261,134);
	}

	//高端会所订单流入项目
	public static function getMarketOrderFlow(){
		return array(
			MARKET_ORDER_FLOW_DISNY		=> '迪斯尼',
		);
	}

	//高端会所订单竞价状态
	public static function getMarketOrderAllStatus(){
		return array(
			MARKET_ORDER_STATUS_WEISHEZHI			=> '未设置',
			MARKET_ORDER_STATUS_WEIKAISHI           => '未开始',
			MARKET_ORDER_STATUS_JINGJIAZHONG        => '竞价中',
			MARKET_ORDER_STATUS_JINGJIACHENGGONG    => '竞价成功',
			MARKET_ORDER_STATUS_LIUPAI              => '流拍',
			MARKET_ORDER_STATUS_QUXIAO              => '无效订单',
			MARKET_ORDER_STATUS_FEIDAN              => '已飞单',
		);
	}

	 /**
     * 高端会所一拍到底用户详情展示信息
     * */
    public static function getMaketDetailInfo(){
		return array(
			1		=> array('title' => '性别'),
			2		=> array('title' => '年龄'),
			914		=> array('title' => '征信情况'),
			24		=> array('title' => '名下是否有房', 'otherId' => array(518, 709, 751)),
			26		=> array('title' => '名下是否有车', 'otherId' => array(750)),
			860		=> array('title' => '职业身份', 'otherId' => array(900, 901, 264, 760)),
			534		=> array('title' => '工资发放形式', 'otherId' => array(799)),		
			59		=> array('title' => '月工资收入', 'otherId' => array(830)),
			635		=> array('title' => '是否缴纳社保', 'otherId' => array(808, 870)),
			52		=> array('title' => '现单位工作年限', 'otherId' => array(614, 650, 765)),			
			200		=> array('title' => '是否本地户籍', 'otherId' => array(911)),							
			45		=> array('title' => '教育程度', 'otherId' => array(891, 758)),			
			205		=> array('title' => '注册经营年限', 'otherId' => array(893)),
			53		=> array('title' => '单位性质'),  	
			58		=> array('title' => '职位', 'otherId' => array(764)),			
		);
	}
    /**
     * 高端会所一锤定音用户详情展示信息
     * */
    public static function getMaketKefuDetailInfo(){
		return array(
			39		=> 'user_age',
			45 	 	=> 'user_nationality',
			88		=> 'op_type',
			12		=> 'user_income_by_card',
			58		=> 'user_work_period',
			43		=> 'user_social_security',
			77		=> 'qid77',
			44		=> 'qid44',
			99		=> 'qid99',
			20		=> 'col_type',
			90		=> 'user_has_car',
			5		=> 'user_loan_experience',
			57		=> 'qid57',
			155		=> 'qid155',
			210		=> 'qid210',
			211		=> 'qid211',
			212		=> 'qid212',
		);
	}
	//迪斯尼专属红包赠送配置
	public static function gerDisneyHongbaoConfig() {
		return array (
				'2' => array('line' =>1000,'amount' => 3,'cnt' => 5),
				'3' => array('line' =>600,'amount' => 4,'cnt' => 5),
				'4' => array('line' =>600,'amount' => 4,'cnt' => 5),
				'5' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'6' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'7' => array('line' =>500,'amount' => 2,'cnt' => 5),
				'9' => array('line' =>600,'amount' => 3,'cnt' => 5),
				'12' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'15' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'16' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'18' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'21' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'25' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'26' => array('line' =>700,'amount' => 3,'cnt' => 5),
				'27' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'28' => array('line' =>500,'amount' => 2,'cnt' => 5),
				'29' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'31' => array('line' =>600,'amount' => 3,'cnt' => 5),
				'32' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'33' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'36' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'48' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'56' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'63' => array('line' =>500,'amount' => 5,'cnt' => 5),
				'70' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'81' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'83' => array('line' =>400,'amount' => 5,'cnt' => 5),
				'88' => array('line' =>600,'amount' => 3,'cnt' => 5),
				'91' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'96' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'110' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'122' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'123' => array('line' =>400,'amount' => 3,'cnt' => 5),
				'134' => array('line' =>600,'amount' => 4,'cnt' => 5),
				'143' => array('line' =>500,'amount' => 5,'cnt' => 5),
				'156' => array('line' =>600,'amount' => 3,'cnt' => 5),
				'159' => array('line' =>500,'amount' => 5,'cnt' => 5),
				'163' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'164' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'185' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'186' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'198' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'200' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'210' => array('line' =>500,'amount' => 3,'cnt' => 5),
				'211' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'218' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'234' => array('line' =>600,'amount' => 3,'cnt' => 5),
				'235' => array('line' =>500,'amount' => 5,'cnt' => 5),
				'237' => array('line' =>300,'amount' => 5,'cnt' => 5),
				'240' => array('line' =>500,'amount' => 5,'cnt' => 5),
				'245' => array('line' =>600,'amount' => 4,'cnt' => 5),
				'246' => array('line' =>600,'amount' => 5,'cnt' => 5),
				'247' => array('line' =>500,'amount' => 5,'cnt' => 5),
				'261' => array('line' =>600,'amount' => 3,'cnt' => 5),
				'270' => array('line' =>500,'amount' => 4,'cnt' => 5),
				'295' => array('line' =>400,'amount' => 3,'cnt' => 5),
				'303' => array('line' =>600,'amount' => 3,'cnt' => 5),
	
		)
		;
	}
	
	//客户分配管理
    //潜客录入/新bd分配权限/公共潜客领取权限/公共注册客户领取权限/手动分配/信用卡贷款踢转
	public static function getBankerAssignManageType(){
	    return array(
	        BANKER_ASSIGN_TYPE_DISTRIBUTE_BD => '新bd',
	        BANKER_ASSIGN_TYPE_ADD_BANKER_APPLY => '录入潜客',
	        BANKER_ASSIGN_TYPE_GET_BANKER_APPLY => '领取潜客',
	        BANKER_ASSIGN_TYPE_GET_BANKER_COMMON => '领取公共客户',
	        BANKER_ASSIGN_TYPE_DISTRIBUTE_BANKER => '分配客户',
	        //BANKER_ASSIGN_TYPE_CREDIT_LOAN_CHANGE => '信用卡/贷款踢转',
	        //BANKER_ASSIGN_TYPE_CHENGSHU_BACK_CHANGE => '成熟转后端'
	    );
	}
	
	//客户来源
    // 1新BD、2潜客、3公共池领取、4公共池分配、5划转
	public static function getBankerFromType(){
	    return array(
	        BANKER_FROM_TYPE_NEW_BD => '新BD',
	        BANKER_FROM_TYPE_BANKER_APPLY => '潜客',
	        BANKER_FROM_TYPE_CLAIM_BANKER_COMMON => '公共池领取',
	        BANKER_FROM_TYPE_TRANSFROM_BANKER => '划转',
	    	BANKER_FROM_TYPE_ASSIGN_BANKER_COMMON_DOUBLEV => '公共池分配—双V',
	    	BANKER_FROM_TYPE_ASSIGN_BANKER_COMMON_APPROVING => '公共池分配—审核中',
	    	BANKER_FROM_TYPE_ASSIGN_BANKER_COMMON_HIGHSCORE => '公共池分配—高评分',
	    	BANKER_FROM_TYPE_ASSIGN_BANKER_COMMON_LOGIN => '公共池分配—登录',
	    	BANKER_FROM_TYPE_ASSIGN_BANKER_COMMON_AUTH => '公共池分配-30天内有认证操作',
	    	BANKER_FROM_TYPE_ASSIGN_BANKER_COMMON_REGISTER => '公共池分配-60天内注册',
	    );
	}
	
	//潜客添加来源（领取、分配、自己录入）
	public static function getBankerApplyFromType(){
	    return array(
	        BANKER_APPLY_FROM_TYPE_CLAIM_BANKER_APPLY => '领取',
	        BANKER_APPLY_FROM_TYPE_ASSIGN_BANKER_APPLY => '分配',
	        BANKER_APPLY_FROM_TYPE_NEW_BANKER_APPLY => '自己录入',
            BANKER_APPLY_FROM_TYPE_TEST_ASSIGN_BANKER => '测试分配',
	    );
	}
	
	/**
     * @brief   生成电话的随机串参数
     */
	public static function getRandomString($length = 10)
	{ 
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$string = '';
		for ($i = 0; $i < $length; $i++)
		{
			$string .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
		}
		return $string;  
    }

	/**
     * @brief   生成外呼电话的自定义参数
	 * $order_id 业务ID
	 * $call_type 外呼通话类型
	 * $system 系统常量
     */
	public static function encCallUserField($order_id, $call_type, $system)
    {
		if ($order_id && $call_type)
        {
            return $system . '_' . $order_id . '_' . $call_type . '_'. microtime(1);
        }
        else
        {
            return '';
        }
    }
	
	/**
     * @brief   获取外呼电话的业务ID和外呼类型
     */
	public static function decCallUserField($callUserField)
    {
		$data = array();
		if ($callUserField)
        {
			$arrTemp = explode('_', $callUserField);
			$data['system_id'] = $arrTemp[0];
            $data['order_id'] = $arrTemp[1];
			$data['call_type'] = $arrTemp[2];
        }
		return $data;
    }
    /*
     * 根据所问问题来判断一些特种产品
     */
    public static function getSpecialQuestion(){
        return array(

            "156"=>156,
            "122"=>122,
            "20"=>20,
            "90"=>90,
            "213"=>213,
            "98"=>98,
            "141"=>141
            //"57"=>57
        );
    }

    /*
    * 一些产品和IS产品重名，排序时需要将这些产品向后排，
    */
    public static function getProductList(){
        //返回产品ID
        return array(

            '10108968'=>10108968,
            '10031717'=>10031717,
            '10036313'=>10036313,
            '10108866'=>10108866,
            '10108824'=>10108824,
            '10094374'=>10094374,
            '10031713'=>10031713,
            '10045669'=>10045669,
            '10078549'=>10078549,
            '10078548'=>10078548,
            '10058622'=>10058622,
            '10058620'=>10058620,
            '10058618'=>10058618,
            '10039188'=>10039188,
            '2806'=>2806,
            '2702'=>2702,
            '2701'=>2701,
            '2703'=>2703,
            '10083728'=>10083728,
            '10083726'=>10083726,
            '10083725'=>10083725,
            '10082938'=>10082938,
            '10055913'=>10055913,
            '10073539'=>10073539,
            '10073537'=>10073537,
            '10073536'=>10073536,
            '10063966'=>10063966,
            '10063958'=>10063958,
            '5'=>5,
            '42'=>42,
            '1351'=>1351,
            '10065396'=>10065396,
            '2493'=>2493,
            '2491'=>2491,
            '10077460'=>10077460,
            '10077459'=>10077459,
            '10080927'=>10080927,
            '10074844'=>10074844,
            '3020'=>3020,
            '10011597'=>10011597,
            '10011595'=>10011595,
            '10011594'=>10011594,
            '10071227'=>10071227,
            '1611'=>1611,
            '10048128'=>10048128,
            '11765'=>11765,
            '10091515'=>10091515,
            '2556'=>2556,
            '10034125'=>10034125,
            '10034124'=>10034124,
            '10086810'=>10086810,
            '10086809'=>10086809,
            '10073451'=>10073451,
            '10020519'=>10020519,
            '10020509'=>10020509,
            '10517'=>10517,
            '10515'=>10515,
            '10093944'=>10093944,
            '10086593'=>10086593,
            '10086581'=>10086581,
            '10086579'=>10086579,
            '10086578'=>10086578,
            '10322'=>10322,
            '10320'=>10320,
            '10319'=>10319,
            '10070018'=>10070018,
            '10068089'=>10068089,
            '10068085'=>10068085,
            '10068084'=>10068084,
            '10068083'=>10068083,
            '10883'=>10883,
            '10882'=>10882,
            '10091978'=>10091978,
            '10010152'=>10010152,
            '10010151'=>10010151,
            '10077890'=>10077890,
            '10077889'=>10077889,
            '10077888'=>10077888,
            '10053963'=>10053963,
            '10053960'=>10053960,
            '10064332'=>10064332,
            '10064331'=>10064331,
            '10051267'=>10051267,
            '10051265'=>10051265,
            '10051262'=>10051262,
            '10021353'=>10021353,
            '10002651'=>10002651,
            '1811'=>1811,
            '1813'=>1813,
            '10002527'=>10002527,
            '1854'=>1854,
            '1856'=>1856,
            '1436'=>1436,
            '10076615'=>10076615,
            '10075821'=>10075821,
            '10075819'=>10075819,
            '10075816'=>10075816,
            '10040181'=>10040181,
            '10040114'=>10040114,
            '10008307'=>10008307,
            '10008305'=>10008305,
            '2316'=>2316,
            '2315'=>2315,
            '10040838'=>10040838,
            '10088717'=>10088717,
            '2065'=>2065,
            '2066'=>2066,
            '10075122'=>10075122,
            '10066822'=>10066822,
            '10066821'=>10066821,
            '10066819'=>10066819,
            '10062998'=>10062998,
            '10062891'=>10062891,
            '10051107'=>10051107,
            '10051101'=>10051101,
            '10051098'=>10051098,
            '10028800'=>10028800,
            '10028799'=>10028799,
            '10030663'=>10030663,
            '10030662'=>10030662,
            '10030661'=>10030661,
            '10030660'=>10030660,
            '10073502'=>10073502,
            '10073500'=>10073500,
            '10073497'=>10073497,
            '10073496'=>10073496,
            '10040362'=>10040362,
            '10074106'=>10074106,
            '10042420'=>10042420,
            '10041789'=>10041789,
            '10070735'=>10070735,
            '10070733'=>10070733,
            '10070732'=>10070732,
            '10095918'=>10095918,
            '10061735'=>10061735,
            '10026507'=>10026507,
            '10026506'=>10026506,
            '10026477'=>10026477,
            '10046570'=>10046570,
            '10046568'=>10046568,
            '10046566'=>10046566,
            '10046565'=>10046565,
            '10066151'=>10066151,
            '10066091'=>10066091,
            '10059011'=>10059011,
            '10029201'=>10029201,
            '10029199'=>10029199,
            '10029198'=>10029198,
            '10029196'=>10029196,
            '10011613'=>10011613,
            '10011612'=>10011612,
            '10011611'=>10011611,
            '10011610'=>10011610,
            '10050873'=>10050873,
            '10033824'=>10033824,
            '10007287'=>10007287,
            '10007285'=>10007285,
            '10006966'=>10006966,
            '10041649'=>10041649,
            '10041643'=>10041643,
            '10091495'=>10091495,
            '10091493'=>10091493,
            '10086382'=>10086382,
            '10086373'=>10086373,
            '10086371'=>10086371,
            '10066186'=>10066186,
            '10066183'=>10066183,
            '10066177'=>10066177,
            '10066172'=>10066172,
            '10037024'=>10037024,
            '10037022'=>10037022,
            '10034175'=>10034175,
            '10034164'=>10034164,
            '10034157'=>10034157,
            '10034142'=>10034142,
            '10080254'=>10080254,
            '10070216'=>10070216,
            '10032024'=>10032024,
            '4123'=>4123,
            '1711'=>1711,
            '440'=>440,
            '441'=>441,
            '443'=>443,
            '10052120'=>10052120,
            '10052117'=>10052117,
            '10052116'=>10052116,
            '10090467'=>10090467,
            '10090465'=>10090465,
            '10090464'=>10090464,
            '10090461'=>10090461,
            '10090460'=>10090460,
            '10072003'=>10072003,
            '10037559'=>10037559,
            '4163'=>4163,
            '2175'=>2175,
            '1287'=>1287,
            '1288'=>1288,
            '1289'=>1289,
            '1290'=>1290,
            '1970'=>1970,
            '10082769'=>10082769,
            '10044310'=>10044310,
            '10044309'=>10044309,
            '343'=>343,
            '344'=>344,
            '1693'=>1693,
            '10074517'=>10074517,
            '10074516'=>10074516,
            '10074514'=>10074514,
            '10074513'=>10074513,
            '10040058'=>10040058,
            '10040057'=>10040057,
            '10040055'=>10040055,
            '10041309'=>10041309,
            '10041300'=>10041300,
            '10004850'=>10004850,
            '10023560'=>10023560,
            '10003301'=>10003301,
            '10003302'=>10003302,
            '10003303'=>10003303,
            '10091425'=>10091425,
            '10091413'=>10091413,
            '10091411'=>10091411,
            '10091410'=>10091410,
            '10081860'=>10081860,
            '10081859'=>10081859,
            '10084942'=>10084942,
            '10084941'=>10084941,
            '10044697'=>10044697,
            '10044695'=>10044695,
            '10044694'=>10044694,
            '2671'=>2671,
            '2326'=>2326,
            '1920'=>1920,
            '1919'=>1919,
            '1917'=>1917,
            '10086137'=>10086137,
            '10086136'=>10086136,
            '10046989'=>10046989,
            '10025565'=>10025565,
            '10025564'=>10025564,
            '10025563'=>10025563,
            '10025561'=>10025561,
            '10025560'=>10025560,
            '10084214'=>10084214,
            '10084213'=>10084213,
            '10084210'=>10084210,
            '10065401'=>10065401,
            '10093564'=>10093564,
            '10093563'=>10093563,
            '10088396'=>10088396,
            '10088395'=>10088395,
            '10088394'=>10088394,
            '10088393'=>10088393,
            '10011946'=>10011946,
            '10011944'=>10011944,
            '10070560'=>10070560
        );
    }
	/**
     * 获取mbd的域名
     * @return string
     */
    public static function getMbdHostUrl(){

        $url = 'http://m.bd.rong360.com';
    	if(Config::$mis_dev_mode && strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false){
    		$url = 'http://m.bd.rong360.com';
    	}
        if(Utils::is_HTTPS()){
            $arrTplData['url'] = str_replace('http://','https://',$url);
        }
    	return $url;
    }
    
    public static function getWeixinEnvolopeNoticeTemplateId(){
    	$templateId = '7bJEYmstf85EvfAF2nuVrumqFGsNBTCq8KeqpJZJxEk';
    	if(Config::$mis_dev_mode){
    		$templateId = 'JjahdTVvTw1ajSQCMt1Twu3U5GNYzwuB_7rwEqqxkuM';
    	}
    	return $templateId;
    }
    
    public static function getBDPrivateProductPrizeNoticeTemplateId(){
    	$templateId = 'APZQIQ82qIXqICX9SEsWgQYyiAKsC-eIewRCq4iBXOk';
    	if(Config::$mis_dev_mode){
    		$templateId = 'Oep87I94PTaDtoMMAhgo7JyHBK-vBvY9myCGbVQD6yQ';
    	}
    	return $templateId;
    }

    //首付房贷的开通城市配置
    public static function supportFristCityConfig(){

        $supportFristCityConfig = array('北京', '上海', '广州', '深圳','苏州' ); 

        return $supportFristCityConfig;

    }

	//信贷员退款审核状态
    public static function getBankerRefundStatus()
    {
        return array(
            BANKER_REFUND_STATUS_NEW            => '待处理',
            BANKER_REFUND_STATUS_BC_DEAL        => 'bc处理中',
            BANKER_REFUND_STATUS_BC_LEADER_DEAL => 'bcleader处理中',
            BANKER_REFUND_STATUS_REVIEW         => '待复审',
            BANKER_REFUND_STATUS_OWN_CANCEL     => '信贷员取消退款',
            BANKER_REFUND_STATUS_VISIT          => '待回访',
            BANKER_REFUND_STATUS_WORKING        => '退款中',
			BANKER_REFUND_STATUS_CANCEL         => '取消退款',
			BANKER_REFUND_STATUS_SUCCESS        => '退款成功',
			BANKER_REFUND_STATUS_FAILURE        => '退款失败',
        );
    }
    
    //获取BD信贷员退款状态
    public static function getBDBankerRefundStatus(){
    	return array(
    			BANKER_REFUND_STATUS_NEW            => '申请退款中',
    			BANKER_REFUND_STATUS_BC_DEAL        => '退款处理中',
    			BANKER_REFUND_STATUS_BC_LEADER_DEAL => '退款处理中',
    			BANKER_REFUND_STATUS_REVIEW         => '退款处理中',
    			BANKER_REFUND_STATUS_OWN_CANCEL     => '取消退款处理中',
    			BANKER_REFUND_STATUS_VISIT          => '取消退款处理中',
    			BANKER_REFUND_STATUS_WORKING        => '退款处理中',
    			BANKER_REFUND_STATUS_CANCEL         => '取消退款',
    			BANKER_REFUND_STATUS_SUCCESS        => '退款成功',
    			BANKER_REFUND_STATUS_FAILURE        => '退款失败',
    	);
    }

    //
    public static function getAppPushProviders()
    {
        return array(
                APP_PUSH_PROVIDER_JPUSH   => APP_PUSH_PROVIDER_JPUSH_VALUE,
                APP_PUSH_PROVIDER_XIAOMI  => APP_PUSH_PROVIDER_XIAOMI_VALUE,
                APP_PUSH_PROVIDER_GETUI   => APP_PUSH_PROVIDER_GETUI_VALUE,
                APP_PUSH_PROVIDER_UMENG   => APP_PUSH_PROVIDER_UMENG_VALUE
        );
    }
    
    //拍客随机红包开通城市
    public static function getBDUpUsermarketOpenCityList(){
    	return array(1,16,18,6,7,27,164,5,4,21,33,11,9,30,198,29);
    }
    
    //锤客随机红包开通城市
    public static function getBDDownUsermarketOpenCityList(){
    	return array(1,16,18,6,7,27,164,5,4,21,33,11,9,30,198,29);
    }
    
    //拍客随机红包时间范围
    public static function getBDUpUsermarketTimeRange(){
    	return array(
    			'start' => 1452787200,
    			'end' => 1456070400
    	);
    }
    
    //锤客随机红包时间范围
    public static function getBDDownUsermarketTimeRange(){
    	return array(
    			'start' => 1452787200,
    			'end' => 1456070400
    	);
    }
	
    //获取产品业务类型
    public static function getProductWithRiskService(){
    	return array(
			WITH_RISK_SERVICE_LIZI      => '例子',
			WITH_RISK_SERVICE_IS        => 'IS',
			WITH_RISK_SERVICE_ISALL     => 'ISALL',
			WITH_RISK_SERVICE_TAOJINYUN => '淘金云',
    	);
    }
    
    //获取个人产品策略优化的城市
    public static function getBDPrivateProductTestCityIds(){
    	return array(187, 99, 28, 57, 108, 125, 163, 76, 47, 91, 75, 77, 187, 162, 135, 109, 116, 110, 96, 100, 148, 99, 60, 171, 79, 175, 55, 83, 73, 80, 95, 142, 93, 64, 71, 107, 94, 123, 46, 69, 61, 270, 105, 124, 56, 40, 35, 41, 36, 97, 42, 70, 58, 169, 167, 50, 44, 28, 37, 173, 66, 19, 45, 13, 152, 51, 78, 89, 65, 39, 68, 161, 38, 127, 133, 92, 160, 30, 84, 113, 129, 81, 106, 49, 114, 74, 98, 88, 115, 131, 170, 120, 134, 102, 128, 174, 43, 101, 122, 34, 172, 144, 182, 209, 197, 315, 63, 331, 178, 212, 177, 262, 217, 184, 210, 213, 225, 211, 302, 282, 243, 222, 186, 22, 300, 202, 264, 195, 294, 180, 236, 258, 176, 237, 201, 312, 297, 199, 238, 309, 48, 242, 231, 196, 280, 204, 191, 10, 188, 271, 248, 299, 308, 267, 241, 298, 179, 256, 200, 215, 259, 20, 296, 233, 244, 208, 183, 206, 205, 253, 272, 8, 254, 239, 327, 279, 223, 221, 181, 234, 219, 317, 207, 252, 255, 235, 240, 220, 311, 251, 265, 293, 326, 250, 214, 203, 192, 224, 216, 268, 185);
    }
    
    //获取个人产品快问价格
    public static function getBDQuickAskPrice(){
    	return array(
    			//信用情况
    			'5'=>array(
    					'0'=>10,
    					'1'=>20,
    					'2'=>2,
    					'3'=>1
    			),
    			//银行卡发放工资
    			'12'=>array(
    					'le'=>20,
    					'ge'=>20
    			),
    			//名下房产类型
    			'20'=>array(
    					'1'=>30,
    					'2'=>30,
    					'3'=>30,
    					'4'=>30,
    					'6'=>20,
    					'8'=>20,
    					'10'=>20,
    					'12'=>20,
    					'14'=>30,
    					'16'=>20
    			),
    			//房产位置
    			'21'=>array(
    					'1'=>5,
    					'2'=>5,
    					'3'=>5,
    			),
    			//住宅房龄
    			'22'=>array(
    					'10'=>5,
    					'15'=>5,
    					'20'=>5,
    					'25'=>5,
    					'30'=>5,
    					'35'=>5,
    			),
    			//房产是否已抵押/按揭中
    			'23'=>array(
    					'1'=>8,
    					'2'=>8
    			),
    			//车辆估值
    			'31'=>array(
    					'ge'=>2,
    					'le'=>2
    			),
    			//年龄
    			'39'=>array(
    					'min'=>3,
    					'max'=>3
    			),
    			//经营注册地
    			'46'=>array(
    					'1'=>3,
    					'2'=>3,
    					'20'=>3
    			),
    			//总经营流水/月
    			'49'=>array(
    					'ge'=>2,
    					'le'=>2
    			),
    			//对公账户经营收入/月
    			'50'=>array(
    					'ge'=>3,
    					'le'=>3
    			),
    			//房产估值
    			'56'=>array(
    					'ge'=>2,
    					'le'=>2
    			),
    			//经营年限
    			'59'=>array(
    					'0'=>3,
    					'3'=>3,
    					'6'=>3,
    					'12'=>3,
    					'24'=>3,
    					'36'=>3,
    					'60'=>3
    			),
    			//商铺房龄
    			'64'=>array(
    					'10'=>5,
    					'15'=>5,
    					'20'=>5,
    					'25'=>5,
    					'30'=>5,
    					'35'=>5,
    			),
    			//办公楼房龄
    			'65'=>array(
    					'10'=>5,
    					'15'=>5,
    					'20'=>5,
    					'25'=>5,
    					'30'=>5,
    					'35'=>5,
    			),
    			//能否提供房产证
    			'66'=>array(
    					'1'=>11,
    					'2'=>11,
    					'4'=>11
    			),
    			//现金收入/月
    			'71'=>array(
    					'le'=>0,
    					'ge'=>0
    			),
    			//职业身份
    			'88'=>array(
    					'1'=>3,
    					'2'=>3,
    					'4'=>3,
    					'6'=>1,
    					'10'=>1
    			),
    			//名下是否有车
    			'90'=>array(
    					'5'=>10,
    					'7'=>2,
    					'10'=>1
    			),
    			//现金结算经营收入/月
    			'91'=>array(
    					'ge'=>0,
    					'le'=>0
    			)
    	);
    }
    public static $arrChannel= array(
        /*'tx'=>array(
            'mp'=>0,//0:标准产品，1：doudi产品，2:p2p产品，3所有产品, 4标准产品或者p2p产品
            'gdt2'=>2
        ),*/
        'rong'=>array(
            'rong'=>3,
        ),
        'jkbd'=>array(
            'xxl_dk'=>0,
        ),
        'jrtoutitiao'=>array(
            'dkpt'=>0,
        ),
        'jrtoutiao'=>array(
            'dkpt'=>0,
        ),
    	'kczx'=>array(
    		'cpa'=>4,
    	)
    );
    public static $arrChannelKey= array(
        'rong_rong'=>'8ZGEDnFXcRO4rg4',
        'jkbd_xxl_dk'=>'7ZKEDnAzFRT4rg3',
        'jrtoutitiao_dkpt'=>'tfamRvS4d5D8EvU',
        'jrtoutiao_dkpt'=>'tfamRvS4d5D8EvU',
    	'kczx_cpa'=>'tYifJVI74nEpqyM',
    );

    public static function getwxshareTimeSpan(){
        return array(
            'start' => strtotime('2016-08-16'),
            'end' => strtotime('2016-10-16')
        );
    }

    public static function getNewBiddingStdProductInfo(){
        return array(
            'time' => array(
                'start' => strtotime('2016-08-26'),
                'end' => strtotime('2016-09-10')
            ),
            'city' => array(190,6,2,162,100,50,3,9,198)
        );
    }

    //信贷员保证金类型
    public static function getBankerDepositType(){
        return array(
            BANKER_DEPOSIT_PRODUCT_ORDERS       => '产品接单保证金',
        );
    }

    public static function getDepositStatus(){
        return array(
            BANKER_DEPOSIT_COMMIT => '提交成功',
            BANKER_DEPOSIT_BC_COMMIT => '审批中',
            BANKER_DEPOSIT_BC_LEADER_COMMIT => '审批中',
            BANKER_DEPOSIT_CRM_COMMIT => '审批中',
            BANKER_DEPOSIT_MIS_COMPLETE => '审批成功',
            BANKER_DEPOSIT_MIS_WEISHOUDAOKUAN => '未收到款',
            BANKER_DEPOSIT_MIS_JINEBUFU => '金额不符',
            BANKER_DEPOSIT_MIS_REFUND_FAIL => '退款失败',
            BANKER_DEPOSIT_CRM_LEADER_CANCEL => '审批被拒',
            BANKER_DEPOSIT_CRM_CANCEL => '审批被拒',
            BANKER_DEPOSIT_CRM_STATUS_CHANGE => '审批失败',
        );
    }
    /*****
     * by liuyue 2016-11-08
     * 机构BD跟进进展
     */
    public static function getBdOrgProgress(){
        return array(
            //BD_ORG_POTENTIAL => '潜客确立',
            BD_ORG_ENTRY => '录入机构',
            BD_ORG_SEE_TOP => '见决策人和上层',
            BD_ORG_UNBUSINESS => '非商务见面决策人',
            BD_ORG_ACC_GIFT => '礼品接受',
            BD_ORG_TECH_MEET => '技术见面',
            BD_ORG_START_DEVELOP => '对方开始开发',
    //        BD_ORG_CON_CONFIRM => '商务条件确认',
            BD_ORG_CONTRACT_SIGNED => '合同签署',
            BD_ORG_ADVANCE_PAY => '预付款',
            BD_ORG_ONLINE => '上线',
        );
    }
    /*****
     * by liuyue 2016-11-08
     * 业务线 机构BD跟进进展
     */
    public static function getBdOrgLineProgress(){
        return array(
            BD_ORG_TECH_MEET => '技术见面',
            BD_ORG_START_DEVELOP => '对方开始开发',
   //         BD_ORG_CON_CONFIRM => '商务条件确认',
            BD_ORG_CONTRACT_SIGNED => '合同签署',
            BD_ORG_ADVANCE_PAY => '预付款',
            BD_ORG_ONLINE => '上线',
        );
    }
    /*****
     * by liuyue 2016-11-08
     * 机构BD 进展打分
     */
    public static function getBdOrgScore(){
        return array(
            BD_ORG_POTENTIAL => 1,
            BD_ORG_ENTRY => 1,
            BD_ORG_SEE_TOP => 2,
            BD_ORG_UNBUSINESS => 1,
            BD_ORG_ACC_GIFT => 0,
            BD_ORG_TECH_MEET => 1.5,
            BD_ORG_START_DEVELOP => 1,
            BD_ORG_CON_CONFIRM => 1,
            BD_ORG_CONTRACT_SIGNED => 2,
            BD_ORG_ADVANCE_PAY => 1,
            BD_ORG_ONLINE => 2,
        );
    }
    
    /*
     * added by xufeng 2017-02-24
     * 机构BD 进展对应增加流失天数
     */
    public static function getBdOrgLiushiDay()
    {
    	return array(
    			BD_ORG_POTENTIAL => 0,
    			BD_ORG_ENTRY => 60,
    			BD_ORG_SEE_TOP => 10,
    			BD_ORG_UNBUSINESS => 5,
    			BD_ORG_ACC_GIFT => 5,
    			BD_ORG_TECH_MEET => 7,
    			BD_ORG_START_DEVELOP => 5,
    			BD_ORG_CON_CONFIRM => 0,
    			BD_ORG_CONTRACT_SIGNED => 10,
    			BD_ORG_ADVANCE_PAY => 5,
    			BD_ORG_ONLINE => 10,
    	);
    }
    
    /*****
     * by liuyue 2016-11-08
     * 机构BD区域
     */
    public static function getBdOrgArea(){
        return array(
            BD_ORG_AREA_EAST => '华东',
            BD_ORG_AREA_SOUTH => '华南',
            BD_ORG_AREA_NORTH => '华北',

        );
    }
    /*
     * by xufeng 2016-12-14
     * 通过名字获得区域id
     */
    public static function getBdOrgAreaId(){
    	return array(
    		'华东' => BD_ORG_AREA_EAST,
    		'华南' => BD_ORG_AREA_SOUTH,
    		'华北' => BD_ORG_AREA_NORTH,
    	);
    }

    /*****
     * by liuyue 2016-11-08
     * 机构BD 业务线
     */
    public static function getBdOrgSerLine(){
        return array(
            BD_ORG_LINE_TIANJI => '天机',
            BD_ORG_LINE_TAOJINYUN => '淘金云',
            BD_ORG_LINE_LEIPAIPAI=> '类拍拍',
            BD_ORG_LINE_GUANGDIANTONG => '广点通',
            BD_ORG_LINE_LIZI => '对公例子',
        );
    }

    /*****
     * by liuyue 2016-11-17
     * 机构BD 业务线 =>角色
     */
    public static function getBdOrgLineRole(){
        return array(
            BD_ORG_LINE_TIANJI => ROLE_BD_ORG_TJ,
            BD_ORG_LINE_TAOJINYUN => ROLE_BD_ORG_TJY,
            BD_ORG_LINE_LEIPAIPAI=> ROLE_BD_ORG_LPP,
            BD_ORG_LINE_GUANGDIANTONG => ROLE_BD_ORG_GDT,
            BD_ORG_LINE_LIZI => ROLE_BD_ORG_PLZ,
        );
    }

    /*****
     * by liuyue 2016-11-17
     * 机构BD 角色=》业务线
     */
    public static function getBdOrgRoleLine(){
        return array(
            ROLE_BD_ORG_TJ => BD_ORG_LINE_TIANJI,
            ROLE_BD_ORG_TJY => BD_ORG_LINE_TAOJINYUN,
            ROLE_BD_ORG_LPP => BD_ORG_LINE_LEIPAIPAI,
            ROLE_BD_ORG_GDT => BD_ORG_LINE_GUANGDIANTONG,
            ROLE_BD_ORG_PLZ => BD_ORG_LINE_LIZI,
        );
    }

    /*****
     * by liuyue 2016-11-08
     * 机构BD 审批结果
     */
    public static function getBdOrgExam(){
        return array(
            BD_ORG_WAIT_EXAM => '未审批',
            BD_ORG_ACCESS_EXAM => '审批通过',
            BD_ORG_REJECT_EXAM=> '审批拒绝',
            BD_ORG_OVER_EXAM => '审批过期',
            BD_ORG_OTHER_EXAM => '审批中',
        );
    }


    public static function getTelBusinessSystem(){
        return array(
            CALL_SYSTEM_RISK     => '风控系统',
            CALL_SYSTEM_KEFU     => '客服系统',
            CALL_SYSTEM_DIANXIAO => '外包电销系统',
            CALL_SYSTEM_CRM      => 'CRM系统',
            CALL_SYSTEM_CUISHOU  => '催收系统',
        );
    }

    public static function getTelBusinessType(){
        return array(
            CALL_SYSTEM_KEFU => array(
                CALL_BUSINESS_TYPE_DIANXIAO      => '电销',
                CALL_BUSINESS_TYPE_RISK_DIANXIAO => '风控电销',
                CALL_BUSINESS_TYPE_YUCEWAIHU     => '预测外呼',
            ),
            CALL_SYSTEM_CRM => array(
                CALL_BUSINESS_TYPE_CONTACT_BANKER => '联系信贷员',
            ),
            CALL_SYSTEM_CUISHOU => array(
                CALL_BUSINESS_TYPE_CUISHOU        => '催收',
            ),
        );
    }


    //人工订单复议选项
    public static function getRerviewArtificialOrderType(){
    	return array(
    			REVIEW_FEIBENRENSHENQING  =>  '非本人申请',
    			REVIEW_DAIKUANEDUBUFU     =>  '贷款额度不符',
    			REVIEW_LIXIYUEGONGYOUYIYI =>  '利息月供有异议',
    			REVIEW_ZUOXIQIANGTUI      =>  '座席强推',
    			REVIEW_ZIZHIXINXILUANTIAN =>  '资质信息乱填',
    			REVIEW_WEIHESHIZHIJIEXIADAN  =>  '未核实直接下单',
    			REVIEW_QITA               =>  '其他',
    	);
    }
    //异地无意愿订单复议
    public static function getRerviewSystemOrderType(){
    	return array(
    			REVIEW_YIDI        =>   '异地用户',
    			REVIEW_WUDAIKUANYIXIANG  => '无贷款意向',
    	);
    }
    //复议选项
    public static function getRerviewOrderType(){
    	return array(
    			REVIEW_FEIBENRENSHENQING  =>  '非本人申请',
    			REVIEW_DAIKUANEDUBUFU     =>  '贷款额度不符',
    			REVIEW_LIXIYUEGONGYOUYIYI =>  '利息月供有异议',
    			REVIEW_ZUOXIQIANGTUI      =>  '座席强推',
    			REVIEW_ZIZHIXINXILUANTIAN =>  '资质信息乱填',
    			REVIEW_WEIHESHIZHIJIEXIADAN  =>  '未核实直接下单',
    			REVIEW_QITA               =>  '其他',
    			REVIEW_YIDI        =>   '异地用户',
    			REVIEW_WUDAIKUANYIXIANG  => '无贷款意向',
    	);
    }
    //复议结果
    public static function getReviewResult(){
    	return array(
    			REVIEW_RESULT_ON   => '待复议',
    			REVIEW_RESULT_PASS => '复议通过',
    			REVIEW_RESULT_NONPASS  =>  '复议不通过',
    	);
    }
    //挂账是否可划转
    public static function getHuaZhuan(){
        return array(
                '0'   => '未划转',
                '1'   => '可划转',
        );
    }

    public static function getArtificialInvalidOrderReason()
    {
    	return array(
    			'1' =>  '无法接通',
    			'2' =>  '外地号码',
    			'3' =>  '无贷款意向',
    			'5' =>  '中介、同行试用',
    			'6' =>  '重复客户',
    			'7' =>  '用户试用',
    			'8' =>  '异地用户',
    			'10' => '资质与订单资质详情不符',
    			'4' =>  '其他',
    	);
    }
    public static function getSystemInvalidOrderReason()
    {
    	return array(
    			'1' =>  '无法接通',
    			'2' =>  '外地号码',
    			'5' =>  '中介、同行试用',
    			'6' =>  '重复客户',
    			'7' =>  '用户试用',
    			'8' =>  '异地用户',
    			'9' =>  '无意愿用户', //kefu是无意愿用户  common是恶意申请
    			'4' =>  '其他',
    	);
    }
}
