#!/bin/sh
rm -rf output
mkdir output
find ./ -type d -name CVS|xargs -i rm -rf {}
cd ..
tar zcf phpconnectpool_src.tar.gz phpconnectpool/
mv phpconnectpool_src.tar.gz phpconnectpool/output/
