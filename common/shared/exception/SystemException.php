<?php
/**
 * 系统异常。需要立即处理。
 */
class SystemException extends Exception
{
	/**
	 * Constructor.
	 */
	public function __construct($message=null,$code=0)
	{
		parent::__construct($message,$code);
	}
}
