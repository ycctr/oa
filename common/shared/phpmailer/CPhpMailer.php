<?php
//require_once(Yii::getPathOfAlias('shared.phpmailer').DIRECTORY_SEPARATOR.'class.phpmailer.php');
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'class.phpmailer.php');

class CPhpMailer
{
    function __construct()
    {
        $this->_mailer = new PHPMailer();
        $this->_mailer->CharSet = "utf-8";
//        $this->_mailer->IsSMTP();
        $this->_mailer->IsSendmail();
        $this->_mailer->SMTPAuth = true;
        $this->_mailer->AltBody = "text/html";
        $this->_mailer->IsHTML(true);
    }
    function init()
    {
        $this->_mailer->Host = $this->host;
        $this->_mailer->Port = $this->port;
        $this->_mailer->Username = $this->user;
        $this->_mailer->Password = $this->pass;
        $this->_mailer->From = $this->from;
        $this->_mailer->FromName = $this->fromName;
    }

    /**
     * 发送普通的html邮件，不含附件。发送人、格式默认。
     * @to 收件人，支持数组或单个收件人
     * @subject 邮件标题，字符串
     * @body 邮件内容，字符串
     * @cc 抄送，支持数组或字符串
     * @cc 秘送，支持数组或字符串
     */
    function send($to, $subject, $body,$cc=null, $bcc=null)
    {
        $this->_mailer->ClearAddresses();
        $this->_mailer->ClearAttachments();
        $this->_mailer->Subject = $subject;

        if(is_array($to)){
            foreach($to as $rec){
                $this->_mailer->AddAddress($rec);
            }
        }else{
            $this->_mailer->AddAddress($to);
        }

        if(!empty($cc))
        {
            if(is_array($cc)){
                foreach($cc as $rec){
                    $this->_mailer->AddCC($rec);
                }
            }else{
                $this->_mailer->AddCC($cc);
            }
        }

        if(!empty($bcc))
        {
            if(is_array($bcc)){
                foreach($bcc as $rec){
                    $this->_mailer->AddBCC($rec);
                }
            }else{
                $this->_mailer->AddBCC($bcc);
            }
        }

        $this->_mailer->Body = $body;
        return $this->_mailer->send();
    }
}
?>
