<?php
//require_once(Yii::getPathOfAlias('shared.phpmailer').DIRECTORY_SEPARATOR.'class.phpmailer.php');
require_once(dirname(__FILE__).DIRECTORY_SEPARATOR.'class.phpmailer.php');

class CPhpMailerSMTP
{
    public $errorMsg = null;
    function __construct()
    {
        $this->_mailer = new PHPMailer();
        $this->_mailer->CharSet = "utf-8";
        $this->_mailer->IsSMTP();
        $this->_mailer->SMTPDebug = 0;
        $this->_mailer->SMTPAuth = true;
        $this->_mailer->SMTPSecure = 'ssl';
        $this->_mailer->AltBody = "text/html";
        $this->_mailer->IsHTML(true);
    }
    function init()
    {
        $this->_mailer->Host = $this->host;
        $this->_mailer->Port = $this->port;
        $this->_mailer->Username = $this->user;
        $this->_mailer->Password = $this->pass;
        $this->_mailer->From = $this->from;
        $this->_mailer->FromName = $this->fromName;
    }

    /**
	 * 发送普通的html邮件，不含附件。发送人、格式默认。
	 * @param to mixed，可以为字符串，或者数组。
	 */
	function send($to, $subject, $body,$cc=null, $bcc=null)
    {
        $this->_mailer->ClearAllRecipients();
        $this->_mailer->ClearAttachments();
		$this->_mailer->Subject = $subject;

		if(is_array($to)){
			foreach($to as $rec){
		        $this->_mailer->AddAddress($rec);
			}
		}else{
		    $this->_mailer->AddAddress($to);
		}

        if(!empty($cc))
        {
            if(is_array($cc)){
                foreach($cc as $rec){
                    $this->_mailer->AddCC($rec);
                }
            }else{
                $this->_mailer->AddCC($cc);
            }
        }

        if(!empty($bcc))
        {
            if(is_array($bcc)){
                foreach($bcc as $rec){
                    $this->_mailer->AddBCC($rec);
                }
            }else{
                $this->_mailer->AddBCC($bcc);
            }
        }

		$this->_mailer->Body = $body;
		$ret = $this->_mailer->send();
		$this->errorMsg = $this->_mailer->errorMsg;
		return  $ret;
	}
}
?>
