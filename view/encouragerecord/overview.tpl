{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">
    <div class="row-fluid">
        {if count($sections)}
        <form action="/teambuildingrecord/list/" method="post" style="margin-top:30px;">
            <div style="float:left;width:350px;">
                <label class="control-label" style="float:left;line-height:30px;">选择部门：</label>
                <div class="controls">
                    <select>
                        {foreach $sections as $section}
                        <option value="{$section.id}">{$section.name}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <input style="margin-bottom:12px;" class="btn btn-success" type="submit" value="search" />
        </form>
        {/if}
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">激励经费</div>
                {if $export_flag != 1}
                    <div class="muted pull-right"><a href="/export/TeamAll">激励经费导出</a></div>
                {/if}
            </div>
            <div class="block-content collapse in">
                <div id="liucheng" class="span12">
                    <table class="table table-hover">
                        <thead>
                            <tr>

                                <th width="15%">业务线</th>
                                <th width="15%">部门</th>
                                <th width="15%">负责人</th>
                                <th width="10%">上级部门</th>
                                <th width="15%">部门人数</th>
                                <th>经费余额（元）</th>
                                <th width="10%">操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach $list as $item}
                            <tr>

                                <td>{$item.team_name}</td>
                                <td>{$item.section_name}</td>
                                <td>{$item.manager_name}</td>
                                <td>{$item.parent_name}</td>
                                <td>{count($sectionUserNum[$item.section_id]['users'])}</td>
                                <td>{$item.amount}</td>
                                <td>
                                    <a href="/teamencouragerecord/list?section_id={$item.section_id}">查看</a>
                                </td>
                            </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>

</div>
<script type="text/javascript">
var absenteeismType = {json_encode($absenteeismType)};
{literal}
$(function(){
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
    $('.btn').click(function(){
        var absenteeism_type = $(this).parentsUntil('tr','td').find('[name="absenteeism_type"]').val();  
        var id = $(this).attr('data-value');
        var me = $(this);
        if(absenteeism_type != 0){
            $.ajax({
                url :'/checkinout/edit',
                data : {id:id,absenteeism_type:absenteeism_type},
                type : 'post',
                dataType : 'json',
                success : function(data){
                    if(data.code == 0){
                        me.parentsUntil('tr','td').html(absenteeismType[absenteeism_type]); 
                    } 
                }
            }) 
        }
    });      
});
{/literal}
</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
