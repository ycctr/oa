{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">人工修正团建经费</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">为部门修正团建经费,可增可减</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/teambuildingrecord/finishing/" method="post">
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">部门</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select required" name="section_id">
                                        <option value="0">请选择</option>
                                        {foreach $sections as $section}
                                        <option value="{$section.id}">{$section.name}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">金额</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required number" placeholder="调整金额" name="amount" value="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">备注</label>
                                <div class="controls">
                                    <textarea name="remark" style="width: 575px; height: 80px"></textarea>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
});

$(function() {
    function calculateDays()
    {
        var absenceType = $('#select01').val();
        var startDay = $('[name="start_day"]').val();
        var endDay = $('[name="end_day"]').val();
        var startNode = $('[name="start_node"]').val();
        var endNode = $('[name="end_node"]').val();

        if(startDay != '' && endDay != '' && startNode != '' && endNode != '' && absenceType != ''){
            $.ajax({
                url : '/userworkflow/CalculateSpan',
                data : {start_day:startDay,end_day:endDay,start_node:startNode,end_node:endNode,absence_type:absenceType},
                type:'get',
                dataType:'json',
                success:function(data){
                    $('[name="day_number"]').val(data.dayNum);                     
                }
            });
        }
        
    }
    $('.time_node,#select01').change(function(){
        calculateDays();      
    });
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true,
        afterSelectDate: calculateDays
    });
});
$(document).delegate('form','submit',function(){
	var dayNum = $('[name="day_number"]').val();
	if(dayNum == 0){
		alert('请假天数不能为零');
		return false;
	}
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
