{extends file="./_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
{/block}
{block name='title'}销出差导出
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}
 
<div id="content" class="span9">
<div>
    <form  method="get" action="cancel" style="margin-bottom:0px">
        <fieldset>
        <div class="control-group success">
            <div class="controls" style="margin-top:15px;">
                日期
                <div class="input-append date datetimepicker">
                    <input class="add-on" name="query[start_time]" placeholder="起始日期" type="text" value="{if $smarty.get.query.start_time eq null}{$initTime.start_time}{else}{$smarty.get.query.start_time}{/if}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                                    到
                <div class="input-append date datetimepicker">
                    <input class="add-on" name="query[end_time]" placeholder="结束日期" type="text" value="{if $smarty.get.query.end_time eq null}{$initTime.end_time}{else}{$smarty.get.query.end_time}{/if}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="control-group">
            部门<input type="text" name="query[section_name]" value="{$smarty.get.query.section_name}" style="margin:0 15px;width:120px;"></input>
            姓名<input type="text" name="query[name]" value="{$smarty.get.query.name}"style="margin:0 15px;width:120px;"></input>
            工号<input type="text" name="query[id_card]" value="{$smarty.get.query.id_card}"style="margin:0 15px;width:120px;"></input>
        </div>
        <div class="btn-group">
            <button class="btn btn-success" type="submit" style="margin-bottom:10px; background: none repeat scroll 0% 0% rgb(33, 122, 237);padding-left: 19px; padding-right: 19px;border-left-width:3px;border-right-width:3px;">搜索</button>
        </div>
        </fieldset>
    </form>
</div>

 <div class = "block-content collapse in">
    {if !empty($arrList)}
    <div class="span12">
        <div class="table-toolbar" style="margin-bottom:18px;">
            <div class="btn-group">
                <a href="{$savelink}">
                    <button class="btn btn-success">保存
                    </button>
                </a>
            </div>
        </div>
        {include file="./widget/cancel_business.tpl"}
    </div>
    {/if}
</div>
</div>
{if $msg neq "" }

<div class="alert alert-error " id="nameRepeat"> 
    <button class="close" data-dismiss="alert"></button> 
    {$msg}
</div> 

{/if}

<script>
$(function() {
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
});
</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>
{/block}
