{extends file="./_base.tpl"}
{block name='title'}角色管理
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}

<div id="content" class="span9">
    <div class="block" style="margin-top:30px;">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                角色权限
            </div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
                <form method="post" action="{if $is_edit eq 1}{$Yii->createUrl('role/edit')}{else}{$Yii->createUrl('role/add')}{/if}" id="f1">
                    <div class="table-toolbar" style="margin-bottom:18px;">
                        <div class="alert alert-error hide" id="inputEmpty">
                            <button class="close" data-dismiss="alert"></button>
                                请输入角色名称。
                        </div>
                        <div class="alert alert-error hide" id="nameRepeat">
                            <button class="close" data-dismiss="alert"></button>
                                角色名称重复。
                        </div>
                        角色名称
                        <input type="hidden" name="role[id]" id="roleid" value="{$role_id}"></input>
                            {if $is_view neq 1}
                                <input type="text"class="span3 required" id="typeahead" name="role[name]" value="{$role_name}" ></input>
                                    <div class="btn-group">
                                        <button class="btn btn-success">保存</button>
                                    </div>
                            {else}
                                <input type="text" name="name" value="{$role_name}" readonly="readonly"></input>
                            {/if}
                    </div>
                    {include file="./widget/role_right.tpl"}
                </form>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){

    $(".select_all").click(function(){
        var self = $(this);
        if(self.is(':checked'))
        {
            self.parent().parent().find('input[type="checkbox"]:not(.select_all)').prop('checked',true);
        }
        else
        {
            self.parent().parent().find('input[type="checkbox"]:not(.select_all)').prop('checked',false);
        }
    });

    $(".module_ck").click(function(){
		var self = $(this);
		var allChecked = true;
		if(!self.is(':checked'))
		{
			$(this).parent().next().find('input[type="checkbox"]').prop('checked',false);
			allChecked = false;
		}
			
		$(this).parent().next().find('input[type="checkbox"]').each(function(){
		if(!$(this).is(':checked'))
		{
			allChecked = false;
			return false;
		}
		});
	
		if(allChecked == false)
		{
			$(this).parent().prev().find('input[type="checkbox"]').prop('checked',false);
		}
		else
		{
			$(this).parent().prev().find('input[type="checkbox"]').prop('checked', true);
		}
	});
	
		$('.op_ck').click(function(){
			var allChecked = true;
			$(this).parent().find('input[type="checkbox"]').each(function(){
				if(!$(this).is(':checked')){
					allChecked = false;
					return false;
				}});
			if(!$(this).parent().prev().find('input[type="checkbox"]').is(':checked'))
			{
				allChecked = false;
			}
	
			if(allChecked == false)
			{
				$(this).parent().prev().prev().find('input[type="checkbox"]').prop('checked',false);
			}
			else
			{
				$(this).parent().prev().prev().find('input[type="checkbox"]').prop('checked', true);
			}
	
			var isChecked = false;
		    $(this).parent().find('input[type="checkbox"]').each(function(){
				if($(this).is(':checked'))
				{
					isChecked = true;
					return true;
				}
			});
			if(isChecked == true)
			{
                $(this).parent().prev().find('input[type="checkbox"]').prop('checked', true);
			}
		});

});
$(function(){

     $('.btn-success').click(function(){
        var val = $('#typeahead').val();  
        if($.trim(val) == ''){
            $('#nameRepeat').hide();
            $('#inputEmpty').show();
            return false;
        }
        $('#inputEmpty').hide();
        var name = $('#typeahead').val();
        var roleid = $('#roleid').val();
        var result = false;
        $.ajax({
            url :  '/role/rolevalidate',
            data : { role_name:name,role_id:roleid },
            type: 'get',
            dataType:'json',
            async: false,
            success:function(data){
                if(!data)
                    $('#nameRepeat').show();
                result = data;
                }
            });
        return result;
    });
 });
</script>
{/block}

