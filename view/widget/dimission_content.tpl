<table class="table table-hover">
    <thead>
        <tr>
            <th>员工姓名</th>
            <th>工号</th>
            <th>手机号</th>
			<th>级别</th>
			<th>所属部门</th>
			<th>直属上级</th>
			<th>离职审批</th>
			<th>交接清单</th>
            {*<th style="width: 15%">操作</th>*}
        </tr>
    </thead>
    <tbody>
        {foreach $dimissions as $item}
        <tr>

            <td>{$item.name}</td>
            <td>{$item.id_card}</td>
            <td>{$item.mobile}</td>
			<td>{$levelNumToName[$item.level]}</td>
			<td>{$item.section_name}</td>
			<td>{$item.manager_name}</td>
			<td>
				{if $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_PASS}
					<a target="_blank" href="/hr/dimissionDetail?id={$item.id}&target=0">查看</a>
				{else}
					暂无
				{/if}
			</td>
			<td>
				{if $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_PASS}
					<a target="_blank" href="/hr/dimissionDetail?id={$item.id}&target=1">查看</a>
				{else}
					暂无
				{/if}
			</td>
        </tr>
        {/foreach}
    </tbody>
</table>
<div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
    <ul>
        {pager_oa count=$cnt pagesize=$rn page=$pn pagelink=$url|cat:"pn=%d" list=3}
    </ul>
</div>

