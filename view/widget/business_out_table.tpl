<table class="table table-hover" name="user">
    <thead>
        <tr>
            <td>工号</td>
            <td>姓名</td>
            <td>部门</td>
            <td>开始时间</td>
            <td>结束时间</td>
            <td>外出天数</td>
            <td>外出地址</td>
            <td>外出原因</td>
            <td>审核状态</td>
        </tr>
    </thead>
    <tbody>
        {foreach from=$arrList item=item key=key}
            <tr>
            <td>{$item.id_card}</td>
            <td>{$item.name}</td>
            <td>{$item.section_name}</td>
            <td>{$item.start_time|date_format:"%Y-%m-%d"}{OAConfig::$time_node[date('H:i:s',$item.start_time)]}</td>
            <td>{$item.end_time|date_format:"%Y-%m-%d"}{OAConfig::$time_node[date('H:i:s',$item.end_time)]}</td>
            <td>{$item.day_number}</td>
            <td>{$item.city}</td>
            <td>{$item.leave_reason}</td>
            <td>
                {if $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_SHENPIZHONG}
                    审批中
                {elseif  $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_PASS}
                    通过
                {elseif $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_REJECT }
                    拒绝
                {elseif $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_CHUANGJIAN}
                    创建
                {/if}
            </td>
            </tr>
        {/foreach}
    </tbody>
</table>

