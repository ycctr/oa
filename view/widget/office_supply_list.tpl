<table class="table table-hover" name="user">
    <thead>
        <tr>
            <td>领用人姓名</td>
            <td>工号</td>
            <td>具体部门</td>
            <td>部门</td>
            <td>业务线</td>
            <td>申请日期</td>
            <td>领用日期</td>
            <td>办公用品类型</td>
            <td>具体物品</td>
            <td>数量</td>
            <td>办公区</td>
        </tr>
    </thead>
    <tbody>
        {foreach $arrList as $key => $record}
            <tr>
                <td>{$record.user_name}</td>
                <td>{$record.user_id_card}</td>
                <td>{$record.jutibumen}</td>
                <td>{$record.bumen}</td>
                <td>{$record.yewuxian}</td>
                <td>{$record.create_time|date_format:"Y-m-d"}</td>
                <td>{$record.uwf_modify_time|date_format:"Y-m-d"}</td>
                <td>{$record.supply_type}</td>
                <td>{$record.supply_detail}</td>
                <td>{$record.supply_amount}</td>
                <td>{$record.office_location}</td>
            </tr>
        {/foreach}
    </tbody>
</table>

