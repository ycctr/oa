{extends file="../../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="header"}
{/block}
{block name="content"}
<div id="content" style="width:700px; margin:0 auto;">
    <style>
        body {
            padding:0;
        }
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
        }
        .form-horizontal .block tr input {
            width:80%;
        }
        .form-horizontal .control-group{
            height: 30px;
        }
        .form-horizontal .control-label{
            text-align: left;
            width: 12%;
            height: 30px;
            line-height: 30px;
            float: left;
        }
        .form-horizontal .controls{
            height: 30px;
            padding-left: 0px;
            margin-left: 0px;
            line-height: 30px;
        }
        @media print {
            .table {
                border-collapse:collapse;
            }
            .table td {
                font-size:12px;
                border:1px #000 solid;
                line-height:26px;
                padding:3px;
            }
            .navbar {
                height:40px;
                line-height:40px;
            }
            .print {
                display:none;
            }
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">出差报销</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <div class="form-horizontal">
                        <div class="title">
                            <div style="float:left;">
                            姓名：{$user.name}
                            工号：{$user.id_card}
                            部门：{$section.name}                   
                            日期：{$smarty.now|date_format:"%Y-%m-%d"}
                            </div>
                            <button style="float:right;" class="btn btn-success print">打印</button>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="card_no">银行卡号：</label>
                            <div class="controls">
                                {$business_reimbursement.card_no}
                            </div>
                        </div>
                         <div class="control-group">
                            <label class="control-label" for="card_no">出差日期：</label>
                            <div class="controls">
                            {$business_reimbursement.bstart_day|date_format:"%Y-%m-%d"}
                            {if $business_reimbursement.bstart_node eq 1}上午{elseif $business_reimbursement.bstart_node eq 2}中午{/if}
                            到
                            {$business_reimbursement.bend_day|date_format:"%Y-%m-%d"}
                            {if $business_reimbursement.bend_node eq 3}下午{elseif $business_reimbursement.bend_node eq 2}中午{/if}
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="card_no">出差地点：</label>
                            <div class="controls">
                                {$business_reimbursement.business_place}
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="card_no">出差目的：</label>
                            <div class="controls">
                                {$business_reimbursement.business_purpose}
                            </div>
                        </div>
                        {if !empty($business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_TRAFFIC])}
                        <div class="block">
                            <div style="" class="navbar navbar-inner block-header">
                                <div class="muted pull-left">交通报销明细</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th width="15%">日期</th>
                                                <th width="15%">交通工具</th>
                                                <th width="20%">起点</th>
                                                <th width="20%">终点</th>
                                                <th width="20%">具体时间</th>
                                                <th width="10%">金额</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {assign var="traffic_total" value=0}
                                        {foreach $business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_TRAFFIC] as $item}
                                            {assign var="traffic_total" value=$traffic_total+$item.amount}
                                            <tr>
                                                <td>{$item.date|date_format:"%Y-%m-%d"}</td>
                                                <td>{if $item.vehicle eq 1}火车{elseif $item.vehicle eq 2}客车{elseif $item.vehicle eq 3}飞机{elseif $item.vehicle eq 4}轮船{elseif $item.vehicle eq 5}其他{/if}</td>
                                                <td>{$item.start_point}</td>
                                                <td>{$item.end_point}</td>
                                                <td>{$item.time|date_format:"%H:%M:%S"}</td>
                                                <td>{$item.amount}</td>
                                            </tr>
                                        {/foreach}
                                            <tr>
                                                <td colspan="5">合计</td>
                                                <td>{$traffic_total}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {/if}
                        {if !empty($business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_TAXI])}
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">出租车报销明细</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th width="90%">交通工具</th>
                                                <th width="10%">总金额</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {assign var="taxi_total" value=0}
                                        {foreach $business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_TAXI] as $item}
                                            {assign var="taxi_total" value=$taxi_total+$item.amount}
                                            <tr>
                                                <td>出租车</td>
                                                <td>{$item.amount}</td>
                                            </tr>
                                            {/foreach}
                                            <tr>
                                                <td colspan="1">合计</td>
                                                <td>{$taxi_total}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {/if}
                        {if !empty($business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_RENT])}
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">房租报销明细</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th width="40%">日期</th>
                                                <th width="50%">酒店名称</th>
                                                <th width="10%">金额</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {assign var="rent_total" value=0}
                                        {foreach $business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_RENT] as $item}
                                            {assign var="rent_total" value=$rent_total+$item.amount}
                                            <tr>
                                                <td>{$item.start_day|date_format:"%Y-%m-%d"}{if $item.start_node eq 1}上午{elseif $item.start_node eq 2}中午{/if}到{$item.end_day|date_format:"%Y-%m-%d"}{if $item.end_node eq 3}下午{elseif $item.end_node eq 2}中午{/if}</td>
                                                <td>{$item.hname} </td>
                                                <td>{$item.amount}</td>
                                            </tr>
                                            {/foreach}
                                            <tr>
                                                <td colspan="2">合计</td>
                                                <td>{$rent_total}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {/if}
                        {if !empty($business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_ENTERTAINMENT])}
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">招待费报销明细</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th width="15%">日期</th>
                                                <th width="15%">地点</th>
                                                <th width="15%">业务目的</th>
                                                <th width="15%">客户单位</th>
                                                <th width="15%">客户姓名</th>
                                                <th width="15%">参加人员</th>
                                                <th width="10%">金额</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {assign var="zhaodai_total" value=0}
                                        {foreach $business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_ENTERTAINMENT] as $item}
                                            {assign var="zhaodai_total" value=$zhaodai_total+$item.amount}
                                            <tr>
                                                <td>{$item.date|date_format:"%Y-%m-%d"}</td>
                                                <td>{$item.hname} </td>
                                                <td>{$item.goal}</td>
                                                <td>{$item.company}</td>
                                                <td>{$item.name}</td>
                                                <td>{$item.participant}</td>
                                                <td>{$item.amount}</td>
                                            </tr>
                                            {/foreach}
                                            <tr>
                                                <td colspan="6">合计</td>
                                                <td>{$zhaodai_total}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {/if}
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="1" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <td>合计</td>
                                                <td style="text-align:right;" width="10%">{$totalCnt}</td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <table style="width:100%;">
                            <tr>
                                <th style="width:30%;text-align:left;height:80px;">部门主管<br />Approval by</th>
                                <td style="width:20%;text-align:left;">{$manage.user_name}</td>
                                <th style="width:30%;text-align:left;">部门负责人<br />Vice-president's Signature</th>
                                <td style="width:20%;text-align:left;">{$vicePresident.user_name}</td>
                            </tr>
                            <tr>
                                <th style="width:30%;text-align:left;height:80px;">CEO签字<br />CEO Signature</th>
                                <td style="width:20%;text-align:left;">{$ceo.user_name}</td>
                                <th style="width:30%;text-align:left;">财务负责人签字<br />Financial signature</th>
                                <td style="width:20%;text-align:left;"></td>
                            </tr>
                            <tr>
                                <th style="width:30%;text-align:left;height:80px;">人事签字<br />Human Resource's Signature</th>
                                <td style="width:20%;text-align:left;"></td>
                                <th style="width:30%;text-align:left;">财务审核<br />Check by</th>
                                <td style="width:20%;text-align:left;"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
<script>
$(function(){
    //window.print();      
    $('.print').click(function(){
        window.print();  
    });
});
</script>
{/block}

