<div class="ba">
    <div class="control-group">
        <label class="control-label" for="card_no">银行卡号：</label>
        <div class="controls">
            {$business_reimbursement.card_no}
        </div>
    </div>
     <div class="control-group">
        <label class="control-label" for="card_no">出差日期：</label>
        <div class="controls">
            {$business_reimbursement.bstart_day|date_format:"%Y-%m-%d"}
            {if $business_reimbursement.bstart_node eq 1}上午{elseif $business_reimbursement.bstart_node eq 2}中午{/if}
            到
            {$business_reimbursement.bend_day|date_format:"%Y-%m-%d"}
            {if $business_reimbursement.bend_node eq 3}下午{elseif $business_reimbursement.bend_node eq 2}中午{/if}
        </div>
    </div>
     <div class="control-group">
        <label class="control-label" for="card_no">出差地点：</label>
        <div class="controls">
            {$business_reimbursement.business_place}
        </div>
    </div>
     <div class="control-group">
        <label class="control-label" for="card_no">出差目的：</label>
        <div class="controls">
            {$business_reimbursement.business_purpose}
        </div>
    </div>
</div>
{if !empty($business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_TRAFFIC])}
<div class="block ba">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">交通报销明细</div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="table">
                <thead>
                    <tr>
                        <th width="15%">日期</th>
                        <th width="15%">交通工具</th>
                        <th width="20%">起点</th>
                        <th width="20%">终点</th>
                        <th width="20%">具体时间</th>
                        <th width="10%">金额</th>
                    </tr>
                </thead>
                <tbody>
                {assign var="traffic_total" value=0}
                {foreach $business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_TRAFFIC] as $item}
                    {assign var="traffic_total" value=$traffic_total+$item.amount}
                    <tr>
                        <td>{$item.date|date_format:"%Y-%m-%d"}</td>
                        <td>{if $item.vehicle eq 1}火车{elseif $item.vehicle eq 2}客车{elseif $item.vehicle eq 3}飞机{elseif $item.vehicle eq 4}轮船{elseif $item.vehicle eq 5}其他{/if}</td>
                        <td>{$item.start_point}</td>
                        <td>{$item.end_point}</td>
                        <td>{$item.time|date_format:"%H:%M:%S"}</td>
                        <td>{$item.amount}</td>
                    </tr>
                {/foreach}
                    <tr>
                        <td colspan="5">合计</td>
                        <td>{$traffic_total}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
{/if}
{if !empty($business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_TAXI])}
<div class="block ba">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">出租车报销明细</div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="table">
                <thead>
                    <tr>
                        <th width="90%">交通工具</th>
                        <th width="10%">总金额</th>
                    </tr>
                </thead>
                <tbody>
                {assign var="taxi_total" value=0}
                {foreach $business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_TAXI] as $item}
                    {assign var="taxi_total" value=$taxi_total+$item.amount}
                    <tr>
                        <td>出租车</td>
                        <td>{$item.amount}</td>
                    </tr>
                    {/foreach}
                    <tr>
                        <td colspan="1">合计</td>
                        <td>{$taxi_total}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
{/if}
{if !empty($business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_RENT])}
<div class="block ba">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">房租报销明细</div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="table">
                <thead>
                    <tr>
                        <th width="40%">日期</th>
                        <th width="50%">酒店名称</th>
                        <th width="10%">金额</th>
                    </tr>
                </thead>
                <tbody>
                {assign var="rent_total" value=0}
                {foreach $business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_RENT] as $item}
                    {assign var="rent_total" value=$rent_total+$item.amount}
                    <tr>
                        <td>{$item.start_day|date_format:"%Y-%m-%d"}{if $item.start_node eq 1}上午{elseif $item.start_node eq 2}中午{/if}到{$item.end_day|date_format:"%Y-%m-%d"}{if $item.end_node eq 3}下午{elseif $item.end_node eq 2}中午{/if}</td>
                        <td>{$item.hname} </td>
                        <td>{$item.amount}</td>
                    </tr>
                    {/foreach}
                    <tr>
                        <td colspan="2">合计</td>
                        <td>{$rent_total}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
{/if}
{if !empty($business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_ENTERTAINMENT])}
<div class="block ba">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">招待费报销明细</div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="table">
                <thead>
                    <tr>
                        <th width="15%">日期</th>
                        <th width="15%">地点</th>
                        <th width="15%">业务目的</th>
                        <th width="15%">客户单位</th>
                        <th width="15%">客户姓名</th>
                        <th width="15%">参加人员</th>
                        <th width="10%">金额</th>
                    </tr>
                </thead>
                <tbody>
                {assign var="zhaodai_total" value=0}
                {foreach $business_reimbursement[$smarty.const.BUSINESS_REIMBURSEMENT_TYPE_ENTERTAINMENT] as $item}
                    {assign var="zhaodai_total" value=$zhaodai_total+$item.amount}
                    <tr>
                        <td>{$item.date|date_format:"%Y-%m-%d"}</td>
                        <td>{$item.hname} </td>
                        <td>{$item.goal}</td>
                        <td>{$item.company}</td>
                        <td>{$item.name}</td>
                        <td>{$item.participant}</td>
                        <td>{$item.amount}</td>
                    </tr>
                    {/foreach}
                    <tr>
                        <td colspan="6">合计</td>
                        <td>{$zhaodai_total}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
{/if}