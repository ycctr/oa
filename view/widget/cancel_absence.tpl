<table class="table table-hover" name="user">
    <thead>
        <tr>
            <td>姓名</td><td>工号</td><td>标题</td><td>开始时间</td><td>结束时间</td><td>部门</td><td>上级部门</td>
            <td>销假原因</td><td>天数</td><td>状态</td>
        </tr>
    </thead>
    <tbody>
        {foreach from=$arrList item=item key=key}
            <tr>
            <td>{$item.uname}</td>
            <td>{$item.id_card}</td>
            <td>{$item.title}</td>
            <td>{$item.start_time|date_format:"%Y-%m-%d"}{OAConfig::$time_node[date('H:i:s',$item.start_time)]}</td>
            <td>{$item.end_time|date_format:"%Y-%m-%d"}{OAConfig::$time_node[date('H:i:s',$item.end_time)]}</td>
            <td>{$item.sname}</td>
            <td>{$item.teamnam}</td>
            <td>{$item.leave_reason}</td>
            <td>{$item.day_number}</td>
            <td>
                {if $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_SHENPIZHONG}
                    审批中
                {elseif  $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_PASS}
                    通过
                {/if}
            </td>
            </tr>
        {/foreach}
    </tbody>
</table>

