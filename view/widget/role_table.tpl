<table class="table table-hover" name="user">
    <thead>
        <tr>
            <td>id</td><td>名称</td><td>创建时间</td><td>修改时间</td><td>操作</td> 
        </tr>
    </thead>
    <tbody>
        {foreach item=item from=$arrList}
            <tr>
            <td>{$item.id}</td>
            <td>{$item.name}</td>
            <td>{$item.create_time|date_format:"%Y-%m-%d"}<input type="hidden" name="create_time" value="{$item.create_time}"></td>
            <td>{$item.modify_time|date_format:"%Y-%m-%d"}<input type="hidden" name="modify_time" value="{$item.modify_time}"></td>
            <td>
                <table>
                    <tr>
                        {if $view_p eq 1}<a href="{$Yii->createUrl('role/view')}?id={$item.id}">查看</a> | {/if}
                        {if $edit_p eq 1}<a href="{$Yii->createUrl('role/edit')}?id={$item.id}">修改</a>  {/if}
                        {if $item.status eq 0}
                            {if $recover_p eq 1}
                                <a href="{$Yii->createUrl('role/recover')}?id={$item.id}">恢复</a>
                            {/if}
                        {else}
                            {if $del_p eq 1}
                               | <a href="{$Yii->createUrl('role/del')}?id={$item.id}&name={$item.name}" >删除</a>
                            {/if}
                        {/if}    
                    </tr>
                </table>
            </td>
            </tr>
        {/foreach}
    </tbody>
</table>

