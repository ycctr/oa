<table class="table table-hover" name="user">
    <thead>
        <tr>
            <td>姓名</td><td>工号</td><td>标题</td><td>开始时间</td><td>结束时间</td><td>部门</td><td>上级部门</td>
            <td>请假原因</td><td>天数</td><td>请假类别</td><td>状态</td><td>查看</td>
        </tr>
    </thead>
    <tbody>
        {foreach from=$arrList item=item key=key}
            <tr>
            <td>{$item.name}</td>
            <td>{$item.id_card}</td>
            <td>{$item.title}</td>
            <td>{$item.start_time|date_format:"%Y-%m-%d"}{OAConfig::$time_node[date('H:i:s',$item.start_time)]}</td>
            <td>{$item.end_time|date_format:"%Y-%m-%d"}{OAConfig::$time_node[date('H:i:s',$item.end_time)]}</td>
            <td>{$item.section_name}</td>
            <td>{$item.parent_name}</td>
            <td>{$item.leave_reason}</td>
            <td>{$item.day_number}</td>
            <td>{OAConfig::$absenceType[$item.type]}
            <td>
                {if $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_SHENPIZHONG}
                    审批中
                {elseif  $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_PASS}
                    通过
                {elseif $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_REJECT }
                    拒绝
                {elseif $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_CHUANGJIAN}
                    创建
                {elseif $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_CANCEL}
                    取消审核
                {/if}
            </td>
             <td><a href="/userworkflow/look?id={$item.user_workflow_id}">查看</a></td>
            </tr>
        {/foreach}
    </tbody>
</table>

