<table class="table table-hover">
    <thead>
        <tr>
            <th>姓名</th>
            <th>业务线</th>
            <th>部门</th>
			<th>分部</th>
			<th>预计报到日期</th>
			<th>部门评估</th>
			<th>人力评估</th>
			<th>入职审批</th>
            {*<th style="width: 15%">操作</th>*}
        </tr>
    </thead>
    <tbody>
        {foreach $entries as $item}
        <tr>

            <td>{$item.name}</td>
            <td>{$item.business_line}</td>
            <td>{$item.section}</td>
			<td>{if $item.subsection == 'Estaff'}无{else}{$item.subsection}{/if}</td>
			<td>{$item.approx_arrive_time|date_format:"%Y-%m-%d"}</td>
            <td>
				{foreach $item.departmentAssessments as $da}
					<a target="_blank" href="/hr/departmentAssessmentDetail?id={$da.id}">查看</a>
				{/foreach}
			</td>
			<td>
				{foreach $item.hrAssessments as $ha}
					<a target="_blank" href="/hr/hrAssessmentDetail?id={$ha.id}">查看</a>
				{/foreach}
			</td>
			<td>
				{if $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_PASS}
				<a target="_blank" href="/hr/entryDetail?id={$item.id}">查看</a>
				{/if}
			</td>
        </tr>
        {/foreach}
    </tbody>
</table>
<div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
    <ul>
        {pager_oa count=$cnt pagesize=$rn page=$pn pagelink=$url|cat:"pn=%d" list=3}
    </ul>
</div>

