<table class="table table-hover" name="section">
    <thead>
        <tr>
            <td>部门id</td><td>名称</td><td>负责人</td><td>上级部门</td>
            <td>操作</td> 
        </tr>
    </thead>
    <tbody>
        {foreach from=$data.info item=item key=key}
            <tr>
            <td>{$item.id}</td>
            <td>{$item.name}</td>
            <td>{$item.manager_name}</td>
            <td>
                {if ($item.parent_name=="") }
                    无
                {else}
                    {$item.parent_name}
                {/if}
            </td>
            <td>
                <a href="editMoney?id={$item.id}">编辑</a>
            </td>
            </tr>
        {/foreach}
    </tbody>
</table>

