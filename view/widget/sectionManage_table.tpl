<table class="table table-hover" name="section">
    <thead>
        <tr>
            <td>部门id</td><td>名称</td><td>负责人</td><td>上级部门</td><td>部门人员</td>
            <td>创建时间</td><td>修改时间</td><td>操作</td> 
        </tr>
    </thead>
    <tbody>
        {foreach from=$data.info item=item key=key}
            <tr>
            <td>{$item.id}</td>
            <td>{$item.name}</td>
            <td>{$item.manager_name}</td>
            <td>
                {if ($item.parent_name=="") }
                    无
                {else}
                    {$item.parent_name}
                {/if}
            </td>
            <td><a href="sectionUser?id={$item.id}">{$item.user_num}</a></td>
            <td>{$item.create_time|date_format:"%Y-%m-%d"}</td>
            <td>{$item.modify_time|date_format:"%Y-%m-%d"}</td>
            <td>
                {if $edit_p eq 1}
                    <a href="edit?id={$item.id}">编辑</a>
                {/if}
                {if $del_p eq 1}
                    | <a href="delete?id={$item.id}">删除</a>
                {/if}
            </td>
            </tr>
        {/foreach}
    </tbody>
</table>

