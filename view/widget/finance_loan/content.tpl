<style type="text/css">
.controls{
    padding-top: 5px;
}
</style>
<div class="control-group">
    <label class="control-label" for="select01">合同类型</label>
    <div class="controls">
        {$extension.con_type}
    </div>
</div>
<div class="control-group">
    <label class="control-label">请款事项简介</label>
    <div class="controls">
        {$extension.summary}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">请款金额</label>
    <div class="controls">
        {$extension.amount}元
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">请款性质</label>
    <div class="controls">
        {$pay_types[$extension.pay_type]}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">开户银行</label>
    <div class="controls">
        {$extension.bank_account}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">账户名称</label>
    <div class="controls">
        {$extension.account_name}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">账号号码</label>
    <div class="controls">
        {$extension.account_number}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">备注信息</label>
    <div class="controls">
        {$extension.remark}
    </div>
</div>