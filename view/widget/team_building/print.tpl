{extends file="../../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="header"}
{/block}
{block name="content"}
<div id="content" style="width:700px; margin:0 auto;">
    <style>
        body {
            padding:0;
        }
        .row-fluid .span12 {
            width:99%;
        }
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
        }
        .form-horizontal .block {
            border:0px;
        }
        .table td {
            border-left:1px #ddd solid;
        }
        .table .td_r {
            border-right:1px #ddd solid;
        }
        .table .td_b {
            border-bottom:1px #ddd solid;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
        }
        .form-horizontal .block tr input {
            width:80%;
        }
        @media print {
            .table {
                border-collapse:collapse;
            }
            .form-horizontal .table td {
                font-size:12px;
                border:1px #000 solid;
                line-height:26px;
                padding:3px;
            }
            .navbar {
                height:40px;
                line-height:40px;
            }
            .print {
                display:none;
            }
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">团建报销</div>
            </div>
            <div class="block-content collapse in">
                <button style="float:right;" class="btn btn-success print">打印</button>
                <div class="span12">
                    <div class="form-horizontal">
                        <div class="block">
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td class="td_b" rowspan="14" valign="middle" align="center" width="20%">团队建设经费使用报销</td>
                                                <td align="center" width="20%">报销人</td>
                                                <td class="td_r">{$user.name}（{$user.id_card}）</td>
                                            </tr>
                                            <tr>
                                                <td align="center" width="20%">部门</td>
                                                <td class="td_r">{$building_section.name}</td>
                                            </tr>
                                            <tr>
                                                <td align="center">活动类型</td>
                                                <td class="td_r">{$activity_type[$userWorkflow.ext.activity_type]}</td>
                                            </tr>
                                            <tr>
                                                <td align="center">活动时间</td>
                                                <td class="td_r">{$userWorkflow.ext.activity_time|date_format:"%Y-%m-%d"}</td>
                                            </tr>
                                            <tr>
                                                <td align="center">活动背景</td>
                                                <td class="td_r">{$userWorkflow.ext.activity_story}</td>
                                            </tr>
                                            <tr>
                                                <td align="center">活动目的</td>
                                                <td class="td_r">{$userWorkflow.ext.title}</td>
                                            </tr>
                                            <tr>
                                                <td align="center">活动内容</td>
                                                <td class="td_r">{$userWorkflow.ext.desc}</td>
                                            </tr>
                                            <tr>
                                                <td align="center">活动地点</td>
                                                <td class="td_r">{$userWorkflow.ext.activity_address}</td>
                                            </tr>

                                            <tr>
                                                <td align="center">参加人数</td>
                                                <td class="td_r">{$userWorkflow.ext.person_num}</td>
                                            </tr>

                                            <tr>
                                                <td align="center">参加人员</td>
                                                <td class="td_r">{$userWorkflow.ext.participant}</td>
                                            </tr>
                                            <tr>
                                                <td align="center">报销金额</td>
                                                <td class="td_r">{$userWorkflow.ext.amount}</td>
                                            </tr>

                                            <tr>
                                                <td align="center">人均消费</td>
                                                <td class="td_r">{$userWorkflow.ext.avg_amount}</td>
                                            </tr>
                                            <tr>
                                                <td align="center">欠款金额</td>
                                                <td class="td_r">{$userWorkflow.ext.debt}</td>
                                            </tr>
                                            <tr>
                                                <td class="td_b" align="center">经费余额</td>
                                                <td class="td_r td_b">{$balance}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="span12">
                            <table style="width:100%;">
                                <tr>
                                    <th style="width:20%;text-align:center;height:60px;">部门负责人</th>
                                    <td style="text-align:left;">{$manage.user_name}</td>
                                    <th style="width:20%;text-align:center;height:60px;">人力资源部</th>
                                    <td style="text-align:left;">{$renli.user_name}</td>
                                </tr>
                                <tr>
                                    <th style="width:20%;text-align:center;height:60px;">人力主管</th>
                                    <td style="text-align:left;">{$renlizhuguan.user_name}</td>
                                    <th style="width:20%;text-align:center;height:60px;">业务线负责人</th>
                                    <td style="text-align:left;">{$vp_name.user_name}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
<script>
$(function(){
    //window.print();      
    $('.print').click(function(){
        window.print();  
    });
});
</script>
{/block}

