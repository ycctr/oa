<table class="table table-hover" name="user">
    <thead>
        <tr>
            <td>票务类型</td>
            <td>出行人姓名</td>
            <td>工号</td>
            <td>具体部门</td>
            <td>部门</td>
            <td>业务线</td>
            <td>申请日期</td>
            <td>审批日期</td>
            {*<td>身份证号码</td>*}
            {*<td>手机号码</td>*}
            <td>出行日期</td>
            <td>出行时间段</td>
            <td>出发城市</td>
            <td>抵达城市</td>
            <td>航班号</td>
            <td>原票价</td>
            <td>额外费用</td>
            {*<td>原因</td>*}
        </tr>
    </thead>
    <tbody>
        {foreach $arrList as $key => $record}
            <tr>
                <td>
                    {if $record.status == 1}
                    订票
                    {elseif $record.status == 2}
                    退票
                    {elseif $record.status == 3}
                    改签
                    {/if}
                </td>
                <td>{$record.user_name}</td>
                <td>{$record.user_id_card}</td>
                <td>{$record.jutibumen}</td>
                <td>{$record.bumen}</td>
                <td>{$record.yewuxian}</td>
                <td>{$record.create_time|date_format:"Y-m-d"}</td>
                <td>{$record.uwf_modify_time|date_format:"Y-m-d"}</td>
                {*<td>{$record.identity_card}</td>*}
                {*<td>{$record.mobile_number}</td>*}
                <td>{if $record.trip_date}{$record.trip_date|date_format:"Y-m-d"}{else}{/if}</td>
                <td>{if $record.trip_time1}{$record.trip_time1}至{$record.trip_time2}{else}{/if}</td>
                <td>{$record.from_city}</td>
                <td>{$record.to_city}</td>
                <td>{$record.flight_number}</td>
                <td>{$record.flight_price}</td>
                <td>{$record.extra_fee}</td>
                {*<td>{$record.trip_reason}</td>*}
            </tr>
        {/foreach}
    </tbody>
</table>

