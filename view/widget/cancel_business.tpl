<table class="table table-hover" name="user">
    <thead>
        <tr>
            <td>姓名</td>
            <td>工号</td>
            <td>开始时间</td>
            <td>结束时间</td>
            <td>部门</td>
            <td>请假原因</td>
            <td>天数</td>
            <td>状态</td>
        </tr>
    </thead>
    <tbody>
        {foreach from=$arrList item=item key=key}
            <tr>
            <td>{$item.uname}</td>
            <td>{$item.id_card}</td>
            <td>{$item.start_time|date_format:"%Y-%m-%d"}{OAConfig::$time_node[date('H:i:s',$item.start_time)]}</td>
            <td>{$item.end_time|date_format:"%Y-%m-%d"}{OAConfig::$time_node[date('H:i:s',$item.end_time)]}</td>
            <td>{$item.sName}</td>
            <td>{$item.leave_reason}</td>
            <td>{$item.day_number}</td>
            <td>{$item.audit_status}</td>
            </tr>
        {/foreach}
    </tbody>
</table>

