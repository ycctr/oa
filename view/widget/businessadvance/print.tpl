{extends file="../../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="header"}
{/block}
{block name="content"}
<div id="content" style="width:700px; margin:0 auto;">
    <style>
        body {
            padding:0;
        }
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
        }
        .form-horizontal .block tr input {
            width:80%;
        }
        .form-horizontal .control-group{
            height: 30px;
        }
        .form-horizontal .control-label{
            text-align: left;
            width: 12%;
            height: 30px;
            line-height: 30px;
            float: left;
        }
        .form-horizontal .controls{
            height: 30px;
            padding-left: 0px;
            margin-left: 0px;
            line-height: 30px;
        }
        @media print {
            .table {
                border-collapse:collapse;
            }
            .table td {
                font-size:12px;
                border:1px #000 solid;
                line-height:26px;
                padding:3px;
            }
            .navbar {
                height:40px;
                line-height:40px;
            }
            .print {
                display:none;
            }
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">出差预借款</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <div class="form-horizontal">
                        <div class="title">
                            <div style="float:left;">
                            姓名：{$user.name}
                            工号：{$user.id_card}
                            部门：{$section.name}                   
                            日期：{$smarty.now|date_format:"%Y-%m-%d"}
                            </div>
                            <button style="float:right;" class="btn btn-success print">打印</button>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="card_no">银行卡号：</label>
                            <div class="controls">
                                {$business_advance.card_no}
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="card_no">出差日期：</label>
                            <div class="controls">
                                 {$business_advance.start_day|date_format:"%Y-%m-%d"}
            {if $business_advance.start_node eq 1}上午{elseif $business_advance.start_node eq 2}中午{/if}
            到
            {$business_advance.end_day|date_format:"%Y-%m-%d"}{if $business_advance.end_node eq 3}下午{elseif $business_advance.end_node eq 2}中午{/if}
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="card_no">出差地点：</label>
                            <div class="controls">
                                {$business_advance.business_place}
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="card_no">出差目的：</label>
                            <div class="controls">
                                {$business_advance.business_purpose}
                            </div>
                        </div>
                        {if !empty($business_advance[$smarty.const.BUSINESS_ADVANCE_TYPE_RENT])}
                        <div class="block">
                            <div style="" class="navbar navbar-inner block-header">
                                <div class="muted pull-left">房租预计借款</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th width="25%">借款类别</th>
                                                <th width="25%">天数</th>
                                                <th width="25%">标准</th>
                                                <th width="25%">预估金额</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {assign var="rent_total" value=0}
                                        {foreach $business_advance[$smarty.const.BUSINESS_ADVANCE_TYPE_RENT] as $item}
                                            {assign var="rent_total" value=$rent_total+$item.amount}
                                            <tr>
                                                <td>房租</td>
                                                <td>{$item.day_no}</td>
                                                <td>{$item.standard}</td>
                                                <td>{$item.amount}</td>
                                            </tr>
                                        {/foreach}
                                            <tr>
                                                <td colspan="3">合计</td>
                                                <td>{$rent_total}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {/if}
                        {if !empty($business_advance[$smarty.const.BUSINESS_ADVANCE_TYPE_ENTERTAINMENT])}
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">宴请预计借款</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th width="25%">借款类别</th>
                                                <th width="25%">次数</th>
                                                <th width="25%">标准</th>
                                                <th width="25%">预估金额</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {assign var="zhaodai_total" value=0}
                                        {foreach $business_advance[$smarty.const.BUSINESS_ADVANCE_TYPE_ENTERTAINMENT] as $item}
                                            {assign var="zhaodai_total" value=$zhaodai_total+$item.amount}
                                            <tr>
                                                <td>宴请</td>
                                                <td>{$item.frequency}</td>
                                                <td>{$item.standard}</td>
                                                <td>{$item.amount}</td>
                                            </tr>
                                            {/foreach}
                                            <tr>
                                                <td colspan="3">合计</td>
                                                <td>{$zhaodai_total}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {/if}
                        {if !empty($business_advance[$smarty.const.BUSINESS_ADVANCE_TYPE_OTHER])}
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">其他</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th width="25%">借款类别</th>
                                                <th width="75%">预估金额</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {assign var="other_total" value=0}
                                        {foreach $business_advance[$smarty.const.BUSINESS_ADVANCE_TYPE_OTHER] as $item}
                                            {assign var="other_total" value=$other_total+$item.amount}
                                            <tr>
                                                <td>{$business_advanceType[$item.type]} </td>
                                                <td>{$item.amount}</td>
                                            </tr>
                                            {/foreach}
                                            <tr>
                                                <td colspan="1">合计</td>
                                                <td>{$other_total}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {/if}
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="1" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <td>合计</td>
                                                <td style="text-align:right;" width="10%">{$totalCnt}</td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <table style="width:100%;">
                            <tr>
                                <th style="width:30%;text-align:left;height:80px;">部门主管<br />Approval by</th>
                                <td style="width:20%;text-align:left;">{$manage.user_name}</td>
                                <th style="width:30%;text-align:left;">部门负责人<br />Vice-president's Signature</th>
                                <td style="width:20%;text-align:left;">{$vicePresident.user_name}</td>
                            </tr>
                            <tr>
                                <th style="width:30%;text-align:left;height:80px;">CEO签字<br />CEO Signature</th>
                                <td style="width:20%;text-align:left;">{$ceo.user_name}</td>
                                <th style="width:30%;text-align:left;">财务负责人签字<br />Financial signature</th>
                                <td style="width:20%;text-align:left;"></td>
                            </tr>
                            <tr>
                                <th style="width:30%;text-align:left;height:80px;">人事签字<br />Human Resource's Signature</th>
                                <td style="width:20%;text-align:left;"></td>
                                <th style="width:30%;text-align:left;">财务审核<br />Check by</th>
                                <td style="width:20%;text-align:left;"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
<script>
$(function(){
    //window.print();      
    $('.print').click(function(){
        window.print();  
    });
});
</script>
{/block}

