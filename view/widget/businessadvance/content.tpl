
<div class="ba">
    <div class="control-group">
        <label class="control-label" for="card_no">银行卡号：</label>
        <div class="controls">
            {$business_advance.card_no}
        </div>
    </div>
     <div class="control-group">
        <label class="control-label" for="card_no">出差日期：</label>
        <div class="controls">
            {$business_advance.start_day|date_format:"%Y-%m-%d"}
            {if $business_advance.start_node eq 1}上午{elseif $business_advance.start_node eq 2}中午{/if}
            到
            {$business_advance.end_day|date_format:"%Y-%m-%d"}
            {if $business_advance.end_node eq 3}下午{elseif $business_advance.end_node eq 2}中午{/if}
        </div>
    </div>
     <div class="control-group">
        <label class="control-label" for="card_no">出差地点：</label>
        <div class="controls">
            {$business_advance.business_place}
        </div>
    </div>
     <div class="control-group">
        <label class="control-label" for="card_no">出差目的：</label>
        <div class="controls">
            {$business_advance.business_purpose}
        </div>
    </div>
</div>

{if !empty($business_advance[$smarty.const.BUSINESS_ADVANCE_TYPE_RENT])}
<div class="block ba">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">房租预计借款</div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="table">
                <thead>
                    <tr>
                        <th width="20%">借款类别</th>
                        <th width="20%">天数</th>
                        <th width="40%">标准</th>
                        <th width="20%">预估金额</th>
                    </tr>
                </thead>
                <tbody>
                {assign var="rent_total" value=0}
                {foreach $business_advance[$smarty.const.BUSINESS_ADVANCE_TYPE_RENT] as $item}
                    {assign var="rent_total" value=$rent_total+$item.amount}
                    <tr>
                        <td>房租</td>
                        <td>{$item.day_no}</td>
                        <td>{$item.standard}</td>
                        <td>{$item.amount}</td>
                    </tr>
                {/foreach}
                    <tr>
                        <td colspan="3">合计</td>
                        <td>{$rent_total}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
{/if}
{if !empty($business_advance[$smarty.const.BUSINESS_ADVANCE_TYPE_ENTERTAINMENT])}
<div class="block ba">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">宴请预计借款</div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="table">
                <thead>
                    <tr>
                        <th width="20%">借款类别</th>
                        <th width="20%">次数</th>
                        <th width="40%">标准</th>
                        <th width="20%">预估金额</th>
                    </tr>
                </thead>
                <tbody>
                {assign var="zhaodai_total" value=0}
                {foreach $business_advance[$smarty.const.BUSINESS_ADVANCE_TYPE_ENTERTAINMENT] as $item}
                    {assign var="zhaodai_total" value=$zhaodai_total+$item.amount}
                    <tr>
                        <td>宴请</td>
                        <td>{$item.day_no}</td>
                        <td>{$item.standard}</td>
                        <td>{$item.amount}</td>
                    </tr>
                    {/foreach}
                    <tr>
                        <td colspan="3">合计</td>
                        <td>{$zhaodai_total}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
{/if}
{if !empty($business_advance[$smarty.const.BUSINESS_ADVANCE_TYPE_OTHER])}
<div class="block ba">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">其他</div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="table">
                <thead>
                    <tr>
                        <th width="80%">借款类别</th>
                        <th width="20%">预估金额</th>
                    </tr>
                </thead>
                <tbody>
                {assign var="other_total" value=0}
                {foreach $business_advance[$smarty.const.BUSINESS_ADVANCE_TYPE_OTHER] as $item}
                    {assign var="other_total" value=$other_total+$item.amount}
                    <tr>
                        <td>{$business_advanceType[$item.type]} </td>
                        <td>{$item.amount}</td>
                    </tr>
                    {/foreach}
                    <tr>
                        <td colspan="1">合计</td>
                        <td>{$other_total}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
{/if}
