
<div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
    <ul>
        <li><a>总计：&nbsp;{$data.page.recordCount}&nbsp;人</a></li>
        {pager_oa count=$data.page.recordCount pagesize=$data.page.pagesize page=$data.page.pn pagelink="manage?pn=%d&selectNum=$selectNum&Condition=$Condition" list=3}
    </ul>
</div>

<table class="table table-hover" name="user">
    <thead>
        <tr>
            <td>id</td><td>姓名</td><td>工号</td><td>在职状态</td><td>邮箱</td><td>电话</td><td>性别</td><td>部门</td>
            <td>行政级别</td><td>修改时间</td><td>操作</td> 
        </tr>
    </thead>
    <tbody>
        {foreach from=$data.info item=item key=key}
            {if (is_null($item.id))}
                {continue}
            {/if}
            <tr>
            
            <td>{$item.id}<input type="hidden" name="id" value="{$item.id}"></td>
            <td>{$item.name}<input type="hidden" name="name" value="{$item.name}"></td>
            <td>{$item.id_card}<input type="hidden" name="id_card" value="{$item.id_card}"></td>

            {if $item.is_leave eq 2 }
                {assign var="str_leave" value="离职"}
            {else}
                {assign var="str_leave" value="在职"}
            {/if}
            
            <td>{$str_leave}<input type="hidden" name="id_card" value="{$item.is_leave}"></td>
            <td>{$item.email}<input type="hidden" name="email" value="{$item.email}"></td>
            <td>{$item.mobile}<input type="hidden" name="mobile" value="{$item.mobile}"></td>
            <td>{if ($item.sex==1)}
                男<input type="hidden" name="sex" value="{$item.sex}">
            {else}
                女<input type="hidden" name="sex" value="{$item.sex}">
            {/if}</td>
            <td>{$item.section_name}<input type="hidden" name="section_id" value="{$item.section_id}"></td>
            <td>
                {foreach $levels as $k=>$v}
                {if $item.level eq $k}{$v}{/if}
                {/foreach}
            </td>
            <td>{$item.modify_time|date_format:"%Y-%m-%d"}<input type="hidden" name="modify_time" value="{$item.modify_time}"></td>
            <td>
                {if $view_p eq 1}
                    <a href="view?id={$item.id}">查看</a>
                {/if}
                {if $edit_p eq 1}
                    | <a href="edit?id={$item.id}">编辑</a>
                {/if}
                {if $del_p eq 1}
                    | <a class="del" href="delete?id={$item.id}">删除</a>
                {/if}
            </td>
            </tr>
            {/foreach}
        </tbody>
</table>

<div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
    <ul>
        <li><a>总计：&nbsp;{$data.page.recordCount}&nbsp;人</a></li>
        {pager_oa count=$data.page.recordCount pagesize=$data.page.pagesize page=$data.page.pn pagelink="manage?pn=%d&selectNum=$selectNum&Condition=$Condition" list=3}
    </ul>
</div>

