<div class="span3" id="sidebar">
    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
        <li {if $Yii->controller->id == 'userworkflow' && $Yii->controller->action->id == 'list'}class="active"{/if}>
            <a href="/userworkflow/list">发起新的流程</a>
        </li>
        <li {if $Yii->controller->id == 'userworkflow' && $Yii->controller->action->id == 'my'}class="active"{/if}>
            <a href="/userworkflow/my">我的流程</a>
        </li>
        <li {if $Yii->controller->id == 'usertask' && $Yii->controller->action->id == 'list'}class="active"{/if}>
            <a href="/usertask/list">{if $Yii->params['userWorkflowCnt']}<span class="badge badge-important pull-right">{$Yii->params['userWorkflowCnt']}</span>{/if}需要我审批</a>
        </li>
        <li {if $Yii->controller->id == 'checkinout' && $Yii->controller->action->id == 'list'}class="active"{/if}>
            <a href="/checkinout/list">{if $Yii->params['checkCnt']}<span class="badge badge-important pull-right">异常情况&nbsp;{$Yii->params['checkCnt']}</span>{/if}打卡记录</a>
        </li>
         
		
        {if $Yii->params['nianjia']}
        <li {if $Yii->controller->id == 'vacationrecord' && $Yii->controller->action->id == 'list'}class="active"{/if}>
            <a href="/vacationrecord/list">年假、病假记录</a>
        </li>
        {/if}

        <!--
        {if $Yii->params['section_flag']}
            <li {if $Yii->controller->id == 'teambuildingrecord' && $Yii->controller->action->id == 'allamount'}class="active"{/if}>
                <a href="/teambuildingrecord/allamount">部门团建经费</a>
            </li>
        {/if}

        {if $Yii->params['encourage_flag']}
             <li {if $Yii->controller->id == 'teamencouragerecord' && $Yii->controller->action->id == 'allamount'}class="active"{/if}> 
                 <a href="/teamencouragerecord/allamount">部门激励经费</a> 
             </li> 
         {/if} 

        {if $Yii->params['is_senior']}
            <li {if $Yii->controller->id == 'teambuildingrecord' && $Yii->controller->action->id == 'senior'}class="active"{/if}>
                <a href="/teambuildingrecord/senior">团建经费分配</a>
            </li>
        {/if}
        -->

        {if $Yii->params['has_team']}
            <li {if $Yii->controller->id == 'myteam' && $Yii->controller->action->id == 'list'}class="active"{/if}>
                <a href="/myteam/list">我的团队</a>
            </li>
        {/if}
    </ul>


    <!--
    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">

        <li {if $Yii->controller->id == 'home' && $Yii->controller->action->id == 'index'}class="active"{/if} >
            <a href="/home/index">我要订晚餐</a>
        </li>

    </ul>
    -->

    {if !empty($Yii->params['header_list']) && (in_array('hr',$Yii->params['header_list']))}
    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">

        <li {if $Yii->controller->id == 'hr' && $Yii->controller->action->id == 'listentry'}class="active"{/if}>
            <a href="/hr/listEntry?status=0">入职管理</a>
        </li>
        <li {if $Yii->controller->id == 'hr' && $Yii->controller->action->id == 'listdimission'}class="active"{/if}>
            <a href="/hr/listDimission?status=0">离职管理</a>
        </li>
        {*<li {if $Yii->controller->id == 'hr' && $Yii->controller->action->id == 'listEntry'}class="active"{/if}>*}
            {*<a href="/hr/listEntry">转正管理</a>*}
        {*</li>*}
        {*<li {if $Yii->controller->id == 'hr' && $Yii->controller->action->id == 'listEntry'}class="active"{/if}>*}
            {*<a href="/hr/listEntry">异动管理</a>*}
        {*</li>*}

    </ul>
    {/if}
    {if !empty($Yii->params['header_list']) && (in_array('ticket',$Yii->params['header_list']))}
        <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">

            <li {if $Yii->controller->id == 'ticket' && $Yii->controller->action->id == 'list'}class="active"{/if}>
                <a href="/ticket/list">票务管理</a>
            </li>

        </ul>
    {/if}

    {if !empty($Yii->params['header_list']) && (in_array('officesupply',$Yii->params['header_list']))}
        <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
            <li {if $Yii->controller->id == 'officesupply' && $Yii->controller->action->id == 'list'}class="active"{/if}>
                <a href="/officeSupply/list">办公用品管理</a>
            </li>
        </ul>
    {/if}

    {if !empty($Yii->params['header_list']) && (in_array('lunchchange',$Yii->params['header_list']))}
        <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
            <li {if $Yii->controller->id == 'lunchchange' && $Yii->controller->action->id == 'list'}class="active"{/if}>
                <a href="/lunchChange/list">午餐变更管理</a>
            </li>
        </ul>
    {/if}

    {if !empty($Yii->params['header_list']) && (in_array('userworkflow/manageloan',$Yii->params['header_list']))}
        <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
            <li {if $Yii->controller->id == 'userworkflow' && $Yii->controller->action->id == 'manageLoan'}class="active"{/if}>
                <a href="/userworkflow/manageLoan">借款管理</a>
            </li>
        </ul>
    {/if}

    {if !empty($Yii->params['header_list']) && (in_array('user',$Yii->params['header_list']) || in_array('section',$Yii->params['header_list']) || in_array('home',$Yii->params['header_list']) ) ||in_array('moneyright',$Yii->params['header_list'])}
    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
        {if in_array('user/manage',$Yii->params['header_list'])}
        <li {if $Yii->controller->id == 'user' && $Yii->controller->action->id == 'manage'}class="active"{/if}>
           <a href="/user/manage">用户管理</a>
        </li>
        {/if}
        {if in_array('section/manage',$Yii->params['header_list'])}
        <li {if $Yii->controller->id == 'section' && $Yii->controller->action->id == 'manage'}class="active"{/if}>
            <a href="/section/manage">部门管理</a>
        </li>
        {/if}

        {if in_array('moneyright/managemoney',$Yii->params['header_list'])}
        <li {if $Yii->controller->id == 'moneyright' && $Yii->controller->action->id == 'managemoney'}class="active"{/if}>
            <a href="/moneyright/manageMoney">部门费用审批权限管理</a>
        </li>
        {/if}

        {if in_array('home/emsmanager',$Yii->params['header_list'])}
        <li {if $Yii->controller->id == 'home' && $Yii->controller->action->id == 'emsmanager'}class="active"{/if}>
            <a href="/home/EMSmanager">快递管理</a>
        </li>
        {/if}

        {if in_array('userworkflow/superadmin',$Yii->params['header_list'])}
        <li {if $Yii->controller->id == 'userworkflow' && $Yii->controller->action->id == 'superadmin'} class="active"{/if}>
            <a href="/userworkflow/superadmin">审批流程总览</a>
        </li>
        {/if}
    </ul>
    {/if}
    {if !empty($Yii->params['header_list']) && (in_array('attendence',$Yii->params['header_list']))}
    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
        {if in_array('attendence/list',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'attendence' && $Yii->controller->action->id == 'list'}class="active"{/if}>
           <a href="/attendence/list">请假导出</a>
        </li>
        {/if}

        {if in_array('export/cancelabsence',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'export' && $Yii->controller->action->id == 'cancelabsence'}class="active"{/if}>
           <a href="/export/cancelabsence">销假导出</a>
        </li>
        {/if}

        {if in_array('business/list',$Yii->params['header_list'])} <li {if $Yii->controller->id == 'business' && $Yii->controller->action->id == 'list'}class="active"{/if}>
           <a href="/business/list">出差导出</a>
        </li>
        {/if}

        {if in_array('business/cancel',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'business' && $Yii->controller->action->id == 'cancel'}class="active"{/if}>
           <a href="/business/cancel">销出差导出</a>
        </li>
        {/if}
        {if in_array('businessout/list',$Yii->params['header_list'])}
            <li {if $Yii->controller->id == 'businessout' && $Yii->controller->action->id == 'list'}class="active"{/if}>
                <a href="/businessout/list">因公外出导出</a>
            </li>
        {/if}
    </ul>
    {/if}

    {if !empty($Yii->params['header_list']) && (in_array('vacationrecord',$Yii->params['header_list']))}
    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
        {if in_array('vacationrecord/finishing',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'vacationrecord' && $Yii->controller->action->id == 'finishing'}class="active"{/if}>
           <a href="/vacationrecord/finishing">修正年假、病假</a>
        </li>
        {/if}

        {if in_array('vacationrecord/superlist',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'vacationrecord' && $Yii->controller->action->id == 'superlist'}class="active"{/if}>
           <a href="/vacationrecord/superlist">年、病假流水导出</a>
        </li>
        {/if}
    </ul>
    {/if}
    {if !empty($Yii->params['header_list']) && (in_array('teambuildingrecord',$Yii->params['header_list']))}
    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
        {if in_array('teambuildingrecord/finishing',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'teambuildingrecord' && $Yii->controller->action->id == 'finishing'}class="active"{/if}>
           <a href="/teambuildingrecord/finishing">修正团建，激励经费</a>
        </li>
        {/if}
        {if in_array('teambuildingrecord/overview',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'teambuildingrecord' && $Yii->controller->action->id == 'overview'}class="active"{/if}>
           <a href="/teambuildingrecord/overview">团建，激励经费余额</a>
        </li>
        {/if}
        {if in_array('export/teambuilding',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'export' && $Yii->controller->action->id == 'teambuilding'}class="active"{/if}>
           <a href="/export/teambuilding">导出团建，激励报销流水</a>
        </li>
        {/if}
    </ul>
    {/if}
    {if !empty($Yii->params['header_list']) && (in_array('entrustaudit',$Yii->params['header_list']))}
    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
        {if in_array('entrustaudit/list',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'entrustaudit' && $Yii->controller->action->id == 'list'}class="active"{/if}>
           <a href="/entrustaudit/list">委托审批</a>
        </li>
        {/if}
    </ul>
    {/if}
    {if !empty($Yii->params['header_list']) && (in_array('export',$Yii->params['header_list']))}
    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
        {if in_array('export/completedailyreimbursement',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'export' && $Yii->controller->action->id == 'completedailyreimbursement'}class="active"{/if}>
           <a href="/export/completedailyreimbursement">日常报销导出</a>
        </li>
        {/if}
        {if in_array('export/businessreimbursement',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'export' && $Yii->controller->action->id == 'businessreimbursement'}class="active"{/if}>
           <a href="/export/businessreimbursement">出差报销导出</a>
        </li>
        {/if}
        {if in_array('export/checkinoutlist',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'export' && $Yii->controller->action->id == 'checkinoutlist'}class="active"{/if}>
           <a href="/export/checkinoutlist">打卡记录导出</a>
        </li>
        {/if}
        {if in_array('export/statistics',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'export' && $Yii->controller->action->id == 'statistics'}class="active"{/if}>
           <a href="/export/statistics">考勤统计导出</a>
        </li>
        {/if}
        {if in_array('export/user',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'export' && $Yii->controller->action->id == 'user'}class="active"{/if}>
           <a href="/export/user">员工信息导出</a>
        </li>
        {/if}
        {if in_array('export/vacation',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'export' && $Yii->controller->action->id == 'vacation'}class="active"{/if}>
           <a href="/export/vacation">剩余年假病假导出</a>
        </li>
        {/if}
        {if in_array('export/expense',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'export' && $Yii->controller->action->id == 'expense'}class="active"{/if}>
           <a href="/export/expense">行政费用付款导出</a>
        </li>
        {/if}
        {if in_array('export/apply',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'export' && $Yii->controller->action->id == 'apply'}class="active"{/if}>
           <a href="/export/apply">物品申领导出</a>
        </li>
        {/if}
        {if in_array('export/card',$Yii->params['header_list'])}
         <li {if $Yii->controller->id == 'export' && $Yii->controller->action->id == 'card'}class="active"{/if}>
           <a href="/export/card">名片制作导出</a>
        </li>
        {/if}
    </ul>
    {/if}
    {if !empty($Yii->params['header_list']) && (in_array('role',$Yii->params['header_list'])) }
    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
        {if in_array('role/list',$Yii->params['header_list'])}
        <li {if $Yii->controller->id == 'role' && $Yii->controller->action->id == 'list'}class="active"{/if}>
            <a href="/role/list">角色管理</a>
        </li>
        {/if}
    {/if}
</div>

