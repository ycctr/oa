{if $current_audit_status == $smarty.const.OA_TASK_YISHENPI}
<table class="table table-hover">
    <thead>
        <tr>
            <th>流程状态</th>
            <th>标题</th>
            <th>申请人</th>
            <th>提交日期</th>
            <th>最后审批人</th>
            <th style="width: 15%">操作</th>
        </tr>
    </thead>
    <tbody>
        {foreach $list as $item}
        <tr>
            <td>
            {if $item.uwf_audit_status == $smarty.const.OA_WORKFLOW_AUDIT_SHENPIZHONG}
                {if !empty($item.deploy_desc)}
                    <span class="label label-warning">跳过审批</span>
                {else}
                    <span class="label label-info">{$uwf_audit_status[$item.uwf_audit_status]}</span>
                {/if}
            {elseif $item.uwf_audit_status == $smarty.const.OA_WORKFLOW_AUDIT_PASS}
                {if !empty($item.deploy_desc)}
                    <span class="label label-warning">跳过审批</span>
                {else}
                    <span class="label label-success">{$uwf_audit_status[$item.uwf_audit_status]}</span>
                {/if}
            {elseif $item.uwf_audit_status == $smarty.const.OA_WORKFLOW_AUDIT_REJECT}
                <span class="label label-important">{$uwf_audit_status[$item.uwf_audit_status]}</span>
            {else}
                <span class="label label-warning">{$uwf_audit_status[$item.uwf_audit_status]}</span>
            {/if} 
            </td>
            <td>{$item.show_title}</td>
            <td>{$item.obj_id.user_name}</td>
            <td>{$item.uwf_create_time|date_format:"%Y-%m-%d %H:%M"}</td>
            <td>{$item.audit_user_name}</td>
            <td>
                <a href="/userworkflow/look?id={$item.user_workflow_id}">详情</a>
            </td>
        </tr>
        {/foreach}
    </tbody>
</table>
{else}
<table class="table table-hover">
    <thead>
        <tr>
            <th>标题</th>
            <th>申请人</th>
            <th>提交日期</th>
            <th style="width: 15%">{if $shenpi}处理结果{else}操作{/if}</th>
        </tr>
    </thead>
    <tbody>
        {foreach $list as $item}
        <tr>
            <td>{$item.show_title}</td>
            <td>{$item.obj_id.user_name}</td>
            <td>{$item.obj_id.create_time|date_format:"%Y-%m-%d %H:%M"}</td>
            <td>
                {if $item.audit_status == $smarty.const.OA_TASK_CHUANGJIAN}
				{if $item.obj_type == "workorder" && $item.workflow_step == "A"}
                <a href="/usertask/workorder?user_task_id={$item.id}&obj_id={$item.obj_id.id}&user_workflow_id={$item.user_workflow_id}">任务分配</a>
				{elseif $item.obj_type == "workorder" && $item.workflow_step == "B"}
                <a href="/usertask/detail?user_task_id={$item.id}">处理任务</a>
				{elseif $item.obj_type == "papp_vpn" && $item.workflow_step == "B"}
                <a href="/usertask/detail?user_task_id={$item.id}">处理任务</a>
                {elseif $item.obj_type == "papp_db" && $item.workflow_step == "B"}
                <a href="/usertask/detail?user_task_id={$item.id}">处理任务</a>
                {elseif $item.obj_type == "entry" && $item.audit_desc == "interviewer"}
                <a href="/usertask/detail?user_task_id={$item.id}">面试评估</a>
                {elseif $item.obj_type == "entry" && $item.audit_desc == "hr"}
                <a href="/usertask/detail?user_task_id={$item.id}">人力评估</a>
                {elseif $item.obj_type == "dimission" && ($item.workflow_step == "J" || $item.workflow_step == "K")}
                <a href="/usertask/detail?user_task_id={$item.id}">详情</a>
                {elseif $item.obj_type == "dimission" && $item.workflow_step != "A"}
                <a href="/usertask/detail?user_task_id={$item.id}">交接</a>
                {elseif $item.role_name == "票务专员" && strpos($item.obj_type, 'ticket_book') === 0}
                    <a href="/usertask/detail?user_task_id={$item.id}">订票</a>
                {elseif $item.role_name == "票务专员" && strpos($item.obj_type, 'ticket_refund') === 0}
                    <a href="/usertask/detail?user_task_id={$item.id}">退票</a>
                {elseif $item.role_name == "票务专员" && strpos($item.obj_type, 'ticket_endorse') === 0}
                    <a href="/usertask/detail?user_task_id={$item.id}">改签</a>
				{else}
                {if !($item.workflow_step=="B"&&$item.role_id==22)}
                <a class="pass" data-value="{$item.id}" href="javascript:void(0);">通过</a>
                {/if}
                <a class="reject" data-value="{$item.id}" href="javascript:void(0);">拒绝</a>
                <a href="/usertask/detail?user_task_id={$item.id}">详情</a>
				{/if}
                {elseif $item.audit_status == $smarty.const.OA_TASK_TONGGUO}
                已通过
                {elseif $item.audit_status == $smarty.const.OA_TASK_JUJUE}
                已拒绝
                {/if}
            </td>
        </tr>
        {/foreach}
    </tbody>
</table>
{/if}
<div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
    <ul>
        {pager_oa count=$userTaskCnt pagesize=$rn page=$pn pagelink=$url|cat:"pn=%d" list=3}
    </ul>
</div>
