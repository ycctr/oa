<table class="table table-hover">
    <thead>
        <tr>
            <th>状态</th>
            <th>类型</th>
            <th>标题</th>
            <th>申请人</th>
            <th>审批人</th>
            <th>提交日期</th>
        </tr>
    </thead>
    <tbody>
        {foreach $userWorkflows as $item}
        <tr>
            <td>审核中</td>
            <td>{$obj_type_arr[$item.obj_type]}</td>
            <td>{$item.title}</td>
            <td>{$item.user_name}</td>
            <td>{$item.check_name}</td>
            <td>{$item.create_time|date_format:"%Y-%m-%d %H:%M"}</td>
        </tr>
        {/foreach}
    </tbody>
</table>
<div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
    <ul>

        {pager_oa count=$page_count  pagesize=$per  page=$cur_page pagelink="superadmin?pn=%d&check_flag=1&obj_type=$obj_type&name=$name&check_name=$check_name"  list=3}
    </ul>
</div>

