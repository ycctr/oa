<table class="table table-hover" name="user">
    <thead>
        <tr>
            <td width="30px">选择</td><td width="100px">模块功能</td><td>操作功能</td>
        </tr>
    </thead>
    <tbody>
        {foreach item=item from=$arrList}
            <tr>
                <td><input type="checkbox" class="select_all"></td>
                <td>{if $item.id}<input type="checkbox" value="{$item.id}" class="module_ck" {if intval($item.selected) eq 1}checked{/if} name="right[{$item.id}]"/>{$item.title}{/if}</td>
                <td>
                    {foreach item=items from=$item.op}
                        <input type="checkbox" value="{$items.id}" class="op_ck" {if intval($items.selected) eq 1}checked{/if} name="right[{$items.id}]"/>{$items.title}
                    {/foreach}
                </td>
            </tr>
        {/foreach}
    </tbody>
</table>

