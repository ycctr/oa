{extends file="../../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="header"}
{/block}
{block name="content"}
<div id="content" style="width:700px; margin:0 auto;">
    <style>
        body {
            padding:0;
        }
        .row-fluid .span12 {
            width:99%;
        }
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
        }
        .form-horizontal .block {
            border:0px;
        }
        .table td {
            border-left:1px #ddd solid;
        }
        .table .td_r {
            border-right:1px #ddd solid;
        }
        .table .td_b {
            border-bottom:1px #ddd solid;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
        }
        .form-horizontal .block tr input {
            width:80%;
        }
        @media print {
            .table {
                border-collapse:collapse;
            }
            .form-horizontal .table td {
                font-size:12px;
                border:1px #000 solid;
                line-height:26px;
                padding:3px;
            }
            .navbar {
                height:40px;
                line-height:40px;
            }
            .print {
                display:none;
            }
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">员工入职</div>
            </div>
            <div class="block-content collapse in">
                <button style="float:right;" class="btn btn-success print">打印</button>
                <div class="span12">
                    <div class="form-horizontal">
                        <div class="block">
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td class="td_b" rowspan="17" valign="middle" align="center" width="20%">员工入职表</td>
                                                <td width="30%">姓名</td>
                                                <td class="td_r">{$dimissionUser.name}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">员工编号</td>
                                                <td class="td_r">{$dimissionUser.id_card}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">所在部门</td>
                                                <td class="td_r">{$dimissionUser.department}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">职位名称</td>
                                                <td class="td_r">{$userWorkflow.ext.position}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">入职日期</td>
                                                <td class="td_r">{$dimissionUser.hiredate|date_format:"%Y-%m-%d"}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">直接上级</td>
                                                <td class="td_r">{$dimissionUser.dimissionUserManager.name}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">申请离职日期</td>
                                                <td class="td_r">{$userWorkflow.ext.create_time|date_format:"%Y-%m-%d"}</td>
                                            </tr>
                                            <tr>
                                                <td class="td_b" width="30%">辞职原因</td>
                                                <td class="td_r td_b">{$userWorkflow.ext.leave_reason}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="span12">
                            <table style="width:100%;">
                                {if $userTasks}
                                {foreach $userTasks as $userTask}
                                    <div class="control-group">
                                        <label class="control-label">{if $userTask.user_id}{$userTask.user_name}{else}{$workflowConfig[$userTask.workflow_step].rolename}{/if}审批</label>
                                        <div class="controls">
                                            {if $userTask.audit_status == $smarty.const.OA_TASK_TONGGUO}
                                                通过
                                            {elseif $userTask.audit_status == $smarty.const.OA_TASK_JUJUE}
                                                拒绝
                                            {elseif $userTask.audit_status == $smarty.const.OA_TASK_CHUANGJIAN}
                                                未审批
                                            {/if}
                                            {if $userTask.audit_status != $smarty.const.OA_TASK_CHUANGJIAN}
                                                <span style="margin-left:30px;">时间：{$userTask.modify_time|date_format:"%Y-%m-%d %H:%M:%S"}</span>
                                            {/if}
                                        </div>
                                    </div>
                                    {if $userTask.audit_desc}
                                        <div class="control-group">
                                            <label class="control-label">备注</label>
                                            <div class="controls">
                                                {$userTask.audit_desc}
                                            </div>
                                        </div>
                                    {/if}
                                {/foreach}
                                {/if}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
<script>
$(function(){
    //window.print();      
    $('.print').click(function(){
        window.print();  
    });
});
</script>
{/block}

