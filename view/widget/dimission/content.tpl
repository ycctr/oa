
<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">员工离职</div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="table">
                <tbody>
                    <tr>
                        <td width="30%">姓名</td>
                        <td>{$dimissionUser.name}</td>
                    </tr>
                    <tr>
                        <td width="30%">员工编号</td>
                        <td>{$dimissionUser.id_card}</td>
                    </tr>
                    <tr>
                        <td width="30%">所在部门</td>
                        <td>{$dimissionUser.department}</td>
                    </tr>
                    <tr>
                        <td width="30%">职位名称</td>
                        <td>{$userWorkflow.ext.position}</td>
                    </tr>
                    <tr>
                        <td width="30%">入职日期</td>
                        <td>{$dimissionUser.hiredate|date_format:"%Y-%m-%d"}</td>
                    </tr>
                    <tr>
                        <td width="30%">直接上级</td>
                        <td>{$dimissionUser.dimissionUserManager.name}</td>
                    </tr>
                    <tr>
                        <td width="30%">申请离职日期</td>
                        <td>{$userWorkflow.ext.create_time|date_format:"%Y-%m-%d"}</td>
                    </tr>
                    <tr>
                        <td width="30%">辞职原因</td>
                        <td>{$userWorkflow.ext.leave_reason}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
