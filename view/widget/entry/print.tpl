{extends file="../../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="header"}
{/block}
{block name="content"}
<div id="content" style="width:700px; margin:0 auto;">
    <style>
        body {
            padding:0;
        }
        .row-fluid .span12 {
            width:99%;
        }
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
        }
        .form-horizontal .block {
            border:0px;
        }
        .table td {
            border-left:1px #ddd solid;
        }
        .table .td_r {
            border-right:1px #ddd solid;
        }
        .table .td_b {
            border-bottom:1px #ddd solid;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
        }
        .form-horizontal .block tr input {
            width:80%;
        }
        @media print {
            .table {
                border-collapse:collapse;
            }
            .form-horizontal .table td {
                font-size:12px;
                border:1px #000 solid;
                line-height:26px;
                padding:3px;
            }
            .navbar {
                height:40px;
                line-height:40px;
            }
            .print {
                display:none;
            }
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">员工入职</div>
            </div>
            <div class="block-content collapse in">
                <button style="float:right;" class="btn btn-success print">打印</button>
                <div class="span12">
                    <div class="form-horizontal">
                        <div class="block">
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td class="td_b" rowspan="17" valign="middle" align="center" width="20%">员工入职表</td>
                                                <td width="30%">姓名</td>
                                                <td class="td_r">{$userWorkflow.ext.name}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">联系电话</td>
                                                <td class="td_r">{$userWorkflow.ext.mobile}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">面试日期</td>
                                                <td class="td_r">{$userWorkflow.ext.interview_time|date_format:"%Y-%m-%d"}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">预计报到日期</td>
                                                <td class="td_r">{$userWorkflow.ext.approx_arrive_time|date_format:"%Y-%m-%d"}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">工作地</td>
                                                <td class="td_r">{$userWorkflow.ext.workplace}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">雇佣类型</td>
                                                <td class="td_r">{$hireTypes[$userWorkflow.ext.hire_type]}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">业务线</td>
                                                <td class="td_r">{$businessLine}</td>
                                            </tr>
                                            {if $section != 'Estaff'}
                                                <tr>
                                                    <td width="30%">部门</td>
                                                    <td class="td_r">{$section1}</td>
                                                </tr>
                                            {/if}
                                            {if $subSection != 'Estaff'}
                                                <tr>
                                                    <td width="30%">分部</td>
                                                    <td class="td_r">{$subSection}</td>
                                                </tr>
                                            {/if}
                                            <tr>
                                                <td width="30%">职位</td>
                                                <td class="td_r">{$userWorkflow.ext.position}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">直接上级</td>
                                                <td class="td_r">{$directManager}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">职级</td>
                                                <td class="td_r">
                                                    {if $userWorkflow.ext.level == $smarty.const.AUDIT_CEO}CEO{/if}
                                                    {if $userWorkflow.ext.level == $smarty.const.AUDIT_VP}VP{/if}
                                                    {if $userWorkflow.ext.level == $smarty.const.AUDIT_MANAGER}部门负责人{/if}
                                                    {if $userWorkflow.ext.level == $smarty.const.AUDIT_VICE_MANAGER}分部门负责人{/if}
                                                    {if $userWorkflow.ext.level == 0}无{/if}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="30%">薪资</td>
                                                <td class="td_r">{$userWorkflow.ext.salary}</td>
                                            </tr>
                                            <tr>
                                                <td width="30%">期权</td>
                                                <td class="td_r">{$userWorkflow.ext.stock_option}</td>
                                            </tr>
                                            <tr>
                                                <td class="td_b" width="30%">其他</td>
                                                <td class="td_r td_b">{$userWorkflow.ext.remark}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="span12">
                            <table style="width:100%;">
                                {foreach $userTasks as $item}
                                    <tr>
                                        <th style="width:30%;text-align:center;height:50px;">{$business_role[$item.audit_desc]}</th>
                                        <td style="text-align:left;">
                                            {$item.user_name}
                                        </td>
                                        <td style="text-align:left;">
                                            {$item.modify_time|date_format:"%Y-%m-%d"}
                                        </td>
                                    </tr>
                                {/foreach}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
<script>
$(function(){
    //window.print();      
    $('.print').click(function(){
        window.print();  
    });
});
</script>
{/block}

