
<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">员工入职明细</div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="table">
                <tbody>
                    <tr>
                        <td width="30%">姓名</td>
                        {*<td>{$userWorkflow.create_time|date_format:"%Y-%m-%d"}</td>*}
                        <td>{$userWorkflow.ext.name}</td>
                    </tr>
                    <tr>
                        <td width="30%">联系电话</td>
                        <td>{$userWorkflow.ext.mobile}</td>
                    </tr>
                    <tr>
                        <td width="30%">面试日期</td>
                        <td>{$userWorkflow.ext.interview_time|date_format:"%Y-%m-%d"}</td>
                    </tr>
                    <tr>
                        <td width="30%">预计报到日期</td>
                        <td>{$userWorkflow.ext.approx_arrive_time|date_format:"%Y-%m-%d"}</td>
                    </tr>
                    <tr>
                        <td width="30%">工作地</td>
                        <td>{$userWorkflow.ext.workplace}</td>
                    </tr>
                    <tr>
                        <td width="30%">雇佣类型</td>
                        <td>{$hireTypes[$userWorkflow.ext.hire_type]}</td>
                    </tr>
                    <tr>
                        <td width="30%">业务线</td>
                        <td>{$businessLine}</td>
                    </tr>
                    {if $section != 'Estaff'}
                        <tr>
                            <td width="30%">部门</td>
                            <td>{$section1}</td>
                        </tr>
                    {/if}
                    {if $subSection != 'Estaff'}
                        <tr>
                            <td width="30%">分部</td>
                            <td>{$subSection}</td>
                        </tr>
                    {/if}
                    <tr>
                        <td width="30%">职位</td>
                        <td>{$userWorkflow.ext.position}</td>
                    </tr>
                    <tr>
                        <td width="30%">直接上级</td>
                        <td>{$directManager}</td>
                    </tr>
                    <tr>
                        <td width="30%">职级</td>
                        <td>
                            {if $userWorkflow.ext.level == $smarty.const.AUDIT_CEO}CEO{/if}
                            {if $userWorkflow.ext.level == $smarty.const.AUDIT_VP}VP{/if}
                            {if $userWorkflow.ext.level == $smarty.const.AUDIT_MANAGER}部门负责人{/if}
                            {if $userWorkflow.ext.level == $smarty.const.AUDIT_VICE_MANAGER}分部门负责人{/if}
                            {if $userWorkflow.ext.level == 0}无{/if}
                        </td>
                    </tr>
                    {*<tr>*}
                        {*<td width="30%">薪资</td>*}
                        {*<td>{$userWorkflow.ext.salary}</td>*}
                    {*</tr>*}
                    {*<tr>*}
                        {*<td width="30%">期权</td>*}
                        {*<td>{$userWorkflow.ext.stock_option}</td>*}
                    {*</tr>*}
                    <tr>
                        <td width="30%">其他</td>
                        <td>{$userWorkflow.ext.remark}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
