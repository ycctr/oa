<style type="text/css">
.controls{
    padding-top: 5px;
}
.img{
    border: 1px solid blue;
    max-width: 350px;
}
</style>
<div class="control-group">
    <label class="control-label" for="select01">合同类型</label>
    <div class="controls">
        {$extension.con_type}
    </div>
</div>
<div class="control-group">
    <label class="control-label">付款事项简介</label>
    <div class="controls">
        {$extension.summary}
    </div>
</div>
<div class="control-group">
    <label class="control-label">供应商名称</label>
    <div class="controls">
        {$extension.supplier}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">{if $extension.loan_id}报销金额{else}付款金额{/if}</label>
    <div class="controls">
        {$extension.amount}元
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">付款方式</label>
    <div class="controls">
        {$extension.payment_method}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">付款性质</label>
    <div class="controls">
        {$payment_natures[$extension.payment_nature]}
    </div>
</div>
{if !$extension.loan_id}
<div class="control-group">
    <label class="control-label" for="date01">付款类别</label>
    <div class="controls">
        {$pay_types[$extension.pay_type]}
    </div>
</div>
{/if}
<div class="control-group">
    <label class="control-label" for="date01">开户银行</label>
    <div class="controls">
        {$extension.bank_account}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">账户名称</label>
    <div class="controls">
        {$extension.account_name}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">账号号码</label>
    <div class="controls">
        {$extension.account_number}
    </div>
</div>
{if !empty($extension.img_address)}
<div class="control-group">
    <label class="control-label" for="date01">票据照片</label>
    <div class="controls">
        {foreach $extension.img_address as $src}
        <a href="{$src}" target="_blank"><img class="img" style="max-height:200px;margin-right:20px;" src="{$src}" /></a>
        {/foreach}
    </div>
</div>
{/if}
<div class="control-group">
    <label class="control-label" for="date01">备注信息</label>
    <div class="controls">
        {$extension.remark}
    </div>
</div>