<style type="text/css">
td{
    word-wrap:break-word;
    word-break:break-all;
}
</style>
<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">上线单明细</div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="table">
                <tbody>
                    {if $userWorkflow.ext.attachment_address}
                    <tr>
                        <td width="30%">附件</td>
                        <td>
                            {foreach $filenames as $key => $filename}
                                <a href="/update/down/?file={$key}">{$filename}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            {/foreach}  
                        </td>
                    </tr>
                    {/if}
                    <tr>
                        <td width="30%">日期</td>
                        <td>{$userWorkflow.create_time|date_format:"%Y-%m-%d"}</td>
                    </tr>
                    <tr>
                        <td width="30%">升级的系统名和版本号</td>
                        <td>{$userWorkflow.ext.title}</td>
                    </tr>
                    <tr>
                        <td width="30%">本次升级是</td>
                        <td>{$function_type[$userWorkflow.ext.upgrade_type]}</td>
                    </tr>
                    <tr>
                        <td width="30%">功能描述</td>
                        <td>{$userWorkflow.ext.description}</td>
                    </tr>
                    <tr>
                        <td width="30%">修正的Bug</td>
                        <td>{$userWorkflow.ext.correction_bug}</td>
                    </tr>
                    <tr>
                        <td width="30%">测试需求</td>
                        <td>{$qa_type[$userWorkflow.ext.qa_type]}</td>
                    </tr>
                    <tr>
                        <td width="30%">源代码(SVN Tag /文件列表)</td>
                        <td>{$userWorkflow.ext.source_code}</td>
                    </tr>
                    <tr>
                        <td width="30%">上线程序位置</td>
                        <td>{$userWorkflow.ext.program_location}</td>
                    </tr>
                    <tr>
                        <td width="30%">新上线程序包Md5值</td>
                        <td>{$userWorkflow.ext.online_package_md5}</td>
                    </tr>
                    <tr>
                        <td width="30%">上线步骤</td>
                        <td>{$steps}</td>
                    </tr>
                    <tr>
                        <td width="30%">回滚方案</td>
                        <td>{$userWorkflow.ext.rollback_plan}</td>
                    </tr>
                    <tr>
                        <td width="30%">监控方案</td>
                        <td>{$userWorkflow.ext.monitoring_plan}</td>
                    </tr>
                    <tr>
                        <td width="30%">备注</td>
                        <td>{$userWorkflow.ext.remark}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
