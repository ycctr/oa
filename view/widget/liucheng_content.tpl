<table class="table table-hover">
    <thead>
        <tr>
            <th>状态</th>
            <th>标题</th>
            <th>提交日期</th>
            <th>更新日期</th>
            <th style="width: 15%">操作</th>
        </tr>
    </thead>
    <tbody>
        {foreach $userWorkflows as $item}
        <tr>
            <td class="shenpi-ing">
	    {if $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_SHENPIZHONG}
		<span class="label label-info">{$auditStatus[$item.audit_status]}</span>
	    {elseif $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_PASS}
		{if $item.obj_type == "workorder"}
		<span class="label label-success">处理完成</span>
		{else}
		<span class="label label-success">{$auditStatus[$item.audit_status]}</span>
		{/if}
	    {elseif $item.audit_status == $smarty.const.WORKORDER_NOT_ISSUE}
		<span class="label label-info">{$auditStatus[$item.audit_status]}</span>
	    {elseif $item.audit_status == $smarty.const.WORKORDER_START}
		<span class="label label-info">开始处理</span>
            {elseif $item.audit_status == $smarty.const.WORKORDER_PAUSE}
		<span class="label label-important">暂停处理</span>
		{elseif $item.audit_status == $smarty.const.WORKORDER_ISSUEED}
		<span class="label label-success">已分配</span>
	    {elseif $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_REJECT}
		{if $item.obj_type == "workorder"}
		<span class="label label-important">拒绝处理</span>
		{else}
		<span class="label label-important">{$auditStatus[$item.audit_status]}</span>
		{/if}
	    {elseif $item.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_CANCEL}
		<span class="label label-important" style="background-color: #9848B9;">{$auditStatus[$item.audit_status]}</span>
	    {else}
	    {if $item.obj_type == "online_order"}
	    <span class="label label-warning">回滚</span>
	    {else}
		<span class="label label-warning">{$item.audit_status}Warning</span>
		{/if}
	    {/if}	
	    </td>
            <td>{$item.name}</td>
            <td>{$item.create_time|date_format:"%Y-%m-%d %H:%M"}</td>
            <td>{$item.modify_time|date_format:"%Y-%m-%d %H:%M"}</td>
            <td><a href="/userworkflow/look?id={$item.id}">查看</a></td>
        </tr>
        {/foreach}
    </tbody>
</table>
<div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
    <ul>
        {pager_oa count=$userWorkflowTotalCnt pagesize=$rn page=$pn pagelink=$url|cat:"pn=%d" list=3}
    </ul>
</div>

