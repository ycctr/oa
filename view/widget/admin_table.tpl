<table class="table table-hover" name="user">
    <thead>
        <tr>
            <td>id</td><td>姓名</td><td>邮箱</td><td>修改时间</td><td>操作</td> 
        </tr>
    </thead>
        {foreach item=item from=$arrList}
            <tr>
            <td>{$item.user_id}</td>
            <td>{$item.user_name}</td>
            <td>{$item.email}</td>
            <td>{$item.last_modify_time|date_format:"%Y-%m-%d"}<input type="hidden" name="modify_time" value="{$item.modify_time}"></td>
            <td>
                <table>
                    <tr>
                        {if $view_p eq 1}<a href="{$Yii->createUrl('admin/view')}?user_id={$item.user_id}">查看</a> | {/if}
                        {if $edit_p eq 1}<a href="{$Yii->createUrl('admin/edit')}?user_id={$item.user_id}">修改</a>  {/if}
                        {if $item.status eq 0}
                            {if $recover_p eq 1}
                                <a href="{$Yii->createUrl('admin/recover')}?user_id={$item.user_id}">恢复</a>
                            {/if}
                        {else}
                            {if $del_p eq 1}
                               | <a href="{$Yii->createUrl('admin/del')}?user_id={$item.user_id}" class="del">删除</a>
                            {/if}
                        {/if}    
                    </tr>
                </table>
            </td>
            </tr>
        {/foreach}
    </tbody>
</table>

