
{if !empty($daily_reimbursement[$smarty.const.REIMBURSEMENT_TYPE_TAXI])}
<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">出租车报销明细</div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="table">
                <thead>
                    <tr>
                        <th width="15%">日期</th>
                        <th width="10%">起点</th>
                        <th width="10%">终点</th>
                        <th width="10%">具体时间</th>
                        <th width="10%">用途</th>
                        <th width="25%">同行人</th>
                        <th width="10%">金额</th>
                    </tr>
                </thead>
                <tbody>
                {assign var="taxi_total" value=0}
                {foreach $daily_reimbursement[$smarty.const.REIMBURSEMENT_TYPE_TAXI] as $item}
                    {assign var="taxi_total" value=$taxi_total+$item.amount}
                    <tr>
                        <td>{$item.date_time|date_format:'%Y-%m-%d'}</td>
                        <td>{$item.start_point}</td>
                        <td>{$item.end_point}</td>
                        <td>{$item.date_time|date_format:'%H:%M:%S'}</td>
                        <td>{$item.remark}</td>
                        <td>{$item.other_people}</td>
                        <td>{$item.amount}</td>
                    </tr>
                {/foreach}
                    <tr>
                        <td colspan="6">合计</td>
                        <td>{$taxi_total}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
{/if}
{if !empty($daily_reimbursement[$smarty.const.REIMBURSEMENT_TYPE_ENTERTAINMENT])}
<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">招待费报销明细</div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="table">
                <thead>
                    <tr>
                        <th width="15%">日期</th>
                        <th width="10%">地点</th>
                        <th width="10%">业务目的</th>
                        <th width="10%">客户单位</th>
                        <th width="10%">客户姓名</th>
                        <th width="25%">参加人员</th>
                        <th width="10%">金额</th>
                    </tr>
                </thead>
                <tbody>
                {assign var="zhaodai_total" value=0}
                {foreach $daily_reimbursement[$smarty.const.REIMBURSEMENT_TYPE_ENTERTAINMENT] as $item}
                    {assign var="zhaodai_total" value=$zhaodai_total+$item.amount}
                    <tr>
                        <td>{$item.date_time|date_format:'%Y-%m-%d'}</td>
                        <td>{$item.address}</td>
                        <td>{$item.purpose}</td>
                        <td>{$item.customer_unit}</td>
                        <td>{$item.customer_name}</td>
                        <td>{$item.other_people}</td>
                        <td>{$item.amount}</td>
                    </tr>
                    {/foreach}
                    <tr>
                        <td colspan="6">合计</td>
                        <td>{$zhaodai_total}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
{/if}
{if !empty($daily_reimbursement[$smarty.const.REIMBURSEMENT_TYPE_OTHER])}
<div class="block">
    <div class="navbar navbar-inner block-header">
        <div class="muted pull-left">加班报销明细</div>
    </div>
    <div class="block-content collapse in">
        <div class="span12">
            <table class="table">
                <thead>
                    <tr>
                        <th width="15%">类型</th>
                        <th width="65%">备注</th>
                        <th width="10%">金额</th>
                    </tr>
                </thead>
                <tbody>
                {assign var="other_total" value=0}
                {foreach $daily_reimbursement[$smarty.const.REIMBURSEMENT_TYPE_OTHER] as $item}
                    {assign var="other_total" value=$other_total+$item.amount}
                    <tr>
                        <td>{$daily_reimbursementType[$item.type]} </td>
                        <td>{$item.remark}</td>
                        <td>{$item.amount}</td>
                    </tr>
                    {/foreach}
                    <tr>
                        <td colspan="2">合计</td>
                        <td>{$other_total}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
{/if}