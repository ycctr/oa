{extends file="../../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="header"}
{/block}
{block name="content"}
<div id="content" style="width:700px; margin:0 auto;">
    <style>
        body {
            padding:0;
        }
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
        }
        .form-horizontal .block tr input {
            width:80%;
        }
        @media print {
            .table {
                border-collapse:collapse;
            }
            .table td {
                font-size:12px;
                border:1px #000 solid;
                line-height:26px;
                padding:3px;
            }
            .navbar {
                height:40px;
                line-height:40px;
            }
            .print {
                display:none;
            }
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">日常报销</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <div class="form-horizontal">
                        <div class="title">
                            <div style="float:left;">
                            姓名：{$user.name}（{$user.id_card}）
                            部门：{$section.name}
                            {foreach $parentSection as $pitem}
                                /{$pitem.name}
                            {/foreach}
                            日期：{$smarty.now|date_format:"%Y-%m-%d"}
                            </div>
                            <button style="float:right;" class="btn btn-success print">打印</button>
                        </div>

                        {if !empty($daily_reimbursement[$smarty.const.REIMBURSEMENT_TYPE_TAXI])}
                        <div class="block">
                            <div style="" class="navbar navbar-inner block-header">
                                <div class="muted pull-left">出租车报销明细</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <td class="td1" width="15%">日期</td>
                                                <td width="10%">起点</td>
                                                <td width="10%">终点</td>
                                                <td width="10%">具体时间</td>
                                                <td width="10%">用途</td>
                                                <td>同行人</td>
                                                <td style="text-align:right;" width="10%">金额</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {assign var="taxi_total" value=0}
                                        {foreach $daily_reimbursement[$smarty.const.REIMBURSEMENT_TYPE_TAXI] as $item}
                                            {assign var="taxi_total" value=$taxi_total+$item.amount}
                                            <tr>
                                                <td width="15%">{$item.date_time|date_format:'%Y-%m-%d'}</td>
                                                <td width="10%">{$item.start_point}</td>
                                                <td width="10%">{$item.end_point}</td>
                                                <td width="10%">{$item.date_time|date_format:'%H:%M:%S'}</td>
                                                <td width="10%">{$item.remark}</td>
                                                <td>{$item.other_people}</td>
                                                <td style="text-align:right;" width="10%">{$item.amount}</td>
                                            </tr>
                                        {/foreach}
                                            <tr>
                                                <td colspan="6">合计</td>
                                                <td width="10%">{$taxi_total}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {/if}
                        {if !empty($daily_reimbursement[$smarty.const.REIMBURSEMENT_TYPE_ENTERTAINMENT])}
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">招待费报销明细</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <td width="15%">日期</td>
                                                <td width="10%">地点</td>
                                                <td width="10%">业务目的</td>
                                                <td width="10%">客户单位</td>
                                                <td width="10%">客户姓名</td>
                                                <td>参加人员</td>
                                                <td style="text-align:right;" width="10%">金额</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {assign var="zhaodai_total" value=0}
                                        {foreach $daily_reimbursement[$smarty.const.REIMBURSEMENT_TYPE_ENTERTAINMENT] as $item}
                                            {assign var="zhaodai_total" value=$zhaodai_total+$item.amount}
                                            <tr>
                                                <td width="15%">{$item.date_time|date_format:'%Y-%m-%d'}</td>
                                                <td width="10%">{$item.address}</td>
                                                <td width="10%">{$item.purpose}</td>
                                                <td width="10%">{$item.customer_unit}</td>
                                                <td width="10%">{$item.customer_name}</td>
                                                <td>{$item.other_people}</td>
                                                <td style="text-align:right;" width="10%">{$item.amount}</td>
                                            </tr>
                                            {/foreach}
                                            <tr>
                                                <td colspan="6">合计</td>
                                                <td width="10%">{$zhaodai_total}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {/if}
                        {if !empty($daily_reimbursement[$smarty.const.REIMBURSEMENT_TYPE_OTHER])}
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">其他报销明细</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <td width="15%">类型</td>
                                                <td>备注</td>
                                                <td style="text-align:right;" width="10%">金额</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {assign var="other_total" value=0}
                                            {foreach $daily_reimbursement[$smarty.const.REIMBURSEMENT_TYPE_OTHER] as $item}
                                            {assign var="other_total" value=$other_total+$item.amount}
                                            <tr>
                                                <td width="15%">{$daily_reimbursementType[$item.type]} </td>
                                                <td>{$item.remark}</td>
                                                <td style="text-align:right;" width="10%">{$item.amount}</td>
                                            </tr>
                                            {/foreach}
                                            <tr>
                                                <td colspan="2">合计</td>
                                                <td width="10%">{$other_total}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {/if}
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"></div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                    <table class="table" width="100%" border="1" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <td>合计</td>
                                                <td style="text-align:right;" width="10%">{$totalCnt}</td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <table style="width:100%;">
                            <tr>
                                <th style="width:30%;text-align:left;height:80px;">部门主管<br />Approval by</th>
                                <td style="width:20%;text-align:left;">{$manage.user_name}</td>
                                <th style="width:30%;text-align:left;">部门负责人<br />Vice-president's Signature</th>
                                <td style="width:20%;text-align:left;">{$vicePresident.user_name}</td>
                            </tr>
                            <tr>
                                <th style="width:30%;text-align:left;height:80px;">CEO签字<br />CEO Signature</th>
                                <td style="width:20%;text-align:left;">{$ceo.user_name}</td>
                                <th style="width:30%;text-align:left;">财务负责人签字<br />Financial signature</th>
                                <td style="width:20%;text-align:left;"></td>
                            </tr>
                            <tr>
                                <th style="width:30%;text-align:left;height:80px;">人事签字<br />Human Resource's Signature</th>
                                <td style="width:20%;text-align:left;"></td>
                                <th style="width:30%;text-align:left;">财务审核<br />Check by</th>
                                <td style="width:20%;text-align:left;"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
<script>
$(function(){
    window.print();      
    $('.print').click(function(){
        window.print();  
    });
});
</script>
{/block}

