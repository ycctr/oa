<style>
    .controls {
        padding-top:5px;
        line-height:20px;
    }
</style>
<div class="control-group">
    <label class="control-label" for="select01">付款公司</label>
    <div class="controls">
        {$extension.payment_company}
    </div>
</div>
<div class="control-group">
    <label class="control-label">付款事项简介</label>
    <div class="controls">
        {$extension.summary}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">供应商(个人)名称</label>
    <div class="controls">
        {$extension.supplier}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">付款金额</label>
    <div class="controls">
        {$extension.amount}
    </div>
</div>
{if !empty($extension.img_address)}
<div class="control-group">
    <label class="control-label" for="date01">票据照片</label>
    <div class="controls">
        {foreach $extension.img_address as $src}
        <a href="{$src}" target="_blank"><img style="max-height:200px;" src="{$src}" /></a>
        {/foreach}
    </div>
</div>
{/if}
<div class="control-group">
    <label class="control-label" for="select02">付款方式</label>
    <div class="controls">
        {$extension.payment_method}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="select02">付款性质</label>
    <div class="controls">
        {$extension.payment_nature}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">开户银行</label>
    <div class="controls">
        {$extension.bank_account}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">账户名称</label>
    <div class="controls">
        {$extension.account_name}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="date01">账户号码</label>
    <div class="controls">
        {$extension.account_number}
    </div>
</div>
<div class="control-group">
    <label class="control-label">备注信息</label>
    <div class="controls">
        {$extension.remark}
    </div>
</div>
