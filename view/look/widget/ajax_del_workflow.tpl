
	    {if $userWorkflow.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_SHENPIZHONG}
                <div type="button" onclick="cancel_workflow()"  style="height: 18px;padding-top: 6px;" class="muted pull-right btn reject">取消流程</div>
                <script>

                   function cancel_workflow() {

                        if(!confirm("确定要取消当前流程吗?")) {

                            return false;

                        }

                        check_loading.show();
                    
                        $.ajax({
                    
                            url: '/userworkflow/delflow?id={$id}',    
                            dataType: 'json',
                            success:function(res){
                    
                                if(res.code == 1){
                    
                                    location.href = "/userworkflow/my"; 
                    
                                }else if(res.code == -1) {
                    
                                    var msg = res.msg;
                                    alert(msg);
                                
                                }                    
                    
                                setTimeout(function(){
                                    check_loading.hide();
                                }, 500);
                    
                            }
                    
                        }) 

                    } 

                </script>
        {/if}
