{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">员工入职</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:125px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
        .form-horizontal .controls {
            padding-top:5px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">员工入职</div>

                {include file="./widget/ajax_del_workflow.tpl"}

            </div>
            <div class="block-content collapse in">
                <div class="span12 form-horizontal">
                    <form class="" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">部门：{$section.name}</label>
                                <label for="">日期：{$userWorkflow.create_time|date_format:"%Y-%m-%d"}</label>
                                {if $canPrint}
                                    <a class="btn btn-primary btn-large" style="float:right;" target="_blank" href="/userworkflow/look?id={$userWorkflow.id}&print=1">打印员工入职表</a>
                                {/if}
                            </div>
                            {include file="../widget/entry/content.tpl"}
                            <div class="block">
                                <div class="block-content collapse in">
                                    <div class="span12">
                                        <table class="table">
                                            <tbody>
                                            {if $userTasks}
                                            {foreach $userTasks as $userTask}
                                                <div class="control-group">
                                                    <label class="control-label">{if $userTask.user_id}{$userTask.user_name}{else}{$workflowConfig[$userTask.workflow_step].rolename}{/if}审批</label>
                                                    <div class="controls">
                                                        {if $userTask.audit_status == $smarty.const.OA_TASK_TONGGUO}
                                                            通过
                                                        {elseif $userTask.audit_status == $smarty.const.OA_TASK_JUJUE}
                                                            拒绝
                                                        {elseif $userTask.audit_status == $smarty.const.OA_TASK_CHUANGJIAN}
                                                            未审批
                                                        {elseif $userTask.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_CANCEL}
                                                            用户取消
                                                        {/if}
                                                        {if $userTask.audit_status != $smarty.const.OA_TASK_CHUANGJIAN}
                                                            <span style="margin-left:30px;">时间：{$userTask.modify_time|date_format:"%Y-%m-%d %H:%M:%S"}</span>
                                                        {/if}
                                                    </div>
                                                </div>
                                                {if $userTask.audit_desc}
                                                    <div class="control-group">
                                                        <label class="control-label">备注</label>
                                                        <div class="controls">
                                                            {if in_array($userTask.audit_desc, array_keys($business_role))}{$business_role[$userTask.audit_desc]}{else}{$userTask.audit_desc}{/if}
                                                        </div>
                                                    </div>
                                                {/if}
                                            {/foreach}
                                            {/if}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{/block}
