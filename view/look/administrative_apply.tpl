{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">物品申请</li>
            </ul>
        </div>
    </div>
    <style>
       .controls {
            padding-top:5px;
            line-height:20px;
        }
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
        .form-horizontal .block tr input {
            width:80%;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">物品申请</div>

                {include file="./widget/ajax_del_workflow.tpl"}

            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <fieldset class="form-horizontal">
                        <div class="title">
                            <label for="">姓名：{$user.name}</label>
                            <label for="">部门：{$section.name}</label>
                            <label for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="">物品名称</label>
                            <div class="controls">
                                {$userWorkflow.ext.staff}
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="">数量</label>
                            <div class="controls">
                                {$userWorkflow.ext.number}
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="">用途</label>
                            <div class="controls">
                                {$userWorkflow.ext.usage}
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="">物品类型</label>
                            <div class="controls">
                                {$userWorkflow.ext.staff_type}
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="">物品型号</label>
                            <div class="controls">
                                {$userWorkflow.ext.staff_ver}
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="">单价</label>
                            <div class="controls">
                                {$userWorkflow.ext.staff_price}
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="">总计</label>
                            <div class="controls">
                                {$userWorkflow.ext.staff_total}
                            </div>
                        </div>
                        {if $userTasks}
                        {foreach $userTasks as $userTask}
                        <div class="control-group">
                            <label class="control-label">{if $userTask.user_id}{$userTask.user_name}{else}{$workflowConfig[$userTask.workflow_step].rolename}{/if}审批</label>
                            <div class="controls">
                                {if $userTask.audit_status == $smarty.const.OA_TASK_TONGGUO}
                                通过
                                {elseif $userTask.audit_status == $smarty.const.OA_TASK_JUJUE}
                                拒绝
                                {elseif $userTask.audit_status == $smarty.const.OA_TASK_CHUANGJIAN}
                                未审批
                                {elseif $userTask.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_CANCEL}
                                用户取消
                                {/if}
                                {if $userTask.audit_status != $smarty.const.OA_TASK_CHUANGJIAN}
                    <span style="margin-left:30px;">时间：{$userTask.modify_time|date_format:"%Y-%m-%d %H:%M:%S"}</span>
                                {/if}
                            </div>
                        </div>
            {if $userTask.audit_desc}
            <div class="control-group">
                            <label class="control-label">备注</label>
                <div class="controls" style="margin-top:5px;">
                    {$userTask.audit_desc}
                </div>
            </div>
            {/if}
                        {/foreach}
                        {/if}
                        {if $finish==1}
                         <div style="color:red;text-align:center;padding-bottom:20px;">
                                您提交的申请我已收到，我们会在1~2个工作日内完成采购，根据物品类型的不同，采购周期会有所不同。我们会在物品到货后第一时间告知您来领取，请知晓！
                            </div>
                            {/if}
                    </fieldset>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{/block}
