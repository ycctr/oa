{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">申请名片</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:125px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
        .form-horizontal .controls {
            padding-top:5px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">申请名片</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12 form-horizontal">
                    <form class="" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="title">
                                <label for="" style="width:100px;">姓名：{$user.name}</label>
                                <label style="width:auto;">部门：
                                {foreach from=$sections key=key item=section name=foo}
                                    {if $smarty.foreach.foo.last}
                                        {$section.name}
                                    {else}
                                        {$section.name}/
                                    {/if}
                                {/foreach}
                                </label>
                                <label for="" style="padding-left:20px;">日期：{$userWorkflow.create_time|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <h2 style="text-align:center;">名片信息</h2>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">姓名</label>
                                <div class="controls">
                                    {$userWorkflow.ext.user_name}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">职位</label>
                                <div class="controls">
                                    {$userWorkflow.ext.position}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">电话</label>
                                <div class="controls">
                                    {$userWorkflow.ext.telephone}
                                    {if $userWorkflow.ext.extension_number}
                                        转 {$userWorkflow.ext.extension_number}
                                    {/if}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">手机</label>
                                <div class="controls">
                                    {$userWorkflow.ext.mobile}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">邮箱</label>
                                <div class="controls">
                                    {$user.email}@rong360.com
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">印刷数量</label>
                                <div class="controls">
                                    {$userWorkflow.ext.count}盒
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">备注</label>
                                <div class="controls">
                                    {$userWorkflow.ext.remark}
                                </div>
                            </div>
                            {if $userTasks}
                            {foreach $userTasks as $userTask}
                            <div class="control-group">
                                <label class="control-label">{if $userTask.user_id}{$userTask.user_name}{else}{$workflowConfig[$userTask.workflow_step].rolename}{/if}审批</label>
                                <div class="controls">
                                    {if $userTask.audit_status == $smarty.const.OA_TASK_TONGGUO}
                                    通过
                                    {elseif $userTask.audit_status == $smarty.const.OA_TASK_JUJUE}
                                    拒绝
                                    {elseif $userTask.audit_status == $smarty.const.OA_TASK_CHUANGJIAN}
                                    未审批
                                    {elseif $userTask.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_CANCEL}
                                    用户取消
                                    {/if}
                                    {if $userTask.audit_status != $smarty.const.OA_TASK_CHUANGJIAN}
				    <span style="margin-left:30px;">时间：{$userTask.modify_time|date_format:"%Y-%m-%d %H:%M:%S"}</span>
                                    {/if}
                                </div>
                            </div>
			    {if $userTask.audit_desc}
			    <div class="control-group">
                                <label class="control-label">备注</label>
				<div class="controls">
				    {$userTask.audit_desc}
				</div>
			    </div>
			    {/if}
                            {/foreach}
                                <div class="control-group">
                                    <label class="control-label">温馨提示</label>
                                    <div class="controls">
                                        行政部审批通过之后，您申请的名片会安排制作，北京办公同事大约在2~3个工作日送达，上海、深圳及其他城市办公同事因需要邮寄，大约在5~6个工作日送达，请知晓！
                                        <br><br>
                                        人力名片审核专员审批：李卉<br>人力主管审批：郭慧文<br>行政审批：张欢
                                    </div>
                                </div>
                            {/if}
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{/block}
