{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">上线单</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:125px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
        .form-horizontal .controls {
            padding-top:5px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">上线单</div>

                {include file="./widget/ajax_del_workflow.tpl"}

            </div>
            <div class="block-content collapse in">
                <div class="span12 form-horizontal">
                    <form class="" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">部门：{$section.name}</label>
                                <label for="">日期：{$userWorkflow.create_time|date_format:"%Y-%m-%d"}</label>
                            </div>
                            {include file="../widget/online_order/content.tpl"}
                            {if isset($auditList['A'])}
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">上线前签字</div>
                                </div>
                                <div class="block-content collapse in">
                                    <div class="span12">
                                        <table class="table">
                                            <tbody>
                                                {foreach $auditList['A'] as $item}
                                                <tr>
                                                    <td width="30%">{$business_role[$item.business_role]}</td>
                                                    <td>
                                                        {if $item.audit_status == $smarty.const.OA_TASK_TONGGUO}
                                                            {$item.username}&nbsp;&nbsp;
                                                       {if $item.deploy_desc != ""}(跳过审核，理由：{$item.deploy_desc}){/if}
                                                        {elseif $item.business_role == $cutTask.audit_desc }
                                                            
	{capture name="is_find" }
		{$is_find = 1}
	{/capture}
                                                            <strong id="deployShow" style="color:orange;" data-ren="{$item.username}">当前流程到:{$item.username} 签字!&nbsp;&nbsp;
                                                            {if $deploy_id > 0}
                                                            <form id="audit-from">
                                                            <input  type="hidden" name="deployid" id='deployid' value="{$deploy_id}"/>
                                                            <input  type="hidden" name="userid" id='userid' value="{$item.user_id}"/>
                                                            <input type="button" id="audit" value="跳过审核" 
                                                            title="Tips：
 当审批者不方便在OA审批流程时，可在与审批者沟通确认后，
 跳过该审批者。但必须和审批者沟通，必须注明跳过理由！"/>&nbsp;&nbsp;
                                                            <input class="input-xlarge required audit_desc" name="audit_desc" placeholder="请输入跳过审核的理由"></input>
                                                            <!--<button id="audit">跳过审核</button>-->    
                                                            </form>
                                                            {/if}
                                                            </strong>
                                                        {else}
                                                            {$item.username} 未签字
                                                        {/if}
                                                    </td>
                                                </tr>
                                                {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            {/if}

                            {if isset($auditList['B'])}
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">上线</div>
                                </div>
                                <div class="block-content collapse in">
                                    <div class="span12">
                                        <table class="table">
                                            <tbody>
                                                {foreach $auditList['B'] as $item}
                                                <tr>
                                                    <td width="30%">{$business_role[$item.business_role]}</td>
                                                    <td>
                                                        {if $item.audit_status == $smarty.const.OA_TASK_TONGGUO || $deploy_id > 0}
                                                         {if $deploy_id > 0}
                                                            {$item.username} <strong style="color:orange;">(上线平台单子，OP无需签字，默认通过)</strong>
                                                         {else}  
                                                            {$item.username}
                                                          {/if}
                                                        {elseif $item.business_role == $cutTask.audit_desc }

	{capture name="is_find" }
		{$is_find = 1}
	{/capture}

                                                            <strong style="color:orange;">当前流程到:{$item.username} 签字!</strong>
                                                        {else}
                                                            {$item.username} 未签字
                                                        {/if}
                                                    </td>
                                                </tr>
                                                {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            {/if}

                            {if isset($auditList['C'])}
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">上线后签字</div>
                                </div>
                                <div class="block-content collapse in">
                                    <div class="span12">
                                        <table class="table">
                                            <tbody>
                                                {foreach $auditList['C'] as $item}
                                                <tr>
                                                    <td width="30%">{$business_role[$item.business_role]}</td>
                                                    <td>
                                                        {if $item.audit_status == $smarty.const.OA_TASK_TONGGUO}
                                                            {$item.username}
                                                        {elseif $item.business_role == $cutTask.audit_desc  && ($is_find != 1) }

                                                            <strong style="color:orange;">当前流程到:{$item.username} 签字!</strong>
                                                        {else}
                                                            {$item.username} 未签字
                                                        {/if}
                                                    </td>
                                                </tr>
                                                {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            {/if}

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
<script type="text/javascript">
(function(window){
var $form = $('#audit-from');
var $deployid = $('#deployid');
var $userid = $('#userid');
//var $audit = $('#audit');
//var $deployShow = $('#deployShow');
    //$(document).delegate($('#audit'),'click',function(){ 
        
   //})
    $('#audit').bind('tap, click', function(){
         var $self = $(this);
         var deployid = $deployid.val();
         var val = $('.audit_desc').val();        
         var userid = $userid.val();        
         if(val == ''){
             alert('请输入跳过审核的理由');
             $('.audit_desc').focus();
             return false;
         }

         $.ajax({
            type: 'POST',
            url: 'http://oa.rong360.com/onlineorderapi/mandatoryaccesssingle',
            dataType: 'json',
            data:{
                  deploy_id: deployid,
                  desc: val,
                  user_id: userid,
            },
            success: function(result){
                     
                 if(result.errcode == 1){
                      alert("当前审批通过");
                      //$audit.hide();
                      //$deployShow.html($deployShow.attr("data-ren")).attr("style","");
                      window.location.href = window.location.href;
                 }else if(result.errcode == 0){
                      alert("审批失败");    
                 }
            },
            error:function(){
                 alert("访问失败");
            }
        });
        return false;
    });

})(window);
</script>

{/block}
