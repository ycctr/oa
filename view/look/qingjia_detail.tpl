{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">{$str_type}</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:125px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
        .form-horizontal .controls {
            padding-top:5px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">{$str_type}</div>
                

                {include file="./widget/ajax_del_workflow.tpl"}
                

            </div>
            <div class="block-content collapse in">
                <div class="span12 form-horizontal">
                    <form class="" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">部门：{$section.name}</label>
                                <label for="">日期：{$userWorkflow.create_time|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">标题</label>
                                <div class="controls">
                                    {$userWorkflow.ext.title}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">{$str_type}类别</label>
                                <div class="controls">
                                    {$absenceType[$userWorkflow.ext.type]}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">日期</label>
                                <div class="controls">
                                    {$userWorkflow.ext.start_time|date_format:"%Y-%m-%d"} {$timeNode[date('H:i:s',$userWorkflow.ext.start_time)]}
                                    到

                                    {$userWorkflow.ext.end_time|date_format:"%Y-%m-%d"} {$timeNode[date('H:i:s',$userWorkflow.ext.end_time)]}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">{$str_type}天数</label>
                                <div class="controls">
                                    {$userWorkflow.ext.day_number|string_format:"%.1f"}天
                                </div>
                            </div>


                            {if $str_type == '出差' || $str_type == '因公外出'}
                            <div class="control-group">
                                <label class="control-label" for="date01">{$str_type}地址</label>
                                <div class="controls">
                                    {$userWorkflow.ext.city}
                                </div>
                            </div>
                            {/if}

                            {if $str_type == '请假' && $userWorkflow.audit_status == 3}
                            <div class="control-group">
                                <label class="control-label" for="date01">扣假明细</label>
                                <div class="controls">
                                    扣除年假{$deduction.nian}天，病假{$deduction.bing}天
                                </div>
                            </div>
                            {/if}

                            <div class="control-group">
                                <label class="control-label">{$str_type}原因</label>
                                <div class="controls">
                                    {if $userWorkflow.obj_type=='cancel_absence'}
                                    {$userWorkflow.ext.cancel_reason}
                                    {else}
                                    {$userWorkflow.ext.leave_reason}
                                    {/if}
                                </div>
                            </div>
                            {if $userTasks}
                            {foreach $userTasks as $userTask}
                            <div class="control-group">
                                <label class="control-label">{if $userTask.user_id}{$userTask.user_name}{else}{$workflowConfig[$userTask.workflow_step].rolename}{/if}审批</label>
                                <div class="controls">
                                    {if $userTask.audit_status == $smarty.const.OA_TASK_TONGGUO}
                                    通过
                                    {elseif $userTask.audit_status == $smarty.const.OA_TASK_JUJUE}
                                    拒绝
                                    {elseif $userTask.audit_status == $smarty.const.OA_TASK_CHUANGJIAN}
                                    未审批
                                    {elseif $userTask.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_CANCEL}
                                    用户取消
                                    {/if}
                                    {if $userTask.audit_status != $smarty.const.OA_TASK_CHUANGJIAN}
				    <span style="margin-left:30px;">时间：{$userTask.modify_time|date_format:"%Y-%m-%d %H:%M:%S"}</span>
                                    {/if}
                                </div>
                            </div>
			    {if $userTask.audit_desc}
			    <div class="control-group">
                                <label class="control-label">备注</label>
				<div class="controls">
				    {$userTask.audit_desc}
				</div>
			    </div>
			    {/if}
                    {/foreach}
                    {/if}
                </fieldset>
                    </form>
                </div>
            </div>

        </div>
        <!-- /block -->

    </div>
</div>
{/block}
