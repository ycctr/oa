{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">团建报销</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
        .controls {
            line-height:30px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">团建报销</div>

                {include file="./widget/ajax_del_workflow.tpl"}
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal">
                        <fieldset>
                            {if $canPrint}
                            <div style="height:50px;">
                                <a class="btn btn-primary btn-large" style="float:right;" target="_blank" href="/userworkflow/look?id={$userWorkflow.id}&print=1">打印团建报销单</a>
                            </div>
                            {/if}
                            <div class="control-group">
                                <div style="float:left;width:512px;">
                                    <label class="control-label" for="typeahead">姓名：</label>
                                    <div style="line-height:30px;" class="controls">{$user.name}</div>
                                </div>
                                <div style="float:left;width:300px;">
                                    <label style="width:auto;" class="control-label" for="typeahead">创建日期：</label>
                                    <div style="line-height:30px; margin-left:50px;" class="controls" style="margin-left:60px;">{$userWorkflow.create_time|date_format:"%Y-%m-%d"}</div>
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float:left;width:512px;">
                                    <label class="control-label">部门：</label>
                                    <div class="controls">{$building_section.name}</div>
                                </div>
                                <div style="float:left;width:300px;">
                                    <label style="width:auto;" class="control-label">活动类型：</label>
                                    <div style="line-height:30px; margin-left:50px;" class="controls" style="margin-left:60px;">{$activity_type[$userWorkflow.ext.activity_type]}</div>
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float:left;width:512px;">
                                    <label class="control-label" for="typeahead">报销金额：</label>
                                    <div class="controls">{$userWorkflow.ext.amount}</div>
                                </div>
                                <div style="float:left;width:300px;">
                                    <label style="width:auto;" class="control-label" for="typeahead">部门余额：</label>
                                    <div id="balance" style="line-height:30px; margin-left:50px;" class="controls" style="margin-left:60px;">{$balance}</div>
                                </div>
                            </div>


                            <div class="control-group">
                                <div style="float:left;width:512px;">
                                    <label class="control-label" for="typeahead">人均消费：</label>
                                    <div class="controls">{$userWorkflow.ext.avg_amount}</div>
                                </div>
                                <div style="float:left;width:300px;">
                                    <label style="width:auto;" class="control-label" for="typeahead">参加人数：</label>
                                    <div id="balance" style="line-height:30px; margin-left:50px;" class="controls" style="margin-left:60px;">{$userWorkflow.ext.person_num}</div>
                                </div>
                            </div>
                            <hr/>

                            <div class="control-group">
                                <label class="control-label">活动背景：</label>
                                <div class="controls">
                                {$userWorkflow.ext.activity_story}
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">活动内容：</label>
                                <div class="controls">
                                {$userWorkflow.ext.desc}
                                </div>
                            </div>
                            <div class="control-group">
                                <div style="float:left;width:512px;">
                                    <label class="control-label" for="date01">活动时间：</label>
                                    <div class="controls">{$userWorkflow.ext.activity_time|date_format:"%Y-%m-%d"}</div>
                                </div>
                                <div style="float:left;width:300px;">
                                    <label style="width:auto;" class="control-label" for="date01">活动目的：</label>
                                    <div class="controls" style="margin-left:50px;">
                                    {$userWorkflow.ext.title}
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">活动地址：</label>
                                <div class="controls">
                                {$userWorkflow.ext.activity_address}
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">参加人员：</label>
                                <div class="controls">
                                {$userWorkflow.ext.participant}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">欠款金额：</label>
                                <div class="controls">
                                {$userWorkflow.ext.debt}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">备注：</label>
                                <div class="controls">
                                {$userWorkflow.ext.remark}
                                </div>
                            </div>
                        </fieldset>
                        <hr />
                        {if $userTasks}
                        {foreach $userTasks as $userTask}
                        <div class="control-group">
                            <label class="control-label">{if $userTask.user_id}{$userTask.user_name}{else}{$workflowConfig[$userTask.workflow_step].rolename}{/if}审批</label>
                            <div class="controls">
                                {if $userTask.audit_status == $smarty.const.OA_TASK_TONGGUO}
                                通过
                                {elseif $userTask.audit_status == $smarty.const.OA_TASK_JUJUE}
                                拒绝
                                {elseif $userTask.audit_status == $smarty.const.OA_TASK_CHUANGJIAN}
                                未审批
                                {elseif $userTask.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_CANCEL}
                                用户取消
                                {/if}
                                {if $userTask.audit_status != $smarty.const.OA_TASK_CHUANGJIAN}
		                <span style="margin-left:30px;">时间：{$userTask.modify_time|date_format:"%Y-%m-%d %H:%M:%S"}</span>
                                {/if}
                            </div>
                        </div>
		        {if $userTask.audit_desc}
		        <div class="control-group">
                            <label class="control-label">备注</label>
		            <div class="controls" style="margin-top:5px;">
		                {$userTask.audit_desc}
		            </div>
		        </div>
		        {/if}
                        {/foreach}
                        {/if}
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
});

$(function() {
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true,
    });
});
$(document).delegate('form','submit',function(){
    var amount = $("input[name='amount']").val();
    var balance = parseInt($('#balance').text());
    if(amount > balance){
        alert('报销金额不能超过当前余额');
        return false;
    }
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
