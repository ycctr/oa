{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">发起流程</a> <span class="divider">/</span>
            </li>
            <li class="active">午餐变更</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
        .form-horizontal .block tr input {
            width:80%;
        }
        .form-horizontal .controls {
            padding-top:5px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">午餐变更</div>
                {include file="./widget/ajax_del_workflow.tpl"}
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <input type="hidden" name="index" value="-1" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="">姓名：{$user.name}</label>
                                <label class="control-label" for="">工号：{$user.id_card}</label>
                                <label class="control-label" style="width:400px;">部门：
                                    {foreach from=$sections key=key item=section name=foo}
                                        {if $smarty.foreach.foo.last}
                                            {$section.name}
                                        {else}
                                            {$section.name} /
                                        {/if}
                                    {/foreach}
                                </label>
                                <label class="control-label" for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <br>
                            <div class="control-group">
                                <label class="control-label" for="select01">办公区</label>
                                <div class="controls" style="padding-top:5px;">
                                    {$lunchLocation[$userWorkflow.ext.lunch_location]}
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="select01">变更类型</label>
                                <div class="controls" style="padding-top:5px;">
                                    {$lunchChangeType[$userWorkflow.ext.lunch_change_type]}
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="date01">温馨提示</label>
                                <div class="controls" style="padding-top:5px;">
                                    每月午餐变更时间为当月<span style="color:#ff0000;">25日-30日</span>之间。未在规定时间内提交变更的，视为午餐预订情况无变化。
                                </div>
                            </div>

                            {if $userTasks}
                                {foreach $userTasks as $userTask}
                                    <div class="control-group">
                                        <label class="control-label">{if $userTask.user_id}{$userTask.user_name}{else}{$workflowConfig[$userTask.workflow_step].rolename}{/if}审批</label>
                                        <div class="controls">
                                            {if $userTask.audit_status == $smarty.const.OA_TASK_TONGGUO}
                                                通过
                                            {elseif $userTask.audit_status == $smarty.const.OA_TASK_JUJUE}
                                                拒绝
                                            {elseif $userTask.audit_status == $smarty.const.OA_TASK_CHUANGJIAN}
                                                未审批
                                            {elseif $userTask.audit_status == $smarty.const.OA_WORKFLOW_AUDIT_CANCEL}
                                                用户取消
                                            {/if}
                                            {if $userTask.audit_status != $smarty.const.OA_TASK_CHUANGJIAN}
                                                <span style="margin-left:30px;">时间：{$userTask.modify_time|date_format:"%Y-%m-%d %H:%M:%S"}</span>
                                            {/if}
                                        </div>
                                    </div>
                                    {if $userTask.audit_desc}
                                        <div class="control-group">
                                            <label class="control-label">备注</label>
                                            <div class="controls">
                                                {$userTask.audit_desc}
                                            </div>
                                        </div>
                                    {/if}
                                {/foreach}
                            {/if}
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
var taxihtml = '<tr><td><input type="hidden" name="row[1][type]" value="1" /><div style="width:150px;" class="input-append date datetimepicker"><input class="add-on required" name="row[1][date]" placeholder="起始日期" type="text" value="" /><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span></div></td><td><input class="required" name="row[1][start_point]" type="text" /></td><td><input class="required" name="row[1][end_point]" type="text" /></td><td><input class="required" name="row[1][time]" type="time" /></td><td><input name="row[1][remark]" type="text" /></td><td><input name="row[1][other_people]" type="text" /></td><td><input class="required number" name="row[1][amount]" type="text" /></td><td><input class="delRow" style="width:60px;" type="button" value="删除行" /></td></tr>';
var zhaodaihtml = '<tr><td><input type="hidden" name="row[2][type]" value="3" /><div class="input-append date datetimepicker"><input class="add-on required" name="row[2][date]" placeholder="起始日期" type="text" value="" /><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span></div></td><td><input class="required" name="row[2][address]" type="text" /></td><td><input name="row[2][purpose]" type="text" /></td><td><input class="required" name="row[2][customer_unit]" type="text" /></td><td><input class="required" name="row[2][customer_name]" type="text" /></td><td><input name="row[2][other_people]" type="text" /></td><td><input class="required number" name="row[2][amount]" type="text" /></td><td><input disabled="disabled" class="delRow" style="width:60px;" type="button" value="删除行" /></td></tr>';
var otherhtml = '<tr><td><select style="width:150px;" name="row[3][type]"><option value="4">加班餐费</option><option value="2">加班交通费</option></select></td><td><input class="required number" style="width:90px;" name="row[3][amount]" type="text" /></td><td><textarea class="required" style="width:350px;" name="row[3][remark]"></textarea></td><td><input disabled="disabled" class="delRow" style="width:60px;" type="button" value="删除行" /></td></tr>';
var officesupplyhtml =
        '<tr id="test">' +
            '<td><select id="supply_type[3]" style="width:180px;" name="supply_type[3]"><option value="">请选择办公用品类型</option>{/literal}{foreach $officeSupply as $key => $value}<option value="{$key}">{$key}</option>{/foreach}{literal}</select></td>' +
            '<td><select id="supply_detail[3]" style="width:180px;" name="supply_detail[3]"><option>请选择办公用品类型</option></select></td>' +
            '<td><input id="supply_amount[3]" class="required number" name="supply_amount[3]" type="text" /></td>' +
            '<td><input class="delRow" style="width:60px;" type="button" value="删除行" /></td>' +
        '</tr>';

jQuery(document).ready(function() {   
    $('.btn-primary').click(function(){
        $('.add-on').each(function(){
            var value = $(this).val();
            if(value == ''){
                return false;
            }
        })  
    });
    FormValidation.init();
});

$(function(){
    $('.addRow').click(function(){
        var index = $('[name="index"]').val();
        var variable = $(this).attr('data-type');
        var html = '';
        if(variable == 'taxi'){
            html = taxihtml;
        }else if(variable == 'zhaodai'){
            html = zhaodaihtml;
        }else if(variable == 'officeSupply'){
            html = officesupplyhtml;
        } else {
            html = otherhtml;
        }
        var dom = $(this).parentsUntil('fieldset','.block').find('tbody'); 
        var reg = new RegExp('\\[\\d\\]','g');
        var nextIndex = parseInt(index) + 1;
        html = html.replace(reg,'['+nextIndex+']');
        $(dom).append(html);
        $(dom).find('.delRow:last').removeAttr('disabled');
        $('[name="index"]').val(nextIndex);

        $('.datetimepicker').datetimepicker({
            format: 'yyyy-MM-dd',
            language: 'zh-CN',
            pickDate: true,
            pickTime: false,
            hourStep: 1,
            minuteStep: 15,
            secondStep: 30,
            inputMask: true
        });
    });
    $('#office_location').change(function(){
        var location = $('#office_location').val();
        if(location == 5){
            document.getElementById('other_location').removeAttribute('readonly');
            document.getElementById('other_location').setAttribute('value', '');
        }else{
            document.getElementById('other_location').setAttribute('readonly', 'readonly');
            document.getElementById('other_location').setAttribute('value', '无');
        }
    });

    $('fieldset').on('change', '[name^="supply_type"]',function(){
        var type = $(this).val();
        var types = {/literal}{$officeSupply|json_encode}{literal};
        var sub_types = types[type];
        var tr = $(this).closest('tr');
        var sub = tr.find('select[name^="supply_detail"]');
        $(this).find('option[value=""]').remove();
        sub.empty();
        for(var i in sub_types){
            sub.append('<option value="' + sub_types[i] + '">' + sub_types[i] + '</option>');
        }
    });


    function calculateDays()
    {
        var absenceType = $('#select01').val();
        var startDay = $('[name="start_day"]').val();
        var endDay = $('[name="end_day"]').val();
        var startNode = $('[name="start_node"]').val();
        var endNode = $('[name="end_node"]').val();
        alert('hehehe');

//        if(startDay != '' && endDay != '' && startNode != '' && endNode != '' && absenceType != ''){
//            $.ajax({
//                url : '/userworkflow/CalculateSpan',
//                data : {start_day:startDay,end_day:endDay,start_node:startNode,end_node:endNode,absence_type:absenceType},
//                type:'get',
//                dataType:'json',
//                success:function(data){
//                    $('[name="day_number"]').val(data.dayNum);
//                    if(data.msg != ''){
//                        alert(data.msg);
//                    }
//                }
//            });
//        }

    }

    $(document).delegate('.delRow','click',function(){
        $(this).parentsUntil('tbody','tr').remove();  
    });
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
