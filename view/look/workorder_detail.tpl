{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">工单系统</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:125px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
        .form-horizontal .controls {
            padding-top:5px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">工单系统</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12 form-horizontal">
                    <form class="" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label style="width:300px;">部门：
                                {foreach from=$sections key=key item=section name=foo}
                                    {if $smarty.foreach.foo.last}
                                        {$section.name}
                                    {else}
                                        {$section.name}/
                                    {/if}
                                {/foreach}
                                </label>
                                <label for="">日期：{$userWorkflow.create_time|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <h2 style="text-align:center;">工单信息</h2>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">姓名</label>
                                <div class="controls">
                                    {$user.name}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">创建时间</label>
                                <div class="controls">
                                    {$create_time|date_format:"%Y-%m-%d %H:%M:%S"}
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="date01">问题类型</label>
                                <div class="controls">
                                    {$workorderType[$type]}
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="date01">问题级别</label>
                                <div class="controls">
                                    {$workorderLevel[$level]}
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label" for="date01">问题描述</label>
                                <div class="controls">
                                    {$problem}
                                </div>
                            </div>

                            {if $issue_time > 0}
                             <div class="control-group">
                                <label class="control-label" for="date01">任务分配时间</label>
                                <div class="controls">
                                    {$issue_time|date_format:"%Y-%m-%d %H:%M:%S"}
                                </div>
                             </div>
                            {/if}


                            {if $issue_desc}
                             <div class="control-group">
                                <label class="control-label" for="date01">任务分配说明</label>
                                <div class="controls">
                                    {$issue_desc}
                                </div>
                             </div>
                            {/if}


							{if $do_time > 0}
                             <div class="control-group">
                                <label class="control-label" for="date01">开始处理时间</label>
                                <div class="controls">
                                    {$do_time|date_format:"%Y-%m-%d %H:%M:%S"}
                                </div>
                             </div>
                            {/if}

                            {if $pause_time > 0}
                             <div class="control-group">
                                <label class="control-label" for="date01">暂停时间</label>
                                <div class="controls">
                                    {$pause_time|date_format:"%Y-%m-%d %H:%M:%S"}
                                </div>
                             </div>
                            {/if}

                            {if $pause_desc}
                             <div class="control-group">
                                <label class="control-label" for="date01">暂停说明</label>
                                <div class="controls">
                                    {$pause_desc}
                                </div>
                             </div>
                            {/if}

           
							
                            {if $userTasks}
                            {foreach $userTasks as $userTask}
                            <div class="control-group">
                                <label class="control-label">{if $userTask.user_id}{$userTask.user_name}{else}{$workflowConfig[$userTask.workflow_step].rolename}{/if}处理</label>
                                <div class="controls">
                                    {if $userTask.audit_status == $smarty.const.OA_TASK_TONGGUO}
                                    已处理
                                    {elseif $userTask.audit_status == $smarty.const.OA_TASK_JUJUE}
                                    拒绝处理
                                    {elseif $userTask.audit_status == $smarty.const.WORKORDER_PAUSE}
                                    暂停中
                                    {elseif $userTask.audit_status == $smarty.const.WORKORDER_START}
                                    开始处理中
                                    {elseif $userTask.audit_status == $smarty.const.OA_TASK_CHUANGJIAN}
                                    未处理完
                                    {/if}
                                    {if $userTask.audit_status != $smarty.const.OA_TASK_CHUANGJIAN}
				    <span style="margin-left:30px;">时间：{$userTask.modify_time|date_format:"%Y-%m-%d %H:%M:%S"}</span>
                                    {/if}
                                </div>
                            </div>
			    {if $userTask.audit_desc}
			    <div class="control-group">
                                <label class="control-label">备注</label>
				<div class="controls">
				    {$userTask.audit_desc}
				</div>
			    </div>
			    {/if}
                            {/foreach}
                            {/if}
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{/block}
