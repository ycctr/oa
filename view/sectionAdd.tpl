{extends file="./_base.tpl"}
{block name='title'}添加部门
{/block}
{block name="css-common"}
<link href="/static/jquery-autocomplete/jquery.autocomplete.css" rel="stylesheet" type="text/css">
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}
<div id="content" class="span9">
<div class="block" style="margin-top:30px;">
 <div class="navbar navbar-inner block-header">
    <div class="muted pull-left">
     部门
    </div>
 </div>
 <div class="block-content collapse in">
    <div class="span12">
        <form method="post" class="form-horizontal" action="add">
            <fieldset>
                <legend>
                    添加部门
                </legend>
                {if (!is_null($data.message.error))}
                    <div class="alert alert-error ">
                        {$data.message.error}
                    </div>
                {/if}
                
                <div class="control-group" text-align:center>
                    <label class="control-label">部门名称:</label>
                    <input type="text" name="section_name" value="{$data.info.section_name}"></input></br>
                </div>
                <div class="control-group">
                    <label class="control-label">上级部门:</label>
                    <input id="parentId" type="text" name="parent_name" value="{$data.info.parent_name}" />
                    <input type="hidden" name="parent_id" value="{$data.info.parent_id}" />
                   <!--  <select  name="parent_id">
                    {foreach from=$data.sections item=item key=key}
                        {if ($data.info.parent_id==$item.id)}
                            <option value="{$item.id}" selected="selected">{$item.name}</option>
                        {else}
                            <option value="{$item.id}">{$item.name}</option>
                        {/if}
                    {/foreach}
                    </select></br> -->
                </div>
                <div class="control-group">
                    <label class="control-label">部门经理:</label>
                    <input type="text" name="manager" placeholder="请输入工号或者邮箱" value=""></input></br>
                </div>

                <div class="control-group">
                    <label class="control-label">业务线:</label>
                    <select  name="team_name" >

                        {foreach from="$section_team" item=item }
                            <option value="{$item}">{$item}</option>
                        {/foreach}

                    </select>
                </div>
                        <div class="form-actions">
                    <button class="btn btn-primary" type="submit" width="10000">添加 </button>
                    <button class="btn" type="reset">取消</button>
                </div>
            <fieldset>
        </form>
    </div>
 </div>
</div>
</div>
<script type="text/javascript" src="/static/jquery-autocomplete/jquery.autocomplete.min.js"></script>
<script>
    var data = [
        {foreach from=$data.sections item=item}
        ['{$item.name} {$item.id}', {$item.id}]{if $item@index < count($data.sections)-1},{/if}
        {/foreach}
    ];
    $("#parentId").result(function(event, data, formatted){ 
        $("input[name='parent_name']").val(data[0].split(" ")[0]);
        $("input[name='parent_id']").val(data[0].split(" ")[1]);
    });
    $('#parentId').autocomplete(data,{
        minChars: 0,
        max:300,
        delay:10,
        matchContains:true,
        multiple:false,
        formatItem: function(row) { return row[0]; }
    });

</script>
{/block}
