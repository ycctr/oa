{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">因公外出</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">因公外出</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">部门：{$section.name}</label>
                                <label for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">类别</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select required" name="type">
                                        <option value="25">因公外出</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">日期</label>
                                <div class="controls">
                                    <div class="input-append date datetimepicker">
                                        <input name="start_day" placeholder="请输入开始时间" type="text" value="{$start_day}" old-value="" class="add-on" />
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <select style="width:100px" class="time_node" name="start_node">
                                        <option value="1">上午</option>
                                        <option value="2">中午</option>
                                    </select> 
                                    到
                                    <div class="input-append date datetimepicker">
                                        <input class="add-on" name="end_day" placeholder="请输入结束时间" type="text" value="{$end_day}" old-value="" />
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <select style="width:100px" class="time_node" name="end_node">
                                        <option value="3">下午</option>
                                        <option value="2">中午</option>
                                    </select> 
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">因公外出天数</label>
                                <div class="controls">
                                    <input type="text" readonly="readonly" class="input-xlarge required number" placeholder="半天请用0.5表示" name="day_number" value="" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="date01">外出地址</label>
                                <div class="controls">
                                    <input type="text"  class="input-xlarge required" placeholder="外出地点" name="city" value="" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">外出原因</label>
                                <div class="controls">
                                    <textarea class="input-xlarge required" name="leave_reason" placeholder="请输入外出原因..." style="width: 575px; height: 200px"></textarea>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
});

$(function() {
    function calculateDays()
    {
        var absenceType = $('#select01').val();
        var startDay = $('[name="start_day"]').val();
        var endDay = $('[name="end_day"]').val();
        var startNode = $('[name="start_node"]').val();
        var endNode = $('[name="end_node"]').val();

        if(startDay != '' && endDay != '' && startNode != '' && endNode != '' && absenceType != ''){
            $.ajax({
                url : '/userworkflow/CalculateBusinessOut',
                data : {start_day:startDay,end_day:endDay,start_node:startNode,end_node:endNode,absence_type:absenceType},
                type:'get',
                dataType:'json',
                success:function(data){
                    $('[name="day_number"]').val(data.dayNum);
                    if(data.msg != ''){
                        alert(data.msg);
                    }
                }
            });
        }
        
    }
    $('.time_node,#select01').change(function(){
        calculateDays();      
    });
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true,
        afterSelectDate: calculateDays
    });
});
$(document).delegate('form','submit',function(){
	var dayNum = $('[name="day_number"]').val();
	if(dayNum == 0){
		alert('外出天数不能为零');
		return false;
	}
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
