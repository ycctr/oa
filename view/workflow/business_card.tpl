{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">名片申请</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">名片申请</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="title" style="height:50px;">
                                <label for="" style="width:100px;">姓名：{$user.name}</label>
                                <label style="width:auto;">部门：{$section.name}
                                {foreach from=$sections key=key item=section name=foo}
                                    {if $smarty.foreach.foo.last}
                                        {$section.name}
                                    {else}
                                        {$section.name}/
                                    {/if}
                                {/foreach}
                                </label>
                                <label for="" style="padding-left:20px;">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">姓名</label>
                                <div class="controls">
                                    <input type="text" class="span4 required" id="typeahead"  data-provide="typeahead" data-required="1" name="user_name" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">职位</label>
                                <div class="controls">
                                    <input type="text" class="span4 required" id="typeahead"  data-provide="typeahead" data-required="1" name="position" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">电话</label>
                                <div class="controls">
                                    <select class="span4 time_node required" name="telephone">
                                        <option value="">选择电话</option>
                                        {foreach $officetelephone as $key => $telephone}
                                        <option value="{$key}">{$telephone}</option>
                                        {/foreach}
                                    </select> 
                                    &nbsp;&nbsp;分机号码：
                                    <input style="width:150px;" type="text" class="input-xlarge" placeholder="分机号码" name="extension_number" value="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">手机</label>
                                <div class="controls">
                                    <input type="text" class="span4 input-xlarge required number" placeholder="手机号码" name="mobile" value="" />
                                    &nbsp;&nbsp;邮箱地址：
                                    {$user.email}@rong360.com
                                    {*<input style="width:150px;" type="text" class="input-xlarge" placeholder="分机号码" name="extension_number" value="" />*}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">数量</label>
                                <div class="controls">
                                    <select name="count" class="span4">
                                        <option value="2">2盒</option>
                                        <option value="4">4盒</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">备注</label>
                                <div class="controls">
                                    <textarea placeholder="如果座机没在列表里，请选择相应区域的座机代替，然后把座机在备注里" name="remark" style="width:300px;height:80px;"></textarea>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/jquery.uniform.min.js"></script>
<script src="/static/vendors/chosen.jquery.min.js"></script>
<script src="/static/vendors/bootstrap-datepicker.js"></script>

<script src="/static/vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
<script src="/static/vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

<script src="/static/vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
