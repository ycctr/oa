{extends file="../_base.tpl"}
{block name="css-page"}
    <link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
    <link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
    <link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
    {include file="../widget/left-nav.tpl"}
    <div id="content" class="span9">

        <div class="navbar">
            <div class="navbar-inner">
                <ul class="breadcrumb">
                    <li>
                        <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>
                    </li>
                    <li>
                        <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span>
                    </li>
                    <li class="active">新员工入职</li>
                </ul>
            </div>
        </div>
        <style>
            .form-horizontal .title {
                height:40px;
                line-height:40px;
                margin-bottom:20px;
                padding-left:180px;
            }
            .form-horizontal .title label {
                height:40px;
                line-height:40px;
                float:left;
                width:160px;
            }
        </style>
        <div class="row-fluid">
            <!-- block -->
            <div class="block">
                <div class="navbar navbar-inner block-header">
                    <div class="muted pull-left">新员工入职</div>
                </div>
                <div class="block-content collapse in">
                    <div class="span12">
                        <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                            <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                            <fieldset>
                                <div class="alert alert-error hide">
                                    <button class="close" data-dismiss="alert"></button>
                                    你的表单填写有错误，请检查!
                                </div>
                                <div class="alert alert-success hide">
                                    <button class="close" data-dismiss="alert"></button>
                                    Your form validation is successful!
                                </div>
                                <h2 style="text-align:center;font-size:16px;">Employee Info. 员工信息</h2>
                                <div class="control-group">
                                    <div style="float:left;width:512px;">
                                        <label class="control-label" for="typeahead">员工姓名：</label>
                                        <div class="controls">
                                            <input style="width:206px;" type="text" class="required" placeholder="" name="employeeName" value="" />
                                        </div>
                                    </div>
                                    <div style="width:auto">
                                        <label style="width:160px;" class="control-label" for="date01">姓名拼音：</label>
                                        <div class="controls" style="margin-left:50px;">
                                            <input style="width:190px;" type="text" name="nameSpell" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float:left;width:512px;">
                                        <label class="control-label" for="date01">联系电话：</label>
                                        <div class="controls">
                                            <input style="width:206px;" type="text" name="mobile" value="" />
                                        </div>
                                    </div>
                                    <div style="width:auto">
                                        <label style="width:160px" class="control-label" for="date01">预计报道日期：</label>
                                        <div class="controls">
                                            <div class="input-append date datetimepicker">
                                                <input style="width:165px;" name="approxArriveTime" placeholder="" type="text" value="" old-value="" class="add-on" />
                                            <span class="add-on">
                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float:left;width:512px;">
                                        <label class="control-label" for="typeahead">工作地：</label>
                                        <div class="controls">
                                            <input style="width:206px;" type="text" class="required" placeholder="" name="workplace" value="" />
                                        </div>
                                    </div>
                                    <div style="width:auto">
                                        <label style="width:160px" class="control-label" for="date01">邮件(用于接收offer)：</label>
                                        <div class="controls" style="margin-left:50px;">
                                            <input style="width:190px;" type="text" name="privateEmail" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float:left;width:512px;">
                                        <label class="control-label" for="typeahead">简历来源：</label>
                                        <div class="controls">
                                            <input style="width:206px;" type="text" class="required" placeholder="" name="resumeSource" value="" />
                                        </div>
                                    </div>
                                    <div style="width:auto">
                                        <label class="control-label" style="width:160px">雇佣类型：</label>
                                        <div class="controls" style=" margin-left:50px;">
                                            <select style="width:205px;" class="chzn-select required" name="hireType">
                                                {foreach $hireTypes as $key => $val}
                                                    <option value="{$key}">{$val}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float:left;width:512px;">
                                        <label class="control-label" for="date01">面试日期：</label>
                                        <div class="controls">
                                            <div class="input-append date datetimepicker">
                                                <input style="width:183px;" name="interviewTime" placeholder="" type="text" value="" old-value="" class="add-on" />
                                            <span class="add-on">
                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <h2 style="text-align:center;font-size:16px;">Position Info. 职位信息</h2>

                                <div class="control-group">
                                    <div style="float:left;width:512px;">
                                        <label class="control-label" for="date01">业务线：</label>
                                        <div class="controls">
                                            <select style="width:220px;" class="chzn-select required" name="businessLineId">
                                                {foreach $sections as $key => $record}
                                                    <option value="{$record.id}">{if $record.id == 1}无{else}{$record.name}{/if}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <div style="width:auto">
                                        <label class="control-label" style="width:160px">部门：</label>
                                        <div class="controls">
                                            <select style="width:210px;" class="chzn-select required" name="sectionId">
                                                {foreach $sections as $key => $record}
                                                    <option value="{$record.id}">{if $record.id == 1}无{else}{$record.name}{/if}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float:left;width:512px;">
                                        <label class="control-label" for="date01">分部：</label>
                                        <div class="controls">
                                            <select style="width:220px;" class="chzn-select required" name="subsectionId">
                                                {foreach $sections as $key => $record}
                                                    <option value="{$record.id}">{if $record.id == 1}无{else}{$record.name}{/if}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float:left;width:512px;">
                                        <label class="control-label" style="width:160px">直接上级：</label>
                                        <div class="controls">
                                            <select style="width:220px;" class="chzn-select required" name="managerId">
                                                {foreach $managers as $key => $record}
                                                    <option value="{$record.id}">{$record.name}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                    <div style="width:auto">
                                        <label class="control-label" style="width:160px">VP：</label>
                                        <div class="controls">
                                            <select style="width:210px;" class="chzn-select required" name="VPId">
                                                {foreach $managers as $key => $record}
                                                    {if $record.level == 90}
                                                        <option value="{$record.id}">{$record.name}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float:left;width:512px;">
                                        <label class="control-label" for="typeahead">职位：</label>
                                        <div class="controls">
                                            <input style="width:206px;" type="text" class="required" placeholder="" name="position" value="" />
                                        </div>
                                    </div>
                                    <div style="width:auto">
                                        <label style="width:160px" class="control-label" for="date01">职级：</label>
                                        <select  style="width:210px;" class="chzn-select required" name="level">
                                            <option value="0" selected="selected">无</option>
                                            <option value="{$smarty.const.AUDIT_CEO}">CEO</option>
                                            <option value="{$smarty.const.AUDIT_VP}">VP</option>
                                            <option value="{$smarty.const.AUDIT_MANAGER}" >部门负责人</option>
                                            <option value="{$smarty.const.AUDIT_VICE_MANAGER}">分部门负责人</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float:left;width:512px;">
                                        <label class="control-label" for="typeahead">薪资：</label>
                                        <div class="controls">
                                            <input style="width:206px;" type="text" class="required" placeholder="" name="salary" value="" />
                                        </div>
                                    </div>
                                    <div style="width:auto">
                                        <label style="width:160px" class="control-label" for="date01">期权：</label>
                                        <div class="controls">
                                            <input style="width:200px;" type="text" name="stockOption" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">其他</label>
                                    <div class="controls">
                                        <textarea class="input-xlarge required" name="remark" placeholder="" style="width: 690px; height: 200px"></textarea>
                                    </div>
                                </div>

                                <h2 style="text-align:center;font-size:16px;">需要填写评估表的相关人员</h2>

                                <div class="control-group">
                                    <label class="control-label" for="date01">用人部门面试评估</label>
                                    <div class="controls">
                                        <input type="text" style="width:400px" placeholder="建议填写邮箱，如多个面试官请用空格区分" class="auditlist span4 required" data-provide="typeahead" data-required="1" name="auditlist[A][interviewer]" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="date01">人力面试评估</label>
                                    <div class="controls">
                                        <input type="text" style="width:400px" placeholder="建议填写邮箱，避免有重名用户造成错误!" class="auditlist span4 required" data-provide="typeahead" data-required="1" name="auditlist[A][hr]" />
                                    </div>
                                </div>

                                {*<div class="control-group">*}
                                    {*<div style="float:left;width:512px;">*}
                                        {*<label class="control-label" for="typeahead">报销金额：</label>*}
                                        {*<div class="controls">*}
                                            {*<input style="width:206px;" type="text" class="required number" id="amount" placeholder="" name="amount" value="" />*}
                                        {*</div>*}
                                    {*</div>*}
                                    {*<div style="float:left;width:300px;">*}
                                        {*<label style="width:auto;" class="control-label" for="typeahead">余额：</label>*}
                                        {*<div style="line-height:30px; margin-left:50px;" class="controls" style="margin-left:60px;"><span id="balance"></span>  <div id="amount_url" class="btn" >查看团建经费流水</div></div>*}
                                    {*</div>*}
                                {*</div>*}
                                {*<div class="control-group">*}
                                    {*<div style="float:left;width:512px;">*}
                                        {*<label class="control-label" for="date01">活动时间：</label>*}
                                        {*<div class="controls">*}
                                            {*<div class="input-append date datetimepicker">*}
                                                {*<input style="width:183px;" name="activity_time" placeholder="请输入活动日期" type="text" value="{$start_day}" old-value="" class="add-on" />*}
                                            {*<span class="add-on">*}
                                                {*<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>*}
                                            {*</span>*}
                                            {*</div>*}
                                        {*</div>*}
                                    {*</div>*}
                                    {*<div style="float:left;width:300px;">*}
                                        {*<label style="width:auto;" class="control-label" for="date01">活动目的：</label>*}
                                        {*<div class="controls" style="margin-left:50px;">*}
                                            {*<input style="width:173px;" type="text" name="title" value="" />*}
                                        {*</div>*}
                                    {*</div>*}
                                {*</div>*}
                                {*<div class="control-group">*}
                                    {*<label class="control-label">活动地点：</label>*}
                                    {*<div class="controls">*}
                                        {*<input style="width:206px;" type="text" name="activity_address" value="" />*}
                                    {*</div>*}
                                {*</div>*}

                                {*<div class="control-group">*}
                                    {*<label class="control-label">活动背景：</label>*}
                                    {*<div class="controls">*}
                                        {*<textarea class="input-xlarge required" name="activity_story" placeholder="" style="width: 575px; height: 80px"></textarea>*}
                                    {*</div>*}
                                {*</div>*}

                                {*<div class="control-group">*}
                                    {*<label class="control-label">活动内容：</label>*}
                                    {*<div class="controls">*}
                                        {*<textarea class="input-xlarge required" name="desc" placeholder="" style="width: 575px; height: 80px"></textarea>*}
                                    {*</div>*}
                                {*</div>*}


                                {*<div class="control-group">*}
                                    {*<label class="control-label">参与人数：</label>*}
                                    {*<div class="controls">*}
                                        {*<input id="person_num" style="width:134px;" type="text" class="required number" placeholder="" name="person_num" value="" />*}
                                    {*</div>*}
                                {*</div>*}

                                {*<div class="control-group">*}
                                    {*<label class="control-label">人均花费：</label>*}
                                    {*<div class="controls">*}
                                        {*<input id="avg_amount" style="width:134px;" type="text" readonly  placeholder="" name="avg_amount" value="" />*}
                                    {*</div>*}
                                {*</div>*}

                                {*<div class="control-group">*}
                                    {*<label class="control-label">参加人员：</label>*}
                                    {*<div class="controls">*}
                                        {*<textarea class="input-xlarge required" name="participant" placeholder="" style="width: 575px; height: 80px"></textarea>*}
                                    {*</div>*}
                                {*</div>*}
                                {*<div class="control-group">*}
                                    {*<label class="control-label">欠款金额：</label>*}
                                    {*<div class="controls">*}
                                        {*<input style="width:134px;" type="text" class="required number" placeholder="" name="debt" value="" /><span style="color:red;margin-left:20px;">注：若无欠款，则填写0</span>*}
                                    {*</div>*}
                                {*</div>*}
                                {*<div class="control-group">*}
                                    {*<label class="control-label">备注：</label>*}
                                    {*<div class="controls">*}
                                        {*<textarea class="input-xlarge required" name="remark" placeholder="" style="width: 575px; height: 80px"></textarea>*}
                                    {*</div>*}
                                {*</div>*}
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary">提交</button>
                                    <button type="reset" class="btn">重置</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /block -->
        </div>
    </div>
{literal}
    <script>
        ;
        jQuery(document).ready(function() {
            FormValidation.init();
        });

        $(function() {
            $('.datetimepicker').datetimepicker({
                format: 'yyyy-MM-dd',
                language: 'zh-CN',
                pickDate: true,
                pickTime: false,
                hourStep: 1,
                minuteStep: 15,
                secondStep: 30,
                inputMask: true,
            });
        });
        $(document).delegate('form','submit',function(){
            var section = $('#select01').val();
            if(section == 0){
                alert('请选择部门');
                return false;
            }
            var amount = $("input[name='amount']").val();
            var balance = parseInt($('#balance').text());
            if(amount > balance){
                alert('报销金额不能超过当前余额');
                return false;
            }
        });

        $(function(){
            $('#select01').change(function(){
                var section_id = $(this).val();
                if(section_id != 0){
                    $.ajax({
                        url:"/teambuildingrecord/getamount?section_id="+section_id,
                        type:'get',
                        dataType:"json",
                        success:function(amount){
                            $('#balance').text(amount);
                        }
                    });
                }else{
                    $('#balance').text('');
                }
            });


            $("#amount_url").click(function() {

                var section_id = $("#select01").val();
                if(section_id == 0) {

                    alert("请选择选择团建报销的部门.");

                } else {

                    var amount_url = "/teambuildingrecord/list/?section_id="+section_id;
                    window.open(amount_url);

                }


            })

            $("#amount").blur(function(){

                var person_num = $("#person_num").val();
                var amount     = $("#amount").val();

                if(person_num > 0 && amount > 0) {

                    var avg_amount =  amount / person_num;
                    avg_amount = avg_amount.toFixed(2);
                    $("#avg_amount").val(avg_amount);

                }

            })

            $("#person_num").blur(function(){

                var person_num = $("#person_num").val();
                var amount     = $("#amount").val();

                if(person_num > 0 && amount > 0) {

                    var avg_amount =  amount / person_num;
                    avg_amount = avg_amount.toFixed(2);
                    $("#avg_amount").val(avg_amount);

                } else {

                    alert("报销金额或参与人数填写有误！");

                }

            })


        });
    </script>
{/literal}
{/block}
{block name="js-page"}
    <script src="/static/vendors/bootstrap-datetimepicker.js"></script>
    <script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

    <script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="/static/assets/form-validation.js"></script>
{/block}
