{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">票务</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div>
                <ul class="nav nav-tabs" id="myTab">
                    <li {if $workflow_id == $ticketBookId}style="border-color: #eee #eee #ddd;background-color: #eee;border-radius: 4px 4px 0 0;"{/if}><a href="/userworkflow/add?workflow_id={$workflow_id}">订票</a></li>
                    <li {if $workflow_id == $ticketRefundId}style="border-color: #eee #eee #ddd;background-color: #eee;border-radius: 4px 4px 0 0;"{/if}><a href="/userworkflow/add?workflow_id={$workflow_id}&status={$ticketRefund}">退票</a></li>
                    <li {if $workflow_id == $ticketEndorseId}style="border-color: #eee #eee #ddd;background-color: #eee;border-radius: 4px 4px 0 0;"{/if}><a href="/userworkflow/add?workflow_id={$workflow_id}&status={$ticketEndorse}">改签</a></li>
                </ul>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="control-group">
                                <label class="control-label">姓名：{$user.name}</label>
                                <label class="control-label">工号：{$user.id_card}</label>
                                <label class="control-label" style="width:400px;">部门：
                                    {foreach from=$sections key=key item=section name=foo}
                                        {if $smarty.foreach.foo.last}
                                            {$section.name}
                                        {else}
                                            {$section.name} /
                                        {/if}
                                    {/foreach}
                                </label>
                                <label class="control-label">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <div class="control-group">
                                <label class="control-label">身份证号码</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required" placeholder="请输入身份证号码" name="data[identity_card]" value="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">手机号码</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required" placeholder="请输入手机号码" name="data[mobile_number]" value="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">出行日期</label>
                                <div class="controls">
                                    <div class="input-append date datetimepicker">
                                        <input type="hidden" readonly="readonly" class="input-xlarge required" placeholder="" name="daysInterval" value="" />
                                        <input name="data[trip_date]" placeholder="请输入出行日期" type="text" value="" class="add-on" />
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <br><br>
                                      为了避免票务紧张给您的出行带来不便，烦请至少提前3天发送订票申请。紧急出行请填写原因~
                                </div>
                            </div>

                            <div class="control-group" style="display:none;" id="div1">
                                <label class="control-label" for="date01">紧急出行原因</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required" placeholder="" name="data[hurry_reason]" />
                                    <br><br>
                                    为了避免审核时间过长导致订票延误，请<span style="color:red;">如实填写</span>紧急出行原因
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">出行时间段</label>
                                <div class="controls">
                                    <div class="input-append date datetimepicker1">
                                        <input name="data[trip_time1]" placeholder="请输入出行时间段" type="text" value="" old-value="" class="add-on" />
                                    </div>
                                    至
                                    <div class="input-append date datetimepicker1">
                                        <input type="hidden" readonly="readonly" class="input-xlarge required" placeholder="" name="minsInterval" value="" />
                                        <input name="data[trip_time2]" placeholder="请输入出行时间段" type="text" value="" old-value="" class="add-on" />
                                    </div>
                                    <br><br>
                                    公司禁止指定航空公司和航班，在不影响您出行的前提下，请提供起飞时间段（至少半小时以上），我们会来合理预定，感谢支持！
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">出发城市</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required" placeholder="出发城市" name="data[from_city]" value="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">抵达城市</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required" placeholder="抵达城市" name="data[to_city]" value="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">出差事由</label>
                                <div class="controls">
                                    <textarea class="input-xlarge required" name="data[trip_reason]" placeholder="请输入出差事由..." style="width: 575px; height: 200px"></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">温馨提示</label>
                                <div class="controls" style="margin-top:5px;">
                                    行政会在一个工作日内完成订票流程，祝您工作顺利!
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
});

$(function() {
    function calculateDays()
    {
        var trip_date = $('[name="data[trip_date]"]').val();
        if(trip_date != ''){
            $.ajax({
                url : '/userworkflow/CalculateSpanTicket',
                data : {trip_date:trip_date},
                type:'get',
                dataType:'json',
                success:function(data){
                    if(data.msg != ''){
                        $('[name="data[trip_date]"]').val('');
                        $('[name="daysInterval').attr('value', '');
                        alert(data.msg);
                    }else if(data.dayNum < 3){
                        $('[name="daysInterval').attr('value', data.dayNum);
                        $('#div1').show();
                        $('[name="data[hurry_reason]"]').addClass('required');
                    }else{
                        $('[name="daysInterval').attr('value', data.dayNum);
                        $('#div1').hide();
                        $('[name="data[hurry_reason]"]').removeClass('required');
                    }
                }
            });
        }
        
    }
    function calculateTimeSpan()
    {
        var trip_time1 = $('[name="data[trip_time1]"]').val();
        var trip_time2 = $('[name="data[trip_time2]"]').val();
        if(trip_time1 != '' && trip_time2 != ''){
            $.ajax({
                url : '/userworkflow/CalculateTimeSpan',
                data : {trip_time1: trip_time1, trip_time2: trip_time2},
                type:'get',
                dataType:'json',
                success:function(data){
                    if(data.msg != ''){
                        $('[name="minsInterval').attr('value', '');
                        alert(data.msg);
                    }else{
                        $('[name="minsInterval').attr('value', data.minInterval);
                    }
                }
            });
        }

    }
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true,
        afterSelectDate: calculateDays
    });
    $('.datetimepicker1').datetimepicker({
        format: 'hh:mm',
        language: 'zh-CN',
        pickDate: false,
        pickTime: true,
        pickSeconds: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true,
        afterSelectDate: calculateTimeSpan
    });
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
