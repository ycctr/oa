{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
                <li>
                    <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>
                </li>
                <li>
                    <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span>
                </li>
                <li class="active">票务</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div>
                <ul class="nav nav-tabs" id="myTab">
                    <li {if $workflow_id == $ticketBookId}style="border-color: #eee #eee #ddd;background-color: #eee;border-radius: 4px 4px 0 0;"{/if}><a href="/userworkflow/add?workflow_id={$workflow_id}">订票</a></li>
                    <li {if $workflow_id == $ticketRefundId}style="border-color: #eee #eee #ddd;background-color: #eee;border-radius: 4px 4px 0 0;"{/if}><a href="/userworkflow/add?workflow_id={$workflow_id}&status={$ticketRefund}">退票</a></li>
                    <li {if $workflow_id == $ticketEndorseId}style="border-color: #eee #eee #ddd;background-color: #eee;border-radius: 4px 4px 0 0;"{/if}><a href="/userworkflow/add?workflow_id={$workflow_id}&status={$ticketEndorse}">改签</a></li>
                </ul>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                        <input type="hidden" name="status" value="{$ticketRefund}" />
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="control-group">
                                <label class="control-label">姓名：{$user.name}</label>
                                <label class="control-label">工号：{$user.id_card}</label>
                                <label class="control-label" style="width:400px;">部门：
                                    {foreach from=$sections key=key item=section name=foo}
                                        {if $smarty.foreach.foo.last}
                                            {$section.name}
                                        {else}
                                            {$section.name} /
                                        {/if}
                                    {/foreach}
                                </label>
                                <label class="control-label">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <div class="control-group">
                                <label class="control-label">退票原因</label>
                                <div class="controls">
                                    <textarea class="input-xlarge required" name="data[trip_reason]" placeholder="请填写本次退票的原因..." style="width: 575px; height: 200px"></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">温馨提示</label>
                                <div class="controls" style="margin-top:5px;">
                                    行政会在一个工作日内完成退票流程，祝您工作顺利!
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
});

$(function() {
    function calculateDays()
    {
        var trip_date = $('[name="data[trip_date]"]').val();
        if(trip_date != ''){
            $.ajax({
                url : '/userworkflow/CalculateSpanTicket',
                data : {trip_date:trip_date},
                type:'get',
                dataType:'json',
                success:function(data){
                    if(data.msg != ''){
                        alert(data.msg);
                    }else if(data.dayNum < 3){
                        $('[name="data[hurry_reason]"]').removeAttr('readonly');
                        $('[name="data[hurry_reason]"]').attr('value', '');
                        $('[name="days').attr('value', data.dayNum);
                    }else{
                        $('[name="data[hurry_reason]"]').attr('readonly', 'readonly');
                        $('[name="data[hurry_reason]"]').attr('value', '非紧急出行');
                        $('[name="days').attr('value', data.dayNum);
                    }
                }
            });
        }
        
    }
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true,
        afterSelectDate: calculateDays
    });
    $('.datetimepicker1').datetimepicker({
        format: 'hh:mm',
        language: 'zh-CN',
        pickDate: false,
        pickTime: true,
        pickSeconds: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true,
//        afterSelectDate: calculateTimeSpan
    });
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
