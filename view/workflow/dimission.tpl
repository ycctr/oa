{extends file="../_base.tpl"}
{block name="css-page"}
    <link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
    <link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
    <link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
    {include file="../widget/left-nav.tpl"}
    <div id="content" class="span9">

        <div class="navbar">
            <div class="navbar-inner">
                <ul class="breadcrumb">
                    <li>
                        <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>
                    </li>
                    <li>
                        <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span>
                    </li>
                    <li class="active">员工离职</li>
                </ul>
            </div>
        </div>
        <style>
            .form-horizontal .title {
                height:40px;
                line-height:40px;
                margin-bottom:20px;
                padding-left:180px;
            }
            .form-horizontal .title label {
                height:40px;
                line-height:40px;
                float:left;
                width:160px;
            }
        </style>
        <div class="row-fluid">
            <!-- block -->
            <div class="block">
                <div class="navbar navbar-inner block-header">
                    <div class="muted pull-left">员工离职申请表</div>
                </div>
                <div class="block-content collapse in">
                    <div class="span12">
                        <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                            <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                            <fieldset>
                                <div class="alert alert-error hide">
                                    <button class="close" data-dismiss="alert"></button>
                                    你的表单填写有错误，请检查!
                                </div>
                                <div class="alert alert-success hide">
                                    <button class="close" data-dismiss="alert"></button>
                                    Your form validation is successful!
                                </div>
                                <h2 style="text-align:center;font-size:16px;">员工基本信息 Employee Information</h2>
                                <div class="control-group">
                                    <div style="float:left;width:512px;">
                                        <label class="control-label" for="typeahead">姓名：</label>
                                        <div class="controls">
                                            {$user.name}
                                        </div>
                                    </div>
                                    <div style="width:auto">
                                        <label style="width:160px;" class="control-label" for="date01">员工编号：</label>
                                        <div class="controls" style="margin-left:50px;">
                                            {$user.id_card}
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float:left;width:512px;">
                                        <label class="control-label" for="date01">所在部门：</label>
                                        <div class="controls">
                                            {$user.sectionName}
                                        </div>
                                    </div>
                                    <div style="width:auto">
                                        <label style="width:160px" class="control-label" for="date01">职位名称：</label>
                                        <div class="controls">
                                            <input style="width:206px;" type="text" class="required" placeholder="" name="position" value="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float:left;width:512px;">
                                        <label class="control-label" for="typeahead">入职日期：</label>
                                        <div class="controls">
                                            {$user.hiredate}
                                        </div>
                                    </div>
                                    <div style="width:auto">
                                        <label style="width:160px" class="control-label" for="date01">直接上级：</label>
                                        <div class="controls" style="margin-left:50px;">
                                            {$user.directManager}
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div style="float:left;width:512px;">
                                        <label class="control-label" for="date01">申请离职日期：</label>
                                        <div class="controls">
                                            {*<div class="input-append date datetimepicker">*}
                                                {*<input style="width:183px;" name="apply_dimission_time" placeholder="" type="text" value="" old-value="" class="add-on" />*}
                                            {*<span class="add-on">*}
                                                {*<i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>*}
                                            {*</span>*}
                                            {*</div>*}
                                            {time()|date_format:"%Y-%m-%d"}
                                        </div>
                                    </div>
                                </div>

                                <h2 style="text-align:center;font-size:16px;">辞职原因 Reason for Resignation</h2>

                                <div class="control-group">                                   
                                    <textarea class="input-xlarge required" name="leave_reason" placeholder="" style="width: 690px; height: 200px; margin-left:100px;"></textarea>

                                </div>


                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary">提交</button>
                                    <button type="reset" class="btn">重置</button>
                                </div>

                                <div class="control-group">                                   
                                    <div style="margin-left:100px; color:red;">每月15日前离职不享受本月年假0.83天、病假0.42天</div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /block -->
        </div>
    </div>
{literal}
    <script>
        ;
        jQuery(document).ready(function() {
            FormValidation.init();
        });

        $(function() {
            $('.datetimepicker').datetimepicker({
                format: 'yyyy-MM-dd',
                language: 'zh-CN',
                pickDate: true,
                pickTime: false,
                hourStep: 1,
                minuteStep: 15,
                secondStep: 30,
                inputMask: true,
            });
        });
        $(document).delegate('form','submit',function(){
            var section = $('#select01').val();
            if(section == 0){
                alert('请选择部门');
                return false;
            }
            var amount = $("input[name='amount']").val();
            var balance = parseInt($('#balance').text());
            if(amount > balance){
                alert('报销金额不能超过当前余额');
                return false;
            }
        });

        $(function(){
            $('#select01').change(function(){
                var section_id = $(this).val();
                if(section_id != 0){
                    $.ajax({
                        url:"/teambuildingrecord/getamount?section_id="+section_id,
                        type:'get',
                        dataType:"json",
                        success:function(amount){
                            $('#balance').text(amount);
                        }
                    });
                }else{
                    $('#balance').text('');
                }
            });


            $("#amount_url").click(function() {

                var section_id = $("#select01").val();
                if(section_id == 0) {

                    alert("请选择选择团建报销的部门.");

                } else {

                    var amount_url = "/teambuildingrecord/list/?section_id="+section_id;
                    window.open(amount_url);

                }


            })

            $("#amount").blur(function(){

                var person_num = $("#person_num").val();
                var amount     = $("#amount").val();

                if(person_num > 0 && amount > 0) {

                    var avg_amount =  amount / person_num;
                    avg_amount = avg_amount.toFixed(2);
                    $("#avg_amount").val(avg_amount);

                }

            })

            $("#person_num").blur(function(){

                var person_num = $("#person_num").val();
                var amount     = $("#amount").val();

                if(person_num > 0 && amount > 0) {

                    var avg_amount =  amount / person_num;
                    avg_amount = avg_amount.toFixed(2);
                    $("#avg_amount").val(avg_amount);

                } else {

                    alert("报销金额或参与人数填写有误！");

                }

            })


        });
    </script>
{/literal}
{/block}
{block name="js-page"}
    <script src="/static/vendors/bootstrap-datetimepicker.js"></script>
    <script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

    <script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="/static/assets/form-validation.js"></script>
{/block}
