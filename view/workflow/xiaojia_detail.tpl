{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">销假</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
        .form-horizontal .controls {
            padding-top:5px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">销假</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <input type="hidden" name="absence_id" value="{$absence.id}" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">部门：{$section.name}</label>
                                <label for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">标题</label>
                                <div class="controls">
                                    {$absence.title}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">{$str_type}类别</label>
                                <div class="controls">
                                    {$absenceType[$absence.type]}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">日期</label>
                                <div class="controls">
                                    {$absence.start_time|date_format:"%Y-%m-%d"} {$timeNode[date('H:i:s',$absence.start_time)]}
                                    到

                                    {$absence.end_time|date_format:"%Y-%m-%d"} {$timeNode[date('H:i:s',$absence.end_time)]}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">请假天数</label>
                                <div class="controls">
                                    {$absence.day_number|string_format:"%.1f"}天
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">请假原因</label>
                                <div class="controls">
                                    {$absence.leave_reason}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">销假原因</label>
                                <div class="controls">
                                    <textarea class="input-xlarge required" name="cancel_reason" placeholder="请输入销假原因..." style="width: 575px; height: 200px"></textarea>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">销假</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
