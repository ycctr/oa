{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">出差报销</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
        .form-horizontal .block tr input {
            width:80%;
        }
        .form-horizontal .block{
            width:80%;
            margin-left: 50px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">出差报销</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <input type="hidden" name="index" value="0" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">工号：{$user.id_card}</label>
                                <label for="">部门：{$section.name}</label>
                                <label for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                             <div class="control-group">
                                <label class="control-label" for="card_no">报销银行卡号</label>
                                <div class="controls">
                                    <input type="text"  class="input-xlarge required" placeholder="请输入招商银行工资卡卡号" name="card_no" value="" />
                                </div>
                            </div>
                             <div class="control-group">
                                <label class="control-label" for="date01">日期</label>
                                <div class="controls">
                                    <div class="input-append date datetimepicker">
                                        <input name="start_day" placeholder="请输入开始时间" type="text" value="{$start_day}" old-value="" class="add-on" />
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <select style="width:100px" class="time_node" name="start_node">
                                        <option value="1">上午</option>
                                        <option value="2">中午</option>
                                    </select> 
                                    到
                                    <div class="input-append date datetimepicker">
                                        <input class="add-on" name="end_day" placeholder="请输入结束时间" type="text" value="{$end_day}" old-value="" />
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <select style="width:100px" class="time_node" name="end_node">
                                        <option value="3">下午</option>
                                        <option value="2">中午</option>
                                    </select> 
                                </div>
                            </div>
                         
                             <div class="control-group">
                                <label class="control-label" for="business_place">出差地点</label>
                                <div class="controls">
                                    <input type="text"  class="input-xlarge required" placeholder="请输入出差地点" name="business_place" value="" />
                                </div>
                            </div>
                             <div class="control-group">
                                <label class="control-label" for="business_purpose">出差目的</label>
                                <div class="controls">
                                    <input type="text"  class="input-xlarge required" placeholder="请输入外出原因" name="business_purpose" value="" />
                                </div>
                            </div>
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">交通报销明细</div>
                                    <div class="muted pull-right">
                                        <input class="addRow" data-type="traffic" type="button" value="添加明细" /> 
                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <div class="span12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th width="20%">日期</th>
                                                    <th width="20%">交通工具</th>
                                                    <th width="10%">起点</th>
                                                    <th width="10%">终点</th>
                                                    <th width="15%">具体时间</th>
                                                    <th width="15%">金额</th>
                                                    <th width="10%">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">出租车报销明细</div>
                                    <div class="muted pull-right">
                                        <input class="addRow" data-type="taxi" type="button" value="添加明细" /> 
                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <div class="span12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th width="20%">交通工具</th>
                                                    <th width="70%">总金额</th>
                                                    <th width="10%">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">房租报销明细</div>
                                    <div class="muted pull-right">
                                        <input class="addRow" data-type="rent" type="button" value="添加明细" /> 
                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <div class="span12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th width="60%">日期</th>
                                                    <th width="15%%">酒店名称</th>
                                                    <th width="15%">金额</th>
                                                    <th width="10%">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">招待费报销明细</div>
                                    <div class="muted pull-right">
                                        <input class="addRow" data-type="zhaodai" type="button" value="添加明细" /> 
                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <div class="span12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th width="20%">日期</th>
                                                    <th>地点</th>
                                                    <th>业务目的</th>
                                                    <th>客户单位</th>
                                                    <th>客户姓名</th>
                                                    <th>参加人员</th>
                                                    <th>金额</th>
                                                    <th width="10%">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
var traffichtml = '<tr><td><input type="hidden" name="row[1][type]" value="1" /><div style="width:150px;" class="input-append date datetimepicker"><input class="add-on required" name="row[1][date]" placeholder="起始日期" type="text" value="" /><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span></div></td><td><select style="width:150px;" name="row[1][vehicle]"><option value="1">火车</option><option value="2">客车</option><option value="3">飞机</option><option value="4">轮船</option><option value="5">其他</option></select></td><td><input class="required" name="row[1][start_point]" type="text" /></td><td><input class="required" name="row[1][end_point]" type="text" /></td><td><input class="required" name="row[1][time]" type="time" /></td><td><input class="required number" name="row[1][amount]" type="text" /></td><td><input class="delRow" style="width:60px;" type="button" value="删除行" /></td></tr>';
var taxihtml = '<tr><td><input type="hidden" name="row[2][type]" value="2" />出租车</td><td><input class="required number" name="row[2][amount]" type="text" /></td><td><input class="delRow" style="width:60px;" type="button" value="删除行" /></td></tr>';
var renthtml = '<tr><td><input type="hidden" name="row[3][type]" value="3" /><div style="width:150px;" class="input-append date datetimepicker"><input class="add-on required" style="width:80px" name="row[3][start_day]" placeholder="入住日期" type="text" value="" /><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span></div><select style="width:60px" class="time_node" name="row[3][start_node]"><option value="1">上午</option><option value="2">中午</option></select>到<div class="input-append date datetimepicker"><input class="add-on" style="width:80px" name="row[3][end_day]" placeholder="离店日期" type="text" value="" old-value="" /><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span></div><select style="width:60px" class="time_node" name="row[3][end_node]"><option value="3">下午</option><option value="2">中午</option></select> </div></td><td><input class="required" name="row[3][hname] type="text"</td><td><input class="required number" name="row[2][amount]" type="text" /></td><td><input class="delRow" style="width:60px;" type="button" value="删除行" /></td></tr>';
var zhaodaihtml = '<tr><td><input type="hidden" name="row[4][type]" value="4" /><div style="width:150px;" class="input-append date datetimepicker"><input class="add-on required" name="row[4][date]" placeholder="起始日期" type="text" value="" /><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span></div></td><td><input class="required" name="row[4][hname] type="text"</td><td><input class="required" name="row[4][goal] type="text"</td><td><input class="required" name="row[4][company] type="text"</td><td><input class="required" name="row[4][name] type="text"</td><td><input class="required" name="row[4][participant] type="text"</td><td><input class="required number" name="row[4][amount]" type="text" /></td><td><input class="delRow" style="width:60px;" type="button" value="删除行" /></td></tr>';

jQuery(document).ready(function() {   
    $('.btn-primary').click(function(){
        $('.add-on').each(function(){
            var value = $(this).val();
            if(value == ''){
                return false;
            }
        })  
    });
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
    FormValidation.init();
});


$(function(){
    $('.addRow').click(function(){
        var index = $('[name="index"]').val();
        var variable = $(this).attr('data-type');
        var html = '';
        if(variable == 'rent'){
            html = renthtml;
        }else if(variable == 'zhaodai'){
            html = zhaodaihtml;
        }else if(variable == 'taxi'){
            html = taxihtml;
        }else{
            html = traffichtml;
        }
        var dom = $(this).parentsUntil('fieldset','.block').find('tbody'); 
        var reg = new RegExp('\\[\\d\\]','g');
        var nextIndex = parseInt(index) + 1;
        html = html.replace(reg,'['+nextIndex+']');
        $(dom).append(html);
        $(dom).find('.delRow:last').removeAttr('disabled');
        $('[name="index"]').val(nextIndex);

        $('.datetimepicker').datetimepicker({
            format: 'yyyy-MM-dd',
            language: 'zh-CN',
            pickDate: true,
            pickTime: false,
            hourStep: 1,
            minuteStep: 15,
            secondStep: 30,
            inputMask: true
        });
    });      
    $(document).delegate('.delRow','click',function(){
        $(this).parentsUntil('tbody','tr').remove();  
    });
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
