{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">数据库权限申请</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">数据库权限申请</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">部门：{$section.name}</label>&nbsp;&nbsp;&nbsp;
                                <label style="margin-left:50px;" for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="">申请权限类别</label>
                                <div class="controls">
                                <label class="control-label" for="">数据库权限申请</label>
                                </div>
                            </div>
 
                            <div class="control-group">
                                <label class="control-label">数据库名</label>
                                <div class="controls">
                                    <textarea id="dbs_name" class="input-xlarge required" name="dbs_name" placeholder="请输入要申请的数据库名" style="width: 575px; height: 20px"></textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">数据库表名</label>
                                <div class="controls">
                                    <textarea id="tables_name" class="input-xlarge required" name="tables_name" placeholder="请输入要申请的数据库表名" style="width: 575px; height: 20px"></textarea>
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">申请权限详细说明</label>
                                <div class="controls">
                                    <textarea id="reason" class="input-xlarge required" name="reason" placeholder="请输入申请权限详细说明..." style="width: 575px; height: 160px"></textarea>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button id="submit_status" type="submit" class="btn btn-primary submit">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
});

</script>
{/literal}


{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
