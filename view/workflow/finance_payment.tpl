{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">{if $loan}借款冲抵{else}付款审批{/if}</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
        .form-horizontal .controls {
            padding-top:5px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">{if $loan}借款报销单{else}付款审批单{/if}</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <input type="hidden" name="loan_id" value="{$loan.id}" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">部门：{$section.name}</label>
                                <label for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">标题</label>
                                <div class="controls">
                                    <input type="text" name="title" class="required" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">合同类型</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select required" name="con_type">
                                        <option value="0">请选择</option>
                                        {foreach $con_types as $k=>$v}
                                        <option value="{$k}" {if $k==$loan.con_type_key}selected{/if}>{$v}</option>
                                        {/foreach}
                                    </select>
                                    <input type="text" name="con_type_other" value="{$loan.con_type}"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">付款事项简介</label>
                                <div class="controls">
                                    <textarea class="input-xlarge required" name="summary"  style="width: 575px; height: 100px">{$loan.summary}</textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">供应商名称</label>
                                <div class="controls">
                                    <input type="text" name="supplier" class="required">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">{if $loan}报销金额{else}付款金额{/if}</label>
                                <div class="controls">
                                    <input type="text" name="amount" class="required number" value="{$loan.amount}"/>&nbsp;元
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select02">付款方式</label>
                                <div class="controls">
                                    <select id="select02" class="chzn-select required" name="payment_method">
                                        <option value="0">请选择</option>
                                        {foreach $payment_methods as $k=>$v}
                                        <option value="{$k}">{$v}</option>
                                        {/foreach}
                                    </select>
                                    <input style="display:none;" type="text" name="payment_method_other" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select03">付款性质</label>
                                <div class="controls">
                                    <select id="select03" class="chzn-select required" name="payment_nature">
                                        <option value="0">请选择</option>
                                        {foreach $payment_natures as $k=>$v}
                                        <option value="{$k}">{$v}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            {if !$loan}
                            <div class="control-group">
                                <label class="control-label" for="select03">付款类别</label>
                                <div class="controls">
                                    {foreach $pay_types as $k=>$v}
                                    <input type="radio" name="pay_type" value="{$k}" {if $k==0}checked{/if}>&nbsp;{$v}&nbsp;&nbsp; 
                                    {/foreach}
                                </div>
                            </div>
                            {/if}
                            <div class="control-group">
                                <label class="control-label" for="date01">开户银行</label>
                                <div class="controls">
                                    <input type="text" class="required" name="bank_account" value="{$loan.bank_account}"/>
                                    <span style="color:red;padding-left:10px;">开户行需精确到支行！</span>
                                </div>
                            </div>
                             <div class="control-group">
                                <label class="control-label" for="date01">账户名称</label>
                                <div class="controls">
                                    <input type="text" class="required" name="account_name" value="{$loan.account_name}"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">账户号码</label>
                                <div class="controls">
                                    <input type="text" class="required" name="account_number" value="{$loan.account_number}"/>
                                </div>
                            </div>
                            {if !$loan}
                            <div class="control-group">
                                <label class="control-label" for="date01">上传票据图片</label>
                                <div class="controls" style="position:relative;">
                                    <input id="filePath" style="width:178px;margin-right:10px;" type="text" class="input-xlarge" name="" value="" />
                                    <input id="fileBtn" style="width:80px;height:30px;" type="button" value="上传" />
                                    <input style="position:absolute;top:0;left:0;opacity:0;filter:alpha(opacity:0);width:285px;" id="file" type="file" name="Filedata" value="" />
                                    <input id="file_path" type="hidden" name="img_address" />
                                    <div id="imgbox"></div>
                                </div>
                            </div>
                            {/if}
                            <div class="control-group">
                                <label class="control-label">备注信息</label>
                                <div class="controls">
                                    <textarea class="input-xlarge required" name="remark"  style="width: 575px; height: 100px; text-align:left;">{if $loan}借款时间：{$modify_time|date_format:"%Y-%m-%d"}
借款金额：{$loan.amount}{/if}</textarea>
                                </div>
                            </div>
                            
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">{if $loan}借款冲抵{else}提交{/if}</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
});
$(function(){
    if($('#select01').val() == 4){
        $("input[name='con_type_other']").show();
        $("input[name='con_type_other']").addClass('required');
    }else{
        $("input[name='con_type_other']").hide();
        $("input[name='con_type_other']").removeClass('required');
    }

    $(document).delegate('#file',"change",function(){
        $('#filePath').val($(this).val());
        $('#fileBtn').val('正在上传');

        $.ajaxFileUpload({
            url: '/update/uploadyunpan',
            type : 'post',
            secureuri: false, //是否需要安全协议，一般设置为false
            fileElementId: 'file', //文件上传域的ID
            dataType: 'json', //返回值类型 一般设置为json
            success: function (resp)  //服务器成功响应处理函数
            {
                $("#file").replaceWith('<input style="position:absolute;top:0;left:0;opacity:0;filter:alpha(opacity:0);width:285px;" id="file" type="file" name="Filedata" value="" />');
                if(resp.code > 0){
                    alert(resp.msg);
                }else{
                    var val = $('#file_path').val();
                    val += ';'+resp.address;
                    $('#file_path').val(val);
                    $('<img style="margin-right:10px;width:200px;height:100px;" src="'+resp.address+'" />').appendTo('#imgbox');
                }
                $('#fileBtn').val('上传');
                $('#filePath').val('');
            }
        });
    });
});
$('#select01').on('change',function(){
    var val = $(this).val(); 
    if(val == 4){
        $("input[name='con_type_other']").show();
        $("input[name='con_type_other']").addClass('required');
    }else{
        $("input[name='con_type_other']").hide();
        $("input[name='con_type_other']").removeClass('required');
    }
}); 
$('#select02').on('change',function(){
    var val = $(this).val(); 
    if(val == 4){
        $("input[name='payment_method_other']").show();
        $("input[name='payment_method_other']").addClass('required');
    }else{
        $("input[name='payment_method_other']").hide();
        $("input[name='payment_method_other']").removeClass('required');
    }
}); 
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
<script type="text/javascript" src="/static/js/ajaxfileupload.js"></script>
{/block}
