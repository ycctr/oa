{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">工单系统</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">工单系统</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal form" action="/usertask/workorderaudit" method="post">
                        <input type="hidden" name="workflow_id" value="10" />
                        <input type="hidden" name="user_workflow_id" value="{$user_workflow_id}" />
                        <input type="hidden" name="obj_id" value="{$obj_id}" />
                        <input type="hidden" name="user_task_id" value="{$user_task_id}" />
                        <input id="audit_status" type="hidden" name="audit_status" value=" " />
                        <fieldset>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">部门：{$section.name}</label>&nbsp;&nbsp;&nbsp;
                                <label for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>

                            <div class="title">
                                <label for="">问题类型：{$workorderType[$obj_info.type]}</label>
                            </div>

                            <div class="title">
                                {if $obj_info.level == $smarty.const.WORKORDER_LEVEL_URG}
                                <label for="">问题级别：<font style="color:#FF0000">{$workorderLevel[$obj_info.level]}</font></label>
                                {elseif $obj_info.level == $smarty.const.WORKORDER_LEVEL_IMP}
                                <label for="">问题级别：<font style="color:#0000FF">{$workorderLevel[$obj_info.level]}</font></label>
                                {else}
                                <label for="">问题级别：{$workorderLevel[$obj_info.level]}</label>
                                {/if}
                            </div>

							<div class="title">
                                <label for="">问题描述：{$obj_info.problem_reason}</label>
                            </div>
                            
                            <div class="title">
                                <label for="">手机号码：{$user.mobile}</label>
                            </div>

                            <div class="title">
                                公司邮箱：{$user.email}@rong360.com
                            </div>


                            <div class="control-group">
							    {if $objInfo.issue == 0}
                                <label class="control-label" for="select01">选择处理人</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select required" name="solve">
                                    {foreach $workorderSolve as $key => $val}
                                    <option value="{$key}">{$val}</option>
                                    {/foreach}
                                    </select>
                                </div>
								{else}
								<div>
								此问题已经分发到相关处理人员.
								</div>
								{/if}
                            </div>
							<div>
							<dd><textarea class="input-xlarge required audit_desc" name="issue_desc" placeholder="请输入备注说明..." style="width: 375px; hei
							    ght: 100px"></textarea></dd>
							</div>

                            <div class="form-actions">
                                <button type="button" class="btn btn-primary" value="{$smarty.const.OA_TASK_TONGGUO}">确定分配</button>
                                <button type="button" class="btn reject" value="{$smarty.const.OA_TASK_JUJUE}">拒绝分配</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script type="text/javascript">
$('.btn').click(function(){
	var audit_status = $(this).val();
	$('#audit_status').val(audit_status);
	$('.form').submit();
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
