{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">出差预借款</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
        .form-horizontal .block tr input {
            width:80%;
        }
        .form-horizontal .block{
            width:80%;
            margin-left: 50px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">出差预借款</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <input type="hidden" name="index" value="0" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">工号：{$user.id_card}</label>
                                <label for="">部门：{$section.name}</label>
                                <label for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                             <div class="control-group">
                                <label class="control-label" for="card_no">预借款银行卡号</label>
                                <div class="controls">
                                    <input type="text"  class="input-xlarge required" placeholder="请输入招商银行工资卡卡号" name="card_no" value="" />
                                </div>
                            </div>
                             <div class="control-group">
                                <label class="control-label" for="date01">预计出差日期</label>
                                <div class="controls">
                                    <div class="input-append date datetimepicker">
                                        <input name="start_day" placeholder="请输入开始时间" type="text" value="{$start_day}" old-value="" class="add-on" />
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <select style="width:100px" class="time_node" name="start_node">
                                        <option value="1">上午</option>
                                        <option value="2">中午</option>
                                    </select> 
                                    到
                                    <div class="input-append date datetimepicker">
                                        <input class="add-on" name="end_day" placeholder="请输入结束时间" type="text" value="{$end_day}" old-value="" />
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <select style="width:100px" class="time_node" name="end_node">
                                        <option value="3">下午</option>
                                        <option value="2">中午</option>
                                    </select> 
                                </div>
                            </div>
                             <div class="control-group">
                                <label class="control-label" for="business_place">出差地点</label>
                                <div class="controls">
                                    <input type="text"  class="input-xlarge required" placeholder="请输入出差地点" name="business_place" value="" />
                                </div>
                            </div>
                             <div class="control-group">
                                <label class="control-label" for="business_purpose">出差目的</label>
                                <div class="controls">
                                    <input type="text"  class="input-xlarge required" placeholder="请输入外出原因" name="business_purpose" value="" />
                                </div>
                            </div>
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">房租预计借款</div>
                                    <div class="muted pull-right">
                                        <input class="addRow" data-type="rent" type="button" value="添加明细" /> 
                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <div class="span12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th width="20%">借款类别</th>
                                                    <th width="20%">天数</th>
                                                    <th width="30%">标准</th>
                                                    <th width="20%">预估金额</th>
                                                    <th width="10%">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">宴请预计借款</div>
                                    <div class="muted pull-right">
                                        <input class="addRow" data-type="zhaodai" type="button" value="添加明细" /> 
                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <div class="span12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th width="20%">借款类别</th>
                                                    <th width="20%">次数</th>
                                                    <th width="30%">标准</th>
                                                    <th width="20%">预估金额</th>
                                                    <th width="10%">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">其他预计借款</div>
                                    <div class="muted pull-right">
                                        <input class="addRow" data-type="other" type="button" value="添加明细" /> 
                                    </div>
                                </div>
                                <div class="block-content collapse in">
                                    <div class="span12">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th width="20%">借款类别</th>
                                                    <th width="70%">预估金额</th>
                                                    <th width="10%">操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                             <div style="color:red;text-align:center;padding-bottom:20px;">
                                *说明：超过5000元的借款需提前3天通知财务。
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
var renthtml = '<tr><td><input type="hidden" name="row[1][type]" value="1" />房租</td><td><input class="required number" name="row[1][day_no]" type="text" /></td><td><input class="required" name="row[1][standard]" type="text" /></td><td><input class="required number" name="row[1][amount]" type="text" /></td><td><input class="delRow" style="width:60px;" type="button" value="删除行" /></td></tr>';
var zhaodaihtml = '<tr><td><input type="hidden" name="row[2][type]" value="3" />宴请</td><td><input class="required number" name="row[2][day_no]" type="text" /></td><td><input class="required" name="row[2][standard]" type="text" /></td><td><input class="required number" name="row[2][amount]" type="text" /></td><td><input disabled="disabled" class="delRow" style="width:60px;" type="button" value="删除行" /></td></tr>';
var otherhtml = '<tr><td><select style="width:150px;" name="row[3][type]"><option value="4">交通</option><option value="2">出租车费及其他</option><option value="5">其他</option></select></td><td><input class="required number" style="width:90px;" name="row[3][amount]" type="text" /></td><td><input disabled="disabled" class="delRow" style="width:60px;" type="button" value="删除行" /></td></tr>';


jQuery(document).ready(function() {   
    $('.btn-primary').click(function(){
        $('.add-on').each(function(){
            var value = $(this).val();
            if(value == ''){
                return false;
            }
        })  
    });
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
    FormValidation.init();
});

$(function(){
    $('.addRow').click(function(){
        var index = $('[name="index"]').val();
        var variable = $(this).attr('data-type');
        var html = '';
        if(variable == 'rent'){
            html = renthtml;
        }else if(variable == 'zhaodai'){
            html = zhaodaihtml;
        }else{
            html = otherhtml;
        }
        var dom = $(this).parentsUntil('fieldset','.block').find('tbody'); 
        var reg = new RegExp('\\[\\d\\]','g');
        var nextIndex = parseInt(index) + 1;
        html = html.replace(reg,'['+nextIndex+']');
        $(dom).append(html);
        $(dom).find('.delRow:last').removeAttr('disabled');
        $('[name="index"]').val(nextIndex);

        $('.datetimepicker').datetimepicker({
            format: 'yyyy-MM-dd',
            language: 'zh-CN',
            pickDate: true,
            pickTime: false,
            hourStep: 1,
            minuteStep: 15,
            secondStep: 30,
            inputMask: true
        });
    });      
    $(document).delegate('.delRow','click',function(){
        $(this).parentsUntil('tbody','tr').remove();  
    });
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
