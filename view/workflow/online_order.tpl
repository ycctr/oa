{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">上线单</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">发起上线</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample" class="form-horizontal" action="/userworkflow/add" method="post" onsubmit="return on_submit();"  >
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <h2 style="text-align:center;font-size:16px;">融360上线单 V1.2</h2>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">部门：{$section.name}</label>
                                <label for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <div class="control-group">
                                <label class="control-label">升级的系统名和版本号</label>
                                <div class="controls">
                                    <input type="text" class="span4 required" data-provide="typeahead" data-required="1" name="title" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">本次升级的是</label>
                                <div class="controls">
                                    <select class="span4 time_node required" name="upgrade_type">
                                        <option>请选择</option>
                                        {foreach $function_type as $key => $val}
                                        <option value="{$key}">{$val}</option>
                                        {/foreach}
                                    </select> 
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">功能描述</label>
                                <div class="controls">
                                    <textarea class="required" placeholder="" name="description" style="width:500px;height:60px;"></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">修正的BUG</label>
                                <div class="controls">
                                    <textarea placeholder="可为空" name="correction_bug" style="width:500px;height:60px;"></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">测试需求</label>
                                <div class="controls">
                                    <select class="span4 time_node required" name="qa_type">
                                        <option>请选择</option>
                                        {foreach $qa_type as $key => $val}
                                        <option value="{$key}">{$val}</option>
                                        {/foreach}
                                    </select> 
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">源代码(SVN Tag /文件列表)</label>
                                <div class="controls">
                                    <textarea placeholder="可为空" name="source_code" style="width:500px;height:60px;"></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">上线程序位置</label>
                                <div class="controls">
                                    <input type="text" placeholder="可为空" class="span4" data-provide="typeahead" data-required="1" name="program_location" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">新上线程序包Md5值</label>
                                <div class="controls">
                                    <input type="text" placeholder="可为空" class="span4" data-provide="typeahead" data-required="1" name="online_package_md5" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">上线步骤</label>
                                <div class="controls">
                                    <textarea class="required" placeholder="" name="online_steps" style="width:500px;height:60px;"></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">回滚方案</label>
                                <div class="controls">
                                    <input type="text" placeholder="可为空" class="span4" data-provide="typeahead" data-required="1" name="rollback_plan" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" >监控方案</label>
                                <div class="controls">
                                    <input type="text" placeholder="可为空" class="span4" data-provide="typeahead" data-required="1" name="monitoring_plan" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">备注</label>
                                <div class="controls">
                                    <textarea placeholder="可为空" name="remark" style="width:500px;height:60px;"></textarea>
                                </div>
                            </div>

                            <h2 style="text-align:center;font-size:16px;">上传附件</h2>
                            <div>
                                <input id="file_path" type="hidden" name="file_path" />
                                <span style="overflow: hidden;position: relative;" class="btn"><input class="file" type="file" id="file" name="dasaifile" style="font-size: 118px;cursor: pointer;position: absolute;right: 0;top: 0;opacity: 0;filter:alpha(opacity=0);" /><span class="btn-val">上传附件</span></span>
                                <span style="margin-left:50px;" id="file_box"></span>
                            </div>
                            
                            <h2 style="text-align:center;font-size:16px;">需要签字的相关人员</h2>

                            <div class="control-group">
                                <label class="control-label" for="date01">开发工程师</label>
                                <div class="controls">
                                    <input type="text" style="width:400px" placeholder="建议填写邮箱，如多个rd请用( , | 1或多空格)区分" class="auditlist span4 required" data-provide="typeahead" data-required="1" name="auditlist[A][rd]" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">测试工程师</label>
                                <div class="controls">
                                    <input type="text" placeholder="建议填写邮箱，避免有重名用户造成错误!" class="auditlist span4 required" data-provide="typeahead" data-required="1" name="auditlist[A][qa]" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">运营部门是否已经做好准备（运营签字或由PM判断并签字）</label>
                                <div class="controls">
                                    <input type="text" placeholder="如没有相关人员可不填写" class="auditlist span4" data-provide="typeahead" data-required="1" name="auditlist[A][yunying]" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">PM（适用于功能相关上线）</label>
                                <div class="controls">
                                    <input type="text" placeholder="建议填写邮箱，避免有重名用户造成错误!" class="auditlist span4 required" data-provide="typeahead" data-required="1" name="auditlist[A][pm]" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">UE负责人（涉及到UE设计）</label>
                                <div class="controls">
                                    <input type="text" placeholder="如没有相关人员可不填写" class="auditlist span4"  data-provide="typeahead" data-required="1" name="auditlist[A][ue]" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">技术部总监</label>
                                <div class="controls">
                                    <input type="text" placeholder="建议填写邮箱，避免有重名用户造成错误!" class="auditlist span4 required" data-provide="typeahead" data-required="1" name="auditlist[A][rd_manger]" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="date01">操作上线op</label>
                                <div class="controls">
                                    <input type="text" placeholder="建议填写邮箱，避免有重名用户造成错误!" class="auditlist span4 required" data-provide="typeahead" data-required="1" name="auditlist[B][op]" />
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
jQuery(document).ready(function() {   

    FormValidation.init();
    var form1 = $('#form_sample');
    var error1 = $('.alert-error', form1);
    var success1 = $('.alert-success', form1);

    form1.validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-inline', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            name: {
                minlength: 2,
                required: true
            },
            email: {
                required: true,
                email: true
            },
            url: {
                required: true,
                url: true
            },
            number: {
                required: true,
                number: true
            },
            digits: {
                required: true,
                digits: true
            },
            creditcard: {
                required: true,
                creditcard: true
            },
            occupation: {
                minlength: 5
            },
            category: {
                required: true
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success1.hide();
            error1.show();
            FormValidation.scrollTo(error1, -200);
        },

        highlight: function (element) {
            $(element)
                .closest('.help-inline').removeClass('ok'); 
            $(element)
                .closest('.control-group').removeClass('success').addClass('error'); 
        },

        unhighlight: function (element) { 
            $(element).closest('.control-group').removeClass('error');
        }

    });

});
var loading = false;
var event_ajax = "";

$(function(){

    $(document).delegate("#file","change",function(){
        $('.btn-val').text('正在上传');
        $.ajaxFileUpload({
            url: '/update/uploadfile',
            type : 'post',
            secureuri: false, //是否需要安全协议，一般设置为false
            fileElementId: 'file', //文件上传域的ID
            dataType: 'json', //返回值类型 一般设置为json
            success: function (resp)  //服务器成功响应处理函数
            {
                $("#file").replaceWith('<input class="file" type="file" id="file" name="dasaifile" style="font-size: 118px;cursor: pointer;position: absolute;right: 0; top: 0;opacity: 0;filter:alpha(opacity=0);" />');
                if(resp.code > 0){
                    alert(resp.msg);
                }else{
                    var val = $('#file_path').val();
                    val += ';'+resp.address;
                    $('#file_path').val(val);
                    $('<span style="margin-right:10px;">'+resp.filename+'</span>').appendTo('#file_box');
                    alert('上传成功！');
                }
                $('.btn-val').text('上传附件');
            }
        });
    });


    //验证用户是否存在
    $('.auditlist').blur(function(){
        var val = $(this).val();
        loading = false;
        if($.trim(val) == ''){
            return;
        }
        var me = this;
    event_ajax = $.ajax({
            url:'/api/checkuser/?name='+val,
            type:'get',
            dataType:'json',
            success:function(ret){
                if(ret.code > 0){
                    alert(ret.msg);
                    $(me).val('').focus();
                }else {

                    loading = true;
                    $(me).val(val);
                
                } 

            }
        });
    });


});


    function on_submit() {

        if(loading == false) {

            $.when(event_ajax).done(function(a1){

                if(a1.code == 0){

                    $("#form_sample").submit();

                }else {

                    return false;

                }

            })



        }else {

            return true;

        }

        return false;

    }
</script>
{/literal}
{/block}
{block name="js-page"}

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>


<script type="text/javascript" src="/static/js/ajaxfileupload.js"></script>
{/block}
