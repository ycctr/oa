{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">借款</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">借款</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">部门：{$section.name}</label>
                                <label for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">合同类型</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select required" name="con_type">
                                        <option value="0">请选择</option>
                                        {foreach $con_types as $k=>$v}
                                        <option value="{$k}">{$v}</option>
                                        {/foreach}
                                    </select>
                                    <input style="display:none;" type="text" name="con_type_other" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">请款事项简介</label>
                                <div class="controls">
                                    <textarea class="input-xlarge required" name="summary"  style="width: 575px; height: 100px"></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">请款金额</label>
                                <div class="controls">
                                    <input type="text" name="amount" class="required number" />&nbsp;元
                                </div>
                            </div>
                             <div class="control-group">
                                <label class="control-label">请款性质</label>
                                <div class="controls">
                                    {foreach $pay_types as $k=>$v}
                                    <input type="radio" name="pay_type" value="{$k}" {if $k==0}checked{/if}>&nbsp;{$v}&nbsp;&nbsp; 
                                    {/foreach}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">开户银行</label>
                                <div class="controls">
                                    <input type="text" class="required" name="bank_account" value="" />
                                    <span style="color:red;padding-left:10px;">开户行需精确到支行！</span>
                                </div>
                            </div>
                             <div class="control-group">
                                <label class="control-label" for="date01">账户名称</label>
                                <div class="controls">
                                    <input type="text" class="required" name="account_name" value="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">账户号码</label>
                                <div class="controls">
                                    <input type="text" class="required number" name="account_number" value="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">备注信息</label>
                                <div class="controls">
                                    <textarea class="" name="remark"  style="width: 575px; height: 100px"></textarea>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
});

$('#select01').on('change',function(){
    var val = $(this).val(); 
    if(val == 4){
        $("input[name='con_type_other']").show();
        $("input[name='con_type_other']").addClass('required');
    }else{
        $("input[name='con_type_other']").hide();
        $("input[name='con_type_other']").removeClass('required');
    }
}); 
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
