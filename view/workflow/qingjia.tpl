{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">请假</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">请假</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">部门：{$section.name}</label>
                                <label for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <div style="color:red;text-align:center;padding-bottom:20px;">您的可用年假为：{if $available_nianjia>0}{$available_nianjia}{else}0{/if}天，病假为：{if $available_bingjia>0 }{$available_bingjia}{else}0{/if}天</div>
                            <div class="control-group">
                                <label class="control-label" for="select01">请假类别</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select required" name="type">
                                    {foreach $absenceType as $key => $val}
                                    <option value="{$key}">{$val}</option>
                                    {/foreach}
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">日期</label>
                                <div class="controls">
                                    <div class="input-append date datetimepicker">
                                        <input name="start_day" placeholder="请输入开始时间" type="text" value="{$start_day}" old-value="" class="add-on" />
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <select style="width:100px" class="time_node" name="start_node">
                                        <option value="1">上午</option>
                                        <option value="2">中午</option>
                                    </select> 
                                    到
                                    <div class="input-append date datetimepicker">
                                        <input class="add-on" name="end_day" placeholder="请输入结束时间" type="text" value="{$end_day}" old-value="" />
                                        <span class="add-on">
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                    <select style="width:100px" class="time_node" name="end_node">
                                        <option value="3">下午</option>
                                        <option value="2">中午</option>
                                    </select> 
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">请假天数</label>
                                <div class="controls">
                                    <input type="text" readonly="readonly" class="input-xlarge required number" placeholder="半天请用0.5表示" name="day_number" value="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls" id="tip" style="color:blue;">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">请假原因</label>
                                <div class="controls">
                                    <textarea class="input-xlarge required" name="leave_reason" placeholder="请输入请假原因..." style="width: 575px; height: 200px"></textarea>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                            <div class="control-group">
                                <label class="control-label"></label>
                                <div class="controls">
                                    病假、年假、事假将按照以下规则扣除：<br>
                                    1)  病假：  优先扣病假；病假不够扣年假；年假不够，扣病休假。<br>
                                    2)  事假或年假：  优先扣年假，年假不够，扣事假。
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
<script type="text/javascript">
   var data = {json_encode($abs_array)};
</script>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
});

$(function() {
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true,
        afterSelectDate: alertInfo
    });
});
// $('form').submit(function(ev) {
//     var dayNum = $('[name="day_number"]').val();
//     if(dayNum == 0){
//         ev.preventDefault();
//         alert('请假天数不能为零');
//         return false;
//     }
// });
var tip = $('#tip');
function alertInfo(){
    calculateDays();  
    var absNum = $('input[name="day_number"]').val();
    if(absNum > 0){
        tip.innerHTML = '';
        var date = new Date();
        var monthNow = date.getMonth()+1;
        var monthNext = date.getMonth()+2;
        if(monthNext == 13){
            monthNext = 1;
        }
        var monthAbs = parseInt($('input[name="end_day"]').val().split("-")[1]);
        if(monthAbs != monthNext){
            var nian = parseFloat(data.nian);
            var bing = parseFloat(data.bing);
        }else{
            var nianPerYear = parseInt(data.nianPerYear);
            var nian = parseFloat(data.nian) + parseFloat((nianPerYear/12).toFixed(2));
            var bing = parseFloat(data.bing) + parseFloat((5/12).toFixed(2));
            if(monthAbs == 12){
                if(nianPerYear==10){
                    nian += 0.04;
                }
                bing -= 0.04;
            }
        }
        if($("#select01").val() == 1){
            if(bing > absNum){
                tip.html('温馨提示：您申请了病假'+absNum+'天，其中剩余病假可抵扣'+absNum+'天');
            }else{
                var bingUse = (bing - Math.floor(bing)) > 0.5 ? Math.floor(bing)+0.5 : Math.floor(bing);
                if((absNum-bingUse) < nian){
                    var nianUse = absNum - bingUse;
                    tip.html('温馨提示：您申请了病假'+absNum+'天，其中剩余病假可抵扣'+bingUse+'天，剩余病假不足用年假抵扣，将扣除年假'+nianUse+'天。');
                }else{
                    var nianUse = (nian - Math.floor(nian)) > 0.5 ? Math.floor(nian)+0.5 : Math.floor(nian);
                    var bingxiuUse = absNum - nianUse - bingUse;
                    tip.html('温馨提示：您申请的病假'+absNum+'天，其中剩余病假可抵扣'+bingUse+'天，年假可抵扣'+nianUse+'天， 剩余'+bingxiuUse+'天病假和年假均不够抵扣，将按病休假核算。');
                }
            }
        }else if ($("#select01").val() == 2){
            if(nian > absNum){
                tip.html('温馨提示：您申请了年假'+absNum+'天，其中剩余年假可抵扣'+absNum+'天');
            }else{
                var nianUse = (nian - Math.floor(nian)) > 0.5 ? Math.floor(nian)+0.5 : Math.floor(nian);
                tip.html('温馨提示：您申请年假'+absNum+'天，其中剩余年假天数可抵扣'+nianUse+'天， 剩余'+(absNum=nianUse)+'天将事假核算。')
            }
        }else if ($("#select01").val() == 3){
            if(nian > absNum){
                tip.html('温馨提示：您本次申请事假'+absNum+'天，将用年假进行抵扣。');
            }else{
                var nianUse = (nian - Math.floor(nian)) > 0.5 ? Math.floor(nian)+0.5 : Math.floor(nian);
                tip.html('温馨提示：您本次申请事假'+absNum+'天，将用年假抵扣'+nianUse+'天，剩余'+(absNum-nianUse)+'天按事假核算。')
            }
        }
    }
}
$('select').change(function(event) {
    alertInfo();
});
function calculateDays()
{
    var absenceType = $('#select01').val();
    var startDay = $('[name="start_day"]').val();
    var endDay = $('[name="end_day"]').val();
    var startNode = $('[name="start_node"]').val();
    var endNode = $('[name="end_node"]').val();

    if(startDay != '' && endDay != '' && startNode != '' && endNode != '' && absenceType != ''){
        $.ajax({
            url : '/userworkflow/CalculateSpan',
            data : {start_day:startDay,end_day:endDay,start_node:startNode,end_node:endNode,absence_type:absenceType},
            type:'get',
            dataType:'json',
            async: false,
            success:function(data){
                $('[name="day_number"]').val(data.dayNum);                
                if(data.msg != ''){
                    alert(data.msg);
                }
            }
        });
    }
    
}
</script>
{/literal}
<script >
$(function(){
});
</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
