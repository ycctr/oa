{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">办公用品</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
        .form-horizontal .block tr input {
            width:80%;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">办公用品</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/userworkflow/add" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <input type="hidden" name="index" value="-1" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="">姓名：{$user.name}</label>
                                <label class="control-label" for="">工号：{$user.id_card}</label>
                                <label class="control-label" style="width:400px;">部门：
                                    {foreach from=$sections key=key item=section name=foo}
                                        {if $smarty.foreach.foo.last}
                                            {$section.name}
                                        {else}
                                            {$section.name} /
                                        {/if}
                                    {/foreach}
                                </label>
                                <label class="control-label" for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">办公用品领用明细</div>
                                    <div class="muted pull-right">
                                        <input class="addRow" data-type="officeSupply" type="button" value="添加办公用品" />
                                    </div>
                                </div>
                                <div class="block-content collapse in control-group">
                                    <div class="span12 controls">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>类型</th>
                                                <th>办公用品</th>
                                                <th>数量</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="control-group">
                                <label class="control-label" for="select01">办公区</label>
                                <div class="controls">
                                    <select id="office_location" class="chzn-select required" name="office_location">
                                        {foreach $officeLocation as $key => $val}
                                            <option value="{$key}">{$val}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            {*<div class="control-group">*}
                                {*<label class="control-label" for="date01">其他办公区</label>*}
                                {*<div class="controls">*}
                                    {*<input id="other_location" type="text" readonly="readonly" class="input-xlarge required" name="other_location" value="无" />*}
                                {*</div>*}
                            {*</div>*}
                            <div class="control-group">
                                <label class="control-label" for="date01">温馨提示</label>
                                <div class="controls" style="padding-top:5px;">
                                    提交流程后，请工作日<span style="color:#ff0000;">15:00-16:00</span>到各办公区前台领用，祝您工作顺利！
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
<!-- Modal -->
<div class="modal hide fade" id="myModal" tabindex="-1" role="dialog">
<div class="modal-body">
    <p style="padding:10px;">亲爱的小融，您的申请已通过，您可以在<span style="color:red;">15:00-16:00</span>直接到前台处领取，领取记录将为您<span style="color:red;">保留3个工作日</span>，超过3个工作日未领取的系统会将您的申请驳回，由此带来不便还望您谅解。</p>
</div>
<div class="modal-footer">
    <a href="javascript:void(0);" class="btn close">确定</a>
</div>
</div>
{literal}
<script>
var taxihtml = '<tr><td><input type="hidden" name="row[1][type]" value="1" /><div style="width:150px;" class="input-append date datetimepicker"><input class="add-on required" name="row[1][date]" placeholder="起始日期" type="text" value="" /><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span></div></td><td><input class="required" name="row[1][start_point]" type="text" /></td><td><input class="required" name="row[1][end_point]" type="text" /></td><td><input class="required" name="row[1][time]" type="time" /></td><td><input name="row[1][remark]" type="text" /></td><td><input name="row[1][other_people]" type="text" /></td><td><input class="required number" name="row[1][amount]" type="text" /></td><td><input class="delRow" style="width:60px;" type="button" value="删除行" /></td></tr>';
var zhaodaihtml = '<tr><td><input type="hidden" name="row[2][type]" value="3" /><div class="input-append date datetimepicker"><input class="add-on required" name="row[2][date]" placeholder="起始日期" type="text" value="" /><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span></div></td><td><input class="required" name="row[2][address]" type="text" /></td><td><input name="row[2][purpose]" type="text" /></td><td><input class="required" name="row[2][customer_unit]" type="text" /></td><td><input class="required" name="row[2][customer_name]" type="text" /></td><td><input name="row[2][other_people]" type="text" /></td><td><input class="required number" name="row[2][amount]" type="text" /></td><td><input disabled="disabled" class="delRow" style="width:60px;" type="button" value="删除行" /></td></tr>';
var otherhtml = '<tr><td><select style="width:150px;" name="row[3][type]"><option value="4">加班餐费</option><option value="2">加班交通费</option></select></td><td><input class="required number" style="width:90px;" name="row[3][amount]" type="text" /></td><td><textarea class="required" style="width:350px;" name="row[3][remark]"></textarea></td><td><input disabled="disabled" class="delRow" style="width:60px;" type="button" value="删除行" /></td></tr>';
var officesupplyhtml =
        '<tr class="test">' +
            '<td><select id="supply_type[3]" style="width:180px;" name="supply_type[3]"><option value="">请选择办公用品类型</option>{/literal}{foreach $officeSupply as $key => $value}<option value="{$key}">{$key}</option>{/foreach}{literal}</select></td>' +
            '<td><select id="supply_detail[3]" style="width:180px;" name="supply_detail[3]"><option>请选择办公用品类型</option></select></td>' +
            '<td><input id="supply_amount[3]" class="required number" name="supply_amount[3]" type="text" /></td>' +
            '<td><input class="delRow" style="width:60px;" type="button" value="删除行" /></td>' +
        '</tr>';

jQuery(document).ready(function() {   
    $('#myModal .close').click(function(){
        $('#myModal').modal('hide');  
    });
    $('.btn-primary').click(function(){
        if($('.test').length == 0){
            alert('请填写申请明细');
            return false;
        }
        $('#myModal').modal('show');
        return false;
    });
    $('#myModal').on('hidden.bs.modal', function(){
        FormValidation.init();
        $('#form_sample_1').submit();
          // 执行一些动作...
    });
});

$(function(){
    $('.addRow').click(function(){
        var index = $('[name="index"]').val();
        var variable = $(this).attr('data-type');
        var html = '';
        if(variable == 'taxi'){
            html = taxihtml;
        }else if(variable == 'zhaodai'){
            html = zhaodaihtml;
        }else if(variable == 'officeSupply'){
            html = officesupplyhtml;
        } else {
            html = otherhtml;
        }
        var dom = $(this).parentsUntil('fieldset','.block').find('tbody'); 
        var reg = new RegExp('\\[\\d\\]','g');
        var nextIndex = parseInt(index) + 1;
        html = html.replace(reg,'['+nextIndex+']');
        $(dom).append(html);
        $(dom).find('.delRow:last').removeAttr('disabled');
        $('[name="index"]').val(nextIndex);

        $('.datetimepicker').datetimepicker({
            format: 'yyyy-MM-dd',
            language: 'zh-CN',
            pickDate: true,
            pickTime: false,
            hourStep: 1,
            minuteStep: 15,
            secondStep: 30,
            inputMask: true
        });
    });
//    $('#office_location').change(function(){
//        var location = $('#office_location').val();
//        if(location == 5){
//            document.getElementById('other_location').removeAttribute('readonly');
//            document.getElementById('other_location').setAttribute('value', '');
//        }else{
//            document.getElementById('other_location').setAttribute('readonly', 'readonly');
//            document.getElementById('other_location').setAttribute('value', '无');
//        }
//    });

    $('fieldset').on('change', '[name^="supply_type"]',function(){
        var type = $(this).val();
        var types = {/literal}{$officeSupply|json_encode}{literal};
        var sub_types = types[type];
        var tr = $(this).closest('tr');
        var sub = tr.find('select[name^="supply_detail"]');
        $(this).find('option[value=""]').remove();
        sub.empty();
        for(var i in sub_types){
            sub.append('<option value="' + sub_types[i] + '">' + sub_types[i] + '</option>');
        }
    });


    function calculateDays()
    {
        var absenceType = $('#select01').val();
        var startDay = $('[name="start_day"]').val();
        var endDay = $('[name="end_day"]').val();
        var startNode = $('[name="start_node"]').val();
        var endNode = $('[name="end_node"]').val();
        alert('hehehe');

//        if(startDay != '' && endDay != '' && startNode != '' && endNode != '' && absenceType != ''){
//            $.ajax({
//                url : '/userworkflow/CalculateSpan',
//                data : {start_day:startDay,end_day:endDay,start_node:startNode,end_node:endNode,absence_type:absenceType},
//                type:'get',
//                dataType:'json',
//                success:function(data){
//                    $('[name="day_number"]').val(data.dayNum);
//                    if(data.msg != ''){
//                        alert(data.msg);
//                    }
//                }
//            });
//        }

    }

    $(document).delegate('.delRow','click',function(){
        $(this).parentsUntil('tbody','tr').remove();  
    });
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
