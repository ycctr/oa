<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>离职申请表</title>
    <link rel="stylesheet" href="bootstrap.min.css"/>
    <script src="bootstrap.min.js"></script>
    <style>
        body{
            width: 990px;
            margin: 20px auto;
            font-size: 12px;
        }
        h2{
            text-align: center;
        }
        input{
            border: none;
        }
    </style>
</head>
<body>
<h2>员工离职申请表</h2>
<table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4" class="active">员工基本信息 Employee Information</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">
            姓名：<input type="text" style="width: 150px;"/><br>
            Chinese Name：<input type="text" style="width: 150px;"/><br>
        </th>
        <th>
            员工编号:<input type="text" style="width: 150px;"/><br>
            Number of employees：<input type="text" style="width: 150px;"/><br>
        </th>
    </tr>
    <tr>
        <th>
            所在部门:<input type="text" style="width: 150px;"/><br>
            Department：<input type="text" style="width: 150px;"/><br>
        </th>
        <th>
            职位名称:<input type="text" style="width: 150px;"/><br>
            Position Title：<input type="text" style="width: 150px;"/><br>
        </th>
    </tr>
    <tr>
        <th scope="row">
            入职日期:<input type="text" style="width: 150px;"/><br>
            Entry Date：<input type="text" style="width: 150px;"/>
        </th>
        <th>
            直接上级:<input type="text" style="width: 150px;"/><br>
            Supervisor：<input type="text" style="width: 150px;"/>
        </th>
    </tr>
    <tr>
        <th scope="row">
            申请离职日期:<input type="text" style="width: 200px;"/><br>
            Proposed Separation Date：<input type="text" style="width: 150px;"/>
        </th>
        <td>

        </td>
    </tr>
    </tbody>
</table>
<table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4" class="active">辞职原因 Reason for Resignation</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>
            <textarea class="form-control" rows="7"></textarea>
            <p style="margin-top: 20px;">
                申请人: <input type="text" width="200px;" style="margin-right: 200px;"/>
                申请日期: <input type="text" width="200px;"/>
            </p>
        </th>

    </tr>
    </tbody>
</table>
<table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4" class="active">Approval Info. 审批信息</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">直接主管审批:<br>Signature & Date </th>
        <td>
            签字:
            <input type="text" width="100px;"/>
        </td>
        <td>
            日期:
            <input type="text" width="100px;"/>
        </td>
    </tr>
    <tr>
        <th scope="row">部门负责人审批:<br>Signature & Date </th>
        <td>
            签字:
            <input type="text" width="100px;"/>
        </td>
        <td>
            日期:
            <input type="text" width="100px;"/>
        </td>
    </tr>
    <tr>
        <th scope="row">人力部门审批:<br>Signature & Date </th>
        <td>
            签字:
            <input type="text" width="100px;"/>
        </td>
        <td>
            日期:
            <input type="text" width="100px;"/>
        </td>
    </tr>
    <tr>
        <th scope="row">V主管VP审批:<br>Signature & Date </th>
        <td>
            签字:
            <input type="text" width="100px;"/>
        </td>
        <td>
            日期:
            <input type="text" width="100px;"/>
        </td>
    </tr>
    </tbody>
</table>
<div class="zhu">
    <p>注：员工辞职可采用邮件或填写本申请表形式，请部门经理及分管副总裁批准后交人事部审核存档。</p>
</div>
</body>
</html>