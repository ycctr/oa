<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>离职交接申请表</title>
    <link rel="stylesheet" href="bootstrap.min.css"/>
    <script src="bootstrap.min.js"></script>
    <style>
        body{
            width: 990px;
            margin: 20px auto;
            font-size: 12px;
        }
        h2{
            text-align: center;
        }
        input{
            border: none;
        }
    </style>
</head>
<body>
{*<h2>离职交接清单</h2>*}
<table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4" class="active">员工基本信息 Employee Information</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">
            姓名：<br>
            Chinese Name：{$dimissionUser.name}<br>
        </th>
        <th>
            员工编号:<br>
            Number of employees：{$dimissionUser.id_card}<br>
        </th>
    </tr>
    <tr>
        <th>
            所在部门:<br>
            Department：{$dimissionUser.department}<br>
        </th>
        <th>
            职位名称:<br>
            Position Title：{$userWorkflow.ext.position}<br>
        </th>
    </tr>
    </tbody>
</table>
<table class="table table-bordered" style="margin-bottom: 20px;">
    {*<thead>*}
    {*<tr>*}
        {*<th colspan="4" class="active">离职移交内容</th>*}
    {*</tr>*}
    {*</thead>*}
    <tbody>
    {if $userTask['workflow_step'] == 'C'}
    <tr>
        <th scope="row" style="width: 150px; text-align: center">本部门工作交接<br>（直属领导)</th>
        <td>
            <p>
                1. 文件及实物移交
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox"> 是
                </label>
            </p>
            <p>
                2. 工作事项移交
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox"> 是
                </label>
            </p>
            <p>
                接收人:<input type="text" name="data[department_receiver]"/>
                监交人:<input type="text" name="data[department_watcher]"/>
            </p>
            <p>
                {*日期:<input type="text" name="data[department_receive_time]" width="300px"/>*}
                日期
                <div class="input-append date datetimepicker">
                    <input name="data[department_receive_time]" type="text" class="add-on" />
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                </div>
                日期
                <div class="input-append date datetimepicker">
                    <input name="data[department_watch_time]" type="text" class="add-on" />
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                </div>
            </p>
            <p>
                3. 员工关系组确认：已和部门确认工作交接完毕
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox"> 是
                </label>
            </p>
        </td>
    </tr>
    {/if}
    {if $userTask['workflow_step'] == 'D'}
    <tr>
        <th rowspan="3" style="width: 150px; text-align: center;">技术支持部物品交接及系统账号交接 </th>
        <td colspan="6">
            <p>确认人：产品部-程昊</p>
            <p>
                1. 系统账号
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                2. 特殊网络访问权限
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                接收人:<input type="text" name="data[system_account_receiver]"/><br>
                日期
            <div class="input-append date datetimepicker">
                <input name="data[system_account_receive_time]" type="text" class="add-on" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
            </div>
            </p>
        </td>
    </tr>
    {/if}
    {if $userTask['workflow_step'] == 'E'}
    <tr>
        <td colspan="6">
            <p>确认人：产品部-王李卿</p>
            <p>
                1.RISK系统账号
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                接收人:<input type="text" name="data[risk_account_receiver]"/><br>
                日期
            <div class="input-append date datetimepicker">
                <input name="data[risk_account_receive_time]" type="text" class="add-on" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
            </div>
            </p>
        </td>
    </tr>
    {/if}
    {if $userTask['workflow_step'] == 'F'}
    <tr>
        <td colspan="6">
            <p>确认人：系统部-张李楠</p>
            <p>
                1.邮箱账号
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                2.VPN账号
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                3.其他
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                接收人:<input type="text" name="data[mail_account_receiver]"/><br>
                日期
            <div class="input-append date datetimepicker">
                <input name="data[mail_account_receive_time]" type="text" class="add-on" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
            </div>
            </p>
        </td>
    </tr>
    {/if}
    {if $userTask['workflow_step'] == 'G'}
    <tr>
        <th scope="row" style="width: 150px; text-align: center">财务部事项交接</th>
        <td>
            <p>确认人：财务部-和潇</p>
            <p>
                1. 预备金返还
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                2. 借款返还
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                3. 结算报销费用
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                4. 主要外部合同
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                5. 其他
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                接收人:<input type="text" name="data[finance_receiver]"/><br>
                日期
            <div class="input-append date datetimepicker">
                <input name="data[finance_receive_time]" type="text" class="add-on" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
            </div>
            </p>
        </td>
    </tr>
    {/if}
    {if $userTask['workflow_step'] == 'H'}
    <tr>
        <th scope="row" style="width: 150px; text-align: center">行政部物品交接</th>
        <td>
            <p>确认人：行政部-吕欣桐\张欢</p>
            <p>
                1. 手提电脑/PC
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                2. 液晶显示器
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                3. 测试机
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                4. 电话（普通/客服专用/大灵通）
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                5. 非标配物品
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                6. 公司名片
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                7. 办公用品
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                8. 门店工服
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                9. 门禁卡
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                10. 工位名牌
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>

            <p>
                11. 滴滴打车
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                接收人:<input type="text" name="data[administration_receiver]"/><br>
                日期
            <div class="input-append date datetimepicker">
                <input name="data[administration_receive_time]" type="text" class="add-on" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
            </div>
            </p>
        </td>
    </tr>
    {/if}
    {if $userTask['workflow_step'] == 'I'}
    <tr>
        <th scope="row" style="width: 150px; text-align: center">人力资源部<br>
            薪酬福利交接<br>
            （人力资源部）
        </th>
        <td>
            <p>确认人：人力资源部 </p>
            <p>
                1. 离职生效日:
                <div class="input-append date datetimepicker">
                    <input name="data[hr_dimission_take_effect_time]" type="text" class="add-on" />
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                </div>
            </p>
            <p>
                2.五险一金停缴日期:
                <div class="input-append date datetimepicker">
                    <input name="data[hr_wuxianyijin_stop_time]" type="text" class="add-on" />
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                </div>
            </p>
            <p>
                3. 年假天数：<input name="data[hr_nianjia_days]" type="text" class="add-on" /> 剩余天数：<input name="data[hr_remain_days]" type="text" class="add-on" />
            </p>
            <p>
                4. 薪资截止日期:
                <div class="input-append date datetimepicker">
                    <input name="data[hr_salary_stop_time]" type="text" class="add-on" />
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                </div>
            </p>
            <p>
                5. 档案调出
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                6. 删除QQ群、微信群
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                7. OA权限
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 否
                </label>
                <label class="checkbox-inline" style="float: right;margin-right: 20px;">
                    <input type="checkbox" id="inlineCheckbox1" value="option1"> 是
                </label>
            </p>
            <p>
                8. 其他
                <textarea class="form-control" rows="2" name="data[hr_remark]"></textarea>
            </p>
            <p>
                确认人:<input type="text" name="data[hr_confirmer]"/><br>
                日期
                <div class="input-append date datetimepicker">
                    <input name="data[hr_confirm_time]" type="text" class="add-on" />
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                </div>
            </p>
        </td>
    </tr>
    {/if}
    {if $userTask['workflow_step'] == 'J'}
    <tr>
        <th scope="row" style="width: 150px; text-align: center">人力资源总监确认</th>
        <td>
            <p>
                签字:<input type="text"/><br>
                日期:
                <div class="input-append date datetimepicker">
                    <input name="data[hrd_confirm_time]" type="text" class="add-on" />
                            <span class="add-on">
                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                            </span>
                </div>
            </p>
        </td>
    </tr>
    {/if}
    {if $userTask['workflow_step'] == 'K'}
    <tr>
        <th scope="row" style="width: 150px; text-align: center">员工签字确认</th>
        <td>
            <p style="color: red">
                个人声明：<br>
                1、本人已结清所有借/欠款项，并将借用/领用物品完好交还公司。如本人存在故意私藏所属公司之任何文件、物品，以及隐瞒任何借/欠款项等行为的，一经发现，公司有权向我本人进一步追索。如我拒不偿还的，公司有权诉诸法律解决。
                <br>2、本人已认真阅读以上离职交接内容，内容与事实相符，无异议，并同意已与公司解除劳动关系，不再直接或间接就该劳动关系主张任何权利，已无其他争议及未决事项。
            </p>
            <p>
                签字:<input type="text"/><br>
                日期:
                <div class="input-append date datetimepicker">
                    <input name="data[hrd_confirm_time]" type="text" class="add-on" />
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                </div>
            </p>
        </td>
    </tr>
    {/if}
    </tbody>
</table>
<div class="zhu">
    注：
    <p>	1.  请按照以上交接流程，分别请各部门负责同事签字确认，最后交给人力资源部留存。</p>
    <p>2.  若本表格不足应用，可用空白纸张作补充，有关人员需签名作实。</p>
    <p>3.  若部分物品未能按时移交，请交部门经理或人力资源部经理跟进。</p>
    <p>4.  若离职手续未办理完毕，缓发离职当月工资，手续办理完毕后补发离职当月工资。</p>
</div>
</body>
</html>