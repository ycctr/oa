{extends file="../_base.tpl"}
{block name="css-page"}
    <link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
    <link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
    <link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="header"}
{/block}
{block name="content"}
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>离职申请表--打印</title>
    <link rel="stylesheet" href="bootstrap.min.css"/>
    <script src="bootstrap.min.js"></script>
    <style>
        body{
            width: 990px;
            margin: 20px auto;
            font-size: 12px;
        }
        h2{
            text-align: center;
        }
        input{
            border: none;
        }
    </style>
</head>
<body>
<style>
    @media print {
        .table {
            border-collapse:collapse;
            width: 1000px;

        }
        .table thead,.table tr{
            width: 100%;
            height: 25px;
            border-left:1px solid #eee;

        }
        .table tr th{
            border-left:1px #000 solid;
            border-bottom:1px #000 solid;
            border-right:1px solid #000;
            line-height:26px;
            padding:3px;
        }
        .table td {
            font-size:12px;
            border-right:1px solid #000;
            border-top:1px #000 solid;
            border-left:1px #000 solid;
            line-height:26px;
            padding:3px;
        }
        .navbar {
            height:40px;
            line-height:40px;
        }
        .print {
            display:none;
        }
        .interview_header {
            height: 150px;
        }
        .interview_header ul {
            width: 100%;
        }
        .interview_header ul li {
            float: left;
            width: 30%;
            line-height: 32px;
        }

        .table-bordered {
            border: 1px solid #000;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

    }
</style>
<h2>员工离职申请表</h2>
<button style="float:right;" class="btn btn-success print">打印</button>
<table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4" class="active">员工基本信息 Employee Information</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">
            姓名：<br>
            Chinese Name：{$user.name}<br>
        </th>
        <th>
            员工编号:<br>
            Number of employees：{$user.id_card}<br>
        </th>
    </tr>
    <tr>
        <th>
            所在部门:<br>
            Department：{$section}
        </th>
        <th>
            职位名称:<br>
            Position Title：{$dimission.position}
        </th>
    </tr>
    <tr>
        <th scope="row">
            入职日期:<br>
            Entry Date：{$user.hiredate}
        </th>
        <th>
            直接上级:<br>
            Supervisor：{$manager}
        </th>
    </tr>
    <tr>
        <th scope="row">
            申请离职日期:<br>
            Proposed Separation Date：{$dimission.create_time|date_format:"Y-m-d"}
        </th>
        <td>

        </td>
    </tr>
    </tbody>
</table>
<table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4" class="active">辞职原因 Reason for Resignation</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>
            {$dimission.leave_reason}
            <p style="margin-top: 20px;">
                申请人: {$user.name}
                申请日期: {$dimission.create_time|date_format:"Y-m-d"}
            </p>
        </th>

    </tr>
    </tbody>
</table>
<table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4" class="active">Approval Info. 审批信息</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">直接主管审批:<br>Signature & Date </th>
        <td>
            签字: {$directManager.user_name}

        </td>
        <td>
            日期: {$directManager.modify_time|date_format:"Y-m-d"}

        </td>
    </tr>
    <tr>
        <th scope="row">部门负责人审批:<br>Signature & Date </th>
        <td>
            签字: {$sectionManager.user_name}

        </td>
        <td>
            日期: {$sectionManager.modify_time|date_format:"Y-m-d"}

        </td>
    </tr>
    <tr>
        <th scope="row">人力部门审批:<br>Signature & Date </th>
        <td>
            签字: {$HRD.ext.name}

        </td>
        <td>
            日期: {$HRD.modify_time|date_format:"Y-m-d"}

        </td>
    </tr>
    <tr>
        <th scope="row">主管VP审批:<br>Signature & Date </th>
        <td>
            签字: {$VP.user_name}

        </td>
        <td>
            日期: {$VP.modify_time|date_format:"Y-m-d"}

        </td>
    </tr>
    </tbody>
</table>
<div class="zhu">
    <p>注：员工辞职可采用邮件或填写本申请表形式，请部门经理及分管副总裁批准后交人事部审核存档。</p>
</div>
</body>
</html>
<script>
    $(function(){
        //window.print();
        $('.print').click(function(){
            window.print();
        });
    });
</script>
{/block}