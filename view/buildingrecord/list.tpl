{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">
    <div class="row-fluid">
        {if count($sections)}
        <form action="/teambuildingrecord/list" method="post" style="margin-top:30px;">
            <div style="float:left;width:350px;">
                <label class="control-label" style="float:left;line-height:30px;">选择部门：</label>
                <div class="controls">
                    <select name="section_id">
                        {foreach $sections as $key=>$item}
                        <option {if $section.id == $item.id}selected="selected"{/if} value="{$item.id}">{$item.name}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <input style="margin-bottom:12px;" class="btn btn-success" type="submit" value="search" />
        </form>
        {/if}
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">{$section.name}团建经费流水</div>
            </div>
            <div class="block-content collapse in">
                <div id="liucheng" class="span12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th width="15%">类型</th>
                                <th width="15%">时间</th>
                                <th width="10%">金额</th>
                                <th width="15%">操作人</th>
                                <th>备注</th>
                                <th width="10%">操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach $list as $item}
                            <tr>
                                <td>
                                    {if $item.user_workflow_id}
                                        团建报销
                                    {else if $item.operator_id}
                                        人工充值
                                    {else}
                                        系统充值
                                    {/if}
                                </td>
                                <td>{$item.date}</td>
                                <td>{$item.amount}</td>
                                <td>{$item.user_name}</td>
                                <td>{$item.remark}</td>
                                <td>{if $item.user_workflow_id}<a href="/userworkflow/look?id={$item.user_workflow_id}">详情</a>{/if}</td>
                            </tr>
                            {/foreach}
                        </tbody>
                    </table>
                    <div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
                        <ul>
                        {pager_oa count=$total pagesize=$rn page=$pn pagelink=$url|cat:"pn=%d" list=3}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>

</div>
<script type="text/javascript">
var absenteeismType = {json_encode($absenteeismType)};
{literal}
$(function(){
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
    $('.btn').click(function(){
        var absenteeism_type = $(this).parentsUntil('tr','td').find('[name="absenteeism_type"]').val();  
        var id = $(this).attr('data-value');
        var me = $(this);
        if(absenteeism_type != 0){
            $.ajax({
                url :'/checkinout/edit',
                data : {id:id,absenteeism_type:absenteeism_type},
                type : 'post',
                dataType : 'json',
                success : function(data){
                    if(data.code == 0){
                        me.parentsUntil('tr','td').html(absenteeismType[absenteeism_type]); 
                    } 
                }
            }) 
        }
    });      
});
{/literal}
</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
