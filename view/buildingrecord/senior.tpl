{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}

<style> 

    #myTab .cur {

        border-color:#ddd #ddd #aaa;
        background-color:#ddd;
        border-radius:4px 4px 0 0;
    
    }

    .quota {

        text-align: center;
        width: 30px;
        height:14px;
                
    }

</style>

<div id="content" class="span9">

    <div class="block">

        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">经费配置</div>
        </div>

        <div class="block-content collapse in">
            <ul class="nav nav-tabs" id="myTab">

                {foreach $sectionTab as  $key=>$sectionName}
                <li {if $key== $sectionId}class="cur"{/if}><a  href="?section_id={$key}">{$sectionName}</a></li>
                {/foreach}

            </ul>

            <div >
               从下面部门收集的经费，每月会自动汇总到「{$section}」。 
            </div>

               <br/>
            <div id="liucheng" class="span12">
            <form  class="form-horizontal" id="form_sample_1" method="POST" action="/teambuildingrecord/senior"  >
                <input type="hidden" name="to_section" value="{$sectionId}"/>
                <table class="table table-hover">
                <thead>
                    <tr>
                        <th>部门</th>
                        <th>团建经费(/月)</th>
                        <th>获取比例</th>
                        <th>获取经费</th>
                    </tr>
                </thead>
                <tbody>
                    
                    {foreach $sectionSenior as $id=>$section }
                        <tr>
                            <td>{$section.name}</td>
                            <td id="money_{$id}">{$section.total}</td>
                            <td><input class="quota nput-xlarge required number" type="text" name="quota[{$id}]" maxlength="3" rel="{$id}" value="{$section.quota}" ><span style="margin-left:5px">%</span></td>
                            <td id="modify_money_{$id}">{$section.money}</td>
                        </tr>
                    {/foreach}
                </tbody>
                </table>
                <div>
                    1.经费会按照你所在部门（含下属）总额分配给设定的部门
                </div>
                <div>
                    2.总计必须操作0-100%范围内
                </div>

                <div class="form-actions">
                    <button class="btn btn-primary" type="submit">保存</button>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <button class="btn" type="reset">重置</button>
                </div>
            </form>

    </div>

</div>

<script>

$(function(){

    var tmp_section_id = "";
    var quota = 0;
    var old_quota = 0;

    $('.quota').blur(function(){

       tmp_section_id = $(this).attr("rel");
       quota = $(this).val();

       if(quota >= 0 && quota <= 100 && quota != "") {
            
            var tmp_money = $("#money_"+tmp_section_id).html();
            var tmp_num =  parseInt(quota) * 0.01  * parseInt(tmp_money);
            tmp_num = tmp_num.toFixed(2);
            $("#modify_money_"+tmp_section_id).html(tmp_num);

       
       }else {

            alert("比例设置范围:0-100")
            $(this).val(old_quota);
       
       }


        
    }).focus(function(){

            old_quota = $(this).val();
            
    })


})

</script>


{/block}

{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
