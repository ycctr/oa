{extends file="./_base.tpl"}
{block name='title'}添加用户
{/block}
{block name="css-common"}
<link href="/static/jquery-autocomplete/jquery.autocomplete.css" rel="stylesheet" type="text/css">
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}
<div id="content" class="span9">
<div class="block" style="margin-top:30px;">
 <div class="navbar navbar-inner block-header">
    <div class="muted pull-left">
     用户
    </div>
 </div>
 <div class="block-content collapse in">
    <div class="span12">
        <form method="post" class="form-horizontal" action="add">
            <fieldset>
                <legend>
                    添加用户
                </legend>
                {if (!is_null($data.message.error))}
                    <div class="alert alert-error ">
                        {$data.message.error}
                    </div>
                {/if}
                <div class="control-group">
                    <label class="control-label" for="typeahead" style="margin-right: 15px;">姓名</label>
                    <input id="typeahead"  type="text" name="name" value="{$data.info.name}"></input></br>
                </div>
                <div class="control-group" text-align:center>
                    <label class="control-label" style="margin-right: 15px;">工号</label>
                    <input type="text" name="id_card" value="{$data.info.id_card}"></input></br>
                </div>
                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">员工类型</label>
                    <select  name="type">
                        {foreach from=$type item=item key=key}
                        <option value="{$key}">{$item}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="control-group" text-align:center>
                    <label class="control-label" style="margin-right: 15px;">邮箱</label>
                    <input type="text" name="email" value="{$data.info.email}"></input>@rong360.com</br>
                </div>
                <div class="control-group" text-align:center>
                    <label class="control-label" style="margin-right: 15px;">电话</label>
                    <input type="tel" name="mobile" value="{$data.info.mobile}"></input></br>
                </div>
                <div class="control-group" text-align:center>
                    <label class="control-label" style="margin-right: 15px;">性别</label>
                    {if ($data.info.sex==1)}
                        <input type="radio" name="sex" value="1" checked="checked">男</input>
                        <input type="radio" name="sex" value="0">女</input></br>
                    {else}
                        <input type="radio" name="sex" value="1">男</input>
                        <input type="radio" name="sex" value="0" checked="checked">女</input></br>
                    {/if}
                </div>

                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">部门</label>
                    <input id="sectionId" type="text" />
                    <input type="hidden" name="section_id" value="" />
                </div>
                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">级别</label>
                    <select  name="level">
                        <option value="0" selected="selected">无</option>
                        <option value="{$smarty.const.AUDIT_CEO}">CEO</option>
                        <option value="{$smarty.const.AUDIT_VP}">VP</option>
                        <option value="{$smarty.const.AUDIT_MANAGER}" >部门负责人</option>
                        <option value="{$smarty.const.AUDIT_VICE_MANAGER}">分部门负责人</option>
                        <option value="{$smarty.const.AUDIT_GROUP}">组负责人</option>
                        <option value="{$smarty.const.AUDIT_LITTLE_GROUP}">小组负责人</option>
                    </select>
                </div>
                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">入职时间</label>
                    <div class="input-append date datetimepicker">
                        <input name="hiredate" type="text" value="" old-value="" class="add-on">
                        <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">首次参加工作时间</label>
                    <div class="input-append date datetimepicker">
                        <input name="work_date" type="text" value="" old-value="" class="add-on">
                        <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"></i>
                        </span>
                    </div>
                </div>
                {include "widget/role_list.tpl"}
                <div class="form-actions">
                    <button class="btn btn-primary" type="submit" width="10000">添加 </button>
                    <button class="btn" type="reset">取消</button>
                </div>
            <fieldset>
        </form>
    </div>
 </div>
</div>
</div>
<script type="text/javascript" src="/static/jquery-autocomplete/jquery.autocomplete.min.js"></script>
<script>
var data = [
    {foreach from=$data.sections item=item}
    ['{$item.name} {$item.id}', {$item.id}]{if $item@index < count($data.sections)-1},{/if}
    {/foreach}
];
$(function(){
$('.datetimepicker').datetimepicker({
    format: 'yyyy-MM-dd',
    language: 'zh-CN',
    pickDate: true,
    pickTime: false,
    hourStep: 1,
    minuteStep: 15,
    secondStep: 30,
    inputMask: true,
});

$('.btn-primary').click(function(){
    var work_date = $("input[name='work_date']").val(),
        hiredate  = $("input[name='hiredate']").val(),
        id_card   = $("input[name='id_card']").val();

    if($.trim(work_date)=='' || $.trim(hiredate)=='' || $.trim(id_card)==''){
        alert('请检查所填写的信息是否有遗漏');
        return false;
    }
})

$("#sectionId").result(function(event, data, formatted){ 
    $("#sectionId").val(data[0].split(" ")[0]);
    $("input[name='section_id']").val(data[0].split(" ")[1]);
});
$('#sectionId').autocomplete(data,{
    minChars: 0,
    max:300,
    delay:10,
    matchContains:true,
    multiple:false,
    formatItem: function(row) { return row[0]; }
});

});
</script>
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>
{/block}

