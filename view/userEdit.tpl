{extends file="./_base.tpl"}
{block name='title'}编辑用户{/block}
{block name="css-common"}
<link href="/static/jquery-autocomplete/jquery.autocomplete.css" rel="stylesheet" type="text/css">
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}
<div id="content" class="span9">
<div class="block" style="margin-top:30px;">
 <div class="navbar navbar-inner block-header">
    <div class="muted pull-left">
     用户
    </div>
 </div>
 <div class="block-content collapse in">
    <div class="span12">

        <form method="post" class="form-horizontal" action="editPost">
            <fieldset>
                <legend>
                    用户编辑
                </legend>
                {if (!is_null($data.message.error))}
                    <div class="alert alert-error ">
                        {$data.message.error}
                    </div>
                {/if}
                <div class="control-group" text-align:center>
                    <input type="hidden" name="id" value="{$data.info.id}" ></input></br>
                </div>

                <div class="control-group" >
                    <label class="control-label" style="margin-right: 15px;">工号</label>
                    <input type="text" name="id_card" value="{$data.info.id_card}" /></input></br>
                </div>
                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">员工类型</label>
                    <select  name="type">
                    {foreach from=$type item=item key=key}
                        {if ($data.info.type == $key)}
                            <option value="{$key}" selected="selected">{$item}</option>
                        {else}
                            <option value="{$key}">{$item}</option>
                        {/if}
                    {/foreach}
                    </select></br>
                </div>
                <div class="control-group" text-align:center>
                    <label class="control-label" style="margin-right: 15px;">姓名</label>
                    <input type="text" name="name" value="{$data.info.name}" readonly></input></br>
                </div>
                <div class="control-group" text-align:center>
                    <label class="control-label" style="margin-right: 15px;">邮箱</label>
                    <input type="text" name="email" value="{$data.info.email}"></input>
                    @rong360.com
                    </br>
                </div>
                <div class="control-group" text-align:center>
                    <label class="control-label" style="margin-right: 15px;">电话</label>
                    <input type="tel" name="mobile" value="{$data.info.mobile}"></input></br>
                </div>
                <div class="control-group" text-align:center>
                    <label class="control-label"  style="margin-right: 15px;" disabled>性别</label>
                    {if ( $data.info.sex == 1 ) }
                        <label class="control-label" disabled>男</label>
                        <input type="hidden" name="sex" value="1"></input></br>
                    {else}
                        <label class="control-label" disabled>女</label>
                        <input type="hidden" name="sex" value="0"></input></br>
                    {/if}
                </div>

                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">部门名称</label>
                    <input id="sectionId" type="text" value="{$data.info.section_name}" />
                    <input type="hidden" name="section_id" value="{$data.info.section_id}" />

{*
                    <select  name="section_id">
                    {foreach from=$data.sections item=item key=key}
                        {if ($data.info.section_id == $item.id)}
                            <option value="{$item.id}" selected="selected">{$item.name}</option>
                        {else}
                            <option value="{$item.id}">{$item.name}</option>
                        {/if}
                    {/foreach}
                    </select></br>
                        *}
                </div>
                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">级别</label>
                    <select  name="level">
                        <option value="0" {if $data.info.level eq 0} selected="selected"{/if}>无</option>
                        <option value="{$smarty.const.AUDIT_CEO}" {if $data.info.level eq $smarty.const.AUDIT_CEO} selected="selected"{/if}>CEO</option>
                        <option value="{$smarty.const.AUDIT_VP}" {if $data.info.level eq $smarty.const.AUDIT_VP} selected="selected"{/if}>VP</option>
                        <option value="{$smarty.const.AUDIT_MANAGER}"{if $data.info.level eq $smarty.const.AUDIT_MANAGER} selected="selected"{/if} >部门负责人</option>
                        <option value="{$smarty.const.AUDIT_VICE_MANAGER}"{if $data.info.level eq $smarty.const.AUDIT_VICE_MANAGER} selected="selected"{/if}>分部门负责人</option>
                        <option value="{$smarty.const.AUDIT_GROUP}"{if $data.info.level eq $smarty.const.AUDIT_GROUP} selected="selected"{/if}>组负责人</option>
                        <option value="{$smarty.const.AUDIT_LITTLE_GROUP}"{if $data.info.level eq $smarty.const.AUDIT_LITTLE_GROUP} selected="selected"{/if}>小组负责人</option>
                    </select>
                </div>
                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">入职时间</label>
                    <div class="input-append date datetimepicker">
                        <input name="hiredate" type="text" value="{$data.info.hiredate}" old-value="" class="add-on">
                        <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">首次参加工作时间</label>
                    <div class="input-append date datetimepicker">
                        <input name="work_date" type="text" value="{$data.info.work_date}" old-value="" class="add-on">
                        <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"></i>
                        </span>
                    </div>
                </div>
                
                <div class="control-group" >

                    <label class="control-label" style="margin-right: 15px;">离职</label>
                    <div><input type="checkbox" name="is_leave" {$is_leave} value="2" /></div>
                </div>

                <div class="control-group" id="leave_date" style="{$is_leave_show}">
                    <label class="control-label" style="margin-right: 15px;">离职时间</label>
                    <div class="input-append date datetimepicker">
                        <input name="leave_date" type="text" value="{$data.info.leave_date}" old-value="" class="add-on">
                        <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"></i>
                        </span>
                    </div>
                </div>

                <div class="control-group" id="pay_date" style="{$is_leave_show}">
                    <label class="control-label" style="margin-right: 15px;">结薪时间</label>
                    <div class="input-append date datetimepicker">
                        <input name="pay_date" type="text" value="{$data.info.pay_date}" old-value="" class="add-on">
                        <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"></i>
                        </span>
                    </div>
                </div>

                <input type="hidden" name="create_time" value="{$data.info.create_time}"></input>
                {include "./widget/role_list.tpl"} 
                {if $is_Edit eq 1}
                <div class="form-actions">
                    <button class="btn btn-primary" type="submit" width="10000">修改</button>
                    <button class="btn" type="reset">重置</button>
                </div>
                {/if}
            <fieldset>
        </form>
    </div>
 </div>
</div>
</div>
<script type="text/javascript" src="/static/jquery-autocomplete/jquery.autocomplete.min.js"></script>
<script>
var data = [
    {foreach from=$data.sections item=item}
    ['{$item.name} {$item.id}', {$item.id}]{if $item@index < count($data.sections)-1},{/if}
    {/foreach}
];
$(function(){
$('.datetimepicker').datetimepicker({
    format: 'yyyy-MM-dd',
    language: 'zh-CN',
    pickDate: true,
    pickTime: false,
    hourStep: 1,
    minuteStep: 15,
    secondStep: 30,
    inputMask: true,
});

$('.btn-primary').click(function(){
    var work_date = $("input[name='work_date']").val(),
        hiredate  = $("input[name='hiredate']").val(),
        id_card   = $("input[name='id_card']").val();

    if($.trim(work_date)=='' || $.trim(hiredate)=='' || $.trim(id_card)==''){
        alert('请检查所填写的信息是否有遗漏');
        return false;
    }
})

});

$("input[name='is_leave']").click(function(){

        var is_check = $(this).is(":checked");

        if (is_check == true) {

            $("#leave_date").show();
            $("#pay_date").show();

        } else {

            $("#leave_date").hide();
            $("#pay_date").hide();

        }
});
$("#sectionId").result(function(event, data, formatted){ 
    $("#sectionId").val(data[0].split(" ")[0]);
    $("input[name='section_id']").val(data[0].split(" ")[1]);
});
$('#sectionId').autocomplete(data,{
    minChars: 0,
    max:300,
    delay:10,
    matchContains:true,
    multiple:false,
    formatItem: function(row) { return row[0]; }
});
</script>
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>
{/block}

