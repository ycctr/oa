{extends file="./_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
{/block}
{block name='title'}午餐变更导出
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}
 
<div id="content" class="span9">
<div>
    <form  method="get" action="" style="margin-bottom:0px">
        <fieldset>
        <div class="control-group success">
            <div class="controls" style="margin-top:15px;">
                日期
                <div class="input-append date datetimepicker">
                    <input class="add-on" name="query[start_time]" placeholder="起始日期" type="text" value="{if $smarty.get.query.start_time eq null}{$initTime.start_time}{else}{$smarty.get.query.start_time}{/if}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                                    到
                <div class="input-append date datetimepicker">
                    <input class="add-on" name="query[end_time]" placeholder="结束日期" type="text" value="{if $smarty.get.query.end_time eq null}{$initTime.end_time}{else}{$smarty.get.query.end_time}{/if}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="control-group">
            部门<input type="text" name="query[section_name]" value="{$smarty.get.query.section_name}" style="margin-left:15px; margin-right:15px;"></input>
            姓名<input type="text" name="query[name]" value="{$smarty.get.query.name}"style="margin-left:15px;"></input>
            &nbsp;&nbsp;&nbsp;<button class="btn btn-success" type="submit" style="margin-bottom:10px; background: none repeat scroll 0% 0% rgb(33, 122, 237);padding-left: 19px; padding-right: 19px;border-left-width:3px;border-right-width:3px;">查询</button>
        </div>
        </fieldset>
    </form>
</div>

<div class="block">

 <div class="navbar navbar-inner block-header">
    <div class="muted pull-left">
     午餐变更
    </div>
</div>
 <div class = "block-content collapse in">
    <div class="span12">
    <div class="table-toolbar" style="margin-bottom:18px;">
        <div class="btn-group">
            <a href="{$savelink}">
                <button class="btn btn-success">导出表格
                </button>
            </a>
        </div>
    </div>

        {include file="./widget/lunch_change_list.tpl"}
        <div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
            <ul>
                {pager_oa count=$arrPager.count pagesize=$arrPager.pagesize page=$arrPager.page pagelink=$arrPager.pagelink list=3}
            </ul>
        </div>
    </div>
</div>
</div>
<script>
$(function() {
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
});
</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>
{/block}
