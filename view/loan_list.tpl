{extends file="./_base.tpl"}
{block name="content"}
{include file="./widget/left-nav.tpl"}
<div id="content" class="span9">
    <ul class="nav nav-tabs" id="myTab">
        <li {if $status == 0}style="border-color: #eee #eee #ddd;background-color: #eee;border-radius: 4px 4px 0 0;"{/if}><a href="/userworkflow/manageLoan?status=0">未冲抵（不包括人事类）</a></li>
        <li {if $status == 1}style="border-color: #eee #eee #ddd;background-color: #eee;border-radius: 4px 4px 0 0;"{/if}><a href="/userworkflow/manageLoan?status=1">已冲抵</a></li>
         <li {if $status == 2}style="border-color: #eee #eee #ddd;background-color: #eee;border-radius: 4px 4px 0 0;"{/if}><a href="/userworkflow/manageLoan?status=2">人事类</a></li>
    </ul>
    <div class="row-fluid">
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">借款管理</div>
            </div>
            <div class="block-content collapse in">
                <div id="liucheng" class="span12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>借款时间</th>
                                <th>发起人</th>
                                <th>借款金额</th>
                                {if $status==1}
                                <th>冲抵时间</th>
                                {/if}
                                <th>借款单</th>
                                {if $status==1}
                                <th>借款报销单</th>
                                {/if}
                            </tr>
                        </thead>
                        <tbody>
                            {foreach $list as $item}
                            <tr>

                                <td>{$item.modify_time|date_format:"%Y-%m-%d"}</td>
                                <td>{$item.name}</td>
                                <td>{$item.amount}</td>
                                {if $status==1}
                                <td>{$item.finish_time|date_format:"%Y-%m-%d"}</td>
                                {/if}
                                <td><a href="look?id={$item.loan_uwf_id}">查看</a></td>
                                {if $status==1}
                                <td><a href="look?id={$item.pay_uwf_id}">查看</a></td>
                                {/if}
                            </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>

</div>
{/block}
