{extends file="./_base.tpl"}
{block name='title'}部门费用审批权限编辑
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}
<div id="content" class="span9">
    <style type="text/css">
    .controls{
        padding-top: 5px;
    }
    </style>
    <div class="block" style="margin-top:30px;">
         <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
             部门费用审批权限编辑
            </div>
         </div>
         <div class="block-content collapse in">
            <div class="span12">
                <form method="post" class="form-horizontal" action="editMoney">
                    <fieldset>
                        <div class="control-group">
                            <input type="hidden" name="section_id" value="{$section.id}" ></input>
                        </div>
                        <div class="control-group" text-align:center>
                            <label class="control-label" style="margin-right: 15px;">部门名称</label>
                            <div class="controls">{$section.name}</div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" style="margin-right: 15px;">借款审批金额</label>
                            <input type="text" name="loan" value="{$money}"></input></br>
                        </div>

                        <div class="form-actions">
                            <button class="btn btn-primary" type="submit" width="10000">修改</button>
                            <button class="btn" type="reset">重置</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
{/block}
