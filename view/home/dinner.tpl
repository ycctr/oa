{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<style>
.dinner_btn{
    background-color:#ff921c !important;
}
.dinner_btn:hover{
    background-color:#fc9f39 !important;
}
</style>
<div id="content" class="span9">

    <div class="row-fluid">
        <form action="/home/addDinner" method="post" id="dinner" onSubmit="return false" style="margin-top:30px;{$display}">
            <span style="margin-right:10px;float:left;line-height:30px">办公区:</span>
            <div class="input-append date datetimepicker" style="float:left;">
                 <select id="office">
                    <option value="0">请选择办公区</option>
                    {foreach $office as $key=>$val}
                       <option {if $key==$index }selected {/if}  value="{$key}">{$val}</option>
                        
                    {/foreach}
                </select>
            </div>
            <div class="input-append date datetimepicker">
            </div>

            {if ($undinner != 1)}

            <input style="margin: 0px 0px 12px 20px;float:left" onclick="post_dinner()" class="btn btn-success" type="submit" value="订  餐" />
            <span style="margin:0px 0px 12px 40px;float:left;line-height:30px">注意:每天最后截至订餐时间下午{$dinnerHour}点。</span>

            {else}

            <button style="margin: 0px 0px 12px 20px;float:left" class="btn btn-error" onclick="javascript:alert('对不起，订餐时间已过！')">订  餐</button>
            <span style="margin:0px 0px 12px 40px;float:left;line-height:30px">注意:每天最后截至订餐时间下午{$dinnerHour}点。</span>

            {/if}

        </form>
    </div>

    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">订餐记录</div>
        </div>
        <div class="block-content collapse in">
            <div id="liucheng" class="span12">
                <div class="dataTables_paginate paging_bootstrap pagination" style="margin: 0px 0px 0px 10px;">
                    <ul>
                        <li>
                            <a href="/export/dinner" class="dinner_btn" style="border:0px; color:#fff">导出全部</a>
                        </li>
                    </ul>
                </div>
                <table class="table table-hover">
                    <thead>
                        <tr >
                            <th width="8%">员工号</th>
                            <th width="8%">姓名</th>
                            <th width="8%">邮箱</th>
                            <th width="14%">点餐区</th>
                            <th width="20%">订餐时间</th>
                        </tr>
                    </thead>

                    <tbody id="dinner_list">

                        {if ($display != "") }
                        <tr style="color: #08c;">
                            <td>{$self.id_card}</td>
                            <td>{$self.name}</td>
                            <td>{$self.email}@rong360.com</td>
                            <td>{$office[$self.office]}</td>
                            <td>{$self.create_time|date_format:"%H:%M"}</td>
                        </tr>
                        {/if}

                    {if (!empty($users) )}
                        {foreach $users as $user}
                        <tr>
                            <td>{$user.id_card}</td>
                            <td>{$user.name}</td>
                            <td>{$user.email}@rong360.com</td>
                            <td>{$office[$user.office]}</td>
                            <td>{$user.create_time|date_format:"%H:%M"}</td>
                        </tr>
                        {/foreach}
                    {/if}

                    </tbody>
                </table>
                <div class="dataTables_paginate paging_bootstrap pagination" style="margin: 0px 0px 0px 10px;">
                    <ul>
                        <li>总共：</li>
                        <li id="dinner_total">{$total}</li>
                        <li>条</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /block -->

</div>
<script type="text/javascript">

function post_dinner() {

    var office = $("#office").val();
    if(office <= 0) {

        alert("请选择办公地点!");
        return false;
    
    }
    check_loading.show();

    $.ajax({

        url: '/home/addDinner?office='+office,    
        dataType: 'json',
        success:function(res){

            if(res.code == 1){

                var dinnerData = res.data;

                var html = "<tr style='color: #08c;'><td>"+dinnerData.id_card+"</td><td>"+dinnerData.name+"</td><td>"+dinnerData.email+"@rong360.com</td><td>"+dinnerData.office+"</td><td>"+dinnerData.create_time+"</td></tr>";

                $("#dinner_list").prepend(html);
                var total = 1+parseInt($("#dinner_total").html());
                $("#dinner_total").html(total);

                $("#dinner").animate(
                {
                    opacity: 'toggle'
                }, 500); 

            }else if(res.code == -1) {

                alert("操作失败，请刷新页面！");
            
            }else if(res.code == -2) {

                alert("操作失败，订餐时间已经过！");
            
            }


            setTimeout(function(){
                check_loading.hide();
            }, 500);

        }

    }) 

}



</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
