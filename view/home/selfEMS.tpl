{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<style>
.dinner_btn{
    background-color:#ff921c !important;
}
.dinner_btn:hover{
    background-color:#fc9f39 !important;
}

#page_list li{
list-style:none;  
float:left;  
border:1px solid #ddd;
}

#page_list li a{
padding: 4px 12px;
}

#page_list li span{
padding: 4px 12px;
}

#page_list .active {

    background-color:#eee;

}

</style>
<div id="content" class="span9">

    <div >
        <div style="color: #FF8600;font-family: 微软雅黑;font-size: 16px;"><h3>注意：</h3>修改或删除明细，请联系各个办公区前台。</div>
    </div>
    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">

            <div class="muted pull-left">快递记录</div>

            <div class="muted pull-right">
                <input class="addRow" data-type="taxi" type="button" value="添加明细"> 
            </div>

        </div>
        <div class="block-content collapse in">

            <div id="liucheng" class="span12">

                <table class="table table-hover">

                    <thead>
                        <tr>
                            <th width="15%">日期</th>
                            <th width="15%">办公区</th>
                            <th width="10%">物品类型</th>
                            <th width="15%">收件人或公司</th>
                            <th width="10%">寄往城市</th>
                            <th width="10%">快递名称</th>
                            <th width="15%">快递单号</th>
                        </tr>
                    </thead>

                    <tbody id="add_express" > 

                    </tbody>

                    <tbody id="express_list">

                        {foreach $userExpress as $data}
                        <tr >
                            <td>{$data.create_time|date_format:"%Y-%m-%d"}</td>
                            <td>{$officeConf[$data.office]}</td>
                            <td>{$goodsConf[$data.goods_type]}</td>
                            <td>{$data.to_where}</td>
                            <td>{$data.to_city}</td>
                            <td>{$expressAllConf[$data.express_type]}</td>
                            <td>{$data.express_num}</td>
                        </tr>
                        {/foreach}

                        <tr>
                            <td style="border-top:2px solid #ddd;"><a>总计：&nbsp;<strong id="express_total">{$total}</strong>&nbsp;条</a></td>
                            <td style="border-top:2px solid #ddd;" colspan=6>
                            <ul id="page_list">
                {pager_oa count=$total pagesize=$per page=$pageNo pagelink="/home/selfems?pageNo=%d" list=3}
                            </ul>
                            </td>
                        </tr>

                    </tbody>

        <tbody id="add_express_copy" style="display:none;"> 

             <tr style="color: #08c;">

                 <td>{$cur_date}</td>
                 <td>
                     <select name="express['office']" id="office" style="width:110px">
                         <option value="0">选择办公区</option>
                         {foreach $officeConf as $key=>$office }
                            <option value="{$key}">{$office}</option>
                         {/foreach}
                 
                     </select>
                 </td>
                 <td>
                     <select name="express['goods_type']" id="goods_type" style="width:110px">
                         <option value="0">选择类型</option>
                         {foreach $goodsConf as $type=>$goods }
                            <option value="{$type}">{$goods}</option>
                         {/foreach}
                 
                     </select>
                 </td>
                 <td><input type="text" name="express['to_where']" id="to_where" style="width:50px"/></td>
                 <td><input type="text" name="express['to_city']" id="to_city" value="" style="width:50px"/></td>
                 <td>
                     
                     <select name="data['express_type']" id="express_type" style="width:100px">

                         <option value="0">选择快递</option>
                         {foreach $expressConf as $type=>$name }
                            <option value="{$type}">{$name}</option>
                         {/foreach}
                 
                     </select>

                 </td>
                 <td><input style="width:140px" type="text" id="express_num" name="express['express_num']"  value=""  /> </td>
                 <td><input type="submit" value="保存" id="express_submit" class="btn btn-success" /></td>

             </tr>
                        
        </tbody>
    </table>

            <!-- <div class="dataTables_paginate paging_bootstrap pagination" style="margin: 0px 0px 0px 10px;"> -->
            <!--         <ul> -->
            <!--         <li><a>总计：&#38;nbsp;<strong id="express_total">{$total}</strong>&#38;nbsp;条</a></li> -->
            <!--     {pager_oa count=$total pagesize=$per page=$pageNo pagelink="/home/selfems?pageNo=%d" list=3} -->
            <!--         </ul> -->
            <!--     </div> -->
            <!-- </div> -->
</div>
    </div>
    <!-- /block -->

</div>
<script type="text/javascript">

$(".addRow").click(function(){
        
    var add_express = $("#add_express_copy").html();
    $("#add_express").html(add_express).show();
        
        
})

$("#add_express").delegate("#express_submit","click",function(){

        if(!confirm("确定要保存快递单吗？")){ 

            return false;

        }
        
        var express_num  = $("#add_express #express_num").val();
        var office =  $("#add_express #office").val();
        var express_type =  $("#add_express #express_type").val();
        var goods_type   =  $("#add_express #goods_type").val();
        var to_city      =  $("#add_express #to_city").val();
        var to_where     =  $("#add_express #to_where").val();

        check_loading.show();
        

        $.ajax({
            url: '/home/addExpress',    
            type:"POST",
            data:{

                express:{
                    "express_num"  : express_num, 
                    "office"       : office,
                    "express_type" : express_type,
                    "goods_type"   : goods_type,
                    "to_where"     : to_where,
                    "to_city"      : to_city 
                }

            },
            dataType: 'json',
            success:function(res){

                var code = res.code;

                if(code == -1)  {

                    alert(res.msg);

                    setTimeout(function(){
                            check_loading.hide();
                        }, 500);
                
                } else {

                   ajaxSelfTable();
                
                }


            },
            error:function(){
                  
                alert("操作失败!");
                setTimeout(function(){
                        check_loading.hide();
                    }, 500);
            }
        }) 
        
    
    })


function ajaxSelfTable() {

    $.ajax({
        url: '/home/ajaxEms',    
        type:"POST",
        data:{
            "user_id"  : "{$userInfo.id}", 
            "pageNo"   : 1  
        },
        dataType: 'html',
        success:function(res){

            $("#express_list").html(res);            
            $("#express_list ").find("tr:first").css("color", "#0077cc");
            $("#add_express").html("").hide();

            setTimeout(function(){
                    check_loading.hide();
                }, 500);

        },
        error:function(){

                alert("操作失败!");
                setTimeout(function(){
                        check_loading.hide();
                        }, 500);
            }
    }) 


}


</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
