{extends file="../_base.tpl"}
{block name='title'}编辑快递单
{/block}
{block name="content"} 
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">
<div class="block" style="margin-top:30px;">
 <div class="navbar navbar-inner block-header">
    <div class="muted pull-left">
     
    </div>
 </div>
 <div class="block-content collapse in">
    <div class="span12">

        <form method="post" class="form-horizontal" action="emsEdit">
            <fieldset>
                <legend>
                    快递单编辑
                </legend>
                {if (!empty($msg))}
                    <div class="alert alert-error ">
                        {$msg}
                    </div>
                {/if}

                <input type="hidden" name="id" value="{$expressInfo.id}" ></input>
                <input type="hidden" name="express[id]" value="{$expressInfo.id}" ></input>
                <input type="hidden" name="is_post" value="1" ></input></br>
                <input type="hidden" name="express[user_id]" value="{$expressInfo.user_id}">

                <div class="control-group" >
                    <label class="control-label" style="margin-right: 15px;">快递单号:</label>
                    <input type="text" name="express[express_num]" value="{$expressInfo.express_num}" /></input></br>
                </div>
                <div class="control-group" text-align:center>
                    <label class="control-label" style="margin-right: 15px;">姓名:</label>
                    <span style="margin-left:30px;" >王亮</span>
                </div>

                <div class="control-group" >
                    <label class="control-label" style="margin-right: 15px;">收件人或公司:</label>
                    <input type="text" name="express[to_where]" value="{$expressInfo.to_where}" /></input></br>
                </div>

                <div class="control-group" >
                    <label class="control-label" style="margin-right: 15px;">城市:</label>
                    <input type="text" name="express[to_city]" value="{$expressInfo.to_city}" /></input></br>
                </div>

                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">办公区:</label>
                    <select  name="express[office]">
                         {foreach $officeConf as $type=>$office }

                            {if $type == $expressInfo.office }
                                <option selected value="{$type}">{$office}</option>
                            {else}
                                <option value="{$type}">{$office}</option>
                            {/if }

                         {/foreach}
                    </select></br>
                </div>

                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">物品类型:</label>
                    <select  name="express[goods_type]">
                         {foreach $goodsConf as $type=>$goods }

                            {if $type == $expressInfo.goods_type }
                                <option selected value="{$type}">{$goods}</option>
                            {else}
                                <option value="{$type}">{$goods}</option>
                            {/if }

                         {/foreach}
                    </select></br>
                </div>
                
                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">快递名称:</label>
                    <select  name="express[express_type]">

                         {foreach $expressConf as $type=>$name }
                            {if $type == $expressInfo.express_type }
                                <option selected value="{$type}">{$name}</option>
                            {else}
                                <option value="{$type}">{$name}</option>
                            {/if }
                         {/foreach}

                    </select></br>
                </div>


                <div class="form-actions">
                    <button class="btn btn-primary" type="submit" width="10000">修改</button>
                    <button class="btn" type="reset">重置</button>
                </div>
            <fieldset>
        </form>
    </div>
 </div>
</div>
</div>
<script>
$(function(){
$('.datetimepicker').datetimepicker({
    format: 'yyyy-MM-dd',
    language: 'zh-CN',
    pickDate: true,
    pickTime: false,
    hourStep: 1,
    minuteStep: 15,
    secondStep: 30,
    inputMask: true,
});

});

$("input[name='is_leave']").click(function(){

        var is_check = $(this).is(":checked");

        if (is_check == true) {

            $("#leave_date").show();
            $("#pay_date").show();

        } else {

            $("#leave_date").hide();
            $("#pay_date").hide();

        }
});

</script>
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>
{/block}

