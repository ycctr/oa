{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}

<style>

.dinner_btn{
    background-color:#ff921c !important;
}

.dinner_btn:hover{
    background-color:#fc9f39 !important;
}

#page_list li{

    list-style:none;  
    float:left;  
    border:1px solid #ddd;

}

#page_list li a{
    padding: 4px 12px;
}

#page_list li span{
    padding: 4px 12px;
}

#page_list .active {

    background-color:#eee;

}

</style>
<div id="content" class="span9">

    <!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">

            <div class="muted pull-left">快递管理</div>

        </div>
        <div class="block-content collapse in">


            <div id="liucheng" class="span12">

                {if (empty($msg) )}

                    <div class="alert alert-error" style="display:none">


                {else}

                    <div class="alert alert-error" >

                {/if}
                    {$msg}
                </div> 

                <div style="float:left ; margin-left: 30px;" class="input-append date datetimepicker">
                    <input id="start_date"  class="add-on" name="query[start_time]" placeholder="起始日期" type="text" value="{$start_date}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                    
                </div>
                <div style="float:left ; margin-left: 30px;" class="input-append date datetimepicker">
                    <input id="end_date" class="add-on" name="query[end_time]" placeholder="截止日期" type="text" value="{$end_date}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                    
                </div>

                <div style="float:left; margin:0px 0px 30px 20px;">

                     <select name="express['office']" id="office" style="width:110px">
                         <option value="0">所有办公区</option>
                         {foreach $officeConf as $key=>$val }
                            {if $key == $office }
                            <option selected value="{$key}">{$val}</option>
                            {else}
                            <option value="{$key}">{$val}</option>
                            {/if}
                         {/foreach}
                     </select>
                    
                </div>

                <div style="float:left; margin:0px 0px 30px 20px;">
                    <input id="condition" style="width:120px;" name="condition" value="{$condition}" placeholder="员工名或订单号" type="text"> 
                </div>

                <div class="dataTables_paginate paging_bootstrap pagination" style=" float:left; margin:0px 0px 30px 20px;">
                    <ul>
                        <li>
                            <a href="#" onclick="jump_date();" class="dinner_btn" style="border:0px; color:#fff">查询</a>
                        </li>
                    </ul>
                </div>

                <div class="table-toolbar" style="margin-bottom:18px; float:right">
                    <div class="btn-group">
                        <a href="#">
                            <button class="btn btn-success" onclick="export_express();">导出</button>
                        </a>
                    </div>
                </div>

                <table class="table table-hover">

                    <thead>
                        <tr>
                            <th width="10%">日期</th>
                            <th width="6%">发件人</th>
                            <th width="8%">部门</th>
                            <th width="5%">业务线</th>
                            <th width="5%">办公区</th>
                            <th width="8%">物品类型</th>
                            <th width="15%">收件人或公司</th>
                            <th width="8%">寄往城市</th>
                            <th width="8%">快递名称</th>
                            <th width="15%">快递单号</th>
                            <th width="15%">操作</th>
                        </tr>
                    </thead>

                    <tbody id="express_list">

                        
                    {if (!empty($userExpress) )}
                        {foreach $userExpress as $data}
                        <tr >
                            <td>{$data.create_time|date_format:"%m/%d"}</td>
                            <td>{$data.name}</td>
                            <td>{$data.section_name}</td>
                            <td>{$data.team_name}</td>
                            <td>{$officeConf[$data.office]}</td>
                            <td>{$goodsConf[$data.goods_type]}</td>
                            <td>{$data.to_where}</td>
                            <td>{$data.to_city}</td>
                            <td>{$expressAllConf[$data.express_type]}</td>
                            <td>{$data.express_num}</td>
                            <td>
                                 <a href="emsEdit?id={$data.id}">编辑</a>
                                 |<a class="del" href="emsDel?id={$data.id}" num="{$data.express_num}" nickname="{$data.nickname}" >删除</a> 
                            </td>
                        </tr>
                        {/foreach}
                    {/if}

                        <tr>
                            <td colspan="2"><a>总计：&nbsp;<strong id="express_total">{$total}</strong>&nbsp;条</a></td>
                            <td colspan="9">
                                <ul id="page_list">
                                {pager_oa count=$total pagesize=$per page=$pageNo pagelink="/home/emsmanager?pageNo=%d&cur_date=$cur_date&office=$office&condition=$condition" list=3}
                                </ul>
                            </td>
                        </tr>

                    </tbody>

    </table>

</div>
    </div>
    <!-- /block -->

</div>
<script type="text/javascript">

$(function() {
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
});

function jump_date() {

    var start_date = $("#start_date").val();
    var end_date   = $("#end_date").val();
    var condition = $("#condition").val();
    var office = $("#office").val();
    location.href = "/home/emsmanager?office=" + office + "&start_date="+start_date+"&end_date="+end_date+"&condition="+condition; 

}


function export_express() {

    var start_date = $("#start_date").val();
    var end_date   = $("#end_date").val();
    var office = $("#office").val();
    location.href = "/export/express?office=" + office + "&start_date="+start_date+"&end_date="+end_date; 

}

    $(function(){

        $("#express_list").delegate(".del","click",function(){
            var num = $(this).attr("num");  
            var nickname = $(this).attr("nickname");
            if(!confirm("您确定要删除"+nickname+"("+num+")订单？")){
                return false;
            }

            check_loading.show();

        });

    })

</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
