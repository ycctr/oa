{extends file="./_base.tpl"}
{block name="content"}
{include file="./widget/left-nav.tpl"}
<div id="content" class="span9">
    <ul class="nav nav-tabs" id="myTab">
        <li style="border-color: #eee #eee #ddd;background-color: #eee;border-radius: 4px 4px 0 0;">
            <a>审核中流程</a>
        </li>
    </ul>
    <div class="row-fluid">

        <form method="GET" action="/userworkflow/superadmin" >
        <input type="hidden" name="check_flag" value="1">
        <input type="hidden" name="pn" value="1">
        <select name="obj_type" id="obj_type" >
            <option value="0">全部流程</option>
            {foreach $obj_type_list as $key => $val}
                <option {if $key==$obj_type}selected{/if}  value="{$key}">{$val}</option>
            {/foreach}
        </select>

        <input type="text" name="name" placeholder="申请姓名或工号" value="{$name}" />

        <input type="text" name="check_name" placeholder="审核人姓名或工号" value="{$check_name}" />

        <input type="submit" class="btn btn-primary" value="搜索" style="margin-left:30px">

        </form>

        {if $msg != ""}
            <div class="alert alert-error hide" style="display: block;">
            <button class="close" data-dismiss="alert"></button>
                {$msg}            
            </div>
        {/if}

        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">审核中的流程</div>
            </div>
            <div class="block-content collapse in">
                {if (!is_null($userWorkflows))}

                <div id="liucheng" class="span12">
                    {include file="./widget/superadmin_content.tpl"}
                </div>

                {/if}
            </div>
        </div>
        <!-- /block -->

    </div>

</div>
<script type="text/javascript">
// $(function(){
//     $('select').change(function(){
//         var audit_status = $(this).val();   
//         $.ajax({
//             url : '/userworkflow/my/?audit_status='+audit_status,
//             dataType: 'html',
//             success:function(html){
//                 $('#liucheng').html(html);
//             }
//         });
//     });
// });
</script>
{/block}
