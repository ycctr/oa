{extends file="./_base.tpl"}
{block name="content"}
{include file="./widget/left-nav.tpl"}
<div id="content" class="span9">
    <ul class="nav nav-tabs" id="myTab">
        <li {if $status == 0}style="border-color: #eee #eee #ddd;background-color: #eee;border-radius: 4px 4px 0 0;"{/if}><a href="/hr/listDimission?status=0">离职中</a></li>
        <li {if $status == 1}style="border-color: #eee #eee #ddd;background-color: #eee;border-radius: 4px 4px 0 0;"{/if}><a href="/hr/listDimission?status=1">已审批</a></li>
    </ul>
    <div class="row-fluid">
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">离职管理</div>
            </div>
            <div class="block-content collapse in">
                <div id="liucheng" class="span12">
                    {include file="./widget/dimission_content.tpl"}
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>

</div>
{/block}
