{extends file="./_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="./widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li class="active">详情</li>
            </ul>
        </div>
    </div>

    <style>
        .ba{
            width: 80%;
            margin-left: 70px;
        } 
        .ba .control-group{
            height: 30px;
        }
        .ba .control-label{
            width: 10%;
            height: 30px;
            line-height: 30px;
            float: left;
        }
        .ba .controls{
            width: 80%;
            height: 30px;
            line-height: 30px;
        }
        .dl-horizontal {
            padding-bottom:15px;
            border-bottom:1px #ddd solid;
            margin-bottom:30px;
        }
        .dl-horizontal dt,.dl-horizontal dd {
            font-weight:normal;
            line-height:20px;
            margin-bottom:20px;
        }
        .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:70px;
        }
        .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:200px;
            text-align:left;
        }
        .form-actions {
            border:0px;
            background:#fff;
        }
        .btn {
            width:100px;
            margin-right:30px;
        }
        .block-content h4 {
            padding-left:70px;
        }
        .form-actions {
            padding-left:85px;
        }
      /*  .table td {
            border:0px;
            line-height:40px;
        }*/
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block" style="border:0px;">
            <div class="form-actions">
                <button type="button" value="{$smarty.const.OA_TASK_TONGGUO}" class="btn btn-primary">
                    {if $userTask['obj_type'] == 'entry' && ($userTask['audit_desc'] == 'interviewer' || $userTask['audit_desc'] == 'hr')}
                        提交评估
                    {elseif $userTask['role_name'] == '票务专员' && $extension.status == 1}
                        确认订票
                    {elseif $userTask['role_name'] == '票务专员' && $extension.status == 2}
                        确认退票
                    {elseif $userTask['role_name'] == '票务专员' && $extension.status == 3}
                        确认改签
                    {else}
                        同意
                    {/if}
                </button>
                {if $userTask['obj_type'] == 'entry' && ($userTask['audit_desc'] == 'interviewer' || $userTask['audit_desc'] == 'hr')}
                {elseif $userTask['role_name'] == '票务专员' && $extension.status == 1}
                    <button type="button" value="{$smarty.const.OA_TASK_JUJUE}" class="btn reject">取消订票</button>
                {elseif $userTask['role_name'] == '票务专员' && $extension.status == 2}
                    <button type="button" value="{$smarty.const.OA_TASK_JUJUE}" class="btn reject">取消退票</button>
                {elseif $userTask['role_name'] == '票务专员' && $extension.status == 3}
                    <button type="button" value="{$smarty.const.OA_TASK_JUJUE}" class="btn reject">取消改签</button>
                {else}
                    <button type="button" value="{$smarty.const.OA_TASK_JUJUE}" class="btn reject">不同意</button>
                {/if}
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form action="/usertask/audit" method="post">
                    <input type="hidden" name="user_task_id" value="{$userTask.id}" />
                    <input id="audit_status" type="hidden" name="audit_status" value="" />
                    <fieldset>
                        <div class="alert alert-error hide">
                            <button class="close" data-dismiss="alert"></button>
                            你的表单填写有错误，请检查!
                        </div>
                        <h3 style="text-align:center;">
                            {if $extension['type'] =='13'} 
                                出差 
                            {else}
                                {$workflow_type[$userTask.obj_type]}
                            {/if}
                        </h3>
                        <br/>
                        <br/>
                        <div class="title">
                            <label style="width:100px;" for="">姓名：{$userTask.user_name}</label>
                            {if !empty($sections)}
                            <label style="width:auto;">部门：
                                {foreach from=$sections item=section name=foo}
                                    {if $smarty.foreach.foo.last}
                                        {$section.name}
                                    {else}
                                        {$section.name} /
                                    {/if}
                                {/foreach}
                            </label>
                            {else}
                            <label for="" style="width:auto;">部门：{$userTask.section_name}</label>
                            {/if}
                            <label for="" style="padding-left:20px;">日期：{$userTask.create_time|date_format:"%Y-%m-%d"}</label>
                        </div>
                        {if $userTask['obj_type'] == 'absence' && $extension['type'] =='13' }
                        <dl class="dl-horizontal">
                            <dt>出差</dt>
                            <dd>{$absenceType[$extension.type]}</dd>
                            <dt>日期</dt>
                            <dd>{$extension.start_time|date_format:"%Y-%m-%d"} {$timeNode[date('H:i:s',$extension.start_time)]} 到 {$extension.end_time|date_format:"%Y-%m-%d"} {$timeNode[date('H:i:s',$extension.end_time)]}</dd>
                            <dt>出差天数</dt>
                            <dd>{$extension.day_number|string_format:"%.1f"}天</dd>
                            {if $extension.city != ""}

                                <dt>出差地址</dt>
                                <dd>{$extension.city}</dd>

                            {/if }
                            <dt>出差原因</dt>
                            <dd>{$extension.leave_reason}</dd>
                            <dt>备注</dt>
                            <dd>
                                <textarea class="input-xlarge required" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                            </dd>
                        </dl>
                        {elseif $userTask['obj_type'] == 'absence' && $extension['type'] =='25' }
                            <dl class="dl-horizontal">
                                <dt>外出类型</dt>
                                <dd>{$absenceType[$extension.type]}</dd>
                                <dt>日期</dt>
                                <dd>{$extension.start_time|date_format:"%Y-%m-%d"} {$timeNode[date('H:i:s',$extension.start_time)]} 到 {$extension.end_time|date_format:"%Y-%m-%d"} {$timeNode[date('H:i:s',$extension.end_time)]}</dd>
                                <dt>外出天数</dt>
                                <dd>{$extension.day_number|string_format:"%.1f"}天</dd>
                                {if $extension.city != ""}

                                    <dt>外出地址</dt>
                                    <dd>{$extension.city}</dd>

                                {/if }
                                <dt>外出原因</dt>
                                <dd>{$extension.leave_reason}</dd>
                                <dt>备注</dt>
                                <dd>
                                    <textarea class="input-xlarge required" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea>
                                </dd>
                            </dl>
                        {elseif $userTask['obj_type'] == 'absence'}
                        <dl class="dl-horizontal">
                            <dt>请假类别</dt>
                            <dd>{$absenceType[$extension.type]}</dd>
                            <dt>日期</dt>
                            <dd>{$extension.start_time|date_format:"%Y-%m-%d"} {$timeNode[date('H:i:s',$extension.start_time)]} 到 {$extension.end_time|date_format:"%Y-%m-%d"} {$timeNode[date('H:i:s',$extension.end_time)]}</dd>
                            <dt>请假天数</dt>
                            <dd>{$extension.day_number|string_format:"%.1f"}天</dd>
                            <dt>请假原因</dt>
                            <dd>{$extension.leave_reason}</dd>
                            <dt>备注</dt>
                            <dd>
                                <textarea class="input-xlarge required" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                            </dd>
                        </dl>
                        {elseif $userTask['obj_type'] == 'daily_reimbursement'}
                            {include file="./widget/dailyreimbursement/content.tpl"}
                            {if $userTask['workflow_step'] == 'B'}
                            <dl class="dl-horizontal">
                                <dt style="width:110px;">应付金额</dt>
                                <dd style="margin-left:140px;">
                                    <input type="text" name="amount_check" style="width: 375px;" />
                                </dd>
                            </dl>
                            {/if}
                            <dl class="dl-horizontal">
                                <dt style="width:110px;">备注</dt>
                                <dd style="margin-left:140px;">
                                    <textarea class="input-xlarge required" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                                </dd>
                            </dl>
                        {elseif $userTask['obj_type'] == 'business_advance'}
                            {include file="./widget/businessadvance/content.tpl"}
                            <dl class="dl-horizontal">
                                <dt style="width:110px;">备注</dt>
                                <dd style="margin-left:140px;">
                                    <textarea class="input-xlarge required" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                                </dd>
                            </dl>
                        {elseif $userTask['obj_type'] == 'business_reimbursement'}
                            {include file="./widget/businessreimbursement/content.tpl"}
                            {if $userTask['workflow_step'] == 'B'}
                            <dl class="dl-horizontal">
                                <dt style="width:110px;">应付金额</dt>
                                <dd style="margin-left:140px;">
                                    <input type="text" name="amount_check" style="width: 375px;" />
                                </dd>
                            </dl>
                            {/if}
                            <dl class="dl-horizontal">
                                <dt style="width:110px;">备注</dt>
                                <dd style="margin-left:140px;">
                                    <textarea class="input-xlarge required" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                                </dd>
                            </dl>
                        {elseif $userTask['obj_type'] == 'cancel_business'}
                        <dl class="dl-horizontal">
                            <dt>日期</dt>
                            <dd>{$extension.start_time|date_format:"%Y-%m-%d"} {$timeNode[date('H:i:s',$extension.start_time)]} 到 {$extension.end_time|date_format:"%Y-%m-%d"} {$timeNode[date('H:i:s',$extension.end_time)]}</dd>
                            <dt>销出差天数</dt>
                            <dd>{$extension.day_number|string_format:"%.1f"}天</dd>
                            <dt>销出差原因</dt>
                            <dd>{$extension.leave_reason}</dd>
                            <dt>备注</dt>
                            <dd>
                                <textarea class="input-xlarge required" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                            </dd>
                        </dl>
                        {elseif $userTask['obj_type'] == 'cancel_absence'}
                        <dl class="dl-horizontal">
                            <dt>销假日期</dt>
                            <dd>{$extension.start_time|date_format:"%Y-%m-%d"} {$timeNode[date('H:i:s',$extension.start_time)]} 到 {$extension.end_time|date_format:"%Y-%m-%d"} {$timeNode[date('H:i:s',$extension.end_time)]}</dd>
                            <dt>销假天数</dt>
                            <dd>{$extension.day_number|string_format:"%.1f"}天</dd>
                            <dt>销假原因</dt>
                            <dd>{$extension.cancel_reason}</dd>
                            <dt>备注</dt>
                            <dd>
                                <textarea class="input-xlarge required" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                            </dd>
                        </dl>
                        {elseif $userTask['obj_type'] == 'business_card'}
                        <dl class="dl-horizontal">
                            <dt>姓名</dt>
                            <dd>{$extension.user_name}</dd>
                            <dt>职位</dt>
                            <dd>{$extension.position}</dd>
                            <dt>电话</dt>
                            <dd>{$extension.telephone}{if $extension.extension_number} 转 {$extension.extension_number}{/if}</dd>
                            <dt>手机</dt>
                            <dd>{$extension.mobile}</dd>
                            <dt>邮箱</dt>
                            <dd>{$applyUser.email}@rong360.com</dd>
                            <dt>印刷数量</dt>
                            <dd>{$extension.count}盒</dd>
                            <dt>名片制作说明</dt>
                            <dd>{$extension.remark}&nbsp;</dd>
                            <dt>备注</dt>
                            <dd>
                                <textarea class="input-xlarge required audit_desc" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                            </dd>
                        </dl>

                        {elseif $userTask['obj_type'] == 'entry' && $userTask['audit_desc'] == 'interviewer'}
                            <br><br>
                            {include file='entry/department_interview.tpl'}
                            <!-- 下面这个模版就是展示型的 -->
                            <!-- {include file='entry/department_interview_bak.tpl'} -->



                        {elseif $userTask['obj_type'] == 'entry' && $userTask['audit_desc'] == 'hr'}
                            <br><br>
                            {include file='entry/hr_interview.tpl'}
                            <!-- 下面这个模版就是展示型的 -->
                           <!--  {include file='entry/hr_interview_bak.tpl'} -->
                            <br><br>
                            <hr>
                            {foreach $departmentAssessments as $table}
                                <br><br>
                                {include file='entry/department_interview_bak.tpl'}
                            {/foreach}


                        {elseif $userTask['obj_type'] == 'entry'}
                            {include file='widget/entry/content.tpl'}
                            {*{foreach $departmentAssessments as $table1}*}
                                {*<h2 style="text-align:center;font-size:16px;">面试评估表-用人部门</h2>*}
                            {*{/foreach}*}

                            {*{foreach $hrAssessments as $table2}*}
                                {*<h2 style="text-align:center;font-size:16px;">面试评估表-人力部门</h2>*}
                            {*{/foreach}*}

                        {elseif $userTask['obj_type'] == 'dimission'}
                            {include file='dimission/lizhi_jiaojie.tpl'}
                            {*{if $userTask['workflow_step'] == 'C'}*}
                                {* 交接清单 - 直属领导 *}
                            {*{elseif $userTask['workflow_step'] == 'D'}*}
                                {* 交接清单 - 程昊 *}
                            {*{elseif $userTask['workflow_step'] == 'E'}*}
                                {* 交接清单 - 王李卿 *}
                            {*{elseif $userTask['workflow_step'] == 'F'}*}
                                {* 交接清单 - 张李楠 *}
                            {*{else}*}
                                {*{include file="widget/dimission/content.tpl"}*}
                            {*{/if}*}

                        {elseif $userTask['obj_type'] == 'workorder'}
                        <dl class="dl-horizontal">
                            <dt>问题类型</dt>
                            <dd>{$workorderType[$workordertype]}</dd>
                            <dt>问题级别</dt>
                            <dd>{$workorderLevel[$workorderlevel]}</dd>
                            <dt>手机号码</dt>
                            <dd>{$workorderUser.mobile}</dd>
                            <dt>公司邮箱</dt>
                            <dd>{$workorderUser.email}@rong360.com</dd>
                            <dd><textarea class="input-xlarge required audit_desc" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                            </dd>
                        </dl>

                        {elseif $userTask['obj_type'] == 'papp_vpn'}
                         <dl class="dl-horizontal">
                            <dt>申请权限类型</dt>
                            <dd>VPN权限申请</dd>
                            <dt>手机号码</dt>
                            <dd>{$pappvpnUser.mobile}</dd>
                            <dt>公司邮箱</dt>
                            <dd>{$pappvpnUser.email}@rong360.com</dd>
                            <dt>VPN类型</dt>
                            <dd>{$vpnType[$type]}</dd>
                            <dt>申请说明</dt>
                            <dd>{$reason}</dd>
                            <dd><textarea class="input-xlarge required audit_desc" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                            </dd>
                        </dl>

                        {elseif $userTask['obj_type'] == 'papp_db'}
                         <dl class="dl-horizontal">
                            <dt>申请权限类型</dt>
                            <dd>数据库权限申请</dd>
                            <dt>手机号码</dt>
                            <dd>{$pappdbUser.mobile}</dd>
                            <dt>公司邮箱</dt>
                            <dd>{$pappdbUser.email}@rong360.com</dd>
                            <dt>数据库名</dt>
                            <dd>{$pappdbItem.dbs_name}</dd>
                            <dt>数据库表名</dt>
                            <dd>{$pappdbItem.tables_name}</dd>
                            <dt>申请详情</dt>
                            <dd>{$pappdbItem.reason}</dd>
                            <dd><textarea class="input-xlarge required audit_desc" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                            </dd>
                        </dl>
                        {elseif $userTask['obj_type'] == 'ticket_book' && $extension.status == 1}
                            <dl class="dl-horizontal">
                                <dt>身份证号码</dt>
                                <dd>{$extension.identity_card}</dd>
                                <dt>手机号码</dt>
                                <dd>{$extension.mobile_number}</dd>
                                <dt>出行日期</dt>
                                <dd>{$extension.trip_date|date_format:"Y-m-d"}</dd>
                                {if $extension.hurry_reason}
                                <dt>紧急出行原因</dt>
                                <dd>{$extension.hurry_reason}</dd>
                                {/if}
                                <dt>出行时间段</dt>
                                <dd>{$extension.trip_time1}至{$extension.trip_time2}</dd>
                                <dt>出发城市</dt>
                                <dd>{$extension.from_city}</dd>
                                <dt>抵达城市</dt>
                                <dd>{$extension.to_city}</dd>
                                <dt>出差事由</dt>
                                <dd>{$extension.trip_reason}</dd>
                            {if $userTask['role_name'] == '票务专员'}
                                <dt>航班号 </dt>
                                <dd>
                                    <input type="text" class="input-xlarge required" name="flight_number"/>
                                </dd>
                                <dt>票价 </dt>
                                <dd><input type="text" class="input-xlarge required number" name="flight_price"/></dd>
                            {/if}
                            </dl>
                        {elseif $userTask['obj_type'] == 'ticket_book' && $extension.status ==2}
                            <dl class="dl-horizontal">
                                <dt>退票原因</dt>
                                <dd>{$extension.trip_reason}</dd>
                                {if $userTask['role_name'] == '票务专员'}
                                    <dt>原票价 </dt>
                                    <dd><input type="text" class="input-xlarge required number" name="flight_price"/></dd>
                                    <dt>航班号 </dt>
                                    <dd>
                                        <input type="text" class="input-xlarge required" name="flight_number"/>
                                    </dd>
                                    <dt>退票费 </dt>
                                    <dd><input type="text" class="input-xlarge required number" name="extra_fee"/></dd>
                                {/if}
                            </dl>
                        {elseif $userTask['obj_type'] == 'ticket_book' && $extension.status == 3}
                            <dl class="dl-horizontal">
                                <dt>改签后日期</dt>
                                <dd>{$extension.trip_date|date_format:"Y-m-d"}</dd>
                                <dt>出行时间段</dt>
                                <dd>{$extension.trip_time1}至{$extension.trip_time2}</dd>
                                <dt>出发城市</dt>
                                <dd>{$extension.from_city}</dd>
                                <dt>抵达城市</dt>
                                <dd>{$extension.to_city}</dd>
                                <dt>改签原因</dt>
                                <dd>{$extension.trip_reason}</dd>
                                {if $userTask['role_name'] == '票务专员'}
                                    <dt>原票价 </dt>
                                    <dd><input type="text" class="input-xlarge required number" name="flight_price"/></dd>
                                    <dt>航班号 </dt>
                                    <dd><input type="text" class="input-xlarge required" name="flight_number"/></dd>
                                    <dt>改签费 </dt>
                                    <dd><input type="text" class="input-xlarge required number" name="extra_fee"/></dd>
                                {/if}
                            </dl>
                        {elseif $userTask['obj_type'] == 'online_order'}
                            {include file="./widget/online_order/content.tpl"}
                        {elseif $userTask['obj_type'] == 'office_supply'}
                            <div class="block">
                                <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left">办公用品领用明细</div>
                                </div>
                                <div class="block-content collapse in control-group">
                                    <div class="span12 controls">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>类型</th>
                                                <th>办公用品</th>
                                                <th>数量</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {foreach $supplyDetails as $key => $value}
                                                <tr>
                                                    <td>{$value[0]}</td>
                                                    <td>{$value[1]}</td>
                                                    <td>{$value[2]}</td>
                                                </tr>
                                            {/foreach}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <dl class="dl-horizontal">
                                <dt>办公区</dt>
                                <dd>{$officeLocation[$userWorkflow.ext.office_location]}</dd>
                                {*<dt>其他办公区</dt>*}
                                {*<dd>{$userWorkflow.ext.other_location}</dd>*}
                            </dl>
                        {elseif $userTask['obj_type'] == 'lunch_change'}
                            <dl class="dl-horizontal">
                                <dt>办公区</dt>
                                <dd>{$lunchLocation[$userWorkflow.ext.lunch_location]}</dd>
                                <dt>变更类型</dt>
                                <dd>{$lunchChangeType[$userWorkflow.ext.lunch_change_type]}</dd>
                            </dl>
                        {elseif $userTask['obj_type'] == 'team_building'}
                        <div style="margin:0 50px;">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">报销金额：</td>
                                        <td width="35%" style=" border:0px;line-height:40px;">{$extension.amount}</td>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">经费金额：</td>
                                        <td style=" border:0px;line-height:40px;">{$balance}</td>
                                    </tr>

                                    <tr>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">人均报销：</td>
                                        <td width="35%" style=" border:0px;line-height:40px;">{$extension.avg_amount}</td>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">参加人数：</td>
                                        <td style=" border:0px;line-height:40px;">{$extension.person_num}</td>
                                    </tr>
                                    <tr>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">活动时间：</td>
                                        <td width="35%" style=" border:0px;line-height:40px;">{$extension.activity_time|date_format:"%Y-%m-%d"}</td>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">活动目的：</td>
                                        <td style=" border:0px;line-height:40px;">{$extension.title}</td>
                                    </tr>

                                    <tr>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">活动背景：</td>
                                        <td colspan="3" style=" border:0px;line-height:40px;">{$extension.activity_story}</td>
                                    </tr>
                                    <tr>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">活动内容：</td>
                                        <td colspan="3" style=" border:0px;line-height:40px;">{$extension.desc}</td>
                                    </tr>
                                    <tr>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">活动地点：</td>
                                        <td colspan="3" style=" border:0px;line-height:40px;">{$extension.activity_address}</td>
                                    </tr>
                                    <tr>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">参加人员：</td>
                                        <td colspan="3" style=" border:0px;line-height:40px;">{$extension.participant}</td>
                                    </tr>
                                    <tr>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">欠款金额：</td>
                                        <td colspan="3" style=" border:0px;line-height:40px;">{$extension.debt}</td>
                                    </tr>
                                    <tr>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">备注：</td>
                                        <td colspan="3" style=" border:0px;line-height:40px;">
                                            <textarea class="input-xlarge required" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        {/if}

                        {if $userTask['obj_type'] == 'administrative_expense'}
                        <div class="form-horizontal">
                            {if $userTask['workflow_step'] == 'B'}
                            <div class="control-group">
                                <label class="control-label">费用类型</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required" name="expense_type" value="" />
                                </div>
                            </div>
                            {/if}
                            {include file="./look/widget/administrative_expense/content.tpl"}
                            <div class="control-group">
                                <label class="control-label">备注</label>
                                <div class="controls">
                                    <textarea class="input-xlarge audit_desc" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                                </div>
                            </div>
                        </div>
                        {/if}

                        {if $userTask['obj_type'] == 'administrative_apply'}
                         <div class="form-horizontal">
                            {if $userTask['workflow_step'] == 'A'}
                            <div class="control-group">
                                <label class="control-label">物品类型</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select required" name="staff_type">
                                    <option value="">请选择</option>
                                    {foreach $staff_type as $key => $val}
                                    <option value="{$key}">{$val}</option>
                                    {/foreach}
                                    </select>
                                </div>
                                <label class="control-label">物品型号</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required" name="staff_ver" value="" />
                                </div>
                                <label class="control-label">单价</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required" name="staff_price" value="" />
                                </div>
                                <label class="control-label">总计</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required" name="staff_total" value="" />
                                </div>
                            </div>
                            {/if}
                            {if $userTask['workflow_step'] == 'C'||$userTask['workflow_step'] == 'D'}
                            <div class="control-group">
                                <label class="control-label" for="">物品类型</label>
                                <div class="controls">
                                    {$extension.staff_type}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="">物品型号</label>
                                <div class="controls">
                                    {$extension.staff_ver}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="">单价</label>
                                <div class="controls">
                                    {$extension.staff_price}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="">总计</label>
                                <div class="controls">
                                    {$extension.staff_total}
                                </div>
                            </div>
                            {/if}
                            <div class="control-group">
                                <label class="control-label" for="">物品名称</label>
                                <div class="controls">
                                    {$extension.staff}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="">数量</label>
                                <div class="controls">
                                    {$extension.number}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="">用途</label>
                                <div class="controls">
                                    {$extension.usage}
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">备注</label>
                                <div class="controls">
                                    <textarea class="input-xlarge audit_desc" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                                </div>
                            </div>
                        {/if}
                        {if $userTask['obj_type'] == 'team_encourage'}
                        <div style="margin:0 50px;">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">报销金额：</td>
                                        <td width="35%" style=" border:0px;line-height:40px;">{$extension.amount}</td>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">经费金额：</td>
                                        <td style=" border:0px;line-height:40px;">{$balance}</td>
                                    </tr>

                                    <tr>
                                        <td width="15%" style="text-align:right; border:0px;line-height:40px;">部门激励详情：</td>
                                        <td colspan="3" style=" border:0px;line-height:40px;">{$extension.remark}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        {/if}
                        {if $userTask['obj_type'] == 'finance_loan'}
                        <div class="form-horizontal">
                            {include file="./widget/finance_loan/content.tpl"}
                        </div>   
                        {/if}
                        {if $userTask['obj_type'] == 'finance_offset' || $userTask['obj_type'] == 'finance_payment'}
                        <div class="form-horizontal">
                            {include file="./widget/finance_payment/content.tpl"}
                        </div>   
                        {/if}


                        {if $userTasks}
                        {foreach $userTasks as $userTask1}
                        <dl class="dl-horizontal">
                            <dt>{if $userTask1.user_id}{$userTask1.user_name}{else}{$workflowConfig[$userTask1.workflow_step].rolename}{/if}</dt>
                            <dd>
                                {if $userTask1.audit_status == $smarty.const.OA_TASK_TONGGUO}
                                通过
                                {elseif $userTask1.audit_status == $smarty.const.OA_TASK_JUJUE}
                                拒绝
                                {elseif $userTask1.audit_status == $smarty.const.OA_TASK_CHUANGJIAN}
                                未审批
                                {/if}
                                {if $userTask1.audit_status != $smarty.const.OA_TASK_CHUANGJIAN}
			        <span style="margin-left:30px;">时间：{$userTask1.modify_time|date_format:"%Y-%m-%d %H:%M:%S"}</span>
                                {/if}
                            </dd>
                        </dl>
			{if $userTask1.audit_desc}
                        <dl class="dl-horizontal">
                            <dt>备注</dt>
			    <dd>
                            {if is_array($business_role) && in_array($userTask1.audit_desc, array_keys($business_role))}{$business_role[$userTask1.audit_desc]}{else}{$userTask1.audit_desc}{/if}
                            </dd>
			</dl>
			{/if}
                        {/foreach}
                        {/if}

                        <div class="form-actions">
                            <button type="button" value="{$smarty.const.OA_TASK_TONGGUO}" class="btn btn-primary">
                                {if $userTask['obj_type'] == 'entry' && ($userTask['audit_desc'] == 'interviewer' || $userTask['audit_desc'] == 'hr')}
                                    提交评估
                                {elseif $userTask['role_name'] == '票务专员' && strpos($userTask['obj_type'], 'ticket_book')  === 0}
                                    确认订票
                                {elseif $userTask['role_name'] == '票务专员' && strpos($userTask['obj_type'], 'ticket_refund')  === 0}
                                    确认退票
                                {elseif $userTask['role_name'] == '票务专员' && strpos($userTask['obj_type'], 'ticket_endorse')  === 0}
                                    确认改签
                                {else}
                                    同意
                                {/if}
                            </button>
                            {if $userTask['obj_type'] == 'entry' && ($userTask['audit_desc'] == 'interviewer' || $userTask['audit_desc'] == 'hr')}
                            {elseif $userTask['role_name'] == '票务专员' && strpos($userTask['obj_type'], 'ticket_book')  === 0}
                                <button type="button" value="{$smarty.const.OA_TASK_JUJUE}" class="btn reject">取消订票</button>
                            {elseif $userTask['role_name'] == '票务专员' && strpos($userTask['obj_type'], 'ticket_refund')  === 0}
                                <button type="button" value="{$smarty.const.OA_TASK_JUJUE}" class="btn reject">取消退票</button>
                            {elseif $userTask['role_name'] == '票务专员' && strpos($userTask['obj_type'], 'ticket_endorse')  === 0}
                                <button type="button" value="{$smarty.const.OA_TASK_JUJUE}" class="btn reject">取消改签</button>
                            {else}
                                <button type="button" value="{$smarty.const.OA_TASK_JUJUE}" class="btn reject">不同意</button>
                            {/if}
                        </div>

                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script type="text/javascript">
$('.btn').click(function(){
    var audit_status = $(this).val();
    $('#audit_status').val(audit_status);
    if($(this).hasClass('reject')){

        if(!confirm("您是否确定拒绝该流程？") ) {

            return false;

        }

        var val = $('.audit_desc').val();        
        if(val == ''){
            alert('请填写拒绝原因，以方便申请同学修改');
            $('.audit_desc').focus();
            return false;
        }
    }
    if($(this).hasClass('btn-primary')){
        if($('input[name="expense_type"]').length > 0){
            var expense_type = $('input[name="expense_type"]').val();  
            if(expense_type == ''){
                alert('请填写费用类型！');
                $('input[name="expense_type"]').focus();
                return false;
            }
        }
    }
    $('form').submit();
});
</script>
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
<script>
    jQuery(document).ready(function() {
        FormValidation.init();
    });
    $(function() {
        $('.datetimepicker').datetimepicker({
            format: 'yyyy-MM-dd',
            language: 'zh-CN',
            pickDate: true,
            pickTime: false,
            hourStep: 1,
            minuteStep: 15,
            secondStep: 30,
            inputMask: true,
        });
    });
</script>
{/literal}
{/block}

