{extends file="./_base.tpl"}
{block name='title'}部门员工查看
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}

<div id="content" class="span9">
<div class="block" style="margin-top:30px;">
<div class="navbar navbar-inner block-header">
    <div class="muted pull-left">
     部门员工查看
    </div>
</div>
<div class="block-content collapse in">
    <div class="span12">
        <table class="table table-hover" name="section">
            <thead>
                <tr>
                    <td>部门id</td><td>部门</td><td>姓名</td><td>工号</td><td>邮箱</td> 
                </tr>
            </thead>
            <tbody>
                {foreach $users as $item}
                    <tr>
                    <td>{$section.id}</td>
                    <td>{$section.name}</td>
                    <td>{$item.name}</td>
                    <td>{$item.id_card}</td>
                    <td>{$item.email}</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
{/block}


