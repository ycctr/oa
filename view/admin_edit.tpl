{extends file="./_base.tpl"}
{block name='title'}管理员管理
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}

<div id="content" class="span9">
    <form method="post" action="{if $is_edit eq 1}{$Yii->createUrl('admin/edit')}{else}{$Yii->createUrl('admin/add')}{/if}" id="f1">
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">
                    管理员信息
                </div>
            </div>
            <div class="alert alert-error hide" id="inputEmpty">
                <button class="close" data-dismiss="alert"></button>
                    请输入员工姓名或邮箱。
            </div>
            <div class="alert alert-error hide" id="roleEmpty">
                <button class="close" data-dismiss="alert"></button>
                    请至少选择一个角色。
            </div>
            <div class="alert alert-error hide" id="errorInput">
                <button class="close" data-dismiss="alert"></button>
                    员工信息输入有误，请检查输入。
            </div>
            <div class="control-group">
                姓名
                {if $is_add neq 1}
                    <input id="user_name"  type="text" name="user[name]" value="{$user.name}" readonly="readonly"></input>
                {else}
                    <input id="user_name"  type="text" name="user[name]" value="{$user.name}"></input>
                {/if}
                </br>
            </div>
            <div class="control-group">
                邮箱
                <input id="email"  type="text" name="user[email]" value="{$user.email}" {if $is_add neq 1} readonly="readonly"{/if}></input></br>
            </div>
            <input type="hidden" id="user_id" name="user[id]" value="{$user.id}"></input>
            {if $is_view neq 1} 
                <div class="btn-group">
                    <button class="btn btn-success">保存</button>
                </div>
            {/if}
        </div>
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">
                    角色信息
                </div>
            </div>
            {include "./widget/role_list.tpl"}
       </div>
    </form>
</div>
<script>
$(function(){

    $('.btn-success').click(
        function(){
            var user_name = $('#user_name').val();
            var email = $('#email').val();
            var userId = $('#user_id').val();
            var checkedRole = $('input[name="role[]"]:checked');
            if($.trim(user_name)=='' && $.trim(email)==''){
                $('#inputEmpty').show();
                $('#roleEmpty').hide();
                $('#errorInput').hide();
                $('[name="errorMessage"]').val("showError");
                return false;
            }
            if( checkedRole.length == 0 )
            {
                 $('#inputEmpty').hide();
                 $('#errorInput').hide();
                 $('#roleEmpty').show();
                 return false;
            }
            $('#inputEmpty').hide();
            $('#roleEmpty').hide();
            if (userId.length != 0)
                return  true;
            var result = false;
            $.ajax({
                url :  '/admin/adminvalidate',
                data : { name:user_name,email:email },
                type: 'get',
                dataType:'json',
                async: false,
                success:function(data){
                   if(!data.result)
                        $('#errorInput').show();
                   result = data.result;
                }
            });
            return result;
        }
    );

});
</script>
 {/block}
