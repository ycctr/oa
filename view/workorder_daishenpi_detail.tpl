{extends file="./_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="./widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li class="active">详情</li>
            </ul>
        </div>
    </div>

    <style>
        .dl-horizontal {
            padding-bottom:15px;
            border-bottom:1px #ddd solid;
            margin-bottom:30px;
        }
        .dl-horizontal dt,.dl-horizontal dd {
            font-weight:normal;
            line-height:20px;
            margin-bottom:20px;
        }
        .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:70px;
        }
        .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:200px;
            text-align:left;
        }
        .form-actions {
            border:0px;
            background:#fff;
        }
        .btn {
            width:100px;
            margin-right:30px;
        }
        .block-content h4 {
            padding-left:70px;
        }
        .form-actions {
            padding-left:85px;
        }
        .table td {
            border:0px;
            line-height:40px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block" style="border:0px;">
            <div class="form-actions">
			    {if $do_time > 0}
                <button disabled="true" type="button" value="{$smarty.const.WORKORDER_START}" class="btn">开始处理</button>
				{else}
                <button type="button" value="{$smarty.const.WORKORDER_START}" class="btn btn-primary">开始处理</button>
				{/if}
				{if $pause_time  > 0}
                <button disabled="true" type="button" value="{$smarty.const.WORKORDER_PAUSE}" class="btn pause">暂停</button>
				{else}
                <button type="button" value="{$smarty.const.WORKORDER_PAUSE}" class="btn btn-primary pause">暂停</button>
				{/if}
                <button type="button" value="{$smarty.const.OA_TASK_JUJUE}" class="btn btn-primary reject">拒绝处理</button>
                <button type="button" value="{$smarty.const.OA_TASK_TONGGUO}" class="btn btn-primary">关闭</button>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form action="/usertask/audit" method="post">
                    <input type="hidden" name="user_task_id" value="{$userTask.id}" />
                    <input id="audit_status" type="hidden" name="audit_status" value="" />
                    <fieldset>
                        <h3 style="text-align:center;">
                            {if $extension['type'] =='13'} 
                                出差 
                            {else}
                                {$workflow_type[$userTask.obj_type]}
                            {/if}
                        </h3>
                        <br/>
                        <br/>
                        <div class="title">
                            <label style="width:110px;" for="">姓名：{$userTask.user_name}</label>
                            {if !empty($sections)}
                            <label style="width:300px;">部门：
                                {foreach from=$sections item=section name=foo}
                                    {if $smarty.foreach.foo.last}
                                        {$section.name}
                                    {else}
                                        {$section.name}/
                                    {/if}
                                {/foreach}
                            </label>
                            {else}
                            <label for="">部门：{$userTask.section_name}</label>
                            {/if}
                            <label for="">日期：{$userTask.create_time|date_format:"%Y-%m-%d"}</label>
                        </div>
                        <dl class="dl-horizontal">
                            <dt>问题类型</dt>
                            <dd>{$workorderType[$workordertype]}</dd>
                            <dt>问题级别</dt>
                            {if $workorderlevel == $smarty.const.WORKORDER_LEVEL_URG}
                            <dd><font style="color:#FF0000">{$workorderLevel[$workorderlevel]}</font></dd>
                            {elseif $workorderlevel == $smarty.const.WORKORDER_LEVEL_IMP}
                            <dd><font style="color:#0000FF">{$workorderLevel[$workorderlevel]}</font></dd>
                            {else}
                            <dd>{$workorderLevel[$workorderlevel]}</dd>
                            {/if}
                            <dt>问题描述</dt>
                            <dd>{$problem_reason}</dd>
                            <dt>手机号码</dt>
                            <dd>{$workorderUser.mobile}</dd>
                            <dt>公司邮箱</dt>
                            <dd>{$workorderUser.email}@rong360.com</dd>
                            <dd><textarea class="input-xlarge required audit_desc" name="audit_desc" placeholder="请输入备注说明..." style="width: 375px; height: 100px"></textarea> 
                            </dd>
                        </dl>



                        {if $userTasks}
                        {foreach $userTasks as $userTask}
                        <dl class="dl-horizontal">
                            <dt>{if $userTask.user_id}{$userTask.user_name}{else}{$workflowConfig[$userTask.workflow_step].rolename}{/if}</dt>
                            <dd>
                                {if $userTask.audit_status == $smarty.const.OA_TASK_TONGGUO}
                                已处理
                                {elseif $userTask.audit_status == $smarty.const.WORKORDER_PAUSE}
								暂停中
                                {elseif $userTask.audit_status == $smarty.const.WORKORDER_START}
								开始处理中
                                {elseif $userTask.audit_status == $smarty.const.OA_TASK_JUJUE}
                                拒绝处理
                                {elseif $userTask.audit_status == $smarty.const.OA_TASK_CHUANGJIAN}
                                未处理
                                {/if}
                                {if $userTask.audit_status != $smarty.const.OA_TASK_CHUANGJIAN}
			        <span style="margin-left:30px;">时间：{$userTask.modify_time|date_format:"%Y-%m-%d %H:%M:%S"}</span>
                                {/if}
                            </dd>
                        </dl>
			{if $userTask.audit_desc}
                        <dl class="dl-horizontal">
                            <dt>备注</dt>
			    <dd>
			        {$userTask.audit_desc}
			    </dd>
			</dl>
			{/if}
                        {/foreach}
                        {/if}
                        <div class="form-actions">
                            {if $do_time > 0}
                               <button disabled="true" type="button" value="{$smarty.const.WORKORDER_START}" class="btn">开始处理</button>
			            	{else}
                               <button type="button" value="{$smarty.const.WORKORDER_START}" class="btn btn-primary">开始处理</button>
				            {/if}
				            {if $pause_time  > 0}
                               <button disabled="true" type="button" value="{$smarty.const.WORKORDER_PAUSE}" class="btn pause">暂停</button>
				            {else}
                               <button type="button" value="{$smarty.const.WORKORDER_PAUSE}" class="btn btn-primary pause">暂停</button>
				            {/if}
            
                            <button type="button" value="{$smarty.const.OA_TASK_JUJUE}" class="btn btn-primary reject">拒绝处理</button>
                            <button type="button" value="{$smarty.const.OA_TASK_TONGGUO}" class="btn btn-primary">关闭</button>
                        </div>
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script type="text/javascript">
$('.btn').click(function(){
    var audit_status = $(this).val();
    $('#audit_status').val(audit_status);
    if($(this).hasClass('reject')){

        if(!confirm("您是否确定拒绝该流程？") ) {

            return false;

        }

        var val = $('.audit_desc').val();        
        if(val == ''){
            alert('请填写拒绝原因，以方便申请同学修改');
            $('.audit_desc').focus();
            return false;
        }
    }

    if($(this).hasClass('pause')){

        if(!confirm("您是否确定暂停该流程？") ) {

            return false;

        }

        var val = $('.audit_desc').val();        
        if(val == ''){
            alert('请填写暂停原因，以方便申请同学了解原因');
            $('.audit_desc').focus();
            return false;
        }
    }

    $('form').submit();
});
</script>
{/literal}
{/block}
