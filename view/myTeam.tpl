{extends file="./_base.tpl"}
{block name='title'}用户管理
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}

<!-- <link rel="stylesheet" type="text/css" href="/css/main.css" /> -->
<link rel="stylesheet" type="text/css" href="/static/css/superfish.css" />
<link rel="stylesheet" type="text/css" href="/css/superfish-vertical.css" />
<link rel="stylesheet" type="text/css" href="/css/superfish-navbar.css" />

<script type="text/javascript" src="/static/js/jquery.superfish.js"></script>
<script type="text/javascript" src="/static/js/superfish.js"></script>
<script type="text/javascript" src="/static/js/hoverIntent.js"></script>
<script type="text/javascript" src="/static/js/superfish.js"></script>

<script type="text/javascript">

$(function(){
        $("ul.sf-menu").superfish();
        $("ul.sf-menu,ul.sf-navbar").superfish();
        $("ul.sf-vertical").superfish({
    animation:{
            opacity:'show',
            height:'show'
    },
    speed:'fast'
    });
});

function superfish_tree(obj) {

    var tmp_html   = $(obj).html() + "<span class='sf-sub-indicator'> »</span>";
    var section_id = $(obj).attr("section_id");
    $("#superfish_target").html(tmp_html);
    $("#superfish_target").attr("section_id", section_id);
    $("#superfish_tree").hide();
    get_cur_url();

}


function goManage(){
    get_cur_url();
    return false;

}

function get_cur_url() {

    check_loading.show();
    var section_id =  $("#superfish_target").attr("section_id");
    var tagname    =  $("#tagname").val();
    window.location.href="/myteam/list?section_id="+section_id+"&tagname="+tagname; 

}

</script>

<div id="content" class="span9">


<div class="block" style="margin-top:30px;">
 <div class="navbar navbar-inner block-header">

    <div class="muted pull-left">
     我的团队
    </div>
 </div>


 <div class="block-content collapse in">
    <div class="span12">

    
    <div class="table-toolbar" style="margin-bottom:18px;height:840px">
        <div >
            <ul class="sf-menu">
                <li>
                    <a id="superfish_target" section_id="{$section_taget_id}" >{$section_taget_name}</a>
                    <ul id="superfish_tree">
                        {$sectionList}
                        <li><a  onclick="superfish_tree(this)" section_id="0" >&nbsp;&nbsp;下属部门</a></li>
                    </ul>

                </ul>
        </div>

        <div style="float: left; line-height: 40px; margin-left: 10px; color: rgb(170, 170, 170);"><span>(*)直属部门</span></div>

        <div class="span5" style="float:left;margin-left:50px;margin-top: 6px">
        <form method="post" action="list" style="margin-bottom:0px" onsubmit="return goManage()">
            <input id="tagname" type="text" name="tagname" value="{$tagname}" placeholder="下属员工信息查询"></input>
            <div class="btn-group">
                <button class="btn btn-success" type="submit" style="margin-bottom:10px; background: none repeat scroll 0% 0% rgb(33, 122, 237); padding-left: 19px; padding-right: 19px;border-left-width:3px;border-right-width:3px;">查询
                </button>
            </div>
        </form>


    </div>

    {if $user_list != false}
    <div id="ajax_content">

        <br/>
            
        <div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 50px; margin-bottom: 0px;">
            <ul>
                <li><a>总计：&nbsp;{$page_count}&nbsp;人</a></li>
                {pager_oa count=$page_count pagesize=$per page=$cur_page pagelink="list?section_id=$section_taget_id&tagname=$tagetname&cur_page=%d" list=3}
            </ul>
        </div>

        <table class="table table-hover" name="user">
            <thead>
                <tr>
                    <td>工号</td>
                    <td>姓名</td>
                    <td>性别</td>
                    <td>邮箱</td>
                    <td>电话</td>
                    <td>部门</td>
                    <td>行政级别</td>
                    <td>操作</td> 
                </tr>
            </thead>
            <tbody>

                {foreach from=$user_list item=item key=key}
                <tr>
                    <td>{$item.id_card}</td>
                    <td>{$item.name}</td>
                    <td>{if ($item.sex==1)}
                        男
                    {else}
                        女
                    {/if}</td>
                    <td>{$item.email}</td>
                    <td>{$item.mobile}</td>
                    {assign var="sectionId" value="`$item.section_id`"}
                    <td>{$sectionInfo.$sectionId.name}</td>
                    <td>

                        {if $item.level eq $smarty.const.AUDIT_CEO}
                                CEO
                        {elseif $item.level eq $smarty.const.AUDIT_VP }
                                VP
                        {elseif $item.level eq $smarty.const.AUDIT_MANAGER }
                                部门负责人
                        {elseif $item.level eq $smarty.const.AUDIT_VICE_MANAGER }
                                分部门负责人
                        {else}
                                无
                        {/if}

                    </td>
                    <td>
                        <a target="_blank" href="checklist?id={$item.id}">打卡记录</a>
                    </td>
                </tr>
                {/foreach}
                </tbody>
        </table>

        <div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
            <ul>
                <li><a>总计：&nbsp;{$page_count}&nbsp;人</a></li>
                {pager_oa count=$page_count pagesize=$per page=$cur_page pagelink="list?section_id=$section_taget_id&tagname=$tagetname&cur_page=%d" list=3}
            </ul>
        </div>


        </div>
        {else}
        <br/>
        <div style="margin:50px 0px 0px 50px;color:#AAA;">没有任何记录！</div>
    {/if}


</div>

   
</div>
<script>


    $("#select_num").change(function(){

        var select_num = $(this).val(); 
        check_loading.show();

        $.ajax({
            url: '/user/Manage?selectNum='+select_num,    
            dataType: 'html',
            success:function(html){

                setTimeout(function(){
                        check_loading.hide();
                        $('#ajax_content').html(html);
                    }, 500);

            }
        }) 

            
    })


</script>


 {/block}

<script type="text/javascript" src="/static/js/check_loading.js"></script>

