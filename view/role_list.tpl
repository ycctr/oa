{extends file="./_base.tpl"}
{block name='title'}角色管理
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}

<div id="content" class="span9">
    <div class="block"style="margin-top:30px;">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                角色权限
            </div>
        </div>
    <div class="block-content collapse in">
        <div class="span12">
        {if (!is_null($error))}
            <div class="alert alert-error ">
                该角色已经被赋予部分员工。
            </div>
        {/if}
                <div class="table-toolbar" style="margin-bottom:18px;">
                    {if $add_p eq 1 }
                    <div class="btn-group">
                        <a href="{$Yii->createUrl('role/add')}">
                            <button class="btn btn-success">增加角色
                                <i class="icon-plus icon-white"></i>
                            </button>
                        </a>
                    </div>
                    {/if}
                    <div class="span4" style="float:right">
                        <form method="get" action="list" style="margin-bottom:0px">
                            <input type="text" name="query[name]" value="{$smarty.get.query.name}" placeholder="输入角色名查询"></input>
                            <div class="btn-group">
                                <button class="btn btn-success" type="submit" style="margin-bottom:10px; background: none repeat scroll 0% 0% rgb(33, 122, 237);padding-left: 19px; padding-right: 19px;border-left-width:3px;border-right-width:3px;">查询
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            {include file="./widget/role_table.tpl"}
            <div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
                <ul>
                    {pager_oa count=$arrPager.count pagesize=$arrPager.pagesize page=$arrPager.page pagelink=$arrPager.pagelink list=3}
                </ul>
            </div>
        </div>
    </div>
</div>
 {/block}
