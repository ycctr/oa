<div class="navbar navbar-fixed-top">

    <div class="navbar-inner">

        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="/userworkflow/list"><img style="with:30px;height:30px" src="/static/images/oa-logo.png"></a>
            <div class="nav-collapse collapse">

                {if $Yii->params['username']}
                <ul class="nav pull-right">
                    <li class="dropdown">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">{$Yii->params['username']}<i class="icon-user"></i><i class="caret"></i></a>
                        <ul class="dropdown-menu">
                            <li class="divider"></li>
                            <li>
                                <a tabindex="-1" href="/site/logout">退出</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                {/if}
            </div>

        </div>

    </div>

</div>
