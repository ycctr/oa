{extends file="./_base.tpl"}
{block name='title'}用户管理
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}

    <div id="content" class="span9">


<div class="block" style="margin-top:30px;">
 <div class="navbar navbar-inner block-header">

    <div class="muted pull-left">
     用户
    </div>
 </div>


 <div class="block-content collapse in">
    <div class="span12">
    {if (!is_null($data.message.error))}
        <div class="alert alert-error ">
            {$data.message.error}
        </div>
    {/if}

    <div class="table-toolbar" style="margin-bottom:18px;height:80px">
    {if $add_p eq 1}
    
        <span>操作功能：</span>
        <div class="btn-group">
            <a href="add">
                <button class="btn btn-success">增加用户
                    <i class="icon-plus icon-white"></i>
                </button>
            </a>

        </div>
        <div class="btn-group">
            <a href="/export/userinfo">
            <button class="btn btn-success">导出全部</button>
            </a>
        </div>

    {/if}
        <div class="span5" style="float:right">
        <form method="post" action="manage" style="margin-bottom:0px">
            <input type="text" name="Condition" value="{$Condition}" placeholder="输入用户名、工号查询"></input>
            <div class="btn-group">
                <button class="btn btn-success" type="submit" style="margin-bottom:10px; background: none repeat scroll 0% 0% rgb(33, 122, 237); padding-left: 19px; padding-right: 19px;border-left-width:3px;border-right-width:3px;">查询
                </button>
            </div>
        </form>

    </div>

    <div  style=" margin-top: 10px;margin-left:0px;">
        <span>筛选查询：</span>

        <select name="select_num" id="select_num" style="width:100px">
            {foreach from=$option_list item=val key=key}

                {if $key eq $selectNum}

                    <option value="{$key}" selected>{$val}</option>

                {else}

                    <option value="{$key}">{$val}</option>

                {/if}

            {/foreach}
        </select>
    </div>

</div>

   
    <div id="ajax_content">
        {include file="./widget/userManage_table.tpl"}
    </div>
</div>
<script>

    $(function(){

        $("#ajax_content").delegate(".del","click",function(){
            var id_card = $(this).parentsUntil('tbody','tr').find('input[name="id"]').val();  
            var name = $(this).parentsUntil('tbody','tr').find('input[name="name"]').val();
            if(!confirm("您确定要删除"+name+"("+id_card+")用户？")){
                return false;
            }

            check_loading.show();

        });

    })

    $("#select_num").change(function(){

        var select_num = $(this).val(); 
        check_loading.show();

        $.ajax({
            url: '/user/Manage?selectNum='+select_num,    
            dataType: 'html',
            success:function(html){

                setTimeout(function(){
                        check_loading.hide();
                        $('#ajax_content').html(html);
                    }, 500);

            }
        }) 

            
    })


</script>


 {/block}

<script type="text/javascript" src="/static/js/check_loading.js"></script>

