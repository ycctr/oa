{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">行政费用付费申请</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">行政费用付费申请</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/administrativeexpense/addpost" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">部门：{$section.name}</label>
                                <label for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <div style="color:red;text-align:center;padding-bottom:20px;">因签字流程及财务打款均需时间，为了避免规定日期未付款而产生的不必要的影响，请至少提前3个工作日提交申请，感谢配合！</div>
                            <div class="control-group">
                                <label class="control-label" for="select01">付款公司</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select required" name="payment_company">
                                    <option value="">请选择</option>
                                    {foreach $paymentCompanys as $key => $val}
                                    <option value="{$key}">{$val}</option>
                                    {/foreach}
                                    </select>
                                    <input style="display:none;" type="text" name="payment_company_name" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">付款事项简介</label>
                                <div class="controls">
                                    <textarea class="input-xlarge required" name="summary"  style="width: 575px; height: 100px"></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">供应商(个人)名称</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required" name="supplier" value="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">付款金额</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required number" name="amount" value="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">上传票据图片</label>
                                <div class="controls" style="position:relative;">
                                    <input id="filePath" style="width:178px;margin-right:10px;" type="text" class="input-xlarge" name="" value="" />
                                    <input id="fileBtn" style="width:80px;height:30px;" type="button" value="上传" />
                                    <input style="position:absolute;top:0;left:0;opacity:0;filter:alpha(opacity:0);width:285px;" id="file" type="file" name="Filedata" value="" />
                                    <input id="file_path" type="hidden" name="img_address" />
                                    <div id="imgbox"></div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select02">付款方式</label>
                                <div class="controls">
                                    <select id="select02" class="chzn-select required" name="payment_method">
                                    <option value="">请选择</option>
                                    {foreach $paymentMethods as $key => $val}
                                    <option value="{$key}">{$val}</option>
                                    {/foreach}
                                    </select>
                                    <input style="display:none;" type="text" name="payment_method_name" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select02">付款性质</label>
                                <div class="controls">
                                    <select class="chzn-select required" name="payment_nature">
                                    <option value="">请选择</option>
                                    {foreach $paymentNatures as $key => $val}
                                    <option value="{$key}">{$val}</option>
                                    {/foreach}
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">开户银行</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required" name="bank_account" value="" />
                                    <span style="color:red;padding-left:10px;">开户行需精确到支行！</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">账户名称</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required" name="account_name" value="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">账户号码</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required number" name="account_number" value="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">备注信息</label>
                                <div class="controls">
                                    <textarea class="input-xlarge required" name="remark"  style="width: 575px; height: 100px"></textarea>
                                </div>
                            </div>

                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
});

$(function(){
    $('#select01').on('change',function(){
        var val = $(this).val(); 
        if(val == 4){
            $("input[name='payment_company_name']").show();
        }else{
            $("input[name='payment_company_name']").hide();
        }
    }); 
    $('#select02').on('change',function(){
        var val = $(this).val(); 
        if(val == 4){
            $("input[name='payment_method_name']").show();
        }else{
            $("input[name='payment_method_name']").hide();
        }
    });
    $(document).delegate('#file',"change",function(){
        $('#filePath').val($(this).val());
        $('#fileBtn').val('正在上传');

        $.ajaxFileUpload({
            url: '/update/uploadyunpan',
            type : 'post',
            secureuri: false, //是否需要安全协议，一般设置为false
            fileElementId: 'file', //文件上传域的ID
            dataType: 'json', //返回值类型 一般设置为json
            success: function (resp)  //服务器成功响应处理函数
            {
                $("#file").replaceWith('<input style="position:absolute;top:0;left:0;opacity:0;filter:alpha(opacity:0);width:285px;" id="file" type="file" name="Filedata" value="" />');
                if(resp.code > 0){
                    alert(resp.msg);
                }else{
                    var val = $('#file_path').val();
                    val += ';'+resp.address;
                    $('#file_path').val(val);
                    $('<img style="margin-right:10px;width:200px;height:100px;" src="'+resp.address+'" />').appendTo('#imgbox');
                }
                $('#fileBtn').val('上传');
                $('#filePath').val('');
            }
        });
    });
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
<script type="text/javascript" src="/static/js/ajaxfileupload.js"></script>
{/block}
