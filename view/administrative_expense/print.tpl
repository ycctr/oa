{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="header"}
{/block}
{block name="content"}
<div id="content" style="width:700px; margin:0 auto;">
    <style>
        body {
            padding:0;
        }
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
        }
        .form-horizontal .block tr input {
            width:80%;
        }
        .table td {
            border-top:1px #000 solid;
            line-height:30px;
        }
        @media print {
            .table {
                border-collapse:collapse;
            }
            .table td {
                font-size:12px;
                line-height:26px;
                padding:3px;
            }
            .navbar {
                height:40px;
                line-height:40px;
            }
            .print {
                display:none;
            }
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted" style="text-align:center;">付款审批单</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <div class="form-horizontal">
                        <div class="title">
                            <div style="float:left;">
                            申请人：{$user.name}（{$user.id_card}）
                            申请部门：{$section.name}
                            {foreach $parentSection as $pitem}
                                /{$pitem.name}
                            {/foreach}
                            申请日期：{$userWorkflow.create_time|date_format:"%Y-%m-%d"}
                            </div>
                            <button style="float:right;" class="btn btn-success print">打印</button>
                        </div>

                        <div class="block">
                            <div class="span12">
                                <table class="table" width="100%" border="1" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td width="20%">合同类型</td>
                                            <td colspan="2">{$userWorkflow.ext.payment_company}</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="5">付款信息</td>
                                            <td width="20%">付款事项简介</td>
                                            <td>{$userWorkflow.ext.summary}</td>
                                        </tr>
                                        <tr>
                                            <td>供应商(个人)名称</td>
                                            <td>{$userWorkflow.ext.supplier}</td>
                                        </tr>
                                        <tr>
                                            <td>付款金额</td>
                                            <td>{$userWorkflow.ext.amount}</td>
                                        </tr>
                                        <tr>
                                            <td>付款方式</td>
                                            <td>{$userWorkflow.ext.payment_method}</td>
                                        </tr>
                                        <tr>
                                            <td>付款性质</td>
                                            <td>{$userWorkflow.ext.payment_nature}</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="3">收款信息</td>
                                            <td>开户银行</td>
                                            <td>{$userWorkflow.ext.bank_account}</td>
                                        </tr>
                                        <tr>
                                            <td>账户名称</td>
                                            <td>{$userWorkflow.ext.account_name}</td>
                                        </tr>
                                        <tr>
                                            <td>账户号码</td>
                                            <td>{$userWorkflow.ext.account_number}</td>
                                        </tr>
                                        <tr>
                                            <td>备注信息</td>
                                            <td colspan="2">{$userWorkflow.ext.remark}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br />
                        <br />

                        <table class="table" style="width:100%;" border="1" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="25%" style="text-align:left;">部门主管/经理</td>
                                <td width="25%" style="text-align:left;">{$manager}</td>
                                <td width="25%" style="text-align:left;">部门负责人</td>
                                <td>{$VP}</td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">人事审核</td>
                                <td style="text-align:left;">{$RENLI}</td>
                                <td style="text-align:left;">CEO签字</td>
                                <td style="text-align:left;">{$CEO}</td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="text-align:left;">财务填写</td>
                                <td style="text-align:left;">财务审核</td>
                                <td colspan="2" style="text-align:left;"></td>
                            </tr>
                            <tr>
                                <td style="text-align:left;">支付日期</td>
                                <td colspan="2" style="text-align:left;"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
<script>
$(function(){
    //window.print();      
    $('.print').click(function(){
        window.print();  
    });
});
</script>
{/block}

