{extends file="./_base.tpl"}
{block name="content"}
{include file="./widget/left-nav.tpl"}
<div id="content" class="span9">
    <ul class="nav nav-tabs" id="myTab">
        <li style="border-color: #eee #eee #ddd;background-color: #eee;border-radius: 4px 4px 0 0;"><a>我的流程</a></li>
        <li><a href="/usertask/list">需要我审批{if $Yii->params['userWorkflowCnt']}({$Yii->params['userWorkflowCnt']}){/if}</a></li>
    </ul>
    <div class="row-fluid">
        <a style="float:right;margin-top:8px;" href="/userworkflow/list">发起新的流程</a>
        <select name="audit_status">
            <option value="0">全部状态</option>
            {foreach $auditStatus as $key => $val}
            <option {if $current_audit_status == $key}selected{/if} value="{$key}">{$val}</option>
            {/foreach}
        </select>
        <select name="workflow_type">
            <option value="0">流程类型</option>
            {foreach $workflowTypes as $key => $val}
            <option {if $workflow_type == $key}selected{/if} value="{$key}">{$val}</option>
            {/foreach}
        </select>
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">我的流程</div>
            </div>
            <div class="block-content collapse in">
                <div id="liucheng" class="span12">
                    {include file="./widget/liucheng_content.tpl"}
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>

</div>
<script type="text/javascript">
;
$(function(){
    $('select').change(function(){
        var audit_status = $("[name='audit_status']").val();   
        var workflow_type = $("[name='workflow_type']").val();
        $.ajax({
            url : '/userworkflow/my/?audit_status='+audit_status+'&workflow_type='+workflow_type,
            dataType: 'html',
            success:function(html){
                $('#liucheng').html(html);
            }
        });
    });
});
</script>
{/block}
