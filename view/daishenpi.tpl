{extends file="./_base.tpl"}
{block name="content"}
{include file="./widget/left-nav.tpl"}
<div id="content" class="span9">
    <ul class="nav nav-tabs" id="myTab">
        <li><a href="/userworkflow/my">我的流程</a></li>
        <li style="border-color:#eee #eee #ddd;background-color:#eee;border-radius:4px 4px 0 0;"><a>需要我审批{if $Yii->params['userWorkflowCnt']}({$Yii->params['userWorkflowCnt']}){/if}</a></li>
    </ul>
    <div class="row-fluid">
        <a style="float:right;margin-top:8px;" href="/userworkflow/list">发起新的流程</a>
        <select name="audit_status" id="audit_status">
            {foreach $user_task_audit_status as $key => $val}
            <option value="{$key}" {if $key == $audit_status}selected="selected"{/if}>{$val}</option>
            {/foreach}
        </select>
        <select name="obj_type" id="obj_type">
            <option value="">流程类型</option>
            {foreach $workflow_type as $key => $val}
            <option value="{$key}"{if $key == $obj_type}selected="selected"{/if}>{$val}</option>
            {/foreach}
        </select>
        <input id="keyword" class="input-xlarge" type="text" placeholder="请输入用户名或工号" value=""  />
        <input style="margin-bottom:12px;" class="btn btn-success" type="button" value="search" />
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">需要我审批</div>
            </div>
            <div class="block-content collapse in">
                <div id="shenpi" class="span12">
                    {include file="./widget/daishenpi_content.tpl"}
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>

</div>
<script type="text/javascript">
;
var pass = {$smarty.const.OA_TASK_TONGGUO};
var reject = {$smarty.const.OA_TASK_JUJUE};
$(function(){
    $('select').change(function(){
        search();
    });
    $('.btn').click(function(){
        search();
    });

    function search(){
        var audit_status = $('#audit_status').val();   
        var obj_type = $('#obj_type').val();
        var keyword = $('#keyword').val();
        var url = '/usertask/list/?';
//        if(audit_status){
//            url += 'audit_status='+audit_status;
//        }
        url += 'audit_status='+audit_status;
        if(obj_type){
            url += '&obj_type='+obj_type;
        }
        if(keyword){
            url += '&keyword='+encodeURIComponent(keyword);
        }
        $.ajax({
            url : url,
            dataType: 'html',
            success:function(html){
                $('#shenpi').html(html);
            }
        });
    }

    $(document).delegate('.pass','click',function(){
        var me = this;
        audit(me,pass);
    });

    $(document).delegate('.reject','click',function(){
        var me = this;
        if( confirm("您确定拒绝该流程吗？") ) {

            audit(me,reject);
        
        }

    });

    function audit(me,audit_status)
    {
        var id = $(me).attr('data-value');              

        check_loading.show();
        $.ajax({
            url:'/usertask/audit/?user_task_id='+id+'&audit_status='+audit_status,
            dataType:'json',
            success:function(data){

                if(data.code == 5){

                    alert("申请人已经取消此流程申请!");
                    $(me).parentsUntil('tbody','tr').remove();                   
                
                }else if(data.code == 1) {
                
                    alert("该审批状态已经发生改变了!");
                    $(me).parentsUntil('tbody','tr').remove();                   

                }else if(data.code < 1){

                    $(me).parentsUntil('tbody','tr').remove();                   

                } 

                setTimeout(function(){
                        check_loading.hide();
                    }, 500);
            }
        });
    }
    
});
</script>
{/block}
