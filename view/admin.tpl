{extends file="./_base.tpl"}
{block name='title'}管理员管理
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}

<div class="span4" style="float:right">
    <form method="get" action="list" style="margin-bottom:0px">
        <input type="text" name="query" value="{$smarty.get.query}"></input>
        <div class="btn-group">
            <button class="btn btn-success" type="submit" style="margin-bottom:10px; background: none repeat scroll 0% 0% rgb(33, 122, 237);">查询
            </button>
        </div>
    </form>
</div>

<div id="content" class="span9">
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                管理员
            </div>
        </div>
    <div class="block-content collapse in">
        <div class="span12">
            {if $add_p eq 1 }
                <div class="table-toolbar" style="margin-bottom:18px;">
                    <div class="btn-group">
                        <a href="{$Yii->createUrl('admin/add')}">
                            <button class="btn btn-success">增加管理员
                                <i class="icon-plus icon-white"></i>
                            </button>
                        </a>
                    </div>
                </div>
            {/if}
            {include file="./widget/admin_table.tpl"}
            <div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
                <ul>
                    {pager_oa count=$arrPager.count pagesize=$arrPager.pagesize page=$arrPager.page pagelink=$arrPager.pagelink list=3}
                </ul>
            </div>
        </div>
    </div>
</div>
 {/block}
