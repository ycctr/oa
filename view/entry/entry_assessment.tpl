{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="header"}
{/block}
{block name="content"}
<div id="content" style="width:700px; margin:0 auto;">
    <style>
     body{
            width: 990px;
            margin: 20px auto;
            font-size: 12px;
             padding:0;
             background-color: #fff;
        }
        .red{
            color: red;
        }
        input{
            border: none !important;
        }
        h2{
            text-align: center;
            margin: 20px 0 20px 0 ;
            font-size: 22px;
        }
        .active{
            background-color: #eee;
        }
    
    </style>
    <h2>入职审批表Check in Application Form</h2>
   <table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4" class="active">Employee Info. 员工信息</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">
            姓名:<input type="text" style="width: 150px;"/>
        </th>
        <th>
            姓名拼音:<input type="text" style="width: 150px;"/>
        </th>
    </tr>
    <tr>
        <th scope="row">
            联系电话:<input type="text" style="width: 200px;"/>
        </th>
        <th>
            <span class="red">预计报到日期</span>:<input type="text" style="width: 150px;"/>
        </th>
    </tr>
    <tr>
        <th scope="row">
            工作地:北京
        </th>
        <th>
            邮件(用于接收offer):<input type="text" style="width: 200px;"/>
        </th>
    </tr>
    <tr>
        <th scope="row">
            简历来源:<input type="text" style="width: 200px;"/>
        </th>
        <td>

        </td>
    </tr>
    <tr>
        <th colspan="4">
            <span style="float: left;">雇佣类型:</span>
            <label class="radio-inline" style="float: left;margin-left: 20px;">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> permanent 正式
            </label>
            <label class="radio-inline" style="float: left;margin-left: 20px;">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> intern实习
            </label>
        </th>

    </tr>
    </tbody>
</table>
<table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4" class="active">Position Info.职位信息</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">
            <span class="red">业务线</span>:<input type="text" style="width: 150px;"/>
        </th>

    </tr>
    <tr>
        <th scope="row">
            <span class="red">部门</span>: <input type="text" style="width: 150px;"/>
        </th>
    </tr>
    <tr>
        <th scope="row">
            <span class="red">分部</span>: <input type="text" style="width: 150px;"/>
        </th>
    </tr>
    <tr>
        <th scope="row">
            职位: <input type="text" style="width: 200px;"/>
        </th>
    </tr>
    <tr>
        <th scope="row">
            直接上级: <input type="text" style="width: 200px;"/>
        </th>
    </tr>
    <tr>
        <th scope="row">
            职级: <input type="text" style="width: 200px;"/>
        </th>
    </tr>
    <tr>
        <th scope="row">
            薪资: <input type="text" style="width: 200px;"/>
        </th>
    </tr>
    <tr>
        <th scope="row">
            期权: <input type="text" style="width: 200px;"/>
        </th>
    </tr>
    <tr>
        <th scope="row">
            其他: <input type="text" style="width: 200px;"/>
        </th>
    </tr>
    </tbody>
</table>
<table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4" class="active">Approval Info. 审批信息</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">直接主管审批:<br>Signature & Date </th>
        <td>
            签字:
            <input type="text" width="100px;"/>
        </td>
        <td>
            日期:
            <input type="text" width="100px;"/>
        </td>
    </tr>
    <tr>
        <th scope="row">部门负责人审批:<br>Signature & Date </th>
        <td>
            签字:
            <input type="text" width="100px;"/>
        </td>
        <td>
            日期:
            <input type="text" width="100px;"/>
        </td>
    </tr>
    <tr>
        <th scope="row">人力部门审批:<br>Signature & Date </th>
        <td>
            签字:
            <input type="text" width="100px;"/>
        </td>
        <td>
            日期:
            <input type="text" width="100px;"/>
        </td>
    </tr>
    <tr>
        <th scope="row">VP审批:<br>Signature & Date </th>
        <td>
            签字:
            <input type="text" width="100px;"/>
        </td>
        <td>
            日期:
            <input type="text" width="100px;"/>
        </td>
    </tr>
    <tr>
        <th scope="row">CEO审批:<br>Signature & Date </th>
        <td>
            签字:
            <input type="text" width="100px;"/>
        </td>
        <td>
            日期:
            <input type="text" width="100px;"/>
        </td>
    </tr>
    </tbody>
</table>
<div class="zhu">
    <h3 style="font-size: 14px;line-height: 14px;">Remark备注：</h3>
    <p>1.此审批表要求在员工入职前2个工作日提交给人力资源部员工关系负责人，并附其简历及面试记录，留存档。</p>
    <p>2.人力资源部将及时通知候选人到岗时间。</p>
</div>
<script>
$(function(){
    //window.print();      
    $('.print').click(function(){
        window.print();  
    });
});
</script>
{/block}

