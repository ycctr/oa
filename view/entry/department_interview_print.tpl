{extends file="../_base.tpl"}
{block name="css-page"}
    <link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
    <link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
    <link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="header"}
{/block}
{block name="content"}
<style type="text/css">
    .row-fluid{
        width: 1000px;
        margin: 20px auto;
    }
    ul,li{ list-style:none;}
     .interview_header{
        height: 150px;
     }
    .interview_header h2{
        text-align:center;
        font-size:24px;
        margin-bottom: 20px;
    }

    .interview_header ul{
        width: 100%;
    }
    .interview_header ul li{
        float: left;
        width: 30%;
    }
    .interview_header ul li span{
        float: left;
    }

    .interview_main .table tbody tr th{
        text-align: center;
    }
    .form-control{
        border: none;
        height: 50px;
        -webkit-box-shadow:none;
        padding:0;
        margin-bottom: 0px;
        width: 100%;
    }
    .radio-inline{
        text-align: center;
    }
    .pinggu-name{
        text-align: center !important; 
        line-height: 34px !important;
    }
     .radio-inline {
    position: relative;
    display: inline-block;
    padding-left: 20px;
    margin-bottom: 0;
    font-weight: 400;
    vertical-align: middle;
    cursor: pointer;
}
input[type=radio] {
    margin: 0;
    line-height: normal;
}
</style>
<div class="interview_header">
    <style>
        @media print {
            .table {
                border-collapse:collapse;
                width: 1000px;

            }
            .table thead,.table thead tr{
                width: 100%;
                height: 25px;
                border-bottom:1px solid #eee;
                border-left:1px solid #eee;
            }
            .table tr th{
                border-left:1px #000 solid;
                line-height:26px;
                padding:3px;
            }
            .table td {
                font-size:12px;
                /*border:1px #000 solid;*/
                border-top:1px #000 solid;
                border-left:1px #000 solid;
                line-height:26px;
                padding:3px;
            }
            .navbar {
                height:40px;
                line-height:40px;
            }
            .print {
                display:none;
            }
            .interview_header {
                height: 150px;
            }
            .interview_header ul {
                width: 100%;
            }
            .interview_header ul li {
                float: left;
                width: 30%;
                line-height: 32px;
            }

            .table-bordered {
                border: 1px solid #ddd;
                border-collapse: separate;
                border-left: 0;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px;
            }

        }
    </style>

    <button style="float:right;" class="btn btn-success print">打印</button>
    <h2>面试评估表-用人部门</h2>
    <ul>
        <li style="width: 20%">
            <span>应聘人员姓名：</span>{$entryName}
        </li>
        <li style="width: 40%">
            <span>应聘部门：</span>{$sectionName}
        </li>
        <li>
            <span>应聘职位：</span>{$position}
        </li><br>
        <li style="width: 20%">
            <span>面试官：</span>{$interviewer}
        </li>
        <li>
            <span>面试时间：</span>{$interviewTime|date_format:"Y-m-d"}
        </li>
    </ul>
</div>
<div class="interview_main">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th width="100px;"></th>
          <th width="122px;">评估内容</th>
          <th width="201px;">解释</th>
          <th width="157px;">A（非常好）</th>
          <th width="135px;">B（较好）</th>
          <th width="136px;">C（一般）</th>
          <th width="112px;" style="border-right:1px solid #eee ">D（差）</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row" rowspan="20" style="line-height: 17;" class="pinggu-xingmu">评估项目</th>
          <td class="pinggu-name">个性特征</td>
          <td style=" padding: 0; width: 200px;">应聘者表现出的与工作相关的个性特征，如沟通能力、灵活性、自信、成熟等</td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.character == 0}checked="checked"{/if}>
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.character == 1}checked="checked"{/if}>
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.character == 2}checked="checked"{/if}>
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.character == 3}checked="checked"{/if}>
              </label>
          </td>
        </tr>
        <tr>
          <td class="pinggu-name">工作经验</td>
          <td style=" padding: 0;">应聘者过去的工作与所申请职位要求相关之处</td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.experience == 0}checked="checked"{/if}>
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.experience == 1}checked="checked"{/if}>
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.experience == 2}checked="checked"{/if}>
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.experience == 3}checked="checked"{/if}>
              </label>
          </td>
        </tr>
        <tr>
          <td class="pinggu-name">专业技能</td>
          <td style=" padding: 0;">应聘者适合此项工作的能力和潜力</td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.skill == 0}checked="checked"{/if}>
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.skill == 1}checked="checked"{/if}>
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.skill == 2}checked="checked"{/if}>
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.skill == 3}checked="checked"{/if}>
              </label>
          </td>
        </tr>
        <tr>
          <td class="pinggu-name">工作态度</td>
          <td style=" padding: 0;">对待工作或职业的态度</td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.attitude == 0}checked="checked"{/if}>
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.attitude == 1}checked="checked"{/if}>
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.attitude == 2}checked="checked"{/if}>
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.attitude == 3}checked="checked"{/if}>
              </label>
          </td>
        </tr>
        <tr>
          <td class="pinggu-name">发展潜力</td>
          <td style=" padding: 0;">未来的发展潜力如何</td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.potential == 0}checked="checked"{/if}>
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.potential == 1}checked="checked"{/if}>
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.potential == 2}checked="checked"{/if}>
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.potential == 3}checked="checked"{/if}>
              </label>
          </td>
        </tr>
      </tbody>
      <tbody>
        <tr>
          <th scope="row" rowspan="20" style="line-height: 17;">综合评估</th>
          <td class="pinggu-name">优点</td>
          <td style=" padding: 0;" colspan="6">{$departmentAssessment.advantage}</td>
        </tr>
        <tr>
          <td class="pinggu-name">缺点</td>
          <td style=" padding: 0;" colspan="6">{$departmentAssessment.disadvantage}</td>
        </tr>
        <tr>
          <td class="pinggu-name">综合评价</td>
          <td style=" padding: 0;" colspan="6">{$departmentAssessment.assessment}</td>
        </tr>
      </tbody>
    </table>
    <p>最终得分: <span>{$departmentAssessment.score}</span> 分（满分10分，6分以上合格）</p>
    <p>面试结果：</p>
        <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.conclusion == 0}checked="checked"{/if}>建议录用
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.conclusion == 1}checked="checked"{/if}>推荐到其他部门
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.conclusion == 2}checked="checked"{/if}>保留
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" {if $departmentAssessment.conclusion == 3}checked="checked"{/if}>不录用
              </label>
          </td>
        <br><br><br><br>
          <p>面试官签字: {$interviewer}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;日期: {$departmentAssessment.update_time|date_format:"Y-m-d"}</p>
  </div>

<script>
    $(function(){
        //window.print();
        $('.print').click(function(){
            window.print();
        });
    });
</script>
{/block}



