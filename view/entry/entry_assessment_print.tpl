{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="header"}
{/block}
{block name="content"}
<div id="content">
    <style>
     body{
            width: 990px;
            margin: 20px auto;
            font-size: 12px;
             padding:0;
             background-color: #fff;
        }
        .red{
            color: red;
        }
        input{
            border: none !important;
        }
        h2{
            text-align: center;
            margin: 20px 0 20px 0 ;
            font-size: 22px;
        }
        .active{
            background-color: #eee;
        }

     @media print {
         .table {
             border-collapse:collapse;
             width: 990px;

         }
         .table thead,.table tr{
             width: 100%;
             height: 25px;
             border-left:1px solid #eee;

         }
         .table tr th{
             border-left:1px #000 solid;
             border-bottom:1px #000 solid;
             border-right:1px solid #000;
             line-height:26px;
             padding:3px;
         }
         .table td {
             font-size:12px;
             border-right:1px solid #000;
             border-top:1px #000 solid;
             border-left:1px #000 solid;
             line-height:26px;
             padding:3px;
         }
         .navbar {
             height:40px;
             line-height:40px;
         }
         .print {
             display:none;
         }
         .interview_header {
             height: 150px;
         }
         .interview_header ul {
             width: 100%;
         }
         .interview_header ul li {
             float: left;
             width: 30%;
             line-height: 32px;
         }

         .table-bordered {
             border: 1px solid #000;
             -webkit-border-radius: 4px;
             -moz-border-radius: 4px;
             border-radius: 4px;
         }

     }

    </style>
    <h2>入职审批表Check in Application Form</h2>
    <button style="float:right;" class="btn btn-success print">打印</button>
   <table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4" class="active">Employee Info. 员工信息</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">
            姓名: {$entry.name}
        </th>
        <th>
            姓名拼音: {$entry.name_spell}
        </th>
    </tr>
    <tr>
        <th scope="row">
            联系电话: {$entry.mobile}
        </th>
        <th>
            <span>预计报到日期</span>: {$entry.approx_arrive_time|date_format:"Y-m-d"}
        </th>
    </tr>
    <tr>
        <th scope="row">
            工作地: {$entry.workplace}
        </th>
        <th>
            邮件(用于接收offer): {$entry.private_email}
        </th>
    </tr>
    <tr>
        <th scope="row">
            简历来源: {$entry.resume_source}
        </th>
        <td>

        </td>
    </tr>
    <tr>
        <th colspan="4">
            <span style="float: left;">雇佣类型:</span>
            <label class="radio-inline" style="float: left;margin-left: 20px;">
                <input type="radio" {if $entry.hire_type == 1}checked="checked"{/if}> permanent 正式
            </label>
            <label class="radio-inline" style="float: left;margin-left: 20px;">
                <input type="radio" {if $entry.hire_type == 0}checked="checked"{/if}> intern实习
            </label>
        </th>

    </tr>
    </tbody>
</table>
<table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4" class="active" style="text-align: left">Position Info.职位信息</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row" style="text-align: left">
            <span>业务线</span>: {$businessLine}
        </th>

    </tr>
    <tr>
        <th scope="row" style="text-align: left">
            <span>部门</span>: {$section}
        </th>
    </tr>
    <tr>
        <th scope="row" style="text-align: left">
            <span>分部</span>: {$subsection}
        </th>
    </tr>
    <tr>
        <th scope="row" style="text-align: left">
            职位: {$entry.position}
        </th>
    </tr>
    <tr>
        <th scope="row" style="text-align: left">
            直接上级: {$manager}
        </th>
    </tr>
    <tr>
        <th scope="row" style="text-align: left">
            职级: {$levelNumToName[$entry.level]}
        </th>
    </tr>
    <tr>
        <th scope="row" style="text-align: left">
            薪资: {$entry.salary}
        </th>
    </tr>
    <tr>
        <th scope="row" style="text-align: left">
            期权: {$entry.stock_option}
        </th>
    </tr>
    <tr>
        <th scope="row" style="text-align: left">
            其他: {$entry.remark}
        </th>
    </tr>
    </tbody>
</table>
<table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4" class="active">Approval Info. 审批信息</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">直接主管审批:<br>Signature & Date </th>
        <td>
            签字: {$directManager.user_name}
           
        </td>
        <td>
            日期: {$directManager.modify_time|date_format:"Y-m-d"}
            
        </td>
    </tr>
    <tr>
        <th scope="row">部门负责人审批:<br>Signature & Date </th>
        <td>
            签字: {$sectionManager.user_name}
            
        </td>
        <td>
            日期: {$sectionManager.modify_time|date_format:"Y-m-d"}
            
        </td>
    </tr>
    <tr>
        <th scope="row">人力部门审批:<br>Signature & Date </th>
        <td>
            签字: {$HRD.user_name}
           
        </td>
        <td>
            日期: {$HRD.modify_time|date_format:"Y-m-d"}
            
        </td>
    </tr>
    <tr>
        <th scope="row">VP审批:<br>Signature & Date </th>
        <td>
            签字: {$VP.user_name}
            
        </td>
        <td>
            日期: {$VP.modify_time|date_format:"Y-m-d"}
            
        </td>
    </tr>
    <tr>
        <th scope="row">CEO审批:<br>Signature & Date </th>
        <td>
            签字: {$CEO.user_name}
            
        </td>
        <td>
            日期: {$CEO.modify_time|date_format:"Y-m-d"}
            
        </td>
    </tr>
    </tbody>
</table>
<div class="zhu">
    <h3 style="font-size: 14px;line-height: 14px;">Remark备注：</h3>
    <p>1.此审批表要求在员工入职前2个工作日提交给人力资源部员工关系负责人，并附其简历及面试记录，留存档。</p>
    <p>2.人力资源部将及时通知候选人到岗时间。</p>
</div>
<script>
$(function(){
    //window.print();      
    $('.print').click(function(){
        window.print();  
    });
});
</script>
{/block}

