{extends file="../_base.tpl"}
{block name="css-page"}
    <link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
    <link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
    <link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="header"}
{/block}
{block name="content"}
<style type="text/css">
    body{
      font-size: 12px;
    }
    ul,li{ list-style:none;}
     .interview_header{
        margin-left: 60px;
       margin-right: 60px;

        }
    .interview_header h2{
        text-align:center;
        font-size:24px;
        margin-bottom: 20px;
    }
    .header_name{
      width: 100px;
    }
    .interview_main{
      margin-left: 60px;
      margin-right: 60px;
    }
    .interview_main .table tbody tr th{
        text-align: center;
    }
    .form-control{
        border: none;
        height: 50px;
        -webkit-box-shadow:none;
        padding:0;
        margin-bottom: 0px;
        width: 100%;
    }
    .radio-inline{
        text-align: center;
    }
    .pinggu-name{
        text-align: center !important; 
        line-height: 34px !important;
    }
     .radio-inline {
    position: relative;
    display: inline-block;
    padding-left: 20px;
    margin-bottom: 0;
    font-weight: 400;
    vertical-align: middle;
    cursor: pointer;
}
input[type=radio] {
    margin: 0;
    line-height: normal;
}
input {
  border: none !important;
}
</style>
<div class="interview_header">
    <style>
        @media print {
            .table {
                border-collapse:collapse;
                width: 1000px;

            }
            .table thead,.table tr{
                width: 100%;
                height: 25px;
                border-left:1px solid #eee;
            }
            .table tr th{
                border-left:1px #000 solid;
                border-bottom:1px #000 solid;
                line-height:26px;
                padding:3px;
            }
            .table td {
                font-size:12px;
                /*border:1px #000 solid;*/
                border-top:1px #000 solid;
                border-left:1px #000 solid;
                line-height:26px;
                padding:3px;
            }
            .navbar {
                height:40px;
                line-height:40px;
            }
            .print {
                display:none;
            }
            .interview_header {
                height: 150px;
            }
            .interview_header ul {
                width: 100%;
            }
            .interview_header ul li {
                float: left;
                width: 30%;
                line-height: 32px;
            }

            .table-bordered {
                border: 1px solid #000;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                border-radius: 4px;
            }

        }
    </style>
    <h2>面试评估表-hr</h2>
    <button style="float:right;" class="btn btn-success print">打印</button>
    <table class="table table-bordered">
      <tbody>
        <tr>
          <th scope="row" class="header_name">申请人</th>
          <td>{$entryName}</td>
          <th scope="row" class="header_name">面试日期</th>
          <td>{$interviewTime|date_format:"Y-m-d"}</td>
        </tr>
        <tr>
          <th scope="row" class="header_name">申请职位</th>
          <td>{$position}</td>
          <th scope="row" class="header_name">面试人</th>
          <td>{$interviewer}</td>
        </tr>
        <tr>
           <th scope="row" class="header_name">所属部门</th>
          <td>{$sectionName}</td>
          <th scope="row" class="header_name">简历获取方式</th>
          <td>{$resumeSource}</td>
        </tr>
      </tbody>
    </table>
</div>
<div class="interview_main">
    <div class="bs-example" data-example-id="bordered-table">
    <table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4">应聘者情况</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row" style="line-height: 6;">是否在职</th>
        <td colspan="4">
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.current_job_status == 0}checked="checked"{/if}>在职,已提出离职
            </label>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.current_job_status == 1}checked="checked"{/if}> 在职,还未提出
            </label>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.current_job_status == 2}checked="checked"{/if}> 离职办理中
            </label>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.current_job_status == 3}checked="checked"{/if}> 已离职
            </label>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.current_job_status == 4}checked="checked"{/if}> 应届毕业生
            </label>
            <label class="radio-inline" style="margin-top: 14px;">
                其他: {$hrAssessment.current_job_remark}
            </label>
        </td>
    </tr>
    <tr>
        <th scope="row">离职原因,及对原公司的评价</th>
        <td colspan="4">{$hrAssessment.dimission_reason}</td>
    </tr>
    <tr>
        <th scope="row" style="line-height: 7">以往学习和工作经历</th>
        <td colspan="4">
            {$hrAssessment.personal_experience}
        </td>
    </tr>
    <tr>
        <th scope="row" style="line-height: 2;">与应聘职位胜任能力要求相关<br>的其他情况</th>
        <td colspan="4">
            {$hrAssessment.relevent_remark}
        </td>
    </tr>
    <tr>
        <th scope="row" style="line-height: 3;">月薪</th>
        <td colspan="4">
            <span>现在: {$hrAssessment.current_salary}</span>(<span>薪资结构 {$hrAssessment.current_salary_structure}</span>);<span style=" margin-left: 20px;">期望:{$hrAssessment.expect_salary}</span>
        </td>
    </tr>
    <tr>
        <th scope="row">个人发展方向、职业生涯规划</th>
        <td colspan="4">{$hrAssessment.career_plan}</td>
    </tr>
    <tr>
        <th scope="row" style="line-height: 4;">对公司的了解</th>
        <td colspan="4">{$hrAssessment.knowing_about_here} </td>
    </tr>
    <tr>
        <th scope="row" style="line-height: 4;">爱好、特长</th>
        <td colspan="4">{$hrAssessment.speciality} </td>
    </tr>
    </tbody>
</table>

<table class="table table-bordered" id="table2">
    <thead>
    <tr>
        <th colspan="6">面试人印象及评价</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row" style="line-height: 3;">考察项目</th>
        <td style="width: 80px;">很出色</td>
        <td>较出色</td>
        <td>基本满意</td>
        <td>尚可</td>
        <td>不满意</td>
    </tr>
    <tr>
        <th scope="row">1、内在的成就动机</th>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.motivation == 0}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.motivation == 1}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.motivation == 2}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.motivation == 3}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.motivation == 4}checked="checked"{/if}>
            </label>
        </td>
    </tr>
    <tr>
        <th scope="row">2、学习能力</th>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.learning_ability == 0}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.learning_ability == 1}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.learning_ability == 2}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.learning_ability == 3}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.learning_ability == 4}checked="checked"{/if}>
            </label>
        </td>
    </tr>
    <tr>
        <th scope="row">3、分析和解决问题能力</th>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.analysis_solve_ability == 0}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.analysis_solve_ability == 1}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.analysis_solve_ability == 2}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.analysis_solve_ability == 3}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.analysis_solve_ability == 4}checked="checked"{/if}>
            </label>
        </td>
    </tr>
    <tr>
        <th scope="row">4、交流沟通能力</th>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.communication_ability == 0}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.communication_ability == 1}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.communication_ability == 2}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.communication_ability == 3}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.communication_ability == 4}checked="checked"{/if}>
            </label>
        </td>
    </tr>
    <tr>
        <th scope="row">5、团队精神</th>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.team_spirit == 0}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.team_spirit == 1}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.team_spirit == 2}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.team_spirit == 3}checked="checked"{/if}
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.team_spirit == 4}checked="checked"{/if}>
            </label>
        </td>
    </tr>
    <tr>
        <th scope="row">6、职业素质，专业技能，工作经验</th>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.skill_experience == 0}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.skill_experience == 1}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.skill_experience == 2}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.skill_experience == 3}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.skill_experience == 4}checked="checked"{/if}>
            </label>
        </td>
    </tr>
    <tr>
        <th scope="row">7、总体评价</th>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.assessment == 0}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.assessment == 1}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.assessment == 2}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.assessment == 3}checked="checked"{/if}>
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" {if $hrAssessment.assessment == 4}checked="checked"{/if}>
            </label>
        </td>
    </tr>
    <tr>
        <th colspan="2">
            <span>优点、可取之处：{$hrAssessment.advantage}</span>
           
        </th>
        <th colspan="2">
            <span>缺点、需改进：{$hrAssessment.disadvantage}</span>
           
        </th>
        <th colspan="2">
            <span>建议（针对需求部门）：{$hrAssessment.suggestion}</span>
            
        </th>
    </tr>
    </tbody>
</table>
<p>最终得分  {$hrAssessment.score}分（满分10分，6分以上合格）</p>
    <p>面试结果：</p>
        <td> 
              <label class="radio-inline">
                <input type="radio" {if $hrAssessment.conclusion == 0}checked="checked"{/if}>建议录用
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" {if $hrAssessment.conclusion == 1}checked="checked"{/if}>推荐到其他部门
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $hrAssessment.conclusion == 2}checked="checked"{/if}>保留
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" {if $hrAssessment.conclusion == 3}checked="checked"{/if}>不录用
              </label>
          </td>
        <br><br><br><br>
        <p>面试官签字: {$interviewer}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;日期: {$hrAssessment.update_time|date_format:"Y-m-d"}</p>
    </div>
</div>
<script>
    $(function(){
        //window.print();
        $('.print').click(function(){
            window.print();
        });
    });
</script>
{/block}



