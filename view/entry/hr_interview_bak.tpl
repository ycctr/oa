<style type="text/css">
    body{
      font-size: 12px;
    }
    ul,li{ list-style:none;}
     .interview_header{
        margin-left: 60px;
       margin-right: 60px;

        }
    .interview_header h2{
        text-align:center;
        font-size:24px;
        margin-bottom: 20px;
    }
    .header_name{
      width: 100px;
    }
    .interview_main{
      margin-left: 60px;
      margin-right: 60px;
    }
    .interview_main .table tbody tr th{
        text-align: center;
    }
    .form-control{
        border: none;
        height: 50px;
        -webkit-box-shadow:none;
        padding:0;
        margin-bottom: 0px;
        width: 100%;
    }
    .radio-inline{
        text-align: center;
    }
    .pinggu-name{
        text-align: center !important; 
        line-height: 34px !important;
    }
     .radio-inline {
    position: relative;
    display: inline-block;
    padding-left: 20px;
    margin-bottom: 0;
    font-weight: 400;
    vertical-align: middle;
    cursor: pointer;
}
input[type=radio] {
    margin: 0;
    line-height: normal;
}
input {
  border: none !important;
}
</style>
<div class="interview_header">
    <h2>面试评估表-hr</h2>
    <table class="table table-bordered">
      <tbody>
        <tr>
          <th scope="row" class="header_name">申请人</th>
          <td>AAA</td>
          <th scope="row" class="header_name">面试日期</th>
          <td>x年x月x日</td>
        </tr>
        <tr>
          <th scope="row" class="header_name">申请职位</th>
          <td>AAA</td>
          <th scope="row" class="header_name">面试人</th>
          <td></td>
        </tr>
        <tr>
           <th scope="row" class="header_name">所属部门</th>
          <td>AAA</td>
          <th scope="row" class="header_name">简历获取方式</th>
          <td></td>
        </tr>
      </tbody>
    </table>
</div>
<div class="interview_main">
    <div class="bs-example" data-example-id="bordered-table">
    <table class="table table-bordered" style="margin-bottom: 20px;">
    <thead>
    <tr>
        <th colspan="4">应聘者情况</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row" style="line-height: 6;">是否在职</th>
        <td colspan="4">
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">在职,已提出离职
            </label>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> 在职,还未提出
            </label>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> 离职办理中
            </label>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> 已离职
            </label>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> 应届毕业生
            </label>
            <label class="radio-inline" style="margin-top: 14px;">
                其他:
            </label>
        </td>
    </tr>
    <tr>
        <th scope="row">离职原因,及对原公司的评价</th>
        <td colspan="4"></td>
    </tr>
    <tr>
        <th scope="row" style="line-height: 7">以往学习和工作经历</th>
        <td colspan="4">
           
        </td>
    </tr>
    <tr>
        <th scope="row" style="line-height: 2;">与应聘职位胜任能力要求相关<br>的其他情况</th>
        <td colspan="4">
           
        </td>
    </tr>
    <tr>
        <th scope="row" style="line-height: 3;">月薪</th>
        <td colspan="4">
            <span>现在: </span>(<span>薪资结构 </span>);<span style=" margin-left: 20px;">期望:</span> 
        </td>
    </tr>
    <tr>
        <th scope="row">个人发展方向、职业生涯规划</th>
        <td colspan="4"></td>
    </tr>
    <tr>
        <th scope="row" style="line-height: 4;">对公司的了解</th>
        <td colspan="4"> </td>
    </tr>
    <tr>
        <th scope="row" style="line-height: 4;">爱好、特长</th>
        <td colspan="4"> </td>
    </tr>
    </tbody>
</table>

<table class="table table-bordered" id="table2">
    <thead>
    <tr>
        <th colspan="6">面试人印象及评价</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row" style="line-height: 3;">考察项目</th>
        <td style="width: 80px;">很出色</td>
        <td>较出色</td>
        <td>基本满意</td>
        <td>尚可</td>
        <td>不满意</td>
    </tr>
    <tr>
        <th scope="row">1、内在的成就动机</th>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
    </tr>
    <tr>
        <th scope="row">2、学习能力</th>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
    </tr>
    <tr>
        <th scope="row">3、分析和解决问题能力</th>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
    </tr>
    <tr>
        <th scope="row">4、交流沟通能力</th>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
    </tr>
    <tr>
        <th scope="row">5、团队精神</th>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
    </tr>
    <tr>
        <th scope="row">6、职业素质，专业技能，工作经验</th>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
    </tr>
    <tr>
        <th scope="row">7、总体评价</th>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
        <td>
            <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
            </label>
        </td>
    </tr>
    <tr>
        <th colspan="2">
            <span>优点、可取之处：</span>
           
        </th>
        <th colspan="2">
            <span>缺点、需改进：</span>
           
        </th>
        <th colspan="2">
            <span>建议（针对需求部门）：</span>
            
        </th>
    </tr>
    </tbody>
</table>
<p>最终得分  分（满分10分，6分以上合格）</p>
    <p>面试结果：</p>
        <td> 
              <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">建议录用
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">推荐到其他部门
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">保留
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option4">不录用
              </label>
          </td>
  </div>
</div>



