<style type="text/css">
    ul,li{ list-style:none;}
     .interview_header{
        height: 150px;
     }
    .interview_header h2{
        text-align:center;
        font-size:24px;
        margin-bottom: 20px;
    }

    .interview_header ul{
        width: 100%;
        margin-left: 70px;
    }
    .interview_header ul li{
        float: left;
        width: 30%;
    }
    .interview_header ul li span{
        float: left;
    }
    .interview_main{
      margin-left: 60px;
      margin-right: 60px;
    }
    .interview_main .table tbody tr th{
        text-align: center;
    }
    .form-control{
        border: none;
        height: 50px;
        -webkit-box-shadow:none;
        padding:0;
        margin-bottom: 0px;
        width: 100%;
    }
    .radio-inline{
        text-align: center;
    }
    .pinggu-name{
        text-align: center !important; 
        line-height: 34px !important;
    }
     .radio-inline {
    position: relative;
    display: inline-block;
    padding-left: 20px;
    margin-bottom: 0;
    font-weight: 400;
    vertical-align: middle;
    cursor: pointer;
}
input[type=radio] {
    margin: 0;
    line-height: normal;
}
</style>
<div class="interview_header">
    <h2>面试评估表-用人部门</h2>
    <ul>
        <li>
            <span>应聘人员姓名：</span>
            <p>{$userWorkflow.ext.name}</p>
        </li>
        <li>
            <span>应聘部门：</span>
            <p>{$targetSection}</p>
        </li>
        <li>
            <span>应聘职位：</span>
            <p>{$userWorkflow.ext.position}</p>
        </li>
        <li>
            <span>面试官：</span>
            <p>{$interviewers}</p>
        </li>
        <li>
            <span>面试时间：</span>
            <p>{$userWorkflow.ext.interview_time|date_format:"Y-m-d"}</p>
        </li>
    </ul>
</div>
<div class="interview_main">
    <div class="bs-example" data-example-id="bordered-table">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th></th>
          <th>评估内容</th>
          <th>解释</th>
          <th>A（非常好）</th>
          <th>B（较好）</th>
          <th>C（一般）</th>
          <th>D（差）</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row" rowspan="20" style="line-height: 17;">评估项目</th>
          <td class="pinggu-name">个性特征</td>
          <td style=" padding: 0;"><textarea class="form-control" rows="3">应聘者表现出的与工作相关的个性特征，如沟通能力、灵活性、自信、成熟等</textarea></td>
          <td> 
              <label class="radio-inline">
                <input type="radio" name="data[character]" id="data[character]" value="0">
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" name="data[character]" id="data[character]" value="1">
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" name="data[character]" id="data[character]" value="2">
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" name="data[character]" id="data[character]" value="3">
              </label>
          </td>
        </tr>
        <tr>
          <td class="pinggu-name">工作经验</td>
          <td style=" padding: 0;"><textarea class="form-control" rows="3">应聘者过去的工作与所申请职位要求相关之处</textarea></td>
          <td> 
              <label class="radio-inline">
                <input type="radio" name="data[experience]" id="data[experience]" value="0">
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" name="data[experience]" id="data[experience]" value="1">
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" name="data[experience]" id="data[experience]" value="2">
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" name="data[experience]" id="data[experience]" value="3">
              </label>
          </td>
        </tr>
        <tr>
          <td class="pinggu-name">专业技能</td>
          <td style=" padding: 0;"><textarea class="form-control" rows="3">应聘者适合此项工作的能力和潜力</textarea></td>
          <td> 
              <label class="radio-inline">
                <input type="radio" name="data[skill]" id="data[skill]" value="0">
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" name="data[skill]" id="data[skill]" value="1">
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" name="data[skill]" id="data[skill]" value="2">
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" name="data[skill]" id="data[skill]" value="3">
              </label>
          </td>
        </tr>
        <tr>
          <td class="pinggu-name">工作态度</td>
          <td style=" padding: 0;"><textarea class="form-control" rows="3">对待工作或职业的态度</textarea></td>
          <td> 
              <label class="radio-inline">
                <input type="radio" name="data[attitude]" id="data[attitude]" value="0">
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" name="data[attitude]" id="data[attitude]" value="1">
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" name="data[attitude]" id="data[attitude]" value="2">
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" name="data[attitude]" id="data[attitude]" value="3">
              </label>
          </td>
        </tr>
        <tr>
          <td class="pinggu-name">发展潜力</td>
          <td style=" padding: 0;"><textarea class="form-control" rows="3">未来的发展潜力如何</textarea></td>
          <td> 
              <label class="radio-inline">
                <input type="radio" name="data[potential]" id="data[potential]" value="0">
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" name="data[potential]" id="data[potential]" value="1">
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" name="data[potential]" id="data[potential]" value="2">
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" name="data[potential]" id="data[potential]" value="3">
              </label>
          </td>
        </tr>
      </tbody>
      <tbody>
        <tr>
          <th scope="row" rowspan="20" style="line-height: 17;">综合评估</th>
          <td class="pinggu-name">优点</td>
          <td style=" padding: 0;" colspan="6"><textarea class="form-control" rows="3" style="height: 90px;" name="data[advantage]"></textarea></td>
        </tr>
        <tr>
          <td class="pinggu-name">缺点</td>
          <td style=" padding: 0;" colspan="6"><textarea class="form-control" rows="3" style="height: 90px;" name="data[disadvantage]"></textarea></td>
        </tr>
        <tr>
          <td class="pinggu-name">综合评价</td>
          <td style=" padding: 0;" colspan="6"><textarea class="form-control" rows="3" style="height: 90px;" name="data[assessment]"></textarea></td>
        </tr>
      </tbody>
    </table>
    <p>最终得分：<input type="text" style="width: 25px; height: 15px; line-height: 15px;vertical-align: baseline;" name="data[score]">  分（满分10分，6分以上合格）</p>
    <p>面试结果：</p>
        <td> 
              <label class="radio-inline">
                <input type="radio" name="data[conclusion]" id="data[conclusion]" value="0">建议录用
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" name="data[conclusion]" id="data[conclusion]" value="1">推荐到其他部门
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" name="data[conclusion]" id="data[conclusion]" value="2">保留
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" name="data[conclusion]" id="data[conclusion]" value="3">不录用
              </label>
          </td>
  </div>
</div>



