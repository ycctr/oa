<style type="text/css">
    ul,li{ list-style:none;}
     .interview_header{
        height: 150px;
     }
    .interview_header h2{
        text-align:center;
        font-size:24px;
        margin-bottom: 20px;
    }

    .interview_header ul{
        width: 100%;
        margin-left: 70px;
    }
    .interview_header ul li{
        float: left;
        width: 30%;
    }
    .interview_header ul li span{
        float: left;
    }
    .interview_main{
      margin-left: 60px;
      margin-right: 60px;
    }
    .interview_main .table tbody tr th{
        text-align: center;
    }
    .form-control{
        border: none;
        height: 50px;
        -webkit-box-shadow:none;
        padding:0;
        margin-bottom: 0px;
        width: 100%;
    }
    .radio-inline{
        text-align: center;
    }
    .pinggu-name{
        text-align: center !important; 
        line-height: 34px !important;
    }
     .radio-inline {
    position: relative;
    display: inline-block;
    padding-left: 20px;
    margin-bottom: 0;
    font-weight: 400;
    vertical-align: middle;
    cursor: pointer;
}
input[type=radio] {
    margin: 0;
    line-height: normal;
}
</style>
<div class="interview_header">
    <h2>面试评估表-用人部门</h2>
    <ul>
        <li>
            <span>应聘人员姓名：</span>
            <p>{$userWorkflow.ext.name}</p>
        </li>
        <li>
            <span>应聘部门：</span>
            <p>{$targetSection}</p>
        </li>
        <li>
            <span>应聘职位：</span>
            <p>{$userWorkflow.ext.position}</p>
        </li>
        <li>
            <span>面试官：</span>
            <p>{$table.interviewers}</p>
        </li>
        <li>
            <span>面试时间：</span>
            <p>{$userWorkflow.ext.interview_time|date_format:"Y-m-d"}</p>
        </li>
    </ul>
</div>
<div class="interview_main">
    <div class="bs-example" data-example-id="bordered-table">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th></th>
          <th>评估内容</th>
          <th>解释</th>
          <th>A（非常好）</th>
          <th>B（较好）</th>
          <th>C（一般）</th>
          <th>D（差）</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row" rowspan="20" style="line-height: 17;">评估项目</th>
          <td class="pinggu-name">个性特征</td>
          <td style=" padding: 0; width: 200px;">应聘者表现出的与工作相关的个性特征，如沟通能力、灵活性、自信、成熟等</td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.character == 0}checked="checked"{/if}>
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" {if $table.character == 1}checked="checked"{/if}>
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.character == 2}checked="checked"{/if}>
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.character == 3}checked="checked"{/if}>
              </label>
          </td>
        </tr>
        <tr>
          <td class="pinggu-name">工作经验</td>
          <td style=" padding: 0;">应聘者过去的工作与所申请职位要求相关之处</td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.experience == 0}checked="checked"{/if}>
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" {if $table.experience == 1}checked="checked"{/if}>
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.experience == 2}checked="checked"{/if}>
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.experience == 3}checked="checked"{/if}>
              </label>
          </td>
        </tr>
        <tr>
          <td class="pinggu-name">专业技能</td>
          <td style=" padding: 0;">应聘者适合此项工作的能力和潜力</td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.skill == 0}checked="checked"{/if}>
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" {if $table.skill == 1}checked="checked"{/if}>
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.skill == 2}checked="checked"{/if}>
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.skill == 3}checked="checked"{/if}>
              </label>
          </td>
        </tr>
        <tr>
          <td class="pinggu-name">工作态度</td>
          <td style=" padding: 0;">对待工作或职业的态度</td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.attitude == 0}checked="checked"{/if}>
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" {if $table.attitude == 1}checked="checked"{/if}>
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.attitude == 2}checked="checked"{/if}>
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.attitude == 3}checked="checked"{/if}>
              </label>
          </td>
        </tr>
        <tr>
          <td class="pinggu-name">发展潜力</td>
          <td style=" padding: 0;">未来的发展潜力如何</td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.potential == 0}checked="checked"{/if}>
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" {if $table.potential == 1}checked="checked"{/if}>
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.potential == 2}checked="checked"{/if}>
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.potential == 3}checked="checked"{/if}>
              </label>
          </td>
        </tr>
      </tbody>
      <tbody>
        <tr>
          <th scope="row" rowspan="20" style="line-height: 17;">综合评估</th>
          <td class="pinggu-name">优点</td>
          <td style=" padding: 0;" colspan="6">{$table.advantage}</td>
        </tr>
        <tr>
          <td class="pinggu-name">缺点</td>
          <td style=" padding: 0;" colspan="6">{$table.disadvantage}</td>
        </tr>
        <tr>
          <td class="pinggu-name">综合评价</td>
          <td style=" padding: 0;" colspan="6">{$table.assessment}</td>
        </tr>
      </tbody>
    </table>
    <p>最终得分: <span>{$table.score}</span> 分（满分10分，6分以上合格）</p>
    <p>面试结果：</p>
        <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.conclusion == 0}checked="checked"{/if}>建议录用
              </label>
          </td>
          <td> 
             <label class="radio-inline">
                <input type="radio" {if $table.conclusion == 1}checked="checked"{/if}>推荐到其他部门
              </label> 
          </td>
          <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.conclusion == 2}checked="checked"{/if}>保留
              </label>
          </td>
           <td> 
              <label class="radio-inline">
                <input type="radio" {if $table.conclusion == 3}checked="checked"{/if}>不录用
              </label>
          </td>
  </div>
</div>



