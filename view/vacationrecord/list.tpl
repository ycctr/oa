{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">
    <div class="row-fluid">
        <div class="block">
            <div class="navbar navbar-inner block-header">
                
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th width="8%">员工号</th>
                                <th width="8%">姓名</th>
                                <th width="12%">部门</th>
                                <th>类型</th>
                                <th style="text-align:center;">总计</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{$user.id_card}</td>
                                <td>{$user.name}</td>
                                <td>{$section.name}</td>
                                <td>年假</td>
                                <td style="text-align:center;">{if $total.nianjia_total>0}{$total.nianjia_total}{else}0{/if}</td>
                            </tr>
                            <tr>
                                <td>{$user.id_card}</td>
                                <td>{$user.name}</td>
                                <td>{$section.name}</td>
                                <td>病假</td>
                                <td style="text-align:center;">{if $total.bingjia_total>0}{$total.bingjia_total}{else}0{/if}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- block -->
        <div id="ajax_content" class="block">

                {include file="./widget/list.tpl"}

        </div>
        <!-- /block -->
    </div>

</div>
<script type="text/javascript">
var absenteeismType = {json_encode($absenteeismType)};
{literal}
$(function(){
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
    $('.btn').click(function(){
        var absenteeism_type = $(this).parentsUntil('tr','td').find('[name="absenteeism_type"]').val();  
        var id = $(this).attr('data-value');
        var me = $(this);
        if(absenteeism_type != 0){
            $.ajax({
                url :'/checkinout/edit',
                data : {id:id,absenteeism_type:absenteeism_type},
                type : 'post',
                dataType : 'json',
                success : function(data){
                    if(data.code == 0){
                        me.parentsUntil('tr','td').html(absenteeismType[absenteeism_type]); 
                    } 
                }
            }) 
        }
    });      
});
{/literal}


    $("#ajax_content").delegate("#select_num","change",function(){

        var select_num = $("#select_num").val(); 

        check_loading.show();
        $.ajax({
            url: '/vacationrecord/list?selectNum='+select_num,    
            dataType: 'html',
            success:function(html){

                setTimeout(function(){
                        check_loading.hide();
                        $('#ajax_content').html(html);
                    }, 500);

            }
        }) 

            
    })

</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
