{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">人工修正年假</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
    </style>
    <div class="row-fluid">
        <div>
            <a href="/vacationrecord/upexcel">上传剩余年假病假文件</a> 
        </div>
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">为员工修正假期,可增可减</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/vacationrecord/finishing/" method="post">
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            {if $msg}<h3 style="color:red;font-size:18px;text-align:center;">{$msg}</h3>{/if}
                            <div class="control-group">
                                <label class="control-label" for="typeahead">工号</label>
                                <div class="controls">
                                    <input type="text" class="span6 required" id="typeahead"  data-provide="typeahead" data-required="1" name="id_card" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">假期类别</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select required" name="absence_type">
                                        <option value="2">年假</option>
                                        <option value="1">病假</option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">假期天数</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required number" placeholder="半天请用0.5表示" name="day_num" value="" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">备注</label>
                                <div class="controls">
                                    <textarea class="input-xlarge required" name="remark" placeholder="请输入备注..." style="width: 575px; height: 100px"></textarea>

                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
});

$(function() {
    function calculateDays()
    {
        var absenceType = $('#select01').val();
        var startDay = $('[name="start_day"]').val();
        var endDay = $('[name="end_day"]').val();
        var startNode = $('[name="start_node"]').val();
        var endNode = $('[name="end_node"]').val();

        if(startDay != '' && endDay != '' && startNode != '' && endNode != '' && absenceType != ''){
            $.ajax({
                url : '/userworkflow/CalculateSpan',
                data : {start_day:startDay,end_day:endDay,start_node:startNode,end_node:endNode,absence_type:absenceType},
                type:'get',
                dataType:'json',
                success:function(data){
                    $('[name="day_number"]').val(data.dayNum);                     
                }
            });
        }
        
    }
    $('.time_node,#select01').change(function(){
        calculateDays();      
    });
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true,
        afterSelectDate: calculateDays
    });
});
$(document).delegate('form','submit',function(){
	var dayNum = $('[name="day_number"]').val();
	if(dayNum == 0){
		alert('请假天数不能为零');
		return false;
	}
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
