<div class="navbar navbar-inner block-header">
    <div class="muted pull-left">所有员工{$title}流水查询</div>
</div>
<div style=" margin-top: 10px;margin-left:20px;">
    <span>筛选查询：</span>

    <form method="GET" action="/vacationrecord/superlist">
    <select style="width:100px" id="select_num" name="select_num">

        {foreach from=$option_list item=val key=key}

            {if $key eq $select_num}

                <option value="{$key}" selected>{$val}</option>

            {else}

                <option value="{$key}">{$val}</option>

            {/if}

        {/foreach}

    </select>
    
    <!-- <input type="text" placeholder="输入部门名称" value="{$sname}" name="sname" style="margin&#45;left:20px">  -->

    <div class="control-group success">
        <div class="controls" style="margin-top:15px;">
        日期
            <div class="input-append date datetimepicker">
                <input class="add-on" type="text" value="{$start_date}" placeholder="起始日期" name="start_date">
                <span class="add-on">
            </div>
        到
            <div class="input-append date datetimepicker">
                <input class="add-on" type="text" value="{$end_date}" placeholder="结束日期" name="end_date">
                <span class="add-on">
            </div>

            <input type="text" placeholder="输入用户名,工号,ID或部门名" value="{$search}" name="search" style="margin-left:20px"> 

            <div class="btn-group">
                <button class="btn btn-success" style="margin-bottom:10px; background: none repeat scroll 0% 0% rgb(33, 122, 237); padding-left: 19px; padding-right: 19px;border-left-width:3px;border-right-width:3px;" type="submit">查询 </button>


            </div>
            <div class="btn-group" style="width: 72px; height: 40px;">
                <a style="margin-left:20px;" href="/export/superlist{$query}">
                <div class="btn btn-success">保存 </div>
                </a>
            <div>
        </div>

    </div>

    </form>
</div>

<div  class="block-content collapse in">
<div id="liucheng" class="span12">

    <div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
        <ul>
        {pager_oa count=$total pagesize=$rn page=$pn pagelink="$url&pn=%d" list=3} </ul>
    </div>

    <table class="table table-hover">
        <thead>
            <tr>
                <th width="8%">员工号</th>
                <th width="8%">姓名</th>
                <th width="12%">部门</th>
                <th>类型</th>
                <th width="16%">日期</th>
                <th width="16%">星期</th>
                <th style="text-align:center;">天数</th>
                <th style="text-align:left;">原因</th>
            </tr>
        </thead>
        <tbody>
            {foreach $list as $item}
            <tr>
                <td>{$item.id_card}</td>
                <td>{$item.uname}</td>
                <td>{$item.sname}</td>
                <td>{if $item.absence_type == 1}病假 {else}年假 {/if}</td>
                <td>{strtotime($item.date)|date_format:'%Y-%m-%d'}</td>
                <td>星期{$week[date('w',strtotime($item.date))]}</td>
                <td style="text-align:center;">{$item.day_num}</td>
                <td>{if $item.remark}{$item.remark}{elseif $item.absence_id}<a href="http://oa.rong360.com/userworkflow/look?absence_id={$item.absence_id}">请假</a>{/if}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
        <ul>
        {pager_oa count=$total pagesize=$rn page=$pn pagelink="$url&pn=%d" list=3} </ul>
    </div>
</div>
