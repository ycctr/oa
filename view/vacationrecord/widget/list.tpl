<div class="navbar navbar-inner block-header">
    <div class="muted pull-left">{$title}流水</div>
</div>
<div style=" margin-top: 10px;margin-left:20px;">
    <span>筛选查询：</span>

    <select style="width:100px" id="select_num" name="select_num">

        {foreach from=$option_list item=val key=key}

            {if $key eq $selectNum}

                <option value="{$key}" selected>{$val}</option>

            {else}

                <option value="{$key}">{$val}</option>

            {/if}

        {/foreach}

    </select>
</div>

<div  class="block-content collapse in">
<div id="liucheng" class="span12">
    <table class="table table-hover">
        <thead>
            <tr>
                <th width="8%">员工号</th>
                <th width="8%">姓名</th>
                <th width="12%">部门</th>
                <th>类型</th>
                <th width="16%">日期</th>
                <th width="16%">星期</th>
                <th style="text-align:center;">天数</th>
                <th style="text-align:left;">原因</th>
            </tr>
        </thead>
        <tbody>
            {foreach $list as $item}
            <tr>
                <td>{$user.id_card}</td>
                <td>{$user.name}</td>
                <td>{$section.name}</td>
                <td>
                    {if isset($absenceType[$item.absence_type])}
                        {$absenceType[$item.absence_type]}
                    {elseif $item.absence_type == 13}
                        出差
                    {else}
                        其他
                    {/if}
                </td>
                <td>{strtotime($item.date)|date_format:'%Y-%m-%d'}</td>
                <td>星期{$week[date('w',strtotime($item.date))]}</td>
                <td style="text-align:center;">{$item.day_num}</td>
                <td>{if $item.remark}{$item.remark}{elseif $item.absence_id}<a href="http://oa.rong360.com/userworkflow/look?id={$item.user_workflow_id}">请假</a>{/if}</td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    <div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
        <ul>
        {pager_oa count=$absence_type_total pagesize=$rn page=$pn pagelink="list?pn=%d&selectNum=$selectNum" list=3} </ul>
    </div>
</div>
