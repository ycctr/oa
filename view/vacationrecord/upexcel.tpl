{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">上传年假、病假余额</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
        .file-box{ position:relative;width:640px}
        .txt{ height:22px; border:1px solid #cdcdcd; width:180px;}
        .file{ position:absolute; top:0; right:190px; height:24px; filter:alpha(opacity:0);opacity: 0;width:260px }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">上传本月假期余额，注意不要重复上传</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" enctype="multipart/form-data" action="/vacationrecord/upexcel/" method="post">
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            {if $msg}<h3 style="color:red;font-size:18px;text-align:center;">{$msg}</h3>{/if}
                            {if !empty($isfail)}
                                <p>用户Id:{implode(',',$isfail)}的记录上传失败，请确认这几位是否录入OA系统。<br />
                                如果需要补录，请单独上传这几位的记录</p>
                            {/if}
                            <div class="control-group file-box">
                                <label class="control-label" for="typeahead">上传文件</label>
                                <div class="controls">
                                    <input type='text' name='textfield' id='textfield' class='txt' />  
                                    <input type='button' class='btn' value='浏览...' />
                                    <input type="file" name="file" class="file" id="fileField" size="28" onchange="document.getElementById('textfield').value=this.value" />
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">上传</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
});

$(function() {
    function calculateDays()
    {
        var absenceType = $('#select01').val();
        var startDay = $('[name="start_day"]').val();
        var endDay = $('[name="end_day"]').val();
        var startNode = $('[name="start_node"]').val();
        var endNode = $('[name="end_node"]').val();

        if(startDay != '' && endDay != '' && startNode != '' && endNode != '' && absenceType != ''){
            $.ajax({
                url : '/userworkflow/CalculateSpan',
                data : {start_day:startDay,end_day:endDay,start_node:startNode,end_node:endNode,absence_type:absenceType},
                type:'get',
                dataType:'json',
                success:function(data){
                    $('[name="day_number"]').val(data.dayNum);                     
                }
            });
        }
        
    }
    $('.time_node,#select01').change(function(){
        calculateDays();      
    });
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true,
        afterSelectDate: calculateDays
    });
});
$(document).delegate('form','submit',function(){
	var dayNum = $('[name="day_number"]').val();
	if(dayNum == 0){
		alert('请假天数不能为零');
		return false;
	}
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
