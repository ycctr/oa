{extends file="../_base.tpl"}
{block name='title'}团建报销导出{/block}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
{/block}
{block name="content"} 
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">
<div>
    <form  method="get" action="/export/teambuilding">
        <fieldset class="form-horizontal">
        <div class="control-group" style="margin-top:30px;">
            <label class="control-label" style="width:60px;text-align:left;" for="date01">日期</label>
            <div class="controls" style="margin-left:60px;">
                <div class="input-append date datetimepicker">
                    <input class="add-on" name="start_day" placeholder="请输入开始时间" type="text" value="{$start_day}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                到
                <div class="input-append date datetimepicker">
                    <input class="add-on" name="end_day" placeholder="请输入结束时间" type="text" value="{$end_day}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="control-group" style="margin-top:30px;">
                <label class="control-label" style="width:60px;text-align:left;">部门</label>
                <div class="input-append">
                    <input style="width:120px;" name="section_name" placeholder="请输入部门名称" type="text" value="{$section_name}" />
                </div>

                <select name="team_type" style="width:120px;margin:0 15px;">
                    <option {if $team_type == 1}selected{/if} value="1">团建经费</option>
                    <option {if $team_type == 2}selected{/if} value="2">激励经费</option>
                </select>

                <button class="btn btn-success" type="submit">搜索</button>
        </div>
        </fieldset>
    </form>
</div>
{if !empty($arrList)}
<div class="block">
 <div class="navbar navbar-inner block-header">
    <div class="muted pull-left">团建流水</div>
</div>
 <div class = "block-content collapse in">
    <div class="span12">
    <div class="table-toolbar" style="margin-bottom:18px;">
        <div class="btn-group">
            <a href="{$savelink}">
                <button class="btn btn-success">导出表格
                </button>
            </a>
        </div>
    </div>
    {if $team_type ==1}
        <table class="table table-hover">
            <thead>
            <tr>
                <td>业务线</td>
                <td>部门</td>
                <td>负责人</td>
                <td>活动时间</td>
                <td>活动类型</td>
                <td>活动目的</td>
                <td>活动内容</td>
                <td>参加人数</td>
                <td>实际花费</td>
                <td>审核状态</td>
            </tr>
            </thead>
            <tbody>
            {foreach $arrList as $key => $record}
                <tr>
                    <td>{$record.team}</td>
                    <td>{$record.s_name}</td>
                    <td>{$record.uname}</td>
                    <td>{date('Y-m-d',$record.time)}</td>
                    <td>{$activity_conf[$record.type]}</td>
                    <td>{$record.title}</td>
                    <td>{$record.desc}</td>
                    <td>{count($sectionUserNum[$record.s_id]['users'])}</td>
                    <td>{$record.amount}</td>
                    <td>{$audit_status[$record.audit_status]}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    {else}
        <table class="table table-hover">
            <thead>
            <tr>
                <td>业务线</td>
                <td>部门</td>
                <td>负责人</td>
                <td>激励详情</td>
                <td>实际花费</td>
                <td>审核状态</td>
            </tr>
            </thead>
            <tbody>
            {foreach $arrList as $key => $record}
                <tr>
                    <td>{$record.team}</td>
                    <td>{$record.s_name}</td>
                    <td>{$record.uname}</td>
                    <td>{$record.remark}</td>
                    <td>{$record.amount}</td>
                    <td>{$audit_status[$record.audit_status]}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    {/if}
    </div>
</div>
</div>
{/if}
</div>
<script>
$(function() {
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
});
</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>
{/block}
