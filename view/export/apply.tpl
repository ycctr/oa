{extends file="../_base.tpl"}
{block name="css-page"}
<link href="../static/vendors/datepicker.css" rel="stylesheet" media="screen">
{/block}
{block name='title'}物品申请导出{/block}
{block name="content"} 
{include file="../widget/left-nav.tpl"}
 
<div id="content" class="span9">
<div>
    <form  method="post" action="/export/apply" style="margin-bottom:0px">
        <fieldset>
        <div class="control-group success">
            <div class="controls" style="margin-top:15px;">
                日期
               <div class="input-append date datetimepicker">
                    <input class="add-on" name="start_day" placeholder="请输入开始时间" type="text" value="{$start_day}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                到
                <div class="input-append date datetimepicker">
                    <input class="add-on" name="end_day" placeholder="请输入结束时间" type="text" value="{$end_day}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="control-group">
            部门<input type="text" name="section" style="margin:0 15px; width:120px;"></input>
            姓名<input type="text" name="name" value="{$smarty.get.query.name}" style="margin:0 15px; width:120px;"></input>
            工号<input type="text" name="id_card" value="{$smarty.get.query.id_card}" style="width:120px;margin-left:15px;"></input>
        </div>
         <button class="btn btn-success" type="submit">导出</button>
        </fieldset>
    </form>
</div>
<script>
$(function() {
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
});
</script>
{/block}
{block name="js-page"}
<script src="../static/vendors/bootstrap-datetimepicker.js"></script>
<script src="../static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>
{/block}
