{extends file="../_base.tpl"}
{block name='title'}考勤导出{/block}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
{/block}
{block name="content"} 
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">
<div>
    <form  method="post" action="/export/expense/">
        <fieldset class="form-horizontal">
        <div class="control-group" style="margin-top:30px;">
            <label class="control-label" style="width:60px;text-align:left;" for="date01">日期</label>
            <div class="controls" style="margin-left:60px;">
                <div class="input-append date datetimepicker">
                    <input class="add-on" name="start_day" placeholder="请输入开始时间" type="text" value="{$start_day}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                到
                <div class="input-append date datetimepicker">
                    <input class="add-on" name="end_day" placeholder="请输入结束时间" type="text" value="{$end_day}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                <button class="btn btn-success" type="submit">导出</button>
            </div>
        </div>
        </fieldset>
    </form>
</div>
<script>
$(function() {
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
});
</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>
{/block}
