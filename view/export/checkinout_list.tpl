{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
{/block}
{block name='title'}考勤记录查询
{/block}
{block name="content"} 
{include file="../widget/left-nav.tpl"}
 
<div id="content" class="span9">
<div>
    <form  method="get" action="" style="margin-bottom:0px">
        <fieldset>
        <div class="control-group success">
            <div class="controls" style="margin-top:15px;">
                日期
                <div class="input-append date datetimepicker">
                    <input class="add-on" name="start_date" placeholder="起始日期" type="text" value="{$start_date}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                                    到
                <div class="input-append date datetimepicker">
                    <input class="add-on" name="end_date" placeholder="结束日期" type="text" value="{$end_date}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="control-group">
            部门<input type="text" name="section_name" value="{$section_name}" style="margin:0 15px;width:120px;"></input>
            姓名<input type="text" name="user_name" value="{$user_name}"style="margin:0 15px;width:120px;"></input>
            工号<input type="text" name="id_card" value="{$id_card}"style="margin:0 15px;width:120px;"></input>
            &nbsp;&nbsp;&nbsp;<button class="btn btn-success" type="submit" style="margin-bottom:10px; background: none repeat scroll 0% 0% rgb(33, 122, 237);padding-left: 19px; padding-right: 19px;border-left-width:3px;border-right-width:3px;">查询</button>
        </div>
        </fieldset>
    </form>
</div>

<div class="block">

 <div class="navbar navbar-inner block-header">
    <div class="muted pull-left">
     考勤记录
    </div>
</div>
 <div class = "block-content collapse in">
    <div class="span12">
    <div class="table-toolbar" style="margin-bottom:18px;">
        <div class="btn-group">
            <a href="{$savelink}">
                <button class="btn btn-success">导出表格
                </button>
            </a>
        </div>
    </div>


        <table class="table table-hover">
            <thead>
            <tr>
                <td>员工号</td>
                <td>姓名</td>
                <td>部门</td>
                <td>日期</td>
                <td>星期</td>
                <td>上班时间</td>
                <td>下班时间</td>
                <td>异常状态</td>
            </tr>
            </thead>
            <tbody>
            {foreach $data as $key => $record}
                <tr>
                    <td>{$record.id_card}</td>
                    <td>{$record.user_name}</td>
                    <td>{$record.section_name}</td>
                    <td>{$record.date}</td>
                    <td>{$record.week}</td>
                    <td>{$record.checkin_time}</td>
                    <td>{$record.checkout_time}</td>
                    <td>{$record.absenteeism_type}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>



        <div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
            <ul>
                {pager_oa count=$arrPager.count pagesize=$arrPager.pagesize page=$arrPager.page pagelink=$arrPager.pagelink list=3}
            </ul>
        </div>
    </div>
</div>
</div>
<script>
$(function() {
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
});
</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>
{/block}
