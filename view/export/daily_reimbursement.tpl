{extends file="../_base.tpl"}
{block name='title'}日常报销导出{/block}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
{/block}
{block name="content"} 
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">
<div>
    <form  method="post" action="/export/completedailyreimbursement">
        <fieldset class="form-horizontal">
        <div class="control-group" style="margin-top:30px;">
            <label class="control-label" style="width:60px;text-align:left;" for="date01">日期</label>
            <div class="controls" style="margin-left:60px;">
                <input style="width:150px;" type="text" class="input-xlarge datepicker required" placeholder="起始日期" id="date01" name="start_day" value="">
                到
                <input style="width:150px;" type="text" class="input-xlarge datepicker required" placeholder="结束日期" name="end_day" value="">
                <button class="btn btn-success" type="submit">导出</button>
            </div>
        </div>
        </fieldset>
    </form>
</div>
<script>
$(function() {
        $(".datepicker").datepicker();
});
</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datepicker.js"></script>
{/block}
