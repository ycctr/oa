{extends file="../_base.tpl"}
{block name='title'}考勤导出{/block}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
{/block}
{block name="content"} 
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">
<div>
    <form  method="post" action="/export/statistics/">
        <fieldset class="form-horizontal">
        <div class="control-group" style="margin-top:30px;">
            <label class="control-label" style="width:60px;text-align:left;" for="date01">日期</label>
            <div class="controls" style="margin-left:60px;">
                <div class="input-append date datetimepicker">
                    <input class="add-on" name="start_day" placeholder="请输入开始时间" type="text" value="{$start_day}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                到
                <div class="input-append date datetimepicker">
                    <input class="add-on" name="end_day" placeholder="请输入结束时间" type="text" value="{$end_day}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                <button class="btn btn-success" type="submit">导出</button>
            </div>
        </div>
        </fieldset>
    </form>
    <div>
        <h3>说明：</h3>
        <p>员工维护的各种异常只计算工作日的天数，唯一例外 “部门排班” 计算考勤周期内的所有标记为“部门排班”的天数</p>
        <p>根据年假记录 统计出各种请假的天数,年假记录 是根据 请假记录和销假记录计算出来的</p>
        <p>打卡记录 删除掉没有打卡并且没有维护的天数</p>
        <p>因公外出、忘打卡等用户维护的状态 根据上面的打卡记录计算出来 只计算工作日 <br />例外：部门排班 包含节假日的“部门排班”</p>
        <p>考勤异常：工作日内用户没有维护状态的异常考勤（10:30以后、5:30之前打卡）</p>
        <p>正常打卡：工作日内正常打卡的天数（10:30之前、5:30之后打卡）</p>
        <p>出勤天数：正常打卡 + （员工维护的异常打卡 - 出差 - 调休）</p>
        <p>应发薪天数： 应出勤 - 事假 - 病休假</p>
        <p>实际发薪天数： 正常打卡 + 员工维护异常 + 带薪假期（除了 事假、病休假、其他 ）</p>
        <p>早餐：工作日打卡时间在早6点至早8点之间的</p>
    </div>
</div>
<script>
$(function() {
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
});
</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>
{/block}
