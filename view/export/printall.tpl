{extends file="../_base.tpl"}
{block name='title'}剩余年假病假导出{/block}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
{/block}
{block name="content"} 
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">
<div>
    <form  method="post" action="/export/vacation">
        <fieldset class="form-horizontal">
        <div class="control-group" style="margin-top:30px;">
            <label class="control-label" style="width:90px;text-align:left;" for="date01">请选择日期:</label>
            <div class="controls" style="margin-left:60px;">
                <div class="input-append date datetimepicker">
                    <input class="add-on" name="start_day" placeholder="请输入时间" type="text" value="{$start_day}" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                <button class="btn btn-success" type="submit">导出</button>

                <span style="color:red;margin-left:10px">(只能选择当日<span style="color:blue;margin:0px 5px;">{$cur_date}</span>之前的日期)</span>
            </div>
            {if $msgno == "-1"}

                <div style="color:red;margin-top:10px;letter-spacing: 2px;"> * 对不起!必须选择<span style="color:blue;margin:0px 5px;font-size:20px">{$cur_date}</span>之前日期才导出！</div>

            {/if}

            {if $msgno == "-3"}

                <div style="color:orange;margin-top:10px;letter-spacing: 2px;"> * 对不起!<span style="color:blue;margin:0px 5px;font-size:20px">{$start_day}</span>之前没有请假数据！</div>

            {/if}
        </div>
        </fieldset>
    </form>
</div>
<script>
$(function() {
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
});
</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>
{/block}
