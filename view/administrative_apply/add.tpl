{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">

    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li>
                <a href="/userworkflow/list">&nbsp;&nbsp;发起流程</a> <span class="divider">/</span> 
            </li>
            <li class="active">物品申领</li>
            </ul>
        </div>
    </div>
    <style>
        .form-horizontal .title {
            height:40px;
            line-height:40px;
            margin-bottom:20px;
            padding-left:180px;
        }
        .form-horizontal .title label {
            height:40px;
            line-height:40px;
            float:left;
            width:160px;
        }
    </style>
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">物品申领</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    <form id="form_sample_1" class="form-horizontal" action="/administrativeapply/addpost" method="post">
                        <input type="hidden" name="workflow_id" value="{$workflow_id}" />
                        <fieldset>
                            <div class="alert alert-error hide">
                                <button class="close" data-dismiss="alert"></button>
                                你的表单填写有错误，请检查!
                            </div>
                            <div class="alert alert-success hide">
                                <button class="close" data-dismiss="alert"></button>
                                Your form validation is successful!
                            </div>
                            <div class="title">
                                <label for="">姓名：{$user.name}</label>
                                <label for="">工号: {$user.id_card}</label>
                                <label for="">部门：{$section.name}</label>
                                <label for="">日期：{$smarty.now|date_format:"%Y-%m-%d"}</label>
                            </div>
                            <div style="color:red;text-align:center;padding-bottom:20px;">
                                因审批流程及采购均需要时间，为了避免耽误使用而产生不必要的影响，请至少提前3个工作日提交申请，感谢您的配合！
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="select01">物品名称</label>
                                <div class="controls">
                                    <select id="select01" class="chzn-select required" name="staff_main">
                                    <option value=0>请选择</option>
                                    {foreach $staff as $key => $val}
                                    <option value="{$key}">{$val['main']}</option>
                                    {/foreach}
                                    </select>
                                    <select id="select011" class="chzn-select required hide" name="staff_sub">
                                    </select>
                                    <input style="display:none;" type="text" name="staff_other" />
                                    <span id="equipment" style="display:none;">
                                        名称：<input type="text" style="width:100px;" name="staff_sub_other_name"  />
                                        型号：<input type="text" style="width:100px;" name="staff_sub_other_type" />
                                    </span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="date01">数量</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge required" name="number" value="" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">申请用途</label>
                                <div class="controls">
                                    <textarea class="input-xlarge required" name="usage"  style="width: 575px; height: 100px"></textarea>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">提交</button>
                                <button type="reset" class="btn">重置</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>
<script>
var data = {json_encode($staff)};
</script>
{literal}
<script>
;
jQuery(document).ready(function() {   
    FormValidation.init();
    alert("用于生育礼金、推荐奖励、非标配设备、小礼品等的申请");
});

// console.log(data);
$(function(){
    var staff_other = $("input[name='staff_other']");
    var staff_sub_other = $('#equipment');
    var staff_sub_other_name = $("input[name='staff_sub_other_name']");
    var staff_sub_other_type = $("input[name='staff_sub_other_type']");
    var staff_sub = $('#select011');
    $('#select01').on('change',function(){
        var val1 = $(this).val();
        if(val1!=0){
            var sub_json = data[val1]['sub'];
        }
        if(val1==4){
            staff_other.show();
            staff_sub_other.hide();
            staff_sub.hide();
            
        }
        else if(val1==2){
            staff_other.hide();
            staff_sub_other.show();
            staff_sub.hide();
            alert("非标配设备名称及型号需员工手动填写！");
        }
        else{
            staff_other.hide();
            staff_sub_other.hide();
            staff_sub.show();
            staff_sub.empty();
            staff_sub.append('<option value=0>请选择</option>');
            if(sub_json){
                for(i=0;i<sub_json.length;i++){
                    text='<option value='+(i+1)+'>'+sub_json[i]+'</option>';
                    staff_sub.append(text);
                }
            }
        }
        staff_sub.is(":visible")?staff_sub.addClass('required'):staff_sub.removeClass('required');
        staff_other.is(":visible")?staff_other.addClass('required'):staff_other.removeClass('required');
        staff_sub_other_name.is(":visible")?staff_sub_other_name.addClass('required'):staff_sub_other_name.removeClass('required');
        staff_sub_other_type.is(":visible")?staff_sub_other_type.addClass('required'):staff_sub_other_type.removeClass('required');
    });
    $('#select011').on('change',function(){
        var val1 = $('#select01').val();
        var val2 = $('#select011').val();
        if(val1==1){
            val2==5?staff_other.show():staff_other.hide();
        }
        if(val1==3){
            val2==5?staff_other.show():staff_other.hide();
        }
        staff_other.is(":visible")?staff_other.addClass('required'):staff_other.removeClass('required');
    }); 
});
</script>
{/literal}
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
