{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">
    <div class="row-fluid">
        <form action="/checkinout/list" method="get" style="margin-top:30px;">
            <div class="input-append date datetimepicker">
                <input class="add-on" name="start_day" placeholder="请输入开始时间" type="text" value="{$start_day}" />
                <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                </span>
            </div>
            <div class="input-append date datetimepicker">
                <input class="add-on" name="end_day" placeholder="请输入结束时间" type="text" value="{$end_day}" />
                <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                </span>
            </div>
            {if $isManager}
            <div class="input-append">
                <input name="subordinate_name" placeholder="查询下属员工考勤,请输入员工姓名" type="text" value="{$subordinate_name}" />
            </div>
            {/if}
            <input style="margin-bottom:12px;" class="btn btn-success" type="submit" value="search" />
        </form>
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">考勤记录</div>
            </div>
            {if $kaoqinException}
            <div style="line-height:40px;color:red;text-align:center;">亲爱的融融，考勤可能存在异常，如果您的考勤记录异常并且确信自己已打卡，请不要维护状态，系统会自动修复。</div>
            {/if}
            <div class="block-content collapse in">
                <div id="liucheng" class="span12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th width="8%">员工号</th>
                                <th width="8%">姓名</th>
                                <th width="12%">部门</th>
                                <th width="16%">日期</th>
                                <th width="18%">上班时间</th>
                                <th width="18%">下班时间</th>
                                <th style="text-align:center;">异常描述</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach $list as $item}
                            <tr>
                                <td>{$user.id_card}</td>
                                <td>{$user.name}</td>
                                <td>{$section.name}</td>
                                <td>{strtotime($item.date)|date_format:'%Y-%m-%d'} 星期{$week[date('w',strtotime($item.date))]}</td>
                                <td>{if $item.checkin_time != '0000-00-00 00:00:00'}{$item.checkin_time}{/if}</td>
                                <td>{if $item.checkout_time != '0000-00-00 00:00:00'}{$item.checkout_time}{/if}</td>
                                <td style="text-align:center;">
                                {if $item.absenteeism_type}
                                {$absenteeismTypeAll[$item.absenteeism_type]}
                                {else}
                                {if $item.abnormal} 
                                {if empty($item.absenteeism_type)}
                                <select style="width:120px;" name="absenteeism_type">
                                    <option value="0">考勤异常原因</option>
                                    {foreach $absenteeismType as $key => $val}
                                    <option value="{$key}">{$val}</option>
                                    {/foreach}
                                </select>
                                <input class="btn btn-warning btn-mini" style="margin-bottom:10px;" type="button" data-value="{$item.id}" value="保存" />
                                {/if}
                                {/if}
                                {/if}
                                </td>
                            </tr>
                            {/foreach}
                        </tbody>
                    </table>
                    <div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
                        <ul>
                            {pager_oa count=$listCnt pagesize=$rn page=$pn pagelink=$url|cat:"pn=%d" list=3}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>

</div>
<script type="text/javascript">
var absenteeismType = {json_encode($absenteeismType)};
{literal}
$(function(){
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-MM-dd',
        language: 'zh-CN',
        pickDate: true,
        pickTime: false,
        hourStep: 1,
        minuteStep: 15,
        secondStep: 30,
        inputMask: true
    });
    $('.btn').click(function(){
        var absenteeism_type = $(this).parentsUntil('tr','td').find('[name="absenteeism_type"]').val();  
        var id = $(this).attr('data-value');
        var me = $(this);
        if(absenteeism_type != 0){
            $.ajax({
                url :'/checkinout/edit',
                data : {id:id,absenteeism_type:absenteeism_type},
                type : 'post',
                dataType : 'json',
                success : function(data){
                    if(data.code == 0){
                        me.parentsUntil('tr','td').html(absenteeismType[absenteeism_type]); 
                    } 
                }
            }) 
        }
    });      
});
{/literal}
</script>
{/block}
{block name="js-page"}
<script src="/static/vendors/bootstrap-datetimepicker.js"></script>
<script src="/static/vendors/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="/static/vendors/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/assets/form-validation.js"></script>
{/block}
