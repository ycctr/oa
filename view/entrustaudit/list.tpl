{extends file="../_base.tpl"}
{block name="css-page"}
<link href="/static/vendors/datepicker.css" rel="stylesheet" media="screen">
<link href="/static/vendors/uniform.default.css" rel="stylesheet" media="screen">
<link href="/static/vendors/chosen.min.css" rel="stylesheet" media="screen">
{/block}
{block name="content"}
{include file="../widget/left-nav.tpl"}
<div id="content" class="span9">
    <div class="row-fluid">
        <div style="margin-top:30px;text-align:right;"><a href="/entrustaudit/add">添加</a></div>
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">委托审批列表</div>
            </div>
            <div class="block-content collapse in">
                <div id="liucheng" class="span12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th width="15%">部门</th>
                                <th width="15%">流程类型</th>
                                <th>委托人</th>
                                <th>代理人</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach $list as $item}
                            <tr>
                                <td>{$item.section_name}</td>
                                <td>{$workflow_type[$item.obj_type]}</td>
                                <td>{$item.entrust_name}</td>
                                <td>{$item.trustee_name}</td>
                                <td><a style="cursor:pointer" class="del" data-value="{$item.id}">删除</a></td>
                            </tr>
                            {/foreach}
                        </tbody>
                    </table>
{*
                    <div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
                        <ul>
                            {pager_oa count=$listCnt pagesize=$rn page=$pn pagelink=$url|cat:"pn=%d" list=3}
                        </ul>
                    </div>
*}
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>

</div>
<script type="text/javascript">
    $('.del').click(function(){
        var id = $(this).attr('data-value');
        var me = this;
        if(id){
            $.ajax({
                url:'/entrustaudit/del?id='+id,
                type:'post',
                dataType:'json',
                success:function(data){
                    if(data.code > 0){
                        alert(data.msg);
                    }else{
                        $('.del').parentsUntil('tbody','tr').remove();
                    } 
                }
            });
        }
    });
</script>
{/block}
