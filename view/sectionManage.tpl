{extends file="./_base.tpl"}
{block name='title'}部门管理
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}

<div id="content" class="span9">
<div class="block" style="margin-top:30px;">
 <div class="navbar navbar-inner block-header">
    <div class="muted pull-left">
     部门
    </div>
 </div>
 <div class="block-content collapse in">
    <div class="span12">
    <div class="table-toolbar" style="margin-bottom: 18px;">
        {if (!is_null($data.message.error))}
            <div class="alert alert-error ">
                {$data.message.error}
            </div>
        {/if}
        {if $add_p eq 1}
        <div class="btn-group">
            <a href="add">
                <button class="btn btn-success">增加部门
                    <i class="icon-plus icon-white"></i>
                </button>
            </a>
        </div>
        {/if}
        <div class="span4" style="float:right">
            <form method="post" action="manage" style="margin-bottom:0px">
                <input type="text" name="Condition" value="{$Condition}" placeholder="输入部门名称或负责人姓名查询"></input>
            <div class="btn-group">
                <button class="btn btn-success" type="submit" style="margin-bottom:10px; background: none repeat scroll 0% 0% rgb(33, 122, 237); padding-left: 19px; padding-right: 19px;border-left-width:3px;border-right-width:3px;">查询
            </button>
        </div>
     </form>
</div>
    </div>
        {include file="./widget/sectionManage_table.tpl"}
        <div class="dataTables_paginate paging_bootstrap pagination" style="margin-top: 0px; margin-bottom: 0px;">
            <ul>
                {pager_oa count=$data.page.recordCount pagesize=$data.page.pagesize page=$data.page.pn pagelink="manage?pn=%d&Condition=$Condition" list=3}
            </ul>
        </div>
    </div>
{/block}


