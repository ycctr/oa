{extends file="./_base.tpl"}
{block name="content"}
{include file="./widget/left-nav.tpl"}
<style>
    .table td {
        border:0;
    }
</style>
<div id="content" class="span9">
    <div class="navbar">
        <div class="navbar-inner">
            <ul class="breadcrumb">
            <li>
                <a href="/userworkflow/my">流程审批</a> <span class="divider">/</span>    
            </li>
            <li class="active">发起流程</li>
            </ul>
        </div>
    </div>
    <div class="row-fluid">
        {foreach from = $workflows key=key item=category name=foo}
        <div class="span6 {$smarty.foreach.foo.index} " {if $smarty.foreach.foo.index % 2 == 0}style="margin-left:0;"{/if}>
            <div class="block">
                <div class="navbar navbar-inner block-header">
                    <div class="muted pull-left">{$workflowCategory[$key]}</div>
                </div>
                <div class="block-content collapse in">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>
                                    {foreach $category as $item}
                                        {if !empty($item.url)}
                                    <span style="margin:0 10px;line-height:30px;word-break:keep-all;white-space:nowrap;"><a href="/{$item.url}?workflow_id={$item.id}">{$item.name}</a></span>
                                        {else}
                                    <span style="margin:0 10px;line-height:30px;word-break:keep-all;white-space:nowrap;"><a href="/userworkflow/add?workflow_id={$item.id}">{$item.name}</a></span>
                                        {/if}
                                    {/foreach}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {/foreach}
    </div>
</div>
{/block}
