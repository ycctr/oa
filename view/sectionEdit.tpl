{extends file="./_base.tpl"}
{block name='title'}部门编辑
{/block}
{block name="css-common"}
<link href="/static/jquery-autocomplete/jquery.autocomplete.css" rel="stylesheet" type="text/css">
{/block}
{block name="content"} 
{include file="./widget/left-nav.tpl"}
<div id="content" class="span9">
<div class="block" style="margin-top:30px;">
 <div class="navbar navbar-inner block-header">
    <div class="muted pull-left">
     部门
    </div>
 </div>
 <div class="block-content collapse in">
    <div class="span12">
        <form method="post" class="form-horizontal" action="editPost">
            <fieldset>
                <legend>
                    编辑部门
                </legend>
                {if (!is_null($data.message.error))}
                    <div class="alert alert-error ">
                        {$data.message.error}
                    </div>
                {/if}
                <div class="control-group">
                    <input type="hidden" name="id" value="{$data.info.id}" ></input>
                </div>
                <div class="control-group" text-align:center>
                    <label class="control-label" style="margin-right: 15px;">部门名称</label>
                    <input type="text" name="section_name" value="{$data.info.section_name}"></input></br>
                </div>
                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">上级部门</label>
                    <input id="parentId" type="text" name="parent_name" value="{$data.info.parent_name}" />
                    <input type="hidden" name="parent_id" value="{$data.info.parent_id}" />
                    <!-- <select  name="parent_id">
                    {foreach from=$data.sections item=item key=key}
                        {if ( $data.info.parent_id == $item.id  )}
                            <option value="{$item.id}" selected="selected">{$item.name}</option> 
                        {else}
                            <option value="{$item.id}">{$item.name}</option>
                        {/if}
                    {/foreach}
                    </select></br> -->
                </div>
                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">部门经理工号</label>
                    <input type="text" name="manager" value="{$data.info.manager_id}"></input></br>
                </div>
                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">部门经理姓名</label>
                    <p style="padding-top:5px;">{$data.info.manager_name}</p>
                </div>

                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">业务线</label>
                    <select  name="team_name" >

                        {foreach from="$section_team" item=item }

                        
                            {if ( $item == $section_team_target )}
                            
                                <option selected="selected" value="{$item}">{$item}</option>

                            {else}

                                <option value="{$item}">{$item}</option>

                            {/if}

                        {/foreach}

                    </select>
                </div>

                <div class="control-group">
                    <label class="control-label" style="margin-right: 15px;">权限</label>
                    <input id="can_building" type="checkbox" {if $data.info.can_building}checked="checked"{/if} name="can_building" value="1"  />团建</br>
                </div>

                <div class="control-group" id="senior_building"  style="{if $data.info.can_building == 0}display:none;{/if}">

                    <input type="radio" style="margin-left: 19%;" {if $data.info.senior_building == 0}checked="checked"{/if} name="senior_building" value="0" />普通团建
                    <input type="radio" {if $data.info.senior_building == 1}checked="checked"{/if} name="senior_building" value="1" style="margin-left: 15px;" />高级团建</br>
                    

                </div>

                <div class="control-group" id="team_encourage">

                    <label class="control-label" style="margin-right: 15px;">团建激励:</label>
                    <input type="radio" {if $data.info.team_encourage == 0}checked="checked"{/if} name="team_encourage" value="0" />关闭
                    <input type="radio" {if $data.info.team_encourage == 1}checked="checked"{/if} name="team_encourage" value="1" style="margin-left: 15px;" />开启</br>
                    

                </div>

                <div class="control-group">
                     <input type="hidden" name="create_time" value="{$data.info.create_time}"></input>
                </div>
                <div class="form-actions">
                    <button class="btn btn-primary" type="submit" width="10000">修改</button>
                    <button class="btn" type="reset">重置</button>
                </div>
            </fieldset>
        </form>
    </div>
 </div>
</div>
</div>
<script type="text/javascript" src="/static/jquery-autocomplete/jquery.autocomplete.min.js"></script>
<script>
    var data = [
        {foreach from=$data.sections item=item}
        ['{$item.name} {$item.id}', {$item.id}]{if $item@index < count($data.sections)-1},{/if}
        {/foreach}
    ];
    $("#parentId").result(function(event, data, formatted){ 
        $("input[name='parent_name']").val(data[0].split(" ")[0]);
        $("input[name='parent_id']").val(data[0].split(" ")[1]);
    });
    $('#parentId').autocomplete(data,{
        minChars: 0,
        max:300,
        delay:10,
        matchContains:true,
        multiple:false,
        formatItem: function(row) { return row[0]; }
    });
     
    $(function(){

        $("#can_building").click(function(){

            var can_building = $(this).is(':checked')
            // console.log(can_building);
            if(can_building) {

                $("#senior_building").show();
            
            }else {
            
                $("#senior_building").hide();

            }

        });

    })

</script>


{/block}
