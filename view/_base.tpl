<!DOCTYPE html >
{block name="status-ok"}<!--STATUS OK-->{/block}
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	{block name="head-more"}{/block} 
	{block name="head-tdk"}
	<title>{block name="title"} OA （测试版）{/block}</title>
	<meta name="description" content="{block name='description'}专业融资贷款搜索引擎。Rong360.com{/block}" />
	<meta name="keywords" content="{block name='keywords'}{/block}" />
	{/block}
	<meta http-equiv="Cache-Control" content="no-transform" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" ></meta>
	<link rel="shortcut icon" href="/static/favicon.ico" />
	{block name="css-base"}
        <link href="/static/bootstrap/css/bootstrap.min.css?v={$smarty.const.STATIC_VERSION}" rel="stylesheet" media="screen">
        <link href="/static/bootstrap/css/bootstrap-responsive.min.css?v={$smarty.const.STATIC_VERSION}" rel="stylesheet" media="screen">
        <link href="/static/assets/styles.css?v={$smarty.const.STATIC_VERSION}" rel="stylesheet" media="screen">

	<!-- 增加rongUI -->
	{/block}
	{block name="css-common"}{/block}
	{block name="css-page"}{/block}
	{block name="css"}{/block}
    <script src="/static/vendors/jquery-1.9.1.js"></script>
    <!--[if lte IE 8]>
    <script language="javascript" type="text/javascript" src="/static/vendors/flot/excanvas.min.js">
    </script><![endif]-->
    <script src="/static/vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <script type="text/javascript" src="/static/js/check_loading.js"></script>

</head>
<body>
    {block name="topad"}{/block}
    {* 增加页面变量列表 *}
    {block name="varlist"}
    {/block}
    {block name="header"}{include file="./header.tpl"} {/block}
    <div class="container-fluid"> 
        <div class="row-fluid">
        {block name="content"}{/block}
        </div>
    </div>
    {block name="_js-frame"}{*仅用于frame嵌套页面.*}{/block}
    {block name="js-common"}{/block}
    {block name="js-page"}{/block}
    {block name="js"}{/block}
    
    
    {block name="other-widget"}
    
    {/block}
    {block name="js-base"}

    <script src="/static/bootstrap/js/bootstrap.min.js"></script>
    {/block}
</body>
</html>
{* 
	head-more 放置head标签下的内容, 此处定义的title和meta优先级最高
	css-common 放置通用的css样式, 可用于多页面公用的抽象模板 
	css-page 放置当前页面的css样式, css定义的优先级高于css-common 
	css 预留的css块, 优先级最高
	header 放置页头 
	content 放置页面主体内容,不包含头尾 
	footer 放置页尾 
	js-common 放置通用的js代码, 可用于多页面公用的抽象模板 
	js-page 放置页面的js代码, 相同变量会覆盖 js-common 中的定义 
	js 预留的js块, 优先级最高 
*}

{* 
	使用说明: 在头部引入 extends file="PATH/TO/_base.tpl", 填充各block即可 
	模板示例:

{extends file="./_base.tpl"}

{block name='title'}{/block}
{block name='description'}{/block}
{block name='keywords'}{/block}

{block name="css-common"}
{/block}
{block name="js-common"}
{/block}
{block name="js-page"}
{/block}

{block name="content"}

{/block}
*}
