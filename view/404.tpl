{extends file="./_base.tpl"}

{*
<meta http-equiv="refresh" content="5;url=/">
*}
{block name="status-ok"}{/block}
{block name='title'}Rong360.com -- 您的访问出错了 {/block}
{block name="footer"}{/block}

{block name="css-common"}
<script type="text/javascript">
if(parent && parent.document && parent.document.location){
	var parentUrl = parent.document.location.href,
		selfUrl = document.location.href,
		IS_IN_FRAME;
		
	//说明页面在iframe中
	if(parentUrl !=selfUrl && parentUrl.indexOf('rong360.com')!=-1){
		IS_IN_FRAME = true;	
	}
}
</script>
<link href="{$Yii->params['STATIC_PATH']}/styles/search.css?v={$smarty.const.STATIC_VERSION}" rel="stylesheet" type="text/css">
<style>
html, body{
	height: 100%;
}
body{
	background: #edece8;
}
.in_frame{
	background: #fff;
}
.in_frame .d-d,
.frame_box{ display: none; }

.in_frame .frame_box{
	display:block;
}
.in_frame .form-wraper{
	width:auto;
}
.infotip{
	vertical-align:top;
	margin-top:20px;
	border-bottom:1px solid #d6dbe5;
	padding-bottom:45px;
}
.infotip .icon{
	display:inline-block;
	background-image:url(/static/assets/reflash_tip.png);
	width:60px;
	_float:left;
	height:75px;
	margin-right:15px;
	vertical-align:top;
}
.infotip .tiper{
	display:inline-block;	
	vertical-align:top;
	font-size:14px;
}
.infotip .tiper h3{
	font-size:22px;
	vertical-align:top;
	height:20px;
	line-height:20px;
	margin:0;
	padding:10px 0;
}
.buttons{
	padding-top:38px;
}
.d-d{
	margin: 0 auto;
	padding-top: 60px;
	text-align: center;
	width: 520px;
}
.d-d .msg{
	color: #aeaeae;
	font-size: 18px;
	padding: 40px 0;
}
.d-d .msg a{
	color: #4673b9;
}
.d-d .home-btn{
	background: #0055bb;
	border-radius: 5px;
	color: #fff;
	display: block;
	font-size: 18px;
	height: 35px;
	line-height: 35px;
	margin: 0 auto;
	text-align: 155px;
	width: 155px;
}
</style>
{/block}
{block name="js-common"}
{/block}

{block name="content"}
<script type="text/javascript">
//非iframe中自动调转
if(IS_IN_FRAME){
	var size = {
		width:540,
		height:285
	}
	window.parent.PopLayer.resize(size);
	document.body.className +="in_frame";
	//load css
	document.write('<link href="http://www.rong360.com/static//styles/register.css" rel="stylesheet" type="text/css">');
}
</script>

<div class="d-d">
    <img src="http://www.rong360.com/static/assets/404.png" style="border:0;" />
    <p class="msg">{if $message}{$message}，请返回上一页重新操作{else}您正在访问一个不存在的页面，建议您<a href="http://oa.rong360.com">返回OA首页</a>继续访问。{/if}</p>
    <a class="home-btn" href="http://oa.rong360.com">返回首页</a>
</div>
<div id="frame_box" class="frame_box form-wraper">
	<div class="register-close close"></div>
	<div class="infotip">
		<span class="icon"></span>
		<span class="tiper">
			<h3>温馨提示</h3>
			{if $code == 601}
				<div>您好，由于您长时间没有操作，页面已经失效。<br/>请返回上一页重新操作</div>
			{else}
				<div>{if $message}{$message}{else}您访问的页面不存在或已失效。{/if}<br/>请返回上一页重新操作</div>
			{/if}
		</span>
	</div>
	<div class="buttons">
		<a class="button green-button close" style="display:inline-block;font-size:14px;line-height:34px; text-decoration:none;color:#fff" href="#">确定</a>
	</div>
<script type="text/javascript">
	if(IS_IN_FRAME){
		$('#frame_box .close').on('click',function(){
			window.parent.PopLayer.close();
		});
	}
</script>
</div>
{/block}
