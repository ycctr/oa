{extends file="./_base.tpl"}

{*
<meta http-equiv="refresh" content="5;url=/">
*}
{block name="status-ok"}{/block}
{block name='title'}Rong360.com -- 您的访问出错了 {/block}

{block name="css-common"}
<link href="{$Yii->params['STATIC_PATH']}/styles/search.css?v={$smarty.const.STATIC_VERSION}" rel="stylesheet" type="text/css">
{/block}
{block name="js-common"}
{/block}

{block name="content"}
{*
<script src="{$Yii->params['JS_PATH']}/tmp/search.js?v={$smarty.const.STATIC_VERSION}" type="text/javascript"></script>
*}
<script type="text/javascript">
  
  window.setTimeout(function(){
    window.location.href='/';
  }, 5000);

</script>

<div class="d-d" style="margin:100px auto;padding-left:430px">
    <h2>很抱歉，系统暂时无法访问，请稍后重试</h2>
    <p style="padding-left:15px">5秒后将自动跳转到网站首页</p>
</div>


{/block}
